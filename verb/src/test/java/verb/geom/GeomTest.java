package verb.geom;

import haxe.root.Array;
import org.junit.After;
import org.junit.AfterClass;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import verb.AssertVerb;
import verb.core.Constants;
import verb.core.MeshData;
import verb.core.NurbsCurveData;
import verb.core.Vec;
import verb.eval.Intersect;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author Matthew Pyne
 */
public class GeomTest {

    void vecShouldBe(double[] expected, double[] test, double tol) {

        if (expected.length != test.length) {
            fail("Expected " + Arrays.toString(expected) + " but was " + Arrays.toString(test));
        }

        for (var i = 0; i < test.length; i++) {

            if (!approxEqual(test[i], expected[i], tol)) {
                fail("Expected " + Arrays.toString(expected) + " but was " + Arrays.toString(test));
            }
        }

    }

    private boolean approxEqual(double a, double b, double tol) {
        return Math.abs(a - b) <= tol;
    }

    @Test
    public void revolvedSurfaceTessellate() {
        Array base = Array.from(new Number[]{0, 0, 0});
        Array axis = Array.from(new Number[]{0, 0, 1});
        double angle = Math.PI;
        Line profile = new Line(Array.from(new Number[]{1, 0, 10}), Array.from(new Number[]{10, 0, 1}));
        RevolvedSurface srf = new RevolvedSurface(profile, base, axis, angle);

        MeshData p = srf.tessellate(null);

        assertTrue(p.uvs.length > 10);
        assertTrue(p.points.length > 10);
        assertTrue(p.faces.length > 10);
        assertTrue(p.normals.length > 10);

        for (Array e : p.points.asList()) {
            AssertVerb.assertEquals(e.length, 3);
        }
        for (Array e : p.uvs.asList()) {
            AssertVerb.assertEquals(e.length, 2);
        }
        for (Array e : p.faces.asList()) {
            AssertVerb.assertEquals(e.length, 3);
        }
        for (Array e : p.normals.asList()) {
            AssertVerb.assertEquals(e.length, 3);
        }
    }

    @Test
    public void circleDerivatives() {
        Circle c = new Circle(Array.from(
                new Number[]{0, 0, 0}),
                Array.from(new Number[]{1, 0, 0}),
                Array.from(new Number[]{0, 1, 0}),
                5);

        Array<Array<Number>> p = c.derivatives(0.5, 1);

        AssertVerb.assertEquals(p.get(0).get(0), -5);
        AssertVerb.assertEquals(p.get(0).get(1), 0);
        AssertVerb.assertEquals(p.get(0).get(2), 0);

        p.set(1, Vec.div(p.get(1), Vec.norm(p.get(1))));

        AssertVerb.assertEquals(p.get(1).get(0), 0);
        AssertVerb.assertEquals(p.get(1).get(1), -1);
        AssertVerb.assertEquals(p.get(1).get(2), 0);
    }

    @Test
    public void ellipsePoint() {
        Ellipse c = new Ellipse(Array.from(
                new Number[]{0, 0, 0}),
                Array.from(new Number[]{5, 0, 0}),
                Array.from(new Number[]{0, 10, 0}));

        Array<Number> p = c.point(0.5);

        AssertVerb.assertEquals(p.get(0), -5);
        AssertVerb.assertEquals(p.get(1), 0);
        AssertVerb.assertEquals(p.get(2), 0);

        p = c.point(0.25);

        AssertVerb.assertEquals(p.get(0), 0);
        AssertVerb.assertEquals(p.get(1), 10);
        AssertVerb.assertEquals(p.get(2), 0);
    }

    @Test
    public void nurbsCurve_lengthAtParam() {
        NurbsCurve crv = createTestCurve();

        double res = crv.lengthAtParam(1);
        assertEquals(4, res, 1e-3);
    }

    @Test
    public void nurbsCurve_derivatives() {
        NurbsCurve crv = createTestCurve();

        Array<Array<Number>> p = crv.derivatives(0.5, 1);
        vecShouldBe(new double[]{2, 0, 0}, p.get(0).asDouble(), 1e-3);
        vecShouldBe(new double[]{3, 0, 0}, p.get(1).asDouble(), 1e-3);
    }

    @Test
    public void nurbsCurve_tangent() {
        NurbsCurve crv = createTestCurve();

        var p = crv.tangent(0.5);
        vecShouldBe(new double[]{3, 0, 0}, p.asDouble(), 1e-3);
    }

    private NurbsCurve createTestCurve() {
        int degree = 3;
        Array<Number> knots = new Array<>(new Double[]{0., 0., 0., 0., 0.5, 1., 1., 1., 1.});

        Array<Array<Number>> controlPoints = new Array<>(new Array[]{
                new Array<>(new Double[]{0., 0., 0.}),
                new Array<>(new Double[]{1., 0., 0.}),
                new Array<>(new Double[]{2., 0., 0.}),
                new Array<>(new Double[]{3., 0., 0.}),
                new Array<>(new Double[]{4., 0., 0.})});
        Array<Number> weights = new Array<>(new Integer[]{1, 1, 1, 1, 1});

        return NurbsCurve.byKnotsControlPointsWeights(degree, knots, controlPoints, weights);
    }

    @Test
    public void nurbsCurve_paramAtLength() {
        NurbsCurve crv = createTestCurve();

        var res = crv.paramAtLength(2., Constants.EPSILON);
        var p = crv.point(res);
        vecShouldBe(new double[]{2, 0, 0}, p.asDouble(), 1e-3);

    }

    @Test
    public void nurbsCurve_divideByEqualArcLength() {
        NurbsCurve crv = createTestCurve();

        int divs = 10;
        double d = 4. / divs;

        var res = crv.divideByEqualArcLength(divs);

        var tol = 1e-3;

        var s = 0.;

        for (int i = 0; i < res.length; i++) {
            var u = res.get(i);
            var pt = crv.point(u.u);

            assertEquals(u.len, s, tol);
            assertEquals(pt.get(0).doubleValue(), s, tol);
            s += d;
        }

    }

    @Test
    public void nurbsCurve_closestParam() {
        int degree = 3;
        Array<Number> knots = new Array<>(new Double[]{0., 0., 0., 0., 0.5, 1., 1., 1., 1.});

        Array<Array<Number>> controlPoints = new Array<>(new Array[]{
                new Array<>(new Double[]{0., 0., 0.}),
                new Array<>(new Double[]{1., 0., 0.}),
                new Array<>(new Double[]{2., 0., 0.}),
                new Array<>(new Double[]{3., 0., 0.}),
                new Array<>(new Double[]{4., 0., 0.})});
        Array<Number> weights = new Array<>(new Double[]{1., 1., 1., 1., 1.});

        NurbsCurve crv = verb.geom.NurbsCurve.byKnotsControlPointsWeights(degree, knots, controlPoints, weights);
        Array<Number> pt = new Array<>(new Double[]{1., 0., 0.});
        var res = crv.closestParam(pt);
        var p = crv.point(res);
        vecShouldBe(new double[]{1, 0, 0}, p.asDouble(), 1e-3);
    }

    @Test
    public void nurbsCurve_split() {
        int degree = 3;
        Array<Number> knots = new Array<>(new Double[]{0., 0., 0., 0., 1., 2., 3., 4., 5., 5., 5., 5.});

        var controlPoints = new Array<Array<Number>>();
        var weights = new Array<Number>();
        for (var i = 0; i < 8; i++) {
            weights.push(1);
            controlPoints.push(new Array<>(new Double[]{(double) i, 0., 0.}));
        }

        var crv = verb.geom.NurbsCurve.byKnotsControlPointsWeights(degree, knots, controlPoints, weights);

        var res = crv.split(2.5);
        check(2.5, res);
    }

    private void check(double u, Array<NurbsCurve> res) {
        var crv0 = res.get(0);
        var crv1 = res.get(1);

        // a point evaluated on each curve is the same
        var p0 = crv0.point(crv0.domain().max.doubleValue());
        var p1 = crv1.point(crv1.domain().min.doubleValue());

        assertEquals(p0.get(0).doubleValue(), p1.get(0).doubleValue(), Constants.TOLERANCE);
        assertEquals(p0.get(1).doubleValue(), p1.get(1).doubleValue(), Constants.TOLERANCE);
        assertEquals(p0.get(2).doubleValue(), p1.get(2).doubleValue(), Constants.TOLERANCE);
    }

    @Test
    public void nurbsSurface_point() {
        NurbsSurface surface = createTestSurface();

        vecShouldBe(new double[]{15, -15, 0}, surface.point(0.5, 0.5).asDouble(), Constants.TOLERANCE);
    }

    @Test
    public void nurbsSurface_normal() {
        NurbsSurface surface = createTestSurface();

        vecShouldBe(new double[]{0, 0, 900}, surface.normal(0.5, 0.5).asDouble(), Constants.TOLERANCE);
    }

    @Test
    public void nurbsSurface_derivatives() {
        NurbsSurface surface = createTestSurface();

        var d = surface.derivatives(0.5, 0.5, 1);

        vecShouldBe(new double[]{15, -15, 0}, d.get(0).get(0).asDouble(), Constants.TOLERANCE);
        vecShouldBe(new double[]{0, -30, 0}, d.get(1).get(0).asDouble(), Constants.TOLERANCE);
        vecShouldBe(new double[]{30, 0, 0}, d.get(0).get(1).asDouble(), Constants.TOLERANCE);
    }

    @Test
    public void nurbsSurface_closestParam() {
        NurbsSurface surface = createTestSurface();

        var d = surface.closestParam(new Array<>(new Double[]{15., -15., 1.}));
        vecShouldBe(new double[]{0.5, 0.5}, d.asDouble(), Constants.TOLERANCE);
    }

    @Test
    public void nurbsSurface_split() {
        NurbsSurface surface = createTestSurface();

        var d = surface.split( 0.5, true );

        assertEquals(d.get(0).domainV().min.doubleValue(), 0., Constants.TOLERANCE);
        assertEquals(d.get(0).domainV().max.doubleValue(), 0.5, Constants.TOLERANCE);

        assertEquals(d.get(0).domainU().min.doubleValue(), 0., Constants.TOLERANCE);
        assertEquals(d.get(0).domainU().max.doubleValue(), 1., Constants.TOLERANCE);

        assertEquals(d.get(1).domainV().min.doubleValue(), 0.5, Constants.TOLERANCE);
        assertEquals(d.get(1).domainV().max.doubleValue(), 1.0, Constants.TOLERANCE);

        assertEquals(d.get(1).domainU().min.doubleValue(), 0., Constants.TOLERANCE);
        assertEquals(d.get(1).domainU().max.doubleValue(), 1.0, Constants.TOLERANCE);

        d = surface.split( 0.5, false );
        assertEquals(d.get(0).domainV().min.doubleValue(), 0., Constants.TOLERANCE);
        assertEquals(d.get(0).domainV().max.doubleValue(), 1., Constants.TOLERANCE);

        assertEquals(d.get(0).domainU().min.doubleValue(), 0., Constants.TOLERANCE);
        assertEquals(d.get(0).domainU().max.doubleValue(), 0.5, Constants.TOLERANCE);

        assertEquals(d.get(1).domainV().min.doubleValue(), 0., Constants.TOLERANCE);
        assertEquals(d.get(1).domainV().max.doubleValue(), 1.0, Constants.TOLERANCE);

        assertEquals(d.get(1).domainU().min.doubleValue(), 0.5, Constants.TOLERANCE);
        assertEquals(d.get(1).domainU().max.doubleValue(), 1.0, Constants.TOLERANCE);
    }

    @Test
    public void cylindricalSurface_surfaceAndSurfaceIntersection() {
        Array<Number> axis = new Array<>(new Double[]{0.,0.,1.});
        Array<Number> xaxis = new Array<>(new Double[]{1.,0.,0.});
        Array<Number> base = new Array<>(new Double[]{0.,0.,0.});
        Array<Number> base1 = new Array<>(new Double[]{0.,1.,0.});
        double height = 1000;
        double radius = 40;

        CylindricalSurface surface0 = new CylindricalSurface(axis, xaxis, base, height, 2 * radius);
        CylindricalSurface surface1 = new CylindricalSurface(xaxis, axis, base1, height, radius);

        Array<NurbsCurveData> intersectingCurves = Intersect.surfaces(surface0._data, surface1._data, Constants.EPSILON);
        assertEquals(1, intersectingCurves.length);

        NurbsCurve curve = new NurbsCurve(intersectingCurves.get(0));

        double min = curve.domain().min.doubleValue();
        double max = curve.domain().max.doubleValue();

        double[] ptMin = curve.point(min).asDouble();
        double[] ptMax = curve.point(max).asDouble();

        vecShouldBe(ptMin, new double[]{69.84983894, -39, 0.0}, Constants.TOLERANCE);
        vecShouldBe(ptMax, new double[]{68.69497798, 41, 0.0}, Constants.TOLERANCE);
    }

    @Test
    public void nurbsSurface_closestPoint() {
        NurbsSurface surface = createTestSurface();

        var d = surface.closestPoint(new Array<>(new Double[]{15., -15., 1.}));
        vecShouldBe(new double[]{15., -15., 0.}, d.asDouble(), Constants.TOLERANCE);
    }

    private NurbsSurface createTestSurface() {
        int degreeU = 3;
        int degreeV = 3;
        Array<Number> knotsU = new Array<>(new Double[]{0., 0., 0., 0., 1., 1., 1., 1.});
        Array<Number> knotsV = new Array<>(new Double[]{0., 0., 0., 0., 1., 1., 1., 1.});
        Array<Array<Array<Number>>> controlPoints = Array.from(
                new Double[][][]{
                        new Double[][]{new Double[]{0., 0., 0.}, new Double[]{10., 0., 0.}, new Double[]{20., 0., 0.}, new Double[]{30., 0., 0.}},
                        new Double[][]{new Double[]{0., -10., 0.}, new Double[]{10., -10., 0.}, new Double[]{20., -10., 0.}, new Double[]{30., -10., 0.}},
                        new Double[][]{new Double[]{0., -20., 0.}, new Double[]{10., -20., 0.}, new Double[]{20., -20., 0.}, new Double[]{30., -20., 0.}},
                        new Double[][]{new Double[]{0., -30., 0.}, new Double[]{10., -30., 0.}, new Double[]{20., -30., 0.}, new Double[]{30., -30., 0.}},
                });
        Array<Array<Number>> weights = Array.from(new Double[][]{
                new Double[]{1., 1., 1., 1.},
                new Double[]{1., 1., 1., 1.},
                new Double[]{1., 1., 1., 1.},
                new Double[]{1., 1., 1., 1.}});

        return NurbsSurface.byKnotsControlPointsWeights(degreeU, degreeV, knotsU, knotsV, controlPoints, weights);
    }

}