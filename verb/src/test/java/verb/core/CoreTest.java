package verb.core;

import haxe.lang.Function;
import haxe.root.Array;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static verb.AssertVerb.assertEquals;
import verb.eval.Intersect;
import verb.geom.BezierCurve;

/**
 *
 * @author Matthew Pyne
 */
public class CoreTest {

    public CoreTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void intersectCurveTest() {
        Array<Array<Number>> points = new Array(new Array[]{});
        points.push(new Array(new Number[]{0, 0, 0}));
        points.push(new Array(new Number[]{0.5, 0.1, 0}));
        points.push(new Array(new Number[]{2, 0, 0}));
        BezierCurve curve1 = new BezierCurve(points, null);
        Array<Array<Number>> points2 = new Array(new Array[]{});
        points2.push(new Array(new Number[]{0.5, 0.5, 0}));
        points2.push(new Array(new Number[]{0.7, 0, 0}));
        points2.push(new Array(new Number[]{0.5, -1.5, 0}));
        BezierCurve curve2 = new BezierCurve(points2, null);

        Array<CurveCurveIntersection> res = verb.geom.Intersect.curves(curve1, curve2, Constants.TOLERANCE);
        assertEquals(res.length, 1);

        assertTrue(Vec.dist(res.get(0).point0, res.get(0).point1) < Constants.TOLERANCE);
    }

}
