package verb.eval;

import haxe.root.Array;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import verb.core.NurbsCurveData;
import haxe.lang.Runtime;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import static verb.AssertVerb.assertEquals;
import verb.core.Constants;
import verb.core.CurveCurveIntersection;
import verb.core.CurveSurfaceIntersection;
import verb.core.NurbsSurfaceData;
import verb.core.Vec;
import verb.geom.Circle;
import verb.geom.CylindricalSurface;
import verb.geom.RevolvedSurface;

/**
 *
 * @author Matthew
 */
public class EvalTest {

    public EvalTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void EvalKnotSpaneGivenNTest() {
        System.out.println("Testing KnotSpanGivenN");
        int n = 7;
        int degree = 2;
        Array knots = new Array(new Number[]{0, 0, 0, 1, 2, 3, 4, 4, 5, 6, 5});

        assertEquals(4, Eval.knotSpanGivenN(n, degree, 2.5, knots));
        assertEquals(3, Eval.knotSpanGivenN(n, degree, 1, knots));
        assertEquals(3, Eval.knotSpanGivenN(n, degree, 1.5, knots));
        assertEquals(7, Eval.knotSpanGivenN(n, degree, 4.9, knots));
        assertEquals(7, Eval.knotSpanGivenN(n, degree, 10, knots));
        assertEquals(7, Eval.knotSpanGivenN(n, degree, 5, knots));
        assertEquals(2, Eval.knotSpanGivenN(n, degree, 0, knots));
        assertEquals(2, Eval.knotSpanGivenN(n, degree, -1, knots));
    }

    @Test
    public void knotSpanTest() {
        int degree = 2;
        Array knots = new Array(new Number[]{0, 0, 0, 1, 2, 3, 4, 4, 5, 5, 5});

        assertEquals(4, Eval.knotSpan(degree, 2.5, knots));
        assertEquals(3, Eval.knotSpan(degree, 1, knots));
        assertEquals(3, Eval.knotSpan(degree, 1.5, knots));
        assertEquals(7, Eval.knotSpan(degree, 4.9, knots));
        assertEquals(7, Eval.knotSpan(degree, 10, knots));
        assertEquals(7, Eval.knotSpan(degree, 5, knots));
        assertEquals(2, Eval.knotSpan(degree, 0, knots));
    }

    @Test
    public void basisFunctionTest() {
        int degree = 2;
        Array knots = new Array(new Number[]{0, 0, 0, 1, 2, 3, 4, 4, 5, 5, 5});

        Array N1 = Eval.basisFunctionsGivenKnotSpanIndex(4, 2.5, degree, knots);
        assertEquals(3, N1.length);
        assertEquals(0.125, N1.get(0));
        assertEquals(0.75, N1.get(1));
        assertEquals(0.125, N1.get(2));

        Array N2 = Eval.basisFunctions(2.5, degree, knots);
        assertEquals(3, N2.length);
        assertEquals(0.125, N2.get(0));
        assertEquals(0.75, N2.get(1));
        assertEquals(0.125, N2.get(2));
    }

    @Test
    public void curvePointTest() {
        int degree = 2;
        int n = 6;
        Array knots = new Array(new Number[]{0, 0, 0, 1, 2, 3, 4, 5, 5, 5});
        Array controlPoints = new Array(new Array[]{});
        controlPoints.push(new Array(new Number[]{10, 0}));
        controlPoints.push(new Array(new Number[]{20, 10}));
        controlPoints.push(new Array(new Number[]{30, 20}));
        controlPoints.push(new Array(new Number[]{40, 30}));
        controlPoints.push(new Array(new Number[]{50, 40}));
        controlPoints.push(new Array(new Number[]{60, 30}));
        controlPoints.push(new Array(new Number[]{70, 80}));
        NurbsCurveData crv = new NurbsCurveData(degree, knots, controlPoints);

        Array p = Eval.curvePointGivenN(n, crv, 2.5);
        assertEquals(p.get(0), 40.);
        assertEquals(p.get(1), 30.);

        Array p_start = Eval.curvePointGivenN(n, crv, 0);
        assertEquals(p_start.get(0), 10.);
        assertEquals(p_start.get(1), 0.);

        Array p_end = Eval.curvePointGivenN(n, crv, 5);
        assertEquals(p_end.get(0), 70.);
        assertEquals(p_end.get(1), 80.);
    }

    @Test
    public void areValidReationsTest() {
        Assert.assertFalse(Eval.areValidRelations(0, 0, 0));
        Assert.assertTrue(Eval.areValidRelations(2, 2, 5));
    }

    @Test
    public void derivativeBasisFunctionGivenNITest() {
        int degree = 2;
        int n = 7;
        int span = 4;
        Array knots = new Array(new Number[]{0, 0, 0, 1, 2, 3, 4, 4, 5, 5, 5});

        Array<Array> N1 = Eval.derivativeBasisFunctionsGivenNI(span, 2.5, degree, n, knots);
        // weights
        assertEquals(0.125, N1.get(0).get(0));
        assertEquals(0.75, N1.get(0).get(1));
        assertEquals(0.125, N1.get(0).get(2));

        // derivatives
        assertEquals(-0.5, N1.get(1).get(0));
        assertEquals(1, N1.get(2).get(0));
        assertEquals(0, N1.get(1).get(1));
        assertEquals(-2, N1.get(2).get(1));
        assertEquals(0.5, N1.get(1).get(2));
        assertEquals(1, N1.get(2).get(2));

        // length
        assertEquals(n + 1, N1.length);
        assertEquals(degree + 1, N1.get(0).length);
    }

    @Test
    public void curveDerivativesTest() {
        int degree = 3;
        double u = 0.;
        Array knots = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array<Array<Number>> controlPoints = new Array(new Array[]{});
        controlPoints.push(new Array(new Number[]{10, 0}));
        controlPoints.push(new Array(new Number[]{20, 10}));
        controlPoints.push(new Array(new Number[]{30, 20}));
        controlPoints.push(new Array(new Number[]{50, 50}));
        int num_derivs = 2;
        NurbsCurveData crv = new NurbsCurveData(degree, knots, controlPoints);

        Array<Array<Number>> p = Eval.curveDerivatives(crv, u, num_derivs);

        assertEquals(p.get(0).get(0), 10);
        assertEquals(p.get(0).get(1), 0);
        assertEquals(Runtime.toDouble(p.get(1).get(0)) / Runtime.toDouble(p.get(1).get(1)), 1);
    }

    @Test
    public void surfacePointGivenNMTest() {
        int degreeU = 3;
        int degreeV = 3;
        Array knotsU = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array knotsV = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array<Array<Array<Number>>> controlPoints = new Array(new Array[]{});
        Array<Array<Number>> cp0 = new Array(new Array[]{});
        cp0.push(new Array(new Number[]{0, 0, 50}));
        cp0.push(new Array(new Number[]{10, 0, 0}));
        cp0.push(new Array(new Number[]{20, 0, 0}));
        cp0.push(new Array(new Number[]{30, 0, 0}));
        controlPoints.push(cp0);
        Array<Array<Number>> cp1 = new Array(new Array[]{});
        cp1.push(new Array(new Number[]{0, -10, 0}));
        cp1.push(new Array(new Number[]{10, -10, 10}));
        cp1.push(new Array(new Number[]{20, -10, 10}));
        cp1.push(new Array(new Number[]{30, -10, 0}));
        controlPoints.push(cp1);
        Array<Array<Number>> cp2 = new Array(new Array[]{});
        cp2.push(new Array(new Number[]{0, -20, 0}));
        cp2.push(new Array(new Number[]{10, -20, 10}));
        cp2.push(new Array(new Number[]{20, -20, 10}));
        cp2.push(new Array(new Number[]{30, -20, 0}));
        controlPoints.push(cp2);
        Array<Array<Number>> cp3 = new Array(new Array[]{});
        cp3.push(new Array(new Number[]{0, -30, 0}));
        cp3.push(new Array(new Number[]{10, -30, 0}));
        cp3.push(new Array(new Number[]{20, -30, 0}));
        cp3.push(new Array(new Number[]{30, -30, 0}));
        controlPoints.push(cp3);

        NurbsSurfaceData surface = new NurbsSurfaceData(degreeU, degreeV, knotsU, knotsV, controlPoints);
        int n = 3;
        int m = 3;

        Array p = Eval.surfacePointGivenNM(n, m, surface, 0, 0);

        assertEquals(p.get(0), 0);
        assertEquals(p.get(1), 0);
        assertEquals(p.get(2), 50);

        p = Eval.surfacePointGivenNM(n, m, surface, 1, 1);

        assertEquals(p.get(0), 30);
        assertEquals(p.get(1), -30);
        assertEquals(p.get(2), 0);
    }

    @Test
    public void surfacePointTest() {
        int degreeU = 3;
        int degreeV = 3;
        Array knotsU = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array knotsV = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array<Array<Array<Number>>> controlPoints = new Array(new Array[]{});
        Array<Array<Number>> cp0 = new Array(new Array[]{});
        cp0.push(new Array(new Number[]{0, 0, 50}));
        cp0.push(new Array(new Number[]{10, 0, 0}));
        cp0.push(new Array(new Number[]{20, 0, 0}));
        cp0.push(new Array(new Number[]{30, 0, 0}));
        controlPoints.push(cp0);
        Array<Array<Number>> cp1 = new Array(new Array[]{});
        cp1.push(new Array(new Number[]{0, -10, 0}));
        cp1.push(new Array(new Number[]{10, -10, 10}));
        cp1.push(new Array(new Number[]{20, -10, 10}));
        cp1.push(new Array(new Number[]{30, -10, 0}));
        controlPoints.push(cp1);
        Array<Array<Number>> cp2 = new Array(new Array[]{});
        cp2.push(new Array(new Number[]{0, -20, 0}));
        cp2.push(new Array(new Number[]{10, -20, 10}));
        cp2.push(new Array(new Number[]{20, -20, 10}));
        cp2.push(new Array(new Number[]{30, -20, 0}));
        controlPoints.push(cp2);
        Array<Array<Number>> cp3 = new Array(new Array[]{});
        cp3.push(new Array(new Number[]{0, -30, 0}));
        cp3.push(new Array(new Number[]{10, -30, 0}));
        cp3.push(new Array(new Number[]{20, -30, 0}));
        cp3.push(new Array(new Number[]{30, -30, 0}));
        controlPoints.push(cp3);

        NurbsSurfaceData surface = new NurbsSurfaceData(degreeU, degreeV, knotsU, knotsV, controlPoints);

        Array p = Eval.surfacePoint(surface, 0, 0);

        assertEquals(p.get(0), 0);
        assertEquals(p.get(1), 0);
        assertEquals(p.get(2), 50);

        p = Eval.surfacePoint(surface, 1, 1);

        assertEquals(p.get(0), 30);
        assertEquals(p.get(1), -30);
        assertEquals(p.get(2), 0);
    }

    @Test
    public void surfacePoint2Test() {
        int degreeU = 1;
        int degreeV = 3;
        Array knotsU = new Array(new Number[]{0, 0, 1, 1});
        Array knotsV = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array<Array<Array<Number>>> controlPoints = new Array(new Array[]{});
        Array<Array<Number>> cp0 = new Array(new Array[]{});
        cp0.push(new Array(new Number[]{0, 0, 50}));
        cp0.push(new Array(new Number[]{10, 0, 0}));
        cp0.push(new Array(new Number[]{20, 0, 0}));
        cp0.push(new Array(new Number[]{30, 0, 0}));
        controlPoints.push(cp0);
        Array<Array<Number>> cp1 = new Array(new Array[]{});
        cp1.push(new Array(new Number[]{0, -10, 0}));
        cp1.push(new Array(new Number[]{10, -10, 10}));
        cp1.push(new Array(new Number[]{20, -10, 10}));
        cp1.push(new Array(new Number[]{30, -10, 0}));
        controlPoints.push(cp1);

        NurbsSurfaceData surface = new NurbsSurfaceData(degreeU, degreeV, knotsU, knotsV, controlPoints);

        Array p = Eval.surfacePoint(surface, 0, 0);

        assertEquals(p.get(0), 0);
        assertEquals(p.get(1), 0);
        assertEquals(p.get(2), 50);
    }

    @Test
    public void surfaceDerivativesGivenNMTest() {
        int degreeU = 3;
        int degreeV = 3;
        Array knotsU = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array knotsV = new Array(new Number[]{0, 0, 0, 0, 1, 1, 1, 1});
        Array<Array<Array<Number>>> controlPoints = new Array(new Array[]{});
        Array<Array<Number>> cp0 = new Array(new Array[]{});
        cp0.push(new Array(new Number[]{0, 0, 0}));
        cp0.push(new Array(new Number[]{10, 10, 0}));
        cp0.push(new Array(new Number[]{20, 10, 0}));
        cp0.push(new Array(new Number[]{30, 0, 0}));
        controlPoints.push(cp0);
        Array<Array<Number>> cp1 = new Array(new Array[]{});
        cp1.push(new Array(new Number[]{0, -10, 0}));
        cp1.push(new Array(new Number[]{10, -10, 10}));
        cp1.push(new Array(new Number[]{20, -10, 10}));
        cp1.push(new Array(new Number[]{30, -10, 0}));
        controlPoints.push(cp1);
        Array<Array<Number>> cp2 = new Array(new Array[]{});
        cp2.push(new Array(new Number[]{0, -20, 0}));
        cp2.push(new Array(new Number[]{10, -20, 10}));
        cp2.push(new Array(new Number[]{20, -20, 10}));
        cp2.push(new Array(new Number[]{30, -20, 0}));
        controlPoints.push(cp2);
        Array<Array<Number>> cp3 = new Array(new Array[]{});
        cp3.push(new Array(new Number[]{0, -30, 0}));
        cp3.push(new Array(new Number[]{10, -30, 0}));
        cp3.push(new Array(new Number[]{20, -30, 0}));
        cp3.push(new Array(new Number[]{30, -30, 0}));
        controlPoints.push(cp3);

        NurbsSurfaceData surface = new NurbsSurfaceData(degreeU, degreeV, knotsU, knotsV, controlPoints);
        int n = 3;
        int m = 3;
        int num_derivatives = 1;

        Array<Array<Array<Number>>> p = Eval.surfaceDerivativesGivenNM(n, m, surface, 0, 0, num_derivatives);

        // 0th derivative with repect to u & v
        assertEquals(p.get(0).get(0).get(0), 0);
        assertEquals(p.get(0).get(0).get(1), 0);
        assertEquals(p.get(0).get(0).get(2), 0);

        // d/du
        assertEquals(Runtime.toDouble(p.get(0).get(1).get(0)) / Runtime.toDouble(p.get(0).get(1).get(0)), 1);
        assertEquals(p.get(0).get(1).get(2), 0);

        // d/dv
        assertEquals(p.get(1).get(0).get(0), 0);
        assertEquals(p.get(1).get(0).get(1), -30);
        assertEquals(p.get(1).get(0).get(2), 0);

        // dd/dudv
        assertEquals(p.get(1).get(1).get(0), 0);
        assertEquals(p.get(1).get(1).get(1), 0);
        assertEquals(p.get(1).get(1).get(2), 0);
    }

    @Test
    public void intersectCurves0() {
        int degree1 = 1;
        Array knots1 = Array.from(new Number[]{0, 0, 1, 1});
        Array<Array<Number>> controlPoints1 = Array.from(new Number[][]{{0, 0, 0, 1}, {2, 0, 0, 1}});
        NurbsCurveData curve1 = new NurbsCurveData(degree1, knots1, controlPoints1);

        int degree2 = 1;
        Array knots2 = Array.from(new Number[]{0, 0, 1, 1});
        Array<Array<Number>> controlPoints2 = Array.from(new Number[][]{{0.5, 0.5, 0, 1}, {0.5, -1.5, 0, 1}});
        NurbsCurveData curve2 = new NurbsCurveData(degree2, knots2, controlPoints2);

        Array<CurveCurveIntersection> res = Intersect.curves(curve1, curve2, Constants.TOLERANCE);

        assertEquals(res.length, 1);

        assertEquals(res.get(0).u0, 0.25);
        assertEquals(res.get(0).u1, 0.25);
    }

    @Test
    public void intersectCurves1() {
        int degree1 = 1;
        Array knots1 = Array.from(new Number[]{0, 0, 1, 1});
        Array<Array<Number>> controlPoints1 = Array.from(new Number[][]{{0, 0, 0, 1}, {2, 0, 0, 1}});
        NurbsCurveData curve1 = new NurbsCurveData(degree1, knots1, controlPoints1);

        int degree2 = 2;
        Array knots2 = Array.from(new Number[]{0, 0, 0, 1, 1, 1});
        Array<Array<Number>> controlPoints2 = Array.from(new Number[][]{{0.5, 0.5, 0, 1}, {0.7, 0, 0, 1}, {0.5, -1.5, 0, 1}});
        NurbsCurveData curve2 = new NurbsCurveData(degree2, knots2, controlPoints2);

        Array<CurveCurveIntersection> res = Intersect.curves(curve1, curve2, Constants.TOLERANCE);

        assertEquals(res.length, 1);

        assertEquals(res.get(0).u0, 0.2964101616038012);
        assertEquals(res.get(0).u1, 0.3660254038069307);
    }

    @Test
    public void intersectCurves2() {
        int degree1 = 2;
        Array knots1 = Array.from(new Number[]{0, 0, 0, 1, 1, 1});
        Array<Array<Number>> controlPoints1 = Array.from(new Number[][]{{0, 0, 0, 1}, {0.5, 0.1, 0, 1}, {2, 0, 0, 1}});
        NurbsCurveData curve1 = new NurbsCurveData(degree1, knots1, controlPoints1);

        int degree2 = 2;
        Array knots2 = Array.from(new Number[]{0, 0, 0, 1, 1, 1});
        Array<Array<Number>> controlPoints2 = Array.from(new Number[][]{{0.5, 0.5, 0, 1}, {0.7, 0, 0, 1}, {0.5, -1.5, 0, 1}});
        NurbsCurveData curve2 = new NurbsCurveData(degree2, knots2, controlPoints2);

        Array<CurveCurveIntersection> res = Intersect.curves(curve1, curve2, Constants.TOLERANCE);

        assertEquals(res.length, 1);

        assertTrue(Vec.dist(res.get(0).point0, res.get(0).point1) < Constants.TOLERANCE);
    }

    @Test
    public void intersectCurveAndSurface0() {
        // build planar surface in the xy plane
        Array<Array<Array<Number>>> homo_controlPoints_srf = Array.from(new Number[][][]{
            {{0, 0, 0, 1}, {20, 0, 0, 1}},
            {{0, 10, 0, 1}, {20, 10, 0, 1}}
        });
        int degreeU = 1;
        int degreeV = 1;
        Array<Number> knotsU = Array.from(new Number[]{0, 0, 1, 1});
        Array<Number> knotsV = Array.from(new Number[]{0, 0, 1, 1});
        NurbsSurfaceData surface = new NurbsSurfaceData(degreeU, degreeV, knotsU, knotsV, homo_controlPoints_srf);

        // line from [5,5,5] to [5,5,-5]
        int degree_crv = 1;
        Array<Number> knots_crv = new Array(new Number[]{0, 0, 1, 1});
        Array<Array<Number>> homo_controlPoints_crv = Array.from(new Number[][]{
            {5.2, 5.2, 5, 1}, {5.2, 5.2, -10, 1}
        });
        NurbsCurveData curve = new NurbsCurveData(degree_crv, knots_crv, homo_controlPoints_crv);

        Array<CurveSurfaceIntersection> res = Intersect.curveAndSurface(curve, surface, Constants.TOLERANCE, null, null);
        assertEquals(res.length, 1);

        assertEquals(1. / 3., res.get(0).u);
        assertEquals(0.52, res.get(0).uv.get(0));
        assertEquals(0.26, res.get(0).uv.get(1));
    }

    @Test
    public void intersectTorusCylindricalSurface() {
        Circle profile = new Circle(Array.from(new Number[]{5, 0, 0}), Array.from(new Number[]{1, 0, 0}), Array.from(new Number[]{0, 0, 1}), 2);

        Array base = Array.from(new Number[]{0, 0, 0});
        Array axis = Array.from(new Number[]{0, 0, 1});
        double angle = 2 * Math.PI;
        RevolvedSurface srf1 = new RevolvedSurface(profile, base, axis, angle);

        Array axis2 = Array.from(new Number[]{-1, 0, 0});
        Array xaxis = Array.from(new Number[]{0, 0, 1});
        Array base2 = Array.from(new Number[]{8, 0, 0});
        double height = 16;
        double radius = 2;

        CylindricalSurface srf2 = new CylindricalSurface(axis2, xaxis, base2, height, radius);

        Array res = Intersect.meshes(srf1.tessellate(null), srf2.tessellate(null), null, null);

        assertEquals(res.length, 8);
    }

    @Test
    public void divideCurveSplitTest() {
        divideCurveSplitTest(0.5);
        divideCurveSplitTest(3.5);
        divideCurveSplitTest(3);
        divideCurveSplitTest(2);
        divideCurveSplitTest(1);
        divideCurveSplitTest(4);
    }

    private void divideCurveSplitTest(double u) {
        int degree = 3;
        Array knots = Array.from(new Number[]{0, 0, 0, 0, 1, 2, 3, 4, 5, 5, 5, 5});
        Array<Array<Number>> controlPoints = new Array();

        for (int i = 0; i < 8; i++) {
            controlPoints.push(Array.from(new Number[]{i, 0, 0, 1}));
        }

        NurbsCurveData crv = new NurbsCurveData(degree, knots, controlPoints);
        Array<NurbsCurveData> after = Divide.curveSplit(crv, u);

        for (int i = 0; i < degree + 1; i++) {
            int d = after.get(0).knots.length - (degree + 1);
            assertEquals(after.get(0).knots.get(d + i), u);
        }

        for (int i = 0; i < degree + 1; i++) {
            int d = 0;
            assertEquals(after.get(1).knots.get(d + i), u);
        }

        // a point evaluated on each curve is the same
        Array p0 = Eval.curvePoint(after.get(0), Runtime.toDouble(after.get(0).knots.get(after.get(0).knots.length - 1)));
        Array p1 = Eval.curvePoint(after.get(1), Runtime.toDouble(after.get(1).knots.get(0)));

        assertEquals(p0.get(0), p1.get(0));
        assertEquals(p0.get(1), p1.get(1));
        assertEquals(p0.get(2), p1.get(2));
        
    }
}
