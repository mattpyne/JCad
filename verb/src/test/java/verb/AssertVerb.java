package verb;


import org.junit.Assert;
import verb.core.Constants;

/**
 *
 * @author Matthew
 */
public class AssertVerb {

    public static void assertEquals(Object actual, Object expected) {
        Assert.assertEquals(haxe.lang.Runtime.toDouble(expected), haxe.lang.Runtime.toDouble(actual), Constants.TOLERANCE);
    }
}
