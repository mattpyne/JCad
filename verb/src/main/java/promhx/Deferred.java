package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Deferred<T> extends promhx.base.AsyncBase<T> {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     */
    public static <T_c> void __hx_ctor_promhx_Deferred(promhx.Deferred<T_c> __hx_this) {
        promhx.base.AsyncBase.__hx_ctor_promhx_base_AsyncBase(__hx_this, null);
    }

    /**
     *
     * @param empty
     */
    public Deferred(haxe.lang.EmptyObject empty) {
        super(haxe.lang.EmptyObject.EMPTY);
    }

    /**
     *
     */
    public Deferred() {
        super(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY)));
        promhx.Deferred.__hx_ctor_promhx_Deferred((this));
    }

    /**
     *
     * @param val
     */
    public void resolve(T val) {
        this.handleResolve(val);
    }

    /**
     *
     * @param e
     */
    public void throwError(java.lang.Object e) {
        this.handleError(e);
    }

    /**
     *
     * @return
     */
    public promhx.Promise<T> promise() {
        return new promhx.Promise<>((this));
    }

    /**
     *
     * @return
     */
    public promhx.Stream<T> stream() {
        return new promhx.Stream<>((this));
    }

    /**
     *
     * @return
     */
    public promhx.PublicStream<T> publicStream() {
        return new promhx.PublicStream<>((this));
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 534_297_417: {
                    if (field.equals("publicStream")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "publicStream"));
                    }

                    break;
                }

                case 1_097_368_044: {
                    if (field.equals("resolve")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "resolve"));
                    }

                    break;
                }

                case -891_990_144: {
                    if (field.equals("stream")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "stream"));
                    }

                    break;
                }

                case -297_755_966: {
                    if (field.equals("throwError")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "throwError"));
                    }

                    break;
                }

                case -309_216_997: {
                    if (field.equals("promise")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "promise"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 534_297_417: {
                    if (field.equals("publicStream")) {
                        __temp_executeDef1 = false;
                        return this.publicStream();
                    }

                    break;
                }

                case 1_097_368_044: {
                    if (field.equals("resolve")) {
                        __temp_executeDef1 = false;
                        this.resolve(((T) (dynargs.get(0))));
                    }

                    break;
                }

                case -891_990_144: {
                    if (field.equals("stream")) {
                        __temp_executeDef1 = false;
                        return this.stream();
                    }

                    break;
                }

                case -297_755_966: {
                    if (field.equals("throwError")) {
                        __temp_executeDef1 = false;
                        this.throwError(dynargs.get(0));
                    }

                    break;
                }

                case -309_216_997: {
                    if (field.equals("promise")) {
                        __temp_executeDef1 = false;
                        return this.promise();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

}
