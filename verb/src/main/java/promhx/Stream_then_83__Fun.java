package promhx;

/**
 *
 * @author Matthew
 * @param <A>
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_then_83__Fun<A, T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<A> next;

    /**
     *
     */
    public haxe.lang.Function f1;

    /**
     *
     * @param next
     * @param f1
     */
    public Stream_then_83__Fun(promhx.base.AsyncBase<A> next, haxe.lang.Function f1) {
        super(1, 0);
        this.next = next;
        this.f1 = f1;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T) (((java.lang.Object) (__fn_float1))))) : (((T) (__fn_dyn1))));
        A tmp = ((A) (this.f1.__hx_invoke1_o(0.0, x)));
        this.next.handleResolve(tmp);
        return null;
    }

}
