package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class PublicStream<T> extends promhx.Stream<T> {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param def
     */
    public static <T_c> void __hx_ctor_promhx_PublicStream(promhx.PublicStream<T_c> __hx_this, promhx.Deferred<T_c> def) {
        promhx.Stream.__hx_ctor_promhx_Stream((__hx_this), (def));
    }

    /**
     *
     * @param <T1>
     * @param val
     * @return
     */
    public static <T1> promhx.PublicStream<T1> publicstream(T1 val) {
        promhx.PublicStream<T1> ps = new promhx.PublicStream<>(((promhx.Deferred<T1>) (null)));
        ps.handleResolve(val);
        return ps;
    }

    /**
     *
     * @param empty
     */
    public PublicStream(haxe.lang.EmptyObject empty) {
        super(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY)));
    }

    /**
     *
     * @param def
     */
    public PublicStream(promhx.Deferred<T> def) {
        super(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY)));
        promhx.PublicStream.__hx_ctor_promhx_PublicStream(this, def);
    }

    /**
     *
     * @param val
     */
    public void resolve(T val) {
        this.handleResolve(val);
    }

    /**
     *
     * @param e
     */
    public void throwError(java.lang.Object e) {
        this.handleError(e);
    }

    /**
     *
     * @param val
     */
    public void update(T val) {
        this.handleResolve(val);
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -838_846_263: {
                    if (field.equals("update")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "update"));
                    }

                    break;
                }

                case 1_097_368_044: {
                    if (field.equals("resolve")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "resolve"));
                    }

                    break;
                }

                case -297_755_966: {
                    if (field.equals("throwError")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "throwError"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -838_846_263: {
                    if (field.equals("update")) {
                        __temp_executeDef1 = false;
                        this.update(((T) (dynargs.get(0))));
                    }

                    break;
                }

                case 1_097_368_044: {
                    if (field.equals("resolve")) {
                        __temp_executeDef1 = false;
                        this.resolve(((T) (dynargs.get(0))));
                    }

                    break;
                }

                case -297_755_966: {
                    if (field.equals("throwError")) {
                        __temp_executeDef1 = false;
                        this.throwError(dynargs.get(0));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

}
