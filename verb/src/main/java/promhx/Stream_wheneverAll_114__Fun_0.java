package promhx;

/**
 *
 * @author Matthew
 * @param <T2>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_wheneverAll_114__Fun_0<T2> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<haxe.root.Array<T2>> next;

    /**
     *
     */
    public java.lang.Object all;

    /**
     *
     * @param next
     * @param all
     */
    public Stream_wheneverAll_114__Fun_0(promhx.base.AsyncBase<haxe.root.Array<T2>> next, java.lang.Object all) {
        super(3, 0);
        this.next = next;
        this.all = all;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke3_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3) {
        T2 v = (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (((T2) (((java.lang.Object) (__fn_float3))))) : (((T2) (__fn_dyn3))));
        promhx.base.AsyncBase<T2> current = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (((promhx.base.AsyncBase<T2>) (((java.lang.Object) (__fn_float2))))) : (((promhx.base.AsyncBase<T2>) (__fn_dyn2))));
        haxe.root.Array<promhx.base.AsyncBase<T2>> arr = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.root.Array<promhx.base.AsyncBase<T2>>) (((java.lang.Object) (__fn_float1))))) : (((haxe.root.Array<promhx.base.AsyncBase<T2>>) (__fn_dyn1))));
        if ((arr.length == 0) || promhx.base.AsyncBase.allFulfilled(arr)) {
            haxe.root.Array<T2> _g = new haxe.root.Array<>((T2[]) (new java.lang.Object[]{}));
            {
                java.lang.Object a = haxe.lang.Runtime.callField(this.all, "iterator", null);
                while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a, "hasNext", null))) {
                    promhx.base.AsyncBase<T2> a1 = ((promhx.base.AsyncBase<T2>) (haxe.lang.Runtime.callField(a, "next", null)));
                    _g.push((((a1 == current)) ? (v) : (a1._val)));
                }
            }
            haxe.root.Array<T2> vals = _g;
            this.next.handleResolve(vals);
        }
        return null;
    }

}
