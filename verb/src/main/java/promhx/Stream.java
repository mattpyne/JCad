package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream<T> extends promhx.base.AsyncBase<T> {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param d
     */
    public static <T_c> void __hx_ctor_promhx_Stream(promhx.Stream<T_c> __hx_this, promhx.Deferred<T_c> d) {
        promhx.base.AsyncBase.__hx_ctor_promhx_base_AsyncBase((__hx_this), (d));
        __hx_this._end_promise = new promhx.Promise<>(((promhx.Deferred<haxe.ds.Option>) (null)));
    }

    /**
     *
     * @param <T1>
     * @param itb
     * @return
     */
    public static <T1> promhx.Stream<T1> foreach(java.lang.Object itb) {
        promhx.Stream<T1> s = new promhx.Stream<>(((promhx.Deferred<T1>) (null)));
        {
            java.lang.Object i = (haxe.lang.Runtime.callField(itb, "iterator", null));
            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(i, "hasNext", null))) {
                T1 i1 = ((T1) (haxe.lang.Runtime.callField(i, "next", null)));
                s.handleResolve(i1);
            }

        }

        s.end();
        return s;
    }

    /**
     *
     * @param <T2>
     * @param itb
     * @return
     */
    public static <T2> promhx.Stream<haxe.root.Array<T2>> wheneverAll(java.lang.Object itb) {
        promhx.Stream<haxe.root.Array<T2>> ret = new promhx.Stream<>(((promhx.Deferred<haxe.root.Array<T2>>) (null)));
        {
            java.lang.Object all = itb;
            promhx.base.AsyncBase<haxe.root.Array<T2>> next = ret;
            haxe.lang.Function cthen = new promhx.Stream_wheneverAll_114__Fun_0<>(next, all);
            {
                java.lang.Object a2 = (haxe.lang.Runtime.callField(all, "iterator", null));
                while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a2, "hasNext", null))) {
                    promhx.base.AsyncBase<T2> a3 = ((promhx.base.AsyncBase<T2>) (haxe.lang.Runtime.callField(a2, "next", null)));
                    haxe.root.Array<java.lang.Object> a4 = a3._update;
                    haxe.lang.Function f = cthen;
                    haxe.root.Array<promhx.base.AsyncBase<T2>> _g1 = new haxe.root.Array<>((new promhx.base.AsyncBase[]{}));
                    {
                        java.lang.Object a21 = (haxe.lang.Runtime.callField(all, "iterator", null));
                        while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a21, "hasNext", null))) {
                            promhx.base.AsyncBase<T2> a22 = ((promhx.base.AsyncBase<T2>) (haxe.lang.Runtime.callField(a21, "next", null)));
                            if ((a22 != a3)) {
                                _g1.push(a22);
                            }

                        }

                    }

                    haxe.root.Array<promhx.base.AsyncBase<T2>> a11 = _g1;
                    promhx.base.AsyncBase<T2> a23 = a3;
                    haxe.lang.Function tmp = new promhx.Stream_wheneverAll_114__Fun<>(f, a23, a11);
                    a4.push(new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (next)), tmp}, new java.lang.String[]{}, new double[]{}));
                }

            }

            if (promhx.base.AsyncBase.allFulfilled(all)) {
                haxe.root.Array<T2> _g2 = new haxe.root.Array<>(((T2[]) (new java.lang.Object[]{})));
                {
                    java.lang.Object a5 = (haxe.lang.Runtime.callField(all, "iterator", null));
                    while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a5, "hasNext", null))) {
                        promhx.base.AsyncBase<T2> a6 = ((promhx.base.AsyncBase<T2>) (haxe.lang.Runtime.callField(a5, "next", null)));
                        _g2.push(a6._val);
                    }

                }

                next.handleResolve(_g2);
            }

        }

        return ret;
    }

    /**
     *
     * @param <T3>
     * @param itb
     * @return
     */
    public static <T3> promhx.Stream<T3> concatAll(java.lang.Object itb) {
        promhx.Stream<T3> ret = new promhx.Stream<>(((promhx.Deferred<T3>) (null)));
        {
            java.lang.Object i = (haxe.lang.Runtime.callField(itb, "iterator", null));
            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(i, "hasNext", null))) {
                promhx.Stream<T3> i1 = ((promhx.Stream<T3>) (haxe.lang.Runtime.callField(i, "next", null)));
                ret.concat(i1);
            }

        }

        return ret;
    }

    /**
     *
     * @param <T4>
     * @param itb
     * @return
     */
    public static <T4> promhx.Stream<T4> mergeAll(java.lang.Object itb) {
        promhx.Stream<T4> ret = new promhx.Stream<>(((promhx.Deferred<T4>) (null)));
        {
            java.lang.Object i = (haxe.lang.Runtime.callField(itb, "iterator", null));
            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(i, "hasNext", null))) {
                promhx.Stream<T4> i1 = ((promhx.Stream<T4>) (haxe.lang.Runtime.callField(i, "next", null)));
                ret.merge(i1);
            }

        }

        return ret;
    }

    /**
     *
     * @param <A>
     * @param _val
     * @return
     */
    public static <A> promhx.Stream<A> stream(A _val) {
        promhx.Stream<A> ret = new promhx.Stream<>(((promhx.Deferred<A>) (null)));
        ret.handleResolve(_val);
        return ret;
    }

    /**
     *
     */
    public promhx.Deferred<T> deferred;

    /**
     *
     */
    public boolean _pause;

    /**
     *
     */
    public boolean _end;

    /**
     *
     */
    public promhx.Promise<haxe.ds.Option> _end_promise;

    /**
     *
     * @param empty
     */
    public Stream(haxe.lang.EmptyObject empty) {
        super(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY)));
    }

    /**
     *
     * @param d
     */
    public Stream(promhx.Deferred<T> d) {
        super(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY)));
        promhx.Stream.__hx_ctor_promhx_Stream(this, d);
    }

    /**
     *
     * @param <A>
     * @param f
     * @return
     */
    @Override
    public <A> promhx.base.AsyncBase<A> then(haxe.lang.Function f) {
        promhx.Stream<A> ret = new promhx.Stream<>(((promhx.Deferred<A>) (null)));
        {
            promhx.base.AsyncBase<A> next = ret;
            haxe.lang.Function f1 = f;
            java.lang.Object __temp_stmt3 = null;
            {
                haxe.lang.Function __temp_odecl1 = new promhx.Stream_then_83__Fun<A, T>(next, f1);
                __temp_stmt3 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (next)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
            }

            this._update.push(__temp_stmt3);
            promhx.base.AsyncBase.immediateLinkUpdate((this), (next), (f1));
        }

        java.lang.Object __temp_stmt4 = null;
        {
            haxe.lang.Function __temp_odecl2 = new promhx.Stream_then_86__Fun<A, T>(ret);
            __temp_stmt4 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret._end_promise)), __temp_odecl2}, new java.lang.String[]{}, new double[]{});
        }

        this._end_promise._update.push(__temp_stmt4);
        return ret;
    }

    /**
     *
     * @param str
     * @return
     */
    public boolean detachStream(promhx.Stream str) {
        haxe.root.Array<java.lang.Object> filtered = new haxe.root.Array<>(new java.lang.Object[]{});
        boolean removed = false;
        {
            int _g = 0;
            haxe.root.Array<java.lang.Object> _g1 = this._update;
            while ((_g < _g1.length)) {
                java.lang.Object u = _g1.get(_g);
                ++_g;
                if ((((promhx.base.AsyncBase) (haxe.lang.Runtime.getField(u, "async", true))) == str)) {
                    this._end_promise._update = this._end_promise._update.filter(new promhx.Stream_detachStream_97__Fun<T>(str));
                    removed = true;
                } else {
                    filtered.push(u);
                }

            }

        }

        this._update = filtered;
        return removed;
    }

    /**
     *
     * @return
     */
    public promhx.Promise<T> first() {
        promhx.Promise<T> s = new promhx.Promise<>(((promhx.Deferred<T>) (null)));
        this.then((new promhx.Stream_first_143__Fun<>(s)));
        return s;
    }

    /**
     *
     * @param val
     */
    @Override
    public void handleResolve(T val) {
        if ((!(this._end) && !(this._pause))) {
            this._resolve(val);
        }

    }

    /**
     *
     * @param set
     */
    public void pause(java.lang.Object set) {
        if (haxe.lang.Runtime.eq(set, null)) {
            set = !(this._pause);
        }

        this._pause = haxe.lang.Runtime.toBool(set);
    }

    /**
     *
     * @param <A>
     * @param f
     * @return
     */
    public <A> promhx.Stream<A> pipe(haxe.lang.Function f) {
        promhx.Stream<A> ret = new promhx.Stream<>(((promhx.Deferred<A>) (null)));
        promhx.base.AsyncBase<A> ret1 = ret;
        haxe.lang.Function f1 = f;
        boolean[] linked = new boolean[]{false};
        haxe.lang.Function linkf = new promhx.Stream_pipe_162__Fun<A, T>(ret1, linked, f1);
        this._update.push(new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret1)), linkf}, new java.lang.String[]{}, new double[]{}));
        if ((this._resolved && !(this._pending))) {
            try {
                linkf.__hx_invoke1_o(0.0, this._val);
            } catch (java.lang.Throwable __temp_catchallException1) {
                haxe.lang.Exceptions.setException(__temp_catchallException1);
                java.lang.Object __temp_catchall2 = __temp_catchallException1;
                if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                    __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
                }

                {
                    java.lang.Object e = __temp_catchall2;
                    ret1.handleError(e);
                }

            }

        }

        this._end_promise.then(((haxe.lang.Function) (new promhx.Stream_pipe_163__Fun<A, T>(ret))));
        return ret;
    }

    /**
     *
     * @param f
     * @return
     */
    public promhx.Stream<T> errorPipe(haxe.lang.Function f) {
        promhx.Stream<T> ret = new promhx.Stream<>(((promhx.Deferred<T>) (null)));
        this.catchError(new promhx.Stream_errorPipe_172__Fun<>(ret, f));
        this.then((new haxe.lang.Closure(ret, "_resolve")));
        this._end_promise.then((new promhx.Stream_errorPipe_178__Fun<>(ret)));
        return ret;
    }

    /**
     *
     */
    public void handleEnd() {
        if (this._pending) {
            promhx.base.EventLoop.queue.add((new haxe.lang.Closure(this, "handleEnd")));
            promhx.base.EventLoop.continueOnNextLoop();
        } else {
            if (this._end_promise._resolved) {

            } else {
                this._end = true;
                haxe.ds.Option o = ((this._resolved) ? (haxe.ds.Option.Some(this._val)) : (haxe.ds.Option.None));
                this._end_promise.handleResolve(o);
                this._update = new haxe.root.Array<>(new java.lang.Object[]{});
                this._error = new haxe.root.Array<>(new haxe.lang.Function[]{});
            }

        }

    }

    /**
     *
     * @return
     */
    public promhx.Stream<T> end() {
        {
            promhx.base.EventLoop.queue.add((new haxe.lang.Closure(this, "handleEnd")));
            promhx.base.EventLoop.continueOnNextLoop();
        }

        return this;
    }

    /**
     *
     * @param <A>
     * @param f
     * @return
     */
    public <A> promhx.Promise<A> endThen(haxe.lang.Function f) {
        return ((promhx.Promise<A>) (this._end_promise.then((f))));
    }

    /**
     *
     * @param f
     * @return
     */
    public promhx.Stream<T> filter(haxe.lang.Function f) {
        promhx.Stream<T> ret = new promhx.Stream<>(((promhx.Deferred<T>) (null)));
        java.lang.Object __temp_stmt2 = null;
        {
            haxe.lang.Function __temp_odecl1 = new promhx.Stream_filter_215__Fun<>(ret, f);
            __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
        }

        this._update.push(__temp_stmt2);
        promhx.base.AsyncBase.immediateLinkUpdate(((promhx.base.AsyncBase<T>) (this)), ((promhx.base.AsyncBase<T>) (ret)), ((haxe.lang.Function) (new promhx.Stream_filter_217__Fun<T>())));
        return ret;
    }

    /**
     *
     * @param s
     * @return
     */
    public promhx.Stream<T> concat(promhx.Stream<T> s) {
        promhx.Stream<T> ret = new promhx.Stream<>(((promhx.Deferred<T>) (null)));
        java.lang.Object __temp_stmt2 = null;
        {
            haxe.lang.Function __temp_odecl1 = (new haxe.lang.Closure(ret, "handleResolve"));
            __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
        }

        this._update.push(__temp_stmt2);
        promhx.base.AsyncBase.immediateLinkUpdate(((promhx.base.AsyncBase<T>) (this)), ((promhx.base.AsyncBase<T>) (ret)), ((haxe.lang.Function) (new promhx.Stream_concat_232__Fun<T>())));
        this._end_promise.then((new promhx.Stream_concat_233__Fun<>(s, ret)));
        return ret;
    }

    /**
     *
     * @param s
     * @return
     */
    public promhx.Stream<T> merge(promhx.Stream<T> s) {
        promhx.Stream<T> ret = new promhx.Stream<>(((promhx.Deferred<T>) (null)));
        java.lang.Object __temp_stmt3 = null;
        {
            haxe.lang.Function __temp_odecl1 = (new haxe.lang.Closure(ret, "handleResolve"));
            __temp_stmt3 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
        }

        this._update.push(__temp_stmt3);
        java.lang.Object __temp_stmt4 = null;
        {
            haxe.lang.Function __temp_odecl2 = (new haxe.lang.Closure(ret, "handleResolve"));
            __temp_stmt4 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret)), __temp_odecl2}, new java.lang.String[]{}, new double[]{});
        }

        s._update.push(__temp_stmt4);
        promhx.base.AsyncBase.immediateLinkUpdate(((promhx.base.AsyncBase<T>) (this)), ((promhx.base.AsyncBase<T>) (ret)), ((haxe.lang.Function) (new promhx.Stream_merge_258__Fun<T>())));
        promhx.base.AsyncBase.immediateLinkUpdate(((promhx.base.AsyncBase<T>) (s)), ((promhx.base.AsyncBase<T>) (ret)), ((haxe.lang.Function) (new promhx.Stream_merge_259__Fun<T>())));
        return ret;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -174_965_768: {
                    if (field.equals("_end_promise")) {
                        __temp_executeDef1 = false;
                        this._end_promise = ((promhx.Promise<haxe.ds.Option>) (value));
                        return value;
                    }

                    break;
                }

                case 647_890_911: {
                    if (field.equals("deferred")) {
                        __temp_executeDef1 = false;
                        this.deferred = ((promhx.Deferred<T>) (value));
                        return value;
                    }

                    break;
                }

                case 2_930_716: {
                    if (field.equals("_end")) {
                        __temp_executeDef1 = false;
                        this._end = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case -1_468_757_769: {
                    if (field.equals("_pause")) {
                        __temp_executeDef1 = false;
                        this._pause = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 103_785_528: {
                    if (field.equals("merge")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "merge"));
                    }

                    break;
                }

                case 647_890_911: {
                    if (field.equals("deferred")) {
                        __temp_executeDef1 = false;
                        return this.deferred;
                    }

                    break;
                }

                case -1_354_795_244: {
                    if (field.equals("concat")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "concat"));
                    }

                    break;
                }

                case -1_468_757_769: {
                    if (field.equals("_pause")) {
                        __temp_executeDef1 = false;
                        return this._pause;
                    }

                    break;
                }

                case -1_274_492_040: {
                    if (field.equals("filter")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "filter"));
                    }

                    break;
                }

                case 2_930_716: {
                    if (field.equals("_end")) {
                        __temp_executeDef1 = false;
                        return this._end;
                    }

                    break;
                }

                case -1_607_244_392: {
                    if (field.equals("endThen")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "endThen"));
                    }

                    break;
                }

                case -174_965_768: {
                    if (field.equals("_end_promise")) {
                        __temp_executeDef1 = false;
                        return this._end_promise;
                    }

                    break;
                }

                case 100_571: {
                    if (field.equals("end")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "end"));
                    }

                    break;
                }

                case 3_558_941: {
                    if (field.equals("then")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "then"));
                    }

                    break;
                }

                case 64_098_707: {
                    if (field.equals("handleEnd")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "handleEnd"));
                    }

                    break;
                }

                case -592_766_733: {
                    if (field.equals("detachStream")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "detachStream"));
                    }

                    break;
                }

                case 329_417_686: {
                    if (field.equals("errorPipe")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "errorPipe"));
                    }

                    break;
                }

                case 97_440_432: {
                    if (field.equals("first")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "first"));
                    }

                    break;
                }

                case 3_441_070: {
                    if (field.equals("pipe")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "pipe"));
                    }

                    break;
                }

                case 1_674_964_644: {
                    if (field.equals("handleResolve")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "handleResolve"));
                    }

                    break;
                }

                case 106_440_182: {
                    if (field.equals("pause")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "pause"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            int __temp_hash2 = field.hashCode();
            boolean __temp_executeDef1 = true;
            switch (__temp_hash2) {
                case 1_674_964_644:
                case 3_558_941: {
                    if (((((__temp_hash2 == 1_674_964_644) && field.equals("handleResolve"))) || field.equals("then"))) {
                        __temp_executeDef1 = false;
                        return haxe.lang.Runtime.slowCallField(this, field, dynargs);
                    }

                    break;
                }

                case -592_766_733: {
                    if (field.equals("detachStream")) {
                        __temp_executeDef1 = false;
                        return this.detachStream(((promhx.Stream) (dynargs.get(0))));
                    }

                    break;
                }

                case 103_785_528: {
                    if (field.equals("merge")) {
                        __temp_executeDef1 = false;
                        return this.merge(((promhx.Stream<T>) (dynargs.get(0))));
                    }

                    break;
                }

                case 97_440_432: {
                    if (field.equals("first")) {
                        __temp_executeDef1 = false;
                        return this.first();
                    }

                    break;
                }

                case -1_354_795_244: {
                    if (field.equals("concat")) {
                        __temp_executeDef1 = false;
                        return this.concat(((promhx.Stream<T>) (dynargs.get(0))));
                    }

                    break;
                }

                case 106_440_182: {
                    if (field.equals("pause")) {
                        __temp_executeDef1 = false;
                        this.pause(dynargs.get(0));
                    }

                    break;
                }

                case -1_274_492_040: {
                    if (field.equals("filter")) {
                        __temp_executeDef1 = false;
                        return this.filter(((haxe.lang.Function) (dynargs.get(0))));
                    }

                    break;
                }

                case 3_441_070: {
                    if (field.equals("pipe")) {
                        __temp_executeDef1 = false;
                        return (this.pipe(((haxe.lang.Function) (dynargs.get(0)))));
                    }

                    break;
                }

                case -1_607_244_392: {
                    if (field.equals("endThen")) {
                        __temp_executeDef1 = false;
                        return (this.endThen(((haxe.lang.Function) (dynargs.get(0)))));
                    }

                    break;
                }

                case 329_417_686: {
                    if (field.equals("errorPipe")) {
                        __temp_executeDef1 = false;
                        return this.errorPipe(((haxe.lang.Function) (dynargs.get(0))));
                    }

                    break;
                }

                case 100_571: {
                    if (field.equals("end")) {
                        __temp_executeDef1 = false;
                        return this.end();
                    }

                    break;
                }

                case 64_098_707: {
                    if (field.equals("handleEnd")) {
                        __temp_executeDef1 = false;
                        this.handleEnd();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("_end_promise");
        baseArr.push("_end");
        baseArr.push("_pause");
        baseArr.push("deferred");
        super.__hx_getFields(baseArr);
    }

}
