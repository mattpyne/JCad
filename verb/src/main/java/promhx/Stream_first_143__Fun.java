package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_first_143__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Promise<T> s;

    /**
     *
     * @param s
     */
    public Stream_first_143__Fun(promhx.Promise<T> s) {
        super(1, 0);
        this.s = s;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T) (((java.lang.Object) (__fn_float1))))) : (((T) (__fn_dyn1))));
        if (!(this.s._resolved)) {
            this.s.handleResolve(x);
        }

        return null;
    }

}
