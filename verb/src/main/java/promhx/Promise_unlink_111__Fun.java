package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Promise_unlink_111__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase to;

    /**
     *
     */
    public promhx.Promise<T> _gthis;

    /**
     *
     * @param to
     * @param _gthis
     */
    public Promise_unlink_111__Fun(promhx.base.AsyncBase to, promhx.Promise<T> _gthis) {
        super(0, 0);
        this.to = to;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        if (!(this._gthis._fulfilled)) {
            java.lang.String msg = "Downstream Promise is not fullfilled";
            this._gthis.handleError(promhx.error.PromiseError.DownstreamNotFullfilled(msg));
        } else {
            this._gthis._update = this._gthis._update.filter(new promhx.Promise_unlink_117__Fun<T>(this.to));
        }

        return null;
    }

}
