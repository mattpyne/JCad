package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_filter_215__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Stream<T> ret;

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param ret
     * @param f
     */
    public Stream_filter_215__Fun(promhx.Stream<T> ret, haxe.lang.Function f) {
        super(1, 0);
        this.ret = ret;
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T) (((java.lang.Object) (__fn_float1))))) : (((T) (__fn_dyn1))));
        if (haxe.lang.Runtime.toBool(this.f.__hx_invoke1_o(0.0, x))) {
            this.ret.handleResolve(x);
        }

        return null;
    }

}
