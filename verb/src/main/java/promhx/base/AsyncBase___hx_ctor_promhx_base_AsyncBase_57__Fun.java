package promhx.base;

/**
 *
 * @author Matthew
 * @param <T_c>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase___hx_ctor_promhx_base_AsyncBase_57__Fun<T_c> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<T_c> next;

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param next
     * @param f
     */
    public AsyncBase___hx_ctor_promhx_base_AsyncBase_57__Fun(promhx.base.AsyncBase<T_c> next, haxe.lang.Function f) {
        super(1, 0);
        this.next = next;
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T_c x1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T_c) (((java.lang.Object) (__fn_float1))))) : (((T_c) (__fn_dyn1))));
        T_c tmp = ((T_c) (this.f.__hx_invoke1_o(0.0, x1)));
        this.next.handleResolve(tmp);
        return null;
    }

}
