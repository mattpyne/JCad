package promhx.base;

/**
 *
 * @author Matthew
 * @param <B>
 * @param <A>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase_link_265__Fun<B, A> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<B> next;

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param next
     * @param f
     */
    public AsyncBase_link_265__Fun(promhx.base.AsyncBase<B> next, haxe.lang.Function f) {
        super(1, 0);
        this.next = next;
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        A x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((A) (((java.lang.Object) (__fn_float1))))) : (((A) (__fn_dyn1))));
        B tmp = ((B) (this.f.__hx_invoke1_o(0.0, x)));
        this.next.handleResolve(tmp);
        return null;
    }

}
