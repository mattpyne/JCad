package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase<T> extends haxe.lang.HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param d
     */
    public static <T_c> void __hx_ctor_promhx_base_AsyncBase(promhx.base.AsyncBase<T_c> __hx_this, promhx.Deferred<T_c> d) {
        __hx_this._resolved = false;
        __hx_this._pending = false;
        __hx_this._errorPending = false;
        __hx_this._fulfilled = false;
        __hx_this._update = new haxe.root.Array<>(new java.lang.Object[]{});
        __hx_this._error = new haxe.root.Array<>(new haxe.lang.Function[]{});
        __hx_this._errored = false;
        if ((d != null)) {
            promhx.base.AsyncBase<T_c> next = __hx_this;
            haxe.lang.Function f = new promhx.base.AsyncBase___hx_ctor_promhx_base_AsyncBase_57__Fun_0<T_c>();
            java.lang.Object __temp_stmt2 = null;
            {
                haxe.lang.Function __temp_odecl1 = new promhx.base.AsyncBase___hx_ctor_promhx_base_AsyncBase_57__Fun<>(next, f);
                __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (next)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
            }

            d._update.push(__temp_stmt2);
            promhx.base.AsyncBase.immediateLinkUpdate((d), (next), (f));
        }

    }

    /**
     *
     * @param <A>
     * @param <B>
     * @param current
     * @param next
     * @param f
     */
    public static <A, B> void link(promhx.base.AsyncBase<A> current, promhx.base.AsyncBase<B> next, haxe.lang.Function f) {
        java.lang.Object __temp_stmt2 = null;
        {
            haxe.lang.Function __temp_odecl1 = new promhx.base.AsyncBase_link_265__Fun<B, A>(next, f);
            __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (next)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
        }

        current._update.push(__temp_stmt2);
        promhx.base.AsyncBase.immediateLinkUpdate((current), (next), (f));
    }

    /**
     *
     * @param <A>
     * @param <B>
     * @param current
     * @param next
     * @param f
     */
    public static <A, B> void immediateLinkUpdate(promhx.base.AsyncBase<A> current, promhx.base.AsyncBase<B> next, haxe.lang.Function f) {
        if (((current._errored && !(current._errorPending)) && (current._error.length <= 0))) {
            next.handleError(current._errorVal);
        }

        if ((current._resolved && !(current._pending))) {
            try {
                next.handleResolve(((B) (f.__hx_invoke1_o(0.0, current._val))));
            } catch (java.lang.Throwable __temp_catchallException1) {
                haxe.lang.Exceptions.setException(__temp_catchallException1);
                java.lang.Object __temp_catchall2 = __temp_catchallException1;
                if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                    __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
                }

                {
                    java.lang.Object e = __temp_catchall2;
                    next.handleError(e);
                }

            }

        }

    }

    /**
     *
     * @param <T1>
     * @param all
     * @param next
     */
    public static <T1> void linkAll(java.lang.Object all, promhx.base.AsyncBase<haxe.root.Array<T1>> next) {
        haxe.lang.Function cthen = new promhx.base.AsyncBase_linkAll_300__Fun<>(next, all);
        {
            java.lang.Object a2 = (haxe.lang.Runtime.callField(all, "iterator", null));
            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a2, "hasNext", null))) {
                promhx.base.AsyncBase<T1> a3 = ((promhx.base.AsyncBase<T1>) (haxe.lang.Runtime.callField(a2, "next", null)));
                haxe.root.Array<java.lang.Object> a4 = a3._update;
                haxe.lang.Function f = cthen;
                haxe.root.Array<promhx.base.AsyncBase<T1>> _g1 = new haxe.root.Array<>((new promhx.base.AsyncBase[]{}));
                {
                    java.lang.Object a21 = (haxe.lang.Runtime.callField(all, "iterator", null));
                    while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a21, "hasNext", null))) {
                        promhx.base.AsyncBase<T1> a22 = ((promhx.base.AsyncBase<T1>) (haxe.lang.Runtime.callField(a21, "next", null)));
                        if ((a22 != a3)) {
                            _g1.push(a22);
                        }

                    }

                }

                haxe.root.Array<promhx.base.AsyncBase<T1>> a11 = _g1;
                promhx.base.AsyncBase<T1> a23 = a3;
                haxe.lang.Function tmp = new promhx.base.AsyncBase_linkAll_310__Fun<>(f, a23, a11);
                a4.push(new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (next)), tmp}, new java.lang.String[]{}, new double[]{}));
            }

        }

        if (promhx.base.AsyncBase.allFulfilled(all)) {
            haxe.root.Array<T1> _g2 = new haxe.root.Array<>(((T1[]) (new java.lang.Object[]{})));
            {
                java.lang.Object a5 = (haxe.lang.Runtime.callField(all, "iterator", null));
                while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a5, "hasNext", null))) {
                    promhx.base.AsyncBase<T1> a6 = ((promhx.base.AsyncBase<T1>) (haxe.lang.Runtime.callField(a5, "next", null)));
                    _g2.push(a6._val);
                }

            }

            next.handleResolve(_g2);
        }

    }

    /**
     *
     * @param <A>
     * @param <B>
     * @param current
     * @param ret
     * @param f
     */
    public static <A, B> void pipeLink(promhx.base.AsyncBase<A> current, promhx.base.AsyncBase<B> ret, haxe.lang.Function f) {
        boolean[] linked = new boolean[]{false};
        haxe.lang.Function linkf = new promhx.base.AsyncBase_pipeLink_327__Fun<B, A>(ret, linked, f);
        current._update.push(new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (ret)), linkf}, new java.lang.String[]{}, new double[]{}));
        if ((current._resolved && !(current._pending))) {
            try {
                linkf.__hx_invoke1_o(0.0, current._val);
            } catch (java.lang.Throwable __temp_catchallException1) {
                haxe.lang.Exceptions.setException(__temp_catchallException1);
                java.lang.Object __temp_catchall2 = __temp_catchallException1;
                if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                    __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
                }

                {
                    java.lang.Object e = __temp_catchall2;
                    ret.handleError(e);
                }

            }

        }

    }

    /**
     *
     * @param as
     * @return
     */
    public static boolean allResolved(java.lang.Object as) {
        {
            java.lang.Object a = (haxe.lang.Runtime.callField(as, "iterator", null));
            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a, "hasNext", null))) {
                promhx.base.AsyncBase a1 = ((promhx.base.AsyncBase) (haxe.lang.Runtime.callField(a, "next", null)));
                if (!(a1._resolved)) {
                    return false;
                }

            }

        }

        return true;
    }

    /**
     *
     * @param as
     * @return
     */
    public static boolean allFulfilled(java.lang.Object as) {
        {
            java.lang.Object a = (haxe.lang.Runtime.callField(as, "iterator", null));
            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(a, "hasNext", null))) {
                promhx.base.AsyncBase a1 = ((promhx.base.AsyncBase) (haxe.lang.Runtime.callField(a, "next", null)));
                if (!(a1._fulfilled)) {
                    return false;
                }

            }

        }

        return true;
    }

    /**
     *
     */
    public T _val;

    /**
     *
     */
    public boolean _resolved;

    /**
     *
     */
    public boolean _fulfilled;

    /**
     *
     */
    public boolean _pending;

    /**
     *
     */
    public haxe.root.Array<java.lang.Object> _update;

    /**
     *
     */
    public haxe.root.Array<haxe.lang.Function> _error;

    /**
     *
     */
    public boolean _errored;

    /**
     *
     */
    public haxe.lang.Function _errorMap;

    /**
     *
     */
    public java.lang.Object _errorVal;

    /**
     *
     */
    public boolean _errorPending;

    /**
     *
     * @param empty
     */
    public AsyncBase(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param d
     */
    public AsyncBase(promhx.Deferred<T> d) {
        promhx.base.AsyncBase.__hx_ctor_promhx_base_AsyncBase(this, d);
    }

    /**
     *
     * @param f
     * @return
     */
    public promhx.base.AsyncBase<T> catchError(haxe.lang.Function f) {
        this._error.push(f);
        return this;
    }

    /**
     *
     * @param f
     * @return
     */
    public promhx.base.AsyncBase<T> errorThen(haxe.lang.Function f) {
        this._errorMap = f;
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isResolved() {
        return this._resolved;
    }

    /**
     *
     * @return
     */
    public boolean isErrored() {
        return this._errored;
    }

    /**
     *
     * @return
     */
    public boolean isErrorHandled() {
        return (this._error.length > 0);
    }

    /**
     *
     * @return
     */
    public boolean isErrorPending() {
        return this._errorPending;
    }

    /**
     *
     * @return
     */
    public boolean isFulfilled() {
        return this._fulfilled;
    }

    /**
     *
     * @return
     */
    public boolean isPending() {
        return this._pending;
    }

    /**
     *
     * @param val
     */
    public void handleResolve(T val) {
        this._resolve(val);
    }

    /**
     *
     * @param val
     */
    public void _resolve(T val) {
        promhx.base.AsyncBase<T> _gthis = this;
        if (this._pending) {
            haxe.lang.Function f = (new haxe.lang.Closure(this, "_resolve"));
            T a1 = val;
            haxe.lang.Function tmp = new promhx.base.AsyncBase__resolve_129__Fun<>(f, a1);
            promhx.base.EventLoop.queue.add(tmp);
            promhx.base.EventLoop.continueOnNextLoop();
        } else {
            this._resolved = true;
            this._pending = true;
            {
                promhx.base.EventLoop.queue.add(new promhx.base.AsyncBase__resolve_140__Fun<>(val, _gthis));
                promhx.base.EventLoop.continueOnNextLoop();
            }

        }

    }

    /**
     *
     * @param error
     */
    public void handleError(java.lang.Object error) {
        this._handleError(error);
    }

    /**
     *
     * @param error
     */
    public void _handleError(java.lang.Object error) {
        promhx.base.AsyncBase<T> _gthis = this;
        haxe.lang.Function update_errors = new promhx.base.AsyncBase__handleError_165__Fun<>(_gthis);
        if (!(this._errorPending)) {
            this._errorPending = true;
            this._errored = true;
            this._errorVal = error;
            {
                promhx.base.EventLoop.queue.add(new promhx.base.AsyncBase__handleError_203__Fun<>(update_errors, error, _gthis));
                promhx.base.EventLoop.continueOnNextLoop();
            }

        }

    }

    /**
     *
     * @param <A>
     * @param f
     * @return
     */
    public <A> promhx.base.AsyncBase<A> then(haxe.lang.Function f) {
        promhx.base.AsyncBase<A> ret = new promhx.base.AsyncBase<>(((promhx.Deferred<A>) (null)));
        {
            promhx.base.AsyncBase<A> next = ret;
            haxe.lang.Function f1 = f;
            java.lang.Object __temp_stmt2 = null;
            {
                haxe.lang.Function __temp_odecl1 = new promhx.base.AsyncBase_then_225__Fun<A, T>(next, f1);
                __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (next)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
            }

            this._update.push(__temp_stmt2);
            promhx.base.AsyncBase.immediateLinkUpdate((this), (next), (f1));
        }

        return ret;
    }

    /**
     *
     * @param to
     */
    public void unlink(promhx.base.AsyncBase to) {
        promhx.base.AsyncBase<T> _gthis = this;
        {
            promhx.base.EventLoop.queue.add(new promhx.base.AsyncBase_unlink_234__Fun<>(to, _gthis));
            promhx.base.EventLoop.continueOnNextLoop();
        }

    }

    /**
     *
     * @param to
     * @return
     */
    public boolean isLinked(promhx.base.AsyncBase to) {
        boolean updated = false;
        {
            int _g = 0;
            haxe.root.Array<java.lang.Object> _g1 = this._update;
            while ((_g < _g1.length)) {
                java.lang.Object u = _g1.get(_g);
                ++_g;
                if ((((promhx.base.AsyncBase) (haxe.lang.Runtime.getField(u, "async", true))) == to)) {
                    return true;
                }

            }

        }

        return updated;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_486_623_064: {
                    if (field.equals("_errorVal")) {
                        __temp_executeDef1 = false;
                        this._errorVal = (value);
                        return value;
                    }

                    break;
                }

                case 2_946_658: {
                    if (field.equals("_val")) {
                        __temp_executeDef1 = false;
                        this._val = ((T) (((java.lang.Object) (value))));
                        return (haxe.lang.Runtime.toDouble(value));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_757_208_654: {
                    if (field.equals("_errorPending")) {
                        __temp_executeDef1 = false;
                        this._errorPending = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 2_946_658: {
                    if (field.equals("_val")) {
                        __temp_executeDef1 = false;
                        this._val = ((T) (value));
                        return value;
                    }

                    break;
                }

                case 1_486_623_064: {
                    if (field.equals("_errorVal")) {
                        __temp_executeDef1 = false;
                        this._errorVal = (value);
                        return value;
                    }

                    break;
                }

                case -250_811_049: {
                    if (field.equals("_resolved")) {
                        __temp_executeDef1 = false;
                        this._resolved = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 1_486_614_419: {
                    if (field.equals("_errorMap")) {
                        __temp_executeDef1 = false;
                        this._errorMap = ((haxe.lang.Function) (value));
                        return value;
                    }

                    break;
                }

                case 966_288_288: {
                    if (field.equals("_fulfilled")) {
                        __temp_executeDef1 = false;
                        this._fulfilled = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 879_240_040: {
                    if (field.equals("_errored")) {
                        __temp_executeDef1 = false;
                        this._errored = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 1_675_636_824: {
                    if (field.equals("_pending")) {
                        __temp_executeDef1 = false;
                        this._pending = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case -1_478_413_047: {
                    if (field.equals("_error")) {
                        __temp_executeDef1 = false;
                        this._error = ((haxe.root.Array<haxe.lang.Function>) (value));
                        return value;
                    }

                    break;
                }

                case 1_869_624_808: {
                    if (field.equals("_update")) {
                        __temp_executeDef1 = false;
                        this._update = ((haxe.root.Array<java.lang.Object>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -428_632_093: {
                    if (field.equals("isLinked")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isLinked"));
                    }

                    break;
                }

                case 2_946_658: {
                    if (field.equals("_val")) {
                        __temp_executeDef1 = false;
                        return this._val;
                    }

                    break;
                }

                case -840_447_469: {
                    if (field.equals("unlink")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "unlink"));
                    }

                    break;
                }

                case -250_811_049: {
                    if (field.equals("_resolved")) {
                        __temp_executeDef1 = false;
                        return this._resolved;
                    }

                    break;
                }

                case 3_558_941: {
                    if (field.equals("then")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "then"));
                    }

                    break;
                }

                case 966_288_288: {
                    if (field.equals("_fulfilled")) {
                        __temp_executeDef1 = false;
                        return this._fulfilled;
                    }

                    break;
                }

                case 847_407_873: {
                    if (field.equals("_handleError")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "_handleError"));
                    }

                    break;
                }

                case 1_675_636_824: {
                    if (field.equals("_pending")) {
                        __temp_executeDef1 = false;
                        return this._pending;
                    }

                    break;
                }

                case 1_469_451_456: {
                    if (field.equals("handleError")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "handleError"));
                    }

                    break;
                }

                case 1_869_624_808: {
                    if (field.equals("_update")) {
                        __temp_executeDef1 = false;
                        return this._update;
                    }

                    break;
                }

                case -839_374_675: {
                    if (field.equals("_resolve")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "_resolve"));
                    }

                    break;
                }

                case -1_478_413_047: {
                    if (field.equals("_error")) {
                        __temp_executeDef1 = false;
                        return this._error;
                    }

                    break;
                }

                case 1_674_964_644: {
                    if (field.equals("handleResolve")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "handleResolve"));
                    }

                    break;
                }

                case 879_240_040: {
                    if (field.equals("_errored")) {
                        __temp_executeDef1 = false;
                        return this._errored;
                    }

                    break;
                }

                case -1_262_366_451: {
                    if (field.equals("isPending")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isPending"));
                    }

                    break;
                }

                case 1_486_614_419: {
                    if (field.equals("_errorMap")) {
                        __temp_executeDef1 = false;
                        return this._errorMap;
                    }

                    break;
                }

                case -661_345_515: {
                    if (field.equals("isFulfilled")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isFulfilled"));
                    }

                    break;
                }

                case 1_486_623_064: {
                    if (field.equals("_errorVal")) {
                        __temp_executeDef1 = false;
                        return this._errorVal;
                    }

                    break;
                }

                case -1_195_611_975: {
                    if (field.equals("isErrorPending")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isErrorPending"));
                    }

                    break;
                }

                case 1_757_208_654: {
                    if (field.equals("_errorPending")) {
                        __temp_executeDef1 = false;
                        return this._errorPending;
                    }

                    break;
                }

                case 179_779_166: {
                    if (field.equals("isErrorHandled")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isErrorHandled"));
                    }

                    break;
                }

                case -488_640_499: {
                    if (field.equals("catchError")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "catchError"));
                    }

                    break;
                }

                case -2_058_763_235: {
                    if (field.equals("isErrored")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isErrored"));
                    }

                    break;
                }

                case 329_535_557: {
                    if (field.equals("errorThen")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "errorThen"));
                    }

                    break;
                }

                case -1_134_599_358: {
                    if (field.equals("isResolved")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "isResolved"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_486_623_064: {
                    if (field.equals("_errorVal")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this._errorVal));
                    }

                    break;
                }

                case 2_946_658: {
                    if (field.equals("_val")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this._val));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -428_632_093: {
                    if (field.equals("isLinked")) {
                        __temp_executeDef1 = false;
                        return this.isLinked(((promhx.base.AsyncBase) (dynargs.get(0))));
                    }

                    break;
                }

                case -488_640_499: {
                    if (field.equals("catchError")) {
                        __temp_executeDef1 = false;
                        return this.catchError(((haxe.lang.Function) (dynargs.get(0))));
                    }

                    break;
                }

                case -840_447_469: {
                    if (field.equals("unlink")) {
                        __temp_executeDef1 = false;
                        this.unlink(((promhx.base.AsyncBase) (dynargs.get(0))));
                    }

                    break;
                }

                case 329_535_557: {
                    if (field.equals("errorThen")) {
                        __temp_executeDef1 = false;
                        return this.errorThen(((haxe.lang.Function) (dynargs.get(0))));
                    }

                    break;
                }

                case 3_558_941: {
                    if (field.equals("then")) {
                        __temp_executeDef1 = false;
                        return (this.then(((haxe.lang.Function) (dynargs.get(0)))));
                    }

                    break;
                }

                case -1_134_599_358: {
                    if (field.equals("isResolved")) {
                        __temp_executeDef1 = false;
                        return this.isResolved();
                    }

                    break;
                }

                case 847_407_873: {
                    if (field.equals("_handleError")) {
                        __temp_executeDef1 = false;
                        this._handleError(dynargs.get(0));
                    }

                    break;
                }

                case -2_058_763_235: {
                    if (field.equals("isErrored")) {
                        __temp_executeDef1 = false;
                        return this.isErrored();
                    }

                    break;
                }

                case 1_469_451_456: {
                    if (field.equals("handleError")) {
                        __temp_executeDef1 = false;
                        this.handleError(dynargs.get(0));
                    }

                    break;
                }

                case 179_779_166: {
                    if (field.equals("isErrorHandled")) {
                        __temp_executeDef1 = false;
                        return this.isErrorHandled();
                    }

                    break;
                }

                case -839_374_675: {
                    if (field.equals("_resolve")) {
                        __temp_executeDef1 = false;
                        this._resolve(((T) (dynargs.get(0))));
                    }

                    break;
                }

                case -1_195_611_975: {
                    if (field.equals("isErrorPending")) {
                        __temp_executeDef1 = false;
                        return this.isErrorPending();
                    }

                    break;
                }

                case 1_674_964_644: {
                    if (field.equals("handleResolve")) {
                        __temp_executeDef1 = false;
                        this.handleResolve(((T) (dynargs.get(0))));
                    }

                    break;
                }

                case -661_345_515: {
                    if (field.equals("isFulfilled")) {
                        __temp_executeDef1 = false;
                        return this.isFulfilled();
                    }

                    break;
                }

                case -1_262_366_451: {
                    if (field.equals("isPending")) {
                        __temp_executeDef1 = false;
                        return this.isPending();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("_errorPending");
        baseArr.push("_errorVal");
        baseArr.push("_errorMap");
        baseArr.push("_errored");
        baseArr.push("_error");
        baseArr.push("_update");
        baseArr.push("_pending");
        baseArr.push("_fulfilled");
        baseArr.push("_resolved");
        baseArr.push("_val");
        super.__hx_getFields(baseArr);
    }

}
