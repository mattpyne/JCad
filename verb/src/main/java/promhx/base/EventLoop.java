package promhx.base;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class EventLoop extends haxe.lang.HxObject {

    /**
     *
     */
    public static haxe.java.vm.AtomicList<haxe.lang.Function> queue;

    /**
     *
     */
    public static haxe.lang.Function nextLoop;

    static {
        promhx.base.EventLoop.queue = new haxe.java.vm.AtomicList<>();
    }

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_promhx_base_EventLoop(promhx.base.EventLoop __hx_this) {
    }

    /**
     *
     * @param eqf
     */
    public static void enqueue(haxe.lang.Function eqf) {
        promhx.base.EventLoop.queue.add(eqf);
        promhx.base.EventLoop.continueOnNextLoop();
    }

    /**
     *
     * @param f
     * @return
     */
    public static haxe.lang.Function set_nextLoop(haxe.lang.Function f) {
        if ((promhx.base.EventLoop.nextLoop != null)) {
            throw haxe.lang.HaxeException.wrap("nextLoop has already been set");
        } else {
            promhx.base.EventLoop.nextLoop = f;
        }

        return promhx.base.EventLoop.nextLoop;
    }

    /**
     *
     * @return
     */
    public static boolean queueEmpty() {
        return (promhx.base.EventLoop.queue.peekLast() == null);
    }

    /**
     *
     * @param max_iterations
     * @return
     */
    public static boolean finish(java.lang.Object max_iterations) {
        int __temp_max_iterations1 = ((haxe.lang.Runtime.eq(max_iterations, null)) ? (1_000) : (haxe.lang.Runtime.toInt(max_iterations)));
        haxe.lang.Function fn = null;
        while (true) {
            boolean tmp = false;
            if ((__temp_max_iterations1-- > 0)) {
                fn = ((haxe.lang.Function) (promhx.base.EventLoop.queue.pop()));
                tmp = (fn != null);
            } else {
                tmp = false;
            }

            if (!(tmp)) {
                break;
            }

            (fn).__hx_invoke0_o();
        }

        return (promhx.base.EventLoop.queue.peekLast() == null);
    }

    /**
     *
     */
    public static void clear() {
        promhx.base.EventLoop.queue = new haxe.java.vm.AtomicList<>();
    }

    /**
     *
     */
    public static void f() {
        haxe.lang.Function fn = ((haxe.lang.Function) (promhx.base.EventLoop.queue.pop()));
        if ((fn != null)) {
            (fn).__hx_invoke0_o();
        }

        if ((promhx.base.EventLoop.queue.peekLast() != null)) {
            promhx.base.EventLoop.continueOnNextLoop();
        }

    }

    /**
     *
     */
    public static void continueOnNextLoop() {
        if ((promhx.base.EventLoop.nextLoop != null)) {
            promhx.base.EventLoop.nextLoop.__hx_invoke1_o(0.0, (new haxe.lang.Closure(promhx.base.EventLoop.class, "f")));
        } else {
            promhx.base.EventLoop.f();
        }

    }

    /**
     *
     * @param empty
     */
    public EventLoop(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public EventLoop() {
        promhx.base.EventLoop.__hx_ctor_promhx_base_EventLoop(this);
    }

}
