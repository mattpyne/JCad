package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase__resolve_140__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public T val;

    /**
     *
     */
    public promhx.base.AsyncBase<T> _gthis;

    /**
     *
     * @param val
     * @param _gthis
     */
    public AsyncBase__resolve_140__Fun(T val, promhx.base.AsyncBase<T> _gthis) {
        super(0, 0);
        this.val = val;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        this._gthis._val = this.val;
        {
            int _g = 0;
            haxe.root.Array<java.lang.Object> _g1 = this._gthis._update;
            while ((_g < _g1.length)) {
                java.lang.Object up = _g1.get(_g);
                ++_g;
                try {
                    haxe.lang.Runtime.callField(up, "linkf", new haxe.root.Array(new java.lang.Object[]{this.val}));
                } catch (java.lang.Throwable __temp_catchallException1) {
                    haxe.lang.Exceptions.setException(__temp_catchallException1);
                    java.lang.Object __temp_catchall2 = __temp_catchallException1;
                    if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                        __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
                    }

                    {
                        java.lang.Object e = __temp_catchall2;
                        ((promhx.base.AsyncBase) (haxe.lang.Runtime.getField(up, "async", true))).handleError(e);
                    }

                }

            }

        }

        this._gthis._fulfilled = true;
        this._gthis._pending = false;
        return null;
    }

}
