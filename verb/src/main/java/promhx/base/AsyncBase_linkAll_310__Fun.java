package promhx.base;

/**
 *
 * @author Matthew
 * @param <T1>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase_linkAll_310__Fun<T1> extends haxe.lang.Function {

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     */
    public promhx.base.AsyncBase<T1> a23;

    /**
     *
     */
    public haxe.root.Array<promhx.base.AsyncBase<T1>> a11;

    /**
     *
     * @param f
     * @param a23
     * @param a11
     */
    public AsyncBase_linkAll_310__Fun(haxe.lang.Function f, promhx.base.AsyncBase<T1> a23, haxe.root.Array<promhx.base.AsyncBase<T1>> a11) {
        super(1, 0);
        this.f = f;
        this.a23 = a23;
        this.a11 = a11;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T1 v1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T1) (((java.lang.Object) (__fn_float1))))) : (((T1) (__fn_dyn1))));
        this.f.__hx_invoke3_o(0.0, this.a11, 0.0, this.a23, 0.0, v1);
        return null;
    }

}
