package promhx.base;

/**
 *
 * @author Matthew
 * @param <B>
 * @param <A>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase_pipeLink_327__Fun<B, A> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<B> ret;

    /**
     *
     */
    public boolean[] linked;

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param ret
     * @param linked
     * @param f
     */
    public AsyncBase_pipeLink_327__Fun(promhx.base.AsyncBase<B> ret, boolean[] linked, haxe.lang.Function f) {
        super(1, 0);
        this.ret = ret;
        this.linked = linked;
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        A x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((A) (((java.lang.Object) (__fn_float1))))) : (((A) (__fn_dyn1))));
        if (!(this.linked[0])) {
            this.linked[0] = true;
            promhx.base.AsyncBase<B> pipe_ret = ((promhx.base.AsyncBase<B>) (this.f.__hx_invoke1_o(0.0, x)));
            java.lang.Object __temp_stmt2 = null;
            {
                haxe.lang.Function __temp_odecl1 = new haxe.lang.Closure(this.ret, "handleResolve");
                __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (this.ret)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
            }
            pipe_ret._update.push(__temp_stmt2);
            promhx.base.AsyncBase.immediateLinkUpdate(((promhx.base.AsyncBase<B>) (pipe_ret)), ((promhx.base.AsyncBase<B>) (this.ret)), ((haxe.lang.Function) (new promhx.base.AsyncBase_pipeLink_335__Fun<B>())));
        }
        return null;
    }

}
