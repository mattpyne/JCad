package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase__handleError_165__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<T> _gthis;

    /**
     *
     * @param _gthis
     */
    public AsyncBase__handleError_165__Fun(promhx.base.AsyncBase<T> _gthis) {
        super(1, 0);
        this._gthis = _gthis;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object e = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1));
        if ((this._gthis._error.length > 0)) {
            int _g = 0;
            haxe.root.Array<haxe.lang.Function> _g1 = this._gthis._error;
            while ((_g < _g1.length)) {
                haxe.lang.Function ef = _g1.get(_g);
                ++_g;
                ef.__hx_invoke1_o(0.0, e);
            }

        } else {
            if ((this._gthis._update.length > 0)) {
                int _g2 = 0;
                haxe.root.Array<java.lang.Object> _g11 = this._gthis._update;
                while ((_g2 < _g11.length)) {
                    java.lang.Object up = _g11.get(_g2);
                    ++_g2;
                    ((promhx.base.AsyncBase) (haxe.lang.Runtime.getField(up, "async", true))).handleError(e);
                }

            } else {
                throw haxe.lang.HaxeException.wrap(e);
            }

        }

        this._gthis._errorPending = false;
        return null;
    }

}
