package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase_unlink_234__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase to;

    /**
     *
     */
    public promhx.base.AsyncBase<T> _gthis;

    /**
     *
     * @param to
     * @param _gthis
     */
    public AsyncBase_unlink_234__Fun(promhx.base.AsyncBase to, promhx.base.AsyncBase<T> _gthis) {
        super(0, 0);
        this.to = to;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        this._gthis._update = this._gthis._update.filter(new promhx.base.AsyncBase_unlink_235__Fun<T>(this.to));
        return null;
    }

}
