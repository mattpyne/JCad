package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase__handleError_203__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public haxe.lang.Function update_errors;

    /**
     *
     */
    public java.lang.Object error;

    /**
     *
     */
    public promhx.base.AsyncBase<T> _gthis;

    /**
     *
     * @param update_errors
     * @param error
     * @param _gthis
     */
    public AsyncBase__handleError_203__Fun(haxe.lang.Function update_errors, java.lang.Object error, promhx.base.AsyncBase<T> _gthis) {
        super(0, 0);
        this.update_errors = update_errors;
        this.error = error;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        if ((this._gthis._errorMap != null)) {
            try {
                T tmp = ((T) (this._gthis._errorMap.__hx_invoke1_o(0.0, this.error)));
                this._gthis._resolve(tmp);
            } catch (java.lang.Throwable __temp_catchallException1) {
                haxe.lang.Exceptions.setException(__temp_catchallException1);
                java.lang.Object __temp_catchall2 = __temp_catchallException1;
                if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                    __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
                }

                {
                    java.lang.Object e1 = __temp_catchall2;
                    this.update_errors.__hx_invoke1_o(0.0, e1);
                }

            }

        } else {
            this.update_errors.__hx_invoke1_o(0.0, this.error);
        }

        return null;
    }

}
