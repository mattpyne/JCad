package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase_unlink_235__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase to;

    /**
     *
     * @param to
     */
    public AsyncBase_unlink_235__Fun(promhx.base.AsyncBase to) {
        super(1, 0);
        this.to = to;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1));
        return (((promhx.base.AsyncBase) (haxe.lang.Runtime.getField(x, "async", true))) != this.to);
    }

}
