package promhx.base;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AsyncBase__resolve_129__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     */
    public T a1;

    /**
     *
     * @param f
     * @param a1
     */
    public AsyncBase__resolve_129__Fun(haxe.lang.Function f, T a1) {
        super(0, 0);
        this.f = f;
        this.a1 = a1;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        this.f.__hx_invoke1_o(0.0, this.a1);
        return null;
    }

}
