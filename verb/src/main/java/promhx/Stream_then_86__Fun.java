package promhx;

/**
 *
 * @author Matthew
 * @param <A>
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_then_86__Fun<A, T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Stream<A> ret;

    /**
     *
     * @param ret
     */
    public Stream_then_86__Fun(promhx.Stream<A> ret) {
        super(1, 0);
        this.ret = ret;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.ds.Option x1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.ds.Option) (((java.lang.Object) (__fn_float1))))) : (((haxe.ds.Option) (__fn_dyn1))));
        this.ret.end();
        return null;
    }

}
