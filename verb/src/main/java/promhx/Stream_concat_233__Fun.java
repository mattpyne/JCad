package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_concat_233__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Stream<T> s;

    /**
     *
     */
    public promhx.Stream<T> ret;

    /**
     *
     * @param s
     * @param ret
     */
    public Stream_concat_233__Fun(promhx.Stream<T> s, promhx.Stream<T> ret) {
        super(1, 0);
        this.s = s;
        this.ret = ret;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.ds.Option _1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.ds.Option) (((java.lang.Object) (__fn_float1))))) : (((haxe.ds.Option) (__fn_dyn1))));
        this.s.pipe((new promhx.Stream_concat_234__Fun<>(this.ret)));
        this.s._end_promise.then((new promhx.Stream_concat_238__Fun<>(this.ret)));
        return null;
    }

}
