package promhx;

import haxe.lang.Closure;
import haxe.lang.Runtime;
import haxe.lang.DynamicObject;
import haxe.lang.EmptyObject;
import haxe.lang.Exceptions;
import haxe.lang.Function;
import haxe.lang.HaxeException;
import promhx.base.AsyncBase;
import promhx.base.EventLoop;
import promhx.error.PromiseError;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Promise<T> extends AsyncBase<T> {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param d
     */
    public static <T_c> void __hx_ctor_promhx_Promise(Promise<T_c> __hx_this, Deferred<T_c> d) {
        AsyncBase.__hx_ctor_promhx_base_AsyncBase((__hx_this), (d));
        __hx_this._rejected = false;
    }

    /**
     *
     * @param <T1>
     * @param itb
     * @return
     */
    public static <T1> Promise<haxe.root.Array<T1>> whenAll(Object itb) {
        Promise<haxe.root.Array<T1>> ret = new Promise<>(((Deferred<haxe.root.Array<T1>>) (null)));
        {
            Object all = itb;
            AsyncBase<haxe.root.Array<T1>> next = ret;
            Function cthen = new Promise_whenAll_86__Fun_0<>(next, all);
            {
                Object a2 = (Runtime.callField(all, "iterator", null));
                while (Runtime.toBool(Runtime.callField(a2, "hasNext", null))) {
                    AsyncBase<T1> a3 = ((AsyncBase<T1>) (Runtime.callField(a2, "next", null)));
                    haxe.root.Array<Object> a4 = a3._update;
                    Function f = cthen;
                    haxe.root.Array<AsyncBase<T1>> _g1 = new haxe.root.Array<>((new AsyncBase[]{}));
                    {
                        Object a21 = (Runtime.callField(all, "iterator", null));
                        while (Runtime.toBool(Runtime.callField(a21, "hasNext", null))) {
                            AsyncBase<T1> a22 = ((AsyncBase<T1>) (Runtime.callField(a21, "next", null)));
                            if ((a22 != a3)) {
                                _g1.push(a22);
                            }

                        }

                    }

                    haxe.root.Array<AsyncBase<T1>> a11 = _g1;
                    AsyncBase<T1> a23 = a3;
                    Function tmp = new Promise_whenAll_86__Fun<>(f, a23, a11);
                    a4.push(new DynamicObject(new String[]{"async", "linkf"}, new Object[]{((AsyncBase) (next)), tmp}, new String[]{}, new double[]{}));
                }

            }

            if (AsyncBase.allFulfilled(all)) {
                haxe.root.Array<T1> _g2 = new haxe.root.Array<>(((T1[]) (new Object[]{})));
                {
                    Object a5 = (Runtime.callField(all, "iterator", null));
                    while (Runtime.toBool(Runtime.callField(a5, "hasNext", null))) {
                        AsyncBase<T1> a6 = ((AsyncBase<T1>) (Runtime.callField(a5, "next", null)));
                        _g2.push(a6._val);
                    }

                }

                next.handleResolve(_g2);
            }

        }

        return ret;
    }

    /**
     *
     * @param <T2>
     * @param _val
     * @return
     */
    public static <T2> Promise<T2> promise(T2 _val) {
        Promise<T2> ret = new Promise<>(((Deferred<T2>) (null)));
        ret.handleResolve(_val);
        return ret;
    }

    /**
     *
     */
    public boolean _rejected;

    /**
     *
     * @param empty
     */
    public Promise(EmptyObject empty) {
        super((EmptyObject) EmptyObject.EMPTY);
    }

    /**
     *
     * @param d
     */
    public Promise(Deferred<T> d) {
        super((EmptyObject) EmptyObject.EMPTY);
        Promise.__hx_ctor_promhx_Promise(this, d);
    }

    /**
     *
     * @return
     */
    public boolean isRejected() {
        return this._rejected;
    }

    /**
     *
     * @param e
     */
    public void reject(Object e) {
        this._rejected = true;
        this.handleError(e);
    }

    /**
     *
     * @param val
     */
    @Override
    public void handleResolve(T val) {
        if (this._resolved) {
            String msg = "Promise has already been resolved";
            throw HaxeException.wrap(PromiseError.AlreadyResolved(msg));
        }

        this._resolve(val);
    }

    /**
     *
     * @param <A>
     * @param f
     * @return
     */
    @Override
    public <A> AsyncBase<A> then(Function f) {
        Promise<A> ret = new Promise<>(((Deferred<A>) (null)));
        AsyncBase<A> next = ret;
        Function f1 = f;
        Object __temp_stmt2;
        Function<A, T> __temp_odecl1 = new Promise_then_106__Fun(next, f1);

        __temp_stmt2 = new DynamicObject(
                new String[]{"async", "linkf"},
                new Object[]{next, __temp_odecl1},
                new String[]{}, new double[]{});

        this._update.push(__temp_stmt2);
        AsyncBase.immediateLinkUpdate((this), (next), (f1));
        return ret;
    }

    /**
     *
     * @param to
     */
    @Override
    public void unlink(AsyncBase to) {
        Promise<T> _gthis = this;
        EventLoop.queue.add(new Promise_unlink_111__Fun<>(to, _gthis));
        EventLoop.continueOnNextLoop();
    }

    /**
     *
     * @param error
     */
    @Override
    public void handleError(Object error) {
        this._rejected = true;
        this._handleError(error);
    }

    /**
     *
     * @param <A>
     * @param f
     * @return
     */
    public <A> Promise<A> pipe(Function f) {
        Promise<A> ret = new Promise((Deferred<A>) null);
        AsyncBase<A> ret1 = ret;
        Function f1 = f;
        boolean[] linked = new boolean[]{false};

        Function<A, T> linkf = new Promise_pipe_129__Fun(ret1, linked, f1);
        this._update.push(new DynamicObject(
                new String[]{"async", "linkf"},
                new Object[]{ret1, linkf},
                new String[]{}, new double[]{}));
        
        if (this._resolved && !this._pending) {
            
            try {
                
                linkf.__hx_invoke1_o(0.0, this._val);
            } catch (Throwable __temp_catchallException1) {
                
                Exceptions.setException(__temp_catchallException1);
                Object __temp_catchall2 = __temp_catchallException1;
                
                if (__temp_catchall2 instanceof HaxeException) {
                    __temp_catchall2 = ((HaxeException) (__temp_catchallException1)).obj;
                }

                Object e = __temp_catchall2;
                ret1.handleError(e);
            }
        }

        return ret;
    }

    /**
     *
     * @param f
     * @return
     */
    public Promise<T> errorPipe(Function f) {
        Promise<T> ret = new Promise<>(((Deferred<T>) (null)));
        this.catchError(new Promise_errorPipe_138__Fun<>(ret, f));
        this.then((new Closure(ret, "_resolve")));
        return ret;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -517_978_659: {
                    if (field.equals("_rejected")) {
                        __temp_executeDef1 = false;
                        this._rejected = Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 329_417_686: {
                    if (field.equals("errorPipe")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "errorPipe"));
                    }

                    break;
                }

                case -517_978_659: {
                    if (field.equals("_rejected")) {
                        __temp_executeDef1 = false;
                        return this._rejected;
                    }

                    break;
                }

                case 3_441_070: {
                    if (field.equals("pipe")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "pipe"));
                    }

                    break;
                }

                case -1_401_766_968: {
                    if (field.equals("isRejected")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "isRejected"));
                    }

                    break;
                }

                case 1_469_451_456: {
                    if (field.equals("handleError")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "handleError"));
                    }

                    break;
                }

                case -934_710_369: {
                    if (field.equals("reject")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "reject"));
                    }

                    break;
                }

                case -840_447_469: {
                    if (field.equals("unlink")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "unlink"));
                    }

                    break;
                }

                case 1_674_964_644: {
                    if (field.equals("handleResolve")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "handleResolve"));
                    }

                    break;
                }

                case 3_558_941: {
                    if (field.equals("then")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "then"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, haxe.root.Array dynargs) {
        {
            int __temp_hash2 = field.hashCode();
            boolean __temp_executeDef1 = true;
            switch (__temp_hash2) {
                case 1_469_451_456:
                case -840_447_469:
                case 3_558_941:
                case 1_674_964_644: {
                    if (((((__temp_hash2 == 1_469_451_456) && field.equals("handleError"))) || ((((__temp_hash2 == -840_447_469) && field.equals("unlink"))) || ((((__temp_hash2 == 3_558_941) && field.equals("then"))) || field.equals("handleResolve"))))) {
                        __temp_executeDef1 = false;
                        return Runtime.slowCallField(this, field, dynargs);
                    }

                    break;
                }

                case -1_401_766_968: {
                    if (field.equals("isRejected")) {
                        __temp_executeDef1 = false;
                        return this.isRejected();
                    }

                    break;
                }

                case 329_417_686: {
                    if (field.equals("errorPipe")) {
                        __temp_executeDef1 = false;
                        return this.errorPipe(((Function) (dynargs.get(0))));
                    }

                    break;
                }

                case -934_710_369: {
                    if (field.equals("reject")) {
                        __temp_executeDef1 = false;
                        this.reject(dynargs.get(0));
                    }

                    break;
                }

                case 3_441_070: {
                    if (field.equals("pipe")) {
                        __temp_executeDef1 = false;
                        return (this.pipe(((Function) (dynargs.get(0)))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<String> baseArr) {
        baseArr.push("_rejected");
        super.__hx_getFields(baseArr);
    }

}
