package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_concat_234__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Stream<T> ret;

    /**
     *
     * @param ret
     */
    public Stream_concat_234__Fun(promhx.Stream<T> ret) {
        super(1, 0);
        this.ret = ret;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T x1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T) (((java.lang.Object) (__fn_float1))))) : (((T) (__fn_dyn1))));
        this.ret.handleResolve(x1);
        return this.ret;
    }

}
