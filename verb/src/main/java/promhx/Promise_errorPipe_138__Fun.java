package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Promise_errorPipe_138__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Promise<T> ret;

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param ret
     * @param f
     */
    public Promise_errorPipe_138__Fun(promhx.Promise<T> ret, haxe.lang.Function f) {
        super(1, 0);
        this.ret = ret;
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object e = ((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1);
        promhx.Promise<T> piped = ((promhx.Promise<T>) (this.f.__hx_invoke1_o(0.0, e)));
        piped.then(new haxe.lang.Closure(this.ret, "_resolve"));
        return null;
    }

}
