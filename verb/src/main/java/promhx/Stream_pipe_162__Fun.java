package promhx;

/**
 *
 * @author Matthew
 * @param <A>
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_pipe_162__Fun<A, T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.base.AsyncBase<A> ret1;

    /**
     *
     */
    public boolean[] linked;

    /**
     *
     */
    public haxe.lang.Function f1;

    /**
     *
     * @param ret1
     * @param linked
     * @param f1
     */
    public Stream_pipe_162__Fun(promhx.base.AsyncBase<A> ret1, boolean[] linked, haxe.lang.Function f1) {
        super(1, 0);
        this.ret1 = ret1;
        this.linked = linked;
        this.f1 = f1;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T) (((java.lang.Object) (__fn_float1))))) : (((T) (__fn_dyn1))));
        if (!(this.linked[0])) {
            this.linked[0] = true;
            promhx.base.AsyncBase<A> pipe_ret = ((promhx.base.AsyncBase<A>) (this.f1.__hx_invoke1_o(0.0, x)));
            java.lang.Object __temp_stmt2 = null;
            {
                haxe.lang.Function __temp_odecl1 = new haxe.lang.Closure(this.ret1, "handleResolve");
                __temp_stmt2 = new haxe.lang.DynamicObject(new java.lang.String[]{"async", "linkf"}, new java.lang.Object[]{((promhx.base.AsyncBase) (this.ret1)), __temp_odecl1}, new java.lang.String[]{}, new double[]{});
            }
            pipe_ret._update.push(__temp_stmt2);
            promhx.base.AsyncBase.immediateLinkUpdate(((promhx.base.AsyncBase<A>) (pipe_ret)), ((promhx.base.AsyncBase<A>) (this.ret1)), ((haxe.lang.Function) (new promhx.Stream_pipe_162__Fun_0<A>())));
        }
        return null;
    }

}
