package promhx;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Stream_errorPipe_172__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Stream<T> ret;

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param ret
     * @param f
     */
    public Stream_errorPipe_172__Fun(promhx.Stream<T> ret, haxe.lang.Function f) {
        super(1, 0);
        this.ret = ret;
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object e = ((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1);
        promhx.Stream<T> piped = ((promhx.Stream<T>) (this.f.__hx_invoke1_o(0.0, e)));
        piped.then(new haxe.lang.Closure(this.ret, "_resolve"));
        piped._end_promise.then(new haxe.lang.Closure(this.ret._end_promise, "_resolve"));
        return null;
    }

}
