package promhx.error;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class PromiseError extends haxe.lang.ParamEnum {

    /**
     *
     */
    public static final java.lang.String[] __hx_constructs = new java.lang.String[]{"AlreadyResolved", "DownstreamNotFullfilled"};

    /**
     *
     * @param message
     * @return
     */
    public static promhx.error.PromiseError AlreadyResolved(java.lang.String message) {
        return new promhx.error.PromiseError(0, new java.lang.Object[]{message});
    }

    /**
     *
     * @param message
     * @return
     */
    public static promhx.error.PromiseError DownstreamNotFullfilled(java.lang.String message) {
        return new promhx.error.PromiseError(1, new java.lang.Object[]{message});
    }

    /**
     *
     * @param index
     * @param params
     */
    public PromiseError(int index, java.lang.Object[] params) {
        super(index, params);
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.String getTag() {
        return promhx.error.PromiseError.__hx_constructs[this.index];
    }

}
