package _List;

import haxe.lang.Function;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ListIterator<T> extends haxe.lang.HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param head
     */
    public static <T_c> void __hx_ctor__List_ListIterator(_List.ListIterator<T_c> __hx_this, _List.ListNode<T_c> head) {
        __hx_this.head = head;
    }

    /**
     *
     */
    public _List.ListNode<T> head;

    /**
     *
     * @param empty
     */
    public ListIterator(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param head
     */
    public ListIterator(_List.ListNode<T> head) {
        _List.ListIterator.__hx_ctor__List_ListIterator(this, head);
    }

    /**
     *
     * @return
     */
    public boolean hasNext() {
        return (this.head != null);
    }

    /**
     *
     * @return
     */
    public T next() {
        if (!hasNext()) {
            return null;
        }
        T val = this.head.item;
        this.head = this.head.next;
        return val;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_198_432: {
                    if (field.equals("head")) {
                        this.head = ((_List.ListNode<T>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw new RuntimeException();
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 3_377_907: {
                if (field.equals("next")) {
                    return new Function(-1, -1) {
                        @Override
                        public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
                            return ((ListIterator) __fn_dyn1).next();
                        }
                    };
                    //return ((haxe.lang.Function) (new haxe.lang.Closure(this, "next")));
                }

                break;
            }

            case 3_198_432: {
                if (field.equals("head")) {
                    return this.head;
                }

                break;
            }

            case 696_759_469: {
                if (field.equals("hasNext")) {
                    return new Function(-1, -1) {
                        @Override
                        public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
                            return ((ListIterator) __fn_dyn1).hasNext();
                        }
                    };
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw new RuntimeException();
        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_377_907: {
                    if (field.equals("next")) {
                        return this.next();
                    }

                    break;
                }

                case 696_759_469: {
                    if (field.equals("hasNext")) {
                        return this.hasNext();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw new RuntimeException();
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("head");
        super.__hx_getFields(baseArr);
    }

}
