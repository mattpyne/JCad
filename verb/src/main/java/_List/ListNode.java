package _List;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ListNode<T> extends haxe.lang.HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param item
     * @param next
     */
    public static <T_c> void __hx_ctor__List_ListNode(_List.ListNode<T_c> __hx_this, T_c item, _List.ListNode<T_c> next) {
        __hx_this.item = item;
        __hx_this.next = next;
    }

    /**
     *
     */
    public T item;

    /**
     *
     */
    public _List.ListNode<T> next;

    /**
     *
     * @param empty
     */
    public ListNode(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param item
     * @param next
     */
    public ListNode(T item, _List.ListNode<T> next) {
        _List.ListNode.__hx_ctor__List_ListNode(this, item, next);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_242_771: {
                    if (field.equals("item")) {
                        this.item = ((T) (((java.lang.Object) (value))));
                        return (haxe.lang.Runtime.toDouble(value));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_377_907: {
                    if (field.equals("next")) {
                        this.next = ((_List.ListNode<T>) (value));
                        return value;
                    }

                    break;
                }

                case 3_242_771: {
                    if (field.equals("item")) {
                        this.item = ((T) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_377_907: {
                    if (field.equals("next")) {
                        __temp_executeDef1 = false;
                        return this.next;
                    }

                    break;
                }

                case 3_242_771: {
                    if (field.equals("item")) {
                        __temp_executeDef1 = false;
                        return this.item;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_242_771: {
                    if (field.equals("item")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this.item));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("next");
        baseArr.push("item");
        super.__hx_getFields(baseArr);
    }

}
