package haxe._Unserializer;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class DefaultResolver extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe__Unserializer_DefaultResolver(haxe._Unserializer.DefaultResolver __hx_this) {
    }

    /**
     *
     * @param empty
     */
    public DefaultResolver(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public DefaultResolver() {
        haxe._Unserializer.DefaultResolver.__hx_ctor_haxe__Unserializer_DefaultResolver(this);
    }

    /**
     *
     * @param name
     * @return
     */
    public java.lang.Class resolveClass(java.lang.String name) {
        return haxe.root.Type.resolveClass(name);
    }

    /**
     *
     * @param name
     * @return
     */
    public java.lang.Class resolveEnum(java.lang.String name) {
        return haxe.root.Type.resolveEnum(name);
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_952_363_789: {
                    if (field.equals("resolveEnum")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "resolveEnum"));
                    }

                    break;
                }

                case 391_809_772: {
                    if (field.equals("resolveClass")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "resolveClass"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_952_363_789: {
                    if (field.equals("resolveEnum")) {
                        __temp_executeDef1 = false;
                        return this.resolveEnum(haxe.lang.Runtime.toString(dynargs.get(0)));
                    }

                    break;
                }

                case 391_809_772: {
                    if (field.equals("resolveClass")) {
                        __temp_executeDef1 = false;
                        return this.resolveClass(haxe.lang.Runtime.toString(dynargs.get(0)));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

}
