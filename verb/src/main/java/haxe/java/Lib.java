package haxe.java;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Lib extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_java_Lib(haxe.java.Lib __hx_this) {
    }

    /**
     *
     * @param _native
     * @return
     */
    public static haxe.root.Array<java.lang.String> array_String(java.lang.String[] _native) {
        return (haxe.root.Array.ofNative(_native));
    }

    /**
     *
     * @param empty
     */
    public Lib(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Lib() {
        haxe.java.Lib.__hx_ctor_haxe_java_Lib(this);
    }

}
