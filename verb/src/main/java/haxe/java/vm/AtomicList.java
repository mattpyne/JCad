package haxe.java.vm;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AtomicList<T> {

    /**
     *
     */
    public volatile haxe.java.vm.AtomicNode<T> head;

    /**
     *
     */
    public volatile java.util.concurrent.atomic.AtomicReference<haxe.java.vm.AtomicNode<T>> tail;

    /**
     *
     */
    public AtomicList() {
        this.head = new haxe.java.vm.AtomicNode<>(null);
        this.head.set(((haxe.java.vm.AtomicNode<T>) (new haxe.java.vm.AtomicNode<T>(null))));
        this.tail = new java.util.concurrent.atomic.AtomicReference<>(this.head);
    }

    /**
     *
     * @param v
     */
    public void add(T v) {
        haxe.java.vm.AtomicNode<T> n = new haxe.java.vm.AtomicNode<>(v);
        java.util.concurrent.atomic.AtomicReference<haxe.java.vm.AtomicNode<T>> tail = this.tail;
        haxe.java.vm.AtomicNode<T> p = null;
        while (true) {
            p = tail.get();
            if (!((!(p.compareAndSet((null), (n)))))) {
                break;
            }

            tail.compareAndSet((p), (p.get()));
        }

        tail.compareAndSet((p), (n));
    }

    /**
     *
     * @return
     */
    public java.lang.Object pop() {
        haxe.java.vm.AtomicNode<T> p = null;
        haxe.java.vm.AtomicNode<T> pget = null;
        haxe.java.vm.AtomicNode<T> head = this.head;
        while (true) {
            p = head.get();
            pget = p.get();
            if ((pget == null)) {
                return null;
            }

            if (!((!(head.compareAndSet((p), (pget)))))) {
                break;
            }

        }

        T ret = pget.value;
        pget.value = null;
        return ret;
    }

    /**
     *
     * @return
     */
    public T peekLast() {
        return this.tail.get().value;
    }

}
