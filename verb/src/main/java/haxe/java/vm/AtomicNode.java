package haxe.java.vm;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AtomicNode<T> extends java.util.concurrent.atomic.AtomicReference<haxe.java.vm.AtomicNode<T>> {

    /**
     *
     */
    public T value;

    /**
     *
     * @param value
     */
    public AtomicNode(T value) {
        super();
        this.value = value;
    }

}
