package haxe.root;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Reflect extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor__Reflect(haxe.root.Reflect __hx_this) {
    }

    /**
     *
     * @param o
     * @param field
     * @return
     */
    public static boolean hasField(java.lang.Object o, java.lang.String field) {
        if ((o instanceof haxe.lang.IHxObject)) {
            return (!(haxe.lang.Runtime.eq(((haxe.lang.IHxObject) (o)).__hx_getField(field, false, true, false), haxe.lang.Runtime.undefined)));
        }

        return haxe.lang.Runtime.slowHasField(o, field);
    }

    /**
     *
     * @param o
     * @param field
     * @return
     */
    public static java.lang.Object field(java.lang.Object o, java.lang.String field) {
        if ((o instanceof haxe.lang.IHxObject)) {
            return ((haxe.lang.IHxObject) (o)).__hx_getField(field, false, false, false);
        }

        return haxe.lang.Runtime.slowGetField(o, field, false);
    }

    /**
     *
     * @param o
     * @param field
     * @param value
     */
    public static void setField(java.lang.Object o, java.lang.String field, java.lang.Object value) {
        if ((o instanceof haxe.lang.IHxObject)) {
            ((haxe.lang.IHxObject) (o)).__hx_setField(field, value, false);
        } else {
            haxe.lang.Runtime.slowSetField(o, field, value);
        }

    }

    /**
     *
     * @param o
     * @param func
     * @param args
     * @return
     */
    public static java.lang.Object callMethod(java.lang.Object o, java.lang.Object func, haxe.root.Array args) {
        return ((haxe.lang.Function) (func)).__hx_invokeDynamic(args);
    }

    /**
     *
     * @param o
     * @return
     */
    public static haxe.root.Array<java.lang.String> fields(java.lang.Object o) {
        if ((o instanceof haxe.lang.IHxObject)) {
            haxe.root.Array<java.lang.String> ret = new haxe.root.Array<>(new java.lang.String[]{});
            ((haxe.lang.IHxObject) (o)).__hx_getFields(ret);
            return ret;
        } else {
            if ((o instanceof java.lang.Class)) {
                return haxe.root.Type.getClassFields(((java.lang.Class) (o)));
            } else {
                return new haxe.root.Array<>(new java.lang.String[]{});
            }

        }

    }

    /**
     *
     * @param empty
     */
    public Reflect(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Reflect() {
        haxe.root.Reflect.__hx_ctor__Reflect(this);
    }

}
