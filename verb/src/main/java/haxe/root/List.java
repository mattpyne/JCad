package haxe.root;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class List<T> extends haxe.lang.HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     */
    public static <T_c> void __hx_ctor__List(haxe.root.List<T_c> __hx_this) {
        __hx_this.length = 0;
    }

    /**
     *
     */
    public _List.ListNode<T> h;

    /**
     *
     */
    public _List.ListNode<T> q;

    /**
     *
     */
    public int length;

    /**
     *
     * @param empty
     */
    public List(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public List() {
        haxe.root.List.__hx_ctor__List(this);
    }

    /**
     *
     * @param item
     */
    public void add(T item) {
        _List.ListNode<T> x = new _List.ListNode<>(item, null);
        if ((this.h == null)) {
            this.h = x;
        } else {
            this.q.next = x;
        }

        this.q = x;
        this.length++;
    }

    /**
     *
     * @return
     */
    public _List.ListIterator<T> iterator() {
        return new _List.ListIterator<>((this.h));
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        this.length = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        this.length = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 104: {
                    if (field.equals("h")) {
                        __temp_executeDef1 = false;
                        this.h = ((_List.ListNode<T>) (value));
                        return value;
                    }

                    break;
                }

                case 113: {
                    if (field.equals("q")) {
                        __temp_executeDef1 = false;
                        this.q = ((_List.ListNode<T>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_182_533_742: {
                    if (field.equals("iterator")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "iterator"));
                    }

                    break;
                }

                case 104: {
                    if (field.equals("h")) {
                        __temp_executeDef1 = false;
                        return this.h;
                    }

                    break;
                }

                case 96_417: {
                    if (field.equals("add")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "add"));
                    }

                    break;
                }

                case 113: {
                    if (field.equals("q")) {
                        __temp_executeDef1 = false;
                        return this.q;
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return this.length;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return (this.length);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_182_533_742: {
                    if (field.equals("iterator")) {
                        __temp_executeDef1 = false;
                        return this.iterator();
                    }

                    break;
                }

                case 96_417: {
                    if (field.equals("add")) {
                        __temp_executeDef1 = false;
                        this.add(((T) (dynargs.get(0))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("length");
        baseArr.push("q");
        baseArr.push("h");
        super.__hx_getFields(baseArr);
    }

}
