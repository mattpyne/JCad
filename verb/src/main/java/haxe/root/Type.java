package haxe.root;

import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Type extends haxe.lang.HxObject {

    /**
     *
     */
    protected static final java.lang.Class[] __createEmptyInstance_EMPTY_TYPES = new java.lang.Class[]{((java.lang.Class) (((java.lang.Class) (haxe.lang.EmptyObject.class))))};

    /**
     *
     */
    protected static final haxe.lang.EmptyObject[] __createEmptyInstance_EMPTY_ARGS = new haxe.lang.EmptyObject[]{((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY))};

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor__Type(haxe.root.Type __hx_this) {
    }

    /**
     *
     * @param o
     * @return
     */
    public static java.lang.Class getEnum(java.lang.Object o) {
        if (((o instanceof java.lang.Enum) || (o instanceof haxe.lang.Enum))) {
            return o.getClass();
        }

        return null;
    }

    /**
     *
     * @param c
     * @return
     */
    public static java.lang.String getClassName(java.lang.Class c) {
        java.lang.Class c1 = (c);
        java.lang.String name = c1.getName();
        if (name.startsWith("haxe.root.")) {
            return haxe.lang.StringExt.substr(name, 10, null);
        }

        if (name.startsWith("java.lang")) {
            name = haxe.lang.StringExt.substr(name, 10, null);
        }

        {
            java.lang.String __temp_svar1 = (name);
            int __temp_hash3 = __temp_svar1.hashCode();
            boolean __temp_executeDef2 = true;
            switch (__temp_hash3) {
                case -1_325_958_191:
                case 2_052_876_273: {
                    if (((((__temp_hash3 == -1_325_958_191) && __temp_svar1.equals("double"))) || __temp_svar1.equals("Double"))) {
                        __temp_executeDef2 = false;
                        return "Float";
                    }

                    break;
                }

                case -1_939_501_217: {
                    if (__temp_svar1.equals("Object")) {
                        __temp_executeDef2 = false;
                        return "Dynamic";
                    }

                    break;
                }

                case 104_431:
                case -672_261_858: {
                    if (((((__temp_hash3 == 104_431) && __temp_svar1.equals("int"))) || __temp_svar1.equals("Integer"))) {
                        __temp_executeDef2 = false;
                        return "Int";
                    }

                    break;
                }

            }

            if (__temp_executeDef2) {
                return name;
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param e
     * @return
     */
    public static java.lang.String getEnumName(java.lang.Class e) {
        java.lang.Class c = (e);
        java.lang.String ret = c.getName();
        if (ret.startsWith("haxe.root.")) {
            return haxe.lang.StringExt.substr(ret, 10, null);
        } else {
            if ((haxe.lang.Runtime.valEq(ret, "boolean") || haxe.lang.Runtime.valEq(ret, "java.lang.Boolean"))) {
                return "Bool";
            }

        }

        return ret;
    }

    /**
     *
     * @param name
     * @return
     */
    public static java.lang.Class resolveClass(java.lang.String name) {
        try {
            if ((haxe.lang.StringExt.indexOf(name, ".", null) == -1)) {
                name = ("haxe.root." + name);
            }

            return (java.lang.Class.forName(haxe.lang.Runtime.toString(name)));
        } catch (java.lang.ClassNotFoundException e) {
            haxe.lang.Exceptions.setException(e);
            {
                java.lang.String __temp_svar1 = (name);
                boolean __temp_executeDef2 = true;
                switch (__temp_svar1.hashCode()) {
                    case 360_541_844: {
                        if (__temp_svar1.equals("haxe.root.Class")) {
                            __temp_executeDef2 = false;
                            return java.lang.Class.class;
                        }

                        break;
                    }

                    case -1_242_153_355: {
                        if (__temp_svar1.equals("haxe.root.String")) {
                            __temp_executeDef2 = false;
                            return java.lang.String.class;
                        }

                        break;
                    }

                    case -140_489_125: {
                        if (__temp_svar1.equals("haxe.root.Dynamic")) {
                            __temp_executeDef2 = false;
                            return java.lang.Object.class;
                        }

                        break;
                    }

                    case 704_654_956: {
                        if (__temp_svar1.equals("haxe.root.Math")) {
                            __temp_executeDef2 = false;
                            return java.lang.Math.class;
                        }

                        break;
                    }

                    case 363_325_304: {
                        if (__temp_svar1.equals("haxe.root.Float")) {
                            __temp_executeDef2 = false;
                            return double.class;
                        }

                        break;
                    }

                    case -1_778_387_957: {
                        if (__temp_svar1.equals("haxe.root.Int")) {
                            __temp_executeDef2 = false;
                            return int.class;
                        }

                        break;
                    }

                }

                if (__temp_executeDef2) {
                    return null;
                } else {
                    throw null;
                }

            }

        }

    }

    /**
     *
     * @param name
     * @return
     */
    public static java.lang.Class resolveEnum(java.lang.String name) {

        if ("Bool".equals(name)) {
            return boolean.class;
        }
        Class r = resolveClass(name);
        if (r != null && (r.getSuperclass() == java.lang.Enum.class || haxe.lang.Enum.class.isAssignableFrom(r))) {
            return r;
        }
        return null;

    }

    /**
     *
     * @param <T>
     * @param cl
     * @return
     */
    public static <T> T createEmptyInstance(java.lang.Class cl) {
        try {
            java.lang.Class t = (cl);
            try {
                java.lang.reflect.Constructor<T> ctor = (t.getConstructor(haxe.root.Type.__createEmptyInstance_EMPTY_TYPES));
                return ctor.newInstance(((java.lang.Object[]) (haxe.root.Type.__createEmptyInstance_EMPTY_ARGS)));
            } catch (java.lang.NoSuchMethodException _1) {
                haxe.lang.Exceptions.setException(_1);
                return ((T) (t.newInstance()));
            }

        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | SecurityException | InvocationTargetException typedException) {
            throw haxe.lang.HaxeException.wrap(typedException);
        }

    }

    /**
     *
     * @param <T>
     * @param e
     * @param constr
     * @param params
     * @return
     */
    public static <T> T createEnum(java.lang.Class e, java.lang.String constr, haxe.root.Array params) {

        if (params == null || params.length == 0) {
            java.lang.Object ret = haxe.lang.Runtime.slowGetField(e, constr, true);
            if (ret instanceof haxe.lang.Function) {
                throw haxe.lang.HaxeException.wrap("Constructor " + constr + " needs parameters");
            }
            return (T) ret;
        } else {
            return (T) haxe.lang.Runtime.slowCallField(e, constr, params);
        }

    }

    /**
     *
     * @param c
     * @return
     */
    public static haxe.root.Array<java.lang.String> getClassFields(java.lang.Class c) {

        Array<String> ret = new Array<>();
        if (c == java.lang.String.class) {
            ret.push("fromCharCode");
            return ret;
        }

        for (java.lang.reflect.Field f : c.getDeclaredFields()) {
            java.lang.String fname = f.getName();
            if (java.lang.reflect.Modifier.isStatic(f.getModifiers()) && !fname.startsWith("__hx_")) {
                ret.push(fname);
            }
        }

        for (java.lang.reflect.Method m : c.getDeclaredMethods()) {
            if (m.getDeclaringClass() == java.lang.Object.class) {
                continue;
            }
            java.lang.String mname = m.getName();
            if (java.lang.reflect.Modifier.isStatic(m.getModifiers()) && !mname.startsWith("__hx_")) {
                ret.push(mname);
            }
        }

        return ret;

    }

    /**
     *
     * @param e
     * @return
     */
    public static haxe.root.Array<java.lang.String> getEnumConstructs(java.lang.Class e) {
        if (haxe.root.Reflect.hasField(e, "__hx_constructs")) {
            haxe.root.Array<java.lang.String> ret = haxe.java.Lib.array_String(((java.lang.String[]) (haxe.lang.Runtime.getField(e, "__hx_constructs", true))));
            return ret.copy();
        }

        java.lang.Enum[] vals = ((java.lang.Enum[]) (haxe.lang.Runtime.callField(e, "values", null)));
        haxe.root.Array<java.lang.String> ret1 = new haxe.root.Array<>(new java.lang.String[]{});
        {
            int _g1 = 0;
            int _g = vals.length;
            while ((_g1 < _g)) {
                int i = _g1++;
                ret1.set(i, vals[i].name());
            }

        }

        return ret1;
    }

    /**
     *
     * @param v
     * @return
     */
    public static haxe.root.ValueType typeof(java.lang.Object v) {

        if (v == null) {
            return ValueType.TNull;
        }

        if (v instanceof haxe.lang.IHxObject) {
            haxe.lang.IHxObject vobj = (haxe.lang.IHxObject) v;
            java.lang.Class cl = vobj.getClass();
            if (v instanceof haxe.lang.DynamicObject) {
                return ValueType.TObject;
            } else {
                return ValueType.TClass(cl);
            }
        } else if (v instanceof java.lang.Number) {
            java.lang.Number n = (java.lang.Number) v;
            if (n.intValue() == n.doubleValue()) {
                return ValueType.TInt;
            } else {
                return ValueType.TFloat;
            }
        } else if (v instanceof haxe.lang.Function) {
            return ValueType.TFunction;
        } else if (v instanceof java.lang.Enum || v instanceof haxe.lang.Enum) {
            return ValueType.TEnum(v.getClass());
        } else if (v instanceof java.lang.Boolean) {
            return ValueType.TBool;
        } else if (v instanceof java.lang.Class) {
            return ValueType.TObject;
        } else {
            return ValueType.TClass(v.getClass());
        }

    }

    /**
     *
     * @param <T>
     * @param a
     * @param b
     * @return
     */
    public static <T> boolean enumEq(T a, T b) {

        if (a instanceof haxe.lang.Enum) {
            return a.equals(b);
        } else {
            return haxe.lang.Runtime.eq(a, b);
        }

    }

    /**
     *
     * @param e
     * @return
     */
    public static java.lang.String enumConstructor(java.lang.Object e) {

        if (e instanceof java.lang.Enum) {
            return ((java.lang.Enum) e).name();
        } else {
            return ((haxe.lang.Enum) e).getTag();
        }

    }

    /**
     *
     * @param e
     * @return
     */
    public static haxe.root.Array enumParameters(java.lang.Object e) {

        return (e instanceof java.lang.Enum) ? new haxe.root.Array() : ((haxe.lang.Enum) e).getParams();

    }

    /**
     *
     * @param e
     * @return
     */
    public static int enumIndex(java.lang.Object e) {

        if (e instanceof java.lang.Enum) {
            return ((java.lang.Enum) e).ordinal();
        } else {
            return ((haxe.lang.Enum) e).index;
        }

    }

    /**
     *
     * @param empty
     */
    public Type(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Type() {
        haxe.root.Type.__hx_ctor__Type(this);
    }

}
