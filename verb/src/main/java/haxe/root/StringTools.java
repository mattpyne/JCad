package haxe.root;

import java.io.UnsupportedEncodingException;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class StringTools extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor__StringTools(haxe.root.StringTools __hx_this) {
    }

    /**
     *
     * @param s
     * @return
     */
    public static java.lang.String urlEncode(java.lang.String s) {
        try {
            return haxe.root.StringTools.postProcessUrlEncode(java.net.URLEncoder.encode(haxe.lang.Runtime.toString(s), haxe.lang.Runtime.toString("UTF-8")));
        } catch (UnsupportedEncodingException typedException) {
            throw haxe.lang.HaxeException.wrap(typedException);
        }

    }

    /**
     *
     * @param s
     * @return
     */
    public static java.lang.String postProcessUrlEncode(java.lang.String s) {
        haxe.root.StringBuf ret = new haxe.root.StringBuf();
        int i = 0;
        int len = s.length();
        while ((i < len)) {
            char _g = s.charAt(i++);
            {
                char __temp_switch1 = (_g);
                if ((__temp_switch1 == 37)) {
                    if ((i <= (len - 2))) {
                        char c1 = s.charAt(i++);
                        char c2 = s.charAt(i++);
                        {
                            char __temp_switch2 = (c1);
                            if ((__temp_switch2 == 50)) {
                                {
                                    char __temp_switch4 = (c2);
                                    if ((__temp_switch4 == 49)) {
                                        ret.addChar(33);
                                    } else {
                                        if ((__temp_switch4 == 55)) {
                                            ret.addChar(39);
                                        } else {
                                            if ((__temp_switch4 == 56)) {
                                                ret.addChar(40);
                                            } else {
                                                if ((__temp_switch4 == 57)) {
                                                    ret.addChar(41);
                                                } else {
                                                    ret.addChar(37);
                                                    ret.addChar((c1));
                                                    ret.addChar((c2));
                                                }

                                            }

                                        }

                                    }

                                }

                            } else {
                                if ((__temp_switch2 == 55)) {
                                    {
                                        char __temp_switch3 = (c2);
                                        if (((__temp_switch3 == 69) || (__temp_switch3 == 101))) {
                                            ret.addChar(126);
                                        } else {
                                            ret.addChar(37);
                                            ret.addChar((c1));
                                            ret.addChar((c2));
                                        }

                                    }

                                } else {
                                    ret.addChar(37);
                                    ret.addChar((c1));
                                    ret.addChar((c2));
                                }

                            }

                        }

                    } else {
                        char chr = _g;
                        ret.addChar((chr));
                    }

                } else {
                    if ((__temp_switch1 == 43)) {
                        ret.add(haxe.lang.Runtime.toString("%20"));
                    } else {
                        char chr1 = _g;
                        ret.addChar((chr1));
                    }

                }

            }

        }

        return ret.toString();
    }

    /**
     *
     * @param s
     * @return
     */
    public static java.lang.String urlDecode(java.lang.String s) {
        try {
            return java.net.URLDecoder.decode(s, "UTF-8");
        } catch (java.lang.Throwable __temp_catchallException1) {
            haxe.lang.Exceptions.setException(__temp_catchallException1);
            java.lang.Object __temp_catchall2 = __temp_catchallException1;
            if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
            }

            {
                java.lang.Object e = __temp_catchall2;
                throw haxe.lang.HaxeException.wrap(e);
            }

        }

    }

    /**
     *
     * @param s
     * @param pos
     * @return
     */
    public static boolean isSpace(java.lang.String s, int pos) {
        java.lang.Object c = haxe.lang.StringExt.charCodeAt(s, pos);
        if (!((((haxe.lang.Runtime.compare(c, 8) > 0) && (haxe.lang.Runtime.compare(c, 14) < 0))))) {
            return haxe.lang.Runtime.eq(c, 32);
        } else {
            return true;
        }

    }

    /**
     *
     * @param s
     * @return
     */
    public static java.lang.String ltrim(java.lang.String s) {
        int l = s.length();
        int r = 0;
        while (((r < l) && haxe.root.StringTools.isSpace(s, r))) {
            ++r;
        }

        if ((r > 0)) {
            return haxe.lang.StringExt.substr(s, r, (l - r));
        } else {
            return s;
        }

    }

    /**
     *
     * @param empty
     */
    public StringTools(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public StringTools() {
        haxe.root.StringTools.__hx_ctor__StringTools(this);
    }

}
