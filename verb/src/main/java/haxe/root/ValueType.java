package haxe.root;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ValueType extends haxe.lang.ParamEnum {

    /**
     *
     */
    public static final java.lang.String[] __hx_constructs = new java.lang.String[]{"TNull", "TInt", "TFloat", "TBool", "TObject", "TFunction", "TClass", "TEnum", "TUnknown"};

    /**
     *
     */
    public static final haxe.root.ValueType TNull = new haxe.root.ValueType(0, null);

    /**
     *
     */
    public static final haxe.root.ValueType TInt = new haxe.root.ValueType(1, null);

    /**
     *
     */
    public static final haxe.root.ValueType TFloat = new haxe.root.ValueType(2, null);

    /**
     *
     */
    public static final haxe.root.ValueType TBool = new haxe.root.ValueType(3, null);

    /**
     *
     */
    public static final haxe.root.ValueType TObject = new haxe.root.ValueType(4, null);

    /**
     *
     */
    public static final haxe.root.ValueType TFunction = new haxe.root.ValueType(5, null);

    /**
     *
     */
    public static final haxe.root.ValueType TUnknown = new haxe.root.ValueType(8, null);

    /**
     *
     * @param c
     * @return
     */
    public static haxe.root.ValueType TClass(java.lang.Class c) {
        return new haxe.root.ValueType(6, new java.lang.Object[]{c});
    }

    /**
     *
     * @param e
     * @return
     */
    public static haxe.root.ValueType TEnum(java.lang.Class e) {
        return new haxe.root.ValueType(7, new java.lang.Object[]{e});
    }

    /**
     *
     * @param index
     * @param params
     */
    public ValueType(int index, java.lang.Object[] params) {
        super(index, params);
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.String getTag() {
        return haxe.root.ValueType.__hx_constructs[this.index];
    }

}
