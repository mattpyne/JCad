package haxe.root;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class StringBuf extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor__StringBuf(StringBuf __hx_this) {
        __hx_this.b = new StringBuilder();
    }

    /**
     *
     */
    public StringBuilder b;

    /**
     *
     * @param empty
     */
    public StringBuf(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public StringBuf() {
        StringBuf.__hx_ctor__StringBuf(this);
    }

    /**
     *
     * @param <T>
     * @param x
     */
    public <T> void add(T x) {
        if (haxe.lang.Runtime.isInt(x)) {
            int x1 = (haxe.lang.Runtime.toInt(x));
            Object xd = x1;
            this.b.append((xd));
        } else {
            this.b.append((x));
        }

    }

    /**
     *
     * @param c
     */
    public void addChar(int c) {
        this.b.append(((char) (c)));
    }

    @Override
    public String toString() {
        return this.b.toString();
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 98: {
                    if (field.equals("b")) {
                        __temp_executeDef1 = false;
                        this.b = ((StringBuilder) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_776_922_004: {
                    if (field.equals("toString")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "toString"));
                    }

                    break;
                }

                case 98: {
                    if (field.equals("b")) {
                        __temp_executeDef1 = false;
                        return this.b;
                    }

                    break;
                }

                case -1_149_089_897: {
                    if (field.equals("addChar")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "addChar"));
                    }

                    break;
                }

                case 96_417: {
                    if (field.equals("add")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "add"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_776_922_004: {
                    if (field.equals("toString")) {
                        __temp_executeDef1 = false;
                        return this.toString();
                    }

                    break;
                }

                case 96_417: {
                    if (field.equals("add")) {
                        __temp_executeDef1 = false;
                        this.add((dynargs.get(0)));
                        return (null);
                    }

                    break;
                }

                case -1_149_089_897: {
                    if (field.equals("addChar")) {
                        __temp_executeDef1 = false;
                        this.addChar((haxe.lang.Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("length");
        baseArr.push("b");
        super.__hx_getFields(baseArr);
    }

}
