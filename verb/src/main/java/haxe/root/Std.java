package haxe.root;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Std {

    /**
     *
     * @param v
     * @param t
     * @return
     */
    public static boolean is(java.lang.Object v, java.lang.Object t) {
        if ((v == null)) {
            return haxe.lang.Runtime.eq(t, java.lang.Object.class);
        }

        if ((t == null)) {
            return false;
        }

        java.lang.Class clt = ((java.lang.Class) (t));
        if ((((java.lang.Object) (clt)) == ((java.lang.Object) (null)))) {
            return false;
        }

        java.lang.String name = clt.getName();
        {
            java.lang.String __temp_svar1 = (name);
            int __temp_hash3 = __temp_svar1.hashCode();
            switch (__temp_hash3) {
                case 344_809_556:
                case 64_711_720: {
                    if (((((__temp_hash3 == 344_809_556) && __temp_svar1.equals("java.lang.Boolean"))) || __temp_svar1.equals("boolean"))) {
                        return v instanceof java.lang.Boolean;
                    }

                    break;
                }

                case 1_063_877_011: {
                    if (__temp_svar1.equals("java.lang.Object")) {
                        return true;
                    }

                    break;
                }

                case 761_287_205:
                case -1_325_958_191: {
                    if (((((__temp_hash3 == 761_287_205) && __temp_svar1.equals("java.lang.Double"))) || __temp_svar1.equals("double"))) {
                        return haxe.lang.Runtime.isDouble(v);
                    }

                    break;
                }

                case -2_056_817_302:
                case 104_431: {
                    if (((((__temp_hash3 == -2_056_817_302) && __temp_svar1.equals("java.lang.Integer"))) || __temp_svar1.equals("int"))) {
                        return haxe.lang.Runtime.isInt(v);
                    }

                    break;
                }

            }

        }

        java.lang.Class clv = v.getClass();
        return clt.isAssignableFrom((clv));
    }

    /**
     *
     * @param s
     * @return
     */
    public static java.lang.String string(java.lang.Object s) {
        return (haxe.lang.Runtime.toString(s) + "");
    }

    /**
     *
     * @param x
     * @return
     */
    public static int _int(double x) {
        return ((int) (x));
    }

    /**
     *
     * @param x
     * @return
     */
    public static java.lang.Object parseInt(java.lang.String x) {

        if (x == null) {
            return null;
        }

        int ret = 0;
        int base = 10;
        int i = 0;
        int len = x.length();

        if (x.startsWith("0") && len > 2) {
            char c = x.charAt(1);
            if (c == 'x' || c == 'X') {
                i = 2;
                base = 16;
            }
        }

        boolean foundAny = i != 0;
        boolean isNeg = false;
        for (; i < len; i++) {
            char c = x.charAt(i);
            if (!foundAny) {
                switch (c) {
                    case '-':
                        isNeg = true;
                        continue;
                    case '+':
                    case '\n':
                    case '\t':
                    case '\r':
                    case ' ':
                        if (isNeg) {
                            return null;
                        }
                        continue;
                }
            }

            if (c >= '0' && c <= '9') {
                if (!foundAny && c == '0') {
                    foundAny = true;
                    continue;
                }
                ret *= base;
                foundAny = true;

                ret += (c - '0');
            } else if (base == 16) {
                if (c >= 'a' && c <= 'f') {
                    ret *= base;
                    foundAny = true;
                    ret += (c - 'a') + 10;
                } else if (c >= 'A' && c <= 'F') {
                    ret *= base;
                    foundAny = true;
                    ret += (c - 'A') + 10;
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        if (foundAny) {
            return isNeg ? -ret : ret;
        } else {
            return null;
        }

    }

    /**
     *
     * @param x
     * @return
     */
    public static double parseFloat(java.lang.String x) {
        if ((x == null)) {
            return java.lang.Double.NaN;
        }

        x = haxe.root.StringTools.ltrim(x);
        boolean found = false;
        boolean hasDot = false;
        boolean hasSign = false;
        boolean hasE = false;
        boolean hasESign = false;
        boolean hasEData = false;
        int i = -1;
        {
            label1:
            while ((++i < x.length())) {
                int chr = (((char) (x.charAt(i))));
                if (((chr >= 48) && (chr <= 57))) {
                    if (hasE) {
                        hasEData = true;
                    }

                    found = true;
                } else {
                    switch (chr) {
                        case 43:
                        case 45: {
                            if ((!(found) && !(hasSign))) {
                                hasSign = true;
                            } else {
                                if ((((found && !(hasESign)) && hasE) && !(hasEData))) {
                                    hasESign = true;
                                } else {
                                    break label1;
                                }

                            }

                            break;
                        }

                        case 46: {
                            if (!(hasDot)) {
                                hasDot = true;
                            } else {
                                break label1;
                            }

                            break;
                        }

                        case 69:
                        case 101: {
                            if (!(hasE)) {
                                hasE = true;
                            } else {
                                break label1;
                            }

                            break;
                        }

                        default: {
                            break label1;
                        }

                    }

                }

            }

        }

        if ((hasE && !(hasEData))) {
            --i;
            if (hasESign) {
                --i;
            }

        }

        if ((i != x.length())) {
            x = haxe.lang.StringExt.substr(x, 0, i);
        }

        try {
            return java.lang.Double.parseDouble(haxe.lang.Runtime.toString(x));
        } catch (Throwable __temp_catchallException1) {
            haxe.lang.Exceptions.setException(__temp_catchallException1);
            java.lang.Object __temp_catchall2 = __temp_catchallException1;
            if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
            }

            {
                java.lang.Object e = __temp_catchall2;
                return java.lang.Double.NaN;
            }

        }

    }

    /**
     *
     */
    private Std() {
    }

}
