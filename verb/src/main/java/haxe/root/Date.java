package haxe.root;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Date extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param min
     * @param sec
     */
    public static void __hx_ctor__Date(haxe.root.Date __hx_this, int year, int month, int day, int hour, int min, int sec) {
        if ((year != 0)) {
            year -= 1_900;
        } else {
            year = 0;
        }

        __hx_this.date = new java.util.Date(year, month, day, hour, min, sec);
    }

    /**
     *
     * @return
     */
    public static haxe.root.Date now() {
        haxe.root.Date d = new haxe.root.Date(0, 0, 0, 0, 0, 0);
        d.date = new java.util.Date();
        return d;
    }

    /**
     *
     * @param t
     * @return
     */
    public static haxe.root.Date fromTime(double t) {
        haxe.root.Date d = new haxe.root.Date(0, 0, 0, 0, 0, 0);
        d.date = new java.util.Date(((long) (t)));
        return d;
    }

    /**
     *
     * @param s
     * @return
     */
    public static haxe.root.Date fromString(java.lang.String s) {
        int _g = s.length();
        switch (_g) {
            case 8: {
                haxe.root.Array<java.lang.String> k = haxe.lang.StringExt.split(s, ":");
                haxe.root.Date d = new haxe.root.Date(0, 0, 0, (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(k.get(0)))), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(k.get(1)))), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(k.get(2)))));
                return d;
            }

            case 10: {
                haxe.root.Array<java.lang.String> k1 = haxe.lang.StringExt.split(s, "-");
                return new haxe.root.Date((haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(k1.get(0)))), ((haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(k1.get(1)))) - (1)), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(k1.get(2)))), 0, 0, 0);
            }

            case 19: {
                haxe.root.Array<java.lang.String> k2 = haxe.lang.StringExt.split(s, " ");
                haxe.root.Array<java.lang.String> y = haxe.lang.StringExt.split(k2.get(0), "-");
                haxe.root.Array<java.lang.String> t = haxe.lang.StringExt.split(k2.get(1), ":");
                return new haxe.root.Date((haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(y.get(0)))), ((haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(y.get(1)))) - (1)), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(y.get(2)))), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(t.get(0)))), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(t.get(1)))), (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(t.get(2)))));
            }

            default: {
                throw haxe.lang.HaxeException.wrap(("Invalid date format : " + s));
            }

        }

    }

    /**
     *
     */
    public java.util.Date date;

    /**
     *
     * @param empty
     */
    public Date(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param min
     * @param sec
     */
    public Date(int year, int month, int day, int hour, int min, int sec) {
        haxe.root.Date.__hx_ctor__Date(this, year, month, day, hour, min, sec);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_076_014: {
                    if (field.equals("date")) {
                        __temp_executeDef1 = false;
                        this.date = ((java.util.Date) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_076_014: {
                    if (field.equals("date")) {
                        __temp_executeDef1 = false;
                        return this.date;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("date");
        super.__hx_getFields(baseArr);
    }

}
