package haxe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Serializer extends haxe.lang.HxObject {

    /**
     *
     */
    public static boolean USE_CACHE;

    /**
     *
     */
    public static boolean USE_ENUM_INDEX;

    /**
     *
     */
    public static java.lang.String BASE64;

    /**
     *
     */
    public static java.lang.Object[] BASE64_CODES;

    static {
        haxe.Serializer.USE_CACHE = false;
        haxe.Serializer.USE_ENUM_INDEX = false;
        haxe.Serializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
        haxe.Serializer.BASE64_CODES = null;
    }

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_Serializer(haxe.Serializer __hx_this) {
        __hx_this.buf = new haxe.root.StringBuf();
        __hx_this.cache = new haxe.root.Array();
        __hx_this.useCache = haxe.Serializer.USE_CACHE;
        __hx_this.useEnumIndex = haxe.Serializer.USE_ENUM_INDEX;
        __hx_this.shash = new haxe.ds.StringMap<>();
        __hx_this.scount = 0;
    }

    /**
     *
     */
    public haxe.root.StringBuf buf;

    /**
     *
     */
    public haxe.root.Array cache;

    /**
     *
     */
    public haxe.ds.StringMap<java.lang.Object> shash;

    /**
     *
     */
    public int scount;

    /**
     *
     */
    public boolean useCache;

    /**
     *
     */
    public boolean useEnumIndex;

    /**
     *
     * @param empty
     */
    public Serializer(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Serializer() {
        haxe.Serializer.__hx_ctor_haxe_Serializer(this);
    }

    @Override
    public java.lang.String toString() {
        return this.buf.toString();
    }

    /**
     *
     * @param s
     */
    public void serializeString(java.lang.String s) {
        java.lang.Object x = this.shash.get(s);
        if ((!(haxe.lang.Runtime.eq(x, null)))) {
            this.buf.add(haxe.lang.Runtime.toString("R"));
            this.buf.add((x));
            return;
        }

        this.shash.set(s, this.scount++);
        this.buf.add(haxe.lang.Runtime.toString("y"));
        s = haxe.root.StringTools.urlEncode(s);
        this.buf.add(((java.lang.Object) (s.length())));
        this.buf.add(haxe.lang.Runtime.toString(":"));
        this.buf.add(haxe.lang.Runtime.toString(s));
    }

    /**
     *
     * @param v
     * @return
     */
    public boolean serializeRef(java.lang.Object v) {
        {
            int _g1 = 0;
            int _g = this.cache.length;
            while ((_g1 < _g)) {
                int i = _g1++;
                if (haxe.lang.Runtime.eq(this.cache.get(i), v)) {
                    this.buf.add(haxe.lang.Runtime.toString("r"));
                    this.buf.add(((java.lang.Object) (i)));
                    return true;
                }

            }

        }

        this.cache.push(v);
        return false;
    }

    /**
     *
     * @param v
     */
    public void serializeFields(java.lang.Object v) {
        {
            int _g = 0;
            haxe.root.Array<java.lang.String> _g1 = haxe.root.Reflect.fields(v);
            while ((_g < _g1.length)) {
                java.lang.String f = _g1.get(_g);
                ++_g;
                this.serializeString(f);
                this.serialize(haxe.root.Reflect.field(v, f));
            }

        }

        this.buf.add(haxe.lang.Runtime.toString("g"));
    }

    /**
     *
     * @param v
     */
    public void serialize(java.lang.Object v) {
        haxe.root.ValueType _g = haxe.root.Type.typeof(v);
        switch (_g.index) {
            case 0: {
                this.buf.add(haxe.lang.Runtime.toString("n"));
                break;
            }

            case 1: {
                int v1 = (haxe.lang.Runtime.toInt(v));
                if ((v1 == 0)) {
                    this.buf.add(haxe.lang.Runtime.toString("z"));
                    return;
                }

                this.buf.add(haxe.lang.Runtime.toString("i"));
                this.buf.add(((java.lang.Object) (v1)));
                break;
            }

            case 2: {
                double v2 = (haxe.lang.Runtime.toDouble(v));
                if (java.lang.Double.isNaN(v2)) {
                    this.buf.add(haxe.lang.Runtime.toString("k"));
                } else {
                    if (!(haxe.lang.Runtime.isFinite(v2))) {
                        this.buf.add(haxe.lang.Runtime.toString((((v2 < 0)) ? ("m") : ("p"))));
                    } else {
                        this.buf.add(haxe.lang.Runtime.toString("d"));
                        this.buf.add(((java.lang.Object) (v2)));
                    }

                }

                break;
            }

            case 3: {
                this.buf.add(haxe.lang.Runtime.toString(((haxe.lang.Runtime.toBool((v))) ? ("t") : ("f"))));
                break;
            }

            case 4: {
                if ((v instanceof java.lang.Class)) {
                    java.lang.String className = haxe.root.Type.getClassName(((java.lang.Class) (v)));
                    this.buf.add(haxe.lang.Runtime.toString("A"));
                    this.serializeString(className);
                } else {
                    if ((v instanceof java.lang.Class)) {
                        this.buf.add(haxe.lang.Runtime.toString("B"));
                        this.serializeString(haxe.root.Type.getEnumName(((java.lang.Class) (v))));
                    } else {
                        if ((this.useCache && this.serializeRef(v))) {
                            return;
                        }

                        this.buf.add(haxe.lang.Runtime.toString("o"));
                        this.serializeFields(v);
                    }

                }

                break;
            }

            case 5: {
                throw haxe.lang.HaxeException.wrap("Cannot serialize function");
            }

            case 6: {
                java.lang.Class c = ((java.lang.Class) (_g.params[0]));
                {
                    if ((((java.lang.Object) (c)) == ((java.lang.Object) (java.lang.String.class)))) {
                        this.serializeString(haxe.lang.Runtime.toString(v));
                        return;
                    }

                    if ((this.useCache && this.serializeRef(v))) {
                        return;
                    }

                    {
                        java.lang.Class __temp_switch1 = (c);
                        if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.root.Array.class)))) {
                            int ucount = 0;
                            this.buf.add(haxe.lang.Runtime.toString("a"));
                            int l = (haxe.lang.Runtime.toInt(haxe.lang.Runtime.getField(v, "length", true)));
                            {
                                int _g1 = 0;
                                int _g2 = l;
                                while ((_g1 < _g2)) {
                                    int i = _g1++;
                                    if ((((java.lang.Object) (haxe.lang.Runtime.callField(v, "__get", new haxe.root.Array(new java.lang.Object[]{i})))) == null)) {
                                        ++ucount;
                                    } else {
                                        if ((ucount > 0)) {
                                            if ((ucount == 1)) {
                                                this.buf.add(haxe.lang.Runtime.toString("n"));
                                            } else {
                                                this.buf.add(haxe.lang.Runtime.toString("u"));
                                                this.buf.add(((java.lang.Object) (ucount)));
                                            }

                                            ucount = 0;
                                        }

                                        this.serialize((haxe.lang.Runtime.callField(v, "__get", new haxe.root.Array(new java.lang.Object[]{i}))));
                                    }

                                }

                            }

                            if ((ucount > 0)) {
                                if ((ucount == 1)) {
                                    this.buf.add(haxe.lang.Runtime.toString("n"));
                                } else {
                                    this.buf.add(haxe.lang.Runtime.toString("u"));
                                    this.buf.add(((java.lang.Object) (ucount)));
                                }

                            }

                            this.buf.add(haxe.lang.Runtime.toString("h"));
                        } else {
                            if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.root.Date.class)))) {
                                haxe.root.Date d = ((haxe.root.Date) (v));
                                this.buf.add(haxe.lang.Runtime.toString("v"));
                                this.buf.add(((double) (d.date.getTime())));
                            } else {
                                if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.root.List.class)))) {
                                    this.buf.add(haxe.lang.Runtime.toString("l"));
                                    haxe.root.List v3 = ((haxe.root.List) (v));
                                    {
                                        _List.ListNode _g_head = v3.h;
                                        while ((_g_head != null)) {
                                            java.lang.Object val = _g_head.item;
                                            _g_head = _g_head.next;
                                            java.lang.Object i1 = (val);
                                            this.serialize(i1);
                                        }

                                    }

                                    this.buf.add(haxe.lang.Runtime.toString("h"));
                                } else {
                                    if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.ds.IntMap.class)))) {
                                        this.buf.add(haxe.lang.Runtime.toString("q"));
                                        haxe.ds.IntMap v4 = ((haxe.ds.IntMap) (v));
                                        {
                                            java.lang.Object k = v4.keys();
                                            while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(k, "hasNext", null))) {
                                                int k1 = (haxe.lang.Runtime.toInt(haxe.lang.Runtime.callField(k, "next", null)));
                                                this.buf.add(haxe.lang.Runtime.toString(":"));
                                                this.buf.add(((java.lang.Object) (k1)));
                                                this.serialize(v4.get(k1));
                                            }

                                        }

                                        this.buf.add(haxe.lang.Runtime.toString("h"));
                                    } else {
                                        if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.ds.ObjectMap.class)))) {
                                            this.buf.add(haxe.lang.Runtime.toString("M"));
                                            haxe.ds.ObjectMap v5 = ((haxe.ds.ObjectMap) (v));
                                            {
                                                java.lang.Object k2 = v5.keys();
                                                while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(k2, "hasNext", null))) {
                                                    java.lang.Object k3 = (haxe.lang.Runtime.callField(k2, "next", null));
                                                    this.serialize(k3);
                                                    this.serialize(v5.get(k3));
                                                }

                                            }

                                            this.buf.add(haxe.lang.Runtime.toString("h"));
                                        } else {
                                            if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.ds.StringMap.class)))) {
                                                this.buf.add(haxe.lang.Runtime.toString("b"));
                                                haxe.ds.StringMap v6 = ((haxe.ds.StringMap) (v));
                                                {
                                                    java.lang.Object k4 = v6.keys();
                                                    while (haxe.lang.Runtime.toBool(haxe.lang.Runtime.callField(k4, "hasNext", null))) {
                                                        java.lang.String k5 = haxe.lang.Runtime.toString(haxe.lang.Runtime.callField(k4, "next", null));
                                                        this.serializeString(k5);
                                                        this.serialize(v6.get(k5));
                                                    }

                                                }

                                                this.buf.add(haxe.lang.Runtime.toString("h"));
                                            } else {
                                                if ((((java.lang.Object) (__temp_switch1)) == ((java.lang.Object) (haxe.io.Bytes.class)))) {
                                                    haxe.io.Bytes v7 = ((haxe.io.Bytes) (v));
                                                    this.buf.add(haxe.lang.Runtime.toString("s"));
                                                    this.buf.add(((java.lang.Object) (((int) (java.lang.Math.ceil((((double) ((v7.length * 8))) / 6)))))));
                                                    this.buf.add(haxe.lang.Runtime.toString(":"));
                                                    int i2 = 0;
                                                    int max = (v7.length - 2);
                                                    java.lang.Object[] b64 = haxe.Serializer.BASE64_CODES;
                                                    if ((b64 == null)) {
                                                        java.lang.Object[] this1 = new java.lang.Object[haxe.Serializer.BASE64.length()];
                                                        b64 = (this1);
                                                        {
                                                            int _g11 = 0;
                                                            int _g3 = haxe.Serializer.BASE64.length();
                                                            while ((_g11 < _g3)) {
                                                                int i3 = _g11++;
                                                                ((java.lang.Object[]) (b64))[i3] = haxe.lang.StringExt.charCodeAt(haxe.Serializer.BASE64, i3);
                                                            }

                                                        }

                                                        haxe.Serializer.BASE64_CODES = b64;
                                                    }

                                                    while ((i2 < max)) {
                                                        int b1 = (v7.b[i2++] & 255);
                                                        int b2 = (v7.b[i2++] & 255);
                                                        int b3 = (v7.b[i2++] & 255);
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[(b1 >> 2)])));
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[((((b1 << 4) | (b2 >> 4))) & 63)])));
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[((((b2 << 2) | (b3 >> 6))) & 63)])));
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[(b3 & 63)])));
                                                    }

                                                    if ((i2 == max)) {
                                                        int b11 = (v7.b[i2++] & 255);
                                                        int b21 = (v7.b[i2++] & 255);
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[(b11 >> 2)])));
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[((((b11 << 4) | (b21 >> 4))) & 63)])));
                                                        this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[((b21 << 2) & 63)])));
                                                    } else {
                                                        if ((i2 == (max + 1))) {
                                                            int b12 = (v7.b[i2++] & 255);
                                                            this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[(b12 >> 2)])));
                                                            this.buf.addChar((haxe.lang.Runtime.toInt(((java.lang.Object[]) (b64))[((b12 << 4) & 63)])));
                                                        }

                                                    }

                                                } else {
                                                    if (this.useCache) {
                                                        this.cache.pop();
                                                    }

                                                    if (haxe.root.Reflect.hasField(v, "hxSerialize")) {
                                                        this.buf.add(haxe.lang.Runtime.toString("C"));
                                                        this.serializeString(haxe.root.Type.getClassName(c));
                                                        if (this.useCache) {
                                                            this.cache.push(v);
                                                        }

                                                        haxe.lang.Runtime.callField(v, "hxSerialize", new haxe.root.Array(new java.lang.Object[]{this}));
                                                        this.buf.add(haxe.lang.Runtime.toString("g"));
                                                    } else {
                                                        this.buf.add(haxe.lang.Runtime.toString("c"));
                                                        this.serializeString(haxe.root.Type.getClassName(c));
                                                        if (this.useCache) {
                                                            this.cache.push(v);
                                                        }

                                                        this.serializeFields(v);
                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

                break;
            }

            case 7: {
                java.lang.Class e = ((java.lang.Class) (_g.params[0]));
                {
                    if (this.useCache) {
                        if (this.serializeRef(v)) {
                            return;
                        }

                        this.cache.pop();
                    }

                    this.buf.add(haxe.lang.Runtime.toString(((this.useEnumIndex) ? ("j") : ("w"))));
                    this.serializeString(haxe.root.Type.getEnumName(e));
                    if (this.useEnumIndex) {
                        this.buf.add(haxe.lang.Runtime.toString(":"));
                        this.buf.add(((java.lang.Object) (haxe.root.Type.enumIndex(v))));
                    } else {
                        this.serializeString(haxe.root.Type.enumConstructor(v));
                    }

                    this.buf.add(haxe.lang.Runtime.toString(":"));
                    haxe.root.Array arr = haxe.root.Type.enumParameters(v);
                    if ((arr != null)) {
                        this.buf.add(((java.lang.Object) (arr.length)));
                        {
                            int _g4 = 0;
                            while ((_g4 < arr.length)) {
                                java.lang.Object v8 = arr.get(_g4);
                                ++_g4;
                                this.serialize(v8);
                            }

                        }

                    } else {
                        this.buf.add(haxe.lang.Runtime.toString("0"));
                    }

                    if (this.useCache) {
                        this.cache.push(v);
                    }

                }

                break;
            }

            default: {
                throw haxe.lang.HaxeException.wrap(("Cannot serialize " + haxe.root.Std.string(v)));
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -907_763_588: {
                    if (field.equals("scount")) {
                        __temp_executeDef1 = false;
                        this.scount = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_284_193_930: {
                    if (field.equals("useEnumIndex")) {
                        __temp_executeDef1 = false;
                        this.useEnumIndex = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 97_907: {
                    if (field.equals("buf")) {
                        __temp_executeDef1 = false;
                        this.buf = ((haxe.root.StringBuf) (value));
                        return value;
                    }

                    break;
                }

                case -309_504_453: {
                    if (field.equals("useCache")) {
                        __temp_executeDef1 = false;
                        this.useCache = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 94_416_770: {
                    if (field.equals("cache")) {
                        __temp_executeDef1 = false;
                        this.cache = ((haxe.root.Array) (value));
                        return value;
                    }

                    break;
                }

                case -907_763_588: {
                    if (field.equals("scount")) {
                        __temp_executeDef1 = false;
                        this.scount = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 109_400_065: {
                    if (field.equals("shash")) {
                        __temp_executeDef1 = false;
                        this.shash = ((haxe.ds.StringMap<java.lang.Object>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -573_479_200: {
                    if (field.equals("serialize")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "serialize"));
                    }

                    break;
                }

                case 97_907: {
                    if (field.equals("buf")) {
                        __temp_executeDef1 = false;
                        return this.buf;
                    }

                    break;
                }

                case -7_657_031: {
                    if (field.equals("serializeFields")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "serializeFields"));
                    }

                    break;
                }

                case 94_416_770: {
                    if (field.equals("cache")) {
                        __temp_executeDef1 = false;
                        return this.cache;
                    }

                    break;
                }

                case 861_138_323: {
                    if (field.equals("serializeRef")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "serializeRef"));
                    }

                    break;
                }

                case 109_400_065: {
                    if (field.equals("shash")) {
                        __temp_executeDef1 = false;
                        return this.shash;
                    }

                    break;
                }

                case 375_065_361: {
                    if (field.equals("serializeString")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "serializeString"));
                    }

                    break;
                }

                case -907_763_588: {
                    if (field.equals("scount")) {
                        __temp_executeDef1 = false;
                        return this.scount;
                    }

                    break;
                }

                case -1_776_922_004: {
                    if (field.equals("toString")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "toString"));
                    }

                    break;
                }

                case -309_504_453: {
                    if (field.equals("useCache")) {
                        __temp_executeDef1 = false;
                        return this.useCache;
                    }

                    break;
                }

                case 1_284_193_930: {
                    if (field.equals("useEnumIndex")) {
                        __temp_executeDef1 = false;
                        return this.useEnumIndex;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -907_763_588: {
                    if (field.equals("scount")) {
                        __temp_executeDef1 = false;
                        return (this.scount);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -573_479_200: {
                    if (field.equals("serialize")) {
                        __temp_executeDef1 = false;
                        this.serialize(dynargs.get(0));
                    }

                    break;
                }

                case -1_776_922_004: {
                    if (field.equals("toString")) {
                        __temp_executeDef1 = false;
                        return this.toString();
                    }

                    break;
                }

                case -7_657_031: {
                    if (field.equals("serializeFields")) {
                        __temp_executeDef1 = false;
                        this.serializeFields(dynargs.get(0));
                    }

                    break;
                }

                case 375_065_361: {
                    if (field.equals("serializeString")) {
                        __temp_executeDef1 = false;
                        this.serializeString(haxe.lang.Runtime.toString(dynargs.get(0)));
                    }

                    break;
                }

                case 861_138_323: {
                    if (field.equals("serializeRef")) {
                        __temp_executeDef1 = false;
                        return this.serializeRef(dynargs.get(0));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("useEnumIndex");
        baseArr.push("useCache");
        baseArr.push("scount");
        baseArr.push("shash");
        baseArr.push("cache");
        baseArr.push("buf");
        super.__hx_getFields(baseArr);
    }

}
