package haxe.io;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Bytes extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param length
     * @param b
     */
    public static void __hx_ctor_haxe_io_Bytes(haxe.io.Bytes __hx_this, int length, byte[] b) {
        __hx_this.length = length;
        __hx_this.b = b;
    }

    /**
     *
     * @param length
     * @return
     */
    public static haxe.io.Bytes alloc(int length) {
        return new haxe.io.Bytes(length, new byte[length]);
    }

    /**
     *
     * @param s
     * @return
     */
    public static haxe.io.Bytes ofString(java.lang.String s) {
        try {
            byte[] b = s.getBytes(haxe.lang.Runtime.toString("UTF-8"));
            return new haxe.io.Bytes(b.length, b);
        } catch (java.lang.Throwable __temp_catchallException1) {
            haxe.lang.Exceptions.setException(__temp_catchallException1);
            java.lang.Object __temp_catchall2 = __temp_catchallException1;
            if ((__temp_catchall2 instanceof haxe.lang.HaxeException)) {
                __temp_catchall2 = ((haxe.lang.HaxeException) (__temp_catchallException1)).obj;
            }

            {
                java.lang.Object e = __temp_catchall2;
                throw haxe.lang.HaxeException.wrap(e);
            }

        }

    }

    /**
     *
     * @param b
     * @return
     */
    public static haxe.io.Bytes ofData(byte[] b) {
        return new haxe.io.Bytes(b.length, b);
    }

    /**
     *
     * @param b
     * @param pos
     * @return
     */
    public static int fastGet(byte[] b, int pos) {
        return (b[pos] & 255);
    }

    /**
     *
     */
    public int length;

    /**
     *
     */
    public byte[] b;

    /**
     *
     * @param empty
     */
    public Bytes(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param length
     * @param b
     */
    public Bytes(int length, byte[] b) {
        haxe.io.Bytes.__hx_ctor_haxe_io_Bytes(this, length, b);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        this.length = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 98: {
                    if (field.equals("b")) {
                        __temp_executeDef1 = false;
                        this.b = ((byte[]) (value));
                        return value;
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        this.length = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 98: {
                    if (field.equals("b")) {
                        __temp_executeDef1 = false;
                        return this.b;
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return this.length;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return (this.length);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("b");
        baseArr.push("length");
        super.__hx_getFields(baseArr);
    }

}
