package haxe.io;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Eof extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_io_Eof(haxe.io.Eof __hx_this) {
    }

    /**
     *
     * @param empty
     */
    public Eof(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Eof() {
        haxe.io.Eof.__hx_ctor_haxe_io_Eof(this);
    }

    @Override
    public java.lang.String toString() {
        return "Eof";
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_776_922_004: {
                    if (field.equals("toString")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "toString"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_776_922_004: {
                    if (field.equals("toString")) {
                        __temp_executeDef1 = false;
                        return this.toString();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

}
