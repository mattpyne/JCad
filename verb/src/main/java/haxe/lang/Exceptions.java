package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Exceptions extends haxe.lang.HxObject {

    /**
     *
     */
    public static java.lang.ThreadLocal<java.lang.Throwable> exception;

    static {
        haxe.lang.Exceptions.exception = new java.lang.ThreadLocal<>();
    }

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_lang_Exceptions(haxe.lang.Exceptions __hx_this) {
    }

    /**
     *
     * @param exc
     */
    public static void setException(java.lang.Throwable exc) {
        haxe.lang.Exceptions.exception.set((exc));
    }

    /**
     *
     * @param empty
     */
    public Exceptions(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Exceptions() {
        haxe.lang.Exceptions.__hx_ctor_haxe_lang_Exceptions(this);
    }

}
