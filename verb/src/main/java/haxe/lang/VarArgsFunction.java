package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class VarArgsFunction extends haxe.lang.VarArgsBase {

    /**
     *
     */
    public haxe.lang.Function fun;

    /**
     *
     * @param fun
     */
    public VarArgsFunction(haxe.lang.Function fun) {
        super(-1, -1);
        this.fun = fun;
    }

    /**
     *
     * @param dynArgs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeDynamic(haxe.root.Array dynArgs) {
        return (this.fun.__hx_invoke1_o(0.0, dynArgs));
    }

}
