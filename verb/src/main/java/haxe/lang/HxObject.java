package haxe.lang;

import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class HxObject implements IHxObject {

    /**
     *
     * @param empty
     */
    public HxObject(EmptyObject empty) {
    }

    /**
     *
     */
    public HxObject() {

    }

    /**
     *
     * @param field
     * @return
     */
    @Override
    public boolean __hx_deleteField(String field) {
        return false;
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @return
     */
    @Override
    public Object __hx_lookupField(String field, boolean throwErrors, boolean isCheck) {
        if (isCheck) {
            return Runtime.undefined;
        } else {
            if (throwErrors) {
                throw HaxeException.wrap("Field not found.");
            } else {
                return null;
            }
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @return
     */
    @Override
    public double __hx_lookupField_f(String field, boolean throwErrors) {
        if (throwErrors) {
            throw HaxeException.wrap("Field not found or incompatible field type.");
        } else {
            return 0.0;
        }
    }

    /**
     *
     * @param field
     * @param value
     * @return
     */
    @Override
    public Object __hx_lookupSetField(String field, Object value) {
        throw HaxeException.wrap("Cannot access field for writing.");
    }

    /**
     *
     * @param field
     * @param value
     * @return
     */
    @Override
    public double __hx_lookupSetField_f(String field, double value) {
        throw HaxeException.wrap("Cannot access field for writing or incompatible type.");
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(String field, double value, boolean handleProperties) {
        return this.__hx_lookupSetField_f(field, value);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        return this.__hx_lookupSetField(field, value);
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        return this.__hx_lookupField(field, throwErrors, isCheck);
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties) {
        return this.__hx_lookupField_f(field, throwErrors);
    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        return ((Function) (this.__hx_getField(field, true, false, false))).__hx_invokeDynamic(dynargs);
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
    }

}
