// Generated by Haxe 3.4.4
package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public interface IHxObject {

    /**
     *
     * @param field
     * @return
     */
    boolean __hx_deleteField(String field);

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @return
     */
    Object __hx_lookupField(String field, boolean throwErrors, boolean isCheck);

    /**
     *
     * @param field
     * @param throwErrors
     * @return
     */
    double __hx_lookupField_f(String field, boolean throwErrors);

    /**
     *
     * @param field
     * @param value
     * @return
     */
    Object __hx_lookupSetField(String field, Object value);

    /**
     *
     * @param field
     * @param value
     * @return
     */
    double __hx_lookupSetField_f(String field, double value);

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    double __hx_setField_f(String field, double value, boolean handleProperties);

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    Object __hx_setField(String field, Object value, boolean handleProperties);

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties);

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties);

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    Object __hx_invokeField(String field, haxe.root.Array dynargs);

    /**
     *
     * @param baseArr
     */
    void __hx_getFields(haxe.root.Array<String> baseArr);

}
