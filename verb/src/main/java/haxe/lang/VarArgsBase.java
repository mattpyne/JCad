package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class VarArgsBase extends haxe.lang.Function {

    /**
     *
     * @param arity
     * @param type
     */
    public VarArgsBase(int arity, int type) {
        super(arity, type);
    }

    /**
     *
     * @param dynArgs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeDynamic(haxe.root.Array dynArgs) {
        throw haxe.lang.HaxeException.wrap("Abstract implementation");
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @param __fn_float4
     * @param __fn_dyn4
     * @param __fn_float5
     * @param __fn_dyn5
     * @param __fn_float6
     * @param __fn_dyn6
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke6_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3, double __fn_float4, java.lang.Object __fn_dyn4, double __fn_float5, java.lang.Object __fn_dyn5, double __fn_float6, java.lang.Object __fn_dyn6) {
        return this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3)))), (((__fn_dyn4 == haxe.lang.Runtime.undefined)) ? (__fn_float4) : (((java.lang.Object) (__fn_dyn4)))), (((__fn_dyn5 == haxe.lang.Runtime.undefined)) ? (__fn_float5) : (((java.lang.Object) (__fn_dyn5)))), (((__fn_dyn6 == haxe.lang.Runtime.undefined)) ? (__fn_float6) : (((java.lang.Object) (__fn_dyn6))))}));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @param __fn_float4
     * @param __fn_dyn4
     * @param __fn_float5
     * @param __fn_dyn5
     * @param __fn_float6
     * @param __fn_dyn6
     * @return
     */
    @Override
    public double __hx_invoke6_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3, double __fn_float4, java.lang.Object __fn_dyn4, double __fn_float5, java.lang.Object __fn_dyn5, double __fn_float6, java.lang.Object __fn_dyn6) {
        return (haxe.lang.Runtime.toDouble(this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3)))), (((__fn_dyn4 == haxe.lang.Runtime.undefined)) ? (__fn_float4) : (((java.lang.Object) (__fn_dyn4)))), (((__fn_dyn5 == haxe.lang.Runtime.undefined)) ? (__fn_float5) : (((java.lang.Object) (__fn_dyn5)))), (((__fn_dyn6 == haxe.lang.Runtime.undefined)) ? (__fn_float6) : (((java.lang.Object) (__fn_dyn6))))}))));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @param __fn_float4
     * @param __fn_dyn4
     * @param __fn_float5
     * @param __fn_dyn5
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke5_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3, double __fn_float4, java.lang.Object __fn_dyn4, double __fn_float5, java.lang.Object __fn_dyn5) {
        return this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3)))), (((__fn_dyn4 == haxe.lang.Runtime.undefined)) ? (__fn_float4) : (((java.lang.Object) (__fn_dyn4)))), (((__fn_dyn5 == haxe.lang.Runtime.undefined)) ? (__fn_float5) : (((java.lang.Object) (__fn_dyn5))))}));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @param __fn_float4
     * @param __fn_dyn4
     * @param __fn_float5
     * @param __fn_dyn5
     * @return
     */
    @Override
    public double __hx_invoke5_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3, double __fn_float4, java.lang.Object __fn_dyn4, double __fn_float5, java.lang.Object __fn_dyn5) {
        return (haxe.lang.Runtime.toDouble(this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3)))), (((__fn_dyn4 == haxe.lang.Runtime.undefined)) ? (__fn_float4) : (((java.lang.Object) (__fn_dyn4)))), (((__fn_dyn5 == haxe.lang.Runtime.undefined)) ? (__fn_float5) : (((java.lang.Object) (__fn_dyn5))))}))));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @param __fn_float4
     * @param __fn_dyn4
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke4_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3, double __fn_float4, java.lang.Object __fn_dyn4) {
        return this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3)))), (((__fn_dyn4 == haxe.lang.Runtime.undefined)) ? (__fn_float4) : (((java.lang.Object) (__fn_dyn4))))}));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @param __fn_float4
     * @param __fn_dyn4
     * @return
     */
    @Override
    public double __hx_invoke4_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3, double __fn_float4, java.lang.Object __fn_dyn4) {
        return (haxe.lang.Runtime.toDouble(this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3)))), (((__fn_dyn4 == haxe.lang.Runtime.undefined)) ? (__fn_float4) : (((java.lang.Object) (__fn_dyn4))))}))));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke3_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3) {
        return this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3))))}));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @param __fn_float3
     * @param __fn_dyn3
     * @return
     */
    @Override
    public double __hx_invoke3_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2, double __fn_float3, java.lang.Object __fn_dyn3) {
        return (haxe.lang.Runtime.toDouble(this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2)))), (((__fn_dyn3 == haxe.lang.Runtime.undefined)) ? (__fn_float3) : (((java.lang.Object) (__fn_dyn3))))}))));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke2_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        return this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1)))), (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (((java.lang.Object) (__fn_dyn2))))}));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public double __hx_invoke2_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        return (haxe.lang.Runtime.toDouble(
                this.__hx_invokeDynamic(
                        new haxe.root.Array<>(
                                new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined))
                                            ? (__fn_float1)
                                            : (((java.lang.Object) (__fn_dyn1)))),
                                    (((__fn_dyn2 == haxe.lang.Runtime.undefined))
                                            ? (__fn_float2)
                                            : (((java.lang.Object) (__fn_dyn2))))}))));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        return this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1))))}));
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, java.lang.Object __fn_dyn1) {
        return (haxe.lang.Runtime.toDouble(this.__hx_invokeDynamic(new haxe.root.Array<>(new java.lang.Object[]{(((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (((java.lang.Object) (__fn_dyn1))))}))));
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        return this.__hx_invokeDynamic(null);
    }

    /**
     *
     * @return
     */
    @Override
    public double __hx_invoke0_f() {
        return (haxe.lang.Runtime.toDouble(this.__hx_invokeDynamic(null)));
    }

}
