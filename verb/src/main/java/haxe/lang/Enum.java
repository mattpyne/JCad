package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Enum {

    /**
     *
     */
    public final int index;

    /**
     *
     * @param index
     */
    public Enum(int index) {
        this.index = index;
    }

    /**
     *
     * @return
     */
    public java.lang.String getTag() {
        throw haxe.lang.HaxeException.wrap("Not Implemented");
    }

    /**
     *
     * @return
     */
    public haxe.root.Array<java.lang.Object> getParams() {
        return new haxe.root.Array<>(new java.lang.Object[]{});
    }

    @Override
    public java.lang.String toString() {
        return this.getTag();
    }

}
