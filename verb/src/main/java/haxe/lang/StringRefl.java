package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class StringRefl {

    /**
     *
     */
    public static haxe.root.Array<java.lang.String> fields;

    static {
        haxe.lang.StringRefl.fields = new haxe.root.Array<>(new java.lang.String[]{"length", "toUpperCase", "toLowerCase", "charAt", "charCodeAt", "indexOf", "lastIndexOf", "split", "substr", "substring"});
    }

    /**
     *
     * @param str
     * @param f
     * @param throwErrors
     * @return
     */
    public static java.lang.Object handleGetField(java.lang.String str, java.lang.String f, boolean throwErrors) {
        {
            java.lang.String __temp_svar1 = (f);
            int __temp_hash3 = __temp_svar1.hashCode();
            boolean __temp_executeDef2 = true;
            switch (__temp_hash3) {
                case -1_106_363_674: {
                    if (__temp_svar1.equals("length")) {
                        __temp_executeDef2 = false;
                        return str.length();
                    }

                    break;
                }

                case -399_551_817:
                case -1_137_582_698:
                case 530_542_161:
                case -891_529_231:
                case 109_648_666:
                case -467_511_597:
                case 1_943_291_465:
                case 397_153_782:
                case -1_361_633_751: {
                    if (((((__temp_hash3 == -399_551_817) && __temp_svar1.equals("toUpperCase"))) || ((((__temp_hash3 == -1_137_582_698) && __temp_svar1.equals("toLowerCase"))) || ((((__temp_hash3 == 530_542_161) && __temp_svar1.equals("substring"))) || ((((__temp_hash3 == -891_529_231) && __temp_svar1.equals("substr"))) || ((((__temp_hash3 == 109_648_666) && __temp_svar1.equals("split"))) || ((((__temp_hash3 == -467_511_597) && __temp_svar1.equals("lastIndexOf"))) || ((((__temp_hash3 == 1_943_291_465) && __temp_svar1.equals("indexOf"))) || ((((__temp_hash3 == 397_153_782) && __temp_svar1.equals("charCodeAt"))) || __temp_svar1.equals("charAt")))))))))) {
                        __temp_executeDef2 = false;
                        return new haxe.lang.Closure(str, f);
                    }

                    break;
                }

            }

            if (__temp_executeDef2) {
                if (throwErrors) {
                    throw haxe.lang.HaxeException.wrap((("Field not found: \'" + f) + "\' in String"));
                } else {
                    return null;
                }

            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param str
     * @param f
     * @param args
     * @return
     */
    public static java.lang.Object handleCallField(java.lang.String str, java.lang.String f, haxe.root.Array args) {
        haxe.root.Array _args = new haxe.root.Array(new java.lang.Object[]{str});
        if ((args == null)) {
            args = _args;
        } else {
            args = _args.concat(args);
        }

        return haxe.lang.Runtime.slowCallField(haxe.lang.StringExt.class, f, args);
    }

    /**
     *
     */
    private StringRefl() {
    }

}
