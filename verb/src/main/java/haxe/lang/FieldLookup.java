package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class FieldLookup extends HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_lang_FieldLookup(FieldLookup __hx_this) {
    }

    /**
     *
     * @param s
     * @return
     */
    public static int hash(String s) {

        return s.hashCode();

    }

    /**
     *
     * @param hash
     * @param hashs
     * @param length
     * @return
     */
    public static int findHash(String hash, String[] hashs, int length) {
        int min = 0;
        int max = length;
        while (min < max) {
            int mid = (max + min) / 2;
            int classify = hash.compareTo(hashs[mid]);
            if (classify < 0) {
                max = mid;
            } else {
                if (classify > 0) {
                    min += 1;
                } else {
                    return mid;
                }
            }
        }

        return ~(min);
    }

    /**
     *
     * @param a
     * @param length
     * @param pos
     */
    public static void removeString(String[] a, int length, int pos) {
        System.arraycopy((a), ((pos + 1)), (a), (pos), (((length - pos) - 1)));
        a[(length - 1)] = null;
    }

    /**
     *
     * @param a
     * @param length
     * @param pos
     */
    public static void removeFloat(double[] a, int length, int pos) {
        System.arraycopy((a), ((pos + 1)), (a), (pos), (((length - pos) - 1)));
        a[(length - 1)] = (0);
    }

    /**
     *
     * @param a
     * @param length
     * @param pos
     */
    public static void removeDynamic(Object[] a, int length, int pos) {
        System.arraycopy((a), ((pos + 1)), (a), (pos), (((length - pos) - 1)));
        a[(length - 1)] = null;
    }

    /**
     *
     * @param a
     * @param length
     * @param pos
     * @param x
     * @return
     */
    public static String[] insertString(String[] a, int length, int pos, String x) {
        String[] a1 = a;
        int capacity = a1.length;
        if ((pos == length)) {
            if ((capacity == length)) {
                String[] newarr = new String[(((length << 1)) + 1)];
                System.arraycopy((a1), (0), (newarr), (0), (a1.length));
                a1 = newarr;
            }

        } else {
            if ((pos == 0)) {
                if ((capacity == length)) {
                    String[] newarr1 = new String[(((length << 1)) + 1)];
                    System.arraycopy((a1), (0), (newarr1), (1), (length));
                    a1 = newarr1;
                } else {
                    System.arraycopy((a1), (0), (a1), (1), (length));
                }

            } else {
                if ((capacity == length)) {
                    String[] newarr2 = new String[(((length << 1)) + 1)];
                    System.arraycopy((a1), (0), (newarr2), (0), (pos));
                    System.arraycopy((a1), (pos), (newarr2), ((pos + 1)), ((length - pos)));
                    a1 = newarr2;
                } else {
                    System.arraycopy((a1), (pos), (a1), ((pos + 1)), ((length - pos)));
                    System.arraycopy((a1), (0), (a1), (0), (pos));
                }

            }

        }

        a1[pos] = x;
        return a1;
    }

    /**
     *
     * @param a
     * @param length
     * @param pos
     * @param x
     * @return
     */
    public static double[] insertFloat(double[] a, int length, int pos, double x) {
        double[] a1 = a;
        int capacity = a1.length;
        if ((pos == length)) {
            if ((capacity == length)) {
                double[] newarr = new double[(((length << 1)) + 1)];
                System.arraycopy((a1), (0), (newarr), (0), (a1.length));
                a1 = newarr;
            }

        } else {
            if ((pos == 0)) {
                if ((capacity == length)) {
                    double[] newarr1 = new double[(((length << 1)) + 1)];
                    System.arraycopy((a1), (0), (newarr1), (1), (length));
                    a1 = newarr1;
                } else {
                    System.arraycopy((a1), (0), (a1), (1), (length));
                }

            } else {
                if ((capacity == length)) {
                    double[] newarr2 = new double[(((length << 1)) + 1)];
                    System.arraycopy((a1), (0), (newarr2), (0), (pos));
                    System.arraycopy((a1), (pos), (newarr2), ((pos + 1)), ((length - pos)));
                    a1 = newarr2;
                } else {
                    System.arraycopy((a1), (pos), (a1), ((pos + 1)), ((length - pos)));
                    System.arraycopy((a1), (0), (a1), (0), (pos));
                }

            }

        }

        a1[pos] = x;
        return a1;
    }

    /**
     *
     * @param a
     * @param length
     * @param pos
     * @param x
     * @return
     */
    public static Object[] insertDynamic(Object[] a, int length, int pos, Object x) {
        Object[] a1 = a;
        int capacity = a1.length;
        if ((pos == length)) {
            if ((capacity == length)) {
                Object[] newarr = new Object[(((length << 1)) + 1)];
                System.arraycopy((a1), (0), (newarr), (0), (a1.length));
                a1 = newarr;
            }

        } else {
            if ((pos == 0)) {
                if ((capacity == length)) {
                    Object[] newarr1 = new Object[(((length << 1)) + 1)];
                    System.arraycopy((a1), (0), (newarr1), (1), (length));
                    a1 = newarr1;
                } else {
                    System.arraycopy((a1), (0), (a1), (1), (length));
                }

            } else {
                if ((capacity == length)) {
                    Object[] newarr2 = new Object[(((length << 1)) + 1)];
                    System.arraycopy((a1), (0), (newarr2), (0), (pos));
                    System.arraycopy((a1), (pos), (newarr2), ((pos + 1)), ((length - pos)));
                    a1 = newarr2;
                } else {
                    System.arraycopy((a1), (pos), (a1), ((pos + 1)), ((length - pos)));
                    System.arraycopy((a1), (0), (a1), (0), (pos));
                }

            }

        }

        a1[pos] = (x);
        return a1;
    }

    /**
     *
     * @param empty
     */
    public FieldLookup(EmptyObject empty) {
    }

    /**
     *
     */
    public FieldLookup() {
        FieldLookup.__hx_ctor_haxe_lang_FieldLookup(this);
    }

}
