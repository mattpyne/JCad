package haxe.lang;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ParamEnum extends haxe.lang.Enum {

    /**
     *
     */
    public final java.lang.Object[] params;

    /**
     *
     * @param index
     * @param params
     */
    public ParamEnum(int index, java.lang.Object[] params) {
        super(index);
        this.params = params;
    }

    /**
     *
     * @return
     */
    @Override
    public haxe.root.Array<java.lang.Object> getParams() {
        if ((this.params == null)) {
            return new haxe.root.Array<>(new java.lang.Object[]{});
        } else {
            java.lang.Object[] this1 = this.params;
            haxe.root.Array a = new haxe.root.Array();
            int len = ((java.lang.Object[]) (this1)).length;
            {
                int _g1 = 0;
                int _g = len;
                while ((_g1 < _g)) {
                    int i = _g1++;
                    a.set(i, (((java.lang.Object[]) (this1))[i]));
                }

            }

            return (a);
        }

    }

    @Override
    public java.lang.String toString() {
        if (((this.params == null) || (((java.lang.Object[]) (this.params)).length == 0))) {
            return this.getTag();
        }

        haxe.root.StringBuf ret = new haxe.root.StringBuf();
        ret.add(haxe.lang.Runtime.toString(this.getTag()));
        ret.add(haxe.lang.Runtime.toString("("));
        boolean first = true;
        {
            int _g = 0;
            java.lang.Object[] _g1 = this.params;
            while ((_g < ((java.lang.Object[]) (_g1)).length)) {
                java.lang.Object p = (((java.lang.Object[]) (_g1))[_g]);
                ++_g;
                if (first) {
                    first = false;
                } else {
                    ret.add(haxe.lang.Runtime.toString(","));
                }

                ret.add((p));
            }

        }

        ret.add(haxe.lang.Runtime.toString(")"));
        return ret.toString();
    }

    @Override
    public boolean equals(java.lang.Object obj) {
        if (haxe.lang.Runtime.eq(obj, this)) {
            return true;
        }

        haxe.lang.ParamEnum obj1 = (((obj instanceof haxe.lang.ParamEnum)) ? (((haxe.lang.ParamEnum) (obj))) : (null));
        boolean ret = (((obj1 != null) && haxe.root.Std.is(obj1, haxe.root.Type.getEnum((this)))) && (obj1.index == this.index));
        if (!(ret)) {
            return false;
        }

        if ((obj1.params == this.params)) {
            return true;
        }

        int len = 0;
        boolean tmp = false;
        if (!((((obj1.params == null) || (this.params == null))))) {
            len = ((java.lang.Object[]) (this.params)).length;
            tmp = (len != ((java.lang.Object[]) (obj1.params)).length);
        } else {
            tmp = true;
        }

        if (tmp) {
            return false;
        }

        {
            int _g1 = 0;
            int _g = len;
            while ((_g1 < _g)) {
                int i = _g1++;
                if (!(haxe.root.Type.enumEq(((java.lang.Object[]) (obj1.params))[i], ((java.lang.Object[]) (this.params))[i]))) {
                    return false;
                }

            }

        }

        return true;
    }

    @Override
    public int hashCode() {
        int h = 19;
        if ((this.params != null)) {
            int _g = 0;
            java.lang.Object[] _g1 = this.params;
            while ((_g < ((java.lang.Object[]) (_g1)).length)) {
                java.lang.Object p = (((java.lang.Object[]) (_g1))[_g]);
                ++_g;
                h *= 31;
                if ((!((p == null)))) {
                    h = (haxe.lang.Runtime.toInt(haxe.lang.Runtime.plus(h, p.hashCode())));
                }

            }

        }

        h += this.index;
        return h;
    }

}
