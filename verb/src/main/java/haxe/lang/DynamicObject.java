package haxe.lang;

import haxe.root.Array;
import haxe.root.Reflect;
import haxe.root.StringBuf;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class DynamicObject extends HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_lang_DynamicObject(DynamicObject __hx_this) {
        __hx_this.__hx_hashes = new String[]{};
        __hx_this.__hx_dynamics = new Object[]{};
        __hx_this.__hx_hashes_f = new String[]{};
        __hx_this.__hx_dynamics_f = new double[]{};
    }

    /**
     *
     * @param __hx_this
     * @param __hx_hashes
     * @param __hx_dynamics
     * @param __hx_hashes_f
     * @param __hx_dynamics_f
     */
    public static void __hx_ctor_haxe_lang_DynamicObject(DynamicObject __hx_this, String[] __hx_hashes, Object[] __hx_dynamics, String[] __hx_hashes_f, double[] __hx_dynamics_f) {
        __hx_this.__hx_hashes = __hx_hashes;
        __hx_this.__hx_dynamics = __hx_dynamics;
        __hx_this.__hx_hashes_f = __hx_hashes_f;
        __hx_this.__hx_dynamics_f = __hx_dynamics_f;
        __hx_this.__hx_length = __hx_hashes.length;
        __hx_this.__hx_length_f = __hx_hashes_f.length;
    }

    /**
     *
     */
    public String[] __hx_hashes;

    /**
     *
     */
    public Object[] __hx_dynamics;

    /**
     *
     */
    public String[] __hx_hashes_f;

    /**
     *
     */
    public double[] __hx_dynamics_f;

    /**
     *
     */
    public int __hx_length;

    /**
     *
     */
    public int __hx_length_f;

    /**
     *
     * @param empty
     */
    public DynamicObject(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     */
    public DynamicObject() {
        DynamicObject.__hx_ctor_haxe_lang_DynamicObject(this);
    }

    /**
     *
     * @param __hx_hashes
     * @param __hx_dynamics
     * @param __hx_hashes_f
     * @param __hx_dynamics_f
     */
    public DynamicObject(String[] __hx_hashes, Object[] __hx_dynamics, String[] __hx_hashes_f, double[] __hx_dynamics_f) {
        DynamicObject.__hx_ctor_haxe_lang_DynamicObject(this, __hx_hashes, __hx_dynamics, __hx_hashes_f, __hx_dynamics_f);
    }

    @Override
    public String toString() {
        Function ts = (Function) Runtime.getField(this, "toString", false);
        if ((ts != null)) {
            return Runtime.toString(ts.__hx_invoke0_o());
        }
        StringBuilder ret = new StringBuilder();
        ret.append("{");
        boolean first = true;
        {
            int _g = 0;
            Array<String> _g1 = Reflect.fields(this);
            while (_g < _g1.length) {
                String f = _g1.get(_g);
                ++_g;
                if (first) {
                    first = false;
                } else {
                    ret.append(",");
                }
                ret.append(" ");
                ret.append(f);
                ret.append(" : ");
                ret.append(Reflect.field(this, f));
            }
        }
        if (!first) {
            ret.append(" ");
        }
        ret.append("}");
        return ret.toString();
    }

    /**
     *
     * @param field
     * @return
     */
    @Override
    public boolean __hx_deleteField(String field) {
        int res = FieldLookup.findHash(field, this.__hx_hashes, this.__hx_length);
        if ((res >= 0)) {
            FieldLookup.removeString(this.__hx_hashes, this.__hx_length, res);
            FieldLookup.removeDynamic(this.__hx_dynamics, this.__hx_length, res);
            this.__hx_length--;
            return true;
        } else {
            res = FieldLookup.findHash(field, this.__hx_hashes_f, this.__hx_length_f);
            if ((res >= 0)) {
                FieldLookup.removeString(this.__hx_hashes_f, this.__hx_length_f, res);
                FieldLookup.removeFloat(this.__hx_dynamics_f, this.__hx_length_f, res);
                this.__hx_length_f--;
                return true;
            }

        }

        return false;
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @return
     */
    @Override
    public Object __hx_lookupField(String field, boolean throwErrors, boolean isCheck) {
        int res = FieldLookup.findHash(field, this.__hx_hashes, this.__hx_length);
        if (res >= 0) {
            return this.__hx_dynamics[res];
        } else {
            res = FieldLookup.findHash(field, this.__hx_hashes_f, this.__hx_length_f);
            if (res >= 0) {
                return this.__hx_dynamics_f[res];
            }
        }

        if (isCheck) {
            return Runtime.undefined;
        } else {
            return null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @return
     */
    @Override
    public double __hx_lookupField_f(String field, boolean throwErrors) {
        int res = FieldLookup.findHash(field, this.__hx_hashes_f, this.__hx_length_f);
        if (res >= 0) {
            return this.__hx_dynamics_f[res];
        } else {
            res = FieldLookup.findHash(field, this.__hx_hashes, this.__hx_length);
            if ((res >= 0)) {
                return (Runtime.toDouble(this.__hx_dynamics[res]));
            }

        }

        return 0.0;
    }

    /**
     *
     * @param field
     * @param value
     * @return
     */
    @Override
    public Object __hx_lookupSetField(String field, Object value) {
        int res = FieldLookup.findHash(field, this.__hx_hashes, this.__hx_length);
        if ((res >= 0)) {
            return this.__hx_dynamics[res] = value;
        } else {
            int res2 = FieldLookup.findHash(field, this.__hx_hashes_f, this.__hx_length_f);
            if ((res2 >= 0)) {
                FieldLookup.removeString(this.__hx_hashes_f, this.__hx_length_f, res2);
                FieldLookup.removeFloat(this.__hx_dynamics_f, this.__hx_length_f, res2);
                this.__hx_length_f--;
            }

        }

        this.__hx_hashes = FieldLookup.insertString(this.__hx_hashes, this.__hx_length, ~(res), field);
        this.__hx_dynamics = FieldLookup.insertDynamic(this.__hx_dynamics, this.__hx_length, ~(res), value);
        this.__hx_length++;
        return value;
    }

    /**
     *
     * @param field
     * @param value
     * @return
     */
    @Override
    public double __hx_lookupSetField_f(String field, double value) {
        int res = FieldLookup.findHash(field, this.__hx_hashes_f, this.__hx_length_f);
        if ((res >= 0)) {
            return this.__hx_dynamics_f[res] = value;
        } else {
            int res2 = FieldLookup.findHash(field, this.__hx_hashes, this.__hx_length);
            if ((res2 >= 0)) {
                FieldLookup.removeString(this.__hx_hashes, this.__hx_length, res2);
                FieldLookup.removeDynamic(this.__hx_dynamics, this.__hx_length, res2);
                this.__hx_length--;
            }

        }

        this.__hx_hashes_f = FieldLookup.insertString(this.__hx_hashes_f, this.__hx_length_f, ~(res), field);
        this.__hx_dynamics_f = FieldLookup.insertFloat(this.__hx_dynamics_f, this.__hx_length_f, ~(res), value);
        this.__hx_length_f++;
        return value;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        int i = 0;
        i = 0;
        while ((i < this.__hx_length)) {
            baseArr.push(this.__hx_hashes[i++]);
        }

        i = 0;
        while ((i < this.__hx_length_f)) {
            baseArr.push(this.__hx_hashes_f[i++]);
        }

        super.__hx_getFields(baseArr);
    }

}
