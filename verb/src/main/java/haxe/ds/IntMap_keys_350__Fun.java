package haxe.ds;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class IntMap_keys_350__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public int[] i;

    /**
     *
     */
    public haxe.ds.IntMap<T> _gthis;

    /**
     *
     * @param i
     * @param _gthis
     */
    public IntMap_keys_350__Fun(int[] i, haxe.ds.IntMap<T> _gthis) {
        super(0, 1);
        this.i = i;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public double __hx_invoke0_f() {
        int ret = this._gthis._keys[this.i[0]];
        this._gthis.cachedIndex = this.i[0];
        this._gthis.cachedKey = ret;
        this.i[0] += 1;
        return ret;
    }

}
