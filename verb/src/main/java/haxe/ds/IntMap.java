package haxe.ds;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class IntMap<T> extends haxe.lang.HxObject implements haxe.IMap<java.lang.Object, T> {

    /**
     *
     */
    public static double HASH_UPPER;

    static {
        haxe.ds.IntMap.HASH_UPPER = 0.7;
    }

    /**
     *
     * @param <T_c>
     * @param __hx_this
     */
    public static <T_c> void __hx_ctor_haxe_ds_IntMap(haxe.ds.IntMap<T_c> __hx_this) {
        __hx_this.cachedIndex = -1;
    }

    /**
     *
     * @param x
     */
    public static void _assert(boolean x) {
    }

    /**
     *
     * @return
     */
    public static int defaultK() {
        return 0;
    }

    /**
     *
     * @param sourceArray
     * @param sourceIndex
     * @param destinationArray
     * @param destinationIndex
     * @param length
     */
    public static void arrayCopy(java.lang.Object sourceArray, int sourceIndex, java.lang.Object destinationArray, int destinationIndex, int length) {
        java.lang.System.arraycopy((sourceArray), (sourceIndex), (destinationArray), (destinationIndex), (length));
    }

    /**
     *
     * @param k
     * @param mask
     * @return
     */
    public static int getInc(int k, int mask) {
        return (((((k >> 3) ^ (k << 3)) | 1)) & mask);
    }

    /**
     *
     * @param i
     * @return
     */
    public static int hash(int i) {
        return i;
    }

    /**
     *
     * @param flag
     * @param i
     * @return
     */
    public static boolean flagIsEmpty(int[] flag, int i) {
        return ((((flag[(i >> 4)] >>> ((((i & 15)) << 1))) & 2)) != 0);
    }

    /**
     *
     * @param flag
     * @param i
     * @return
     */
    public static boolean flagIsDel(int[] flag, int i) {
        return ((((flag[(i >> 4)] >>> ((((i & 15)) << 1))) & 1)) != 0);
    }

    /**
     *
     * @param flag
     * @param i
     * @return
     */
    public static boolean isEither(int[] flag, int i) {
        return ((((flag[(i >> 4)] >>> ((((i & 15)) << 1))) & 3)) != 0);
    }

    /**
     *
     * @param flag
     * @param i
     */
    public static void setIsDelFalse(int[] flag, int i) {
        flag[(i >> 4)] &= ~(((1 << ((((i & 15)) << 1)))));
    }

    /**
     *
     * @param flag
     * @param i
     */
    public static void setIsEmptyFalse(int[] flag, int i) {
        flag[(i >> 4)] &= ~(((2 << ((((i & 15)) << 1)))));
    }

    /**
     *
     * @param flag
     * @param i
     */
    public static void setIsBothFalse(int[] flag, int i) {
        flag[(i >> 4)] &= ~(((3 << ((((i & 15)) << 1)))));
    }

    /**
     *
     * @param flag
     * @param i
     */
    public static void setIsDelTrue(int[] flag, int i) {
        flag[(i >> 4)] |= (1 << ((((i & 15)) << 1)));
    }

    /**
     *
     * @param x
     * @return
     */
    public static int roundUp(int x) {
        --x;
        x |= (x >>> 1);
        x |= (x >>> 2);
        x |= (x >>> 4);
        x |= (x >>> 8);
        x |= (x >>> 16);
        return ++x;
    }

    /**
     *
     * @param m
     * @return
     */
    public static int flagsSize(int m) {
        if ((m < 16)) {
            return 1;
        } else {
            return (m >> 4);
        }

    }

    /**
     *
     */
    public int[] flags;

    /**
     *
     */
    public int[] _keys;

    /**
     *
     */
    public T[] vals;

    /**
     *
     */
    public int nBuckets;

    /**
     *
     */
    public int size;

    /**
     *
     */
    public int nOccupied;

    /**
     *
     */
    public int upperBound;

    /**
     *
     */
    public int cachedKey;

    /**
     *
     */
    public int cachedIndex;

    /**
     *
     * @param empty
     */
    public IntMap(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public IntMap() {
        haxe.ds.IntMap.__hx_ctor_haxe_ds_IntMap(this);
    }

    /**
     *
     * @param key
     * @param value
     */
    public void set(int key, T value) {
        int x = 0;
        if ((this.nOccupied >= this.upperBound)) {
            if ((this.nBuckets > (this.size << 1))) {
                this.resize((this.nBuckets - 1));
            } else {
                this.resize((this.nBuckets + 1));
            }

        }

        int[] flags = this.flags;
        int[] _keys = this._keys;
        {
            int mask = (this.nBuckets - 1);
            x = this.nBuckets;
            int site = x;
            int k = key;
            int i = (k & mask);
            int delKey = -1;
            if (((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 2)) != 0)) {
                x = i;
            } else {
                int inc = (((((k >> 3) ^ (k << 3)) | 1)) & mask);
                int last = i;
                while (!(((((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 2)) != 0) || (_keys[i] == key))))) {
                    if ((((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 1)) != 0) && (delKey == -1))) {
                        delKey = i;
                    }

                    i = ((i + inc) & mask);
                }

                if ((((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 2)) != 0) && (delKey != -1))) {
                    x = delKey;
                } else {
                    x = i;
                }

            }

        }

        if (((((flags[(x >> 4)] >>> ((((x & 15)) << 1))) & 2)) != 0)) {
            _keys[x] = key;
            this.vals[x] = value;
            flags[(x >> 4)] &= ~(((3 << ((((x & 15)) << 1)))));
            this.size++;
            this.nOccupied++;
        } else {
            if (((((flags[(x >> 4)] >>> ((((x & 15)) << 1))) & 1)) != 0)) {
                _keys[x] = key;
                this.vals[x] = value;
                flags[(x >> 4)] &= ~(((3 << ((((x & 15)) << 1)))));
                this.size++;
            } else {
                this.vals[x] = value;
            }

        }

    }

    /**
     *
     * @param key
     * @return
     */
    public int lookup(int key) {
        if ((this.nBuckets != 0)) {
            int[] flags = this.flags;
            int[] _keys = this._keys;
            int mask = (this.nBuckets - 1);
            int k = key;
            int i = (k & mask);
            int inc = (((((k >> 3) ^ (k << 3)) | 1)) & mask);
            int last = i;
            while ((((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 2)) == 0) && ((((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 1)) != 0) || (_keys[i] != key))))) {
                i = ((i + inc) & mask);
                if ((i == last)) {
                    return -1;
                }

            }

            if (((((flags[(i >> 4)] >>> ((((i & 15)) << 1))) & 3)) != 0)) {
                return -1;
            } else {
                return i;
            }

        }

        return -1;
    }

    /**
     *
     * @param key
     * @return
     */
    public java.lang.Object get(int key) {
        int idx = -1;
        boolean tmp = false;
        if ((this.cachedKey == key)) {
            idx = this.cachedIndex;
            tmp = (idx != -1);
        } else {
            tmp = false;
        }

        if (tmp) {
            return this.vals[idx];
        }

        idx = this.lookup(key);
        if ((idx != -1)) {
            this.cachedKey = key;
            this.cachedIndex = idx;
            return this.vals[idx];
        }

        return null;
    }

    /**
     *
     * @param key
     * @return
     */
    public boolean exists(int key) {
        int idx = -1;
        boolean tmp = false;
        if ((this.cachedKey == key)) {
            idx = this.cachedIndex;
            tmp = (idx != -1);
        } else {
            tmp = false;
        }

        if (tmp) {
            return true;
        }

        idx = this.lookup(key);
        if ((idx != -1)) {
            this.cachedKey = key;
            this.cachedIndex = idx;
            return true;
        }

        return false;
    }

    /**
     *
     * @param newNBuckets
     */
    public void resize(int newNBuckets) {
        int[] newFlags = null;
        int j = 1;
        {
            int x = newNBuckets;
            --x;
            x |= (x >>> 1);
            x |= (x >>> 2);
            x |= (x >>> 4);
            x |= (x >>> 8);
            x |= (x >>> 16);
            newNBuckets = ++x;
            if ((newNBuckets < 4)) {
                newNBuckets = 4;
            }

            if ((this.size >= ((newNBuckets * 0.7) + 0.5))) {
                j = 0;
            } else {
                int nfSize = (((newNBuckets < 16)) ? (1) : ((newNBuckets >> 4)));
                newFlags = new int[nfSize];
                {
                    int _g1 = 0;
                    int _g = nfSize;
                    while ((_g1 < _g)) {
                        int i = _g1++;
                        newFlags[i] = -1_431_655_766;
                    }

                }

                if ((this.nBuckets < newNBuckets)) {
                    int[] k = new int[newNBuckets];
                    if ((this._keys != null)) {
                        java.lang.System.arraycopy((this._keys), (0), (k), (0), (this.nBuckets));
                    }

                    this._keys = k;
                    T[] v = ((T[]) (((java.lang.Object) (new java.lang.Object[newNBuckets]))));
                    if ((this.vals != null)) {
                        java.lang.System.arraycopy((this.vals), (0), (v), (0), (this.nBuckets));
                    }

                    this.vals = v;
                }

            }

        }

        if ((j != 0)) {
            this.cachedKey = 0;
            this.cachedIndex = -1;
            j = -1;
            int nBuckets = this.nBuckets;
            int[] _keys = this._keys;
            T[] vals = this.vals;
            int[] flags = this.flags;
            int newMask = (newNBuckets - 1);
            while ((++j < nBuckets)) {
                if (((((flags[(j >> 4)] >>> ((((j & 15)) << 1))) & 3)) == 0)) {
                    int key = _keys[j];
                    T val = vals[j];
                    flags[(j >> 4)] |= (1 << ((((j & 15)) << 1)));
                    while (true) {
                        int k1 = key;
                        int inc = (((((k1 >> 3) ^ (k1 << 3)) | 1)) & newMask);
                        int i1 = (k1 & newMask);
                        while (((((newFlags[(i1 >> 4)] >>> ((((i1 & 15)) << 1))) & 2)) == 0)) {
                            i1 = ((i1 + inc) & newMask);
                        }

                        newFlags[(i1 >> 4)] &= ~(((2 << ((((i1 & 15)) << 1)))));
                        if (((i1 < nBuckets) && ((((flags[(i1 >> 4)] >>> ((((i1 & 15)) << 1))) & 3)) == 0))) {
                            {
                                int tmp = _keys[i1];
                                _keys[i1] = key;
                                key = tmp;
                            }

                            {
                                T tmp1 = vals[i1];
                                vals[i1] = val;
                                val = tmp1;
                            }

                            flags[(i1 >> 4)] |= (1 << ((((i1 & 15)) << 1)));
                        } else {
                            _keys[i1] = key;
                            vals[i1] = val;
                            break;
                        }

                    }

                }

            }

            if ((nBuckets > newNBuckets)) {
                {
                    int[] k2 = new int[newNBuckets];
                    java.lang.System.arraycopy((_keys), (0), (k2), (0), (newNBuckets));
                    this._keys = k2;
                }

                {
                    T[] v1 = ((T[]) (((java.lang.Object) (new java.lang.Object[newNBuckets]))));
                    java.lang.System.arraycopy((vals), (0), (v1), (0), (newNBuckets));
                    this.vals = v1;
                }

            }

            this.flags = newFlags;
            this.nBuckets = newNBuckets;
            this.nOccupied = this.size;
            this.upperBound = ((int) (((newNBuckets * 0.7) + .5)));
        }

    }

    /**
     *
     * @return
     */
    public java.lang.Object keys() {
        haxe.ds.IntMap<T> _gthis = this;
        int[] i = new int[]{0};
        int len = this.nBuckets;
        {
            haxe.lang.Function __temp_odecl1 = new haxe.ds.IntMap_keys_339__Fun<>(len, i, _gthis);
            haxe.lang.Function __temp_odecl2 = new haxe.ds.IntMap_keys_350__Fun<>(i, _gthis);
            return new haxe.lang.DynamicObject(new java.lang.String[]{"hasNext", "next"}, new java.lang.Object[]{__temp_odecl1, __temp_odecl2}, new java.lang.String[]{}, new double[]{});
        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_005_083_856: {
                    if (field.equals("cachedIndex")) {
                        __temp_executeDef1 = false;
                        this.cachedIndex = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 325_636_987: {
                    if (field.equals("nBuckets")) {
                        __temp_executeDef1 = false;
                        this.nBuckets = ((int) (value));
                        return value;
                    }

                    break;
                }

                case -553_141_795: {
                    if (field.equals("cachedKey")) {
                        __temp_executeDef1 = false;
                        this.cachedKey = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 3_530_753: {
                    if (field.equals("size")) {
                        __temp_executeDef1 = false;
                        this.size = ((int) (value));
                        return value;
                    }

                    break;
                }

                case -1_690_761_732: {
                    if (field.equals("upperBound")) {
                        __temp_executeDef1 = false;
                        this.upperBound = ((int) (value));
                        return value;
                    }

                    break;
                }

                case -394_102_484: {
                    if (field.equals("nOccupied")) {
                        __temp_executeDef1 = false;
                        this.nOccupied = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_005_083_856: {
                    if (field.equals("cachedIndex")) {
                        __temp_executeDef1 = false;
                        this.cachedIndex = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 97_513_095: {
                    if (field.equals("flags")) {
                        __temp_executeDef1 = false;
                        this.flags = ((int[]) (value));
                        return value;
                    }

                    break;
                }

                case -553_141_795: {
                    if (field.equals("cachedKey")) {
                        __temp_executeDef1 = false;
                        this.cachedKey = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 91_023_059: {
                    if (field.equals("_keys")) {
                        __temp_executeDef1 = false;
                        this._keys = ((int[]) (value));
                        return value;
                    }

                    break;
                }

                case -1_690_761_732: {
                    if (field.equals("upperBound")) {
                        __temp_executeDef1 = false;
                        this.upperBound = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 3_612_018: {
                    if (field.equals("vals")) {
                        __temp_executeDef1 = false;
                        this.vals = ((T[]) (value));
                        return value;
                    }

                    break;
                }

                case -394_102_484: {
                    if (field.equals("nOccupied")) {
                        __temp_executeDef1 = false;
                        this.nOccupied = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 325_636_987: {
                    if (field.equals("nBuckets")) {
                        __temp_executeDef1 = false;
                        this.nBuckets = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 3_530_753: {
                    if (field.equals("size")) {
                        __temp_executeDef1 = false;
                        this.size = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_288_564: {
                    if (field.equals("keys")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "keys"));
                    }

                    break;
                }

                case 97_513_095: {
                    if (field.equals("flags")) {
                        __temp_executeDef1 = false;
                        return this.flags;
                    }

                    break;
                }

                case -934_437_708: {
                    if (field.equals("resize")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "resize"));
                    }

                    break;
                }

                case 91_023_059: {
                    if (field.equals("_keys")) {
                        __temp_executeDef1 = false;
                        return this._keys;
                    }

                    break;
                }

                case -1_289_358_244: {
                    if (field.equals("exists")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "exists"));
                    }

                    break;
                }

                case 3_612_018: {
                    if (field.equals("vals")) {
                        __temp_executeDef1 = false;
                        return this.vals;
                    }

                    break;
                }

                case 102_230: {
                    if (field.equals("get")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "get"));
                    }

                    break;
                }

                case 325_636_987: {
                    if (field.equals("nBuckets")) {
                        __temp_executeDef1 = false;
                        return this.nBuckets;
                    }

                    break;
                }

                case -1_097_094_790: {
                    if (field.equals("lookup")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "lookup"));
                    }

                    break;
                }

                case 3_530_753: {
                    if (field.equals("size")) {
                        __temp_executeDef1 = false;
                        return this.size;
                    }

                    break;
                }

                case 113_762: {
                    if (field.equals("set")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "set"));
                    }

                    break;
                }

                case -394_102_484: {
                    if (field.equals("nOccupied")) {
                        __temp_executeDef1 = false;
                        return this.nOccupied;
                    }

                    break;
                }

                case 1_005_083_856: {
                    if (field.equals("cachedIndex")) {
                        __temp_executeDef1 = false;
                        return this.cachedIndex;
                    }

                    break;
                }

                case -1_690_761_732: {
                    if (field.equals("upperBound")) {
                        __temp_executeDef1 = false;
                        return this.upperBound;
                    }

                    break;
                }

                case -553_141_795: {
                    if (field.equals("cachedKey")) {
                        __temp_executeDef1 = false;
                        return this.cachedKey;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_005_083_856: {
                    if (field.equals("cachedIndex")) {
                        __temp_executeDef1 = false;
                        return (this.cachedIndex);
                    }

                    break;
                }

                case 325_636_987: {
                    if (field.equals("nBuckets")) {
                        __temp_executeDef1 = false;
                        return (this.nBuckets);
                    }

                    break;
                }

                case -553_141_795: {
                    if (field.equals("cachedKey")) {
                        __temp_executeDef1 = false;
                        return (this.cachedKey);
                    }

                    break;
                }

                case 3_530_753: {
                    if (field.equals("size")) {
                        __temp_executeDef1 = false;
                        return (this.size);
                    }

                    break;
                }

                case -1_690_761_732: {
                    if (field.equals("upperBound")) {
                        __temp_executeDef1 = false;
                        return (this.upperBound);
                    }

                    break;
                }

                case -394_102_484: {
                    if (field.equals("nOccupied")) {
                        __temp_executeDef1 = false;
                        return (this.nOccupied);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_288_564: {
                    if (field.equals("keys")) {
                        __temp_executeDef1 = false;
                        return this.keys();
                    }

                    break;
                }

                case 113_762: {
                    if (field.equals("set")) {
                        __temp_executeDef1 = false;
                        this.set((haxe.lang.Runtime.toInt(dynargs.get(0))), ((T) (dynargs.get(1))));
                    }

                    break;
                }

                case -934_437_708: {
                    if (field.equals("resize")) {
                        __temp_executeDef1 = false;
                        this.resize((haxe.lang.Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case -1_097_094_790: {
                    if (field.equals("lookup")) {
                        __temp_executeDef1 = false;
                        return this.lookup((haxe.lang.Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case -1_289_358_244: {
                    if (field.equals("exists")) {
                        __temp_executeDef1 = false;
                        return this.exists((haxe.lang.Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case 102_230: {
                    if (field.equals("get")) {
                        __temp_executeDef1 = false;
                        return this.get((haxe.lang.Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("cachedIndex");
        baseArr.push("cachedKey");
        baseArr.push("upperBound");
        baseArr.push("nOccupied");
        baseArr.push("size");
        baseArr.push("nBuckets");
        baseArr.push("vals");
        baseArr.push("_keys");
        baseArr.push("flags");
        super.__hx_getFields(baseArr);
    }

}
