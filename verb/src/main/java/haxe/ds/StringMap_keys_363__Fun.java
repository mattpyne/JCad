package haxe.ds;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class StringMap_keys_363__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public int len;

    /**
     *
     */
    public int[] i;

    /**
     *
     */
    public haxe.ds.StringMap<T> _gthis;

    /**
     *
     * @param len
     * @param i
     * @param _gthis
     */
    public StringMap_keys_363__Fun(int len, int[] i, haxe.ds.StringMap<T> _gthis) {
        super(0, 0);
        this.len = len;
        this.i = i;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        {
            int _g1 = this.i[0];
            int _g = this.len;
            while ((_g1 < _g)) {
                int j = _g1++;
                if ((((this._gthis.hashes[j] & -2)) != 0)) {
                    this.i[0] = j;
                    return true;
                }

            }

        }

        return false;
    }

}
