package haxe.ds;

/**
 *
 * @author Matthew
 * @param <V>
 * @param <K>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ObjectMap_keys_374__Fun<V, K> extends haxe.lang.Function {

    /**
     *
     */
    public int[] i;

    /**
     *
     */
    public haxe.ds.ObjectMap<K, V> _gthis;

    /**
     *
     * @param i
     * @param _gthis
     */
    public ObjectMap_keys_374__Fun(int[] i, haxe.ds.ObjectMap<K, V> _gthis) {
        super(0, 0);
        this.i = i;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        K ret = this._gthis._keys[this.i[0]];
        this._gthis.cachedIndex = this.i[0];
        this._gthis.cachedKey = ret;
        this.i[0] += 1;
        return ret;
    }

}
