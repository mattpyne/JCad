package haxe.ds;

/**
 *
 * @author Matthew
 * @param <V>
 * @param <K>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ObjectMap_keys_363__Fun<V, K> extends haxe.lang.Function {

    /**
     *
     */
    public int len;

    /**
     *
     */
    public int[] i;

    /**
     *
     */
    public haxe.ds.ObjectMap<K, V> _gthis;

    /**
     *
     * @param len
     * @param i
     * @param _gthis
     */
    public ObjectMap_keys_363__Fun(int len, int[] i, haxe.ds.ObjectMap<K, V> _gthis) {
        super(0, 0);
        this.len = len;
        this.i = i;
        this._gthis = _gthis;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke0_o() {
        {
            int _g1 = this.i[0];
            int _g = this.len;
            while ((_g1 < _g)) {
                int j = _g1++;
                if ((((this._gthis.hashes[j] & -2)) != 0)) {
                    this.i[0] = j;
                    return true;
                }

            }

        }

        return false;
    }

}
