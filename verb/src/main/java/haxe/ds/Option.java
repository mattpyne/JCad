package haxe.ds;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Option extends haxe.lang.ParamEnum {

    /**
     *
     */
    public static final java.lang.String[] __hx_constructs = new java.lang.String[]{"Some", "None"};

    /**
     *
     */
    public static final haxe.ds.Option None = new haxe.ds.Option(1, null);

    /**
     *
     * @param v
     * @return
     */
    public static haxe.ds.Option Some(java.lang.Object v) {
        return new haxe.ds.Option(0, new java.lang.Object[]{v});
    }

    /**
     *
     * @param index
     * @param params
     */
    public Option(int index, java.lang.Object[] params) {
        super(index, params);
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.String getTag() {
        return haxe.ds.Option.__hx_constructs[this.index];
    }

}
