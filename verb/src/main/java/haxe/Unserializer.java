package haxe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Unserializer extends haxe.lang.HxObject {

    /**
     *
     */
    public static java.lang.Object DEFAULT_RESOLVER;

    /**
     *
     */
    public static java.lang.String BASE64;

    /**
     *
     */
    public static haxe.root.Array<java.lang.Object> CODES;

    static {
        haxe.Unserializer.DEFAULT_RESOLVER = (new haxe._Unserializer.DefaultResolver());
        haxe.Unserializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
        haxe.Unserializer.CODES = null;
    }

    /**
     *
     * @param __hx_this
     * @param buf
     */
    public static void __hx_ctor_haxe_Unserializer(haxe.Unserializer __hx_this, java.lang.String buf) {
        __hx_this.buf = buf;
        __hx_this.length = buf.length();
        __hx_this.pos = 0;
        __hx_this.scache = new haxe.root.Array<>();
        __hx_this.cache = new haxe.root.Array();
        java.lang.Object r = haxe.Unserializer.DEFAULT_RESOLVER;
        if ((r == null)) {
            r = new haxe._Unserializer.DefaultResolver();
            haxe.Unserializer.DEFAULT_RESOLVER = r;
        }
        __hx_this.resolver = r;
    }

    /**
     *
     * @return
     */
    public static haxe.root.Array<java.lang.Object> initCodes() {
        haxe.root.Array<java.lang.Object> codes = new haxe.root.Array<>();
        {
            int _g1 = 0;
            int _g = haxe.Unserializer.BASE64.length();
            while ((_g1 < _g)) {
                int i = _g1++;
                java.lang.String s = haxe.Unserializer.BASE64;
                codes.set((((i < s.length())) ? (s.charAt(i)) : (-1)), i);
            }

        }

        return codes;
    }

    /**
     *
     */
    public java.lang.String buf;

    /**
     *
     */
    public int pos;

    /**
     *
     */
    public int length;

    /**
     *
     */
    public haxe.root.Array cache;

    /**
     *
     */
    public haxe.root.Array<java.lang.String> scache;

    /**
     *
     */
    public java.lang.Object resolver;

    /**
     *
     * @param empty
     */
    public Unserializer(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param buf
     */
    public Unserializer(java.lang.String buf) {
        haxe.Unserializer.__hx_ctor_haxe_Unserializer(this, buf);
    }

    /**
     *
     * @return
     */
    public int readDigits() {
        int k = 0;
        boolean s = false;
        int fpos = this.pos;
        while (true) {
            int p = this.pos;
            java.lang.String s1 = this.buf;
            int c = (((p < s1.length())) ? (s1.charAt(p)) : (-1));
            if ((c == -1)) {
                break;
            }

            if ((c == 45)) {
                if ((this.pos != fpos)) {
                    break;
                }

                s = true;
                this.pos++;
                continue;
            }

            if (((c < 48) || (c > 57))) {
                break;
            }

            k = ((k * 10) + ((c - 48)));
            this.pos++;
        }

        if (s) {
            k *= -1;
        }

        return k;
    }

    /**
     *
     * @return
     */
    public double readFloat() {
        int p1 = this.pos;
        while (true) {
            int p = this.pos;
            java.lang.String s = this.buf;
            int c = (((p < s.length())) ? (s.charAt(p)) : (-1));
            if ((c == -1)) {
                break;
            }

            if (((((c >= 43) && (c < 58)) || (c == 101)) || (c == 69))) {
                this.pos++;
            } else {
                break;
            }

        }

        return haxe.root.Std.parseFloat(haxe.lang.StringExt.substr(this.buf, p1, (this.pos - p1)));
    }

    /**
     *
     * @param o
     */
    public void unserializeObject(java.lang.Object o) {
        while (true) {
            if ((this.pos >= this.length)) {
                throw haxe.lang.HaxeException.wrap("Invalid object");
            }

            int p = this.pos;
            java.lang.String s = this.buf;
            if ((((((p < s.length())) ? (s.charAt(p)) : (-1))) == 103)) {
                break;
            }

            java.lang.String k = haxe.lang.Runtime.toString(this.unserialize());
            if (!((k instanceof java.lang.String))) {
                throw haxe.lang.HaxeException.wrap("Invalid object key");
            }

            java.lang.Object v = this.unserialize();
            haxe.root.Reflect.setField(o, k, v);
        }

        this.pos++;
    }

    /**
     *
     * @param edecl
     * @param tag
     * @return
     */
    public java.lang.Object unserializeEnum(java.lang.Class edecl, java.lang.String tag) {
        int p = this.pos++;
        java.lang.String s = this.buf;
        if ((((((p < s.length())) ? (s.charAt(p)) : (-1))) != 58)) {
            throw haxe.lang.HaxeException.wrap("Invalid enum format");
        }

        int nargs = this.readDigits();
        if ((nargs == 0)) {
            return (haxe.root.Type.createEnum(edecl, haxe.lang.Runtime.toString(tag), null));
        }

        haxe.root.Array args = new haxe.root.Array();
        while ((nargs-- > 0)) {
            args.push(this.unserialize());
        }

        return (haxe.root.Type.createEnum(edecl, haxe.lang.Runtime.toString(tag), args));
    }

    /**
     *
     * @return
     */
    public java.lang.Object unserialize() {
        {
            int p = this.pos++;
            java.lang.String s = this.buf;
            int _g = (((p < s.length())) ? (s.charAt(p)) : (-1));
            switch (_g) {
                case 65: {
                    java.lang.String name = haxe.lang.Runtime.toString(this.unserialize());
                    java.lang.Class cl = ((java.lang.Class) (haxe.lang.Runtime.callField(this.resolver, "resolveClass", new haxe.root.Array(new java.lang.Object[]{name}))));
                    if ((((java.lang.Object) (cl)) == ((java.lang.Object) (null)))) {
                        throw haxe.lang.HaxeException.wrap(("Class not found " + name));
                    }

                    return cl;
                }

                case 66: {
                    java.lang.String name1 = haxe.lang.Runtime.toString(this.unserialize());
                    java.lang.Class e = ((java.lang.Class) (haxe.lang.Runtime.callField(this.resolver, "resolveEnum", new haxe.root.Array(new java.lang.Object[]{name1}))));
                    if ((((java.lang.Object) (e)) == ((java.lang.Object) (null)))) {
                        throw haxe.lang.HaxeException.wrap(("Enum not found " + name1));
                    }

                    return e;
                }

                case 67: {
                    java.lang.String name2 = haxe.lang.Runtime.toString(this.unserialize());
                    java.lang.Class cl1 = ((java.lang.Class) (haxe.lang.Runtime.callField(this.resolver, "resolveClass", new haxe.root.Array(new java.lang.Object[]{name2}))));
                    if ((((java.lang.Object) (cl1)) == ((java.lang.Object) (null)))) {
                        throw haxe.lang.HaxeException.wrap(("Class not found " + name2));
                    }

                    java.lang.Object o = (haxe.root.Type.createEmptyInstance(cl1));
                    this.cache.push(o);
                    haxe.lang.Runtime.callField(o, "hxUnserialize", new haxe.root.Array(new java.lang.Object[]{this}));
                    int p1 = this.pos++;
                    java.lang.String s1 = this.buf;
                    if ((((((p1 < s1.length())) ? (s1.charAt(p1)) : (-1))) != 103)) {
                        throw haxe.lang.HaxeException.wrap("Invalid custom data");
                    }

                    return o;
                }

                case 77: {
                    haxe.ds.ObjectMap h = new haxe.ds.ObjectMap();
                    this.cache.push(h);
                    java.lang.String buf = this.buf;
                    while (true) {
                        int p2 = this.pos;
                        java.lang.String s2 = this.buf;
                        if (!(((((((p2 < s2.length())) ? (s2.charAt(p2)) : (-1))) != 104)))) {
                            break;
                        }

                        java.lang.Object s3 = this.unserialize();
                        h.set(s3, this.unserialize());
                    }

                    this.pos++;
                    return h;
                }

                case 82: {
                    int n = this.readDigits();
                    if (((n < 0) || (n >= this.scache.length))) {
                        throw haxe.lang.HaxeException.wrap("Invalid string reference");
                    }

                    return this.scache.get(n);
                }

                case 97: {
                    java.lang.String buf1 = this.buf;
                    haxe.root.Array a = new haxe.root.Array();
                    this.cache.push(a);
                    while (true) {
                        int p3 = this.pos;
                        java.lang.String s4 = this.buf;
                        int c = (((p3 < s4.length())) ? (s4.charAt(p3)) : (-1));
                        if ((c == 104)) {
                            this.pos++;
                            break;
                        }

                        if ((c == 117)) {
                            this.pos++;
                            int n1 = this.readDigits();
                            a.set(((a.length + n1) - 1), null);
                        } else {
                            a.push(this.unserialize());
                        }

                    }

                    return a;
                }

                case 98: {
                    haxe.ds.StringMap h1 = new haxe.ds.StringMap();
                    this.cache.push(h1);
                    java.lang.String buf2 = this.buf;
                    while (true) {
                        int p4 = this.pos;
                        java.lang.String s5 = this.buf;
                        if (!(((((((p4 < s5.length())) ? (s5.charAt(p4)) : (-1))) != 104)))) {
                            break;
                        }

                        java.lang.String s6 = haxe.lang.Runtime.toString(this.unserialize());
                        h1.set(s6, this.unserialize());
                    }

                    this.pos++;
                    return h1;
                }

                case 99: {
                    java.lang.String name3 = haxe.lang.Runtime.toString(this.unserialize());
                    java.lang.Class cl2 = ((java.lang.Class) (haxe.lang.Runtime.callField(this.resolver, "resolveClass", new haxe.root.Array(new java.lang.Object[]{name3}))));
                    if ((((java.lang.Object) (cl2)) == ((java.lang.Object) (null)))) {
                        throw haxe.lang.HaxeException.wrap(("Class not found " + name3));
                    }

                    java.lang.Object o1 = (haxe.root.Type.createEmptyInstance(cl2));
                    this.cache.push(o1);
                    this.unserializeObject(o1);
                    return o1;
                }

                case 100: {
                    return this.readFloat();
                }

                case 102: {
                    return false;
                }

                case 105: {
                    return this.readDigits();
                }

                case 106: {
                    java.lang.String name4 = haxe.lang.Runtime.toString(this.unserialize());
                    java.lang.Class edecl = ((java.lang.Class) (haxe.lang.Runtime.callField(this.resolver, "resolveEnum", new haxe.root.Array(new java.lang.Object[]{name4}))));
                    if ((((java.lang.Object) (edecl)) == ((java.lang.Object) (null)))) {
                        throw haxe.lang.HaxeException.wrap(("Enum not found " + name4));
                    }

                    this.pos++;
                    int index = this.readDigits();
                    java.lang.String tag = haxe.root.Type.getEnumConstructs(edecl).get(index);
                    if ((tag == null)) {
                        throw haxe.lang.HaxeException.wrap(((("Unknown enum index " + name4) + "@") + index));
                    }

                    java.lang.Object e1 = this.unserializeEnum(edecl, tag);
                    this.cache.push(e1);
                    return e1;
                }

                case 107: {
                    return java.lang.Double.NaN;
                }

                case 108: {
                    haxe.root.List l = new haxe.root.List();
                    this.cache.push(l);
                    java.lang.String buf3 = this.buf;
                    while (true) {
                        int p5 = this.pos;
                        java.lang.String s7 = this.buf;
                        if (!(((((((p5 < s7.length())) ? (s7.charAt(p5)) : (-1))) != 104)))) {
                            break;
                        }

                        l.add(this.unserialize());
                    }

                    this.pos++;
                    return l;
                }

                case 109: {
                    return java.lang.Double.NEGATIVE_INFINITY;
                }

                case 110: {
                    return null;
                }

                case 111: {
                    java.lang.Object o2 = new haxe.lang.DynamicObject(new java.lang.String[]{}, new java.lang.Object[]{}, new java.lang.String[]{}, new double[]{});
                    this.cache.push(o2);
                    this.unserializeObject(o2);
                    return o2;
                }

                case 112: {
                    return java.lang.Double.POSITIVE_INFINITY;
                }

                case 113: {
                    haxe.ds.IntMap h2 = new haxe.ds.IntMap();
                    this.cache.push(h2);
                    java.lang.String buf4 = this.buf;
                    int p6 = this.pos++;
                    java.lang.String s8 = this.buf;
                    int c1 = (((p6 < s8.length())) ? (s8.charAt(p6)) : (-1));
                    while ((c1 == 58)) {
                        int i = this.readDigits();
                        h2.set(i, this.unserialize());
                        int p7 = this.pos++;
                        java.lang.String s9 = this.buf;
                        if ((p7 < s9.length())) {
                            c1 = (s9.charAt(p7));
                        } else {
                            c1 = -1;
                        }

                    }

                    if ((c1 != 104)) {
                        throw haxe.lang.HaxeException.wrap("Invalid IntMap format");
                    }

                    return h2;
                }

                case 114: {
                    int n2 = this.readDigits();
                    if (((n2 < 0) || (n2 >= this.cache.length))) {
                        throw haxe.lang.HaxeException.wrap("Invalid reference");
                    }

                    return this.cache.get(n2);
                }

                case 115: {
                    int len = this.readDigits();
                    java.lang.String buf5 = this.buf;
                    boolean tmp = false;
                    int p8 = this.pos++;
                    java.lang.String s10 = this.buf;
                    if ((((((p8 < s10.length())) ? (s10.charAt(p8)) : (-1))) == 58)) {
                        tmp = ((this.length - this.pos) < len);
                    } else {
                        tmp = true;
                    }

                    if (tmp) {
                        throw haxe.lang.HaxeException.wrap("Invalid bytes length");
                    }

                    haxe.root.Array<java.lang.Object> codes = haxe.Unserializer.CODES;
                    if ((codes == null)) {
                        codes = haxe.Unserializer.initCodes();
                        haxe.Unserializer.CODES = codes;
                    }

                    int i1 = this.pos;
                    int rest = (len & 3);
                    int size = ((((len >> 2)) * 3) + ((((rest >= 2)) ? ((rest - 1)) : (0))));
                    int max = (i1 + ((len - rest)));
                    haxe.io.Bytes bytes = haxe.io.Bytes.alloc(size);
                    int bpos = 0;
                    while ((i1 < max)) {
                        int index1 = i1++;
                        int c11 = (haxe.lang.Runtime.toInt(codes.get(((index1 < buf5.length())) ? (buf5.charAt(index1)) : (-1))));
                        int index2 = i1++;
                        int c2 = (haxe.lang.Runtime.toInt(codes.get(((index2 < buf5.length())) ? (buf5.charAt(index2)) : (-1))));
                        bytes.b[bpos++] = ((byte) (((c11 << 2) | (c2 >> 4))));
                        int index3 = i1++;
                        int c3 = (haxe.lang.Runtime.toInt(codes.get(((index3 < buf5.length())) ? (buf5.charAt(index3)) : (-1))));
                        bytes.b[bpos++] = ((byte) (((c2 << 4) | (c3 >> 2))));
                        int index4 = i1++;
                        int c4 = (haxe.lang.Runtime.toInt(codes.get(((index4 < buf5.length())) ? (buf5.charAt(index4)) : (-1))));
                        bytes.b[bpos++] = ((byte) (((c3 << 6) | c4)));
                    }

                    if ((rest >= 2)) {
                        int index5 = i1++;
                        int c12 = (haxe.lang.Runtime.toInt(codes.get(((index5 < buf5.length())) ? (buf5.charAt(index5)) : (-1))));
                        int index6 = i1++;
                        int c21 = (haxe.lang.Runtime.toInt(codes.get(((index6 < buf5.length())) ? (buf5.charAt(index6)) : (-1))));
                        bytes.b[bpos++] = ((byte) (((c12 << 2) | (c21 >> 4))));
                        if ((rest == 3)) {
                            int index7 = i1++;
                            int c31 = (haxe.lang.Runtime.toInt(codes.get(((index7 < buf5.length())) ? (buf5.charAt(index7)) : (-1))));
                            bytes.b[bpos++] = ((byte) (((c21 << 4) | (c31 >> 2))));
                        }

                    }

                    this.pos += len;
                    this.cache.push(bytes);
                    return bytes;
                }

                case 116: {
                    return true;
                }

                case 118: {
                    haxe.root.Date d = null;
                    boolean tmp1 = false;
                    boolean tmp2 = false;
                    boolean tmp3 = false;
                    boolean tmp4 = false;
                    boolean tmp5 = false;
                    boolean tmp6 = false;
                    boolean tmp7 = false;
                    boolean tmp8 = false;
                    int p9 = this.pos;
                    java.lang.String s11 = this.buf;
                    if ((((((p9 < s11.length())) ? (s11.charAt(p9)) : (-1))) >= 48)) {
                        int p10 = this.pos;
                        java.lang.String s12 = this.buf;
                        tmp8 = (((((p10 < s12.length())) ? (s12.charAt(p10)) : (-1))) <= 57);
                    } else {
                        tmp8 = false;
                    }

                    if (tmp8) {
                        int p11 = (this.pos + 1);
                        java.lang.String s13 = this.buf;
                        tmp7 = (((((p11 < s13.length())) ? (s13.charAt(p11)) : (-1))) >= 48);
                    } else {
                        tmp7 = false;
                    }

                    if (tmp7) {
                        int p12 = (this.pos + 1);
                        java.lang.String s14 = this.buf;
                        tmp6 = (((((p12 < s14.length())) ? (s14.charAt(p12)) : (-1))) <= 57);
                    } else {
                        tmp6 = false;
                    }

                    if (tmp6) {
                        int p13 = (this.pos + 2);
                        java.lang.String s15 = this.buf;
                        tmp5 = (((((p13 < s15.length())) ? (s15.charAt(p13)) : (-1))) >= 48);
                    } else {
                        tmp5 = false;
                    }

                    if (tmp5) {
                        int p14 = (this.pos + 2);
                        java.lang.String s16 = this.buf;
                        tmp4 = (((((p14 < s16.length())) ? (s16.charAt(p14)) : (-1))) <= 57);
                    } else {
                        tmp4 = false;
                    }

                    if (tmp4) {
                        int p15 = (this.pos + 3);
                        java.lang.String s17 = this.buf;
                        tmp3 = (((((p15 < s17.length())) ? (s17.charAt(p15)) : (-1))) >= 48);
                    } else {
                        tmp3 = false;
                    }

                    if (tmp3) {
                        int p16 = (this.pos + 3);
                        java.lang.String s18 = this.buf;
                        tmp2 = (((((p16 < s18.length())) ? (s18.charAt(p16)) : (-1))) <= 57);
                    } else {
                        tmp2 = false;
                    }

                    if (tmp2) {
                        int p17 = (this.pos + 4);
                        java.lang.String s19 = this.buf;
                        tmp1 = (((((p17 < s19.length())) ? (s19.charAt(p17)) : (-1))) == 45);
                    } else {
                        tmp1 = false;
                    }

                    if (tmp1) {
                        d = haxe.root.Date.fromString(haxe.lang.StringExt.substr(this.buf, this.pos, 19));
                        this.pos += 19;
                    } else {
                        d = haxe.root.Date.fromTime(this.readFloat());
                    }

                    this.cache.push(d);
                    return d;
                }

                case 119: {
                    java.lang.String name5 = haxe.lang.Runtime.toString(this.unserialize());
                    java.lang.Class edecl1 = ((java.lang.Class) (haxe.lang.Runtime.callField(this.resolver, "resolveEnum", new haxe.root.Array(new java.lang.Object[]{name5}))));
                    if ((((java.lang.Object) (edecl1)) == ((java.lang.Object) (null)))) {
                        throw haxe.lang.HaxeException.wrap(("Enum not found " + name5));
                    }

                    java.lang.Object e2 = this.unserializeEnum(edecl1, haxe.lang.Runtime.toString(this.unserialize()));
                    this.cache.push(e2);
                    return e2;
                }

                case 120: {
                    throw haxe.lang.HaxeException.wrap(this.unserialize());
                }

                case 121: {
                    int len1 = this.readDigits();
                    boolean tmp9 = false;
                    int p18 = this.pos++;
                    java.lang.String s20 = this.buf;
                    if ((((((p18 < s20.length())) ? (s20.charAt(p18)) : (-1))) == 58)) {
                        tmp9 = ((this.length - this.pos) < len1);
                    } else {
                        tmp9 = true;
                    }

                    if (tmp9) {
                        throw haxe.lang.HaxeException.wrap("Invalid string length");
                    }

                    java.lang.String s21 = haxe.lang.StringExt.substr(this.buf, this.pos, len1);
                    this.pos += len1;
                    s21 = haxe.root.StringTools.urlDecode(s21);
                    this.scache.push(s21);
                    return s21;
                }

                case 122: {
                    return 0;
                }

                default: {
                    break;
                }

            }

        }

        this.pos--;
        throw haxe.lang.HaxeException.wrap(((("Invalid char " + haxe.lang.StringExt.charAt(this.buf, this.pos)) + " at position ") + this.pos));
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -341_328_890: {
                    if (field.equals("resolver")) {
                        __temp_executeDef1 = false;
                        this.resolver = (value);
                        return value;
                    }

                    break;
                }

                case 111_188: {
                    if (field.equals("pos")) {
                        __temp_executeDef1 = false;
                        this.pos = ((int) (value));
                        return value;
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        this.length = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -341_328_890: {
                    if (field.equals("resolver")) {
                        __temp_executeDef1 = false;
                        this.resolver = (value);
                        return value;
                    }

                    break;
                }

                case 97_907: {
                    if (field.equals("buf")) {
                        __temp_executeDef1 = false;
                        this.buf = haxe.lang.Runtime.toString(value);
                        return value;
                    }

                    break;
                }

                case -908_198_161: {
                    if (field.equals("scache")) {
                        __temp_executeDef1 = false;
                        this.scache = ((haxe.root.Array<java.lang.String>) (value));
                        return value;
                    }

                    break;
                }

                case 111_188: {
                    if (field.equals("pos")) {
                        __temp_executeDef1 = false;
                        this.pos = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 94_416_770: {
                    if (field.equals("cache")) {
                        __temp_executeDef1 = false;
                        this.cache = ((haxe.root.Array) (value));
                        return value;
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        this.length = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -505_039_769: {
                    if (field.equals("unserialize")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "unserialize"));
                    }

                    break;
                }

                case 97_907: {
                    if (field.equals("buf")) {
                        __temp_executeDef1 = false;
                        return this.buf;
                    }

                    break;
                }

                case 1_438_134_792: {
                    if (field.equals("unserializeEnum")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "unserializeEnum"));
                    }

                    break;
                }

                case 111_188: {
                    if (field.equals("pos")) {
                        __temp_executeDef1 = false;
                        return this.pos;
                    }

                    break;
                }

                case -657_057_146: {
                    if (field.equals("unserializeObject")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "unserializeObject"));
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return this.length;
                    }

                    break;
                }

                case -1_136_761_242: {
                    if (field.equals("readFloat")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "readFloat"));
                    }

                    break;
                }

                case 94_416_770: {
                    if (field.equals("cache")) {
                        __temp_executeDef1 = false;
                        return this.cache;
                    }

                    break;
                }

                case -940_119_524: {
                    if (field.equals("readDigits")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "readDigits"));
                    }

                    break;
                }

                case -908_198_161: {
                    if (field.equals("scache")) {
                        __temp_executeDef1 = false;
                        return this.scache;
                    }

                    break;
                }

                case -341_328_890: {
                    if (field.equals("resolver")) {
                        __temp_executeDef1 = false;
                        return this.resolver;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -341_328_890: {
                    if (field.equals("resolver")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this.resolver));
                    }

                    break;
                }

                case 111_188: {
                    if (field.equals("pos")) {
                        __temp_executeDef1 = false;
                        return (this.pos);
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return (this.length);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -505_039_769: {
                    if (field.equals("unserialize")) {
                        __temp_executeDef1 = false;
                        return this.unserialize();
                    }

                    break;
                }

                case -940_119_524: {
                    if (field.equals("readDigits")) {
                        __temp_executeDef1 = false;
                        return this.readDigits();
                    }

                    break;
                }

                case 1_438_134_792: {
                    if (field.equals("unserializeEnum")) {
                        __temp_executeDef1 = false;
                        return this.unserializeEnum(((java.lang.Class) (dynargs.get(0))), haxe.lang.Runtime.toString(dynargs.get(1)));
                    }

                    break;
                }

                case -1_136_761_242: {
                    if (field.equals("readFloat")) {
                        __temp_executeDef1 = false;
                        return this.readFloat();
                    }

                    break;
                }

                case -657_057_146: {
                    if (field.equals("unserializeObject")) {
                        __temp_executeDef1 = false;
                        this.unserializeObject(dynargs.get(0));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("resolver");
        baseArr.push("scache");
        baseArr.push("cache");
        baseArr.push("length");
        baseArr.push("pos");
        baseArr.push("buf");
        super.__hx_getFields(baseArr);
    }

}
