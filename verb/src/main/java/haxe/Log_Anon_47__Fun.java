package haxe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Log_Anon_47__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static haxe.Log_Anon_47__Fun __hx_current;

    /**
     *
     */
    public Log_Anon_47__Fun() {
        super(2, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke2_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        java.lang.Object infos = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : ((((__fn_dyn2 == null)) ? (null) : (__fn_dyn2))));
        java.lang.Object v = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1));
        java.lang.String str = null;
        if ((!((infos == null)))) {
            str = ((((haxe.lang.Runtime.toString(haxe.lang.Runtime.getField(infos, "fileName", true)) + ":") + ((int) (haxe.lang.Runtime.getField_f(infos, "lineNumber", true)))) + ": ") + haxe.root.Std.string(v));
            if ((((haxe.root.Array) (haxe.lang.Runtime.getField(infos, "customParams", true))) != null)) {
                str += ("," + ((haxe.root.Array) (haxe.lang.Runtime.getField(infos, "customParams", true))).join(","));
            }

        } else {
            str = haxe.lang.Runtime.toString(v);
        }

        return null;
    }

}
