package haxe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Log extends haxe.lang.HxObject {

    /**
     *
     */
    public static haxe.lang.Function trace;

    static {
        haxe.Log.trace = (((haxe.Log_Anon_47__Fun.__hx_current != null)) ? (haxe.Log_Anon_47__Fun.__hx_current) : (haxe.Log_Anon_47__Fun.__hx_current = (new haxe.Log_Anon_47__Fun())));
    }

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_haxe_Log(haxe.Log __hx_this) {
    }

    /**
     *
     * @param empty
     */
    public Log(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Log() {
        haxe.Log.__hx_ctor_haxe_Log(this);
    }

}
