package verb.geom;

import haxe.lang.Closure;
import haxe.lang.Runtime;
import haxe.lang.EmptyObject;
import haxe.root.Array;
import verb.core.Constants;
import verb.core.Interval;
import verb.core.Mat;
import verb.eval.Make;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Arc extends NurbsCurve {

    /**
     *
     * @param __hx_this
     * @param center
     * @param xaxis
     * @param yaxis
     * @param radius
     * @param minAngle
     * @param maxAngle
     */
    public static void __hx_ctor_verb_geom_Arc(
            Arc __hx_this,
            Array<Number> center,
            Array<Number> xaxis,
            Array<Number> yaxis,
            double radius, double minAngle, double maxAngle) {

        NurbsCurve.__hx_ctor_verb_geom_NurbsCurve(__hx_this, Make.arc(center, xaxis, yaxis, radius, minAngle, maxAngle));

        __hx_this._center = center;
        __hx_this._xaxis = xaxis;
        __hx_this._yaxis = yaxis;
        __hx_this._radius = radius;
        __hx_this._minAngle = minAngle;
        __hx_this._maxAngle = maxAngle;
    }

    /**
     *
     */
    public Array<Number> _center;

    /**
     *
     */
    public Array<Number> _xaxis;

    /**
     *
     */
    public Array<Number> _yaxis;

    /**
     *
     */
    public double _radius;

    /**
     *
     */
    public double _minAngle;

    /**
     *
     */
    public double _maxAngle;

    /**
     *
     * @param empty
     */
    public Arc(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param center
     * @param xaxis
     * @param yaxis
     * @param radius
     * @param minAngle
     * @param maxAngle
     */
    public Arc(Array<Number> center, Array<Number> xaxis, Array<Number> yaxis, double radius, double minAngle, double maxAngle) {
        super(EmptyObject.EMPTY);
        Arc.__hx_ctor_verb_geom_Arc(this, center, xaxis, yaxis, radius, minAngle, maxAngle);
    }

    /**
     *
     * @return
     */
    public Array<Number> center() {
        return this._center;
    }

    /**
     *
     * @return
     */
    public Array<Number> xaxis() {
        return this._xaxis;
    }

    /**
     *
     * @return
     */
    public Array<Number> yaxis() {
        return this._yaxis;
    }

    /**
     *
     * @return
     */
    public double radius() {
        return this._radius;
    }

    /**
     *
     * @return
     */
    public double minAngle() {
        return this._minAngle;
    }

    /**
     *
     * @return
     */
    public double maxAngle() {
        return this._maxAngle;
    }

    @Override
    public Array<NurbsCurve> split(double u) {
        Interval<Number> domain = domain();

        double uMax = domain.max.doubleValue();
        double uMin = domain.min.doubleValue();

        double splitFactor = ((u - uMin) / uMax);
        if (splitFactor > 1 - Constants.EPSILON
                || splitFactor < Constants.EPSILON) {

            return new Array();
        }
        double splitAngle = splitFactor * (_maxAngle - _minAngle) + _minAngle;

        Array curves = new Array(new NurbsCurve[2]);

        curves.set(0, new Arc(
                _center.copy(),
                _xaxis.copy(),
                _yaxis.copy(),
                _radius,
                _minAngle,
                splitAngle));

        curves.set(1, new Arc(
                _center.copy(),
                _xaxis.copy(),
                _yaxis.copy(),
                _radius,
                splitAngle,
                _maxAngle));

        return curves;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(String field, double value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 470_733_614: {
                if (field.equals("_maxAngle")) {
                    this._maxAngle = (value);
                    return value;
                }

                break;
            }

            case 1_769_892_273: {
                if (field.equals("_radius")) {
                    this._radius = (value);
                    return value;
                }

                break;
            }

            case -1_305_463_040: {
                if (field.equals("_minAngle")) {
                    this._minAngle = (value);
                    return value;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField_f(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 470_733_614: {
                if (field.equals("_maxAngle")) {
                    this._maxAngle = (Runtime.toDouble(value));
                    return value;
                }

                break;
            }

            case 1_344_457_076: {
                if (field.equals("_center")) {
                    this._center = ((Array<Number>) (value));
                    return value;
                }

                break;
            }

            case -1_305_463_040: {
                if (field.equals("_minAngle")) {
                    this._minAngle = (Runtime.toDouble(value));
                    return value;
                }

                break;
            }

            case -1_461_367_014: {
                if (field.equals("_xaxis")) {
                    this._xaxis = ((Array<Number>) (value));
                    return value;
                }

                break;
            }

            case 1_769_892_273: {
                if (field.equals("_radius")) {
                    this._radius = (Runtime.toDouble(value));
                    return value;
                }

                break;
            }

            case -1_460_443_493: {
                if (field.equals("_yaxis")) {
                    this._yaxis = ((Array<Number>) (value));
                    return value;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 380_215_759: {
                if (field.equals("maxAngle")) {
                    return (new Closure(this, "maxAngle"));
                }

                break;
            }

            case 1_344_457_076: {
                if (field.equals("_center")) {
                    return this._center;
                }

                break;
            }

            case -1_395_980_895: {
                if (field.equals("minAngle")) {
                    return (new Closure(this, "minAngle"));
                }

                break;
            }

            case -1_461_367_014: {
                if (field.equals("_xaxis")) {
                    return this._xaxis;
                }

                break;
            }

            case -938_578_798: {
                if (field.equals("radius")) {
                    return (new Closure(this, "radius"));
                }

                break;
            }

            case -1_460_443_493: {
                if (field.equals("_yaxis")) {
                    return this._yaxis;
                }

                break;
            }

            case 114_754_458: {
                if (field.equals("yaxis")) {
                    return (new Closure(this, "yaxis"));
                }

                break;
            }

            case 1_769_892_273: {
                if (field.equals("_radius")) {
                    return this._radius;
                }

                break;
            }

            case 113_830_937: {
                if (field.equals("xaxis")) {
                    return (new Closure(this, "xaxis"));
                }

                break;
            }

            case -1_305_463_040: {
                if (field.equals("_minAngle")) {
                    return this._minAngle;
                }

                break;
            }

            case -1_364_013_995: {
                if (field.equals("center")) {
                    return (new Closure(this, "center"));
                }

                break;
            }

            case 470_733_614: {
                if (field.equals("_maxAngle")) {
                    return this._maxAngle;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 470_733_614: {
                if (field.equals("_maxAngle")) {
                    return this._maxAngle;
                }

                break;
            }

            case 1_769_892_273: {
                if (field.equals("_radius")) {
                    return this._radius;
                }

                break;
            }

            case -1_305_463_040: {
                if (field.equals("_minAngle")) {
                    return this._minAngle;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField_f(field, throwErrors, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 380_215_759: {
                if (field.equals("maxAngle")) {
                    return this.maxAngle();
                }

                break;
            }

            case -1_364_013_995: {
                if (field.equals("center")) {
                    return this.center();
                }

                break;
            }

            case -1_395_980_895: {
                if (field.equals("minAngle")) {
                    return this.minAngle();
                }

                break;
            }

            case 113_830_937: {
                if (field.equals("xaxis")) {
                    return this.xaxis();
                }

                break;
            }

            case -938_578_798: {
                if (field.equals("radius")) {
                    return this.radius();
                }

                break;
            }

            case 114_754_458: {
                if (field.equals("yaxis")) {
                    return this.yaxis();
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_invokeField(field, dynargs);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("_maxAngle");
        baseArr.push("_minAngle");
        baseArr.push("_radius");
        baseArr.push("_yaxis");
        baseArr.push("_xaxis");
        baseArr.push("_center");
        super.__hx_getFields(baseArr);
    }

    @Override
    public NurbsCurve clone() {
        return super.clone();
    }

}
