package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsSurface_boundariesAsync_363__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.geom.NurbsSurface_boundariesAsync_363__Fun __hx_current;

    /**
     *
     */
    public NurbsSurface_boundariesAsync_363__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.root.Array<verb.core.NurbsCurveData> cs = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.root.Array<verb.core.NurbsCurveData>) (((java.lang.Object) (__fn_float1))))) : (((haxe.root.Array<verb.core.NurbsCurveData>) (__fn_dyn1))));
        return (cs.map(((verb.geom.NurbsSurface_boundariesAsync_364__Fun.__hx_current != null)) ? (verb.geom.NurbsSurface_boundariesAsync_364__Fun.__hx_current) : (verb.geom.NurbsSurface_boundariesAsync_364__Fun.__hx_current = (new verb.geom.NurbsSurface_boundariesAsync_364__Fun()))));
    }

}
