package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Ellipse extends verb.geom.EllipseArc {

    /**
     *
     * @param __hx_this
     * @param center
     * @param xaxis
     * @param yaxis
     */
    public static void __hx_ctor_verb_geom_Ellipse(verb.geom.Ellipse __hx_this, haxe.root.Array<java.lang.Number> center, haxe.root.Array<java.lang.Number> xaxis, haxe.root.Array<java.lang.Number> yaxis) {
        verb.geom.EllipseArc.__hx_ctor_verb_geom_EllipseArc(__hx_this, center, xaxis, yaxis, 0, java.lang.Math.PI * 2);
    }

    /**
     *
     * @param empty
     */
    public Ellipse(haxe.lang.EmptyObject empty) {
        super(haxe.lang.EmptyObject.EMPTY);
    }

    /**
     *
     * @param center
     * @param xaxis
     * @param yaxis
     */
    public Ellipse(haxe.root.Array<java.lang.Number> center, haxe.root.Array<java.lang.Number> xaxis, haxe.root.Array<java.lang.Number> yaxis) {
        super(haxe.lang.EmptyObject.EMPTY);
        verb.geom.Ellipse.__hx_ctor_verb_geom_Ellipse(this, center, xaxis, yaxis);
    }

    @Override
    public NurbsCurve clone() {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
