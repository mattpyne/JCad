package verb.geom;

import haxe.lang.EmptyObject;
import haxe.lang.Function;
import haxe.root.Array;
import verb.core.Constants;
import verb.core.Interval;
import verb.core.Mat;
import verb.core.NurbsCurveData;
import verb.eval.Divide;
import verb.eval.Eval;
import verb.eval.Make;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class BezierCurve extends NurbsCurve {

    /**
     *
     * @param __hx_this
     * @param points
     * @param weights
     */
    public static void __hx_ctor_verb_geom_BezierCurve(BezierCurve __hx_this, Array<Array<Number>> points, Array<Number> weights) {
        NurbsCurve.__hx_ctor_verb_geom_NurbsCurve(__hx_this, Make.rationalBezierCurve(points, weights));
    }

    /**
     *
     * @param empty
     */
    public BezierCurve(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param points
     * @param weights
     */
    public BezierCurve(Array<Array<Number>> points, Array<Number> weights) {
        super(EmptyObject.EMPTY);
        BezierCurve.__hx_ctor_verb_geom_BezierCurve(this, points, weights);
    }
    
    @Override
    public NurbsCurve transform(Array<Array<Number>> mat) {
        Array<Array<Number>> pts = Eval.dehomogenize1d(_data.controlPoints);
        Array<Number> weights = Eval.weight1d(_data.controlPoints);
        int _g1 = 0;
        int _g = pts.length;
        while (_g1 < _g) {
            int i = _g1++;
            Array<Number> homoPt = pts.get(i);
            homoPt.push(1.0);
            pts.set(i, (Mat.dot(mat, homoPt).slice(0, (homoPt.length - 1))));
        }
        return new BezierCurve(pts, weights);
    }

    @Override
    public Array<NurbsCurve> split(double u) {
        return Divide.curveSplit(this._data, u)
                .map(new Function(1, 0) {
                    @Override
                    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
                        NurbsCurveData nCurve = (NurbsCurveData) __fn_dyn1;

                        Array<Array<Number>> points = Eval.dehomogenize1d(nCurve.controlPoints);
                        Array<Number> weights = Eval.weight1d(nCurve.controlPoints);
                        
                        return new BezierCurve(points, weights);
                    }
                });
    }

    @Override
    public NurbsCurve clone() {
        return super.clone();
    }

}
