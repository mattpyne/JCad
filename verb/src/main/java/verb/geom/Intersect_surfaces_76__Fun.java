package verb.geom;

import verb.core.NurbsCurveData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_surfaces_76__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static final Intersect_surfaces_76__Fun __hx_current = new Intersect_surfaces_76__Fun();

    /**
     *
     */
    public Intersect_surfaces_76__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        NurbsCurveData cd = (__fn_dyn1 == haxe.lang.Runtime.undefined)
                ? (NurbsCurveData) ((Object) __fn_float1)
                : (NurbsCurveData) __fn_dyn1;
        return new NurbsCurve(cd);
    }

}
