package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsSurface_splitAsync_301__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.geom.NurbsSurface_splitAsync_301__Fun __hx_current;

    /**
     *
     */
    public NurbsSurface_splitAsync_301__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object s = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1));
        return (haxe.lang.Runtime.callField(s, "map", new haxe.root.Array(new java.lang.Object[]{((verb.geom.NurbsSurface_splitAsync_302__Fun.__hx_current != null)) ? (verb.geom.NurbsSurface_splitAsync_302__Fun.__hx_current) : (verb.geom.NurbsSurface_splitAsync_302__Fun.__hx_current = (new verb.geom.NurbsSurface_splitAsync_302__Fun()))})));
    }

}
