package verb.geom;

import verb.core.NurbsSurfaceData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsSurface_split_294__Fun extends haxe.lang.Function {

    /**
     *
     */
    public final static NurbsSurface_split_294__Fun __hx_current = new NurbsSurface_split_294__Fun();

    /**
     *
     */
    public NurbsSurface_split_294__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        return new NurbsSurface((NurbsSurfaceData) __fn_dyn1);
    }

}
