package verb.geom;

import haxe.lang.Closure;
import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.root.Array;
import promhx.Promise;
import verb.core.ArrayExtensions;
import verb.core.Interval;
import verb.core.NurbsCurveData;
import verb.core.SerializableBase;
import verb.eval.Analyze;
import verb.eval.Check;
import verb.eval.CurveLengthSample;
import verb.eval.Divide;
import verb.eval.Eval;
import verb.eval.Make;
import verb.eval.Modify;
import verb.eval.Tess;
import verb.exe.Dispatcher;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsCurve extends SerializableBase implements ICurve, Cloneable {

    /**
     *
     * @param __hx_this
     * @param data
     */
    public static void __hx_ctor_verb_geom_NurbsCurve(
            NurbsCurve __hx_this, 
            NurbsCurveData data) {
        
        __hx_this._data = Check.isValidNurbsCurveData(data);
    }
    
    public static NurbsCurve nurbsCurveNoCheck(NurbsCurveData data) {
        
        NurbsCurve curve = new NurbsCurve(EmptyObject.EMPTY);
        curve._data = data;
        
        return curve;
    } 

    /**
     *
     * @param degree
     * @param knots
     * @param controlPoints
     * @param weights
     * @return
     */
    public static NurbsCurve byKnotsControlPointsWeights(
            int degree, 
            Array<Number> knots, 
            Array<Array<Number>> controlPoints,
            Array<Number> weights) {
        
        return new NurbsCurve(new NurbsCurveData(degree,
                knots.copy(),
                Eval.homogenize1d(controlPoints, weights)));
    }

    /**
     *
     * @param points
     * @param degree
     * @return
     */
    public static NurbsCurve byPoints(
            Array<Array<Number>> points, 
            Object degree) {
        
        int __temp_degree27 = Runtime.eq(degree, null) ? 3 : Runtime.toInt(degree);
        return new NurbsCurve(Make.rationalInterpCurve(points, __temp_degree27, null, null, null));
    }

    /**
     *
     */
    public NurbsCurveData _data;

    /**
     *
     * @param empty
     */
    public NurbsCurve(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param data
     */
    public NurbsCurve(NurbsCurveData data) {
        NurbsCurve.__hx_ctor_verb_geom_NurbsCurve(this, data);
    }

    /**
     *
     * @return
     */
    public int degree() {
        return this._data.degree;
    }

    /**
     *
     * @return
     */
    public Array<Number> knots() {
        return (this._data.knots.slice(0, null));
    }

    /**
     *
     * @return
     */
    public Array<Array<Number>> controlPoints() {
        return Eval.dehomogenize1d(this._data.controlPoints);
    }

    /**
     *
     * @return
     */
    public Array<Number> weights() {
        return Eval.weight1d(this._data.controlPoints);
    }

    /**
     *
     * @return
     */
    @Override
    public NurbsCurveData asNurbs() {
        return new NurbsCurveData(
                this.degree(),
                this.knots(),
                Eval.homogenize1d(this.controlPoints(), this.weights()));
    }

    @Override
    public NurbsCurve clone() {
        return new NurbsCurve((this._data));
    }

    /**
     *
     * @return
     */
    @Override
    public Interval<Number> domain() {
        return new Interval(
                Runtime.toDouble(ArrayExtensions.first(this._data.knots)),
                Runtime.toDouble(ArrayExtensions.last(this._data.knots)));
    }

    /**
     *
     * @param mat
     * @return
     */
    public NurbsCurve transform(Array<Array<Number>> mat) {
        return new NurbsCurve(Modify.rationalCurveTransform(this._data, mat));
    }

    /**
     *
     * @param mat
     * @return
     */
    public Promise<NurbsCurve> transformAsync(Array<Array<Object>> mat) {
        return (Promise<NurbsCurve>) Dispatcher.dispatchMethod(
                Modify.class,
                Runtime.toString("rationalCurveTransform"),
                new Array(new Object[]{this._data, mat}))
                .<NurbsCurve>then(NurbsCurve_transformAsync_149__Fun.__hx_current);
    }

    /**
     *
     * @param u
     * @return
     */
    @Override
    public Array<Number> point(double u) {
        return Eval.rationalCurvePoint(this._data, u);
    }

    /**
     *
     * @param u
     * @return
     */
    public Promise<Array<Object>> pointAsync(double u) {
        return Dispatcher.dispatchMethod(
                Eval.class,
                Runtime.toString("rationalCurvePoint"),
                new Array(new Object[]{this._data, u}));
    }

    /**
     *
     * @param u
     * @return
     */
    public Array<Number> tangent(double u) {
        return Eval.rationalCurveTangent(this._data, u);
    }

    /**
     *
     * @param u
     * @return
     */
    public Promise<Array<Object>> tangentAsync(double u) {
        return Dispatcher.dispatchMethod(
                Eval.class,
                Runtime.toString("rationalCurveTangent"),
                new Array(new Object[]{this._data, u}));
    }

    /**
     *
     * @param u
     * @param numDerivs
     * @return
     */
    @Override
    public Array<Array<Number>> derivatives(double u, Object numDerivs) {
        int __temp_numDerivs25 = Runtime.eq(numDerivs, null) ? 1 : Runtime.toInt(numDerivs);
        return Eval.rationalCurveDerivatives(this._data, u, __temp_numDerivs25);
    }

    /**
     *
     * @param u
     * @param numDerivs
     * @return
     */
    public Promise<Array<Array<Object>>> derivativesAsync(double u, Object numDerivs) {
        int __temp_numDerivs26 = Runtime.eq(numDerivs, null) ? 1 : Runtime.toInt(numDerivs);
        return Dispatcher.dispatchMethod(
                Eval.class,
                Runtime.toString("rationalCurveDerivatives"),
                new Array(new Object[]{this._data, u, __temp_numDerivs26}));
    }

    /**
     *
     * @param pt
     * @return
     */
    public Array<Number> closestPoint(Array<Number> pt) {
        return Analyze.rationalCurveClosestPoint(this._data, pt);
    }

    /**
     *
     * @param pt
     * @return
     */
    public Promise<Array<Number>> closestPointAsync(Array<Number> pt) {
        return Dispatcher.dispatchMethod(
                Analyze.class,
                Runtime.toString("rationalCurveClosestPoint"),
                new Array(new Object[]{this._data, pt}));
    }

    /**
     *
     * @param pt
     * @return
     */
    public double closestParam(Array<Number> pt) {
        return Analyze.rationalCurveClosestParam(this._data, pt);
    }

    /**
     *
     * @param pt
     * @return
     */
    public Promise<Array<Object>> closestParamAsync(Object pt) {
        return Dispatcher.dispatchMethod(
                Analyze.class,
                Runtime.toString("rationalCurveClosestParam"),
                new Array(new Object[]{this._data, pt}));
    }

    /**
     *
     * @return
     */
    public double length() {
        return Analyze.rationalCurveArcLength(this._data, null, null);
    }

    /**
     *
     * @return
     */
    public Promise<Object> lengthAsync() {
        return Dispatcher.dispatchMethod(
                Analyze.class,
                Runtime.toString("rationalCurveArcLength"),
                new Array(new Object[]{this._data}));
    }

    /**
     *
     * @param u
     * @return
     */
    public double lengthAtParam(double u) {
        return Analyze.rationalCurveArcLength(this._data, u, null);
    }

    /**
     *
     * @return
     */
    public Promise<Object> lengthAtParamAsync() {
        return Dispatcher.dispatchMethod(
                Analyze.class,
                Runtime.toString("rationalCurveArcLength"),
                new Array(new Object[]{this._data}));
    }

    /**
     *
     * @param len
     * @param tolerance
     * @return
     */
    public double paramAtLength(double len, Object tolerance) {
        return Analyze.rationalCurveParamAtArcLength(this._data, len, tolerance, null, null);
    }

    /**
     *
     * @param len
     * @param tolerance
     * @return
     */
    public Promise<Object> paramAtLengthAsync(double len, Object tolerance) {
        return Dispatcher.dispatchMethod(
                Analyze.class,
                Runtime.toString("rationalCurveParamAtArcLength"),
                new Array(new Object[]{this._data, len, tolerance}));
    }

    /**
     *
     * @param divisions
     * @return
     */
    public Array<CurveLengthSample> divideByEqualArcLength(int divisions) {
        return Divide.rationalCurveByEqualArcLength(this._data, divisions);
    }

    /**
     *
     * @param divisions
     * @return
     */
    public Promise<Array<CurveLengthSample>> divideByEqualArcLengthAsync(int divisions) {
        return Dispatcher.dispatchMethod(
                Divide.class,
                Runtime.toString("rationalCurveByEqualArcLength"),
                new Array(new Object[]{this._data, divisions}));
    }

    /**
     *
     * @param arcLength
     * @return
     */
    public Array<CurveLengthSample> divideByArcLength(double arcLength) {
        return Divide.rationalCurveByArcLength(this._data, arcLength);
    }

    /**
     *
     * @param divisions
     * @return
     */
    public Promise<Array<CurveLengthSample>> divideByArcLengthAsync(int divisions) {
        return Dispatcher.dispatchMethod(
                Divide.class,
                Runtime.toString("rationalCurveByArcLength"),
                new Array(new Object[]{this._data, divisions}));
    }

    /**
     *
     * @param u
     * @return
     */
    public Array<NurbsCurve> split(double u) {
        return Divide.curveSplit(this._data, u).map(NurbsCurve_split_361__Fun.__hx_current);
    }

    /**
     *
     * @param u
     * @return
     */
    public Promise<Array<NurbsCurve>> splitAsync(double u) {
        return (Promise<Array<NurbsCurve>>) Dispatcher.dispatchMethod(
                Divide.class,
                Runtime.toString("curveSplit"),
                new Array(new Object[]{this._data, u}))
                .<Array<NurbsCurve>>then(NurbsCurve_splitAsync_368__Fun.__hx_current);
    }

    /**
     *
     * @return
     */
    public NurbsCurve reverse() {
        return new NurbsCurve(Modify.curveReverse(this._data));
    }

    /**
     *
     * @return
     */
    public Promise<NurbsCurve> reverseAsync() {
        return (Promise<NurbsCurve>) Dispatcher.dispatchMethod(
                Modify.class,
                Runtime.toString("curveReverse"),
                new Array(new Object[]{this._data}))
                .<NurbsCurve>then(NurbsCurve_reverseAsync_387__Fun.__hx_current);
    }

    /**
     *
     * @param tolerance
     * @return
     */
    public Array<Array<Object>> tessellate(Object tolerance) {
        return Tess.rationalCurveAdaptiveSample(this._data, tolerance, false);
    }

    /**
     *
     * @param tolerance
     * @return
     */
    public Promise<Array<Array<Object>>> tessellateAsync(Object tolerance) {
        return Dispatcher.dispatchMethod(
                Tess.class,
                Runtime.toString("rationalCurveAdaptiveSample"),
                new Array(new Object[]{this._data, tolerance, false}));
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {

        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 90_810_505: {
                if (field.equals("_data")) {
                    this._data = (NurbsCurveData) value;
                    return value;
                }

                break;
            }
        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -864_854_658: {
                    if (field.equals("tessellateAsync")) {
                        __temp_executeDef1 = false;
                        return new Closure(this, "tessellateAsync");
                    }

                    break;
                }

                case 90_810_505: {
                    if (field.equals("_data")) {
                        __temp_executeDef1 = false;
                        return this._data;
                    }

                    break;
                }

                case 1_347_995_550: {
                    if (field.equals("tessellate")) {
                        __temp_executeDef1 = false;
                        return new Closure(this, "tessellate");
                    }

                    break;
                }

                case -1_335_595_316: {
                    if (field.equals("degree")) {
                        __temp_executeDef1 = false;
                        return new Closure(this, "degree");
                    }

                    break;
                }

                case -100_254_534: {
                    if (field.equals("reverseAsync")) {
                        __temp_executeDef1 = false;
                        return new Closure(this, "reverseAsync");
                    }

                    break;
                }

                case 102_204_139: {
                    if (field.equals("knots")) {
                        __temp_executeDef1 = false;
                        return new Closure(this, "knots");
                    }

                    break;
                }

                case 1_099_846_370: {
                    if (field.equals("reverse")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "reverse"));
                    }

                    break;
                }

                case 793_539_744: {
                    if (field.equals("controlPoints")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "controlPoints"));
                    }

                    break;
                }

                case -367_536_254: {
                    if (field.equals("splitAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "splitAsync"));
                    }

                    break;
                }

                case 1_230_441_723: {
                    if (field.equals("weights")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "weights"));
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "split"));
                    }

                    break;
                }

                case -738_470_902: {
                    if (field.equals("asNurbs")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "asNurbs"));
                    }

                    break;
                }

                case 1_603_682_388: {
                    if (field.equals("divideByArcLengthAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "divideByArcLengthAsync"));
                    }

                    break;
                }

                case 94_756_189: {
                    if (field.equals("clone")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "clone"));
                    }

                    break;
                }

                case 140_310_280: {
                    if (field.equals("divideByArcLength")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "divideByArcLength"));
                    }

                    break;
                }

                case -1_326_197_564: {
                    if (field.equals("domain")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "domain"));
                    }

                    break;
                }

                case -128_006_904: {
                    if (field.equals("divideByEqualArcLengthAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "divideByEqualArcLengthAsync"));
                    }

                    break;
                }

                case 1_052_666_732: {
                    if (field.equals("transform")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "transform"));
                    }

                    break;
                }

                case 492_392_916: {
                    if (field.equals("divideByEqualArcLength")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "divideByEqualArcLength"));
                    }

                    break;
                }

                case -1_700_664_720: {
                    if (field.equals("transformAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "transformAsync"));
                    }

                    break;
                }

                case 166_652_278: {
                    if (field.equals("paramAtLengthAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "paramAtLengthAsync"));
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "point"));
                    }

                    break;
                }

                case 576_954_534: {
                    if (field.equals("paramAtLength")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "paramAtLength"));
                    }

                    break;
                }

                case 1_238_546_124: {
                    if (field.equals("pointAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "pointAsync"));
                    }

                    break;
                }

                case 237_581_448: {
                    if (field.equals("lengthAtParamAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "lengthAtParamAsync"));
                    }

                    break;
                }

                case -1_541_971_387: {
                    if (field.equals("tangent")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "tangent"));
                    }

                    break;
                }

                case -2_019_085_740: {
                    if (field.equals("lengthAtParam")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "lengthAtParam"));
                    }

                    break;
                }

                case 113_136_439: {
                    if (field.equals("tangentAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "tangentAsync"));
                    }

                    break;
                }

                case 1_611_314_998: {
                    if (field.equals("lengthAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "lengthAsync"));
                    }

                    break;
                }

                case 979_228_620: {
                    if (field.equals("derivatives")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "derivatives"));
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "length"));
                    }

                    break;
                }

                case -107_529_712: {
                    if (field.equals("derivativesAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "derivativesAsync"));
                    }

                    break;
                }

                case 1_582_111_016: {
                    if (field.equals("closestParamAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "closestParamAsync"));
                    }

                    break;
                }

                case -627_759_433: {
                    if (field.equals("closestPoint")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "closestPoint"));
                    }

                    break;
                }

                case -628_168_268: {
                    if (field.equals("closestParam")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "closestParam"));
                    }

                    break;
                }

                case -1_899_788_795: {
                    if (field.equals("closestPointAsync")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "closestPointAsync"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -864_854_658: {
                    if (field.equals("tessellateAsync")) {
                        __temp_executeDef1 = false;
                        return this.tessellateAsync(dynargs.get(0));
                    }

                    break;
                }

                case -1_335_595_316: {
                    if (field.equals("degree")) {
                        __temp_executeDef1 = false;
                        return this.degree();
                    }

                    break;
                }

                case 1_347_995_550: {
                    if (field.equals("tessellate")) {
                        __temp_executeDef1 = false;
                        return this.tessellate(dynargs.get(0));
                    }

                    break;
                }

                case 102_204_139: {
                    if (field.equals("knots")) {
                        __temp_executeDef1 = false;
                        return this.knots();
                    }

                    break;
                }

                case -100_254_534: {
                    if (field.equals("reverseAsync")) {
                        __temp_executeDef1 = false;
                        return this.reverseAsync();
                    }

                    break;
                }

                case 793_539_744: {
                    if (field.equals("controlPoints")) {
                        __temp_executeDef1 = false;
                        return this.controlPoints();
                    }

                    break;
                }

                case 1_099_846_370: {
                    if (field.equals("reverse")) {
                        __temp_executeDef1 = false;
                        return this.reverse();
                    }

                    break;
                }

                case 1_230_441_723: {
                    if (field.equals("weights")) {
                        __temp_executeDef1 = false;
                        return this.weights();
                    }

                    break;
                }

                case -367_536_254: {
                    if (field.equals("splitAsync")) {
                        __temp_executeDef1 = false;
                        return this.splitAsync((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case -738_470_902: {
                    if (field.equals("asNurbs")) {
                        __temp_executeDef1 = false;
                        return this.asNurbs();
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return this.split((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case 94_756_189: {
                    if (field.equals("clone")) {
                        __temp_executeDef1 = false;
                        return this.clone();
                    }

                    break;
                }

                case 1_603_682_388: {
                    if (field.equals("divideByArcLengthAsync")) {
                        __temp_executeDef1 = false;
                        return this.divideByArcLengthAsync((Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case -1_326_197_564: {
                    if (field.equals("domain")) {
                        __temp_executeDef1 = false;
                        return this.domain();
                    }

                    break;
                }

                case 140_310_280: {
                    if (field.equals("divideByArcLength")) {
                        __temp_executeDef1 = false;
                        return this.divideByArcLength((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case 1_052_666_732: {
                    if (field.equals("transform")) {
                        __temp_executeDef1 = false;
                        return this.transform(((Array<Array<Number>>) (dynargs.get(0))));
                    }

                    break;
                }

                case -128_006_904: {
                    if (field.equals("divideByEqualArcLengthAsync")) {
                        __temp_executeDef1 = false;
                        return this.divideByEqualArcLengthAsync((Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case -1_700_664_720: {
                    if (field.equals("transformAsync")) {
                        __temp_executeDef1 = false;
                        return this.transformAsync(((Array<Array<Object>>) (dynargs.get(0))));
                    }

                    break;
                }

                case 492_392_916: {
                    if (field.equals("divideByEqualArcLength")) {
                        __temp_executeDef1 = false;
                        return this.divideByEqualArcLength((Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case 166_652_278: {
                    if (field.equals("paramAtLengthAsync")) {
                        __temp_executeDef1 = false;
                        return this.paramAtLengthAsync((Runtime.toDouble(dynargs.get(0))), dynargs.get(1));
                    }

                    break;
                }

                case 1_238_546_124: {
                    if (field.equals("pointAsync")) {
                        __temp_executeDef1 = false;
                        return this.pointAsync((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case 576_954_534: {
                    if (field.equals("paramAtLength")) {
                        __temp_executeDef1 = false;
                        return this.paramAtLength((Runtime.toDouble(dynargs.get(0))), dynargs.get(1));
                    }

                    break;
                }

                case -1_541_971_387: {
                    if (field.equals("tangent")) {
                        __temp_executeDef1 = false;
                        return this.tangent((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case 237_581_448: {
                    if (field.equals("lengthAtParamAsync")) {
                        __temp_executeDef1 = false;
                        return this.lengthAtParamAsync();
                    }

                    break;
                }

                case 113_136_439: {
                    if (field.equals("tangentAsync")) {
                        __temp_executeDef1 = false;
                        return this.tangentAsync((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case -2_019_085_740: {
                    if (field.equals("lengthAtParam")) {
                        __temp_executeDef1 = false;
                        return this.lengthAtParam((Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case 979_228_620: {
                    if (field.equals("derivatives")) {
                        __temp_executeDef1 = false;
                        return this.derivatives((Runtime.toDouble(dynargs.get(0))), dynargs.get(1));
                    }

                    break;
                }

                case 1_611_314_998: {
                    if (field.equals("lengthAsync")) {
                        __temp_executeDef1 = false;
                        return this.lengthAsync();
                    }

                    break;
                }

                case -107_529_712: {
                    if (field.equals("derivativesAsync")) {
                        __temp_executeDef1 = false;
                        return this.derivativesAsync((Runtime.toDouble(dynargs.get(0))), dynargs.get(1));
                    }

                    break;
                }

                case -1_106_363_674: {
                    if (field.equals("length")) {
                        __temp_executeDef1 = false;
                        return this.length();
                    }

                    break;
                }

                case -627_759_433: {
                    if (field.equals("closestPoint")) {
                        __temp_executeDef1 = false;
                        return this.closestPoint(((Array<Number>) (dynargs.get(0))));
                    }

                    break;
                }

                case 1_582_111_016: {
                    if (field.equals("closestParamAsync")) {
                        __temp_executeDef1 = false;
                        return this.closestParamAsync(dynargs.get(0));
                    }

                    break;
                }

                case -1_899_788_795: {
                    if (field.equals("closestPointAsync")) {
                        __temp_executeDef1 = false;
                        return this.closestPointAsync(((Array<Number>) (dynargs.get(0))));
                    }

                    break;
                }

                case -628_168_268: {
                    if (field.equals("closestParam")) {
                        __temp_executeDef1 = false;
                        return this.closestParam(((Array<Number>) (dynargs.get(0))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("_data");
        super.__hx_getFields(baseArr);
    }

    @Override
    public String toString() {
        return "NurbsCurve{" + "_data=" + _data + '}';
    }

}
