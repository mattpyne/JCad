package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class SweptSurface extends verb.geom.NurbsSurface {

    /**
     *
     * @param __hx_this
     * @param profile
     * @param rail
     */
    public static void __hx_ctor_verb_geom_SweptSurface(verb.geom.SweptSurface __hx_this, verb.geom.ICurve profile, verb.geom.ICurve rail) {
        verb.geom.NurbsSurface.__hx_ctor_verb_geom_NurbsSurface(__hx_this, verb.eval.Make.rationalTranslationalSurface(profile.asNurbs(), rail.asNurbs()));
        __hx_this._profile = profile;
        __hx_this._rail = rail;
    }

    /**
     *
     */
    public verb.geom.ICurve _profile;

    /**
     *
     */
    public verb.geom.ICurve _rail;

    /**
     *
     * @param empty
     */
    public SweptSurface(haxe.lang.EmptyObject empty) {
        super(haxe.lang.EmptyObject.EMPTY);
    }

    /**
     *
     * @param profile
     * @param rail
     */
    public SweptSurface(verb.geom.ICurve profile, verb.geom.ICurve rail) {
        super(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY)));
        verb.geom.SweptSurface.__hx_ctor_verb_geom_SweptSurface(this, profile, rail);
    }

    /**
     *
     * @return
     */
    public verb.geom.ICurve profile() {
        return this._profile;
    }

    /**
     *
     * @return
     */
    public verb.geom.ICurve rail() {
        return this._rail;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 91_227_249: {
                    if (field.equals("_rail")) {
                        __temp_executeDef1 = false;
                        this._rail = ((verb.geom.ICurve) (value));
                        return value;
                    }

                    break;
                }

                case 2_048_798_826: {
                    if (field.equals("_profile")) {
                        __temp_executeDef1 = false;
                        this._profile = ((verb.geom.ICurve) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_492_754: {
                    if (field.equals("rail")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "rail"));
                    }

                    break;
                }

                case 2_048_798_826: {
                    if (field.equals("_profile")) {
                        __temp_executeDef1 = false;
                        return this._profile;
                    }

                    break;
                }

                case -309_425_751: {
                    if (field.equals("profile")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "profile"));
                    }

                    break;
                }

                case 91_227_249: {
                    if (field.equals("_rail")) {
                        __temp_executeDef1 = false;
                        return this._rail;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_492_754: {
                    if (field.equals("rail")) {
                        __temp_executeDef1 = false;
                        return this.rail();
                    }

                    break;
                }

                case -309_425_751: {
                    if (field.equals("profile")) {
                        __temp_executeDef1 = false;
                        return this.profile();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("_rail");
        baseArr.push("_profile");
        super.__hx_getFields(baseArr);
    }

    @Override
    public NurbsSurface clone() {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
