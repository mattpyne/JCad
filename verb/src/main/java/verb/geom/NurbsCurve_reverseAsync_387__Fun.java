package verb.geom;

import haxe.lang.Function;
import haxe.lang.Runtime ;
import verb.core.NurbsCurveData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsCurve_reverseAsync_387__Fun extends Function {

    /**
     *
     */
    public static final NurbsCurve_reverseAsync_387__Fun __hx_current = new NurbsCurve_reverseAsync_387__Fun();

    /**
     *
     */
    public NurbsCurve_reverseAsync_387__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        NurbsCurveData c = (__fn_dyn1 == Runtime.undefined) 
                ? (NurbsCurveData) ((Object) __fn_float1) 
                : (NurbsCurveData) __fn_dyn1;
        return new NurbsCurve((c));
    }

}
