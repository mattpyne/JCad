package verb.geom;

import haxe.lang.EmptyObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Circle extends Arc {

    /**
     *
     * @param __hx_this
     * @param center
     * @param xaxis
     * @param yaxis
     * @param radius
     */
    public static void __hx_ctor_verb_geom_Circle(
            Circle __hx_this, Array<Number> center, 
            Array<Number> xaxis,
            Array<Number> yaxis, 
            double radius) {
        
        Arc.__hx_ctor_verb_geom_Arc(__hx_this, center, xaxis, yaxis, radius, 0, Math.PI * 2);
    }

    /**
     *
     * @param empty
     */
    public Circle(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param center
     * @param xaxis
     * @param yaxis
     * @param radius
     */
    public Circle(Array<Number> center, Array<Number> xaxis, Array<Number> yaxis, double radius) {
        super(EmptyObject.EMPTY);
        Circle.__hx_ctor_verb_geom_Circle(this, center, xaxis, yaxis, radius);
    }

    @Override
    public NurbsCurve clone() {
        return super.clone();
    }

}
