package verb.geom;

import haxe.lang.Function;
import haxe.lang.Runtime;
import haxe.root.Array;
import verb.core.NurbsCurveData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsCurve_splitAsync_368__Fun extends Function {

    /**
     *
     */
    public static final NurbsCurve_splitAsync_368__Fun __hx_current = new NurbsCurve_splitAsync_368__Fun();

    /**
     *
     */
    public NurbsCurve_splitAsync_368__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        Array<NurbsCurveData> cs = (__fn_dyn1 == Runtime.undefined) 
                ? (Array<NurbsCurveData>) ((Object) __fn_float1) 
                : (Array<NurbsCurveData>) __fn_dyn1;
        return cs.map(NurbsCurve_splitAsync_369__Fun.__hx_current);
    }

}
