package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsSurface_boundaries_356__Fun extends haxe.lang.Function {

    /**
     *
     */
    public final static verb.geom.NurbsSurface_boundaries_356__Fun __hx_current = new NurbsSurface_boundaries_356__Fun();

    /**
     *
     */
    public NurbsSurface_boundaries_356__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        verb.core.NurbsCurveData x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((verb.core.NurbsCurveData) (((java.lang.Object) (__fn_float1))))) : (((verb.core.NurbsCurveData) (__fn_dyn1))));
        return new verb.geom.NurbsCurve((x));
    }

}
