package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_surfacesAsync_83__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.geom.Intersect_surfacesAsync_83__Fun __hx_current;

    /**
     *
     */
    public Intersect_surfacesAsync_83__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object cds = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1));
        return (haxe.lang.Runtime.callField(cds, "map", new haxe.root.Array(new java.lang.Object[]{((verb.geom.Intersect_surfacesAsync_84__Fun.__hx_current != null)) ? (verb.geom.Intersect_surfacesAsync_84__Fun.__hx_current) : (verb.geom.Intersect_surfacesAsync_84__Fun.__hx_current = (new verb.geom.Intersect_surfacesAsync_84__Fun()))})));
    }

}
