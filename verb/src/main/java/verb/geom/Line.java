package verb.geom;

import haxe.lang.Closure;
import haxe.lang.EmptyObject;
import haxe.root.Array;
import verb.core.Constants;
import verb.core.Interval;
import verb.core.Mat;
import verb.core.Vec;
import verb.eval.Make;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Line extends NurbsCurve {

    /**
     *
     * @param __hx_this
     * @param start
     * @param end
     */
    public static void __hx_ctor_verb_geom_Line(Line __hx_this, Array<Number> start, Array<Number> end) {
        NurbsCurve.__hx_ctor_verb_geom_NurbsCurve(__hx_this, Make.polyline(new Array(new Array[]{start, end})));
        __hx_this._start = start;
        __hx_this._end = end;
    }

    /**
     *
     */
    public Array<Number> _start;

    /**
     *
     */
    public Array<Number> _end;

    /**
     *
     * @param empty
     */
    public Line(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param start
     * @param end
     */
    public Line(Array<Number> start, Array<Number> end) {
        super(EmptyObject.EMPTY);
        Line.__hx_ctor_verb_geom_Line(this, start, end);
    }

    /**
     *
     * @return
     */
    public Array<Number> start() {
        return this._start;
    }

    /**
     *
     * @return
     */
    public Array<Number> end() {
        return this._end;
    }

    @Override
    public NurbsCurve transform(Array<Array<Number>> mat) {

        Array<Number> end = _end.copy();
        end.push(1.0);
        end = Mat.dot(mat, end).slice(0, (end.length - 1));

        Array<Number> start = _start.copy();
        start.push(1.0);
        start = Mat.dot(mat, start).slice(0, (start.length - 1));

        return new Line(start, end);
    }

    @Override
    public Array<NurbsCurve> split(double u) {
        Interval<Number> domain = domain();

        double uMax = domain.max.doubleValue();
        double uMin = domain.min.doubleValue();

        double splitFactor = ((u - uMin) / uMax);
        if (splitFactor > 1 - Constants.EPSILON
                || splitFactor < Constants.EPSILON) {

            return new Array();
        }
        Array splitPoint = Vec.add(_start, Vec.mul(splitFactor, Vec.sub(_end, _start)));

        Array curves = new Array(new NurbsCurve[2]);

        curves.set(0, new Line(_start.copy(), splitPoint));

        curves.set(1, new Line(splitPoint.copy(), _end.copy()));

        return curves;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 2_930_716: {
                    if (field.equals("_end")) {
                        __temp_executeDef1 = false;
                        this._end = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

                case -1_465_440_413: {
                    if (field.equals("_start")) {
                        __temp_executeDef1 = false;
                        this._start = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 100_571: {
                    if (field.equals("end")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "end"));
                    }

                    break;
                }

                case -1_465_440_413: {
                    if (field.equals("_start")) {
                        __temp_executeDef1 = false;
                        return this._start;
                    }

                    break;
                }

                case 109_757_538: {
                    if (field.equals("start")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "start"));
                    }

                    break;
                }

                case 2_930_716: {
                    if (field.equals("_end")) {
                        __temp_executeDef1 = false;
                        return this._end;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 100_571: {
                    if (field.equals("end")) {
                        __temp_executeDef1 = false;
                        return this.end();
                    }

                    break;
                }

                case 109_757_538: {
                    if (field.equals("start")) {
                        __temp_executeDef1 = false;
                        return this.start();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("_end");
        baseArr.push("_start");
        super.__hx_getFields(baseArr);
    }

    @Override
    public NurbsCurve clone() {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
