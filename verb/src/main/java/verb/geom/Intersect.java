package verb.geom;

import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.lang.HxObject;
import haxe.root.Array;
import verb.core.CurveCurveIntersection;
import verb.core.CurveSurfaceIntersection;
import verb.exe.Dispatcher;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect extends HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_geom_Intersect(Intersect __hx_this) {
    }

    /**
     *
     * @param first
     * @param second
     * @param tol
     * @return
     */
    public static Array<CurveCurveIntersection> curves(ICurve first, ICurve second, Object tol) {
        double __temp_tol36 = ((Runtime.eq(tol, null)) ? (1e-3) : (Runtime.toDouble(tol)));
        return verb.eval.Intersect.curves(first.asNurbs(), second.asNurbs(), __temp_tol36);
    }

    /**
     *
     * @param first
     * @param second
     * @param tol
     * @return
     */
    public static promhx.Promise<Array<CurveCurveIntersection>> curvesAsync(ICurve first, ICurve second, Object tol) {
        double __temp_tol37 = Runtime.eq(tol, null) ? 1e-3 : Runtime.toDouble(tol);
        return (((promhx.Promise) (Dispatcher.dispatchMethod(verb.eval.Intersect.class, Runtime.toString("curves"), new Array(new Object[]{first.asNurbs(), second.asNurbs(), __temp_tol37})))));
    }

    /**
     *
     * @param curve
     * @param surface
     * @param tol
     * @return
     */
    public static Array<CurveSurfaceIntersection> curveAndSurface(ICurve curve, ISurface surface, Object tol) {
        double __temp_tol38 = ((Runtime.eq(tol, null)) ? (1e-3) : (Runtime.toDouble(tol)));
        return verb.eval.Intersect.curveAndSurface(curve.asNurbs(), surface.asNurbs(), __temp_tol38, null, null);
    }

    /**
     *
     * @param curve
     * @param surface
     * @param tol
     * @return
     */
    public static promhx.Promise<Array<CurveSurfaceIntersection>> curveAndSurfaceAsync(ICurve curve, ISurface surface, Object tol) {
        double __temp_tol39 = ((Runtime.eq(tol, null)) ? (1e-3) : (Runtime.toDouble(tol)));
        return (((promhx.Promise) (Dispatcher.dispatchMethod(verb.eval.Intersect.class, Runtime.toString("curveAndSurface"), new Array(new Object[]{curve.asNurbs(), surface.asNurbs(), __temp_tol39})))));
    }

    /**
     *
     * @param first
     * @param second
     * @param tol
     * @return
     */
    public static Array<NurbsCurve> surfaces(ISurface first, ISurface second, Object tol) {
        double __temp_tol40 = Runtime.eq(tol, null) ? 1e-3 : Runtime.toDouble(tol);
        return verb.eval.Intersect
                .surfaces(first.asNurbs(), second.asNurbs(), __temp_tol40)
                .map(Intersect_surfaces_76__Fun.__hx_current);
    }

    /**
     *
     * @param first
     * @param second
     * @param tol
     * @return
     */
    public static promhx.Promise<Array<NurbsCurve>> surfacesAsync(ISurface first, ISurface second, Object tol) {
        double __temp_tol41 = ((Runtime.eq(tol, null)) ? (1e-3) : (Runtime.toDouble(tol)));
        return ((promhx.Promise<Array<NurbsCurve>>) (((Dispatcher.dispatchMethod((verb.eval.Intersect.class),
                Runtime.toString("surfaces"),
                (new Array(
                        new Object[]{first.asNurbs(), second.asNurbs(), __temp_tol41})))))
                .<Array<NurbsCurve>>then(((((Intersect_surfacesAsync_83__Fun.__hx_current != null)) ? (Intersect_surfacesAsync_83__Fun.__hx_current) : (Intersect_surfacesAsync_83__Fun.__hx_current = (new Intersect_surfacesAsync_83__Fun())))))));
    }

    /**
     *
     * @param empty
     */
    public Intersect(EmptyObject empty) {
    }

    /**
     *
     */
    public Intersect() {
        Intersect.__hx_ctor_verb_geom_Intersect(this);
    }

}
