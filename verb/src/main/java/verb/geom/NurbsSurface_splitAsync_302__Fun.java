package verb.geom;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsSurface_splitAsync_302__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.geom.NurbsSurface_splitAsync_302__Fun __hx_current;

    /**
     *
     */
    public NurbsSurface_splitAsync_302__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        verb.core.NurbsSurfaceData x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((verb.core.NurbsSurfaceData) (((java.lang.Object) (__fn_float1))))) : (((verb.core.NurbsSurfaceData) (__fn_dyn1))));
        return new verb.geom.NurbsSurface((x));
    }

}
