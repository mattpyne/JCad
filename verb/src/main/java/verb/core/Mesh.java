package verb.core;

import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.lang.HxObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Mesh extends HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_core_Mesh(Mesh __hx_this) {
    }

    /**
     *
     * @param points
     * @param tri
     * @return
     */
    public static Array<Number> getTriangleNorm(Array<Array<Number>> points, Array<Number> tri) {
        Array<Number> v0 = points.get(Runtime.toInt(tri.get(0)));
        Array<Number> v1 = points.get(Runtime.toInt(tri.get(1)));
        Array<Number> v2 = points.get(Runtime.toInt(tri.get(2)));
        Array<Number> u = Vec.sub(v1, v0);
        Array<Number> v = Vec.sub(v2, v0);
        Array<Number> n = Vec.cross(u, v);
        return Vec.mul(1 / Vec.norm(n), n);
    }

    /**
     *
     * @param mesh
     * @param faceIndices
     * @return
     */
    public static BoundingBox makeMeshAabb(MeshData mesh, Array<Number> faceIndices) {
        BoundingBox bb = new BoundingBox(EmptyObject.EMPTY);
        
        for (int i = 0; i < faceIndices.length; i++) {
            
            int x = Runtime.toInt(faceIndices.get(i));
            bb.add(mesh.points.get(Runtime.toInt(mesh.faces.get(x).get(0))));
            bb.add(mesh.points.get(Runtime.toInt(mesh.faces.get(x).get(1))));
            bb.add(mesh.points.get(Runtime.toInt(mesh.faces.get(x).get(2))));
        }

        return bb;
    }

    /**
     *
     * @param bb
     * @param mesh
     * @param faceIndices
     * @return
     */
    public static Array<Number> sortTrianglesOnLongestAxis(BoundingBox bb, MeshData mesh, Array<Number> faceIndices) {
        int longAxis = bb.getLongestAxis();
        
        Array<Pair<Number, Number>> minCoordFaceMap = new Array<>(new Pair[faceIndices.length]);
        for (int i = 0; i < faceIndices.length; i++) {

            int faceIndex = Runtime.toInt(faceIndices.get(i));
            double tri_min = Mesh.getMinCoordOnAxis(mesh.points, mesh.faces.get(faceIndex), longAxis);
            minCoordFaceMap.set(i, new Pair(tri_min, faceIndex));
        }

        minCoordFaceMap.sort(Mesh_sortTrianglesOnLongestAxis_86__Fun.__hx_current);
        Array<Number> sortedFaceIndices = new Array(new Number[minCoordFaceMap.length]);
        for (int i = 0; i < minCoordFaceMap.length; i++) {

            sortedFaceIndices.set(i, Runtime.toInt(minCoordFaceMap.get(i).item1));
        }

        return sortedFaceIndices;
    }

    /**
     *
     * @param points
     * @param tri
     * @param axis
     * @return
     */
    public static double getMinCoordOnAxis(Array<Array<Number>> points, Array<Number> tri, int axis) {
        double min = Double.POSITIVE_INFINITY;
        int _g = 0;
        while ((_g < 3)) {
            int i = _g++;
            double coord = (Runtime.toDouble(points.get(Runtime.toInt(tri.get(i))).get(axis)));
            if ((coord < min)) {
                min = coord;
            }
        }

        return min;
    }

    /**
     *
     * @param points
     * @param tri
     * @return
     */
    public static Array<Object> getTriangleCentroid(Array<Array<Object>> points, Array<Object> tri) {
        Array<Object> centroid = new Array<>(new Object[]{0.0, 0.0, 0.0});
        int _g = 0;
        while ((_g < 3)) {
            int i = _g++;
            int _g1 = 0;
            while ((_g1 < 3)) {
                int j = _g1++;
                centroid.set(j, Runtime.toDouble(centroid.get(j)) + Runtime.toDouble(points.get(Runtime.toInt(tri.get(i))).get(j)));
            }
        }

        int _g2 = 0;
        while ((_g2 < 3)) {
            int i1 = _g2++;
            centroid.set(i1, Runtime.toDouble(centroid.get(i1)) / 3);
        }

        return centroid;
    }

    /**
     *
     * @param mesh
     * @param faceIndex
     * @param f
     * @return
     */
    public static Array<Number> triangleUVFromPoint(MeshData mesh, int faceIndex, Array<Number> f) {
        Array<Number> tri = mesh.faces.get(faceIndex);
        Array<Number> p1 = mesh.points.get(Runtime.toInt(tri.get(0)));
        Array<Number> p2 = mesh.points.get(Runtime.toInt(tri.get(1)));
        Array<Number> p3 = mesh.points.get(Runtime.toInt(tri.get(2)));
        Array<Number> uv1 = mesh.uvs.get(Runtime.toInt(tri.get(0)));
        Array<Number> uv2 = mesh.uvs.get(Runtime.toInt(tri.get(1)));
        Array<Number> uv3 = mesh.uvs.get(Runtime.toInt(tri.get(2)));
        Array<Number> f1 = Vec.sub(p1, f);
        Array<Number> f2 = Vec.sub(p2, f);
        Array<Number> f3 = Vec.sub(p3, f);
        double a = Vec.norm(Vec.cross(Vec.sub(p1, p2), Vec.sub(p1, p3)));
        double a1 = Vec.norm(Vec.cross(f2, f3)) / a;
        double a2 = Vec.norm(Vec.cross(f3, f1)) / a;
        double a3 = Vec.norm(Vec.cross(f1, f2)) / a;
        return Vec.add(Vec.mul(a1, uv1), Vec.add(Vec.mul(a2, uv2), Vec.mul(a3, uv3)));
    }

    /**
     *
     * @param empty
     */
    public Mesh(EmptyObject empty) {
    }

    /**
     *
     */
    public Mesh() {
        Mesh.__hx_ctor_verb_core_Mesh(this);
    }

}
