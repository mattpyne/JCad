package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Deserializer extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_core_Deserializer(verb.core.Deserializer __hx_this) {
    }

    /**
     *
     * @param <T>
     * @param s
     * @return
     */
    public static <T> T deserialize(java.lang.String s) {
        haxe.Unserializer unserializer = new haxe.Unserializer(haxe.lang.Runtime.toString(s));
        T r = ((T) (unserializer.unserialize()));
        return r;
    }

    /**
     *
     * @param empty
     */
    public Deserializer(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Deserializer() {
        verb.core.Deserializer.__hx_ctor_verb_core_Deserializer(this);
    }

}
