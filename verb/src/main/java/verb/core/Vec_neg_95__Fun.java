package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Vec_neg_95__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.core.Vec_neg_95__Fun __hx_current = new Vec_neg_95__Fun();

    /**
     *
     */
    public Vec_neg_95__Fun() {
        super(1, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, java.lang.Object __fn_dyn1) {
        double x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (haxe.lang.Runtime.toDouble(__fn_dyn1)));
        return -(x);
    }

}
