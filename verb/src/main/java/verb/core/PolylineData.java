package verb.core;

import haxe.lang.EmptyObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class PolylineData extends SerializableBase {

    /**
     *
     * @param __hx_this
     * @param points
     * @param params
     */
    public static void __hx_ctor_verb_core_PolylineData(PolylineData __hx_this, Array<Array<Number>> points, Array<Number> params) {
        __hx_this.points = points;
        __hx_this.params = params;
    }

    /**
     *
     */
    public Array<Array<Number>> points;

    /**
     *
     */
    public Array<Number> params;

    /**
     *
     * @param empty
     */
    public PolylineData(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param points
     * @param params
     */
    public PolylineData(Array<Array<Number>> points, Array<Number> params) {
        PolylineData.__hx_ctor_verb_core_PolylineData(this, points, params);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -995_427_962: {
                    if (field.equals("params")) {
                        __temp_executeDef1 = false;
                        this.params = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

                case -982_754_077: {
                    if (field.equals("points")) {
                        __temp_executeDef1 = false;
                        this.points = ((Array<Array<Number>>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -995_427_962: {
                    if (field.equals("params")) {
                        __temp_executeDef1 = false;
                        return this.params;
                    }

                    break;
                }

                case -982_754_077: {
                    if (field.equals("points")) {
                        __temp_executeDef1 = false;
                        return this.points;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("params");
        baseArr.push("points");
        super.__hx_getFields(baseArr);
    }

}
