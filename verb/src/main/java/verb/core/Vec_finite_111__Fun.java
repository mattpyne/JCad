package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Vec_finite_111__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.core.Vec_finite_111__Fun __hx_current  = new Vec_finite_111__Fun();

    /**
     *
     */
    public Vec_finite_111__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        double x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (haxe.lang.Runtime.toDouble(__fn_dyn1)));
        return haxe.lang.Runtime.isFinite(x);
    }

}
