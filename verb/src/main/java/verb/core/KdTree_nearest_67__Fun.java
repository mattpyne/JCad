package verb.core;

import haxe.lang.Function;
import haxe.lang.Runtime;
import haxe.root.Array;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KdTree_nearest_67__Fun<T> extends Function {

    /**
     *
     */
    public Array<Number> point;

    /**
     *
     */
    public Function[] nearestSearch;

    /**
     *
     */
    public int maxNodes;

    /**
     *
     */
    public BinaryHeap<KdNode<T>> bestNodes;

    /**
     *
     */
    public KdTree<T> _gthis;

    /**
     *
     * @param point
     * @param nearestSearch
     * @param maxNodes
     * @param bestNodes
     * @param _gthis
     */
    public KdTree_nearest_67__Fun(Array<Number> point, Function[] nearestSearch, int maxNodes, BinaryHeap<KdNode<T>> bestNodes, KdTree<T> _gthis) {
        super(1, 0);
        this.point = point;
        this.nearestSearch = nearestSearch;
        this.maxNodes = maxNodes;
        this.bestNodes = bestNodes;
        this._gthis = _gthis;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        KdNode<T> node = (KdNode<T>) __fn_dyn1;
        KdNode<T> bestChild;
        int dimension = node.dimension;
        double ownDistance = this._gthis.distanceFunction.__hx_invoke2_f(0.0, this.point, 0.0, node.kdPoint.point);
        Array<Number> linearPoint = Vec.zeros1d(this._gthis.dim);
        double linearDistance;
        KdNode<T> otherChild;
        Function saveNode = new KdTree_nearest_77__Fun(this.maxNodes, this.bestNodes);
        int _g21 = 0;
        int _g11 = this._gthis.dim;
        while (_g21 < _g11) {
            
            int i2 = _g21++;
            if (i2 == node.dimension) {
                
                linearPoint.set(i2, Runtime.toDouble(this.point.get(i2)));
            } else {
                
                linearPoint.set(i2, Runtime.toDouble(node.kdPoint.point.get(i2)));
            }
        }

        linearDistance = this._gthis.distanceFunction.__hx_invoke2_f(0.0, linearPoint, 0.0, node.kdPoint.point);
        if ((node.right == null) && (node.left == null)) {
            
            if (this.bestNodes.size() < this.maxNodes || ownDistance < Runtime.toDouble(this.bestNodes.peek().item1)) {
                
                saveNode.__hx_invoke2_o(0.0, node, ownDistance, Runtime.undefined);
            }

            return null;
        }
        if (node.right == null) {
            
            bestChild = node.left;
        } else {
            
            if (node.left == null) {
                
                bestChild = node.right;
            } else {
                
                if (Runtime.toDouble(this.point.get(dimension)) < Runtime.toDouble(node.kdPoint.point.get(dimension))) {
                    
                    bestChild = node.left;
                } else {
                    
                    bestChild = node.right;
                }
            }
        }
        this.nearestSearch[0].__hx_invoke1_o(0.0, bestChild);
        if (this.bestNodes.size() < this.maxNodes || ownDistance < Runtime.toDouble(this.bestNodes.peek().item1)) {
            
            saveNode.__hx_invoke2_o(0.0, node, ownDistance, Runtime.undefined);
        }
        if (this.bestNodes.size() < this.maxNodes || Math.abs(linearDistance) < Runtime.toDouble(this.bestNodes.peek().item1)) {
            
            if (bestChild == node.left) {
                
                otherChild = node.right;
            } else {
                
                otherChild = node.left;
            }

            if (otherChild != null) {
                
                this.nearestSearch[0].__hx_invoke1_o(0.0, otherChild);
            }
        }
        
        return null;
    }

}
