package verb.core;

import haxe.ds.IntMap;
import haxe.lang.EmptyObject;
import haxe.lang.HxObject;
import haxe.lang.Runtime;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Binomial extends HxObject {

    public final static IntMap<IntMap<Object>> MEMO = new IntMap<>();

    public static double get(int n, int k) {
        if (k == 0.0) {
            return 1.0;
        }

        if ((n == 0) || (k > n)) {
            return 0.0;
        }

        if (k > (n - k)) {
            k = n - k;
        }

        if (Binomial.memo_exists(n, k)) {
            return Binomial.get_memo(n, k);
        }

        double r = (1);
        int n_o = n;
        int _g1 = 1;
        int _g = (k + 1);
        while ((_g1 < _g)) {
            int d = _g1++;
            if (Binomial.memo_exists(n_o, d)) {
                --n;
                r = Binomial.get_memo(n_o, d);
                continue;
            }

            r *= n--;
            r /= d;
            Binomial.memoize(n_o, d, r);
        }

        return r;
    }

    public static boolean memo_exists(int n, int k) {
        if (Binomial.MEMO.exists(n)) {
            return ((IntMap<Object>) (Binomial.MEMO.get(n))).exists(k);
        } else {
            return false;
        }
    }

    public static double get_memo(int n, int k) {
        return Runtime.toDouble(((IntMap<Object>) (Binomial.MEMO.get(n))).get(k));
    }

    public static void memoize(int n, int k, double val) {
        if (!(Binomial.MEMO.exists(n))) {
            Binomial.MEMO.set(n, new IntMap<>());
        }

        ((IntMap<Object>) (Binomial.MEMO.get(n))).set(k, val);
    }

}
