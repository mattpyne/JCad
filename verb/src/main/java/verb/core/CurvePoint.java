package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class CurvePoint extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param u
     * @param pt
     */
    public static void __hx_ctor_verb_core_CurvePoint(verb.core.CurvePoint __hx_this, double u, haxe.root.Array<java.lang.Object> pt) {
        __hx_this.u = u;
        __hx_this.pt = pt;
    }

    /**
     *
     */
    public double u;

    /**
     *
     */
    public haxe.root.Array<java.lang.Object> pt;

    /**
     *
     * @param empty
     */
    public CurvePoint(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param u
     * @param pt
     */
    public CurvePoint(double u, haxe.root.Array<java.lang.Object> pt) {
        verb.core.CurvePoint.__hx_ctor_verb_core_CurvePoint(this, u, pt);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_588: {
                    if (field.equals("pt")) {
                        __temp_executeDef1 = false;
                        this.pt = ((haxe.root.Array<java.lang.Object>) (value));
                        return value;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_588: {
                    if (field.equals("pt")) {
                        __temp_executeDef1 = false;
                        return this.pt;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("pt");
        baseArr.push("u");
        super.__hx_getFields(baseArr);
    }

}
