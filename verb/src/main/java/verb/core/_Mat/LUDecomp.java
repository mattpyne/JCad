package verb.core._Mat;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class LUDecomp extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param lu
     * @param p
     */
    public static void __hx_ctor_verb_core__Mat_LUDecomp(verb.core._Mat.LUDecomp __hx_this, haxe.root.Array<haxe.root.Array<java.lang.Number>> lu, haxe.root.Array<java.lang.Number> p) {
        __hx_this.LU = lu;
        __hx_this.P = p;
    }

    /**
     *
     */
    public haxe.root.Array<haxe.root.Array<java.lang.Number>> LU;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> P;

    /**
     *
     * @param empty
     */
    public LUDecomp(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param lu
     * @param p
     */
    public LUDecomp(haxe.root.Array<haxe.root.Array<java.lang.Number>> lu, haxe.root.Array<java.lang.Number> p) {
        verb.core._Mat.LUDecomp.__hx_ctor_verb_core__Mat_LUDecomp(this, lu, p);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 80: {
                    if (field.equals("P")) {
                        __temp_executeDef1 = false;
                        this.P = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 2_441: {
                    if (field.equals("LU")) {
                        __temp_executeDef1 = false;
                        this.LU = ((haxe.root.Array<haxe.root.Array<java.lang.Number>>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 80: {
                    if (field.equals("P")) {
                        __temp_executeDef1 = false;
                        return this.P;
                    }

                    break;
                }

                case 2_441: {
                    if (field.equals("LU")) {
                        __temp_executeDef1 = false;
                        return this.LU;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("P");
        baseArr.push("LU");
        super.__hx_getFields(baseArr);
    }

}
