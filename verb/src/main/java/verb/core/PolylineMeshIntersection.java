package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class PolylineMeshIntersection extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param point
     * @param u
     * @param uv
     * @param polylineIndex
     * @param faceIndex
     */
    public static void __hx_ctor_verb_core_PolylineMeshIntersection(verb.core.PolylineMeshIntersection __hx_this, haxe.root.Array<java.lang.Number> point, double u, haxe.root.Array<java.lang.Number> uv, int polylineIndex, int faceIndex) {
        __hx_this.point = point;
        __hx_this.u = u;
        __hx_this.uv = uv;
        __hx_this.polylineIndex = polylineIndex;
        __hx_this.faceIndex = faceIndex;
    }

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> point;

    /**
     *
     */
    public double u;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> uv;

    /**
     *
     */
    public int polylineIndex;

    /**
     *
     */
    public int faceIndex;

    /**
     *
     * @param empty
     */
    public PolylineMeshIntersection(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param point
     * @param u
     * @param uv
     * @param polylineIndex
     * @param faceIndex
     */
    public PolylineMeshIntersection(haxe.root.Array<java.lang.Number> point, double u, haxe.root.Array<java.lang.Number> uv, int polylineIndex, int faceIndex) {
        verb.core.PolylineMeshIntersection.__hx_ctor_verb_core_PolylineMeshIntersection(this, point, u, uv, polylineIndex, faceIndex);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_791_961_995: {
                    if (field.equals("faceIndex")) {
                        __temp_executeDef1 = false;
                        this.faceIndex = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (value);
                        return value;
                    }

                    break;
                }

                case -1_975_202_062: {
                    if (field.equals("polylineIndex")) {
                        __temp_executeDef1 = false;
                        this.polylineIndex = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_791_961_995: {
                    if (field.equals("faceIndex")) {
                        __temp_executeDef1 = false;
                        this.faceIndex = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        this.point = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case -1_975_202_062: {
                    if (field.equals("polylineIndex")) {
                        __temp_executeDef1 = false;
                        this.polylineIndex = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case 3_745: {
                    if (field.equals("uv")) {
                        __temp_executeDef1 = false;
                        this.uv = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_791_961_995: {
                    if (field.equals("faceIndex")) {
                        __temp_executeDef1 = false;
                        return this.faceIndex;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point;
                    }

                    break;
                }

                case -1_975_202_062: {
                    if (field.equals("polylineIndex")) {
                        __temp_executeDef1 = false;
                        return this.polylineIndex;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

                case 3_745: {
                    if (field.equals("uv")) {
                        __temp_executeDef1 = false;
                        return this.uv;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_791_961_995: {
                    if (field.equals("faceIndex")) {
                        __temp_executeDef1 = false;
                        return (this.faceIndex);
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

                case -1_975_202_062: {
                    if (field.equals("polylineIndex")) {
                        __temp_executeDef1 = false;
                        return (this.polylineIndex);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("faceIndex");
        baseArr.push("polylineIndex");
        baseArr.push("uv");
        baseArr.push("u");
        baseArr.push("point");
        super.__hx_getFields(baseArr);
    }

}
