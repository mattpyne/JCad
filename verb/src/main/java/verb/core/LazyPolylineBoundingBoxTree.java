// Generated by Haxe 3.4.4
package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class LazyPolylineBoundingBoxTree extends haxe.lang.HxObject implements verb.eval.IBoundingBoxTree<java.lang.Object> {

    /**
     *
     * @param __hx_this
     * @param polyline
     * @param interval
     */
    public static void __hx_ctor_verb_core_LazyPolylineBoundingBoxTree(verb.core.LazyPolylineBoundingBoxTree __hx_this, verb.core.PolylineData polyline, verb.core.Interval<java.lang.Object> interval) {
        __hx_this._boundingBox = null;
        __hx_this._polyline = polyline;
        if ((interval == null)) {
            interval = new verb.core.Interval<>(0, (((polyline.points.length != 0)) ? ((polyline.points.length - 1)) : (0)));
        }

        __hx_this._interval = interval;
    }

    /**
     *
     */
    public verb.core.Interval<java.lang.Object> _interval;

    /**
     *
     */
    public verb.core.PolylineData _polyline;

    /**
     *
     */
    public verb.core.BoundingBox _boundingBox;

    /**
     *
     * @param empty
     */
    public LazyPolylineBoundingBoxTree(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param polyline
     * @param interval
     */
    public LazyPolylineBoundingBoxTree(verb.core.PolylineData polyline, verb.core.Interval<java.lang.Object> interval) {
        verb.core.LazyPolylineBoundingBoxTree.__hx_ctor_verb_core_LazyPolylineBoundingBoxTree(this, polyline, interval);
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.Pair<verb.eval.IBoundingBoxTree<java.lang.Object>, verb.eval.IBoundingBoxTree<java.lang.Object>> split() {
        int min = (haxe.lang.Runtime.toInt(this._interval.min));
        int max = (haxe.lang.Runtime.toInt(this._interval.max));
        int pivot = (min + ((int) (java.lang.Math.ceil((((double) (((max - min)))) / 2)))));
        verb.core.Interval<java.lang.Object> l = new verb.core.Interval<>(min, pivot);
        verb.core.Interval<java.lang.Object> r = new verb.core.Interval<>(pivot, max);
        return new verb.core.Pair<>(new verb.core.LazyPolylineBoundingBoxTree(this._polyline, l), new verb.core.LazyPolylineBoundingBoxTree(this._polyline, r));
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.BoundingBox boundingBox() {
        if ((this._boundingBox == null)) {
            this._boundingBox = new verb.core.BoundingBox((this._polyline.points));
        }

        return this._boundingBox;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Object yield() {
        return ((int) (haxe.lang.Runtime.toInt(this._interval.min)));
    }

    /**
     *
     * @param tolerance
     * @return
     */
    @Override
    public boolean indivisible(double tolerance) {
        return (((haxe.lang.Runtime.toInt(this._interval.max)) - (haxe.lang.Runtime.toInt(this._interval.min))) == 1);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean empty() {
        return (((haxe.lang.Runtime.toInt(this._interval.max)) - (haxe.lang.Runtime.toInt(this._interval.min))) == 0);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_884_107_832: {
                    if (field.equals("_boundingBox")) {
                        __temp_executeDef1 = false;
                        this._boundingBox = ((verb.core.BoundingBox) (value));
                        return value;
                    }

                    break;
                }

                case 660_936_228: {
                    if (field.equals("_interval")) {
                        __temp_executeDef1 = false;
                        this._interval = ((verb.core.Interval<java.lang.Object>) (value));
                        return value;
                    }

                    break;
                }

                case 652_456_735: {
                    if (field.equals("_polyline")) {
                        __temp_executeDef1 = false;
                        this._polyline = ((verb.core.PolylineData) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 96_634_189: {
                    if (field.equals("empty")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "empty"));
                    }

                    break;
                }

                case 660_936_228: {
                    if (field.equals("_interval")) {
                        __temp_executeDef1 = false;
                        return this._interval;
                    }

                    break;
                }

                case -1_421_565_752: {
                    if (field.equals("indivisible")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "indivisible"));
                    }

                    break;
                }

                case 652_456_735: {
                    if (field.equals("_polyline")) {
                        __temp_executeDef1 = false;
                        return this._polyline;
                    }

                    break;
                }

                case 114_974_605: {
                    if (field.equals("yield")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "yield"));
                    }

                    break;
                }

                case -1_884_107_832: {
                    if (field.equals("_boundingBox")) {
                        __temp_executeDef1 = false;
                        return this._boundingBox;
                    }

                    break;
                }

                case -1_262_064_249: {
                    if (field.equals("boundingBox")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "boundingBox"));
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "split"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 96_634_189: {
                    if (field.equals("empty")) {
                        __temp_executeDef1 = false;
                        return this.empty();
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return this.split();
                    }

                    break;
                }

                case -1_421_565_752: {
                    if (field.equals("indivisible")) {
                        __temp_executeDef1 = false;
                        return this.indivisible((haxe.lang.Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case -1_262_064_249: {
                    if (field.equals("boundingBox")) {
                        __temp_executeDef1 = false;
                        return this.boundingBox();
                    }

                    break;
                }

                case 114_974_605: {
                    if (field.equals("yield")) {
                        __temp_executeDef1 = false;
                        return this.yield();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("_boundingBox");
        baseArr.push("_polyline");
        baseArr.push("_interval");
        super.__hx_getFields(baseArr);
    }

}
