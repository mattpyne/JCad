package verb.core;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KdPoint<T> extends haxe.lang.HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param point
     * @param obj
     */
    public static <T_c> void __hx_ctor_verb_core_KdPoint(verb.core.KdPoint<T_c> __hx_this, haxe.root.Array<java.lang.Number> point, T_c obj) {
        __hx_this.point = point;
        __hx_this.obj = obj;
    }

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> point;

    /**
     *
     */
    public T obj;

    /**
     *
     * @param empty
     */
    public KdPoint(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param point
     * @param obj
     */
    public KdPoint(haxe.root.Array<java.lang.Number> point, T obj) {
        verb.core.KdPoint.__hx_ctor_verb_core_KdPoint(this, point, obj);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 109_815: {
                    if (field.equals("obj")) {
                        __temp_executeDef1 = false;
                        this.obj = ((T) (((java.lang.Object) (value))));
                        return (haxe.lang.Runtime.toDouble(value));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 109_815: {
                    if (field.equals("obj")) {
                        __temp_executeDef1 = false;
                        this.obj = ((T) (value));
                        return value;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        this.point = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 109_815: {
                    if (field.equals("obj")) {
                        __temp_executeDef1 = false;
                        return this.obj;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 109_815: {
                    if (field.equals("obj")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this.obj));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("obj");
        baseArr.push("point");
        super.__hx_getFields(baseArr);
    }

}
