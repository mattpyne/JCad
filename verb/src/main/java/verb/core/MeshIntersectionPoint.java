package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class MeshIntersectionPoint extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param uv0
     * @param uv1
     * @param point
     * @param faceIndex0
     * @param faceIndex1
     */
    public static void __hx_ctor_verb_core_MeshIntersectionPoint(verb.core.MeshIntersectionPoint __hx_this, haxe.root.Array<java.lang.Number> uv0, haxe.root.Array<java.lang.Number> uv1, haxe.root.Array<java.lang.Number> point, int faceIndex0, int faceIndex1) {
        __hx_this.visited = false;
        __hx_this.adj = null;
        __hx_this.opp = null;
        __hx_this.uv0 = uv0;
        __hx_this.uv1 = uv1;
        __hx_this.point = point;
    }

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> uv0;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> uv1;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> point;

    /**
     *
     */
    public int faceIndex0;

    /**
     *
     */
    public int faceIndex1;

    /**
     *
     */
    public verb.core.MeshIntersectionPoint opp;

    /**
     *
     */
    public verb.core.MeshIntersectionPoint adj;

    /**
     *
     */
    public boolean visited;

    /**
     *
     * @param empty
     */
    public MeshIntersectionPoint(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param uv0
     * @param uv1
     * @param point
     * @param faceIndex0
     * @param faceIndex1
     */
    public MeshIntersectionPoint(haxe.root.Array<java.lang.Number> uv0, haxe.root.Array<java.lang.Number> uv1, haxe.root.Array<java.lang.Number> point, int faceIndex0, int faceIndex1) {
        verb.core.MeshIntersectionPoint.__hx_ctor_verb_core_MeshIntersectionPoint(this, uv0, uv1, point, faceIndex0, faceIndex1);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 283_753_052: {
                    if (field.equals("faceIndex1")) {
                        __temp_executeDef1 = false;
                        this.faceIndex1 = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 283_753_051: {
                    if (field.equals("faceIndex0")) {
                        __temp_executeDef1 = false;
                        this.faceIndex0 = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 466_760_490: {
                    if (field.equals("visited")) {
                        __temp_executeDef1 = false;
                        this.visited = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 116_143: {
                    if (field.equals("uv0")) {
                        __temp_executeDef1 = false;
                        this.uv0 = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 96_423: {
                    if (field.equals("adj")) {
                        __temp_executeDef1 = false;
                        this.adj = ((verb.core.MeshIntersectionPoint) (value));
                        return value;
                    }

                    break;
                }

                case 116_144: {
                    if (field.equals("uv1")) {
                        __temp_executeDef1 = false;
                        this.uv1 = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 110_255: {
                    if (field.equals("opp")) {
                        __temp_executeDef1 = false;
                        this.opp = ((verb.core.MeshIntersectionPoint) (value));
                        return value;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        this.point = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 283_753_052: {
                    if (field.equals("faceIndex1")) {
                        __temp_executeDef1 = false;
                        this.faceIndex1 = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 283_753_051: {
                    if (field.equals("faceIndex0")) {
                        __temp_executeDef1 = false;
                        this.faceIndex0 = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 466_760_490: {
                    if (field.equals("visited")) {
                        __temp_executeDef1 = false;
                        return this.visited;
                    }

                    break;
                }

                case 116_143: {
                    if (field.equals("uv0")) {
                        __temp_executeDef1 = false;
                        return this.uv0;
                    }

                    break;
                }

                case 96_423: {
                    if (field.equals("adj")) {
                        __temp_executeDef1 = false;
                        return this.adj;
                    }

                    break;
                }

                case 116_144: {
                    if (field.equals("uv1")) {
                        __temp_executeDef1 = false;
                        return this.uv1;
                    }

                    break;
                }

                case 110_255: {
                    if (field.equals("opp")) {
                        __temp_executeDef1 = false;
                        return this.opp;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point;
                    }

                    break;
                }

                case 283_753_052: {
                    if (field.equals("faceIndex1")) {
                        __temp_executeDef1 = false;
                        return this.faceIndex1;
                    }

                    break;
                }

                case 283_753_051: {
                    if (field.equals("faceIndex0")) {
                        __temp_executeDef1 = false;
                        return this.faceIndex0;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 283_753_052: {
                    if (field.equals("faceIndex1")) {
                        __temp_executeDef1 = false;
                        return (this.faceIndex1);
                    }

                    break;
                }

                case 283_753_051: {
                    if (field.equals("faceIndex0")) {
                        __temp_executeDef1 = false;
                        return (this.faceIndex0);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("visited");
        baseArr.push("adj");
        baseArr.push("opp");
        baseArr.push("faceIndex1");
        baseArr.push("faceIndex0");
        baseArr.push("point");
        baseArr.push("uv1");
        baseArr.push("uv0");
        super.__hx_getFields(baseArr);
    }

}
