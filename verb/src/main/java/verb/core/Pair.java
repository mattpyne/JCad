package verb.core;

/**
 *
 * @author Matthew
 * @param <T1>
 * @param <T2>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Pair<T1, T2> extends haxe.lang.HxObject {

    /**
     *
     * @param <T1_c>
     * @param <T2_c>
     * @param __hx_this
     * @param item1
     * @param item2
     */
    public static <T1_c, T2_c> void __hx_ctor_verb_core_Pair(verb.core.Pair<T1_c, T2_c> __hx_this, T1_c item1, T2_c item2) {
        __hx_this.item0 = item1;
        __hx_this.item1 = item2;
    }

    /**
     *
     */
    public T1 item0;

    /**
     *
     */
    public T2 item1;

    /**
     *
     * @param empty
     */
    public Pair(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param item1
     * @param item2
     */
    public Pair(T1 item1, T2 item2) {
        verb.core.Pair.__hx_ctor_verb_core_Pair(this, item1, item2);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 100_525_950: {
                    if (field.equals("item1")) {
                        __temp_executeDef1 = false;
                        this.item1 = ((T2) (((java.lang.Object) (value))));
                        return (haxe.lang.Runtime.toDouble(value));
                    }

                    break;
                }

                case 100_525_949: {
                    if (field.equals("item0")) {
                        __temp_executeDef1 = false;
                        this.item0 = ((T1) (((java.lang.Object) (value))));
                        return (haxe.lang.Runtime.toDouble(value));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 100_525_950: {
                    if (field.equals("item1")) {
                        __temp_executeDef1 = false;
                        this.item1 = ((T2) (value));
                        return value;
                    }

                    break;
                }

                case 100_525_949: {
                    if (field.equals("item0")) {
                        __temp_executeDef1 = false;
                        this.item0 = ((T1) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 100_525_950: {
                    if (field.equals("item1")) {
                        __temp_executeDef1 = false;
                        return this.item1;
                    }

                    break;
                }

                case 100_525_949: {
                    if (field.equals("item0")) {
                        __temp_executeDef1 = false;
                        return this.item0;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 100_525_950: {
                    if (field.equals("item1")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this.item1));
                    }

                    break;
                }

                case 100_525_949: {
                    if (field.equals("item0")) {
                        __temp_executeDef1 = false;
                        return (haxe.lang.Runtime.toDouble(this.item0));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("item1");
        baseArr.push("item0");
        super.__hx_getFields(baseArr);
    }

}
