// Generated by Haxe 3.4.4
package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class LazyCurveBoundingBoxTree extends haxe.lang.HxObject implements verb.eval.IBoundingBoxTree<verb.core.NurbsCurveData> {

    /**
     *
     * @param __hx_this
     * @param curve
     * @param knotTol
     */
    public static void __hx_ctor_verb_core_LazyCurveBoundingBoxTree(verb.core.LazyCurveBoundingBoxTree __hx_this, verb.core.NurbsCurveData curve, java.lang.Object knotTol) {
        __hx_this._boundingBox = null;
        __hx_this._curve = curve;
        if (haxe.lang.Runtime.eq(knotTol, null)) {
            haxe.root.Array<java.lang.Number> a = __hx_this._curve.knots;
            knotTol = ((((haxe.lang.Runtime.toDouble(verb.core.ArrayExtensions.last((haxe.root.Array) (a)))) - (haxe.lang.Runtime.toDouble(verb.core.ArrayExtensions.first((((haxe.root.Array) (a)))))))) / 64);
        }

        __hx_this._knotTol = (haxe.lang.Runtime.toDouble(knotTol));
    }

    /**
     *
     */
    public verb.core.NurbsCurveData _curve;

    /**
     *
     */
    public verb.core.BoundingBox _boundingBox;

    /**
     *
     */
    public double _knotTol;

    /**
     *
     * @param empty
     */
    public LazyCurveBoundingBoxTree(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param curve
     * @param knotTol
     */
    public LazyCurveBoundingBoxTree(verb.core.NurbsCurveData curve, java.lang.Object knotTol) {
        verb.core.LazyCurveBoundingBoxTree.__hx_ctor_verb_core_LazyCurveBoundingBoxTree(this, curve, knotTol);
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.Pair<verb.eval.IBoundingBoxTree<verb.core.NurbsCurveData>, verb.eval.IBoundingBoxTree<verb.core.NurbsCurveData>> split() {
        double min = (haxe.lang.Runtime.toDouble(verb.core.ArrayExtensions.first((haxe.root.Array) (this._curve.knots))));
        double max = (haxe.lang.Runtime.toDouble(verb.core.ArrayExtensions.last((haxe.root.Array) (this._curve.knots))));
        double dom = (max - min);
        haxe.root.Array<verb.core.NurbsCurveData> crvs = verb.eval.Divide.curveSplit(this._curve, ((((max + min)) / 2.0) + ((dom * 0.1) * java.lang.Math.random())));
        return new verb.core.Pair<>(new verb.core.LazyCurveBoundingBoxTree(crvs.get(0), this._knotTol), new verb.core.LazyCurveBoundingBoxTree(crvs.get(1), this._knotTol));
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.BoundingBox boundingBox() {
        if ((this._boundingBox == null)) {
            this._boundingBox = new verb.core.BoundingBox((verb.eval.Eval.dehomogenize1d(this._curve.controlPoints)));
        }

        return this._boundingBox;
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.NurbsCurveData yield() {
        return this._curve;
    }

    /**
     *
     * @param tolerance
     * @return
     */
    @Override
    public boolean indivisible(double tolerance) {
        haxe.root.Array<java.lang.Number> a = this._curve.knots;
        return (((haxe.lang.Runtime.toDouble(verb.core.ArrayExtensions.last(a))) - (haxe.lang.Runtime.toDouble(verb.core.ArrayExtensions.first((a))))) < this._knotTol);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean empty() {
        return false;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_792_128_106: {
                    if (field.equals("_knotTol")) {
                        __temp_executeDef1 = false;
                        this._knotTol = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_792_128_106: {
                    if (field.equals("_knotTol")) {
                        __temp_executeDef1 = false;
                        this._knotTol = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case -1_480_170_512: {
                    if (field.equals("_curve")) {
                        __temp_executeDef1 = false;
                        this._curve = ((verb.core.NurbsCurveData) (value));
                        return value;
                    }

                    break;
                }

                case -1_884_107_832: {
                    if (field.equals("_boundingBox")) {
                        __temp_executeDef1 = false;
                        this._boundingBox = ((verb.core.BoundingBox) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 96_634_189: {
                    if (field.equals("empty")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "empty"));
                    }

                    break;
                }

                case -1_480_170_512: {
                    if (field.equals("_curve")) {
                        __temp_executeDef1 = false;
                        return this._curve;
                    }

                    break;
                }

                case -1_421_565_752: {
                    if (field.equals("indivisible")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "indivisible"));
                    }

                    break;
                }

                case -1_884_107_832: {
                    if (field.equals("_boundingBox")) {
                        __temp_executeDef1 = false;
                        return this._boundingBox;
                    }

                    break;
                }

                case 114_974_605: {
                    if (field.equals("yield")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "yield"));
                    }

                    break;
                }

                case 1_792_128_106: {
                    if (field.equals("_knotTol")) {
                        __temp_executeDef1 = false;
                        return this._knotTol;
                    }

                    break;
                }

                case -1_262_064_249: {
                    if (field.equals("boundingBox")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "boundingBox"));
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "split"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_792_128_106: {
                    if (field.equals("_knotTol")) {
                        __temp_executeDef1 = false;
                        return this._knotTol;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 96_634_189: {
                    if (field.equals("empty")) {
                        __temp_executeDef1 = false;
                        return this.empty();
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return this.split();
                    }

                    break;
                }

                case -1_421_565_752: {
                    if (field.equals("indivisible")) {
                        __temp_executeDef1 = false;
                        return this.indivisible((haxe.lang.Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case -1_262_064_249: {
                    if (field.equals("boundingBox")) {
                        __temp_executeDef1 = false;
                        return this.boundingBox();
                    }

                    break;
                }

                case 114_974_605: {
                    if (field.equals("yield")) {
                        __temp_executeDef1 = false;
                        return this.yield();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("_knotTol");
        baseArr.push("_boundingBox");
        baseArr.push("_curve");
        super.__hx_getFields(baseArr);
    }

}
