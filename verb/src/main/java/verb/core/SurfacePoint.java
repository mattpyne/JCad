package verb.core;

import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class SurfacePoint extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param point
     * @param normal
     * @param uv
     * @param id
     * @param degen
     */
    public static void __hx_ctor_verb_core_SurfacePoint(SurfacePoint __hx_this, Array<Number> point, Array<Number> normal, Array<Number> uv, Object id, Object degen) {
        boolean __temp_degen6 = ((haxe.lang.Runtime.eq(degen, null)) ? (false) : (haxe.lang.Runtime.toBool(degen)));
        int __temp_id5 = ((haxe.lang.Runtime.eq(id, null)) ? (-1) : (haxe.lang.Runtime.toInt(id)));
        __hx_this.uv = uv;
        __hx_this.point = point;
        __hx_this.normal = normal;
        __hx_this.id = __temp_id5;
        __hx_this.degen = __temp_degen6;
    }

    /**
     *
     * @param u
     * @param v
     * @return
     */
    public static SurfacePoint fromUv(double u, double v) {
        return new SurfacePoint(null, null, new Array<>(new Number[]{u, v}), null, null);
    }

    /**
     *
     */
    public Array<Number> uv;

    /**
     *
     */
    public Array<Number> point;

    /**
     *
     */
    public Array<Number> normal;

    /**
     *
     */
    public int id;

    /**
     *
     */
    public boolean degen;

    /**
     *
     * @param empty
     */
    public SurfacePoint(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param point
     * @param normal
     * @param uv
     * @param id
     * @param degen
     */
    public SurfacePoint(Array<Number> point, Array<Number> normal, Array<Number> uv, Object id, Object degen) {
        SurfacePoint.__hx_ctor_verb_core_SurfacePoint(this, point, normal, uv, id, degen);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_355: {
                    if (field.equals("id")) {
                        __temp_executeDef1 = false;
                        this.id = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 95_463_215: {
                    if (field.equals("degen")) {
                        __temp_executeDef1 = false;
                        this.degen = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 3_745: {
                    if (field.equals("uv")) {
                        __temp_executeDef1 = false;
                        this.uv = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

                case 3_355: {
                    if (field.equals("id")) {
                        __temp_executeDef1 = false;
                        this.id = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        this.point = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

                case -1_039_745_817: {
                    if (field.equals("normal")) {
                        __temp_executeDef1 = false;
                        this.normal = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 95_463_215: {
                    if (field.equals("degen")) {
                        __temp_executeDef1 = false;
                        return this.degen;
                    }

                    break;
                }

                case 3_745: {
                    if (field.equals("uv")) {
                        __temp_executeDef1 = false;
                        return this.uv;
                    }

                    break;
                }

                case 3_355: {
                    if (field.equals("id")) {
                        __temp_executeDef1 = false;
                        return this.id;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point;
                    }

                    break;
                }

                case -1_039_745_817: {
                    if (field.equals("normal")) {
                        __temp_executeDef1 = false;
                        return this.normal;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_355: {
                    if (field.equals("id")) {
                        __temp_executeDef1 = false;
                        return (this.id);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("degen");
        baseArr.push("id");
        baseArr.push("normal");
        baseArr.push("point");
        baseArr.push("uv");
        super.__hx_getFields(baseArr);
    }

}
