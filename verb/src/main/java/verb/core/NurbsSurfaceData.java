package verb.core;

import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.root.Array;
import java.util.HashMap;
import verb.eval.AdaptiveRefinementNode;
import verb.eval.AdaptiveRefinementOptions;
import verb.eval.Tess;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class NurbsSurfaceData extends SerializableBase {

    /**
     *
     * @param __hx_this
     * @param degreeU
     * @param degreeV
     * @param knotsU
     * @param knotsV
     * @param controlPoints
     */
    public static void __hx_ctor_verb_core_NurbsSurfaceData(
            NurbsSurfaceData __hx_this,
            int degreeU,
            int degreeV,
            Array<Number> knotsU,
            Array<Number> knotsV,
            Array<Array<Array<Number>>> controlPoints) {

        __hx_this.degreeU = degreeU;
        __hx_this.degreeV = degreeV;
        __hx_this.knotsU = knotsU;
        __hx_this.knotsV = knotsV;
        __hx_this.controlPoints = controlPoints;
    }

    /**
     *
     */
    public int degreeU;

    /**
     *
     */
    public int degreeV;

    /**
     *
     */
    public Array<Number> knotsU;

    /**
     *
     */
    public Array<Number> knotsV;

    /**
     *
     */
    public Array<Array<Array<Number>>> controlPoints;

    /**
     *
     */
    public HashMap<AdaptiveRefinementOptions, MeshData> meshData = new HashMap();

    /**
     *
     * @param empty
     */
    public NurbsSurfaceData(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param degreeU
     * @param degreeV
     * @param knotsU
     * @param knotsV
     * @param controlPoints
     */
    public NurbsSurfaceData(int degreeU, int degreeV, Array<Number> knotsU, Array<Number> knotsV, Array<Array<Array<Number>>> controlPoints) {
        NurbsSurfaceData.__hx_ctor_verb_core_NurbsSurfaceData(this, degreeU, degreeV, knotsU, knotsV, controlPoints);
    }

    /**
     *
     * @param options
     * @return
     */
    public MeshData getMeshData(AdaptiveRefinementOptions options) {
        if (options == null) {
            options = new AdaptiveRefinementOptions();
        }

        MeshData mesh = meshData.get(options);

        if (null == mesh) {

            Array<AdaptiveRefinementNode> arrTrees = Tess.divideRationalSurfaceAdaptive(this, options);
            mesh = Tess.triangulateAdaptiveRefinementNodeTree(arrTrees);
            meshData.put(options, mesh);
        }

        return mesh;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(String field, double value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 1_546_218_250: {
                if (field.equals("degreeV")) {
                    this.degreeV = ((int) (value));
                    return value;
                }

                break;
            }

            case 1_546_218_249: {
                if (field.equals("degreeU")) {
                    this.degreeU = ((int) (value));
                    return value;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField_f(field, value, handleProperties);
        } else {
            throw null;
        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 793_539_744: {
                if (field.equals("controlPoints")) {
                    __temp_executeDef1 = false;
                    this.controlPoints = ((Array<Array<Array<Number>>>) (value));
                    return value;
                }

                break;
            }

            case 1_546_218_249: {
                if (field.equals("degreeU")) {
                    __temp_executeDef1 = false;
                    this.degreeU = (Runtime.toInt(value));
                    return value;
                }

                break;
            }

            case -1_126_638_901: {
                if (field.equals("knotsV")) {
                    __temp_executeDef1 = false;
                    this.knotsV = ((Array<Number>) (value));
                    return value;
                }

                break;
            }

            case 1_546_218_250: {
                if (field.equals("degreeV")) {
                    __temp_executeDef1 = false;
                    this.degreeV = (Runtime.toInt(value));
                    return value;
                }

                break;
            }

            case -1_126_638_902: {
                if (field.equals("knotsU")) {
                    __temp_executeDef1 = false;
                    this.knotsU = ((Array<Number>) (value));
                    return value;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw null;
        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 793_539_744: {
                if (field.equals("controlPoints")) {
                    __temp_executeDef1 = false;
                    return this.controlPoints;
                }

                break;
            }

            case 1_546_218_249: {
                if (field.equals("degreeU")) {
                    __temp_executeDef1 = false;
                    return this.degreeU;
                }

                break;
            }

            case -1_126_638_901: {
                if (field.equals("knotsV")) {
                    __temp_executeDef1 = false;
                    return this.knotsV;
                }

                break;
            }

            case 1_546_218_250: {
                if (field.equals("degreeV")) {
                    __temp_executeDef1 = false;
                    return this.degreeV;
                }

                break;
            }

            case -1_126_638_902: {
                if (field.equals("knotsU")) {
                    __temp_executeDef1 = false;
                    return this.knotsU;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw null;
        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 1_546_218_250: {
                if (field.equals("degreeV")) {
                    __temp_executeDef1 = false;
                    return (this.degreeV);
                }

                break;
            }

            case 1_546_218_249: {
                if (field.equals("degreeU")) {
                    __temp_executeDef1 = false;
                    return (this.degreeU);
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField_f(field, throwErrors, handleProperties);
        } else {
            throw null;
        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("controlPoints");
        baseArr.push("knotsV");
        baseArr.push("knotsU");
        baseArr.push("degreeV");
        baseArr.push("degreeU");
        super.__hx_getFields(baseArr);
    }

}
