package verb.core;

import haxe.lang.Function;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KdTree_buildTree_40__Fun<T> extends Function {

    /**
     *
     */
    public int dim;

    /**
     *
     * @param dim
     */
    public KdTree_buildTree_40__Fun(int dim) {
        super(2, 1);
        this.dim = dim;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public double __hx_invoke2_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        KdPoint<T> b = (KdPoint<T>) __fn_dyn2;
        KdPoint<T> a = (KdPoint<T>) __fn_dyn1;
        
        double diff = a.point.get(this.dim).doubleValue() - b.point.get(this.dim).doubleValue();
        if (diff == 0.0) {
            return 0;
        } else {
            if (diff > 0) {
                return 1;
            } else {
                return -1;
            }
        }
    }

}
