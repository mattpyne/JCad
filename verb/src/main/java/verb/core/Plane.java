package verb.core;

import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Plane extends SerializableBase {

    /**
     *
     * @param __hx_this
     * @param origin
     * @param normal
     */
    public static void __hx_ctor_verb_core_Plane(Plane __hx_this, Array<Number> origin, Array<Number> normal) {
        __hx_this.origin = origin;
        __hx_this.normal = normal;
    }

    /**
     *
     */
    public Array<Number> normal;

    /**
     *
     */
    public Array<Number> origin;

    /**
     *
     * @param empty
     */
    public Plane(haxe.lang.EmptyObject empty) {
        super(haxe.lang.EmptyObject.EMPTY);
    }

    /**
     *
     * @param origin
     * @param normal
     */
    public Plane(Array<Number> origin, Array<Number> normal) {
        Plane.__hx_ctor_verb_core_Plane(this, origin, normal);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_008_619_738: {
                    if (field.equals("origin")) {
                        __temp_executeDef1 = false;
                        this.origin = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

                case -1_039_745_817: {
                    if (field.equals("normal")) {
                        __temp_executeDef1 = false;
                        this.normal = ((Array<Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_008_619_738: {
                    if (field.equals("origin")) {
                        __temp_executeDef1 = false;
                        return this.origin;
                    }

                    break;
                }

                case -1_039_745_817: {
                    if (field.equals("normal")) {
                        __temp_executeDef1 = false;
                        return this.normal;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("origin");
        baseArr.push("normal");
        super.__hx_getFields(baseArr);
    }

}
