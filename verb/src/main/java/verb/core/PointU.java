package verb.core;

import haxe.root.Array;

/**
 *
 * @author Matthew
 */
public class PointU {
    
    public Array<Number> point;
    public double u;
    
    public PointU(Array<Number> point, double u) {
        this.point = point;
        this.u = u;
    }
    
}