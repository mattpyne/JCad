package verb.core;

import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.lang.HxObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class CurveCurveIntersection extends HxObject {

    /**
     *
     * @param __hx_this
     * @param point0
     * @param point1
     * @param u0
     * @param u1
     */
    public static void __hx_ctor_verb_core_CurveCurveIntersection(CurveCurveIntersection __hx_this, Array<Number> point0, Array<Number> point1, double u0, double u1) {
        __hx_this.point0 = point0;
        __hx_this.point1 = point1;
        __hx_this.u0 = u0;
        __hx_this.u1 = u1;
    }

    /**
     *
     */
    public Array<Number> point0;

    /**
     *
     */
    public Array<Number> point1;

    /**
     *
     */
    public double u0;

    /**
     *
     */
    public double u1;

    /**
     *
     * @param empty
     */
    public CurveCurveIntersection(EmptyObject empty) {
    }

    /**
     *
     * @param point0
     * @param point1
     * @param u0
     * @param u1
     */
    public CurveCurveIntersection(Array<Number> point0, Array<Number> point1, double u0, double u1) {
        CurveCurveIntersection.__hx_ctor_verb_core_CurveCurveIntersection(this, point0, point1, u0, u1);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(String field, double value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 3_676: {
                if (field.equals("u1")) {
                    this.u1 = value;
                    return value;
                }
                break;
            }

            case 3_675: {
                if (field.equals("u0")) {
                    this.u0 = value;
                    return value;
                }
                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField_f(field, value, handleProperties);
        } else {
            throw new RuntimeException();
        }
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 3_676: {
                if (field.equals("u1")) {
                    this.u1 = Runtime.toDouble(value);
                    return value;
                }
                break;
            }

            case -982_754_144: {
                if (field.equals("point0")) {
                    this.point0 = (Array<Number>) value;
                    return value;
                }
                break;
            }

            case 3_675: {
                if (field.equals("u0")) {
                    this.u0 = Runtime.toDouble(value);
                    return value;
                }
                break;
            }

            case -982_754_143: {
                if (field.equals("point1")) {
                    this.point1 = (Array<Number>) value;
                    return value;
                }
                break;
            }
        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw new RuntimeException();
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 3_676: {
                if (field.equals("u1")) {
                    return this.u1;
                }
                break;
            }

            case -982_754_144: {
                if (field.equals("point0")) {
                    return this.point0;
                }
                break;
            }

            case 3_675: {
                if (field.equals("u0")) {
                    return this.u0;
                }
                break;
            }

            case -982_754_143: {
                if (field.equals("point1")) {
                    return this.point1;
                }
                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 3_676: {
                if (field.equals("u1")) {
                    return this.u1;
                }
                break;
            }

            case 3_675: {
                if (field.equals("u0")) {
                    return this.u0;
                }
                break;
            }
        }

        if (__temp_executeDef1) {
            return super.__hx_getField_f(field, throwErrors, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("u1");
        baseArr.push("u0");
        baseArr.push("point1");
        baseArr.push("point0");
        super.__hx_getFields(baseArr);
    }

}
