package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class CurveSurfaceIntersection extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param u
     * @param uv
     * @param curvePoint
     * @param surfacePoint
     */
    public static void __hx_ctor_verb_core_CurveSurfaceIntersection(verb.core.CurveSurfaceIntersection __hx_this, double u, haxe.root.Array<java.lang.Number> uv, haxe.root.Array<java.lang.Number> curvePoint, haxe.root.Array<java.lang.Number> surfacePoint) {
        __hx_this.u = u;
        __hx_this.uv = uv;
        __hx_this.curvePoint = curvePoint;
        __hx_this.surfacePoint = surfacePoint;
    }

    /**
     *
     */
    public double u;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> uv;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> curvePoint;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> surfacePoint;

    /**
     *
     * @param empty
     */
    public CurveSurfaceIntersection(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param u
     * @param uv
     * @param curvePoint
     * @param surfacePoint
     */
    public CurveSurfaceIntersection(double u, haxe.root.Array<java.lang.Number> uv, haxe.root.Array<java.lang.Number> curvePoint, haxe.root.Array<java.lang.Number> surfacePoint) {
        verb.core.CurveSurfaceIntersection.__hx_ctor_verb_core_CurveSurfaceIntersection(this, u, uv, curvePoint, surfacePoint);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -543_466_269: {
                    if (field.equals("surfacePoint")) {
                        __temp_executeDef1 = false;
                        this.surfacePoint = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case -1_861_770_783: {
                    if (field.equals("curvePoint")) {
                        __temp_executeDef1 = false;
                        this.curvePoint = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 3_745: {
                    if (field.equals("uv")) {
                        __temp_executeDef1 = false;
                        this.uv = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -543_466_269: {
                    if (field.equals("surfacePoint")) {
                        __temp_executeDef1 = false;
                        return this.surfacePoint;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

                case -1_861_770_783: {
                    if (field.equals("curvePoint")) {
                        __temp_executeDef1 = false;
                        return this.curvePoint;
                    }

                    break;
                }

                case 3_745: {
                    if (field.equals("uv")) {
                        __temp_executeDef1 = false;
                        return this.uv;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("surfacePoint");
        baseArr.push("curvePoint");
        baseArr.push("uv");
        baseArr.push("u");
        super.__hx_getFields(baseArr);
    }

}
