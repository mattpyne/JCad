package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Vec_all_107__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.core.Vec_all_107__Fun __hx_current = new Vec_all_107__Fun();

    /**
     *
     */
    public Vec_all_107__Fun() {
        super(2, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke2_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        boolean a = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (haxe.lang.Runtime.toBool((__fn_float2))) : (haxe.lang.Runtime.toBool(__fn_dyn2)));
        boolean x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (haxe.lang.Runtime.toBool((__fn_float1))) : (haxe.lang.Runtime.toBool(__fn_dyn1)));
        if (a) {
            return x;
        } else {
            return false;
        }

    }

}
