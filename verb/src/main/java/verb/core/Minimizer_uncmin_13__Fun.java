package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Minimizer_uncmin_13__Fun extends haxe.lang.Function {

    /**
     *
     */
    public haxe.lang.Function f;

    /**
     *
     * @param f
     */
    public Minimizer_uncmin_13__Fun(haxe.lang.Function f) {
        super(1, 0);
        this.f = f;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.root.Array<java.lang.Object> x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.root.Array<java.lang.Object>) (((java.lang.Object) (__fn_float1))))) : (((haxe.root.Array<java.lang.Object>) (__fn_dyn1))));
        return verb.core.Minimizer.numericalGradient(this.f, x);
    }

}
