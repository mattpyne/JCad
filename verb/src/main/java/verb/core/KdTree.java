package verb.core;

import haxe.lang.Closure;
import haxe.lang.Runtime;
import haxe.lang.EmptyObject;
import haxe.lang.Function;
import haxe.lang.HxObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KdTree<T> extends HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param points
     * @param distanceFunction
     */
    public static <T_c> void __hx_ctor_verb_core_KdTree(KdTree<T_c> __hx_this, Array<KdPoint<T_c>> points, Function distanceFunction) {
        __hx_this.dim = 3;
        __hx_this.points = points;
        __hx_this.distanceFunction = distanceFunction;
        __hx_this.dim = points.get(0).point.length;
        __hx_this.root = __hx_this.buildTree(points, 0, null);
    }

    /**
     *
     */
    public Array<KdPoint<T>> points;

    /**
     *
     */
    public Function distanceFunction;

    /**
     *
     */
    public int dim;

    /**
     *
     */
    public KdNode<T> root;

    /**
     *
     * @param empty
     */
    public KdTree(EmptyObject empty) {
    }

    /**
     *
     * @param points
     * @param distanceFunction
     */
    public KdTree(Array<KdPoint<T>> points, Function distanceFunction) {
        KdTree.__hx_ctor_verb_core_KdTree(this, points, distanceFunction);
    }

    /**
     *
     * @param points
     * @param depth
     * @param parent
     * @return
     */
    public KdNode<T> buildTree(Array<KdPoint<T>> points, int depth, KdNode<T> parent) {
        int dim = (depth % this.dim);
        int median;
        KdNode<T> node;
        if (points.length == 0) {
            return null;
        }

        if (points.length == 1) {
            return new KdNode<>(points.get(0), dim, parent);
        }

        points.sort(new KdTree_buildTree_40__Fun(dim));
        median = points.length / 2;
        node = new KdNode<>(points.get(median), dim, parent);
        node.left = this.buildTree(points.slice(0, median), depth + 1, node);
        node.right = this.buildTree(points.slice(median + 1, null), depth + 1, node);
        return node;
    }

    /**
     *
     * @param point
     * @param maxNodes
     * @param maxDistance
     * @return
     */
    public Array<Pair<KdPoint, T>> nearest(Array<Number> point, int maxNodes, double maxDistance) {
        KdTree<T> _gthis = this;
        BinaryHeap<KdNode<T>> bestNodes = new BinaryHeap(new KdTree_nearest_64__Fun<>());
        Function[] nearestSearch = new Function[]{null};
        nearestSearch[0] = new KdTree_nearest_67__Fun(point, nearestSearch, maxNodes, bestNodes, _gthis);
        Function nearestSearch1 = nearestSearch[0];
        int _g12 = 0;
        int _g3 = maxNodes;
        while (_g12 < _g3) {
            _g12++;
            bestNodes.push(new Pair(null, maxDistance));
        }

        nearestSearch1.__hx_invoke1_o(0.0, this.root);

        Pair<KdPoint, Object>[] pairArrayType = new Pair[]{};

        Array<Pair<KdPoint, T>> result = new Array(pairArrayType);

        for (int _g13 = 0; _g13 < maxNodes; _g13++) {
            if (bestNodes.content.get(_g13).item0 != null) {
                result.push(new Pair(bestNodes.content.get(_g13).item0.kdPoint, Runtime.toDouble(bestNodes.content.get(_g13).item1)));
            }
        }

        return result;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 99_464: {
                    if (field.equals("dim")) {
                        __temp_executeDef1 = false;
                        this.dim = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_506_402: {
                    if (field.equals("root")) {
                        __temp_executeDef1 = false;
                        this.root = ((KdNode<T>) (value));
                        return value;
                    }

                    break;
                }

                case -982_754_077: {
                    if (field.equals("points")) {
                        __temp_executeDef1 = false;
                        this.points = ((Array<KdPoint<T>>) (value));
                        return value;
                    }

                    break;
                }

                case 99_464: {
                    if (field.equals("dim")) {
                        __temp_executeDef1 = false;
                        this.dim = (Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case -1_273_061_651: {
                    if (field.equals("distanceFunction")) {
                        __temp_executeDef1 = false;
                        this.distanceFunction = ((Function) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_825_779_806: {
                    if (field.equals("nearest")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "nearest"));
                    }

                    break;
                }

                case -982_754_077: {
                    if (field.equals("points")) {
                        __temp_executeDef1 = false;
                        return this.points;
                    }

                    break;
                }

                case -1_400_977_620: {
                    if (field.equals("buildTree")) {
                        __temp_executeDef1 = false;
                        return (new Closure(this, "buildTree"));
                    }

                    break;
                }

                case -1_273_061_651: {
                    if (field.equals("distanceFunction")) {
                        __temp_executeDef1 = false;
                        return this.distanceFunction;
                    }

                    break;
                }

                case 3_506_402: {
                    if (field.equals("root")) {
                        __temp_executeDef1 = false;
                        return this.root;
                    }

                    break;
                }

                case 99_464: {
                    if (field.equals("dim")) {
                        __temp_executeDef1 = false;
                        return this.dim;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 99_464: {
                    if (field.equals("dim")) {
                        __temp_executeDef1 = false;
                        return (this.dim);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_825_779_806: {
                    if (field.equals("nearest")) {
                        __temp_executeDef1 = false;
                        return this.nearest(((Array<Number>) (dynargs.get(0))), (Runtime.toInt(dynargs.get(1))), (Runtime.toDouble(dynargs.get(2))));
                    }

                    break;
                }

                case -1_400_977_620: {
                    if (field.equals("buildTree")) {
                        __temp_executeDef1 = false;
                        return this.buildTree(((Array<KdPoint<T>>) (dynargs.get(0))), (Runtime.toInt(dynargs.get(1))), ((KdNode<T>) (dynargs.get(2))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("root");
        baseArr.push("dim");
        baseArr.push("distanceFunction");
        baseArr.push("points");
        super.__hx_getFields(baseArr);
    }

}
