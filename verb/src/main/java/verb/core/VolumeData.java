package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class VolumeData extends verb.core.SerializableBase {

    /**
     *
     * @param __hx_this
     * @param degreeU
     * @param degreeV
     * @param degreeW
     * @param knotsU
     * @param knotsV
     * @param knotsW
     * @param controlPoints
     */
    public static void __hx_ctor_verb_core_VolumeData(verb.core.VolumeData __hx_this, int degreeU, int degreeV, int degreeW, haxe.root.Array<java.lang.Number> knotsU, haxe.root.Array<java.lang.Number> knotsV, haxe.root.Array<java.lang.Number> knotsW, haxe.root.Array<haxe.root.Array<haxe.root.Array<haxe.root.Array<java.lang.Number>>>> controlPoints) {
        __hx_this.degreeU = degreeU;
        __hx_this.degreeV = degreeV;
        __hx_this.degreeW = degreeW;
        __hx_this.knotsU = knotsU;
        __hx_this.knotsV = knotsV;
        __hx_this.knotsW = knotsW;
        __hx_this.controlPoints = controlPoints;
    }

    /**
     *
     */
    public int degreeU;

    /**
     *
     */
    public int degreeV;

    /**
     *
     */
    public int degreeW;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> knotsU;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> knotsV;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> knotsW;

    /**
     *
     */
    public haxe.root.Array<haxe.root.Array<haxe.root.Array<haxe.root.Array<java.lang.Number>>>> controlPoints;

    /**
     *
     * @param empty
     */
    public VolumeData(haxe.lang.EmptyObject empty) {
        super(haxe.lang.EmptyObject.EMPTY);
    }

    /**
     *
     * @param degreeU
     * @param degreeV
     * @param degreeW
     * @param knotsU
     * @param knotsV
     * @param knotsW
     * @param controlPoints
     */
    public VolumeData(int degreeU, int degreeV, int degreeW, haxe.root.Array<java.lang.Number> knotsU, haxe.root.Array<java.lang.Number> knotsV, haxe.root.Array<java.lang.Number> knotsW, haxe.root.Array<haxe.root.Array<haxe.root.Array<haxe.root.Array<java.lang.Number>>>> controlPoints) {
        verb.core.VolumeData.__hx_ctor_verb_core_VolumeData(this, degreeU, degreeV, degreeW, knotsU, knotsV, knotsW, controlPoints);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_546_218_251: {
                    if (field.equals("degreeW")) {
                        __temp_executeDef1 = false;
                        this.degreeW = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 1_546_218_249: {
                    if (field.equals("degreeU")) {
                        __temp_executeDef1 = false;
                        this.degreeU = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 1_546_218_250: {
                    if (field.equals("degreeV")) {
                        __temp_executeDef1 = false;
                        this.degreeV = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 793_539_744: {
                    if (field.equals("controlPoints")) {
                        __temp_executeDef1 = false;
                        this.controlPoints = ((haxe.root.Array<haxe.root.Array<haxe.root.Array<haxe.root.Array<java.lang.Number>>>>) (value));
                        return value;
                    }

                    break;
                }

                case 1_546_218_249: {
                    if (field.equals("degreeU")) {
                        __temp_executeDef1 = false;
                        this.degreeU = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case -1_126_638_900: {
                    if (field.equals("knotsW")) {
                        __temp_executeDef1 = false;
                        this.knotsW = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 1_546_218_250: {
                    if (field.equals("degreeV")) {
                        __temp_executeDef1 = false;
                        this.degreeV = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case -1_126_638_901: {
                    if (field.equals("knotsV")) {
                        __temp_executeDef1 = false;
                        this.knotsV = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 1_546_218_251: {
                    if (field.equals("degreeW")) {
                        __temp_executeDef1 = false;
                        this.degreeW = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case -1_126_638_902: {
                    if (field.equals("knotsU")) {
                        __temp_executeDef1 = false;
                        this.knotsU = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 793_539_744: {
                    if (field.equals("controlPoints")) {
                        __temp_executeDef1 = false;
                        return this.controlPoints;
                    }

                    break;
                }

                case 1_546_218_249: {
                    if (field.equals("degreeU")) {
                        __temp_executeDef1 = false;
                        return this.degreeU;
                    }

                    break;
                }

                case -1_126_638_900: {
                    if (field.equals("knotsW")) {
                        __temp_executeDef1 = false;
                        return this.knotsW;
                    }

                    break;
                }

                case 1_546_218_250: {
                    if (field.equals("degreeV")) {
                        __temp_executeDef1 = false;
                        return this.degreeV;
                    }

                    break;
                }

                case -1_126_638_901: {
                    if (field.equals("knotsV")) {
                        __temp_executeDef1 = false;
                        return this.knotsV;
                    }

                    break;
                }

                case 1_546_218_251: {
                    if (field.equals("degreeW")) {
                        __temp_executeDef1 = false;
                        return this.degreeW;
                    }

                    break;
                }

                case -1_126_638_902: {
                    if (field.equals("knotsU")) {
                        __temp_executeDef1 = false;
                        return this.knotsU;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_546_218_251: {
                    if (field.equals("degreeW")) {
                        __temp_executeDef1 = false;
                        return (this.degreeW);
                    }

                    break;
                }

                case 1_546_218_249: {
                    if (field.equals("degreeU")) {
                        __temp_executeDef1 = false;
                        return (this.degreeU);
                    }

                    break;
                }

                case 1_546_218_250: {
                    if (field.equals("degreeV")) {
                        __temp_executeDef1 = false;
                        return (this.degreeV);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("controlPoints");
        baseArr.push("knotsW");
        baseArr.push("knotsV");
        baseArr.push("knotsU");
        baseArr.push("degreeW");
        baseArr.push("degreeV");
        baseArr.push("degreeU");
        super.__hx_getFields(baseArr);
    }

}
