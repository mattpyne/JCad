package verb.core;

import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.lang.HxObject;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Interval<T> extends HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param min
     * @param max
     */
    public static <T_c> void __hx_ctor_verb_core_Interval(Interval<T_c> __hx_this, T_c min, T_c max) {
        __hx_this.min = min;
        __hx_this.max = max;
    }

    /**
     *
     */
    public T min;

    /**
     *
     */
    public T max;

    /**
     *
     * @param empty
     */
    public Interval(EmptyObject empty) {
    }

    /**
     *
     * @param min
     * @param max
     */
    public Interval(T min, T max) {
        Interval.__hx_ctor_verb_core_Interval(this, min, max);
    }

    public Number width() {
        return Runtime.toDouble(max)  - Runtime.toDouble(min);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 107_876: {
                if (field.equals("max")) {
                    this.max = ((T) (((java.lang.Object) (value))));
                    return (Runtime.toDouble(value));
                }
                break;
            }

            case 108_114: {
                if (field.equals("min")) {
                    this.min = ((T) (((java.lang.Object) (value))));
                    return (Runtime.toDouble(value));
                }
                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField_f(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 107_876: {
                if (field.equals("max")) {
                    this.max = ((T) (value));
                    return value;
                }
                break;
            }

            case 108_114: {
                if (field.equals("min")) {
                    this.min = ((T) (value));
                    return value;
                }
                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 107_876: {
                if (field.equals("max")) {
                    return this.max;
                }
                break;
            }

            case 108_114: {
                if (field.equals("min")) {
                    return this.min;
                }
                break;
            }
        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 107_876: {
                if (field.equals("max")) {
                    return (Runtime.toDouble(this.max));
                }
                break;
            }

            case 108_114: {
                if (field.equals("min")) {
                    return (Runtime.toDouble(this.min));
                }
                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField_f(field, throwErrors, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("max");
        baseArr.push("min");
        super.__hx_getFields(baseArr);
    }

}
