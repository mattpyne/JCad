package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class TriSegmentIntersection extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param point
     * @param s
     * @param t
     * @param r
     */
    public static void __hx_ctor_verb_core_TriSegmentIntersection(verb.core.TriSegmentIntersection __hx_this, haxe.root.Array<java.lang.Number> point, double s, double t, double r) {
        __hx_this.point = point;
        __hx_this.s = s;
        __hx_this.t = t;
        __hx_this.p = r;
    }

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> point;

    /**
     *
     */
    public double s;

    /**
     *
     */
    public double t;

    /**
     *
     */
    public double p;

    /**
     *
     * @param empty
     */
    public TriSegmentIntersection(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param point
     * @param s
     * @param t
     * @param r
     */
    public TriSegmentIntersection(haxe.root.Array<java.lang.Number> point, double s, double t, double r) {
        verb.core.TriSegmentIntersection.__hx_ctor_verb_core_TriSegmentIntersection(this, point, s, t, r);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 112: {
                    if (field.equals("p")) {
                        __temp_executeDef1 = false;
                        this.p = (value);
                        return value;
                    }

                    break;
                }

                case 115: {
                    if (field.equals("s")) {
                        __temp_executeDef1 = false;
                        this.s = (value);
                        return value;
                    }

                    break;
                }

                case 116: {
                    if (field.equals("t")) {
                        __temp_executeDef1 = false;
                        this.t = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 112: {
                    if (field.equals("p")) {
                        __temp_executeDef1 = false;
                        this.p = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        this.point = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 116: {
                    if (field.equals("t")) {
                        __temp_executeDef1 = false;
                        this.t = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case 115: {
                    if (field.equals("s")) {
                        __temp_executeDef1 = false;
                        this.s = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 112: {
                    if (field.equals("p")) {
                        __temp_executeDef1 = false;
                        return this.p;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point;
                    }

                    break;
                }

                case 116: {
                    if (field.equals("t")) {
                        __temp_executeDef1 = false;
                        return this.t;
                    }

                    break;
                }

                case 115: {
                    if (field.equals("s")) {
                        __temp_executeDef1 = false;
                        return this.s;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 112: {
                    if (field.equals("p")) {
                        __temp_executeDef1 = false;
                        return this.p;
                    }

                    break;
                }

                case 115: {
                    if (field.equals("s")) {
                        __temp_executeDef1 = false;
                        return this.s;
                    }

                    break;
                }

                case 116: {
                    if (field.equals("t")) {
                        __temp_executeDef1 = false;
                        return this.t;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("p");
        baseArr.push("t");
        baseArr.push("s");
        baseArr.push("point");
        super.__hx_getFields(baseArr);
    }

}
