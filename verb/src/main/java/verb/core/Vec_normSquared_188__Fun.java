package verb.core;

import haxe.lang.Function;
import haxe.lang.Runtime;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Vec_normSquared_188__Fun extends Function {

    /**
     *
     */
    public static verb.core.Vec_normSquared_188__Fun __hx_current = new Vec_normSquared_188__Fun();

    /**
     *
     */
    public Vec_normSquared_188__Fun() {
        super(2, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public double __hx_invoke2_f(double __fn_float1, Object __fn_dyn1, double __fn_float2, Object __fn_dyn2) {
        double a1 = Runtime.toDouble(__fn_dyn2);
        double x = Runtime.toDouble(__fn_dyn1);
        return a1 + (x * x);
    }

}
