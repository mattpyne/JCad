package verb.core;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KdNode<T> extends haxe.lang.HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param kdPoint
     * @param dimension
     * @param parent
     */
    public static <T_c> void __hx_ctor_verb_core_KdNode(verb.core.KdNode<T_c> __hx_this, verb.core.KdPoint<T_c> kdPoint, int dimension, verb.core.KdNode<T_c> parent) {
        __hx_this.kdPoint = kdPoint;
        __hx_this.left = null;
        __hx_this.right = null;
        __hx_this.parent = parent;
        __hx_this.dimension = dimension;
    }

    /**
     *
     */
    public verb.core.KdPoint<T> kdPoint;

    /**
     *
     */
    public verb.core.KdNode<T> left;

    /**
     *
     */
    public verb.core.KdNode<T> right;

    /**
     *
     */
    public verb.core.KdNode<T> parent;

    /**
     *
     */
    public int dimension;

    /**
     *
     * @param empty
     */
    public KdNode(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param kdPoint
     * @param dimension
     * @param parent
     */
    public KdNode(verb.core.KdPoint<T> kdPoint, int dimension, verb.core.KdNode<T> parent) {
        verb.core.KdNode.__hx_ctor_verb_core_KdNode(this, kdPoint, dimension, parent);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_095_013_018: {
                    if (field.equals("dimension")) {
                        __temp_executeDef1 = false;
                        this.dimension = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_095_013_018: {
                    if (field.equals("dimension")) {
                        __temp_executeDef1 = false;
                        this.dimension = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case -881_145_929: {
                    if (field.equals("kdPoint")) {
                        __temp_executeDef1 = false;
                        this.kdPoint = ((verb.core.KdPoint<T>) (value));
                        return value;
                    }

                    break;
                }

                case -995_424_086: {
                    if (field.equals("parent")) {
                        __temp_executeDef1 = false;
                        this.parent = ((verb.core.KdNode<T>) (value));
                        return value;
                    }

                    break;
                }

                case 3_317_767: {
                    if (field.equals("left")) {
                        __temp_executeDef1 = false;
                        this.left = ((verb.core.KdNode<T>) (value));
                        return value;
                    }

                    break;
                }

                case 108_511_772: {
                    if (field.equals("right")) {
                        __temp_executeDef1 = false;
                        this.right = ((verb.core.KdNode<T>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_095_013_018: {
                    if (field.equals("dimension")) {
                        __temp_executeDef1 = false;
                        return this.dimension;
                    }

                    break;
                }

                case -881_145_929: {
                    if (field.equals("kdPoint")) {
                        __temp_executeDef1 = false;
                        return this.kdPoint;
                    }

                    break;
                }

                case -995_424_086: {
                    if (field.equals("parent")) {
                        __temp_executeDef1 = false;
                        return this.parent;
                    }

                    break;
                }

                case 3_317_767: {
                    if (field.equals("left")) {
                        __temp_executeDef1 = false;
                        return this.left;
                    }

                    break;
                }

                case 108_511_772: {
                    if (field.equals("right")) {
                        __temp_executeDef1 = false;
                        return this.right;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_095_013_018: {
                    if (field.equals("dimension")) {
                        __temp_executeDef1 = false;
                        return (this.dimension);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("dimension");
        baseArr.push("parent");
        baseArr.push("right");
        baseArr.push("left");
        baseArr.push("kdPoint");
        super.__hx_getFields(baseArr);
    }

}
