package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class SurfaceSurfaceIntersectionPoint extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param uv0
     * @param uv1
     * @param point
     * @param dist
     */
    public static void __hx_ctor_verb_core_SurfaceSurfaceIntersectionPoint(verb.core.SurfaceSurfaceIntersectionPoint __hx_this, haxe.root.Array<java.lang.Number> uv0, haxe.root.Array<java.lang.Number> uv1, haxe.root.Array<java.lang.Number> point, double dist) {
        __hx_this.uv0 = uv0;
        __hx_this.uv1 = uv1;
        __hx_this.point = point;
        __hx_this.dist = dist;
    }

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> uv0;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> uv1;

    /**
     *
     */
    public haxe.root.Array<java.lang.Number> point;

    /**
     *
     */
    public double dist;

    /**
     *
     * @param empty
     */
    public SurfaceSurfaceIntersectionPoint(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param uv0
     * @param uv1
     * @param point
     * @param dist
     */
    public SurfaceSurfaceIntersectionPoint(haxe.root.Array<java.lang.Number> uv0, haxe.root.Array<java.lang.Number> uv1, haxe.root.Array<java.lang.Number> point, double dist) {
        verb.core.SurfaceSurfaceIntersectionPoint.__hx_ctor_verb_core_SurfaceSurfaceIntersectionPoint(this, uv0, uv1, point, dist);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_083_686: {
                    if (field.equals("dist")) {
                        __temp_executeDef1 = false;
                        this.dist = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_083_686: {
                    if (field.equals("dist")) {
                        __temp_executeDef1 = false;
                        this.dist = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case 116_143: {
                    if (field.equals("uv0")) {
                        __temp_executeDef1 = false;
                        this.uv0 = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        this.point = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

                case 116_144: {
                    if (field.equals("uv1")) {
                        __temp_executeDef1 = false;
                        this.uv1 = ((haxe.root.Array<java.lang.Number>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_083_686: {
                    if (field.equals("dist")) {
                        __temp_executeDef1 = false;
                        return this.dist;
                    }

                    break;
                }

                case 116_143: {
                    if (field.equals("uv0")) {
                        __temp_executeDef1 = false;
                        return this.uv0;
                    }

                    break;
                }

                case 106_845_584: {
                    if (field.equals("point")) {
                        __temp_executeDef1 = false;
                        return this.point;
                    }

                    break;
                }

                case 116_144: {
                    if (field.equals("uv1")) {
                        __temp_executeDef1 = false;
                        return this.uv1;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_083_686: {
                    if (field.equals("dist")) {
                        __temp_executeDef1 = false;
                        return this.dist;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("dist");
        baseArr.push("point");
        baseArr.push("uv1");
        baseArr.push("uv0");
        super.__hx_getFields(baseArr);
    }

}
