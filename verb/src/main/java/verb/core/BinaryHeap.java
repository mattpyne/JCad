package verb.core;

import haxe.lang.Closure;
import haxe.lang.Runtime;
import haxe.lang.EmptyObject;
import haxe.lang.Function;
import haxe.lang.HaxeException;
import haxe.lang.HxObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class BinaryHeap<T> extends HxObject {

    /**
     *
     * @param <T_c>
     * @param __hx_this
     * @param scoreFunction
     */
    public static <T_c> void __hx_ctor_verb_core_BinaryHeap(BinaryHeap<T_c> __hx_this, Function scoreFunction) {
        __hx_this.content = new Array<>(new Pair[]{});
        __hx_this.scoreFunction = scoreFunction;
    }

    /**
     *
     */
    public Array<Pair<T, Object>> content;

    /**
     *
     */
    public Function scoreFunction;

    /**
     *
     * @param empty
     */
    public BinaryHeap(EmptyObject empty) {
    }

    /**
     *
     * @param scoreFunction
     */
    public BinaryHeap(Function scoreFunction) {
        BinaryHeap.__hx_ctor_verb_core_BinaryHeap(this, scoreFunction);
    }

    /**
     *
     * @param element
     */
    public void push(Pair<T, Object> element) {
        this.content.push(element);
        this.bubbleUp((this.content.length - 1));
    }

    /**
     *
     * @return
     */
    public Pair<T, Object> pop() {
        Pair<T, Object> result = this.content.get(0);
        Pair<T, Object> end = ((Pair<T, Object>) (this.content.pop()));
        if ((this.content.length > 0)) {
            this.content.set(0, end);
            this.sinkDown(0);
        }

        return result;
    }

    /**
     *
     * @return
     */
    public Pair<T, Object> peek() {
        return this.content.get(0);
    }

    /**
     *
     * @param node
     */
    public void remove(Pair<T, Object> node) {
        int len = this.content.length;
        {
            int _g1 = 0;
            int _g = len;
            while ((_g1 < _g)) {
                int i = _g1++;
                if ((this.content.get(i) == node)) {
                    Pair<T, Object> end = ((Pair<T, Object>) (this.content.pop()));
                    if ((i != (len - 1))) {
                        this.content.set(i, end);
                        if (((this.scoreFunction.__hx_invoke1_f(0.0, end)) < (this.scoreFunction.__hx_invoke1_f(0.0, node)))) {
                            this.bubbleUp(i);
                        } else {
                            this.sinkDown(i);
                        }
                    }

                    return;
                }
            }
        }

        throw HaxeException.wrap("Node not found.");
    }

    /**
     *
     * @return
     */
    public int size() {
        return this.content.length;
    }

    /**
     *
     * @param n
     */
    public void bubbleUp(int n) {
        Pair<T, Object> element = this.content.get(n);
        while ((n > 0)) {
            int parentN = (((int) (Math.floor((((n + 1.0)) / 2)))) - 1);
            Pair<T, Object> parent = this.content.get(parentN);
            if (((this.scoreFunction.__hx_invoke1_f(0.0, element)) < (this.scoreFunction.__hx_invoke1_f(0.0, parent)))) {
                this.content.set(parentN, element);
                this.content.set(n, parent);
                n = parentN;
            } else {
                break;
            }

        }

    }

    /**
     *
     * @param n
     */
    public void sinkDown(int n) {
        int length = this.content.length;
        Pair<T, Object> element = this.content.get(n);
        double elemScore = (this.scoreFunction.__hx_invoke1_f(0.0, element));
        while (true) {
            int child2N = (((n + 1)) * 2);
            int child1N = (child2N - 1);
            int swap = -1;
            double child1Score = 0.0;
            if ((child1N < length)) {
                Pair<T, Object> child1 = this.content.get(child1N);
                child1Score = (this.scoreFunction.__hx_invoke1_f(0.0, child1));
                if ((child1Score < elemScore)) {
                    swap = child1N;
                }

            }

            if ((child2N < length)) {
                Pair<T, Object> child2 = this.content.get(child2N);
                double child2Score = (this.scoreFunction.__hx_invoke1_f(0.0, child2));
                if ((child2Score < ((((swap == -1)) ? (elemScore) : (child1Score))))) {
                    swap = child2N;
                }

            }

            if ((swap != -1)) {
                this.content.set(n, this.content.get(swap));
                this.content.set(swap, element);
                n = swap;
            } else {
                break;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 1_268_147_338: {
                    if (field.equals("scoreFunction")) {
                        this.scoreFunction = ((Function) (value));
                        return value;
                    }

                    break;
                }

                case 951_530_617: {
                    if (field.equals("content")) {
                        this.content = ((Array<Pair<T, Object>>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw new RuntimeException();
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 508_491_221: {
                    if (field.equals("sinkDown")) {
                        return (new Closure(this, "sinkDown"));
                    }

                    break;
                }

                case 951_530_617: {
                    if (field.equals("content")) {
                        return this.content;
                    }

                    break;
                }

                case -1_640_051_641: {
                    if (field.equals("bubbleUp")) {
                        return (new Closure(this, "bubbleUp"));
                    }

                    break;
                }

                case 1_268_147_338: {
                    if (field.equals("scoreFunction")) {
                        return this.scoreFunction;
                    }

                    break;
                }

                case 3_530_753: {
                    if (field.equals("size")) {
                        return (new Closure(this, "size"));
                    }

                    break;
                }

                case 3_452_698: {
                    if (field.equals("push")) {
                        return (new Closure(this, "push"));
                    }

                    break;
                }

                case -934_610_812: {
                    if (field.equals("remove")) {
                        return (new Closure(this, "remove"));
                    }

                    break;
                }

                case 111_185: {
                    if (field.equals("pop")) {
                        return (new Closure(this, "pop"));
                    }

                    break;
                }

                case 3_436_891: {
                    if (field.equals("peek")) {
                        return (new Closure(this, "peek"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw new RuntimeException();
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public Object __hx_invokeField(String field, Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 508_491_221: {
                    if (field.equals("sinkDown")) {
                        __temp_executeDef1 = false;
                        this.sinkDown((Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case 3_452_698: {
                    if (field.equals("push")) {
                        __temp_executeDef1 = false;
                        this.push(((Pair<T, Object>) (dynargs.get(0))));
                    }

                    break;
                }

                case -1_640_051_641: {
                    if (field.equals("bubbleUp")) {
                        __temp_executeDef1 = false;
                        this.bubbleUp((Runtime.toInt(dynargs.get(0))));
                    }

                    break;
                }

                case 111_185: {
                    if (field.equals("pop")) {
                        return this.pop();
                    }

                    break;
                }

                case 3_530_753: {
                    if (field.equals("size")) {
                        return this.size();
                    }

                    break;
                }

                case 3_436_891: {
                    if (field.equals("peek")) {
                        return this.peek();
                    }

                    break;
                }

                case -934_610_812: {
                    if (field.equals("remove")) {
                        __temp_executeDef1 = false;
                        this.remove(((Pair<T, Object>) (dynargs.get(0))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("scoreFunction");
        baseArr.push("content");
        super.__hx_getFields(baseArr);
    }

}
