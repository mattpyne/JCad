package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class SerializableBase extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_core_SerializableBase(verb.core.SerializableBase __hx_this) {
    }

    /**
     *
     * @param empty
     */
    public SerializableBase(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public SerializableBase() {
        verb.core.SerializableBase.__hx_ctor_verb_core_SerializableBase(this);
    }

    /**
     *
     * @return
     */
    public java.lang.String serialize() {
        haxe.Serializer serializer = new haxe.Serializer();
        serializer.serialize(this);
        return serializer.toString();
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -573_479_200: {
                    if (field.equals("serialize")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "serialize"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -573_479_200: {
                    if (field.equals("serialize")) {
                        __temp_executeDef1 = false;
                        return this.serialize();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

}
