package verb.core;

import haxe.lang.EmptyObject;
import haxe.root.Array;

/**
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class MeshData extends SerializableBase {

    /**
     * @param __hx_this
     * @param faces
     * @param points
     * @param normals
     * @param uvs
     */
    public static void __hx_ctor_verb_core_MeshData(MeshData __hx_this,
                                                    Array<Array<Number>> faces,
                                                    Array<Array<Number>> points,
                                                    Array<Array<Number>> normals,
                                                    Array<Array<Number>> uvs) {

        __hx_this.faces = faces;
        __hx_this.points = points;
        __hx_this.normals = normals;
        __hx_this.uvs = uvs;
    }

    /**
     * @return
     */
    public static MeshData empty() {
        return new MeshData(new Array<>(new Array[]{}),
                new Array<>(new Array[]{}),
                new Array<>(new Array[]{}),
                new Array<>(new Array[]{}));
    }

    /**
     *
     */
    public Array<Array<Number>> faces;

    /**
     *
     */
    public Array<Array<Number>> points;

    /**
     *
     */
    public Array<Array<Number>> normals;

    /**
     *
     */
    public Array<Array<Number>> uvs;

    /**
     * @param empty
     */
    public MeshData(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     * @param faces
     * @param points
     * @param normals
     * @param uvs
     */
    public MeshData(Array<Array<Number>> faces,
                    Array<Array<Number>> points,
                    Array<Array<Number>> normals,
                    Array<Array<Number>> uvs) {

        MeshData.__hx_ctor_verb_core_MeshData(this, faces, points, normals, uvs);
    }

    /**
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {

        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 116_210: {
                if (field.equals("uvs")) {
                    __temp_executeDef1 = false;
                    this.uvs = ((Array<Array<Number>>) (value));
                    return value;
                }

                break;
            }

            case 97_187_254: {
                if (field.equals("faces")) {
                    __temp_executeDef1 = false;
                    this.faces = ((Array<Array<Number>>) (value));
                    return value;
                }

                break;
            }

            case 2_127_618_156: {
                if (field.equals("normals")) {
                    __temp_executeDef1 = false;
                    this.normals = ((Array<Array<Number>>) (value));
                    return value;
                }

                break;
            }

            case -982_754_077: {
                if (field.equals("points")) {
                    __temp_executeDef1 = false;
                    this.points = ((Array<Array<Number>>) (value));
                    return value;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {

        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case 116_210: {
                if (field.equals("uvs")) {
                    __temp_executeDef1 = false;
                    return this.uvs;
                }

                break;
            }

            case 97_187_254: {
                if (field.equals("faces")) {
                    __temp_executeDef1 = false;
                    return this.faces;
                }

                break;
            }

            case 2_127_618_156: {
                if (field.equals("normals")) {
                    __temp_executeDef1 = false;
                    return this.normals;
                }

                break;
            }

            case -982_754_077: {
                if (field.equals("points")) {
                    __temp_executeDef1 = false;
                    return this.points;
                }

                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("uvs");
        baseArr.push("normals");
        baseArr.push("points");
        baseArr.push("faces");
        super.__hx_getFields(baseArr);
    }

}
