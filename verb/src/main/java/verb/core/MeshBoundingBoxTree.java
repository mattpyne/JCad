package verb.core;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class MeshBoundingBoxTree extends haxe.lang.HxObject implements verb.eval.IBoundingBoxTree<java.lang.Number> {

    /**
     *
     * @param __hx_this
     * @param mesh
     * @param faceIndices
     */
    public static void __hx_ctor_verb_core_MeshBoundingBoxTree(verb.core.MeshBoundingBoxTree __hx_this, verb.core.MeshData mesh, haxe.root.Array<java.lang.Number> faceIndices) {
        __hx_this._empty = false;
        __hx_this._face = -1;
        if ((faceIndices == null)) {
            haxe.root.Array<java.lang.Number> _g = new haxe.root.Array<>(new java.lang.Number[]{});
            {
                int _g2 = 0;
                int _g1 = mesh.faces.length;
                while ((_g2 < _g1)) {
                    int i = _g2++;
                    _g.push(i);
                }

            }

            faceIndices = _g;
        }

        __hx_this._boundingBox = verb.core.Mesh.makeMeshAabb(mesh, faceIndices);
        if ((faceIndices.length < 1)) {
            __hx_this._empty = true;
            return;
        } else {
            if ((faceIndices.length < 2)) {
                __hx_this._face = (haxe.lang.Runtime.toInt(faceIndices.get(0)));
                return;
            }

        }

        haxe.root.Array<java.lang.Number> as = verb.core.Mesh.sortTrianglesOnLongestAxis(__hx_this._boundingBox, mesh, faceIndices);
        haxe.root.Array<java.lang.Number> l = (verb.core.ArrayExtensions.left(as));
        haxe.root.Array<java.lang.Number> r = (verb.core.ArrayExtensions.right(as));
        __hx_this._children = new verb.core.Pair<>(new verb.core.MeshBoundingBoxTree(mesh, l), new verb.core.MeshBoundingBoxTree(mesh, r));
    }

    /**
     *
     */
    public verb.core.Pair<verb.eval.IBoundingBoxTree<java.lang.Number>, verb.eval.IBoundingBoxTree<java.lang.Number>> _children;

    /**
     *
     */
    public verb.core.BoundingBox _boundingBox;

    /**
     *
     */
    public int _face;

    /**
     *
     */
    public boolean _empty;

    /**
     *
     * @param empty
     */
    public MeshBoundingBoxTree(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param mesh
     * @param faceIndices
     */
    public MeshBoundingBoxTree(verb.core.MeshData mesh, haxe.root.Array<java.lang.Number> faceIndices) {
        verb.core.MeshBoundingBoxTree.__hx_ctor_verb_core_MeshBoundingBoxTree(this, mesh, faceIndices);
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.Pair<verb.eval.IBoundingBoxTree<java.lang.Number>, verb.eval.IBoundingBoxTree<java.lang.Number>> split() {
        return this._children;
    }

    /**
     *
     * @return
     */
    @Override
    public verb.core.BoundingBox boundingBox() {
        return this._boundingBox;
    }

    /**
     *
     * @return
     */
    @Override
    public java.lang.Number yield() {
        return this._face;
    }

    /**
     *
     * @param tolerance
     * @return
     */
    @Override
    public boolean indivisible(double tolerance) {
        return (this._children == null);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean empty() {
        return this._empty;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 90_869_564: {
                    if (field.equals("_face")) {
                        __temp_executeDef1 = false;
                        this._face = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_478_563_762: {
                    if (field.equals("_empty")) {
                        __temp_executeDef1 = false;
                        this._empty = haxe.lang.Runtime.toBool(value);
                        return value;
                    }

                    break;
                }

                case 1_750_044_510: {
                    if (field.equals("_children")) {
                        __temp_executeDef1 = false;
                        this._children = ((verb.core.Pair<verb.eval.IBoundingBoxTree<java.lang.Number>, verb.eval.IBoundingBoxTree<java.lang.Number>>) (value));
                        return value;
                    }

                    break;
                }

                case 90_869_564: {
                    if (field.equals("_face")) {
                        __temp_executeDef1 = false;
                        this._face = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case -1_884_107_832: {
                    if (field.equals("_boundingBox")) {
                        __temp_executeDef1 = false;
                        this._boundingBox = ((verb.core.BoundingBox) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 96_634_189: {
                    if (field.equals("empty")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "empty"));
                    }

                    break;
                }

                case 1_750_044_510: {
                    if (field.equals("_children")) {
                        __temp_executeDef1 = false;
                        return this._children;
                    }

                    break;
                }

                case -1_421_565_752: {
                    if (field.equals("indivisible")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "indivisible"));
                    }

                    break;
                }

                case -1_884_107_832: {
                    if (field.equals("_boundingBox")) {
                        __temp_executeDef1 = false;
                        return this._boundingBox;
                    }

                    break;
                }

                case 114_974_605: {
                    if (field.equals("yield")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "yield"));
                    }

                    break;
                }

                case 90_869_564: {
                    if (field.equals("_face")) {
                        __temp_executeDef1 = false;
                        return this._face;
                    }

                    break;
                }

                case -1_262_064_249: {
                    if (field.equals("boundingBox")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "boundingBox"));
                    }

                    break;
                }

                case -1_478_563_762: {
                    if (field.equals("_empty")) {
                        __temp_executeDef1 = false;
                        return this._empty;
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "split"));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 90_869_564: {
                    if (field.equals("_face")) {
                        __temp_executeDef1 = false;
                        return (this._face);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 96_634_189: {
                    if (field.equals("empty")) {
                        __temp_executeDef1 = false;
                        return this.empty();
                    }

                    break;
                }

                case 109_648_666: {
                    if (field.equals("split")) {
                        __temp_executeDef1 = false;
                        return this.split();
                    }

                    break;
                }

                case -1_421_565_752: {
                    if (field.equals("indivisible")) {
                        __temp_executeDef1 = false;
                        return this.indivisible((haxe.lang.Runtime.toDouble(dynargs.get(0))));
                    }

                    break;
                }

                case -1_262_064_249: {
                    if (field.equals("boundingBox")) {
                        __temp_executeDef1 = false;
                        return this.boundingBox();
                    }

                    break;
                }

                case 114_974_605: {
                    if (field.equals("yield")) {
                        __temp_executeDef1 = false;
                        return this.yield();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("_empty");
        baseArr.push("_face");
        baseArr.push("_boundingBox");
        baseArr.push("_children");
        super.__hx_getFields(baseArr);
    }

}
