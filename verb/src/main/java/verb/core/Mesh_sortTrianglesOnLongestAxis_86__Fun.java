package verb.core;

import haxe.lang.Function;
import haxe.lang.Runtime;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Mesh_sortTrianglesOnLongestAxis_86__Fun extends Function {

    /**
     *
     */
    public final static Mesh_sortTrianglesOnLongestAxis_86__Fun __hx_current = new Mesh_sortTrianglesOnLongestAxis_86__Fun();

    /**
     *
     */
    public Mesh_sortTrianglesOnLongestAxis_86__Fun() {
        super(2, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public double __hx_invoke2_f(double __fn_float1, Object __fn_dyn1, double __fn_float2, Object __fn_dyn2) {
        Pair<Object, Object> b = (Pair<Object, Object>) __fn_dyn2;
        Pair<Object, Object> a = (Pair<Object, Object>) __fn_dyn1;
        double a0 = Runtime.toDouble(a.item0);
        double b0 = Runtime.toDouble(b.item0);
        if (a0 == b0) {
            return 0;
        } else {
            if (a0 > b0) {
                return 1;
            } else {
                return -1;
            }
        }
    }

}
