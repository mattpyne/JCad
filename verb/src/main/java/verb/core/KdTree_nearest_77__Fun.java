package verb.core;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KdTree_nearest_77__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public int maxNodes;

    /**
     *
     */
    public verb.core.BinaryHeap<verb.core.KdNode<T>> bestNodes;

    /**
     *
     * @param maxNodes
     * @param bestNodes
     */
    public KdTree_nearest_77__Fun(int maxNodes, verb.core.BinaryHeap<verb.core.KdNode<T>> bestNodes) {
        super(2, 0);
        this.maxNodes = maxNodes;
        this.bestNodes = bestNodes;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke2_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        double distance = ((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (haxe.lang.Runtime.toDouble(__fn_dyn2));
        verb.core.KdNode<T> node1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined))
                ? (((verb.core.KdNode<T>) (((java.lang.Object) (__fn_float1)))))
                : (((verb.core.KdNode<T>) (__fn_dyn1))));
        this.bestNodes.push(new verb.core.Pair<>(node1, distance));
        if ((this.bestNodes.size() > this.maxNodes)) {
            this.bestNodes.pop();
        }
        return null;
    }

}
