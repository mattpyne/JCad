package verb.core;

import haxe.lang.EmptyObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Ray extends verb.core.SerializableBase {

    /**
     *
     * @param __hx_this
     * @param origin
     * @param dir
     */
    public static void __hx_ctor_verb_core_Ray(verb.core.Ray __hx_this, Array<Number> origin, Array<Number> dir) {
        __hx_this.origin = origin;
        __hx_this.dir = dir;
    }

    /**
     *
     */
    public Array<Number> dir;

    /**
     *
     */
    public Array<Number> origin;

    /**
     *
     * @param empty
     */
    public Ray(EmptyObject empty) {
        super(EmptyObject.EMPTY);
    }

    /**
     *
     * @param origin
     * @param dir
     */
    public Ray(Array<Number> origin, Array<Number> dir) {
        verb.core.Ray.__hx_ctor_verb_core_Ray(this, origin, dir);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_setField(String field, Object value, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case -1_008_619_738: {
                if (field.equals("origin")) {
                    this.origin = (Array<Number>) value;
                    return value;
                }
                break;
            }

            case 99_469: {
                if (field.equals("dir")) {
                    this.dir = (Array<Number>) value;
                    return value;
                }
                break;
            }
        }

        if (__temp_executeDef1) {
            return super.__hx_setField(field, value, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public Object __hx_getField(String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        boolean __temp_executeDef1 = true;
        switch (field.hashCode()) {
            case -1_008_619_738: {
                if (field.equals("origin")) {
                    return this.origin;
                }
                break;
            }

            case 99_469: {
                if (field.equals("dir")) {
                    return this.dir;
                }
                break;
            }

        }

        if (__temp_executeDef1) {
            return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
        } else {
            throw null;
        }
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(Array<String> baseArr) {
        baseArr.push("origin");
        baseArr.push("dir");
        super.__hx_getFields(baseArr);
    }

}
