package verb.core;

import haxe.lang.EmptyObject;
import haxe.lang.Runtime;
import haxe.lang.Function;
import haxe.lang.HxObject;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ArrayExtensions extends HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_core_ArrayExtensions(ArrayExtensions __hx_this) {
    }

    /**
     *
     * @param <T>
     * @param a
     * @param n
     */
    public static <T> void alloc(Array<T> a, int n) {
        if (n < 0) {
            
            return;
        }

        while (a.length < n) {
            
            a.push(null);
        }
    }

    /**
     *
     * @param <T>
     * @param a
     * @return
     */
    public static <T> Array<T> reversed(Array<T> a) {
        Array<T> ac = a.copy();
        ac.reverse();
        return ac;
    }

    /**
     *
     * @param <T>
     * @param a
     * @return
     */
    public static <T> T last(Array<T> a) {
        return a.get(a.length - 1);
    }

    /**
     *
     * @param <T>
     * @param a
     * @return
     */
    public static <T> T first(Array<T> a) {
        return a.get(0);
    }

    /**
     *
     * @param <T>
     * @param a
     * @param start
     * @param end
     * @param ele
     */
    public static <T> void spliceAndInsert(Array<T> a, int start, int end, T ele) {
        a.splice(start, end);
        a.insert(start, ele);
    }

    /**
     *
     * @param <T>
     * @param arr
     * @return
     */
    public static <T> Array<T> left(Array<T> arr) {
        if (arr.length == 0) {

            return new Array(new Object[]{});
        }

        int len = (int) Math.ceil(((double) arr.length) / 2);
        return arr.slice(0, len);
    }

    /**
     *
     * @param <T>
     * @param arr
     * @return
     */
    public static <T> Array<T> right(Array<T> arr) {
        if (arr.length == 0) {

            return new Array((T[]) new Object[]{});
        }

        int len = (int) Math.ceil((((double) arr.length) / 2));
        return arr.slice(len, null);
    }

    /**
     *
     * @param <T>
     * @param arr
     * @return
     */
    public static <T> Array<T> rightWithPivot(Array<T> arr) {
        if (arr.length == 0) {

            return new Array((T[]) new Object[]{});
        }

        int len = (int) (Math.ceil(((double) arr.length) / 2));
        return arr.slice((len - 1), null);
    }

    /**
     *
     * @param <T>
     * @param arr
     * @param comp
     * @return
     */
    public static <T> Array<T> unique(Array<T> arr, Function comp) {
        if (arr.length == 0) {

            return new Array((T[]) new Object[]{});
        }

        Array<T> uniques = new Array((T[]) new Object[]{arr.pop()});
        while (arr.length > 0) {

            T ele = (T) arr.pop();
            boolean isUnique = true;
            for (int j = 0; j < uniques.length; j++) {

                Object unique = uniques.get(j);
                if (Runtime.toBool(comp.__hx_invoke2_o(0.0, ele, 0.0, unique))) {

                    isUnique = false;
                    break;
                }
            }

            if (isUnique) {

                uniques.push(ele);
            }
        }

        return uniques;
    }

    /**
     *
     * @param empty
     */
    public ArrayExtensions(EmptyObject empty) {
    }

    /**
     *
     */
    public ArrayExtensions() {
        ArrayExtensions.__hx_ctor_verb_core_ArrayExtensions(this);
    }

}
