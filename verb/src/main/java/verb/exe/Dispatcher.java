package verb.exe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Dispatcher extends haxe.lang.HxObject {

    /**
     *
     */
    public static int THREADS;

    /**
     *
     */
    public static verb.exe.ThreadPool _threadPool;

    /**
     *
     */
    public static boolean _init;

    static {
        verb.exe.Dispatcher.THREADS = 1;
        verb.exe.Dispatcher._init = false;
    }

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_exe_Dispatcher(verb.exe.Dispatcher __hx_this) {
    }

    /**
     *
     */
    public static void init() {
        if (verb.exe.Dispatcher._init) {
            return;
        }

        verb.exe.Dispatcher._threadPool = new verb.exe.ThreadPool((verb.exe.Dispatcher.THREADS));
        verb.exe.Dispatcher._init = true;
    }

    /**
     *
     * @param <T>
     * @param classType
     * @param methodName
     * @param args
     * @return
     */
    public static <T> promhx.Promise<T> dispatchMethod(java.lang.Class classType, java.lang.String methodName, haxe.root.Array args) {
        verb.exe.Dispatcher.init();
        promhx.Deferred<T> def = new promhx.Deferred<>();
        haxe.lang.Function callback = new verb.exe.Dispatcher_dispatchMethod_44__Fun<>(def);
        verb.exe.Dispatcher._threadPool.addTask(new verb.exe.Dispatcher_dispatchMethod_51__Fun(methodName, classType, args), null, callback);
        return new promhx.Promise<>((def));
    }

    /**
     *
     * @param empty
     */
    public Dispatcher(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Dispatcher() {
        verb.exe.Dispatcher.__hx_ctor_verb_exe_Dispatcher(this);
    }

}
