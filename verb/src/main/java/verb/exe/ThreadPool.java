package verb.exe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class ThreadPool extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param numThreads
     */
    public static void __hx_ctor_verb_exe_ThreadPool(verb.exe.ThreadPool __hx_this, int numThreads) {
        __hx_this.nextID = 0;
        __hx_this.tasks = new haxe.root.Array<>();
    }

    /**
     *
     */
    public haxe.root.Array<java.lang.Object> tasks;

    /**
     *
     */
    public int nextID;

    /**
     *
     * @param empty
     */
    public ThreadPool(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param numThreads
     */
    public ThreadPool(int numThreads) {
        verb.exe.ThreadPool.__hx_ctor_verb_exe_ThreadPool(this, numThreads);
    }

    /**
     *
     * @param task
     * @param arg
     * @param onFinish
     */
    public void addTask(haxe.lang.Function task, java.lang.Object arg, haxe.lang.Function onFinish) {
        this.tasks.push(new haxe.lang.DynamicObject(new java.lang.String[]{"arg", "done", "onFinish", "task"}, new java.lang.Object[]{arg, false, onFinish, task}, new java.lang.String[]{"id"}, new double[]{((double) (((double) (this.nextID))))}));
        this.nextID++;
    }

    /**
     *
     */
    public void blockRunAllTasks() {
        {
            int _g = 0;
            haxe.root.Array<java.lang.Object> _g1 = this.tasks;
            while ((_g < _g1.length)) {
                java.lang.Object task = _g1.get(_g);
                ++_g;
                if ((((haxe.lang.Function) (haxe.lang.Runtime.getField(task, "onFinish", true))) != null)) {
                    java.lang.Object tmp = (haxe.lang.Runtime.callField(task, "task", new haxe.root.Array(new java.lang.Object[]{haxe.lang.Runtime.getField(task, "arg", true)})));
                    haxe.lang.Runtime.callField(task, "onFinish", new haxe.root.Array(new java.lang.Object[]{tmp}));
                }

            }

        }

        this.tasks = new haxe.root.Array<>();
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_048_796_338: {
                    if (field.equals("nextID")) {
                        __temp_executeDef1 = false;
                        this.nextID = ((int) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_048_796_338: {
                    if (field.equals("nextID")) {
                        __temp_executeDef1 = false;
                        this.nextID = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 110_132_110: {
                    if (field.equals("tasks")) {
                        __temp_executeDef1 = false;
                        this.tasks = ((haxe.root.Array<java.lang.Object>) (value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 538_823_499: {
                    if (field.equals("blockRunAllTasks")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "blockRunAllTasks"));
                    }

                    break;
                }

                case 110_132_110: {
                    if (field.equals("tasks")) {
                        __temp_executeDef1 = false;
                        return this.tasks;
                    }

                    break;
                }

                case -1_148_589_626: {
                    if (field.equals("addTask")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "addTask"));
                    }

                    break;
                }

                case -1_048_796_338: {
                    if (field.equals("nextID")) {
                        __temp_executeDef1 = false;
                        return this.nextID;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case -1_048_796_338: {
                    if (field.equals("nextID")) {
                        __temp_executeDef1 = false;
                        return (this.nextID);
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 538_823_499: {
                    if (field.equals("blockRunAllTasks")) {
                        __temp_executeDef1 = false;
                        this.blockRunAllTasks();
                    }

                    break;
                }

                case -1_148_589_626: {
                    if (field.equals("addTask")) {
                        __temp_executeDef1 = false;
                        this.addTask(((haxe.lang.Function) (dynargs.get(0))), dynargs.get(1), ((haxe.lang.Function) (dynargs.get(2))));
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("nextID");
        baseArr.push("tasks");
        super.__hx_getFields(baseArr);
    }

}
