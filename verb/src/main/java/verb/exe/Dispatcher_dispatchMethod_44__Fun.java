package verb.exe;

/**
 *
 * @author Matthew
 * @param <T>
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Dispatcher_dispatchMethod_44__Fun<T> extends haxe.lang.Function {

    /**
     *
     */
    public promhx.Deferred<T> def;

    /**
     *
     * @param def
     */
    public Dispatcher_dispatchMethod_44__Fun(promhx.Deferred<T> def) {
        super(1, 0);
        this.def = def;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        T x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((T) (((java.lang.Object) (__fn_float1))))) : (((T) (__fn_dyn1))));
        this.def.resolve(x);
        return null;
    }

}
