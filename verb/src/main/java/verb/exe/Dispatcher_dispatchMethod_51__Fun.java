package verb.exe;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Dispatcher_dispatchMethod_51__Fun extends haxe.lang.Function {

    /**
     *
     */
    public java.lang.String methodName;

    /**
     *
     */
    public java.lang.Class classType;

    /**
     *
     */
    public haxe.root.Array args;

    /**
     *
     * @param methodName
     * @param classType
     * @param args
     */
    public Dispatcher_dispatchMethod_51__Fun(java.lang.String methodName, java.lang.Class classType, haxe.root.Array args) {
        super(1, 0);
        this.methodName = methodName;
        this.classType = classType;
        this.args = args;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object _1 = ((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1);
        java.lang.Object r = haxe.root.Reflect.field(this.classType, this.methodName);
        java.lang.Object r1 = haxe.root.Reflect.callMethod(this.classType, r, this.args);
        return r1;
    }

}
