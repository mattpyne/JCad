package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class CurveLengthSample extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param u
     * @param len
     */
    public static void __hx_ctor_verb_eval_CurveLengthSample(verb.eval.CurveLengthSample __hx_this, double u, double len) {
        __hx_this.u = u;
        __hx_this.len = len;
    }

    /**
     *
     */
    public double u;

    /**
     *
     */
    public double len;

    /**
     *
     * @param empty
     */
    public CurveLengthSample(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param u
     * @param len
     */
    public CurveLengthSample(double u, double len) {
        verb.eval.CurveLengthSample.__hx_ctor_verb_eval_CurveLengthSample(this, u, len);
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 107_029: {
                    if (field.equals("len")) {
                        __temp_executeDef1 = false;
                        this.len = (value);
                        return value;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 107_029: {
                    if (field.equals("len")) {
                        __temp_executeDef1 = false;
                        this.len = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        this.u = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 107_029: {
                    if (field.equals("len")) {
                        __temp_executeDef1 = false;
                        return this.len;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 107_029: {
                    if (field.equals("len")) {
                        __temp_executeDef1 = false;
                        return this.len;
                    }

                    break;
                }

                case 117: {
                    if (field.equals("u")) {
                        __temp_executeDef1 = false;
                        return this.u;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("len");
        baseArr.push("u");
        super.__hx_getFields(baseArr);
    }

}
