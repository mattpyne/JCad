package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curveAndSurfaceWithEstimate_501__Fun extends haxe.lang.Function {

    /**
     *
     */
    public verb.core.NurbsSurfaceData surface;

    /**
     *
     */
    public verb.core.NurbsCurveData curve;

    /**
     *
     * @param surface
     * @param curve
     */
    public Intersect_curveAndSurfaceWithEstimate_501__Fun(verb.core.NurbsSurfaceData surface, verb.core.NurbsCurveData curve) {
        super(1, 0);
        this.surface = surface;
        this.curve = curve;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.root.Array<java.lang.Object> x1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.root.Array<java.lang.Object>) (((java.lang.Object) (__fn_float1))))) : (((haxe.root.Array<java.lang.Object>) (__fn_dyn1))));
        haxe.root.Array<haxe.root.Array<java.lang.Number>> dc = verb.eval.Eval.rationalCurveDerivatives(this.curve, haxe.lang.Runtime.toDouble(x1.get(0)), 1);
        haxe.root.Array<haxe.root.Array<haxe.root.Array<java.lang.Number>>> ds = verb.eval.Eval.rationalSurfaceDerivatives(this.surface, haxe.lang.Runtime.toDouble(x1.get(1)), haxe.lang.Runtime.toDouble(x1.get(2)), 1);
        haxe.root.Array<java.lang.Number> r = verb.core.Vec.sub(ds.get(0).get(0), dc.get(0));
        haxe.root.Array<java.lang.Number> drdt = verb.core.Vec.mul(-1.0, dc.get(1));
        haxe.root.Array<java.lang.Number> drdu = ds.get(1).get(0);
        haxe.root.Array<java.lang.Number> drdv = ds.get(0).get(1);
        return new haxe.root.Array<>(new java.lang.Number[]{(((2.0 * verb.core.Vec.dot(drdt, r)))), (((2.0 * verb.core.Vec.dot(drdu, r)))), (((2.0 * verb.core.Vec.dot(drdv, r))))});
    }

}
