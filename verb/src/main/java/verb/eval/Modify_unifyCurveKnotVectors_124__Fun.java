package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Modify_unifyCurveKnotVectors_124__Fun extends haxe.lang.Function {

    /**
     *
     */
    public double scale;

    /**
     *
     * @param scale
     */
    public Modify_unifyCurveKnotVectors_124__Fun(double scale) {
        super(1, 1);
        this.scale = scale;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, java.lang.Object __fn_dyn1) {
        double x4 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (haxe.lang.Runtime.toDouble(__fn_dyn1)));
        return (x4 * this.scale);
    }

}
