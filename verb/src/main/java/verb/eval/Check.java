package verb.eval;

import haxe.lang.EmptyObject;
import haxe.lang.HaxeException;
import haxe.lang.HxObject;
import haxe.lang.Runtime;
import haxe.root.Array;
import verb.core.ArrayExtensions;
import verb.core.Constants;
import verb.core.NurbsCurveData;
import verb.core.NurbsSurfaceData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Check extends HxObject {

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_eval_Check(Check __hx_this) {
    }

    /**
     *
     * @param vec
     * @param degree
     * @return
     */
    public static boolean isValidKnotVector(Array<Number> vec, int degree) {
        if (vec.length == 0) {
            
            return false;
        }

        if (vec.length < (degree + 1) * 2) {
            
            return false;
        }

        double rep = Runtime.toDouble(ArrayExtensions.first(vec));
        int _g1 = 0;
        int _g = degree + 1;
        while (_g1 < _g) {
            
            int i = _g1++;
            if (Math.abs(Runtime.toDouble(vec.get(i)) - rep) > Constants.EPSILON) {
                
                return false;
            }
        }

        rep = Runtime.toDouble(ArrayExtensions.last(vec));
        int _g11 = vec.length - degree - 1;
        int _g2 = vec.length;
        while (_g11 < _g2) {
            
            int i1 = _g11++;
            if (Math.abs(Runtime.toDouble(vec.get(i1)) - rep) > Constants.EPSILON) {
                
                return false;
            }
        }

        return isNonDecreasing(vec);
    }

    /**
     *
     * @param vec
     * @return
     */
    public static boolean isNonDecreasing(Array<Number> vec) {
        double rep = Runtime.toDouble(ArrayExtensions.first(vec));
        int _g1 = 0;
        int _g = vec.length;
        while (_g1 < _g) {
            
            int i = _g1++;
            if (Runtime.toDouble(vec.get(i)) < (rep - Constants.EPSILON)) {
                
                return false;
            }

            rep = Runtime.toDouble(vec.get(i));
        }

        return true;
    }

    /**
     *
     * @param data
     * @return
     */
    public static NurbsCurveData isValidNurbsCurveData(NurbsCurveData data) {
        if (data.controlPoints == null) {
            throw HaxeException.wrap("Control points array cannot be null!");
        }

        if (data.degree < 1) {
            throw HaxeException.wrap("Degree must be greater than 1!");
        }

        if (data.knots == null) {
            throw HaxeException.wrap("Knots cannot be null!");
        }

        if (data.knots.length != data.controlPoints.length + data.degree + 1) {
            throw HaxeException.wrap("controlPoints.length + degree + 1 must equal knots.length!");
        }

        if (!isValidKnotVector(data.knots, data.degree)) {
            throw HaxeException.wrap("Invalid knot vector format!  Should begin with degree + 1 repeats and end with degree + 1 repeats!");
        }

        return data;
    }

    /**
     *
     * @param data
     * @return
     */
    public static NurbsSurfaceData isValidNurbsSurfaceData(NurbsSurfaceData data) {
        if ((data.controlPoints == null)) {
            throw HaxeException.wrap("Control points array cannot be null!");
        }

        if ((data.degreeU < 1)) {
            throw HaxeException.wrap("DegreeU must be greater than 1!");
        }

        if ((data.degreeV < 1)) {
            throw HaxeException.wrap("DegreeV must be greater than 1!");
        }

        if ((data.knotsU == null)) {
            throw HaxeException.wrap("KnotsU cannot be null!");
        }

        if ((data.knotsV == null)) {
            throw HaxeException.wrap("KnotsV cannot be null!");
        }

        if ((data.knotsU.length != ((data.controlPoints.length + data.degreeU) + 1))) {
            throw HaxeException.wrap("controlPointsU.length + degreeU + 1 must equal knotsU.length!");
        }

        if (data.knotsV.length != data.controlPoints.get(0).length + data.degreeV + 1) {
            throw HaxeException.wrap("controlPointsV.length + degreeV + 1 must equal knotsV.length!");
        }

        if ((!(isValidKnotVector(data.knotsU, data.degreeU)) || !(isValidKnotVector(data.knotsV, data.degreeV)))) {
            throw HaxeException.wrap("Invalid knot vector format!  Should begin with degree + 1 repeats and end with degree + 1 repeats!");
        }

        return data;
    }

    /**
     *
     * @param empty
     */
    public Check(EmptyObject empty) {
    }

    /**
     *
     */
    public Check() {
        Check.__hx_ctor_verb_eval_Check(this);
    }

}
