// Generated by Haxe 3.4.4
package verb.eval;

import haxe.lang.Function;
import haxe.root.Array;
import verb.core.Constants;
import verb.core.Interval;
import verb.core.MeshIntersectionPoint;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_meshes_187__Fun extends Function {

    /**
     *
     */
    public final static verb.eval.Intersect_meshes_187__Fun __hx_current = new Intersect_meshes_187__Fun();

    /**
     *
     */
    public Intersect_meshes_187__Fun() {
        super(2, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public Object __hx_invoke2_o(double __fn_float1, Object __fn_dyn1, double __fn_float2, Object __fn_dyn2) {
        Interval<MeshIntersectionPoint> b = (Interval<MeshIntersectionPoint>) __fn_dyn2;
        Interval<MeshIntersectionPoint> a = (Interval<MeshIntersectionPoint>) __fn_dyn1;
        
        Array<Number> s1 = Vec.sub(a.min.uv0, b.min.uv0);
        double d1 = Vec.dot(s1, s1);
        
        Array<Number> s2 = Vec.sub(a.max.uv0, b.max.uv0);
        double d2 = Vec.dot(s2, s2);
        
        Array<Number> s3 = Vec.sub(a.min.uv0, b.max.uv0);
        double d3 = Vec.dot(s3, s3);
        
        Array<Number> s4 = Vec.sub(a.max.uv0, b.min.uv0);
        double d4 = Vec.dot(s4, s4);
        
        if (!(d1 < Constants.EPSILON && d2 < Constants.EPSILON)) {
            
            if (d3 < Constants.EPSILON) {
                
                return d4 < Constants.EPSILON;
            } else {
                
                return false;
            }
        } else {
            
            return true;
        }
    }

}
