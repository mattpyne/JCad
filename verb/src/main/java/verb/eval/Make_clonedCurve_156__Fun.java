package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Make_clonedCurve_156__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.eval.Make_clonedCurve_156__Fun __hx_current;

    /**
     *
     */
    public Make_clonedCurve_156__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.root.Array<java.lang.Object> x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.root.Array<java.lang.Object>) (((java.lang.Object) (__fn_float1))))) : (((haxe.root.Array<java.lang.Object>) (__fn_dyn1))));
        return (x.copy());
    }

}
