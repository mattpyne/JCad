package verb.eval;

import verb.core.NurbsCurveData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Divide_rationalCurveByArcLength_145__Fun extends haxe.lang.Function {

    /**
     *
     */
    public final static Divide_rationalCurveByArcLength_145__Fun __hx_current = new Divide_rationalCurveByArcLength_145__Fun();

    /**
     *
     */
    public Divide_rationalCurveByArcLength_145__Fun() {
        super(1, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, Object __fn_dyn1) {
        return Analyze.rationalBezierCurveArcLength((NurbsCurveData) __fn_dyn1, null, null);
    }

}
