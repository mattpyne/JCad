package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import verb.core.NurbsCurveData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Analyze_rationalCurveClosestParam_388__Fun extends Function {

    /**
     *
     */
    public NurbsCurveData curve;

    /**
     *
     * @param curve
     */
    public Analyze_rationalCurveClosestParam_388__Fun(NurbsCurveData curve) {
        super(1, 0);
        this.curve = curve;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        double u2 = (((__fn_dyn1 == Runtime.undefined)) ? (__fn_float1) : (Runtime.toDouble(__fn_dyn1)));
        return Eval.rationalCurveDerivatives(this.curve, u2, 2);
    }

}
