package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class KnotMultiplicity extends haxe.lang.HxObject {

    /**
     *
     * @param __hx_this
     * @param knot
     * @param mult
     */
    public static void __hx_ctor_verb_eval_KnotMultiplicity(verb.eval.KnotMultiplicity __hx_this, double knot, int mult) {
        __hx_this.knot = knot;
        __hx_this.mult = mult;
    }

    /**
     *
     */
    public double knot;

    /**
     *
     */
    public int mult;

    /**
     *
     * @param empty
     */
    public KnotMultiplicity(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     * @param knot
     * @param mult
     */
    public KnotMultiplicity(double knot, int mult) {
        verb.eval.KnotMultiplicity.__hx_ctor_verb_eval_KnotMultiplicity(this, knot, mult);
    }

    /**
     *
     */
    public void inc() {
        this.mult++;
    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_setField_f(java.lang.String field, double value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_363_120: {
                    if (field.equals("mult")) {
                        __temp_executeDef1 = false;
                        this.mult = ((int) (value));
                        return value;
                    }

                    break;
                }

                case 3_296_904: {
                    if (field.equals("knot")) {
                        __temp_executeDef1 = false;
                        this.knot = (value);
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField_f(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param value
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_363_120: {
                    if (field.equals("mult")) {
                        __temp_executeDef1 = false;
                        this.mult = (haxe.lang.Runtime.toInt(value));
                        return value;
                    }

                    break;
                }

                case 3_296_904: {
                    if (field.equals("knot")) {
                        __temp_executeDef1 = false;
                        this.knot = (haxe.lang.Runtime.toDouble(value));
                        return value;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_setField(field, value, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param isCheck
     * @param handleProperties
     * @return
     */
    @Override
    public java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 104_414: {
                    if (field.equals("inc")) {
                        __temp_executeDef1 = false;
                        return (new haxe.lang.Closure(this, "inc"));
                    }

                    break;
                }

                case 3_296_904: {
                    if (field.equals("knot")) {
                        __temp_executeDef1 = false;
                        return this.knot;
                    }

                    break;
                }

                case 3_363_120: {
                    if (field.equals("mult")) {
                        __temp_executeDef1 = false;
                        return this.mult;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param throwErrors
     * @param handleProperties
     * @return
     */
    @Override
    public double __hx_getField_f(java.lang.String field, boolean throwErrors, boolean handleProperties) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 3_363_120: {
                    if (field.equals("mult")) {
                        __temp_executeDef1 = false;
                        return (this.mult);
                    }

                    break;
                }

                case 3_296_904: {
                    if (field.equals("knot")) {
                        __temp_executeDef1 = false;
                        return this.knot;
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_getField_f(field, throwErrors, handleProperties);
            } else {
                throw null;
            }

        }

    }

    /**
     *
     * @param field
     * @param dynargs
     * @return
     */
    @Override
    public java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs) {
        {
            boolean __temp_executeDef1 = true;
            switch (field.hashCode()) {
                case 104_414: {
                    if (field.equals("inc")) {
                        __temp_executeDef1 = false;
                        this.inc();
                    }

                    break;
                }

            }

            if (__temp_executeDef1) {
                return super.__hx_invokeField(field, dynargs);
            }

        }

        return null;
    }

    /**
     *
     * @param baseArr
     */
    @Override
    public void __hx_getFields(haxe.root.Array<java.lang.String> baseArr) {
        baseArr.push("mult");
        baseArr.push("knot");
        super.__hx_getFields(baseArr);
    }

}
