package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class AdaptiveRefinementNode_getAllCorners_531__Fun extends haxe.lang.Function {

    /**
     *
     */
    public verb.eval.AdaptiveRefinementNode that;

    /**
     *
     */
    public double e;

    /**
     *
     * @param that
     * @param e
     */
    public AdaptiveRefinementNode_getAllCorners_531__Fun(verb.eval.AdaptiveRefinementNode that, double e) {
        super(1, 0);
        this.that = that;
        this.e = e;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        java.lang.Object c1 = ((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (__fn_dyn1);
        if ((haxe.lang.Runtime.toDouble(((haxe.root.Array<java.lang.Object>) (haxe.lang.Runtime.getField(c1, "uv", true))).get(1))) > ((haxe.lang.Runtime.toDouble(this.that.corners.get(0).uv.get(1))) + this.e)) {
            return (haxe.lang.Runtime.toDouble(((haxe.root.Array<java.lang.Object>) (haxe.lang.Runtime.getField(c1, "uv", true))).get(1))) < ((haxe.lang.Runtime.toDouble(this.that.corners.get(2).uv.get(1))) - this.e);
        } else {
            return false;
        }
    }

}
