package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import haxe.root.Array;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Make_rationalInterpCurve_722__Fun extends Function {

    /**
     *
     */
    public int i3;

    /**
     *
     * @param i3
     */
    public Make_rationalInterpCurve_722__Fun(int i3) {
        super(1, 1);
        this.i3 = i3;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, Object __fn_dyn1) {
        Array<Object> x = (__fn_dyn1 == Runtime.undefined)
                ? (Array<Object>) ((Object) __fn_float1)
                : (Array<Object>) __fn_dyn1;
        
        return Runtime.toDouble(x.get(this.i3));
    }

}
