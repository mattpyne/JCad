package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Modify_unifyCurveKnotVectors_100__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.eval.Modify_unifyCurveKnotVectors_100__Fun __hx_current;

    /**
     *
     */
    public Modify_unifyCurveKnotVectors_100__Fun() {
        super(2, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public double __hx_invoke2_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        int a = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (((int) (__fn_float2))) : (haxe.lang.Runtime.toInt(__fn_dyn2)));
        verb.core.NurbsCurveData x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((verb.core.NurbsCurveData) (((java.lang.Object) (__fn_float1))))) : (((verb.core.NurbsCurveData) (__fn_dyn1))));
        return (verb.eval.Modify.imax(x.degree, a));
    }

}
