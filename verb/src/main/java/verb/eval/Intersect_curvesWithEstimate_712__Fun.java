package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import haxe.root.Array;
import verb.core.NurbsCurveData;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curvesWithEstimate_712__Fun extends Function {

    /**
     *
     */
    public NurbsCurveData curve1;

    /**
     *
     */
    public NurbsCurveData curve0;

    /**
     *
     * @param curve1
     * @param curve0
     */
    public Intersect_curvesWithEstimate_712__Fun(NurbsCurveData curve1, NurbsCurveData curve0) {
        super(1, 0);
        this.curve1 = curve1;
        this.curve0 = curve0;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        Array<Object> x1 = (__fn_dyn1 == Runtime.undefined)
                ? (Array<Object>) ((Object) __fn_float1)
                : (Array<Object>) __fn_dyn1;
        
        Array<Array<Number>> dc0 = Eval.rationalCurveDerivatives(this.curve0, Runtime.toDouble(x1.get(0)), 1);
        Array<Array<Number>> dc1 = Eval.rationalCurveDerivatives(this.curve1, Runtime.toDouble(x1.get(1)), 1);
        Array<Number> r = Vec.sub(dc0.get(0), dc1.get(0));
        Array<Number> drdu = dc0.get(1);
        Array<Number> drdt = Vec.mul(-1.0, dc1.get(1));
        
        return new Array(new Number[]{2.0 * Vec.dot(drdu, r), 2.0 * Vec.dot(drdt, r)});
    }

}
