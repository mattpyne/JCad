package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import verb.core.NurbsCurveData;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Make_loftedSurface_142__Fun extends Function {

    /**
     *
     */
    public int i;

    /**
     *
     * @param i
     */
    public Make_loftedSurface_142__Fun(int i) {
        super(1, 0);
        this.i = i;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        NurbsCurveData x = __fn_dyn1 == Runtime.undefined ? (NurbsCurveData) (Object) __fn_float1 : (NurbsCurveData) __fn_dyn1;
        return x.controlPoints.get(this.i);
    }

}
