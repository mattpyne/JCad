package verb.eval;

import verb.core.SurfaceSurfaceIntersectionPoint;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_surfaces_67__Fun extends haxe.lang.Function {

    /**
     *
     */
    public final static Intersect_surfaces_67__Fun __hx_current = new Intersect_surfaces_67__Fun();

    /**
     *
     */
    public Intersect_surfaces_67__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        SurfaceSurfaceIntersectionPoint y = (SurfaceSurfaceIntersectionPoint) __fn_dyn1;
        return y.point;
    }

}
