package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Modify_unifyCurveKnotVectors_119__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.eval.Modify_unifyCurveKnotVectors_119__Fun __hx_current;

    /**
     *
     */
    public Modify_unifyCurveKnotVectors_119__Fun() {
        super(2, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public double __hx_invoke2_f(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        double a1 = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (__fn_float2) : (haxe.lang.Runtime.toDouble(__fn_dyn2)));
        double x3 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (haxe.lang.Runtime.toDouble(__fn_dyn1)));
        return java.lang.Math.max(x3, a1);
    }

}
