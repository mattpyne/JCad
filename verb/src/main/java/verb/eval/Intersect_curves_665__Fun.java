package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curves_665__Fun extends haxe.lang.Function {

    /**
     *
     */
    public double tolerance;

    /**
     *
     * @param tolerance
     */
    public Intersect_curves_665__Fun(double tolerance) {
        super(2, 0);
        this.tolerance = tolerance;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke2_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        verb.core.CurveCurveIntersection b = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (((verb.core.CurveCurveIntersection) (((java.lang.Object) (__fn_float2))))) : (((verb.core.CurveCurveIntersection) (__fn_dyn2))));
        verb.core.CurveCurveIntersection a = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((verb.core.CurveCurveIntersection) (((java.lang.Object) (__fn_float1))))) : (((verb.core.CurveCurveIntersection) (__fn_dyn1))));
        return (java.lang.Math.abs((a.u0 - b.u0)) < (this.tolerance * 5));
    }

}
