package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_lookupAdjacentSegment_399__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.eval.Intersect_lookupAdjacentSegment_399__Fun __hx_current;

    /**
     *
     */
    public Intersect_lookupAdjacentSegment_399__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        verb.core.Pair<verb.core.KdPoint<verb.core.MeshIntersectionPoint>, java.lang.Object> r1
                = (((__fn_dyn1 == haxe.lang.Runtime.undefined))
                        ? (((verb.core.Pair<verb.core.KdPoint<verb.core.MeshIntersectionPoint>, java.lang.Object>) (((java.lang.Object) (__fn_float1)))))
                        : (((verb.core.Pair<verb.core.KdPoint<verb.core.MeshIntersectionPoint>, java.lang.Object>) (__fn_dyn1))));
        return r1.item0.obj;
    }

}
