package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curveAndSurface_451__Fun extends haxe.lang.Function {

    /**
     *
     */
    public double __temp_tol15;

    /**
     *
     * @param __temp_tol15
     */
    public Intersect_curveAndSurface_451__Fun(double __temp_tol15) {
        super(2, 0);
        this.__temp_tol15 = __temp_tol15;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @param __fn_float2
     * @param __fn_dyn2
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke2_o(double __fn_float1, java.lang.Object __fn_dyn1, double __fn_float2, java.lang.Object __fn_dyn2) {
        verb.core.CurveSurfaceIntersection b = (((__fn_dyn2 == haxe.lang.Runtime.undefined)) ? (((verb.core.CurveSurfaceIntersection) (((java.lang.Object) (__fn_float2))))) : (((verb.core.CurveSurfaceIntersection) (__fn_dyn2))));
        verb.core.CurveSurfaceIntersection a = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((verb.core.CurveSurfaceIntersection) (((java.lang.Object) (__fn_float1))))) : (((verb.core.CurveSurfaceIntersection) (__fn_dyn1))));
        return (java.lang.Math.abs((a.u - b.u)) < (0.5 * this.__temp_tol15));
    }

}
