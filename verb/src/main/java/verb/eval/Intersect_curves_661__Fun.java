package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import haxe.root.Array;
import verb.core.ArrayExtensions;
import verb.core.NurbsCurveData;
import verb.core.Pair;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curves_661__Fun extends Function {

    /**
     *
     */
    public double tolerance;

    /**
     *
     */
    public NurbsCurveData curve2;

    /**
     *
     */
    public NurbsCurveData curve1;

    /**
     *
     * @param tolerance
     * @param curve2
     * @param curve1
     */
    public Intersect_curves_661__Fun(double tolerance, NurbsCurveData curve2, NurbsCurveData curve1) {
        super(1, 0);
        this.tolerance = tolerance;
        this.curve2 = curve2;
        this.curve1 = curve1;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        Pair<NurbsCurveData, NurbsCurveData> x = (((__fn_dyn1 == Runtime.undefined)) 
                ? (((Pair<NurbsCurveData, NurbsCurveData>) (((Object) (__fn_float1))))) 
                : (((Pair<NurbsCurveData, NurbsCurveData>) (__fn_dyn1))));
        double tmp = Runtime.toDouble(ArrayExtensions.first(x.item0.knots));
        double tmp1 = Runtime.toDouble(ArrayExtensions.first(x.item1.knots));
        return Intersect.curvesWithEstimate(this.curve1, this.curve2, tmp, tmp1, this.tolerance);
    }

}
