package verb.eval;

import verb.core.CurveSurfaceIntersection;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curveAndSurface_449__Fun extends haxe.lang.Function {

    /**
     *
     */
    public double tol;

    /**
     *
     * @param __temp_tol15
     */
    public Intersect_curveAndSurface_449__Fun(double __temp_tol15) {
        super(1, 0);
        this.tol = __temp_tol15;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        CurveSurfaceIntersection x = (__fn_dyn1 == haxe.lang.Runtime.undefined)
                ? (CurveSurfaceIntersection) ((Object) __fn_float1)
                : (CurveSurfaceIntersection) __fn_dyn1;
        
        return Vec.distSquared(x.curvePoint, x.surfacePoint) < (this.tol * this.tol);
    }

}
