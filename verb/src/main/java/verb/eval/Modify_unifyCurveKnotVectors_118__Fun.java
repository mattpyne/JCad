package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Modify_unifyCurveKnotVectors_118__Fun extends haxe.lang.Function {

    /**
     *
     */
    public static verb.eval.Modify_unifyCurveKnotVectors_118__Fun __hx_current;

    /**
     *
     */
    public Modify_unifyCurveKnotVectors_118__Fun() {
        super(1, 1);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, java.lang.Object __fn_dyn1) {
        verb.core.Interval<java.lang.Object> x2 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((verb.core.Interval<java.lang.Object>) (((java.lang.Object) (__fn_float1))))) : (((verb.core.Interval<java.lang.Object>) (__fn_dyn1))));
        return ((haxe.lang.Runtime.toDouble(x2.max)) - (haxe.lang.Runtime.toDouble(x2.min)));
    }

}
