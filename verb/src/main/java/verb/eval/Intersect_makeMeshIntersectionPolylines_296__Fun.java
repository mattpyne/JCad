package verb.eval;

import verb.core.MeshIntersectionPoint;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_makeMeshIntersectionPolylines_296__Fun extends haxe.lang.Function {

    /**
     *
     */
    public final static verb.eval.Intersect_makeMeshIntersectionPolylines_296__Fun __hx_current = new Intersect_makeMeshIntersectionPolylines_296__Fun();

    /**
     *
     */
    public Intersect_makeMeshIntersectionPolylines_296__Fun() {
        super(1, 0);
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        MeshIntersectionPoint x = (MeshIntersectionPoint) __fn_dyn1;
        return (x.adj == null);
    }

}
