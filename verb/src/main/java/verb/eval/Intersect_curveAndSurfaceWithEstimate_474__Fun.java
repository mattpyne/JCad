package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curveAndSurfaceWithEstimate_474__Fun extends haxe.lang.Function {

    /**
     *
     */
    public verb.core.NurbsSurfaceData surface;

    /**
     *
     */
    public verb.core.NurbsCurveData curve;

    /**
     *
     * @param surface
     * @param curve
     */
    public Intersect_curveAndSurfaceWithEstimate_474__Fun(verb.core.NurbsSurfaceData surface, verb.core.NurbsCurveData curve) {
        super(1, 1);
        this.surface = surface;
        this.curve = curve;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, java.lang.Object __fn_dyn1) {
        haxe.root.Array<java.lang.Object> x = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (((haxe.root.Array<java.lang.Object>) (((java.lang.Object) (__fn_float1))))) : (((haxe.root.Array<java.lang.Object>) (__fn_dyn1))));
        haxe.root.Array<java.lang.Number> p1 = verb.eval.Eval.rationalCurvePoint(this.curve, haxe.lang.Runtime.toDouble(x.get(0)));
        haxe.root.Array<java.lang.Number> p2 = verb.eval.Eval.rationalSurfacePoint(this.surface, haxe.lang.Runtime.toDouble(x.get(1)), haxe.lang.Runtime.toDouble(x.get(2)));
        haxe.root.Array<java.lang.Number> p1_p2 = verb.core.Vec.sub(p1, p2);
        return verb.core.Vec.dot(p1_p2, p1_p2);
    }

}
