package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import haxe.root.Array;
import verb.core.NurbsCurveData;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curvesWithEstimate_691__Fun extends Function {

    /**
     *
     */
    public NurbsCurveData curve1;

    /**
     *
     */
    public NurbsCurveData curve0;

    /**
     *
     * @param curve1
     * @param curve0
     */
    public Intersect_curvesWithEstimate_691__Fun(NurbsCurveData curve1, NurbsCurveData curve0) {
        super(1, 1);
        this.curve1 = curve1;
        this.curve0 = curve0;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, Object __fn_dyn1) {
        Array<Object> x = (((__fn_dyn1 == Runtime.undefined)) 
                ? (((Array<Object>) (((Object) (__fn_float1)))))
                : (((Array<Object>) (__fn_dyn1))));
        
        Array<Number> p1 = Eval.rationalCurvePoint(this.curve0, Runtime.toDouble(x.get(0)));
        Array<Number> p2 = Eval.rationalCurvePoint(this.curve1, Runtime.toDouble(x.get(1)));
        
        Array<Number> p1_p2 = Vec.sub(p1, p2);
        
        return Vec.dot(p1_p2, p1_p2);
    }

}
