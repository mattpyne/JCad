package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Modify_unifyCurveKnotVectors_114__Fun extends haxe.lang.Function {

    /**
     *
     */
    public double min;

    /**
     *
     * @param min
     */
    public Modify_unifyCurveKnotVectors_114__Fun(double min) {
        super(1, 1);
        this.min = min;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public double __hx_invoke1_f(double __fn_float1, java.lang.Object __fn_dyn1) {
        double x1 = (((__fn_dyn1 == haxe.lang.Runtime.undefined)) ? (__fn_float1) : (haxe.lang.Runtime.toDouble(__fn_dyn1)));
        return (x1 - this.min);
    }

}
