package verb.eval;

import haxe.lang.DynamicObject;
import haxe.lang.EmptyObject;
import haxe.lang.Function;
import haxe.lang.HaxeException;
import haxe.lang.HxObject;
import haxe.lang.Runtime;
import haxe.root.Array;
import verb.core.ArrayExtensions;
import verb.core.BoundingBox;
import verb.core.Constants;
import verb.core.CurveCurveIntersection;
import verb.core.CurveSurfaceIntersection;
import verb.core.CurveTriPoint;
import verb.core.Interval;
import verb.core.KdPoint;
import verb.core.KdTree;
import verb.core.LazyCurveBoundingBoxTree;
import verb.core.LazyMeshBoundingBoxTree;
import verb.core.LazyPolylineBoundingBoxTree;
import verb.core.LazySurfaceBoundingBoxTree;
import verb.core.Mesh;
import verb.core.MeshBoundingBoxTree;
import verb.core.MeshData;
import verb.core.MeshIntersectionPoint;
import verb.core.MinimizationResult;
import verb.core.Minimizer;
import verb.core.NurbsCurveData;
import verb.core.NurbsSurfaceData;
import verb.core.Pair;
import verb.core.PolylineData;
import verb.core.PolylineMeshIntersection;
import verb.core.Ray;
import verb.core.SurfaceSurfaceIntersectionPoint;
import verb.core.TriSegmentIntersection;
import verb.core.Vec;

/**
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect extends HxObject {

    /**
     * @param __hx_this
     */
    public static void __hx_ctor_verb_eval_Intersect(Intersect __hx_this) {
    }

    /**
     * @param surface0
     * @param surface1
     * @param tol
     * @return
     */
    public static Array<NurbsCurveData> surfaces(NurbsSurfaceData surface0, NurbsSurfaceData surface1, double tol) {
        MeshData tess0 = Tess.rationalSurfaceAdaptive(surface0, null);
        MeshData tess1 = Tess.rationalSurfaceAdaptive(surface1, null);

        return surfaces(tess0, tess1, surface0, surface1, tol);
    }
    
    /**
     * @param tess0
     * @param surface0
     * @param tess1
     * @param surface1
     * @param tol
     * @return
     */
    public static Array<NurbsCurveData> surfaces(
            MeshData tess0, MeshData tess1, 
            NurbsSurfaceData surface0, NurbsSurfaceData surface1, 
            double tol) {

        Array<Array<MeshIntersectionPoint>> resApprox = Intersect.meshes(tess0, tess1, null, null);
        Array<Array<SurfaceSurfaceIntersectionPoint>> exactPls = resApprox.map(new Intersect_surfaces_59__Fun(tol, surface1, surface0));

        int maxSurfDim = Math.max(surface0.degreeU, surface0.degreeV);
        maxSurfDim = Math.max(maxSurfDim, surface1.degreeV);
        maxSurfDim = Math.max(maxSurfDim, surface1.degreeU);

        return exactPls.map(new Intersect_surfaces_66__Fun(maxSurfDim));
    }

    /**
     * Refine a pair of surface points to a point where the two surfaces intersect
     *
     * @param surface0 NurbsSurfaceData for the first surface
     * @param surface1 NurbsSurfaceData for the second
     * @param uv1 the UV for the point on the first surface
     * @param uv2 the UV for the point on the second surface
     * @param tol  a tolerance value to terminate the refinement procedure
     * @return a SurfaceSurfaceIntersectionPoint object
     */
    public static SurfaceSurfaceIntersectionPoint surfacesAtPointWithEstimate(
            NurbsSurfaceData surface0,
            NurbsSurfaceData surface1,
            Array<Number> uv1,
            Array<Number> uv2,
            double tol) {

        Array<Array<Array<Number>>> pds;
        Array<Number> p = null;
        Array<Number> pn;
        Array<Number> pu;
        Array<Number> pv;
        double pd;
        Array<Array<Array<Number>>> qds;
        Array<Number> q;
        Array<Number> qn;
        Array<Number> qu;
        Array<Number> qv;
        double qd;
        double dist = 0.0;
        int maxits = 10000;
        int its = 0;
        double tolsq = tol * tol;
        while (true) {
            pds = Eval.rationalSurfaceDerivatives(surface0, Runtime.toDouble(uv1.get(0)), Runtime.toDouble(uv1.get(1)), 1);
            p = pds.get(0).get(0);

            qds = Eval.rationalSurfaceDerivatives(surface1, Runtime.toDouble(uv2.get(0)), Runtime.toDouble(uv2.get(1)), 1);
            q = qds.get(0).get(0);

            dist = Vec.distSquared(p, q);
            if (dist < tolsq) {
                break;
            }

            pu = pds.get(1).get(0);
            pv = pds.get(0).get(1);
            pn = Vec.normalized(Vec.cross(pu, pv));
            pd = Vec.dot(pn, p);

            qu = qds.get(1).get(0);
            qv = qds.get(0).get(1);
            qn = Vec.normalized(Vec.cross(qu, qv));
            qd = Vec.dot(qn, q);

            // 2) construct plane perp to both that passes through p (fn)
            Array<Number> fn = Vec.normalized(Vec.cross(pn, qn));
            double fd = Vec.dot(fn, p);

            // 3) x = intersection of all 3 planes
            Array<Number> x = Intersect.threePlanes(pn, pd, qn, qd, fn, fd);
            if (x == null) {
                throw HaxeException.wrap("panic!");
            }

            // 4) represent the difference vectors (pd = x - p, qd = x - q) in the partial
            // 		derivative vectors of the respective surfaces (pu, pv, qu, qv)
            Array<Number> pdif = Vec.sub(x, p);
            Array<Number> qdif = Vec.sub(x, q);

            Array<Number> rw = Vec.cross(pu, pn);
            Array<Number> rt = Vec.cross(pv, pn);

            Array<Number> su = Vec.cross(qu, qn);
            Array<Number> sv = Vec.cross(qv, qn);

            double dw = Vec.dot(rt, pdif) / Vec.dot(rt, pu);
            double dt = Vec.dot(rw, pdif) / Vec.dot(rw, pv);

            double du = Vec.dot(sv, qdif) / Vec.dot(sv, qu);
            double dv = Vec.dot(su, qdif) / Vec.dot(su, qv);

            uv1 = Vec.add(new Array(new Number[]{dw, dt}), uv1);
            uv2 = Vec.add(new Array(new Number[]{du, dv}), uv2);

            ++its;
            if (its > maxits) {
                break;
            }
        }

        return new SurfaceSurfaceIntersectionPoint(uv1, uv2, p, dist);
    }

    /**
     * @param mesh0
     * @param mesh1
     * @param bbtree0
     * @param bbtree1
     * @return
     */
    public static Array<Array<MeshIntersectionPoint>> meshes(
            MeshData mesh0,
            MeshData mesh1,
            IBoundingBoxTree<Number> bbtree0,
            IBoundingBoxTree<Number> bbtree1) {

        if (bbtree0 == null) {

            bbtree0 = new LazyMeshBoundingBoxTree(mesh0, null);
        }

        if (bbtree1 == null) {

            bbtree1 = new LazyMeshBoundingBoxTree(mesh1, null);
        }

        Array<Pair<Number, Number>> bbints = Intersect.boundingBoxTrees(bbtree0, bbtree1, Constants.EPSILON);

        Array<Interval<MeshIntersectionPoint>> segments
                = ArrayExtensions.unique(
                bbints.<Interval<MeshIntersectionPoint>>map(new Intersect_meshes_181__Fun(mesh1, mesh0))
                        .filter(Intersect_meshes_183__Fun.__hx_current)
                        .filter(Intersect_meshes_185__Fun.__hx_current),
                Intersect_meshes_187__Fun.__hx_current);

        return Intersect.makeMeshIntersectionPolylines(segments);
    }

    /**
     * @param mesh
     * @param min
     * @param max
     * @param step
     * @return
     */
    public static Array<Array<Array<MeshIntersectionPoint>>> meshSlices(MeshData mesh, double min, double max, double step) {
        MeshBoundingBoxTree bbtree = new MeshBoundingBoxTree(mesh, null);
        BoundingBox bb = bbtree.boundingBox();
        double x0 = (Runtime.toDouble(bb.min.get(0)));
        double y0 = (Runtime.toDouble(bb.min.get(1)));
        double x1 = (Runtime.toDouble(bb.max.get(0)));
        double y1 = (Runtime.toDouble(bb.max.get(1)));
        Array<Number> span = Vec.span(min, max, step);
        Array<Array<Array<MeshIntersectionPoint>>> slices = new Array<>(new Array[]{});
        int _g = 0;
        while ((_g < span.length)) {
            double z = (Runtime.toDouble(span.get(_g)));
            ++_g;
            Array<Array<Number>> pts = new Array<>(new Array[]{new Array<>(new Object[]{((Object) (x0)), ((Object) (y0)), ((Object) (z))}), new Array<>(new Object[]{((Object) (x1)), ((Object) (y0)), ((Object) (z))}), new Array<>(new Object[]{((Object) (x1)), ((Object) (y1)), ((Object) (z))}), new Array<>(new Object[]{((Object) (x0)), ((Object) (y1)), ((Object) (z))})});
            Array<Array<Number>> uvs = new Array<>(new Array[]{new Array<>(new Object[]{((Object) (0.0)), ((Object) (0.0))}), new Array<>(new Object[]{((Object) (1.0)), ((Object) (0.0))}), new Array<>(new Object[]{((Object) (1.0)), ((Object) (1.0))}), new Array<>(new Object[]{((Object) (0.0)), ((Object) (1.0))})});
            Array<Array<Number>> faces = new Array<>(new Array[]{new Array<>(new Object[]{0, 1, 2}), new Array<>(new Object[]{0, 2, 3})});
            MeshData plane = new MeshData(faces, pts, null, uvs);
            slices.push(Intersect.meshes(mesh, plane, bbtree, null));
        }

        return slices;
    }

    /**
     * @param segments
     * @return
     */
    public static Array<Array<MeshIntersectionPoint>> makeMeshIntersectionPolylines(Array<Interval<MeshIntersectionPoint>> segments) {
        if (segments.length == 0) {
            return new Array(new Array[]{});
        }

        int _g = 0;
        while (_g < segments.length) {
            Interval<MeshIntersectionPoint> s = segments.get(_g);
            ++_g;
            s.max.opp = s.min;
            s.min.opp = s.max;
        }

        KdTree<MeshIntersectionPoint> tree = Intersect.kdTreeFromSegments(segments);
        Array<MeshIntersectionPoint> ends = new Array(new MeshIntersectionPoint[]{});
        int _g1 = 0;
        while (_g1 < segments.length) {
            Interval<MeshIntersectionPoint> seg = segments.get(_g1);
            ++_g1;
            ends.push(seg.min);
            ends.push(seg.max);
        }

        int _g2 = 0;
        while (_g2 < ends.length) {
            MeshIntersectionPoint segEnd = ends.get(_g2);
            ++_g2;
            if (segEnd.adj != null) {
                continue;
            }

            MeshIntersectionPoint adjEnd = Intersect.lookupAdjacentSegment(segEnd, tree, segments.length);
            if ((adjEnd != null) && (adjEnd.adj == null)) {
                segEnd.adj = adjEnd;
                adjEnd.adj = segEnd;
            }
        }

        Array<MeshIntersectionPoint> freeEnds = ends.filter(Intersect_makeMeshIntersectionPolylines_296__Fun.__hx_current);
        if (freeEnds.length == 0) {
            freeEnds = ends;
        }

        Array<Array<MeshIntersectionPoint>> pls = new Array<>(new Array[]{});
        int numVisitedEnds = 0;
        boolean loopDetected = false;
        while (freeEnds.length != 0) {

            MeshIntersectionPoint end = (MeshIntersectionPoint) freeEnds.pop();
            if (!end.visited) {

                Array<MeshIntersectionPoint> pl = new Array<>(new MeshIntersectionPoint[]{});
                MeshIntersectionPoint curEnd = end;
                while (curEnd != null) {
                    if (curEnd.visited) {
                        break;
                    }

                    curEnd.visited = true;
                    curEnd.opp.visited = true;
                    pl.push(curEnd);
                    numVisitedEnds += 2;
                    curEnd = curEnd.opp.adj;
                    if (curEnd == end) {
                        break;
                    }
                }

                if (pl.length > 0) {
                    pl.push(pl.get((pl.length - 1)).opp);
                    pls.push(pl);
                }
            }

            if (freeEnds.length == 0 && ends.length > 0 && (loopDetected || numVisitedEnds < ends.length)) {

                loopDetected = true;
                MeshIntersectionPoint e = (MeshIntersectionPoint) ends.pop();
                freeEnds.push(e);
            }
        }

        return pls;
    }

    /**
     * @param segments
     * @return
     */
    public static KdTree<MeshIntersectionPoint> kdTreeFromSegments(Array<Interval<MeshIntersectionPoint>> segments) {
        Array<KdPoint<MeshIntersectionPoint>> treePoints = new Array<>(new KdPoint[]{});

        int _g = 0;
        while (_g < segments.length) {
            Interval<MeshIntersectionPoint> seg = segments.get(_g);
            ++_g;
            treePoints.push(new KdPoint<>(seg.min.point, seg.min));
            treePoints.push(new KdPoint<>(seg.max.point, seg.max));
        }

        return new KdTree(treePoints, new Function(-1, -1) {
            @Override
            public double __hx_invoke2_f(double __fn_float1, Object __fn_dyn1, double __fn_float2, Object __fn_dyn2) {
                return Vec.distSquared((Array<Number>) __fn_dyn1, (Array<Number>) __fn_dyn2);
            }
        });
    }

    /**
     * @param segEnd
     * @param tree
     * @param numResults
     * @return
     */
    public static MeshIntersectionPoint lookupAdjacentSegment(MeshIntersectionPoint segEnd, KdTree<MeshIntersectionPoint> tree, int numResults) {
        Array<MeshIntersectionPoint> adj
                = (((tree.nearest(segEnd.point, numResults, Constants.EPSILON)
                .filter(new Intersect_lookupAdjacentSegment_396__Fun(segEnd))
                .map(((Intersect_lookupAdjacentSegment_399__Fun.__hx_current != null))
                        ? (Intersect_lookupAdjacentSegment_399__Fun.__hx_current)
                        : (Intersect_lookupAdjacentSegment_399__Fun.__hx_current = (new Intersect_lookupAdjacentSegment_399__Fun()))))));

        if ((adj.length == 1)) {
            return adj.get(0);
        } else {
            return null;
        }

    }

    /**
     * @param curve
     * @param surface
     * @param tol
     * @param crvBbTree
     * @param srfBbTree
     * @return
     */
    public static Array<CurveSurfaceIntersection> curveAndSurface(
            NurbsCurveData curve,
            NurbsSurfaceData surface,
            Object tol,
            IBoundingBoxTree<NurbsCurveData> crvBbTree,
            IBoundingBoxTree<NurbsSurfaceData> srfBbTree) {

        double told = Runtime.eq(tol, null) ? 1e-3 : Runtime.toDouble(tol);

        if (crvBbTree == null) {

            crvBbTree = new LazyCurveBoundingBoxTree(curve, null);
        }

        if (srfBbTree == null) {

            srfBbTree = new LazySurfaceBoundingBoxTree(surface, null, null, null);
        }

        Array<Pair<NurbsCurveData, NurbsSurfaceData>> ints = Intersect.boundingBoxTrees(crvBbTree, srfBbTree, told);

        return ArrayExtensions.unique((Array) ints.map(new Intersect_curveAndSurface_428__Fun(told))
                        .filter(new Intersect_curveAndSurface_449__Fun(told)),
                new Intersect_curveAndSurface_451__Fun(told));
    }

    /**
     * @param curve
     * @param surface
     * @param start_params
     * @param tol
     * @return
     */
    public static CurveSurfaceIntersection curveAndSurfaceWithEstimate(NurbsCurveData curve, NurbsSurfaceData surface, Array<Number> start_params, Object tol) {
        double __temp_tol16 = ((Runtime.eq(tol, null)) ? (1e-3) : (Runtime.toDouble(tol)));
        Function objective = new Intersect_curveAndSurfaceWithEstimate_474__Fun(surface, curve);
        Function grad = new Intersect_curveAndSurfaceWithEstimate_501__Fun(surface, curve);
        MinimizationResult sol_obj = Minimizer.uncmin(objective, start_params, (__temp_tol16 * __temp_tol16), grad, null);
        Array<Number> _final = sol_obj.solution;
        return new CurveSurfaceIntersection((Runtime.toDouble(_final.get(0))), new Array<>(new Number[]{((((Runtime.toDouble(_final.get(1)))))), ((((Runtime.toDouble(_final.get(2))))))}), Eval.rationalCurvePoint(curve, (Runtime.toDouble(_final.get(0)))), Eval.rationalSurfacePoint(surface, (Runtime.toDouble(_final.get(1))), (Runtime.toDouble(_final.get(2)))));
    }

    /**
     * @param polyline
     * @param mesh
     * @param tol
     * @return
     */
    public static Array<PolylineMeshIntersection> polylineAndMesh(PolylineData polyline, MeshData mesh, double tol) {
        Array<Pair<Object, Object>> res = Intersect.boundingBoxTrees(
                new LazyPolylineBoundingBoxTree(polyline, null),
                (IBoundingBoxTree<Object>) ((IBoundingBoxTree) new LazyMeshBoundingBoxTree(mesh, null)),
                tol);

        Array<PolylineMeshIntersection> finalResults = new Array<>(new PolylineMeshIntersection[]{});

        int _g = 0;
        while (_g < res.length) {

            Pair<Object, Object> event = res.get(_g);
            ++_g;
            int polid = (Runtime.toInt(event.item0));
            int faceid = (Runtime.toInt(event.item1));

            TriSegmentIntersection inter = Intersect.segmentWithTriangle(
                    polyline.points.get(polid),
                    polyline.points.get(polid + 1),
                    mesh.points,
                    mesh.faces.get(faceid));

            if (inter == null) {

                continue;
            }

            Array<Number> pt = inter.point;
            double u = Runtime.toDouble(Vec.lerp(
                    inter.p,
                    new Array<>(new Number[]{Runtime.toDouble(polyline.params.get(polid))}),
                    new Array<>(new Number[]{Runtime.toDouble(polyline.params.get(polid + 1))})).get(0));

            Array<Number> uv = Mesh.triangleUVFromPoint(mesh, faceid, pt);
            finalResults.push(new PolylineMeshIntersection(pt, u, uv, polid, faceid));
        }

        return finalResults;
    }

    /**
     * @param <T1>
     * @param <T2>
     * @param ai
     * @param bi
     * @param tol
     * @return
     */
    public static <T1, T2> Array<Pair<T1, T2>> boundingBoxTrees(IBoundingBoxTree<T1> ai, IBoundingBoxTree<T2> bi, Object tol) {
        double __temp_tol17 = Runtime.eq(tol, null) ? 1e-9 : Runtime.toDouble(tol);

        Array<IBoundingBoxTree<T1>> atrees = new Array(new IBoundingBoxTree[]{});
        Array<IBoundingBoxTree<T2>> btrees = new Array(new IBoundingBoxTree[]{});
        atrees.push(ai);
        btrees.push(bi);

        Array<Pair<T1, T2>> results = new Array(new Pair[]{});
        while (atrees.length > 0) {
            IBoundingBoxTree<T1> a = (IBoundingBoxTree<T1>) atrees.pop();
            IBoundingBoxTree<T2> b = (IBoundingBoxTree<T2>) btrees.pop();
            if (a.empty() || b.empty()) {

                continue;
            }

            if (!a.boundingBox().intersects(b.boundingBox(), __temp_tol17)) {

                continue;
            }

            boolean ai1 = a.indivisible(__temp_tol17);
            boolean bi1 = b.indivisible(__temp_tol17);
            if (ai1 && bi1) {

                results.push(new Pair(a.yield(), b.yield()));
                continue;
            } else {

                if (ai1 && !bi1) {

                    Pair<IBoundingBoxTree<T2>, IBoundingBoxTree<T2>> bs = b.split();
                    atrees.push(a);
                    btrees.push(bs.item1);

                    atrees.push(a);
                    btrees.push(bs.item0);

                    continue;
                } else if (!ai1 && bi1) {

                    Pair<IBoundingBoxTree<T1>, IBoundingBoxTree<T1>> as = a.split();
                    atrees.push(as.item1);
                    btrees.push(b);

                    atrees.push(as.item0);
                    btrees.push(b);

                    continue;
                }
            }

            Pair<IBoundingBoxTree<T1>, IBoundingBoxTree<T1>> as = a.split();
            Pair<IBoundingBoxTree<T2>, IBoundingBoxTree<T2>> bs = b.split();
            atrees.push(as.item1);
            btrees.push(bs.item1);

            atrees.push(as.item1);
            btrees.push(bs.item0);

            atrees.push(as.item0);
            btrees.push(bs.item1);

            atrees.push(as.item0);
            btrees.push(bs.item0);
        }

        return results;
    }

    /**
     * @param curve1
     * @param curve2
     * @param tolerance
     * @return
     */
    public static Array<CurveCurveIntersection> curves(NurbsCurveData curve1, NurbsCurveData curve2, double tolerance) {
        Array<Pair<NurbsCurveData, NurbsCurveData>> ints = Intersect.boundingBoxTrees(
                new LazyCurveBoundingBoxTree(curve1, null),
                new LazyCurveBoundingBoxTree(curve2, null),
                tolerance);

        return ArrayExtensions.unique(
                (Array) ints.map(new Intersect_curves_661__Fun(tolerance, curve2, curve1))
                        .filter(new Intersect_curves_663__Fun(tolerance)),
                new Intersect_curves_665__Fun(tolerance));
    }

    /**
     * @param curve0
     * @param curve1
     * @param u0
     * @param u1
     * @param tolerance
     * @return
     */
    public static CurveCurveIntersection curvesWithEstimate(NurbsCurveData curve0, NurbsCurveData curve1, double u0, double u1, double tolerance) {
        Function objective = new Intersect_curvesWithEstimate_691__Fun(curve1, curve0);
        Function grad = new Intersect_curvesWithEstimate_712__Fun(curve1, curve0);

        MinimizationResult sol_obj = Minimizer.uncmin(objective, new Array(new Number[]{u0, u1}), tolerance * tolerance, grad, null);

        double u11 = Runtime.toDouble(sol_obj.solution.get(0));
        double u2 = Runtime.toDouble(sol_obj.solution.get(1));

        Array<Number> p11 = Eval.rationalCurvePoint(curve0, u11);
        Array<Number> p21 = Eval.rationalCurvePoint(curve1, u2);

        return new CurveCurveIntersection(p11, p21, u11, u2);
    }

    /**
     * @param mesh0
     * @param faceIndex0
     * @param mesh1
     * @param faceIndex1
     * @return
     */
    public static Interval<MeshIntersectionPoint> triangles(MeshData mesh0, int faceIndex0, MeshData mesh1, int faceIndex1) {
        Array<Number> tri0 = mesh0.faces.get(faceIndex0);
        Array<Number> tri1 = mesh1.faces.get(faceIndex1);
        Array<Number> n0 = Mesh.getTriangleNorm(mesh0.points, tri0);
        Array<Number> n1 = Mesh.getTriangleNorm(mesh1.points, tri1);
        Array<Number> o0 = mesh0.points.get((Runtime.toInt(tri0.get(0))));
        Array<Number> o1 = mesh1.points.get((Runtime.toInt(tri1.get(0))));
        Ray ray = Intersect.planes(o0, n0, o1, n1);
        if ((ray == null)) {
            return null;
        }

        Interval<CurveTriPoint> clip1 = Intersect.clipRayInCoplanarTriangle(ray, mesh0, faceIndex0);
        if (clip1 == null) {
            return null;
        }

        Interval<CurveTriPoint> clip2 = Intersect.clipRayInCoplanarTriangle(ray, mesh1, faceIndex1);
        if (clip2 == null) {
            return null;
        }

        Interval<MeshIntersectionPoint> merged = Intersect.mergeTriangleClipIntervals(clip1, clip2, mesh0, faceIndex0, mesh1, faceIndex1);
        if (merged == null) {
            return null;
        }

        return new Interval(
                new MeshIntersectionPoint(merged.min.uv0, merged.min.uv1, merged.min.point, faceIndex0, faceIndex1),
                new MeshIntersectionPoint(merged.max.uv0, merged.max.uv1, merged.max.point, faceIndex0, faceIndex1));
    }

    /**
     * @param ray
     * @param mesh
     * @param faceIndex
     * @return
     */
    public static Interval<CurveTriPoint> clipRayInCoplanarTriangle(Ray ray, MeshData mesh, int faceIndex) {
        Array<Number> tri = mesh.faces.get(faceIndex);
        Array<Number> oo0 = mesh.points.get(Runtime.toInt(tri.get(0)));
        Array<Number> oo1 = mesh.points.get(Runtime.toInt(tri.get(1)));
        Array<Number> oo2 = mesh.points.get(Runtime.toInt(tri.get(2)));

        Array<Number> uvs0 = mesh.uvs.get(Runtime.toInt(tri.get(0)));
        Array<Number> uvs1 = mesh.uvs.get(Runtime.toInt(tri.get(1)));
        Array<Number> uvs2 = mesh.uvs.get(Runtime.toInt(tri.get(2)));

        Array<Number> uvd0 = Vec.sub(uvs1, uvs0);
        Array<Number> uvd1 = Vec.sub(uvs2, uvs1);
        Array<Number> uvd2 = Vec.sub(uvs0, uvs2);

        Array<Number> s0 = Vec.sub(oo1, oo0);
        Array<Number> s1 = Vec.sub(oo2, oo1);
        Array<Number> s2 = Vec.sub(oo0, oo2);

        Array<Number> dd0 = Vec.normalized(s0);
        Array<Number> dd1 = Vec.normalized(s1);
        Array<Number> dd2 = Vec.normalized(s2);

        double l0 = Vec.norm(s0);
        double l1 = Vec.norm(s1);
        double l2 = Vec.norm(s2);

        CurveTriPoint minU = null;
        CurveTriPoint maxU = null;

        CurveCurveIntersection res = Intersect.rays(oo0, dd0, ray.origin, ray.dir);
        if (res != null) {

            double ld = Runtime.toDouble(l0);

            double useg = res.u0;
            double uray = res.u1;
            if (!((useg < -Constants.EPSILON) || (useg > (ld + Constants.EPSILON)))) {

                minU = new CurveTriPoint(uray, Vec.onRay(ray.origin, ray.dir, uray), Vec.onRay(uvs0, uvd0, (useg / ld)));

                maxU = new CurveTriPoint(uray, Vec.onRay(ray.origin, ray.dir, uray), Vec.onRay(uvs0, uvd0, (useg / ld)));
            }
        }

        res = Intersect.rays(oo1, dd1, ray.origin, ray.dir);
        if (res != null) {
            double ld = Runtime.toDouble(l1);

            double useg = res.u0;
            double uray = res.u1;
            if (!((useg < -Constants.EPSILON) || (useg > (ld + Constants.EPSILON)))) {

                if ((minU == null) || (uray < minU.u)) {

                    minU = new CurveTriPoint(uray, Vec.onRay(ray.origin, ray.dir, uray), Vec.onRay(uvs1, uvd1, (useg / ld)));
                }

                if ((maxU == null) || (uray > maxU.u)) {

                    maxU = new CurveTriPoint(uray, Vec.onRay(ray.origin, ray.dir, uray), Vec.onRay(uvs1, uvd1, (useg / ld)));
                }
            }
        }

        res = Intersect.rays(oo2, dd2, ray.origin, ray.dir);
        if (res != null) {

            double ld = Runtime.toDouble(l2);

            double useg = res.u0;
            double uray = res.u1;
            if (!((useg < -Constants.EPSILON) || (useg > (ld + Constants.EPSILON)))) {

                if ((minU == null) || (uray < minU.u)) {

                    minU = new CurveTriPoint(uray, Vec.onRay(ray.origin, ray.dir, uray), Vec.onRay(uvs2, uvd2, (useg / ld)));
                }

                if ((maxU == null) || (uray > maxU.u)) {

                    maxU = new CurveTriPoint(uray, Vec.onRay(ray.origin, ray.dir, uray), Vec.onRay(uvs2, uvd2, (useg / ld)));
                }
            }
        }

        if (maxU == null) {

            return null;
        }

        return new Interval(minU, maxU);
    }

    /**
     * @param clip1
     * @param clip2
     * @param mesh1
     * @param faceIndex1
     * @param mesh2
     * @param faceIndex2
     * @return
     */
    public static Interval<MeshIntersectionPoint> mergeTriangleClipIntervals(
            Interval<CurveTriPoint> clip1,
            Interval<CurveTriPoint> clip2,
            MeshData mesh1, int faceIndex1,
            MeshData mesh2, int faceIndex2) {

        if ((clip2.min.u > (clip1.max.u + Constants.EPSILON))
                || (clip1.min.u > (clip2.max.u + Constants.EPSILON))) {

            return null;
        }

        Pair<CurveTriPoint, Object> min = (clip1.min.u > clip2.min.u) ? new Pair(clip1.min, 0) : new Pair(clip2.min, 1);
        Pair<CurveTriPoint, Object> max = (clip1.max.u < clip2.max.u) ? new Pair(clip1.max, 0) : new Pair(clip2.max, 1);
        Interval<MeshIntersectionPoint> res = new Interval(
                new MeshIntersectionPoint(null, null, min.item0.point, faceIndex1, faceIndex2),
                new MeshIntersectionPoint(null, null, max.item0.point, faceIndex1, faceIndex2));

        if (Runtime.toInt(min.item1) == 0) {

            res.min.uv0 = min.item0.uv;
            res.min.uv1 = Mesh.triangleUVFromPoint(mesh2, faceIndex2, min.item0.point);
        } else {

            res.min.uv0 = Mesh.triangleUVFromPoint(mesh1, faceIndex1, min.item0.point);
            res.min.uv1 = min.item0.uv;
        }

        if (Runtime.toInt(max.item1) == 0) {

            res.max.uv0 = max.item0.uv;
            res.max.uv1 = Mesh.triangleUVFromPoint(mesh2, faceIndex2, max.item0.point);
        } else {

            res.max.uv0 = Mesh.triangleUVFromPoint(mesh1, faceIndex1, max.item0.point);
            res.max.uv1 = max.item0.uv;
        }

        return res;
    }

    /**
     * @param origin0
     * @param normal0
     * @param origin1
     * @param normal1
     * @return
     */
    public static Ray planes(Array<Number> origin0, Array<Number> normal0, Array<Number> origin1, Array<Number> normal1) {
        Array<Number> d = Vec.cross(normal0, normal1);
        if (Vec.dot(d, d) < Constants.EPSILON) {
            return null;
        }

        int li = 0;
        double mi = Math.abs((Runtime.toDouble(d.get(0))));
        double m1 = Math.abs((Runtime.toDouble(d.get(1))));
        double m2 = Math.abs((Runtime.toDouble(d.get(2))));
        if (m1 > mi) {
            li = 1;
            mi = m1;
        }

        if (m2 > mi) {
            li = 2;
        }

        double a1;
        double b1;
        double a2;
        double b2;
        if (li == 0) {
            a1 = Runtime.toDouble(normal0.get(1));
            b1 = Runtime.toDouble(normal0.get(2));
            a2 = Runtime.toDouble(normal1.get(1));
            b2 = Runtime.toDouble(normal1.get(2));
        } else {
            if (li == 1) {
                a1 = Runtime.toDouble(normal0.get(0));
                b1 = Runtime.toDouble(normal0.get(2));
                a2 = Runtime.toDouble(normal1.get(0));
                b2 = Runtime.toDouble(normal1.get(2));
            } else {
                a1 = Runtime.toDouble(normal0.get(0));
                b1 = Runtime.toDouble(normal0.get(1));
                a2 = Runtime.toDouble(normal1.get(0));
                b2 = Runtime.toDouble(normal1.get(1));
            }
        }

        double d1 = -Vec.dot(origin0, normal0);
        double d2 = -Vec.dot(origin1, normal1);
        double den = (a1 * b2) - (b1 * a2);
        double x = (((b1 * d2) - (d1 * b2))) / den;
        double y = (((d1 * a2) - (a1 * d2))) / den;
        Array<Number> p;
        if (li == 0) {
            p = new Array<>(new Number[]{0, x, y});
        } else {
            if (li == 1) {
                p = new Array<>(new Number[]{x, 0, y});
            } else {
                p = new Array<>(new Number[]{x, y, 0});
            }

        }

        return new Ray(p, Vec.normalized(d));
    }

    /**
     * @param n0
     * @param d0
     * @param n1
     * @param d1
     * @param n2
     * @param d2
     * @return
     */
    public static Array<Number> threePlanes(Array<Number> n0, double d0, Array<Number> n1, double d1, Array<Number> n2, double d2) {
        Array<Number> u = Vec.cross(n1, n2);
        double den = Vec.dot(n0, u);
        
        if (Math.abs(den) < Constants.EPSILON) {
            return null;
        }

        Array<Number> diff = Vec.sub(Vec.mul(d2, n1), Vec.mul(d1, n2));
        Array<Number> num = Vec.add(Vec.mul(d0, u), Vec.cross(n0, diff));
        
        return Vec.mul(1 / den, num);
    }

    /**
     * @param polyline0
     * @param polyline1
     * @param tol
     * @return
     */
    public static Array<CurveCurveIntersection> polylines(PolylineData polyline0, PolylineData polyline1, double tol) {
        Array<Pair<Object, Object>> res = Intersect.boundingBoxTrees(
                new LazyPolylineBoundingBoxTree(polyline0, null),
                new LazyPolylineBoundingBoxTree(polyline1, null),
                tol);

        Array<CurveCurveIntersection> finalResults = new Array<>(new CurveCurveIntersection[]{});
        int _g = 0;
        while (_g < res.length) {

            Pair<Object, Object> event = res.get(_g);
            ++_g;
            int polid0 = Runtime.toInt(event.item0);
            int polid1 = Runtime.toInt(event.item1);

            CurveCurveIntersection inter = Intersect.segments(
                    polyline0.points.get(polid0),
                    polyline0.points.get(polid0 + 1),
                    polyline1.points.get(polid1),
                    polyline1.points.get(polid1 + 1), tol);

            if (inter == null) {
                continue;
            }

            inter.u0 = Runtime.toDouble(
                    Vec.lerp(inter.u0,
                            new Array(new Number[]{Runtime.toDouble(polyline0.params.get(polid0))}),
                            new Array(new Number[]{Runtime.toDouble(polyline0.params.get(polid0 + 1))})).get(0));

            inter.u1 = Runtime.toDouble(
                    Vec.lerp(inter.u1,
                            new Array(new Number[]{Runtime.toDouble(polyline1.params.get(polid1))}),
                            new Array(new Number[]{Runtime.toDouble(polyline1.params.get(polid1 + 1))})).get(0));

            finalResults.push(inter);
        }

        return finalResults;
    }

    /**
     * @param a0
     * @param a1
     * @param b0
     * @param b1
     * @param tol
     * @return
     */
    public static CurveCurveIntersection segments(Array<Number> a0, Array<Number> a1, Array<Number> b0, Array<Number> b1, double tol) {
        Array<Number> a1ma0 = Vec.sub(a1, a0);
        double aN = Math.sqrt(Vec.dot(a1ma0, a1ma0));
        Array<Number> a = Vec.mul((1 / aN), a1ma0);
        Array<Number> b1mb0 = Vec.sub(b1, b0);
        double bN = Math.sqrt(Vec.dot(b1mb0, b1mb0));
        Array<Number> b = Vec.mul((1 / bN), b1mb0);
        CurveCurveIntersection int_params = Intersect.rays(a0, a, b0, b);
        if ((int_params != null)) {
            double u0 = Math.min(Math.max((0), (int_params.u0 / aN)), 1.0);
            double u1 = Math.min(Math.max((0), (int_params.u1 / bN)), 1.0);
            Array<Number> point0 = Vec.onRay(a0, a1ma0, u0);
            Array<Number> point1 = Vec.onRay(b0, b1mb0, u1);
            double dist = Vec.distSquared(point0, point1);
            if ((dist < (tol * tol))) {
                return new CurveCurveIntersection(point0, point1, u0, u1);
            }

        }

        return null;
    }

    /**
     * @param a0
     * @param a
     * @param b0
     * @param b
     * @return
     */
    public static CurveCurveIntersection rays(Array<Number> a0, Array<Number> a, Array<Number> b0, Array<Number> b) {
        double dab = Vec.dot(a, b);
        double dab0 = Vec.dot(a, b0);
        double daa0 = Vec.dot(a, a0);
        double dbb0 = Vec.dot(b, b0);
        double dba0 = Vec.dot(b, a0);
        double daa = Vec.dot(a, a);
        double dbb = Vec.dot(b, b);
        double div = (daa * dbb) - (dab * dab);

        if (Math.abs(div) < Constants.EPSILON) {

            return null;
        }

        double num = (dab * (dab0 - daa0)) - (daa * (dbb0 - dba0));
        double w = num / div;
        double t = ((dab0 - daa0) + (w * dab)) / daa;
        Array<Number> p0 = Vec.onRay(a0, a, t);
        Array<Number> p1 = Vec.onRay(b0, b, w);
        return new CurveCurveIntersection(p0, p1, t, w);
    }

    /**
     * @param p0
     * @param p1
     * @param points
     * @param tri
     * @return
     */
    public static TriSegmentIntersection segmentWithTriangle(Array<Number> p0, Array<Number> p1, Array<Array<Number>> points, Array<Number> tri) {
        Array<Number> v0 = points.get((Runtime.toInt(tri.get(0))));
        Array<Number> v1 = points.get((Runtime.toInt(tri.get(1))));
        Array<Number> v2 = points.get((Runtime.toInt(tri.get(2))));
        Array<Number> u = Vec.sub(v1, v0);
        Array<Number> v = Vec.sub(v2, v0);
        Array<Number> n = Vec.cross(u, v);
        Array<Number> dir = Vec.sub(p1, p0);
        Array<Number> w0 = Vec.sub(p0, v0);
        double a = -(Vec.dot(n, w0));
        double b = Vec.dot(n, dir);
        if ((Math.abs(b) < Constants.EPSILON)) {
            return null;
        }

        double r = (a / b);
        if (((r < 0) || (r > 1))) {
            return null;
        }

        Array<Number> pt = Vec.add(p0, Vec.mul(r, dir));
        double uv = Vec.dot(u, v);
        double uu = Vec.dot(u, u);
        double vv = Vec.dot(v, v);
        Array<Number> w = Vec.sub(pt, v0);
        double wu = Vec.dot(w, u);
        double wv = Vec.dot(w, v);
        double denom = ((uv * uv) - (uu * vv));
        if ((Math.abs(denom) < Constants.EPSILON)) {
            return null;
        }

        double s = ((((uv * wv) - (vv * wu))) / denom);
        double t = ((((uv * wu) - (uu * wv))) / denom);
        if ((((((s > (1.0 + Constants.EPSILON)) || (t > (1.0 + Constants.EPSILON))) || (t < -(Constants.EPSILON))) || (s < -(Constants.EPSILON))) || ((s + t) > (1.0 + Constants.EPSILON)))) {
            return null;
        }

        return new TriSegmentIntersection(pt, s, t, r);
    }

    /**
     * @param p0
     * @param p1
     * @param v0
     * @param n
     * @return
     */
    public static Object segmentAndPlane(Array<Number> p0, Array<Number> p1, Array<Number> v0, Array<Number> n) {
        double denom = Vec.dot(n, Vec.sub(p1, p0));
        if ((Math.abs(denom) < Constants.EPSILON)) {
            return null;
        }

        double numer = Vec.dot(n, Vec.sub(v0, p0));
        double p = (numer / denom);
        if (((p > (1.0 + Constants.EPSILON)) || (p < -(Constants.EPSILON)))) {
            return null;
        }

        return new DynamicObject(new String[]{}, new Object[]{}, new String[]{"p"}, new double[]{((p))});
    }

    /**
     * @param empty
     */
    public Intersect(EmptyObject empty) {
    }

    /**
     *
     */
    public Intersect() {
        Intersect.__hx_ctor_verb_eval_Intersect(this);
    }

}
