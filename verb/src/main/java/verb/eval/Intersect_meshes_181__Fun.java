package verb.eval;

import haxe.lang.Function;
import haxe.lang.Runtime;
import verb.core.MeshData;
import verb.core.Pair;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_meshes_181__Fun extends Function {

    /**
     *
     */
    public MeshData mesh1;

    /**
     *
     */
    public MeshData mesh0;

    /**
     *
     * @param mesh1
     * @param mesh0
     */
    public Intersect_meshes_181__Fun(MeshData mesh1, MeshData mesh0) {
        super(1, 0);
        this.mesh1 = mesh1;
        this.mesh0 = mesh0;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
        Pair<Object, Object> ids = (Pair<Object, Object>) __fn_dyn1;
        return Intersect.triangles(this.mesh0, Runtime.toInt(ids.item0), this.mesh1, Runtime.toInt(ids.item1));
    }

}
