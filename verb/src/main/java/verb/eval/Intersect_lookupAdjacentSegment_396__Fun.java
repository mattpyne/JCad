package verb.eval;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_lookupAdjacentSegment_396__Fun extends haxe.lang.Function {

    /**
     *
     */
    public verb.core.MeshIntersectionPoint segEnd;

    /**
     *
     * @param segEnd
     */
    public Intersect_lookupAdjacentSegment_396__Fun(verb.core.MeshIntersectionPoint segEnd) {
        super(1, 0);
        this.segEnd = segEnd;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        verb.core.Pair<verb.core.KdPoint<verb.core.MeshIntersectionPoint>, java.lang.Object> r
                = (((__fn_dyn1 == haxe.lang.Runtime.undefined))
                        ? (((verb.core.Pair<verb.core.KdPoint<verb.core.MeshIntersectionPoint>, java.lang.Object>) (((java.lang.Object) (__fn_float1)))))
                        : (((verb.core.Pair<verb.core.KdPoint<verb.core.MeshIntersectionPoint>, java.lang.Object>) (__fn_dyn1))));
        return (this.segEnd != r.item0.obj);
    }

}
