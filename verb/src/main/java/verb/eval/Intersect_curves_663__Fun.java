package verb.eval;

import verb.core.CurveCurveIntersection;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Intersect_curves_663__Fun extends haxe.lang.Function {

    /**
     *
     */
    public double tolerance;

    /**
     *
     * @param tolerance
     */
    public Intersect_curves_663__Fun(double tolerance) {
        super(1, 0);
        this.tolerance = tolerance;
    }

    /**
     *
     * @param __fn_float1
     * @param __fn_dyn1
     * @return
     */
    @Override
    public java.lang.Object __hx_invoke1_o(double __fn_float1, java.lang.Object __fn_dyn1) {
        CurveCurveIntersection x1 = __fn_dyn1 == haxe.lang.Runtime.undefined
                ? (CurveCurveIntersection) ((Object) (__fn_float1))
                : (CurveCurveIntersection) __fn_dyn1;
        return Vec.distSquared(x1.point0, x1.point1) < this.tolerance;
    }

}
