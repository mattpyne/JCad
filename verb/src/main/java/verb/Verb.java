package verb;

/**
 *
 * @author Matthew
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class Verb extends haxe.lang.HxObject {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        {
            verb.Verb.main();
        }
    }

    /**
     *
     * @param __hx_this
     */
    public static void __hx_ctor_verb_Verb(verb.Verb __hx_this) {
    }

    /**
     *
     */
    public static void main() {
        haxe.Log.trace.__hx_invoke2_o(0.0, "verb 2.1.0", 0.0, new haxe.lang.DynamicObject(new java.lang.String[]{"className", "fileName", "methodName"}, new java.lang.Object[]{"verb.Verb", "Verb.hx", "main"}, new java.lang.String[]{"lineNumber"}, new double[]{((double) (((double) (45))))}));
    }

    /**
     *
     * @param empty
     */
    public Verb(haxe.lang.EmptyObject empty) {
    }

    /**
     *
     */
    public Verb() {
        verb.Verb.__hx_ctor_verb_Verb(this);
    }

}
