package playground;

import haxe.root.Array;
import verb.eval.Intersect;
import verb.geom.ICurve;
import verb.geom.NurbsCurve;
import verb.geom.NurbsSurface;

/**
 *
 * @author matt pyne
 */
public class main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Array p1 = new Array(new Integer[]{-10, 0, 0});
        Array p2 = new Array(new Integer[]{10, 0, 0});
        Array p3 = new Array(new Double[]{10., 10., 0.});
        Array p4 = new Array(new Integer[]{0, 10, 0});
        Array p5 = new Array(new Integer[]{5, 5, 0});
        Array p6 = new Array(new Integer[]{5, 5, 2});

        Array pts = new Array();
        pts.push(p1);
        pts.push(p2);
        pts.push(p3);
        pts.push(p4);
        pts.push(p5);
        pts.push(p6);

        NurbsCurve interpCurve = NurbsCurve.byPoints(pts, 3);

        testFlatPlaneIntersect();
    }

    private static void testFlatPlaneIntersect() {
        Array ctPts = new Array<>();
        ctPts.push(new Array(new Double[]{0., 0., 0.}));
        ctPts.push(new Array(new Double[]{1., 0., 0.}));
        Array<Number> knots = new Array<>(new Double[]{0., 0., 1., 1.});
        NurbsCurve line00 = NurbsCurve.byKnotsControlPointsWeights(1, knots, ctPts, null);
        Array ctPts2 = new Array<>();
        ctPts2.push(new Array(new Double[]{0., 1., 0.}));
        ctPts2.push(new Array(new Double[]{1., 1., 0.}));
        NurbsCurve line01 = NurbsCurve.byKnotsControlPointsWeights(1, knots, ctPts2, null);

        NurbsSurface surface0 = NurbsSurface.byLoftingCurves(new Array<>(new ICurve[]{line00, line01}), 1);

        Array ctPts3 = new Array<>();
        ctPts3.push(new Array(new Double[]{0.5, 0., -0.5}));
        ctPts3.push(new Array(new Double[]{0.5, 0.5, -0.5}));
        NurbsCurve line10 = NurbsCurve.byKnotsControlPointsWeights(1, knots, ctPts3, null);

        Array ctPts4 = new Array<>();
        ctPts4.push(new Array(new Double[]{0.5, 0., 0.5}));
        ctPts4.push(new Array(new Double[]{.5, 0.5, 0.}));

        NurbsCurve line11 = NurbsCurve.byKnotsControlPointsWeights(1, knots, ctPts4, null);

        NurbsSurface surface1 = NurbsSurface.byLoftingCurves(new Array<>(new ICurve[]{line10, line11}), 1);

        Array<verb.core.NurbsCurveData> curves = Intersect.surfaces(surface0.asNurbs(), surface1.asNurbs(), 0.0001);
        // [{ knots => [0,0,0,0,0.45,1,1,1,1],
        // degree => 3, 
        // controlPoints => [[0.5,0.5,0,1],[0.5,0.425,0,1],[0.5,0.258333333333333,0,1],[0.500000000000001,0.0916666666666668,0,1],[0.5,0,0,1]] }]

    }
}
