package org.pyne.jcad.core;

import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.core.entity.EntityFactory;

@Plugin(name = "ModelMaker")
public class ModelMaker {

    private final EntityFactory entityFactory = new EntityFactory();

    public boolean someAPI() {
        return false;
    }

    public EntityFactory entityFactory() {
        return entityFactory;
    }
}
