package eu.mihosoft.jcsg.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Matthew Alderson
 */
public class JCSGTimeTracker {
    
    private static JCSGTimeTracker instance = null;

    /**
     *
     * @return
     */
    public static synchronized JCSGTimeTracker getInstance() {
        if (instance == null) {
            instance = new JCSGTimeTracker();
        }
        return instance;
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";

    private final Map<Integer, Long> startTimes;
    private final Map<Integer, Long> accumulatorTimes;
    
    private int timeIndex;
    private int accumulatorIndex;
    
    private boolean on;
    private boolean soutOnlyNonZeroTime;
    private final LinkedHashMap<String, Long> accumulator;
    private final LinkedHashMap<String, Long> numAccumulations;
    private boolean inNanoSecs;

    /**
     *
     */
    public JCSGTimeTracker() {
        this.startTimes = new HashMap<>();
        this.timeIndex = 0;
        this.on = true;
        this.accumulator = new LinkedHashMap<>();
        this.numAccumulations = new LinkedHashMap<>();
        this.accumulatorTimes = new HashMap<>();
    }

    /**
     * Record the current time at the next time index.
     * 
     * @Matt A.
     */
    public void setStartTime() {
        if (on) {
            timeIndex++;
            startTimes.put(timeIndex, getCurrentTime());
        }
    }
    
    /**
     * Record the current time at the next time index.
     * 
     * @param message
     * @Matt A.
     */
    public void setStartTime(String message) {
        if (on) {
            soutMessage("BEGIN: " + message);
            timeIndex++;
            startTimes.put(timeIndex, getCurrentTime());
        }
    }
    
    /**
     * Record the current time at the next time index.
     * 
     * @param message
     * @Matt A.
     */
    public void soutEndTime(String message) {
        soutTime("END: " + message);
    }
    
    /**
     * Get the current time in milliseconds or nanoseconds.
     * 
     * @return 
     * 
     * @Matt A.
     */
    private long getCurrentTime() {
        return inNanoSecs ? System.nanoTime() : new Date().getTime();
    }

    /**
     * Sout the given message.
     * 
     * @param message
     * 
     * @Matt A.
     */
    public void soutMessage(String message) {
        if (on) {
            System.out.println(getIndent() + message);
        }
    }

    /**
     * Get indentation based on the time index.
     * 
     * @return
     * 
     * @Matt A.
     */
    private String getIndent() {
        
        if (timeIndex > 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < timeIndex; i++) {
                sb.append(" ");
            }
            return sb.toString();
        }
        
        return "";
    }

    /**
     * 
     * @param name 
     */
    public void resetAccumulator(String name) {
        accumulator.put(name, 0L);
    }

    /**
     * Record the current time in the accumulator times map.
     * 
     * @Matt A.
     */
    public void recordAccumulatorTime() {
        if (on) {
            accumulatorIndex++;
            accumulatorTimes.put(accumulatorIndex, getCurrentTime());
        }
    }
    
    /**
     * Accumulate time for the given key in the accumulator map.
     * 
     * @param key
     * 
     * @Matt A.
     */
    public void accumulateTime(String key) {
        if (on) {
            Long time = getCurrentTime() - accumulatorTimes.get(accumulatorIndex);
            accumulator.put(key, accumulator.getOrDefault(key, 0L) + time);
            numAccumulations.put(key, accumulator.getOrDefault(key, 0L) + 1);
            accumulatorIndex--;
        }
    }
    
    /**
     * Print the accumulated times.
     * 
     * @Matt A.
     */
    public void soutAccumulator() {
//        if (on) {
            accumulator.entrySet().forEach(entry -> {
                soutTime(entry.getKey(), entry.getValue());
            });
            
            accumulatorIndex = 0;
            clearAccumulator();
//        }
    }
    
    /**
     * Print the accumulated times.
     * 
     * @Matt A.
     */
    public void soutAccumulator(boolean soutNumAccumulations) {
        if (on) {
            accumulator.entrySet().forEach(entry -> {
                if (soutNumAccumulations) {
                    soutTime(
                            entry.getKey(), 
                            entry.getValue(), 
                            numAccumulations.get(entry.getKey()));
                } else {
                    soutTime(entry.getKey(), entry.getValue());
                }
            });
            
            accumulatorIndex = 0;
            clearAccumulator();
        }
    }
    
    /**
     * Clears the accumulator map.
     * 
     * @Matt A.
     */
    public void clearAccumulator() {
        accumulator.clear();
    }

    /**
     * Souts the given message.
     * 
     * @param message
     * 
     * @Matt A.
     */
    public void soutTime(String message) {
        if (on) {
            Long time = (getCurrentTime()) - startTimes.get(timeIndex);

            boolean soutTime = !soutOnlyNonZeroTime || time > 0;
            if (soutTime) {
                soutTime(message, time);
            }

            timeIndex--;
        }
    }

    /**
     * Souts the given time and message.
     * 
     * @param time
     * @param message 
     * 
     * @Matt A.
     */
    private void soutTime(String message, Long time) {
        String timeStr = Long.toString(time);
        if (time > 0) {
            timeStr = ANSI_RED.concat(timeStr).concat(ANSI_RESET);
        }
        
        System.out.println(getIndent() + message + ": " + timeStr);
    }
    
    /**
     * Souts the given time and message.
     * 
     * @param time
     * @param message 
     * @param numAccumulations 
     * 
     * @Matt A.
     */
    private void soutTime(String message, Long time, Long numAccumulations) {
    
        String timeStr = Long.toString(time);
        if (time > 0) {
            String numAccumulationsStr = Long.toString(numAccumulations);
            timeStr = ANSI_RED.concat(timeStr + ", " + numAccumulationsStr).concat(ANSI_RESET);
        }
        
        System.out.println(getIndent() + message + ": " + timeStr);
    }

    /**
     *
     * @return
     */
    public boolean isOn() {
        return on;
    }

    /**
     *
     * @param on
     */
    public void setOn(boolean on) {
        this.on = on;
    }

    /**
     *
     * @return
     */
    public boolean isSoutOnlyNonZeroTime() {
        return soutOnlyNonZeroTime;
    }

    /**
     * 
     * @param soutOnlyNonZeroTime
     */
    public void setSoutOnlyNonZeroTime(boolean soutOnlyNonZeroTime) {
        this.soutOnlyNonZeroTime = soutOnlyNonZeroTime;
    }

    /**
     * 
     * @return 
     */
    public boolean isInNanoSecs() {
        return inNanoSecs;
    }

    /**
     * 
     * @param inNanoSecs 
     */
    public void setInNanoSecs(boolean inNanoSecs) {
        this.inNanoSecs = inNanoSecs;
    }

}
