package eu.mihosoft.jcsg.utils;

import eu.mihosoft.jcsg.Bounds;
import eu.mihosoft.jcsg.Vertex;
import eu.mihosoft.vvecmath.Vector3d;
import java.util.Collection;
import java.util.stream.Stream;

/**
 *
 * @author Matthew Alderson
 */
public class JCSGExtrema3D {

    private double minX;
    private double minY;
    private double minZ;
    private double maxX;
    private double maxY;
    private double maxZ;

    /**
     *
     */
    private JCSGExtrema3D() {
        
        minX = Double.POSITIVE_INFINITY;
        minY = Double.POSITIVE_INFINITY;
        minZ = Double.POSITIVE_INFINITY;

        maxX = Double.NEGATIVE_INFINITY;
        maxY = Double.NEGATIVE_INFINITY;
        maxZ = Double.NEGATIVE_INFINITY;
    }

    /**
     * Calculates the extrema given a list of 3d pts.
     *
     * @param pts
     */
    public void calcExtremaFor(Stream<Vector3d> pts) {
        pts.forEach(pt -> checkAgainst(pt));
    }
    
    /**
     * Calculates the extrema given a list of 3d pts.
     *
     * @param pts
     */
    public void calcExtremaFor(Collection<Vector3d> pts) {
        pts.forEach(pt -> checkAgainst(pt));
    }

    /**
     *
     * @param other
     */
    public void checkAgainst(JCSGExtrema3D other) {

        double otherMinX = other.minX;
        double otherMinY = other.minY;
        double otherMinZ = other.minZ;
        
        double otherMaxX = other.maxX;
        double otherMaxY = other.maxY;
        double otherMaxZ = other.maxZ;

        if (otherMinX < minX) {
            minX = otherMinX;
        }
        if (otherMinY < minY) {
            minY = otherMinY;
        }
        if (otherMinZ < minZ) {
            minZ = otherMinZ;
        }

        if (otherMaxX > maxX) {
            maxX = otherMaxX;
        }
        if (otherMaxY > maxY) {
            maxY = otherMaxY;
        }
        if (otherMaxZ > maxZ) {
            maxZ = otherMaxZ;
        }
        
    }
    
    /**
     * 
     * @param e1
     * @param e2
     * @return 
     */
    public static JCSGExtrema3D combine(JCSGExtrema3D e1, JCSGExtrema3D e2) {
        
        JCSGExtrema3D combined = new JCSGExtrema3D();
        
        combined.checkAgainst(e1);
        combined.checkAgainst(e2);
        
        return combined;
    }

    /**
     *
     * @param v
     */
    public void checkAgainst(Vector3d v) {

        double vx = v.x();
        double vy = v.y();
        double vz = v.z();
        
        if (vx < minX) {
            minX = vx;
        }
        if (vy < minY) {
            minY = vy;
        }
        if (vz < minZ) {
            minZ = vz;
        }

        if (vx > maxX) {
            maxX = vx;
        }
        if (vy > maxY) {
            maxY = vy;
        }
        if (vz > maxZ) {
            maxZ = vz;
        }
    }

    /**
     *
     * @return
     */
    public Vector3d getMin() {
        return Vector3d.xyz(minX, minY, minZ);
    }

    public double getMinX() {
        return getMin().x();
    }

    public double getMinY() {
        return getMin().y();
    }

    public double getMinZ() {
        return getMin().z();
    }

    /**
     *
     * @return
     */
    public Vector3d getMax() {
        return Vector3d.xyz(maxX, maxY, maxZ);
    }

    public double getMaxX() {
        return getMax().x();
    }

    public double getMaxY() {
        return getMax().y();
    }

    public double getMaxZ() {
        return getMax().z();
    }

    public double getWidthX() {
        return getMaxX() - getMinX();
    }

    public double getWidthY() {
        return getMaxY() - getMinY();
    }

    public double getWidthZ() {
        return getMaxZ() - getMinZ();
    }
    
    /**
     * 
     * @param verts
     * @return 
     */
    public static JCSGExtrema3D calcVector3dExtrema(Stream<Vector3d> verts) {
        
        JCSGExtrema3D extrema = new JCSGExtrema3D();
        extrema.calcExtremaFor(verts);
        
        return extrema;
    }
    
    /**
     * 
     * @param verts
     * @return 
     */
    public static JCSGExtrema3D calcVertexExtrema(Stream<Vertex> verts) {
        
       return calcVector3dExtrema(verts.map(v -> v.pos));
    }
    
    /**
     * 
     * @param verts
     * @return 
     */
    public static Bounds calcVector3dBounds(Stream<Vector3d> verts) {
        
        JCSGExtrema3D extrema = calcVector3dExtrema(verts);
        
        Vector3d min = extrema.getMin();
        Vector3d max = extrema.getMax();
        
        return new Bounds(min, max);
    }
    
    /**
     *
     * @param verts
     * @return
     */
    public static Bounds calcVertexBounds(Stream<Vertex> verts) {

        JCSGExtrema3D extrema = calcVertexExtrema(verts);

        Vector3d min = extrema.getMin();
        Vector3d max = extrema.getMax();

        return new Bounds(min, max);
    }

    /**
     * 
     * @return 
     */
    public static JCSGExtrema3D emptyExtrema() {
        return new JCSGExtrema3D();
    }
    
    public double getCenterX() {
        return (getMinX() + getMaxX()) / 2.0;
    }
    
    public double getCenterY() {
        return (getMinY() + getMaxY()) / 2.0;
    }
    
    public double getCenterZ() {
        return (getMinZ() + getMaxZ()) / 2.0;
    }

    /**
     * 
     * @param b
     * @return 
     */
    public boolean intersects(JCSGExtrema3D b) {

        Vector3d min = getMin();
        Vector3d max = getMax();
        
        Vector3d bMin = b.getMin();
        Vector3d bMax = b.getMax();
        
        if (bMin.x() > max.x() || bMax.x() < min.x()) {
            return false;
        }
        
        if (bMin.y() > max.y() || bMax.y() < min.y()) {
            return false;
        }
        
        if (bMin.z() > max.z() || bMax.z() < min.z()) {
            return false;
        }

        return true;

    }

}
