package eu.mihosoft.jcsg.utils;

import eu.mihosoft.vvecmath.Vector3d;
import java.util.Arrays;

/**
 *
 * @author Matthew Alderson
 */
public class JCSGVecMath {
    
    /**
     * 
     * @param v
     * @return 
     */
    public static Vector3d makeDoubleArrayFinite(Vector3d v) {
        
        double[] finiteArray = makeDoubleArrayFinite(v.get());
        
        return Vector3d.xyz(finiteArray[0], finiteArray[1], finiteArray[2]);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static double[] makeDoubleArrayFinite(double[] array) {
        double[] finiteArray = Arrays.copyOf(array, array.length);
        if (Arrays.stream(array)
                .anyMatch(d -> ! Double.isFinite(d))) {
            Arrays.fill(finiteArray, 0);
        }

        return finiteArray;
    }
    
    public static boolean isNonPositive(double d, double tolerance) {
        return isZero(d, tolerance) || d < 0.;
    }
    
    public static boolean isZero(double d, double tolerance) {
        return Math.abs(d) < tolerance;
    }
}
