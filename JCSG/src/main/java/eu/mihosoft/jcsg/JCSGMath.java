package eu.mihosoft.jcsg;

import eu.mihosoft.vvecmath.Vector3d;

/**
 *
 * @author Matthew
 */
public class JCSGMath {

    public static final double EPS = 0.000001;

    public static boolean equal(Vector3d v0, Vector3d v1) {
        return Math.abs(v0.x() - v1.x()) < EPS
                && Math.abs(v0.y() - v1.y()) < EPS
                && Math.abs(v0.z() - v1.z()) < EPS;
    }
}
