/**
 * PolygonUtil.java
 *
 * Copyright 2014-2014 Michael Hoffer <info@michaelhoffer.de>. All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Michael Hoffer <info@michaelhoffer.de> "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Michael Hoffer <info@michaelhoffer.de> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of Michael Hoffer
 * <info@michaelhoffer.de>.
 */
package eu.mihosoft.jcsg.ext.org.poly2tri;

import eu.mihosoft.jcsg.Edge;
import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.jcsg.Extrude;
import eu.mihosoft.jcsg.Plane;
import eu.mihosoft.vvecmath.Vector3d;
import eu.mihosoft.jcsg.Vertex;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author Michael Hoffer &lt;info@michaelhoffer.de&gt;
 */
public class PolygonUtil {

    private PolygonUtil() {
        throw new AssertionError("Don't instantiate me!", null);
    }

    /**
     *
     * @param polygon
     * @return
     */
    public static Stream<Polygon> triangulate(Polygon polygon) {
        List<Polygon> triangles = new ArrayList<>();
        if (polygon.vertices.size() >= 3) {

            for (int i = 0; i < polygon.vertices.size() - 2; i++) {

                triangles.add(Polygon.fromPoints(polygon.vertices.get(0).pos,
                        polygon.vertices.get(i + 1).pos,
                        polygon.vertices.get(i + 2).pos));
            }
        }

        return triangles.stream();
    }

    /**
     * Converts a CSG polygon to a poly2tri polygon (including holes)
     * @param polygon the polygon to convert
     * @return a CSG polygon to a poly2tri polygon (including holes)
     */
    private static eu.mihosoft.jcsg.ext.org.poly2tri.Polygon fromCSGPolygon(
            eu.mihosoft.jcsg.Polygon polygon) {
        
        // convert polygon
        List< PolygonPoint> points = new ArrayList<>();
        for (Vertex v : polygon.vertices) {
            PolygonPoint vp = new PolygonPoint(v.pos.x(), v.pos.y(), v.pos.z());
            points.add(vp);
        }

        eu.mihosoft.jcsg.ext.org.poly2tri.Polygon result
                = new eu.mihosoft.jcsg.ext.org.poly2tri.Polygon(points);

        // convert holes
        Optional<List<Polygon>> holesOfPresult
                = polygon.
                getStorage().getValue(Edge.KEY_POLYGON_HOLES);
        if (holesOfPresult.isPresent()) {
            List<Polygon> holesOfP = holesOfPresult.get();

            holesOfP.stream().forEach((hP) -> {
                result.addHole(fromCSGPolygon(hP));
            });
        }

        return result;
    }

    public static List<Polygon> concaveToConvex(
            Polygon concave) {

        return concaveToConvex(concave, new ArrayList<>());
    }

    public static Vector3d normalOf(List<? extends Vector3d> points) {
        Vector3d normal = Vector3d.ZERO;

        for (int i = 0; i < points.size(); i++) {
            int j = (i + 1) % points.size();
            double x = (points.get(i).z() + points.get(j).z())
                    * (points.get(j).y() - points.get(i).y());
            double y = (points.get(i).x() + points.get(j).x())
                    * (points.get(j).z() - points.get(i).z());
            double z = (points.get(i).y() + points.get(j).y())
                    * (points.get(j).x() - points.get(i).x());

            normal = normal.plus(x, y, z);
        }

        return normal.normalized();
    }
    
    public static Plane planeOf(List<Vector3d> points) {
        Plane plane = Plane.createFromPoints(Vector3d.ZERO, Vector3d.ZERO, Vector3d.ZERO);

        int index = 0;
        while (Vector3d.ZERO.equals(plane.normal)
                && index + 3 <= points.size()) {

            plane = Plane.createFromPoints(
                    points.get(index),
                    points.get(index + 1),
                    points.get(index + 2));
            index++;
        }

        return plane;
    }

    public static List<Polygon> concaveToConvex(
            Polygon concave,
            List<Polygon> holes) {

        boolean cw = !Extrude.isCCW(concave);

        List<Polygon> result = new ArrayList<>();

        Vector3d normal = concave.vertices.get(0).normal.clone();

        eu.mihosoft.jcsg.ext.org.poly2tri.Polygon p
                = fromCSGPolygon(concave);

        holes.stream()
                .map(pHole -> fromCSGPolygon(pHole))
                .forEach(pHole -> p.addHole(pHole));

        try {
            
            Poly2Tri.triangulate(p);
        } catch (NullPointerException ex) {
            
            System.out.println("Failed to trianglulate: \n\n" + concave.toStlString());
            result.add(concave);
            return result;
        }
        
        List<DelaunayTriangle> triangles = p.getTriangles();

        List<Vertex> triPoints = new ArrayList<>();

        for (DelaunayTriangle t : triangles) {

            int counter = 0;
            for (TriangulationPoint tp : t.points) {

                triPoints.add(new Vertex(
                        Vector3d.xyz(tp.getX(), tp.getY(), tp.getZ()),
                        normal));

                if (counter == 2) {
                    if (!cw) {
                        Collections.reverse(triPoints);
                    }
                    Polygon poly =
                            new Polygon(
                                    triPoints, concave.getStorage());
                    result.add(poly);
                    counter = 0;
                    triPoints = new ArrayList<>();

                } else {
                    counter++;
                }
            }
        }

        return result;
    }
    
    /**
     * Convert the main polygon and all it's holes to a convex set
     * of polygons. I.e. triangulate.
     * 
     * @param concave
     * @param holes
     * @return 
     * 
     * @Matt A.
     */
    public static List<Polygon> concaveToConvex2(
            Polygon concave,
            List<Polygon> holes) {
        
        holes.forEach(concave::addHole);
        
        return concave.toTriangles2();
    }
}
