/**
 * Polygon.java
 *
 * Copyright 2014-2014 Michael Hoffer <info@michaelhoffer.de>. All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Michael Hoffer <info@michaelhoffer.de> "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Michael Hoffer <info@michaelhoffer.de> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of Michael Hoffer
 * <info@michaelhoffer.de>.
 */
package eu.mihosoft.jcsg;

import eu.mihosoft.jcsg.ext.org.poly2tri.PolygonUtil;
import eu.mihosoft.jcsg.utils.JCSGExtrema3D;
import eu.mihosoft.vvecmath.Transform;
import eu.mihosoft.vvecmath.Vector3d;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a convex polygon.
 *
 * Each convex polygon has a {@code shared} property, which is shared between
 * all polygons that are clones of each other or where split from the same
 * polygon. This can be used to define per-polygon properties (such as surface
 * color).
 */
public final class Polygon {

    /**
     * Polygon vertices
     */
    public final List<Vertex> vertices;
    
    /**
     * The holes in this polygon.
     */
    public final List<Polygon> holes;
    
    /**
     * Shared property (can be used for shared color etc.).
     */
    private PropertyStorage shared;
    /**
     * Plane defined by this polygon.
     *
     * <b>Note:</b> uses first three vertices to define the plane.
     */
    public Plane _csg_plane;
    private eu.mihosoft.vvecmath.Plane plane;
    private List<Edge> edges;

    // Triangle poly point contianment optimization
    private double dot01;
    private double dot11;
    private double dot00;
    private double denom;
    private Vector3d A;
    private Vector3d B;
    private Vector3d C;
    private Vector3d v0;
    private Vector3d v1;

    private Bounds bounds;
    private JCSGExtrema3D extrema;


    public List<Edge> edges() {
        if (null == edges) {

            edges = Edge.fromPolygon(this);
        }

        return edges;
    }

    /**
     * Returns the plane defined by this triangle.
     *
     * @return plane
     */
    public eu.mihosoft.vvecmath.Plane getPlane() {
        return plane;
    }

    void setStorage(PropertyStorage storage) {
        this.shared = storage;
    }

    /**
     * Decomposes the specified concave polygon into convex polygons.
     *
     * @param points the points that define the polygon
     * @return the decomposed concave polygon (list of convex polygons)
     */
    public static List<Polygon> fromConcavePoints(Vector3d... points) {
        Polygon p = fromPoints(points);

        return PolygonUtil.concaveToConvex(p);
    }

    /**
     * Decomposes the specified concave polygon into convex polygons.
     *
     * @param points the points that define the polygon
     * @return the decomposed concave polygon (list of convex polygons)
     */
    public static List<Polygon> fromConcavePoints(List<Vector3d> points) {
        Polygon p = fromPoints(points);

        return PolygonUtil.concaveToConvex(p);
    }

    /**
     * Indicates whether this polyon is valid, i.e., if it
     *
     * @return
     */
    public boolean isValid() {
        return valid;
    }

    private boolean valid = true;

    /**
     * Constructor. Creates a new polygon that consists of the specified
     * vertices.
     *
     * <b>Note:</b> the vertices used to initialize a polygon must be coplanar
     * and form a convex loop.
     *
     * @param vertices polygon vertices
     * @param shared shared property
     */
    public Polygon(List<Vertex> vertices, PropertyStorage shared) {
        this.vertices = vertices;
        this.holes = new ArrayList<>();
        this.shared = shared;
        initPlanes();
        validateAndInit();
    }
    
    /**
     * 
     */
    private void initPlanes() {
        this._csg_plane = PolygonUtil.planeOf(getVertices3d());
        this.plane = eu.mihosoft.vvecmath.Plane.
                fromPointAndNormal(centroid(), _csg_plane.normal);
    }

    /**
     * 
     * @return 
     */
    public List<Vector3d> getVertices3d() {
        return vertices.stream()
                .map(vert -> vert.pos)
                .collect(Collectors.toList());
    }
    
    public List<Vertex> getVertices() {
        return vertices;
    }

    private void validateAndInit() {
        vertices.forEach(v -> {
            v.normal = _csg_plane.normal;
        });

        if (vertices.size() < 3) {

            valid = false;

        } else if (Vector3d.ZERO.equals(_csg_plane.normal)) {

            valid = false;
        } else if (Double.isNaN(_csg_plane.normal.getX())
                || Double.isInfinite(_csg_plane.normal.getX())) {

            valid = false;
        }

        if (!valid) {

            return;
        }

        if (vertices.size() == 3) {

            A = vertices.get(0).pos;
            B = vertices.get(1).pos;
            C = vertices.get(2).pos;

            v0 = C.minus(A);
            v1 = B.minus(A);

            dot00 = v0.dot(v0);
            dot01 = v0.dot(v1);
            dot11 = v1.dot(v1);

            // compute barycentric coordinates
            denom = dot00 * dot11 - dot01 * dot01;
        }
    }

    /**
     * Constructor. Creates a new polygon that consists of the specified
     * vertices.
     *
     * <b>Note:</b> the vertices used to initialize a polygon must be coplanar
     * and form a convex loop.
     *
     * @param vertices polygon vertices
     */
    public Polygon(List<Vertex> vertices) {
        this.vertices = vertices;
        this.holes = new ArrayList<>();
        initPlanes();
        validateAndInit();
     }

    /**
     * Constructor. Creates a new polygon that consists of the specified
     * vertices.
     *
     * <b>Note:</b> the vertices used to initialize a polygon must be coplanar
     * and form a convex loop.
     *
     * @param vertices polygon vertices
     *
     */
    public Polygon(Vertex... vertices) {
        this(Arrays.asList(vertices));
    }

    @Override
    public Polygon clone() {
        List<Vertex> newVertices = new ArrayList<>();
        this.vertices.forEach((vertex) -> {
            newVertices.add(vertex.clone());
        });
        return new Polygon(newVertices, getStorage());
    }

    /**
     * Flips this polygon.
     *
     * @return this polygon
     */
    public Polygon flip() {
        vertices.forEach((vertex) -> {
            vertex.flip();
        });
        Collections.reverse(vertices);

        _csg_plane.flip();

        validateAndInit();
        clearBounds();
        clearExtrema();

        return this;
    }

    /**
     * Returns a flipped copy of this polygon.
     *
     * <b>Note:</b> this polygon is not modified.
     *
     * @return a flipped copy of this polygon
     */
    public Polygon flipped() {
        return clone().flip();
    }

    /**
     * Returns this polygon in STL string format.
     *
     * @return this polygon in STL string format
     */
    public String toStlString() {
        return toStlString(new StringBuilder()).toString();
    }

    /**
     * Returns this polygon in STL string format.
     *
     * @param sb string builder
     *
     * @return the specified string builder
     */
    public StringBuilder toStlString(StringBuilder sb) {

        if (this.vertices.size() >= 3) {

            // TODO: improve the triangulation?
            //
            // STL requires triangular polygons.
            // If our polygon has more vertices, create
            // multiple triangles:
            String firstVertexStl = this.vertices.get(0).toStlString();
            for (int i = 0; i < this.vertices.size() - 2; i++) {
                sb.
                        append("  facet normal ").append(this._csg_plane.normal.toStlString()).append("\n").
                        append("    outer loop\n").
                        append("      ").append(firstVertexStl).append("\n").
                        append("      ");
                this.vertices.get(i + 1).toStlString(sb).append("\n").
                        append("      ");
                this.vertices.get(i + 2).toStlString(sb).append("\n").
                        append("    endloop\n").
                        append("  endfacet\n");
            }
        }

        return sb;
    }
    
    /**
     * Returns a triangulated version of this polygon.
     *
     * @return triangles
     */
    public List<Polygon> toTriangles() {

        boolean cw = !Extrude.isCCW(this);
        
        List<Polygon> result = new ArrayList<>();

        if (this.vertices.size() >= 3) {

            double[] points = new double[vertices.size() * 3];
            for (int i = 0; i < vertices.size(); i++) {
                Vector3d pos = vertices.get(i).pos;
                points[3 * i] = pos.x();
                points[3 * i + 1] = pos.y();
                points[3 * i + 2] = pos.z();
            }

            List<Integer> triangles = Earcut.earcut(points, null, 3);

            for (int i = 0; i < triangles.size(); i+=3) {
                Integer idx0 = triangles.get(i);
                Integer idx1 = triangles.get(i + 1);
                Integer idx2 = triangles.get(i + 2);

                // Create triangle
                Polygon polygon = Polygon.fromPoints(
                        vertices.get(idx0).pos,
                        vertices.get(idx1).pos,
                        vertices.get(idx2).pos);
                if (cw) {
                    polygon.flip();
                }
                result.add(polygon);
            }
        }

        return result;
    }
    
    /**
     * Returns a triangulated version of this polygon.
     *
     * @return triangles
     */
    public List<Polygon> toTriangles2() {

        boolean cw = !Extrude.isCCW(this);
        
        List<Polygon> result = new ArrayList<>();

        if (this.vertices.size() >= 3) {

            List<Vertex> holeVerts = holes.stream()
                    .flatMap(hole -> hole.getVertices().stream())
                    .collect(Collectors.toList());
            
            int numOuterVerts = vertices.size();
            int numHoleVerts = holeVerts.size();
            int totalNumVerts = numHoleVerts + numOuterVerts;
            
            double[] points = new double[totalNumVerts * 3];
            for (int i = 0; i < numOuterVerts; i++) {
                Vector3d pos = vertices.get(i).pos;
                points[3 * i] = pos.x();
                points[3 * i + 1] = pos.y();
                points[3 * i + 2] = pos.z();
            }
            
            int numHoles = holes.size();
            int[] holeIndices = new int[numHoles];

            int startIdx = numOuterVerts;
            for (int holeIdx = 0; holeIdx < numHoles; holeIdx++) {
                holeIndices[holeIdx] = startIdx;
                startIdx = addHoleToData(points, startIdx, holeIdx);
            }

            List<Integer> triangles = Earcut.earcut(points, holeIndices, 3);

            List<Vector3d> dataVectors = getDataVectors(points);
            
            for (int i = 0; i < triangles.size(); i+=3) {
                Integer idx0 = triangles.get(i);
                Integer idx1 = triangles.get(i + 1);
                Integer idx2 = triangles.get(i + 2);

                // Create triangle
                Polygon polygon = Polygon.fromPoints(
                        dataVectors.get(idx0),
                        dataVectors.get(idx1),
                        dataVectors.get(idx2));
                if (cw) {
                    polygon.flip();
                }
                
                result.add(polygon);
            }
        }

        return result;
    }

    /**
     * Adds the hole data to the array of points and passes back the 
     * next starting position.
     * 
     * @param points
     * @param startIdx
     * @param holeIdx
     * @return 
     */
    private int addHoleToData(
            double[] points, 
            int startIdx,
            int holeIdx) {
        
        Polygon hole = holes.get(holeIdx);
        List<Vertex> holeVerts = hole.getVertices();

        int j;
        for (int i = 0; i < holeVerts.size(); i++) {
            Vector3d pos = holeVerts.get(i).pos;

            j = startIdx + i;

            points[3 * j] = pos.x();
            points[3 * j + 1] = pos.y();
            points[3 * j + 2] = pos.z();
        }

        return startIdx + holeVerts.size();
    }
    
    /**
     * 
     * @param points
     * @return 
     */
    private List<Vector3d> getDataVectors(double[] points) {
        
        List<Vector3d> dataVectors = new ArrayList<>();
        
        for (int i = 0; i < points.length; i += 3) {
            dataVectors.add(Vector3d.xyz(
                    points[i], 
                    points[i + 1], 
                    points[i + 2]));
        }
        
        return dataVectors;
    }
    
    /**
     * Signed area. Simplified greens theorem.
     * 
     * @return 
     */
    public double area() {
        double area = 0;
        for (int i = 1; i <= vertices.size(); ++i) {
            area += vertices.get(i).pos.x() * (vertices.get(i + 1).pos.y() - vertices.get(i - 1).pos.y());
        }
        return area;
    }

    /**
     * Translates this polygon.
     *
     * @param v the vector that defines the translation
     * @return this polygon
     */
    public Polygon translate(Vector3d v) {
        
        vertices.forEach((vertex) -> {
            vertex.pos = vertex.pos.plus(v);
        });

        Plane tempPlane = PolygonUtil.planeOf(getVertices3d());
        this._csg_plane.dist = tempPlane.dist;
        this._csg_plane.normal = tempPlane.normal;

        this.plane = eu.mihosoft.vvecmath.Plane.
                fromPointAndNormal(centroid(), _csg_plane.normal);

        validateAndInit();
        clearBounds();
        clearExtrema();

        return this;
    }

    /**
     * Returns a translated copy of this polygon.
     *
     * <b>Note:</b> this polygon is not modified
     *
     * @param v the vector that defines the translation
     *
     * @return a translated copy of this polygon
     */
    public Polygon translated(Vector3d v) {
        return clone().translate(v);
    }

    /**
     * Applies the specified transformation to this polygon.
     *
     * <b>Note:</b> if the applied transformation performs a mirror operation
     * the vertex order of this polygon is reversed.
     *
     * @param transform the transformation to apply
     *
     * @return this polygon
     */
    public Polygon transform(Transform transform) {

        this.vertices.forEach(v -> v.transform(transform));

        Vector3d a = this.vertices.get(0).pos;
        Vector3d b = this.vertices.get(1).pos;
        Vector3d c = this.vertices.get(2).pos;

        this._csg_plane.normal = b.minus(a).crossed(c.minus(a)).normalized();
        this._csg_plane.dist = this._csg_plane.normal.dot(a);

        this.plane = eu.mihosoft.vvecmath.Plane.
                fromPointAndNormal(centroid(), _csg_plane.normal);

        vertices.forEach((vertex) -> {
            vertex.normal = plane.getNormal();
        });

        if (transform.isMirror()) {
            // the transformation includes mirroring. flip polygon
            flip();

        }

        validateAndInit();
        clearBounds();
        clearExtrema();

        return this;
    }

    /**
     * Returns a transformed copy of this polygon.
     *
     * <b>Note:</b> if the applied transformation performs a mirror operation
     * the vertex order of this polygon is reversed.
     *
     * <b>Note:</b> this polygon is not modified
     *
     * @param transform the transformation to apply
     * @return a transformed copy of this polygon
     */
    public Polygon transformed(Transform transform) {
        return clone().transform(transform);
    }

    /**
     * Creates a polygon from the specified point list.
     *
     * @param points the points that define the polygon
     * @param shared shared property storage
     * @return a polygon defined by the specified point list
     */
    public static Polygon fromPoints(List<? extends Vector3d> points,
            PropertyStorage shared) {
        return fromPoints(points, shared, null);
    }

    /**
     * Creates a polygon from the specified point list.
     *
     * @param points the points that define the polygon
     * @return a polygon defined by the specified point list
     */
    public static Polygon fromPoints(List<? extends Vector3d> points) {
        return fromPoints(points, new PropertyStorage(), null);
    }

    /**
     * Creates a polygon from the specified points.
     *
     * @param points the points that define the polygon
     * @return a polygon defined by the specified point list
     */
    public static Polygon fromPoints(Vector3d... points) {
        return fromPoints(Arrays.asList(points), new PropertyStorage(), null);
    }

    /**
     * Creates a polygon from the specified point list.
     *
     * @param points the points that define the polygon
     * @param shared
     * @param plane may be null
     * @return a polygon defined by the specified point list
     */
    public static Polygon fromPoints(
            List<? extends Vector3d> points, PropertyStorage shared, Plane plane) {

        Vector3d normal
                = (plane != null) ? plane.normal.clone() : null;

        if (normal == null) {
            normal = PolygonUtil.normalOf(points);
        }

        List<Vertex> vertices = new ArrayList<>();

        for (Vector3d p : points) {
            Vector3d vec = p.clone();
            Vertex vertex = new Vertex(vec, normal);
            vertices.add(vertex);
        }

        return new Polygon(vertices, shared);
    }
    
    /**
     * 
     * @return 
     */
    public Bounds getBounds() {
        
        if (null == bounds) {
            bounds = calcBounds();
        }
        
        return bounds;
    }

    public Vector3d centroid() {
        Vector3d sum = Vector3d.zero();

        for (Vertex v : vertices) {
            sum = sum.plus(v.pos);
        }

        return sum.times(1.0 / vertices.size());
    }

    /**
     * 
     */
    public void clearBounds() {
        this.bounds = null;
    }

    /**
     * Indicates whether the specified point is contained within this polygon.
     *
     * @param p point
     * @param precision
     * @return {@code true} if the point is inside the polygon or on one of the
     * edges; {@code false} otherwise
     */
    public boolean contains(Vector3d p, double precision) {
        // P not on the plane
        if (plane.distance(p) > precision) {
            return false;
        }

        double precisionSq = precision * precision;
        // if P is on one of the vertices, return true
        if (vertices.stream()
                .anyMatch(vector -> p.minus(vector.pos).magnitudeSq() < precisionSq)) {

            return true;
        }

        // if P is on the plane, we proceed with projection to XY plane
        //
        // P1--P------P2
        //     ^
        //     |
        // P is on the segment if( dist(P1,P) + dist(P2,P) - dist(P1,P2) < TOL)
        for (int i = 0; i < vertices.size(); i++) {

            Vector3d p1 = vertices.get(i).pos;
            Vector3d p2 = vertices.get((i + 1) % vertices.size()).pos;

            boolean onASegment = p1.minus(p).magnitude()
                    + p2.minus(p).magnitude()
                    - p1.minus(p2).magnitude() < precision;

            if (onASegment) {
                return true;
            }
        }

        return vertices.size() == 3
                ? containsByTri(p)
                : containsByNonTri(p);
    }
    
    /**
     * Returns the bounds of this polygon.
     *
     * @return bouds of this polygon
     */
    public Bounds calcBounds() {
        
        return JCSGExtrema3D.calcVertexBounds(vertices.stream());
    }
    
    /**
     * 
     */
    public void clearExtrema() {
        this.extrema = null;
    }
    
    /**
     * 
     * @return 
     */
    public JCSGExtrema3D getExtrema() {
        
        if (null == extrema) {
            extrema = calcExtrema();
        }
        
        return extrema;
    }
    
    /**
     * Returns the bounds of this polygon.
     *
     * @return bouds of this polygon
     */
    public JCSGExtrema3D calcExtrema() {
        
        return JCSGExtrema3D.calcVertexExtrema(vertices.stream());
    }

    protected boolean containsByTri(Vector3d p) {
        Vector3d v2 = p.minus(A);

        double dot02 = v0.dot(v2);
        double dot12 = v1.dot(v2);

        // compute barycentric coordinates
        double u = (dot11 * dot02 - dot01 * dot12);
        double v = (dot00 * dot12 - dot01 * dot02);

        return (u >= 0) && (v >= 0) && (u + v < denom);
    }

    private boolean containsByNonTri(Vector3d p){
        int coordIndex1 = 0;
        int coordIndex2 = 1;

        boolean orthogonalToXY = Math.abs(eu.mihosoft.vvecmath.Plane.XY_PLANE.getNormal()
                .dot(plane.getNormal())) < Plane.EPSILON;

        boolean foundProjectionPlane = false;
        if (!orthogonalToXY && !foundProjectionPlane) {
            coordIndex1 = 0;
            coordIndex2 = 1;
            foundProjectionPlane = true;
        }

        boolean orthogonalToXZ = Math.abs(eu.mihosoft.vvecmath.Plane.XZ_PLANE.getNormal()
                .dot(plane.getNormal())) < Plane.EPSILON;

        if (!orthogonalToXZ && !foundProjectionPlane) {
            coordIndex1 = 0;
            coordIndex2 = 2;
            foundProjectionPlane = true;
        }

        boolean orthogonalToYZ = Math.abs(eu.mihosoft.vvecmath.Plane.YZ_PLANE.getNormal()
                .dot(plane.getNormal())) < Plane.EPSILON;

        if (!orthogonalToYZ && !foundProjectionPlane) {
            coordIndex1 = 1;
            coordIndex2 = 2;
            foundProjectionPlane = true;
        }

        // see from http://www.java-gaming.org/index.php?topic=26013.0
        // see http://alienryderflex.com/polygon/
        // see http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
        int i, j = vertices.size() - 1;
        boolean oddNodes = false;
        double x = p.get(coordIndex1);
        double y = p.get(coordIndex2);
        for (i = 0; i < vertices.size(); i++) {
            double xi = vertices.get(i).pos.get(coordIndex1);
            double yi = vertices.get(i).pos.get(coordIndex2);
            double xj = vertices.get(j).pos.get(coordIndex1);
            double yj = vertices.get(j).pos.get(coordIndex2);
            if ((yi < y && yj >= y
                    || yj < y && yi >= y)
                    && (xi <= x || xj <= x)) {
                oddNodes ^= (xi + (y - yi) / (yj - yi) * (xj - xi) < x);
            }
            j = i;
        }
        return oddNodes;
    }

    /**
     * Indicates whether the specified point is contained within this polygon.
     *
     * @param p point
     * @return {@code true} if the point is inside the polygon or on one of the
     * edges; {@code false} otherwise
     */
    public boolean contains(Vector3d p) {
        return contains(p, Plane.EPSILON);
    }

    /**
     * 
     * @param p
     * @return
     */
    public boolean intersects(Polygon p) {

        return p.vertices
                .stream()
                .anyMatch((v) -> (contains(v.pos)));
    }

    /**
     *
     * @param p
     * @return
     */
    public boolean contains(Polygon p) {

        return p.vertices
                .stream()
                .allMatch((v) -> (contains(v.pos)));
    }

    /**
     * @return the shared
     */
    public PropertyStorage getStorage() {

        if (shared == null) {
            shared = new PropertyStorage();
        }

        return shared;
    }

    public static void main(String[] args) {
        Vector3d a = Vector3d.xyz(0, -0, 0);
        Vector3d b = Vector3d.xyz(0, 5, 0);
        Vector3d c = Vector3d.xyz(5, 5, 0);
        Vector3d d = Vector3d.xyz(5, 0, 0);

        Polygon polygon = Polygon.fromPoints(a, b, c, d);

        System.out.println(polygon._csg_plane.normal);
        
        List<Polygon> triangles = polygon.toTriangles();
        
        triangles.forEach(tri -> {
        
            System.out.println(tri._csg_plane.normal);
        });
        
        System.out.println("==================");
        Polygon polygonFlipped = polygon.flipped();

        System.out.println(polygonFlipped._csg_plane.normal);
        
        List<Polygon> trianglesFliped = polygonFlipped.toTriangles();
        
        trianglesFliped.forEach(tri -> {
        
            System.out.println(tri._csg_plane.normal);
        });
    }

    /**
     * 
     */
    public void reverse() {
        Collections.reverse(vertices);
        initPlanes();
        validateAndInit();
        clearBounds();
        clearExtrema();
    }

    /**
     * 
     * @return 
     */
    public List<Polygon> getHoles() {
        return holes;
    }
    
    /**
     * 
     * @param hole 
     */
    public void addHole(Polygon hole) {
        if (null != hole) {
            holes.add(hole);
        }
    }
}
