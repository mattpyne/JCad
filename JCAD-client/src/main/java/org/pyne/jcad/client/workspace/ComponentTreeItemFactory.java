package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.*;

/**
 *
 * @author Matthew Pyne
 */
public class ComponentTreeItemFactory {

    public static ComponentTreeItem create(IComponent component) {

        if (component instanceof Constructor) {

            return new ConstructorTreeItem((Constructor) component);
        } else if (component instanceof Material) {

            return new MaterialTreeItem((Material) component);
        } else if (component instanceof Identification) {

            return new IdentificationTreeItem((Identification) component);
        } else if (component instanceof Sketches) {

            return new SketchesTreeItem((Sketches) component);
        } else if (component instanceof Connections) {
            ConnectionsTreeItem treeItem = new ConnectionsTreeItem((Connections) component);

            for (Connection connection : ((Connections)component)) {
                treeItem.getChildren().add(new EntityTreeItem(connection.getChild()));
            }

            return treeItem;
        }

        return new ComponentTreeItem(component);
    }
}
