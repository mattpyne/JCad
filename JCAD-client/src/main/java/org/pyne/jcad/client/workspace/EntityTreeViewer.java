package org.pyne.jcad.client.workspace;

import javafx.scene.control.Button;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.ClientDocumentAPI;
import org.pyne.jcad.client.ClientInjectorImp;

import javax.inject.Inject;

/**
 *
 * @author Matthew Pyne
 */
@Plugin(name = "EntityTreeViewer")
public class EntityTreeViewer extends BorderPane {

    public EntityTreeController controller;
    public TreeView<String> treeView;
    public Button newEntity;

    @Inject
    public EntityTreeViewer(ClientInjectorImp clientInjector, ClientDocumentAPI documentAPI) {
        super();
        newEntity = new Button("New Entity");

        // Add button for adding a new Entity
        setTop(newEntity);

        treeView = new TreeView<>();
        treeView.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        setCenter(treeView);

        setId("EntityTreeViewer");
        setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        clientInjector.addToLeftPane(this);
        
        controller = new EntityTreeController(this, documentAPI);
    }

    void clear() {
        getChildren().clear();
    }

    public TreeView<String> treeView() {
        return treeView;
    }

    public Button newEntityButton() {
        return newEntity;
    }

    public EntityTreeController controller() {
        return controller;
    }
}
