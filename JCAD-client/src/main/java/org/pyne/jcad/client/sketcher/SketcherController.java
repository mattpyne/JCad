package org.pyne.jcad.client.sketcher;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;
import org.pyne.jcad.client.ClientDocumentAPI;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.viewer.*;
import org.pyne.jcad.client.workspace.EntityTreeViewer;
import org.pyne.jcad.core.entity.Sketch;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.PlanarSurface;

/**
 * @author Matthew Pyne
 */
public class SketcherController {

    private final Sketcher sketcher;
    private final ClientDocumentAPI documentAPI;

    private final SketchCircleBehavior circleDrawBehavior;
    private final SketchLineBehavior lineDrawBehavior;
    private final EventHandler<MouseEvent> pickSketchArea;
    private boolean creatingSketch;

    private SketchView activeSketchView;
    private EntityView entityView;
    private SurfaceView viewPlaneXY;
    private SurfaceView viewPlaneXZ;
    private SurfaceView viewPlaneYZ;

    public SketcherController(Sketcher sketcher, ClientDocumentAPI documentAPI) {
        this.sketcher = sketcher;
        this.documentAPI = documentAPI;
        this.pickSketchArea = (me) -> onPickSketchArea(me);
        this.circleDrawBehavior = new SketchCircleBehavior(this);
        this.lineDrawBehavior = new SketchLineBehavior(this);
        sketcher.sketchButton().setOnAction(this::onSketchButton);
        attachActionToSketchTools();
    }

    private void attachActionToSketchTools() {
        sketcher.circleSketchButton().setOnAction(e -> {
            ViewerController viewerController = JCadApp.PLUGIN_MANAGER
                    .getPlugin(Viewer.class)
                    .controller();
            circleDrawBehavior.attach(activeSketchView, viewerController.viewer());
        });
        sketcher.lineSketchButton().setOnAction(e -> {
            ViewerController viewerController = JCadApp.PLUGIN_MANAGER
                    .getPlugin(Viewer.class)
                    .controller();
            lineDrawBehavior.attach(activeSketchView, viewerController.viewer());
        });
    }

    public CurveProjectedView addCurve(Curve curve) {
        activeSketchView.sketch().addCurve(curve);

        return addCurveView(curve);
    }

    public CurveProjectedView addCurveView(Curve curve) {
        CurveProjectedView sketchCurveView = new CurveProjectedView(curve, activeSketchView.surfaceView());
        sketchCurveView.setOverrideColor(Color.BLACK);
        sketchCurveView.repaint();

        Viewer2DMouseBehavior mouseBehavior = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller()
                .mouseBehavior();

        mouseBehavior.makeDraggable(sketchCurveView);
        mouseBehavior.makeSelectable(sketchCurveView);
        mouseBehavior.makeHoverable(sketchCurveView);

        activeSketchView.getChildren().add(sketchCurveView);

        return sketchCurveView;
    }

    private void removeSketchView() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);

        Viewer2D viewer2d = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer2D.class);

        viewer2d.getChildren().remove(activeSketchView);
        viewer.viewModel().getChildren().remove(activeSketchView.surfaceView());
    }

    private void onSketchButton(Event ae) {
        creatingSketch = !creatingSketch;

        if (creatingSketch) {

            startSketcherInstance();
        } else {

            acceptSketcherInstance();
        }
    }

    private void onPickSketchArea(MouseEvent me) {
        PickResult pickResult = me.getPickResult();
        Node pickedNode = pickResult.getIntersectedNode().getParent();

        if (!(pickedNode instanceof ShellView)) {

            return;
        }

        ViewerController viewerController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class)
                .controller();

        ShellView shellView = (ShellView) pickedNode;

        entityView = ViewExplorer.parentViewBy(shellView, EntityView.class)
                .orElse(viewerController.getSubject());

        Point3D intersectionPoint = pickResult.getIntersectedPoint();

        shellView.shell()
                .faces()
                .stream()
                .filter(face -> face.surface().isPointOn(
                        new Point3DC(
                                intersectionPoint.getX(),
                                intersectionPoint.getY(),
                                intersectionPoint.getZ())))
                .findAny()
                .ifPresent(face -> setSketchViewNoPaint(new Sketch(face.surface())));

        removeDefaultSketchPlanes();
        removePickSketchAreaHandler();
    }

    private void orientateTo(BrepSurface surface) {
        Point3DC temp = surface.normal(Point3DC.ZERO);

        Point3D normal = new Point3D(temp.x(), temp.y(), temp.z());

        double rot = normal.angle(0, 0, -1);
        Point3D rotAxis = normal.add(0, 0, -1).magnitude() < 0.0001
                ? new Point3D(0, 1, 0)
                : normal.crossProduct(0, 0, -1);

        double[] eulerXYZ = CadMath.toEuler(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ(), rot);
        ViewerController viewerController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class)
                .controller();
        Rotate rotX = viewerController.rotateX();
        Rotate rotY = viewerController.rotateY();
        Rotate rotZ = viewerController.rotateZ();

        Viewer2DController view2dController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller();

        new Timeline(60,
                new KeyFrame(Duration.seconds(0.5),
                        (e) -> view2dController.repaintView(),
                        new KeyValue(rotX.angleProperty(), eulerXYZ[0]),
                        new KeyValue(rotY.angleProperty(), eulerXYZ[1]),
                        new KeyValue(rotZ.angleProperty(), eulerXYZ[2])))
                .play();
    }

    private void setActiveSketchView(Sketch sketch) {
        setSketchViewNoPaint(sketch);

        repaintView2D();
    }

    private void repaintView2D() {
        Viewer2DController view2dController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller();
        view2dController.repaintView();
    }

    public void showBasePlanes() {
        Viewer viewerController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class);
        Viewer2DController view2dController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller();

        PlanarSurface planeXY = new PlanarSurface(Point3DC.Z_ONE, 1.);
        planeXY.domainU().min = -2000;
        planeXY.domainU().max = 2000;
        planeXY.domainV().min = -2000;
        planeXY.domainV().max = 2000;
        BrepSurface brepPlaneXY = new BrepSurface(planeXY);
        viewPlaneXY = new SurfaceView(brepPlaneXY);
        view2dController.view().getChildren().add(viewPlaneXY);
        viewerController.viewModel().getChildren().add(viewPlaneXY);

        PlanarSurface planeXZ = new PlanarSurface(Point3DC.Y_ONE, 1.);
        planeXZ.domainU().min = -2000;
        planeXZ.domainU().max = 2000;
        planeXZ.domainV().min = -2000;
        planeXZ.domainV().max = 2000;
        BrepSurface brepPlaneXZ = new BrepSurface(planeXZ);
        viewPlaneXZ = new SurfaceView(brepPlaneXZ);
        view2dController.view().getChildren().add(viewPlaneXZ);
        viewerController.viewModel().getChildren().add(viewPlaneXZ);

        PlanarSurface planeYZ = new PlanarSurface(Point3DC.X_ONE, 1.);
        planeYZ.domainU().min = -2000;
        planeYZ.domainU().max = 2000;
        planeYZ.domainV().min = -2000;
        planeYZ.domainV().max = 2000;
        BrepSurface brepPlaneYZ = new BrepSurface(planeYZ);
        viewPlaneYZ = new SurfaceView(new BrepSurface(brepPlaneYZ));
        view2dController.view().getChildren().add(new SurfaceView(new BrepSurface(planeYZ)));
        viewerController.viewModel().getChildren().add(viewPlaneYZ);

        view2dController.repaintView();
    }

    protected void setSketchViewNoPaint(Sketch sketch) {

        if (null != activeSketchView) {
            clearSketchView();
        }

        orientateTo(sketch.surface());

        Viewer2DController view2dController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller();
        Viewer viewerController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class);
        activeSketchView = new SketchView(sketch);
        view2dController.view().getChildren().add(activeSketchView);
        viewerController.viewModel().getChildren().add(activeSketchView.surfaceView());
        sketch.curves().forEach(this::addCurveView);

    }

    private void clearSketchView() {
        Viewer2DController view2dController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller();
        Viewer viewerController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class);

        view2dController.view().getChildren().remove(activeSketchView);
        viewerController.viewModel().getChildren().remove(activeSketchView.surfaceView());
    }

    private void removePickSketchAreaHandler() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);
        viewer.viewModel().removeEventHandler(MouseEvent.MOUSE_CLICKED, pickSketchArea);
    }

    private void addPickSketchAreaHandler() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);
        viewer.viewModel().addEventHandler(MouseEvent.MOUSE_CLICKED, pickSketchArea);
    }

    /**
     * Reset Sketcher. Handle any newly added Sketches. Enable Viewer behavior.
     */
    private void acceptSketcherInstance() {

        ViewerController viewerController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class)
                .controller();

        viewerController.mouseBehavior().enable(true);
        viewerController.scrollBehavior().enable(true);

        if (null != activeSketchView
                && !activeSketchView.sketch().curves().isEmpty()) {

            JCadApp.PLUGIN_MANAGER.getPlugin(EntityTreeViewer.class)
                    .controller()
                    .addSketch(entityView.entity(), activeSketchView.sketch());
        }

        if (null != activeSketchView
                && activeSketchView.sketch().curves().isEmpty()) {

            removeSketchView();
        }

        removeDefaultSketchPlanes();

        removePickSketchAreaHandler();

        sketcher.displayNotCreatingSketch();
    }

    private void removeDefaultSketchPlanes() {
        if (null != viewPlaneXY) {

            removeViewPlaneXY();
        }

        if (null != viewPlaneXZ) {

            removeViewPlaneXZ();
        }

        if (null != viewPlaneYZ) {

            removeViewPlaneYZ();
        }
    }

    private void removeViewPlaneYZ() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);

        Viewer2D viewer2d = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer2D.class);

        viewer2d.getChildren().remove(viewPlaneYZ);
        viewer.viewModel().getChildren().remove(viewPlaneYZ);
    }

    private void removeViewPlaneXZ() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);

        Viewer2D viewer2d = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer2D.class);

        viewer2d.getChildren().remove(viewPlaneXZ);
        viewer.viewModel().getChildren().remove(viewPlaneXZ);
    }

    private void removeViewPlaneXY() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);

        Viewer2D viewer2d = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer2D.class);

        viewer2d.getChildren().remove(viewPlaneXY);
        viewer.viewModel().getChildren().remove(viewPlaneXY);
    }

    /**
     * Start Sketcher. Disable viewer behavior.
     */
    private void startSketcherInstance() {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);

        viewer.controller().mouseBehavior().enable(false);

        showBasePlanes();

        addPickSketchAreaHandler();

        sketcher.displayCreatingSketch();
    }

    public void startSketcherInstance(Sketch sketch) {
        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);

        viewer.controller().mouseBehavior().enable(false);

        sketcher.displayCreatingSketch();

        setActiveSketchView(sketch);
    }

}
