package org.pyne.jcad.client.tool;

import goryachev.fx.HPane;
import goryachev.fxdock.FxDockPane;

/**
 *
 * @author Matthew
 */
public class FxDockHPane extends FxDockPane {

    private static int HPANE = 0;

    HPane hPane;

    public FxDockHPane() {
        super("HPane " + HPANE++);
        hPane = new HPane();

        setClosable(false);
        setContent(hPane);
    }

    public void addPane(FxDockPane pane) {
        hPane.add(pane);
    }
}
