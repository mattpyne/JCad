package org.pyne.jcad.client.viewer;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.transform.Affine;
import javafx.scene.transform.MatrixType;
import org.pyne.jcad.core.entity.Entity;
import org.pyne.jcad.core.entity.EntityExplorer;
import org.pyne.jcad.core.entity.Geometry;
import org.pyne.jcad.core.entity.Material;
import org.pyne.jcad.core.entity.Transformation;

/**
 * @author Matthew Pyne
 */
public class EntityView extends View {

    private final Entity entity;

    EntityView(Entity entity) {
        super();
        this.entity = entity;

        refresh();
    }

    public final void refresh() {

        getChildren().clear();

        refreshChildren();

        refreshGeometry();

        refreshColor();

        refreshTransforms();
    }

    public final void refreshChildren() {

        new EntityExplorer().directChildren(entity)
                .stream()
                .forEach(child -> getChildren().add(new EntityView(child)));
    }

    public final void refreshTransforms() {
        getTransforms().clear();

        entity.getComponent(Transformation.class).ifPresent(transform -> {

            getTransforms().add(
                    new Affine(transform.getAbsoluteTransform().toArray1D(),
                            MatrixType.MT_3D_3x4,
                            0));
        });
    }

    public final void refreshColor() {
        entity.getComponent(Material.class).ifPresent(material -> {

            nodeBy(View.class)
                    .forEach(view -> view.setMaterial(
                            new PhongMaterial(
                                    new Color(
                                            material.getColor().r,
                                            material.getColor().g,
                                            material.getColor().b,
                                            material.getColor().a))));
        });
    }

    public final void refreshGeometry() {
        entity.getComponent(Geometry.class).ifPresent(geom -> {

            if (null != geom.getTopo()) {
                getChildren().add(new TopoViewFactory().create(geom.getTopo()));
            }
        });

        refreshColor();
    }

    public Entity entity() {
        return entity;
    }
}
