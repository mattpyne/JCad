package org.pyne.jcad.client.sketcher;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.DepthTest;
import javafx.scene.paint.Color;
import org.pyne.jcad.client.viewer.CurveProjectedView;
import org.pyne.jcad.client.viewer.SurfaceView;
import org.pyne.jcad.client.viewer.View;
import org.pyne.jcad.core.entity.Sketch;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.geom.Curve;

/**
 *
 * @author Matthew Pyne
 */
public class SketchView extends View {

    private final Sketch sketch;
    private final SurfaceView surfaceView;

    public SketchView(Sketch sketch) {
        this.sketch = sketch;

        surfaceView = new SurfaceView(sketch.surface());
        surfaceView.setDepthTest(DepthTest.DISABLE);

        BrepSurface surf = sketch.surface();
        Curve curveUMin = surf.impl().toNurbs().isoCurveAlignU(surf.domainU().min.doubleValue());
        Curve curveUMax = surf.impl().toNurbs().isoCurveAlignU(surf.domainU().max.doubleValue());
        Curve curveVMin = surf.impl().toNurbs().isoCurveAlignV(surf.domainV().min.doubleValue());
        Curve curveVMax = surf.impl().toNurbs().isoCurveAlignV(surf.domainV().max.doubleValue());

        CurveProjectedView c1 = new CurveProjectedView(curveUMin, surfaceView);
        CurveProjectedView c2 = new CurveProjectedView(curveUMax, surfaceView);
        CurveProjectedView c3 = new CurveProjectedView(curveVMin, surfaceView);
        CurveProjectedView c4 = new CurveProjectedView(curveVMax, surfaceView);
        c1.setOverrideColor(Color.BLACK);
        c2.setOverrideColor(Color.BLACK);
        c3.setOverrideColor(Color.BLACK);
        c4.setOverrideColor(Color.BLACK);
        getChildren().add(c1);
        getChildren().add(c2);
        getChildren().add(c3);
        getChildren().add(c4);

    }

    public SurfaceView surfaceView() {
        return surfaceView;
    }

    public Sketch sketch() {
        return sketch;
    }

}
