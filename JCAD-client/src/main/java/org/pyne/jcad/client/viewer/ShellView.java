package org.pyne.jcad.client.viewer;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.MeshView;
import org.pyne.jcad.client.Display;
import org.pyne.jcad.client.FxNodeSearchable;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.TopoExplorer;
import org.pyne.jcad.core.math.brep.creators.BrepTessellator;

import java.util.List;

/**
 *
 * @author Matthew Pyne
 */
public class ShellView extends View implements FxNodeSearchable {

    private final Shell shell;
    private final MeshView mesh;

    /**
     *
     * @param shell
     */
    public ShellView(Shell shell) {
        super();
        this.shell = shell;

        List<List<Point3DC>> surfaceTriangles = BrepTessellator.tessellate(shell);

        mesh = new MeshView(JavaFxTriangles.toJavaFXMeshSimple(surfaceTriangles));
        mesh.setCullFace(CullFace.NONE);

        PhongMaterial material = new PhongMaterial(new Color(0, 0, 0, 0.3));
        mesh.setMaterial(material);
        getChildren().add(mesh);

//        TopoExplorer explorer = new TopoExplorer(shell);
//
//        explorer.faces().forEach(face -> {
//
//            getChildren().add(new FaceView(face));
//        });

//        explorer.edges().forEach(edge -> {
//
//            getChildren().add(new EdgeView(edge));
//        });

    }

    @Override
    public Node getPhysicalNode() {
        return mesh;
    }

    public void setMaterial(PhongMaterial phongMaterial) {
        mesh.setMaterial(phongMaterial);
    }

    public Shell shell() {
        return shell;
    }
}
