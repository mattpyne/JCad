package org.pyne.jcad.client.sketcher;

import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import org.pyne.jcad.client.viewer.ProjectedView;
import org.pyne.jcad.client.viewer.VertexProjectedView;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Vertex;

/**
 *
 * @author Matthew Pyne
 */
public abstract class SketchBehavior {

    protected final SketcherController controller;

    protected SketchView sketchView;
    protected Node eventReciever;

    public SketchBehavior(SketcherController controller) {
        this.controller = controller;
    }

    protected Point3DC getDrawPoint(MouseEvent event) {

        PickResult pickResult = event.getPickResult();

        if (null == pickResult.getIntersectedNode()) {
            return Point3DC.ZERO;
        }

        if (pickResult.getIntersectedNode().getParent() instanceof VertexProjectedView) {

            VertexProjectedView vertexView = (VertexProjectedView) pickResult.getIntersectedNode().getParent();

            Vertex vertex = vertexView.vertex();

            return vertex.point();
        }

        if (pickResult.getIntersectedNode().getParent() instanceof ProjectedView) {

            ProjectedView projectedView = (ProjectedView) pickResult.getIntersectedNode().getParent();

            Point3D scenePt = pickResult.getIntersectedNode().localToScene(event.getPickResult().getIntersectedPoint());
            
            return projectedView.unProject(scenePt);
        }

        Point3D temp = event.getPickResult().getIntersectedPoint();
        return new Point3DC(temp.getX(), temp.getY(), temp.getZ());
    }

    public abstract void attach(SketchView view, Node eventReciever);

    public abstract void detach();
}
