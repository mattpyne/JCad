package org.pyne.jcad.client.viewer;

import java.util.List;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.MeshView;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.creators.BrepTessellator;

/**
 *
 * @author Matthew Pyne
 */
public class SurfaceView extends View {

    private final BrepSurface surf;
    private final MeshView mesh;


    public SurfaceView(BrepSurface surf) {
        this.surf = surf;
        List<List<Point3DC>> surfaceTriangles = BrepTessellator.tessellate(surf);

        mesh = new MeshView(JavaFxTriangles.toJavaFXMeshSimple(surfaceTriangles));

        PhongMaterial material = new PhongMaterial(new Color(0, 0, 0, 0));
        mesh.setMaterial(material);
        getChildren().add(mesh);

    }

    public BrepSurface surface() {
        return surf;
    }

    @Override
    public Node getPhysicalNode() {
        return mesh;
    }

    public void setMaterial(PhongMaterial phongMaterial) {
        mesh.setMaterial(phongMaterial);
    }
}
