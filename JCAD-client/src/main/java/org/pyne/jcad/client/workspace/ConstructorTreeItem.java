package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.Constructor;

/**
 *
 * @author Matthew Pyne
 */
public class ConstructorTreeItem extends ComponentTreeItem {
    
    public ConstructorTreeItem(Constructor construtor) {
        super("Constructor", construtor);
        
        construtor.operations().stream()
                .map(OperationTreeItemFactory::create)
                .forEach(opItem -> getChildren().add(opItem));
    }
}
