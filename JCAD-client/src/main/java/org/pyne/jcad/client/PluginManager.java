package org.pyne.jcad.client;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import org.plugface.core.PluginRef;
import org.plugface.core.PluginSource;
import org.plugface.core.factory.PluginSources;
import java.util.Collection;
import java.util.Objects;
import javax.inject.Inject;
import org.plugface.core.PluginContext;
import org.plugface.core.annotations.Plugin;
import org.plugface.core.impl.DefaultPluginContext;
import org.plugface.core.internal.AnnotationProcessor;
import org.plugface.core.internal.DependencyResolver;
import org.plugface.core.internal.di.MissingDependencyException;
import org.plugface.core.internal.di.Node;
import org.pyne.jcad.client.sculptor.Sculptor;
import org.pyne.jcad.client.viewer.Viewer2D;
import org.pyne.jcad.client.tool.FileHandlingPlugin;
import org.pyne.jcad.client.tool.ModelViewPlugin;
import org.pyne.jcad.client.sketcher.Sketcher;
import org.pyne.jcad.client.viewer.Viewer;
import org.pyne.jcad.client.workspace.EntityTreeViewer;
import org.pyne.jcad.client.workspace.ExtrudeOperationEditor;
import org.pyne.jcad.core.ModelMaker;

public class PluginManager implements org.plugface.core.PluginManager {

    protected final PluginContext context;
    protected final AnnotationProcessor annotationProcessor;
    protected final DependencyResolver dependencyResolver;

    public PluginManager(String pluginsLocation) {

        context = new DefaultPluginContext();
        annotationProcessor = new AnnotationProcessor();
        dependencyResolver = new DependencyResolver(annotationProcessor);

        try {

            loadPlugins(PluginSources.classList(
                    ClientInjectorImp.class,
                    ClientDocumentAPI.class,
                    ModelMaker.class,
                    FileHandlingPlugin.class,
                    ModelViewPlugin.class,
                    Sketcher.class,
                    Sculptor.class,
                    Viewer.class,
                    Viewer2D.class,
                    ExtrudeOperationEditor.class,
                    EntityTreeViewer.class
            ));

        } catch (Exception e) {

            System.err.println("Failed to load default plugins");
            e.printStackTrace();
        }

        try {

            loadPlugins(PluginSources.jarSource(pluginsLocation));
        } catch (Exception e) {

            System.err.println("Failed to load system plugins from " + pluginsLocation);
            e.printStackTrace();
        }
    }

    @Override
    public <T> void register(T plugin) {
        context.addPlugin(plugin);
    }

    @Override
    public <T> void register(String name, T plugin) {
        context.addPlugin(name, plugin);
    }

    @Override
    public <T> T getPlugin(String name) {
        return context.getPlugin(name);
    }

    @Override
    public <T> T getPlugin(Class<T> pluginClass) {
        return context.getPlugin(pluginClass);
    }

    @Override
    public Collection<PluginRef> getAllPlugins() {
        return context.getAllPlugins();
    }

    @Override
    public <T> T removePlugin(String name) {
        return context.removePlugin(name);
    }

    @Override
    public <T> T removePlugin(T plugin) {
        return context.removePlugin(plugin);
    }

    @Override
    public Collection<Object> loadPlugins(PluginSource source) throws Exception {
        final Collection<Class<?>> pluginClassesOrg = Objects.requireNonNull(source, "Plugin Source cannot be null").load();
        final Collection<Class<?>> pluginClasses = Objects.requireNonNull(source, "Plugin Source cannot be null").load();
        final Collection<Object> loaded = new ArrayList<>();

        if (pluginClasses.isEmpty()) {
            return loaded;
        }

        for (Class<?> pluginClass : pluginClassesOrg) {
            if (pluginClass.getAnnotation(Plugin.class) == null) {
                pluginClasses.remove(pluginClass);
                continue;
            }
            if (context.hasPlugin(pluginClass)) {
                pluginClasses.remove(pluginClass);
            }
        }

        Collection<Node<?>> nodes = dependencyResolver.resolve(pluginClasses);

        pluginClasses.stream()
                .filter(pClass -> !hasDeps(pClass, nodes))
                .forEach(pClass -> createPlugin(pClass, loaded));

        createPlugins(nodes, loaded);
        return loaded;
    }

    private boolean hasDeps(Class<?> pClass, Collection<Node<?>> nodes) {
        return nodes.stream()
                .anyMatch(node -> pClass.isAssignableFrom(node.getRefClass()));
    }

    private void createPlugins(Collection<Node<?>> nodes, Collection<Object> loaded) {
        for (Node<?> node : nodes) {
            createPlugin(node, loaded);
        }
    }

    private void createPlugin(Node<?> node, Collection<Object> loaded) {
        final Class<?> refClass = node.getRefClass();
        createPlugin(refClass, loaded);
    }

    private void createPlugin(final Class<?> refClass, Collection<Object> loaded) {
        if (refClass.getAnnotation(Plugin.class) == null) {
            return;
        }
        if (!context.hasPlugin(refClass)) {
            final Object plugin = Objects.requireNonNull(create(refClass), "Could not create plugin of type " + refClass.getName());
            context.addPlugin(plugin);
            loaded.add(plugin);
        }
    }

    protected Object create(Class<?> refClass) {
        final Constructor<?>[] constructors = refClass.getConstructors();
        for (Constructor<?> constructor : constructors) {

            try {
                final Class<?>[] parameterTypes = constructor.getParameterTypes();
                if (parameterTypes.length == 0) {
                    return refClass.newInstance();
                } else {
                    if (constructor.getAnnotation(Inject.class) != null) {
                        final Object[] deps = new Object[parameterTypes.length];
                        for (int i = 0; i < parameterTypes.length; i++) {
                            final Class<?> param = parameterTypes[i];
                            final Object dep = context.getPlugin(param);
                            if (dep != null) {
                                deps[i] = dep;
                            } else {
                                throw new MissingDependencyException("No plugin found for type %s while it is required by %s", param.getName(), refClass.getName());
                            }

                        }
                        return constructor.newInstance(deps);

                    }
                }
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | IllegalArgumentException ignored) {
                ignored.printStackTrace();
            }
        }
        return null;
    }

}
