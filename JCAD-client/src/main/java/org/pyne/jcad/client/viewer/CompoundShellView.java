package org.pyne.jcad.client.viewer;

import org.pyne.jcad.core.math.brep.CompoundShell;

/**
 *
 * @author Matthew Pyne
 */
public class CompoundShellView extends View {

    private final CompoundShell shell;

    public CompoundShellView(CompoundShell shell) {
        this.shell = shell;
        ((CompoundShell) shell).shells()
                .stream()
                .forEach(s -> getChildren().add(new ShellView(s)));
    }

    public CompoundShell getShell() {
        return shell;
    }

}
