package org.pyne.jcad.client.tool;

import goryachev.fx.VPane;
import goryachev.fxdock.FxDockPane;

/**
 *
 * @author Matthew Pyne
 */
public class FxDockVPane extends FxDockPane {
    
    private static int VPANE = 0;
    
    VPane vPane;
    
    public FxDockVPane() {
        super("VPane " + VPANE++);
        vPane = new VPane();

        setClosable(false);
        setContent(vPane);
    }

    public void addPane(FxDockPane pane) {
        vPane.add(pane);
    }
}
