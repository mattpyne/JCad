package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.BaseOperation;

/**
 *
 * @author Matthew Pyne
 */
public class BaseOperationTreeItem extends OperationTreeItem {
    
    
    public BaseOperationTreeItem(BaseOperation operation){
        super("Base Shape", operation);
    }
    
}
