package org.pyne.jcad.client;

import org.pyne.jcad.cadexchange.STLReader;
import org.pyne.jcad.cadexchange.STLWriter;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.brep.Shell;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import org.pyne.jcad.cadexchange.step.reader.StepReader;
import org.pyne.jcad.core.ModelMaker;
import org.pyne.jcad.core.entity.Entity;
import org.pyne.jcad.core.entity.EntityExplorer;
import org.pyne.jcad.core.entity.EntityFactory;
import org.pyne.jcad.core.entity.Geometry;
import org.pyne.jcad.core.math.brep.CompoundShell;

public class DocumentModel {

    private HashSet<DocumentModelChangedListener> documentModelChangedListeners;

    private Entity model;

    public DocumentModel() {
    }

    public void listen(DocumentModelChangedListener listener) {

        if (null == documentModelChangedListeners) {

            documentModelChangedListeners = new HashSet<>();
        }

        documentModelChangedListeners.add(listener);
    }

    public void fireListeners() {

        if (null == documentModelChangedListeners) {

            return;
        }

        for (DocumentModelChangedListener documentModelChangedListener : documentModelChangedListeners) {

            documentModelChangedListener.onModelChanged();
        }
    }

    public void setModel(Entity model) {
        this.model = model;
        fireListeners();
    }

    public void loadData(File file) {
        try {

            EntityFactory entityFactory = JCadApp.PLUGIN_MANAGER
                    .getPlugin(ModelMaker.class)
                    .entityFactory();

            String fName = file.getName().toLowerCase();
            if (fName.endsWith("stl")) {

                model = entityFactory.create(STLReader.read(file));
            } else if (fName.endsWith("stp") | fName.endsWith("step")) {

                model = entityFactory.create(new StepReader().read(file));
            }
            fireListeners();

        } catch (Exception e) {
            Debug.log(e);
        }
    }

    public void saveData(File file) {
        try {

            CompoundShell compoundShell = new CompoundShell();

            new EntityExplorer()
                    .entities(model)
                    .stream()
                    .map(entity -> entity.getComponent(Geometry.class))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(Geometry::getTopo)
                    .filter(topo -> topo instanceof Shell || topo instanceof CompoundShell)
                    .forEach(shell -> {
                        if (shell instanceof CompoundShell) {

                            compoundShell.addAll(((CompoundShell) shell).shells());
                        } else {

                            compoundShell.add((Shell) shell);
                        }
                    });

            STLWriter.write(compoundShell, file);
        } catch (IOException e) {
            Debug.log(e);
        }
    }

    public Entity getModel() {
        return model;
    }
}
