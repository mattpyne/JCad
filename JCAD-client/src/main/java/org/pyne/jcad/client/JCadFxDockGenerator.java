package org.pyne.jcad.client;

import goryachev.fxdock.FxDockFramework;
import goryachev.fxdock.FxDockPane;
import goryachev.fxdock.FxDockWindow;
import javafx.scene.Node;
import org.pyne.jcad.client.tool.LeftToolPane;
import org.pyne.jcad.client.tool.RightToolPane;
import org.pyne.jcad.client.viewer.Viewer;

import java.util.HashMap;
import java.util.Map;

public class JCadFxDockGenerator implements FxDockFramework.Generator {

    public static String VIEWER = "viewer";
    public static String TOP_TOOLS = "top-tools";
    public static String RIGHT_TOOLS = "right-tools";
    public static String LEFT_TOOLS = "left-tools";

    private Map<String, Node> typeToJavaFxScenes = new HashMap();

    public void addFxNode(Node node) {

        typeToJavaFxScenes.put(node.getId(), node);
    }

    @Override
    public FxDockWindow createWindow() {

        return new JCadWindow();
    }

    @Override
    public FxDockPane createPane(String type) {

        if (type.equals(VIEWER)) {

            return JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);
        } else if (type.equals(RIGHT_TOOLS)) {

            return new RightToolPane();
        } else if (type.equals(LEFT_TOOLS)) {

            return new LeftToolPane();
        }

        if (typeToJavaFxScenes.get(type) instanceof FxDockPane) {
            return (FxDockPane) typeToJavaFxScenes.get(type);
        }

        FxDockPane pane = new FxDockPane(type) {};

        pane.setContent(typeToJavaFxScenes.get(type));

        return pane;
    }
}
