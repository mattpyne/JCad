package org.pyne.jcad.client.workspace;

import javafx.scene.control.TreeItem;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.sketcher.Sketcher;
import org.pyne.jcad.client.sketcher.SketcherController;
import org.pyne.jcad.core.entity.Sketch;

/**
 *
 * @author Matthew
 */
public class SketchTreeItem extends TreeItem implements Clickable {

    private Sketch sketch;

    public SketchTreeItem(Sketch sketch) {
        super("Sketch");
        this.sketch = sketch;
    }

    @Override
    public void onDoubleClicked() {
        SketcherController sketcherController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Sketcher.class)
                .controller();

        sketcherController.startSketcherInstance(sketch);
    }

}
