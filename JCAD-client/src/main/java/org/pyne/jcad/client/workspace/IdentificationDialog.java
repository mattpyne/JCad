package org.pyne.jcad.client.workspace;

import java.util.HashMap;
import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.pyne.jcad.core.entity.Identification;

/**
 *
 * @author Matthew Pyne
 */
public class IdentificationDialog extends Dialog {

    public IdentificationDialog(Identification identification) {

        setTitle("Identification");
        setHeaderText("Choose identification properties!");

        // Set the icon (must be included in the project).
//        setGraphic(new ImageView(this.getClass().getResource("login.png").toString()));

        // Set the button types.
        ButtonType acceptButtonType = new ButtonType("Accept", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(acceptButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField displayName = new TextField();
        displayName.setPromptText("Display Name");
        displayName.setText(identification.getDisplayName());

        TextField name = new TextField();
        name.setPromptText("Name");
        name.setText(identification.getName());

        TextField uuid = new TextField();
        uuid.setPromptText("UUID");
        uuid.setText(identification.getUuid());

        grid.add(new Label("Display Name:"), 0, 0);
        grid.add(displayName, 1, 0);
        grid.add(new Label("Name:"), 0, 1);
        grid.add(name, 1, 1);
        grid.add(new Label("UUID:"), 0, 2);
        grid.add(uuid, 1, 2);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> displayName.requestFocus());

        setResultConverter(dialogButton -> {
            if (dialogButton == acceptButtonType) {
                
                HashMap<String, String> result = new HashMap<>();
                result.put("name", name.getText());
                result.put("displayName", displayName.getText());
                result.put("uuid", uuid.getText());
                
                return result;
            }
            return null;
        });

        Optional<HashMap<String, String>> result = showAndWait();

        result.ifPresent(id -> {

            identification.setDisplayName(id.get("displayName"));
            identification.setName(id.get("name"));
            identification.setUuid(id.get("uuid"));
        });
    }
}
