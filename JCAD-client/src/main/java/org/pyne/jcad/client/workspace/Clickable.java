package org.pyne.jcad.client.workspace;

/**
 *
 * @author Matthew Pyne
 */
public interface Clickable {
    
    void onDoubleClicked();
}
