package org.pyne.jcad.client;

import com.pixelduke.control.ribbon.RibbonGroup;
import com.pixelduke.control.ribbon.RibbonTab;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * Used as the API for adding new nodes to the Client.
 */
public interface ClientInjector {

    boolean addToTopTool(RibbonGroup group, String ribbonTabId);

    boolean addToTopToolStart(RibbonGroup group, String ribbonTabId);
    
    boolean addToTopTool(Node node, String ribbonTabId, String ribbonGroupId);
    
    boolean addToTopTool(RibbonTab tab);
    
    /**
     * Add the give node to thr LeftPane.
     *
     * @param node
     * @return
     */
    boolean addToLeftPane(Node node);

    /**
     * Add the given node to the RightPane.
     *
     * @param node
     * @return
     */
    boolean addToRightPane(Node node);

    /**
     * Add the given node to the pane which has the paneId given.
     *
     * @param node
     * @param paneId
     * @return
     */
    boolean addTo(Node node, String paneId);
    
    Node getNode(String id);

    public void addPaneToMainSplitPane(Pane node);
    public void addPaneToMainSplitPane(int idx, Pane node);
}
