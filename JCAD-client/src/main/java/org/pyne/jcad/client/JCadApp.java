package org.pyne.jcad.client;

import goryachev.common.util.GlobalSettings;
import goryachev.common.util.Log;
import goryachev.fxdock.FxDockFramework;
import javafx.application.Application;
import javafx.stage.Stage;
import org.pyne.jcad.core.ModelMaker;
import org.pyne.jcad.core.entity.ExtrudeOperation;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Transform;
import org.pyne.jcad.core.math.brep.*;
import org.pyne.jcad.core.math.brep.creators.ShellPrimitives;
import org.pyne.jcad.core.math.brep.eval.bool.OperationObserver;
import org.pyne.jcad.core.math.brep.eval.bool.ShellBooleanOps;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import org.pyne.jcad.core.math.geom.ParametricCurve;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author matt pyne
 */
public class JCadApp extends Application {

    public static PluginManager PLUGIN_MANAGER;
    public static JCadFxDockGenerator WINDOW_GENERATOR;
    public static JCadWindow WINDOW;
    public static DocumentModel DOCUMENT_MODEL;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // init logger
        Log.initConsole();
        Log.conf("DebugSettingsProvider", true);

        // init non-ui subsystems
        GlobalSettings.setFileProvider(new File("settings.conf"));

        DOCUMENT_MODEL = new DocumentModel();

        launch(JCadApp.class, args);
    }

    @Override
    public void start(Stage primaryStage) {

        WINDOW_GENERATOR = new JCadFxDockGenerator();
        FxDockFramework.setGenerator(WINDOW_GENERATOR);

//        int ct = FxDockFramework.loadLayout();
//        if (ct == 0) {
        // when no saved layout exists, open the first window
        // make sure to call open() instead of show()
        WINDOW = JCadWindow.openDefault();
//        }

        // init plugin system
        PLUGIN_MANAGER = new PluginManager(new File("./").toURI().toString());
//        Shell shell = ShellPrimitives.box(480, 480, 480);

        Shell shell = ShellPrimitives.box(500, 500, 500);
        shell = shell.subtract(ShellPrimitives.cylinder(30, 1000, Transform.rotation(Math.PI / 2,1.0, 0.0, 0.0).combine(Transform.translate(40, 40, 40))));
        shell = shell.subtract(ShellPrimitives.cylinder(30, 1000, Transform.rotation(Math.PI / 2,1.0, 0.0, 0.0).combine(Transform.translate(4, 4, 4))));
        shell = shell.subtract(ShellPrimitives.cylinder(140, 1000, Transform.translate(31, 23., 23.)));

//        Shell shell = ShellPrimitives.cylinder(30, 30, null);
//
////        shell.addFaces(ShellPrimitives.cylinder(30, 30, new Matrix3().translate(20.0, 0, 0)).faces());
//        shell = new ShellBooleanOps().apply(
//                shell,
//                ShellPrimitives.cylinder(30, 30, new Matrix3().translate(20.0, 0, 0)),
//                ShellBooleanOps.TYPE.UNION,
//                null);
//
//        shell = new ShellBooleanOps().apply(
//                shell,
//                ShellPrimitives.cylinder(15, 30, new Matrix3().translate(20.0, 0, 0)),
//                ShellBooleanOps.TYPE.SUBTRACT,
//                null);
//
//        shell = new ShellBooleanOps().apply(
//                shell,
//                ShellPrimitives.cylinder(15, 30, null),
//                ShellBooleanOps.TYPE.SUBTRACT,
//                testing_DisplayFaceEdges());

//        Shell shell = ShellPrimitives.box(100, 100, 100, null);
//
//        for (int i = 0; i < shell.faces().size(); i++) {
//            shell.faces().get(i).setId(i + "A");
//        }
//
////        shell.addFaces(ShellPrimitives.cylinder(10, 10,
////                new Matrix3().translate(0, 0, 50.0)).faces());
//
//        shell = shell.union(ShellPrimitives.cylinder(10, 10,
//                new Matrix3().translate(0, 0, 50.0)));
//
//        Curve circle = CurvePrimitives.circle(
//                Point3DC.xyz(50, 10, 20),
//                Point3DC.Y_ONE,
//                Point3DC.Z_ONE,
//                5);
//
//        List<ParametricCurve> curves = circle.split(0.5);
//
//        Loop loop = new Loop();
//        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
//        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
//        loop.link();
//
//        ExtrudeOperation extrudeOperation = new ExtrudeOperation();
//        extrudeOperation.setAmount(10);
//        extrudeOperation.setDir(Point3DC.X_ONE);
//        extrudeOperation.setLoops(Arrays.asList(loop));
//
//        Shell extrusion = (Shell) extrudeOperation.topoOp();
//
//        for (int i = 0; i < extrusion.faces().size(); i++) {
//            extrusion.faces().get(i).setId(i + "B");
//        }
//
//        shell = shell.union(extrusion);

        try {
            DOCUMENT_MODEL.setModel(JCadApp.PLUGIN_MANAGER
                    .getPlugin(ModelMaker.class)
                    .entityFactory().create(shell));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//
//        try {
//            DOCUMENT_MODEL.loadData(new File("C:\\Users\\mattp\\projects\\JCad\\JCAD-core\\src\\test\\resources\\stepycolors.step"));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    public static OperationObserver testing_DisplayFaceEdges() {
        return face -> {
            Display.display(face.surface().impl(), face.loops(), face.op.newEdges);
        };
    }

}
