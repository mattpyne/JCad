package org.pyne.jcad.client.viewer;

import java.util.List;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import org.pyne.jcad.client.JavaFXInterop;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.Curve;

/**
 *
 * @author Matthew Pyne
 */
public class CurveProjectedView extends ProjectedView {

    private static final Color HOVER_COLOR = Color.DEEPSKYBLUE;
    private static final Color SELECT_COLOR = Color.AQUA;
    private static final Color HIDDEN_COLOR = new Color(0, 0, 0, 0);
    private Color overrideColor;

    private final Polyline curveNode;
    private final Curve curve;

    public CurveProjectedView(Curve curve, View context) {
        super(context);
        this.curve = curve;
        curveNode = new Polyline();
        curveNode.setStrokeWidth(4);
        getChildren().add(curveNode);
    }

    public void setOverrideColor(Color overrideColor) {
        this.overrideColor = overrideColor;
    }

    public Curve curve() {
        return curve;
    }

    @Override
    public void repaint() {
        List<Point3DC> points = curve.tessellate();

        Double[] point2ds = new Double[points.size() * 2];
        for (int i = 0; i < points.size(); i++) {
            Point3DC p3d = points.get(i);
            Point2D p2d = project(p3d);

            point2ds[2 * i] = p2d.getX();
            point2ds[2 * i + 1] = p2d.getY();
        }

        curveNode.getPoints().setAll(point2ds);
        updateCurveColor();
    }

    @Override
    public void setHovered(boolean hovered) {
        super.setHovered(hovered);
        updateCurveColor();
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        updateCurveColor();
    }

    private void updateCurveColor() {
        if (isSelected()) {
            curveNode.setStroke(SELECT_COLOR);
        } else if (isHovered()) {
            curveNode.setStroke(HOVER_COLOR);
        } else if (null != overrideColor) {
            curveNode.setStroke(overrideColor);
        } else {
            curveNode.setStroke(HIDDEN_COLOR);
        }
    }

    @Override
    public void translate(Point3D translation) {
        curve.translated(JavaFXInterop.from(translation));
        
        repaint();
    }

    @Override
    public Node getPhysicalNode() {
        return curveNode;
    }

    public void setFill(Color color) {
        curveNode.setFill(color);
    }

    public void setStroke(Color color) {
        curveNode.setStroke(color);
    }

}
