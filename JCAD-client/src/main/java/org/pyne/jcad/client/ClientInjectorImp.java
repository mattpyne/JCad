package org.pyne.jcad.client;

import com.pixelduke.control.Ribbon;
import com.pixelduke.control.ribbon.RibbonGroup;
import com.pixelduke.control.ribbon.RibbonTab;
import goryachev.fxdock.FxDockFramework;
import goryachev.fxdock.FxDockPane;
import goryachev.fxdock.FxDockWindow;
import goryachev.fxdock.internal.FxDockSplitPane;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import org.pyne.jcad.client.tool.FxDockVPane;
import org.pyne.jcad.client.tool.LeftToolPane;
import org.pyne.jcad.client.tool.RightToolPane;
import java.util.List;
import java.util.stream.Collectors;
import javafx.scene.Parent;
import org.plugface.core.annotations.Plugin;

@Plugin(name = "ClientInjector")
public class ClientInjectorImp implements ClientInjector {

    @Override
    public boolean addToLeftPane(Node node) {

        if (!addTo(node, LeftToolPane.Id())) {

            FxDockPane pane = JCadApp.WINDOW_GENERATOR.createPane(LeftToolPane.Id());
            if (null == pane) {
                return false;
            }

            addPaneToMainSplitPane(0, pane);

            Node main = getNode("main");
            if (null != main && main instanceof FxDockSplitPane) {
                if (null == getNode(RightToolPane.Id())) {

                    ((FxDockSplitPane) main).setDividerPositions(0.15);
                } else {

                    ((FxDockSplitPane) main).setDividerPositions(0.15, 0.85);
                }
            }

            return addTo(node, LeftToolPane.Id());
        }

        return true;
    }

    @Override
    public boolean addToRightPane(Node node) {

        if (!addTo(node, RightToolPane.Id())) {

            FxDockPane pane = JCadApp.WINDOW_GENERATOR.createPane(RightToolPane.Id());
            if (null == pane) {
                return false;
            }

            addPaneToMainSplitPane(((FxDockSplitPane) getNode("main")).getPaneCount() - 1, pane);
            Node main = getNode("main");
            if (null != main && main instanceof FxDockSplitPane) {
                if (null == getNode(LeftToolPane.Id())) {

                    ((FxDockSplitPane) main).setDividerPositions(0.85);
                } else {

                    ((FxDockSplitPane) main).setDividerPositions(0.15, 0.85);
                }
            }
            
            return addTo(node, RightToolPane.Id());
        }

        return true;
    }

    @Override
    public boolean addTo(Node node, String paneId) {

        FxDockWindow window = FxDockFramework.getWindows()
                .stream()
                .filter(w -> w.getTitle().contains(JCadWindow.TITLE))
                .findAny()
                .orElse(null);

        if (null == window) {
            return false;
        }

        Node parent = getNode(paneId);

        if (null == parent) {
            return false;
        }

        JCadApp.WINDOW_GENERATOR.addFxNode(node);

        if (!(parent instanceof FxDockPane)) {
            return false;
        }

        FxDockPane parentPane = (FxDockPane) parent;
        FxDockPane fxDockPane = JCadApp.WINDOW_GENERATOR.createPane(node.getId());

//        fxDockPane.prefWidthProperty().bind(parentPane.widthProperty());
//        fxDockPane.prefHeightProperty().bind(parentPane.heightProperty());

//        if (node instanceof Pane) {
//            Pane nodePane = (Pane) node;
//            nodePane.prefWidthProperty().bind(fxDockPane.widthProperty());
//            nodePane.prefHeightProperty().bind(fxDockPane.heightProperty());
//        }
        if (parentPane instanceof FxDockVPane) {
            FxDockVPane parentVPane = (FxDockVPane) parentPane;
            parentVPane.addPane(fxDockPane);
        } else {
            parentPane.getChildren().add(fxDockPane);
        }

        return true;
    }

    @Override
    public Node getNode(String id) {
        FxDockWindow window = FxDockFramework.getWindows()
                .stream()
                .filter(w -> w.getTitle().contains(JCadWindow.TITLE))
                .findAny()
                .orElse(null);

        if (null == window) {
            return null;
        }

        List<Node> nodes = new ArrayList<>();
        nodes.addAll(getAllNodes(window.getDockRootPane()));
        nodes.addAll(getAllNodes(window.getTop()));
        nodes.addAll(getAllNodes(window.getBottom()));
        nodes.addAll(getAllNodes(window.getLeft()));
        nodes.addAll(getAllNodes(window.getRight()));

        nodes = nodes.stream()
                .filter(n -> null != n)
                .filter(n -> null != n.getId())
                .filter(n -> n.getId().equals(id))
                .collect(Collectors.toList());

        if (nodes.isEmpty()) {
            return null;
        }

        return nodes.get(0);
    }

    public static ArrayList<Node> getAllNodes(Node root) {
        ArrayList<Node> nodes = new ArrayList<>();
        if (root instanceof Parent) {
            addAllDescendents((Parent) root, nodes);
        } else if (null != root) {
            nodes.add(root);
        }
        return nodes;
    }

    private static void addAllDescendents(Parent parent, ArrayList<Node> nodes) {
        for (Node node : parent.getChildrenUnmodifiable()) {
            nodes.add(node);
            if (node instanceof Parent) {
                addAllDescendents((Parent) node, nodes);
            }
        }
    }

    @Override
    public boolean addToTopTool(RibbonGroup group, String ribbonTabId) {
        Node topTool = getNode("TopTool");

        if (!(topTool instanceof Ribbon)) {
            return false;
        }

        Ribbon ribbon = (Ribbon) topTool;

        RibbonTab tab = ribbon.getTabs()
                .stream()
                .filter(t -> t.getId().equals(ribbonTabId))
                .findFirst()
                .orElse(new RibbonTab(ribbonTabId));

        if (null == tab.getContent().getParent()) {

            ribbon.getTabs().add(tab);
            tab.setId(ribbonTabId);
        }

        tab.getRibbonGroups().add(group);

        return true;
    }

    @Override
    public boolean addToTopToolStart(RibbonGroup group, String ribbonTabId) {
        Node topTool = getNode("TopTool");

        if (!(topTool instanceof Ribbon)) {
            return false;
        }

        Ribbon ribbon = (Ribbon) topTool;

        RibbonTab tab = ribbon.getTabs()
                .stream()
                .filter(t -> t.getId().equals(ribbonTabId))
                .findFirst()
                .orElse(new RibbonTab(ribbonTabId));

        if (null == tab.getContent().getParent()) {

            ribbon.getTabs().add(tab);
            tab.setId(ribbonTabId);
        }

        tab.getRibbonGroups().add(0,group);

        return true;
    }

    @Override
    public boolean addToTopTool(Node node, String ribbonTabId, String ribbonGroupId) {
        Node topTool = getNode("TopTool");

        if (!(topTool instanceof Ribbon)) {
            return false;
        }

        Ribbon ribbon = (Ribbon) topTool;

        RibbonTab tab = ribbon.getTabs()
                .stream()
                .filter(t -> t.getId().equals(ribbonTabId))
                .findFirst()
                .orElse(new RibbonTab(ribbonTabId));

        if (null == tab.getContent().getParent()) {

            ribbon.getTabs().add(tab);
            tab.setId(ribbonTabId);
        }

        RibbonGroup group = tab.getRibbonGroups()
                .stream()
                .filter(t -> t.getId().equals(ribbonGroupId))
                .findFirst()
                .orElse(new RibbonGroup());

        if (null == group.getParent()) {

            tab.getRibbonGroups().add(group);
            group.setId(ribbonGroupId);
        }

        group.getNodes().add(node);

        return true;
    }

    @Override
    public boolean addToTopTool(RibbonTab tab) {
        Node topTool = getNode("TopTool");

        if (!(topTool instanceof Ribbon)) {
            return false;
        }

        Ribbon ribbon = (Ribbon) topTool;

        ribbon.getTabs().add(tab);

        return true;
    }

    @Override
    public void addPaneToMainSplitPane(Pane node) {
        FxDockSplitPane main = (FxDockSplitPane) getNode("main");
        if (null != main) {

            main.addPane(node);
        }
    }

    @Override
    public void addPaneToMainSplitPane(int idx, Pane node) {
        FxDockSplitPane main = (FxDockSplitPane) getNode("main");
        if (null != main) {

            main.addPane(idx, node);
        }
    }
}
