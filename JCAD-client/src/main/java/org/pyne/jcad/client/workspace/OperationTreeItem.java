package org.pyne.jcad.client.workspace;

import javafx.scene.control.TreeItem;
import org.pyne.jcad.core.entity.IOperation;

/**
 *
 * @author Matthew Pyne
 */
public class OperationTreeItem extends TreeItem implements Clickable {

    private final IOperation operation;

    public OperationTreeItem(IOperation operation) {
        super(operation.getClass().getSimpleName());
        this.operation = operation;
    }

    public OperationTreeItem(String text, IOperation operation) {
        super(text);
        this.operation = operation;
    }

    public IOperation getOperation() {
        return operation;
    }

    @Override
    public void onDoubleClicked() {
    }

}
