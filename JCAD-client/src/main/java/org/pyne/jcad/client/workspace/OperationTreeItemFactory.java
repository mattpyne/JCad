package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.BaseOperation;
import org.pyne.jcad.core.entity.ExtrudeOperation;
import org.pyne.jcad.core.entity.IOperation;

/**
 *
 * @author Matthew Pyne
 */
public class OperationTreeItemFactory {

    public static OperationTreeItem create(IOperation operation) {

        if (operation instanceof ExtrudeOperation) {

            return new ExtrudeOperationTreeItem((ExtrudeOperation) operation);
        } else if (operation instanceof BaseOperation) {

            return new BaseOperationTreeItem((BaseOperation) operation);
        }

        return new OperationTreeItem(operation);
    }
}
