package org.pyne.jcad.client.viewer;

import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.TopoExplorer;

/**
 *
 * @author Matthew
 */
public class WireShellView extends View {

    private final Shell shell;

    /**
     *
     * @param shell
     */
    public WireShellView(Shell shell) {
        super();
        this.shell = shell;
        
        TopoExplorer explorer = new TopoExplorer(shell);

        explorer.edges().forEach(edge -> {

            getChildren().add(new EdgeView(edge));
        });
    }

    public Shell shell() {
        return shell;
    }
}
