package org.pyne.jcad.client.viewer;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import javafx.scene.transform.Affine;
import org.pyne.jcad.core.math.Axis3;
import org.pyne.jcad.core.math.L3Space;
import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author Matthew
 */
public class Line3D extends Box {

    public Line3D(Point3DC p1, Point3DC p2) {
        super(p1.distance(p2), 0, 0);

        setDrawMode(DrawMode.LINE);
        setMaterial(new PhongMaterial(Color.BLACK));

        Point3DC lineDir = p2.minus(p1).normalized();

        Axis3 basis = L3Space.basisForPlane(lineDir);
        Point3DC a = lineDir;
        Point3DC b = basis.v1();
        Point3DC c = basis.v2();

        Point3DC centerAdj = lineDir.times(getWidth() / 2);
        Point3DC translation = p1.plus(centerAdj);

        getTransforms().add(new Affine(
                a.x(), b.x(), c.x(), translation.x(),
                a.y(), b.y(), c.y(), translation.y(),
                a.z(), b.z(), c.z(), translation.z()));
    }

    void setThickness(double thickness) {
        setHeight(thickness);
        setDepth(thickness);
    }

}
