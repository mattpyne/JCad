package org.pyne.jcad.client.viewer;

import java.util.Timer;
import java.util.TimerTask;

import com.sun.javafx.geom.PickRay;
import com.sun.javafx.geom.Shape;
import com.sun.javafx.geom.Vec3d;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.input.PickResult;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.Shape3D;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.core.math.CadMath;

/**
 *
 * @author matt pyne
 */
public class ViewerScrollBehavior implements EventHandler<ScrollEvent> {

    private final ViewerController controller;
    private boolean enabled;
    private boolean cameraModified;
    private Timer zoomDone;

    /**
     *
     * @param controller
     */
    public ViewerScrollBehavior(ViewerController controller) {
        this.controller = controller;
        this.enabled = true;
    }

    public void enable(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void handle(ScrollEvent event) {

        if (!enabled) {
            return;
        }

        Viewer2DController sketcherController = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller();

        sketcherController.disable();

        if (event.getPickResult().getIntersectedNode() instanceof Shape3D) {
            double zoomFactor = 0.05;
            double deltaY = event.getDeltaY();
            if (deltaY < 0) {
                zoomFactor = -0.05;
            }
            zoom(event.getPickResult(), zoomFactor);
        } else {
            double zoomFactor = -1.05;
            double deltaY = event.getDeltaY();
            if (deltaY < 0) {
                zoomFactor = 1.05;
            }
            zoom(zoomFactor);
        }
        event.consume();

        if (null == zoomDone) {

            zoomDone = new Timer();
        } else {

            zoomDone.cancel();
            zoomDone.purge();
            zoomDone = new Timer();
        }
        zoomDone.schedule(new TimerTask() {
            @Override
            public void run() {
                if (cameraModified) {
                    cameraModified = false;

                    // Avoid throwing IllegalStateException by running from a non-JavaFX thread.
                    Platform.runLater(
                            () -> {
                                sketcherController.enable();

                                sketcherController.repaintView();
                            }
                    );
                }
            }
        }, 500);
    }

    public void zoom(double factor) {
        PerspectiveCamera cam = (PerspectiveCamera) controller.viewer().camera();
        cam.setTranslateZ(cam.getTranslateZ() + factor * 10);

        cameraModified = true;
    }

    public void zoom(PickResult pickResult, double zoom) {
        PerspectiveCamera cam = (PerspectiveCamera) controller.viewer().camera();
        Point3D point = pickResult.getIntersectedNode()
                .getLocalToSceneTransform()
                .transform(pickResult.getIntersectedPoint());

        Point3D vecBetweenIntersectionAndCamera = point
                .subtract(
                        new Point3D(
                                cam.getTranslateX(),
                                cam.getTranslateY(),
                                cam.getTranslateZ()));
        double distanceBetweenCameraAndIntersection = vecBetweenIntersectionAndCamera.magnitude();

        Point3D toMouseDir = vecBetweenIntersectionAndCamera
                .normalize()
                .multiply(zoom * distanceBetweenCameraAndIntersection);

        cam.setTranslateX(cam.getTranslateX() - toMouseDir.getX());
        cam.setTranslateY(cam.getTranslateY() - toMouseDir.getY());
        cam.setTranslateZ(cam.getTranslateZ() - toMouseDir.getZ());

        cameraModified = true;
    }

    public static double clamp(double value, double min, double max) {

        if (Double.compare(value, min) < 0) {
            return min;
        }

        if (Double.compare(value, max) > 0) {
            return max;
        }

        return value;
    }

}
