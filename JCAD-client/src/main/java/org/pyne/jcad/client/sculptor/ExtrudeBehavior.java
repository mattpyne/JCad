package org.pyne.jcad.client.sculptor;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.pyne.jcad.client.ClientInjector;
import org.pyne.jcad.client.ClientInjectorImp;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.workspace.ExtrudeOperationEditor;
import org.pyne.jcad.core.entity.ExtrudeOperation;

/**
 *
 * @author Matthew Pyne
 */
public class ExtrudeBehavior {

    private final SculptorController controller;

    private final EventHandler<MouseEvent> mousePressedHandler;
    private final EventHandler<MouseEvent> mouseReleasedHandler;
    private final EventHandler<MouseEvent> mouseDraggedHandler;
    private final EventHandler<KeyEvent> keyTypedHandler;

    private Node eventReciever;
    private ExtrudeOperation extrude;

    private ExtrudeOperationEditor extrudeEditor;

    public ExtrudeBehavior(SculptorController controller) {
        this.controller = controller;
        this.mousePressedHandler = (MouseEvent event) -> {

        };
        this.mouseReleasedHandler = (MouseEvent event) -> {

        };
        this.mouseDraggedHandler = (MouseEvent event) -> {

        };
        this.keyTypedHandler = (KeyEvent event) -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                detach();
            }
        };
    }

    public void attach(Node eventReciever) {
        extrude = new ExtrudeOperation();
        extrudeEditor = JCadApp.PLUGIN_MANAGER.getPlugin(ExtrudeOperationEditor.class);

        extrudeEditor.setExtrude(extrude);
        extrudeEditor.show();

        extrudeEditor.visibleProperty().addListener((closeRequest) -> {
            if (null == extrudeEditor.getParent()) {
                detach();
            }
        });

        this.eventReciever = eventReciever;

        eventReciever.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
        eventReciever.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
        eventReciever.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedHandler);
        eventReciever.addEventHandler(KeyEvent.KEY_TYPED, keyTypedHandler);
    }

    public void detach() {
        eventReciever.removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
        eventReciever.removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
        eventReciever.removeEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedHandler);
        eventReciever.removeEventHandler(KeyEvent.KEY_TYPED, keyTypedHandler);
        extrudeEditor.setExtrude(null);
        extrude = null;
        extrudeEditor = null;
        clear();
    }

    private void clear() {
    }
}
