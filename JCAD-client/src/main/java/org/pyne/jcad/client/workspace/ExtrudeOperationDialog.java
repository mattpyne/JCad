package org.pyne.jcad.client.workspace;

import java.util.List;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.sculptor.Sculptor;
import org.pyne.jcad.client.viewer.Viewer;
import org.pyne.jcad.core.entity.ExtrudeOperation;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Loop;

/**
 *
 * @author Matthew Pyne
 */
public class ExtrudeOperationDialog extends Dialog {

    private final ExtrudeOperation extrude;

    private ExpressionEditor amountField;
    private ExpressionEditor xDirField;
    private ExpressionEditor yDirField;
    private ExpressionEditor zDirField;
    private ExpressionEditor tapperField;
    private ExpressionEditor offsetField;

    private ListView<String> errors;

    public ExtrudeOperationDialog(ExtrudeOperation extrude) {
        super();

        this.extrude = extrude;
        setTitle("Extrude");

        Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);
        Point2D viewerLocationOnScreen = viewer.localToScreen(new Point2D(0., 0.));

        setX(viewerLocationOnScreen.getX());
        setX(viewerLocationOnScreen.getY());

        initOwner(JCadApp.WINDOW);

        initModality(Modality.NONE);

        // Set the button types.
        ButtonType acceptButtonType = new ButtonType("Accept", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(acceptButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 10, 10, 10));

        amountField = new ExpressionEditor("Amount", extrude.getAmount(), e -> updateExtrusion());
        xDirField = new ExpressionEditor("X Direction", extrude.getxDir(), e -> updateExtrusion());
        yDirField = new ExpressionEditor("Y Direction", extrude.getyDir(), e -> updateExtrusion());
        zDirField = new ExpressionEditor("Z Direction", extrude.getzDir(), e -> updateExtrusion());
        tapperField = new ExpressionEditor("Tapper", extrude.getTapper(), e -> updateExtrusion());
        offsetField = new ExpressionEditor("Offset", extrude.getOffset(), e -> updateExtrusion());

        errors = new ListView<>(FXCollections.observableList(extrude.getErrorMessages()));

        grid.add(amountField, 0, 0);
        grid.add(xDirField, 0, 1);
        grid.add(yDirField, 0, 2);
        grid.add(zDirField, 0, 3);
        grid.add(tapperField, 0, 4);
        grid.add(offsetField, 0, 5);

        grid.add(errors, 0, 6, 2, 4);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> amountField.requestFocus());

        setOnCloseRequest((e) -> {

            updateExtrusion();

            viewer
                    .controller()
                    .updateView(extrude);

            JCadApp.PLUGIN_MANAGER.getPlugin(Sculptor.class)
                    .controller()
                    .clearPreview();
            
            close();
        });

        show();
    }

    protected void updateExtrusion() {
        try {

            if (extrude.getLoops().isEmpty()) {
                List<Loop> selectedLoops = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class)
                        .controller()
                        .getSelectedLoops();

                extrude.getLoops().addAll(selectedLoops);
            }

            JCadApp.PLUGIN_MANAGER.getPlugin(Sculptor.class)
                    .controller()
                    .preview(extrude.topoOp());

        } catch (NumberFormatException ex) {

        }
    }
}
