package org.pyne.jcad.client.sketcher;

import com.pixelduke.control.ribbon.RibbonGroup;
import javafx.scene.control.Button;
import javax.inject.Inject;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.ClientDocumentAPI;
import org.pyne.jcad.client.ClientInjector;
import org.pyne.jcad.client.ClientInjectorImp;

/**
 *
 * @author Matthew Pyne
 */
@Plugin(name = "Sketcher")
public class Sketcher {

    private final Button sketchButton;
    private final Button circleSketchButton;
    private final Button lineSketchButton;

    private final SketcherController controller;

    @Inject
    public Sketcher(ClientInjectorImp clientInjector, ClientDocumentAPI documentAPI) {
        super();

        RibbonGroup sketchGroup = new RibbonGroup();
        sketchGroup.setTitle("Sketcher");

        sketchButton = new Button("Create Sketch");

        sketchGroup.getNodes().add(sketchButton);

        circleSketchButton = new Button("Circle");
        circleSketchButton.setDisable(true);
        sketchGroup.getNodes().add(circleSketchButton);
        
        lineSketchButton = new Button("Line");
        lineSketchButton.setDisable(true);
        sketchGroup.getNodes().add(lineSketchButton);

        clientInjector.addToTopTool(sketchGroup, "Home");

        controller = new SketcherController(this, documentAPI);
    }

    public void displayCreatingSketch() {
        sketchButton.setText("Accept Sketch");
        circleSketchButton.setDisable(false);
        lineSketchButton.setDisable(false);
    }
    
    public void displayNotCreatingSketch() {
        sketchButton.setText("Create Sketch");
        circleSketchButton.setDisable(true);
        lineSketchButton.setDisable(true);
    }
    
    public Button sketchButton() {
        return sketchButton;
    }

    public Button circleSketchButton() {
        return circleSketchButton;
    }
    
    public Button lineSketchButton() {
        return lineSketchButton;
    }

    public SketcherController controller() {
        return controller;
    }
}
