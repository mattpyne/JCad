package org.pyne.jcad.client.viewer;

import java.util.HashMap;
import java.util.List;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import org.pyne.jcad.client.JCadApp;

/**
 *
 * @author Matthew Pyne
 */
public class Viewer2DMouseBehavior {

    private Viewer2D viewer2D;

    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;

    Point3D org3DCoords;

    ProjectedView draggingProjectView;
    HashMap<Node, Boolean> mouseTransparentBeforeProjectedViewDrag = new HashMap<>();

    public void register(Viewer2D viewer2D) {
        this.viewer2D = viewer2D;

        viewer2D.addEventHandler(
                MouseEvent.MOUSE_CLICKED,
                me -> {
                    if (me.isControlDown()) {
                        return;
                    }

                    viewer2D.streamNodeBy(ProjectedView.class)
                            .forEach(sketch -> sketch.setSelected(false));
                });

        viewer2D.addEventHandler(
                MouseEvent.ANY,
                me -> {
                    viewer2D.streamNodeBy(ProjectedView.class)
                            .forEach(sketch -> sketch.setHovered(false));
                });
    }

    public void makeDraggable(Node node) {

        node.addEventHandler(
                MouseEvent.MOUSE_PRESSED,
                mousePressedDraggableEventHandler);

        node.addEventHandler(
                MouseEvent.MOUSE_DRAGGED,
                mouseDraggedEventHandler);

        node.addEventHandler(
                MouseEvent.MOUSE_RELEASED,
                mouseReleaseHandler);

    }

    EventHandler<MouseEvent> mouseReleaseHandler = t -> {
        if (null != draggingProjectView) {

            ViewUtils.getAllChildren(draggingProjectView.getScene().getRoot())
                    .stream()
                    .forEach(node -> {
                        Boolean mouseTransparent = mouseTransparentBeforeProjectedViewDrag.get(node);
                        if (null == mouseTransparent) {
                            return;
                        }
                        node.setMouseTransparent(mouseTransparent);
                    });

            draggingProjectView.setSelected(true);
            draggingProjectView = null;
        }
    };

    EventHandler<MouseEvent> mousePressedDraggableEventHandler = t -> {

        orgSceneX = t.getSceneX();
        orgSceneY = t.getSceneY();

        Node p = ((Node) (t.getSource()));

        orgTranslateX = p.getTranslateX();
        orgTranslateY = p.getTranslateY();

        draggingProjectView = null;

        if (t.getSource() instanceof ProjectedView) {

            ProjectedView pv = (ProjectedView) t.getSource();
            draggingProjectView = pv;
            View context = pv.context();

            List<Node> contextNodes = ViewUtils.getAllChildren(context);
            contextNodes.add(context);

            mouseTransparentBeforeProjectedViewDrag.clear();

            Group viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class).viewModel();
            ViewUtils.getAllChildren(viewer)
                    .stream()
                    .forEach(node -> {
                        mouseTransparentBeforeProjectedViewDrag.put(node, node.isMouseTransparent());

                        node.setMouseTransparent(!contextNodes.contains(node));
                    });

            Viewer2D viewer2d = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer2D.class);
            ViewUtils.getAllChildren(viewer2d)
                    .stream()
                    .forEach(node -> {

                        mouseTransparentBeforeProjectedViewDrag.put(node, node.isMouseTransparent());
                        node.setMouseTransparent(true);
                    });

            Camera camera = pv.camera();

            if (null == camera) {
                return;
            }

            org3DCoords = ViewUtils.rayCastPt(
                    pv.camera(),
                    new Point2D(orgSceneX, orgSceneY),
                    pv.getScene());
        }

    };

    EventHandler<MouseEvent> mouseDraggedEventHandler = t -> {

        System.out.println("Dragging");

        if (null != draggingProjectView) {

            Point3D coords = ViewUtils.rayCastPt(
                    draggingProjectView.camera(), 
                    new Point2D(t.getSceneX(), t.getSceneY()),
                    draggingProjectView.getScene());
            
            Point3D diff = coords.subtract(org3DCoords);

            org3DCoords = coords;

            System.out.println("Diff: " + diff);
            draggingProjectView.translate(diff);            
        } else {

            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;

            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;

            Node p = ((Node) (t.getSource()));

            p.setTranslateX(newTranslateX);
            p.setTranslateY(newTranslateY);
        }
    };

    public void makeSelectable(ProjectedView sketch) {
        sketch.addEventHandler(
                Event.ANY,
                me -> {
                    if (me.getEventType() == MouseEvent.MOUSE_CLICKED) {
                        Platform.runLater(() -> sketch.setSelected(true));
                    }
                });
    }

    public void makeHoverable(ProjectedView sketch) {
        sketch.addEventHandler(
                Event.ANY,
                me -> {
                    if (me.getEventType() == MouseEvent.MOUSE_MOVED) {
                        Platform.runLater(() -> sketch.setHovered(true));
                    }
                });
    }
}
