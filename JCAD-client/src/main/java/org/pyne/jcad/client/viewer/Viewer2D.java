package org.pyne.jcad.client.viewer;

import javafx.scene.Group;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.FxNodeSearchable;

/**
 * @author Matthew Pyne
 */
@Plugin(name = "Viewer2D")
public class Viewer2D extends Group implements FxNodeSearchable {

    private final Viewer2DController controller;

    public Viewer2D() {
        super();

        controller = new Viewer2DController(this);
    }

    public void clear() {
        getChildren().removeIf(ProjectedView.class::isInstance);
    }

    public Viewer2DController controller() {
        return controller;
    }

}
