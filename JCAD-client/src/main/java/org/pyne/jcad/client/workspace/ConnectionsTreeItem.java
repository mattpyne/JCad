package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.Connections;

/**
 *
 * @author Matthew Pyne
 */
public class ConnectionsTreeItem extends ComponentTreeItem {
    
    public ConnectionsTreeItem(Connections component) {
        super("Connections", component);
    }
    
}
