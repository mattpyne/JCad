package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.Material;

/**
 *
 * @author Matthew Pyne
 */
public class MaterialTreeItem extends ComponentTreeItem {

    public MaterialTreeItem(Material material) {
        super(material);
    }

    @Override
    public void onDoubleClicked() {
        new MaterialDialog((Material) component());
    }

}
