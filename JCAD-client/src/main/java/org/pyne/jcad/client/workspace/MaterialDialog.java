package org.pyne.jcad.client.workspace;

import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.JavaFXInterop;
import org.pyne.jcad.client.viewer.Viewer;
import org.pyne.jcad.core.entity.Material;

/**
 *
 * @author Matthew Pyne
 */
public class MaterialDialog extends Dialog {

    public MaterialDialog(Material material) {

        setTitle("Material");
        setHeaderText("Choose material properties!");

        // Set the icon (must be included in the project).
//        setGraphic(new ImageView(this.getClass().getResource("login.png").toString()));

        // Set the button types.
        ButtonType acceptButtonType = new ButtonType("Accept", ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(acceptButtonType, ButtonType.CANCEL);

        // Create the description and color labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField description = new TextField();
        description.setPromptText("Description");
        description.setText(material.getDescription());
        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(JavaFXInterop.from(material.getColor()));

        grid.add(new Label("Description:"), 0, 0);
        grid.add(description, 1, 0);
        grid.add(new Label("Color:"), 0, 1);
        grid.add(colorPicker, 1, 1);

        getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> description.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        setResultConverter(dialogButton -> {
            if (dialogButton == acceptButtonType) {
                return new Pair<>(description.getText(), colorPicker.getValue());
            }
            return null;
        });

        Optional<Pair<String, Color>> result = showAndWait();

        result.ifPresent(descriptionColor -> {
            
            material.setDescription(descriptionColor.getKey());
            material.setColor(JavaFXInterop.from(descriptionColor.getValue()));
            
            JCadApp.PLUGIN_MANAGER
                    .getPlugin(Viewer.class)
                    .controller()
                    .refreshColor();
        });
    }
}
