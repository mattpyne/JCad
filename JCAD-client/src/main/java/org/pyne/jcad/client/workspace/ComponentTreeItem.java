package org.pyne.jcad.client.workspace;

import javafx.scene.control.TreeItem;
import org.pyne.jcad.core.entity.IComponent;

/**
 *
 * @author Matthew Pyne
 */
public class ComponentTreeItem extends TreeItem implements Clickable {

    private final IComponent component;

    public ComponentTreeItem(IComponent component) {
        super(component.getClass().getSimpleName());
        this.component = component;
    }
    
    public ComponentTreeItem(String text, IComponent component) {
        super(text);
        this.component = component;
    }

    public IComponent component() {
        return component;
    }

    @Override
    public void onDoubleClicked() {
    }

}
