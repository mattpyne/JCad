package org.pyne.jcad.client;

import org.pyne.jcad.client.viewer.ViewUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.scene.Node;

/**
 *
 * @author Matthew Pyne
 */
public interface FxNodeSearchable {
    
    default public <T extends Object> Stream<T> streamNodeBy(Class<T> clazz) {
        return getAllChildren().stream()
                .filter(clazz::isInstance)
                .map(clazz::cast);
    }
    
    default public <T extends Object> List<T> nodeBy(Class<T> clazz) {
        return streamNodeBy(clazz).collect(Collectors.toList());
    }

    default public ArrayList<Node> getAllChildren() {
        return ViewUtils.getAllChildren((Node) this);
    }

}
