package org.pyne.jcad.client;

import javafx.geometry.Point3D;
import javafx.scene.paint.Color;
import org.pyne.jcad.core.math.Point3DC;

public class JavaFXInterop {

    public static Point3DC from(Point3D point) {
        return new Point3DC(point.getX(), point.getY(), point.getZ());
    }

    public static Point3D from(Point3DC point) {
        return new Point3D(point.x(), point.y(), point.z());
    }

    public static Color from(org.pyne.jcad.core.Color color) {
        return new Color(color.r, color.g, color.b, color.a);
    }

    public static org.pyne.jcad.core.Color from(Color color) {
        return new org.pyne.jcad.core.Color(
                color.getRed(),
                color.getGreen(),
                color.getBlue(),
                color.getOpacity());
    }
}
