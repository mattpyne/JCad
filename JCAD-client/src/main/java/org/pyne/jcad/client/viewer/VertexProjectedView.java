package org.pyne.jcad.client.viewer;

import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Vertex;

/**
 *
 * @author Matthew Pyne
 */
public class VertexProjectedView extends ProjectedView {

    private static final Color HOVER_COLOR = Color.DEEPSKYBLUE;
    private static final Color SELECT_COLOR = Color.AQUA;
    private static final Color HIDDEN_COLOR = new Color(0, 0, 0, 0);

    private final Circle circleNode;
    private final Vertex vertex;

    public VertexProjectedView(Vertex vertex, View context) {
        super(context);
        this.vertex = vertex;
        circleNode = new Circle(6);
        getChildren().add(circleNode);
    }

    @Override
    public void repaint() {

        if (null == vertex) {
            return;
        }

        Point3DC p3d = vertex.point();
        Point2D p2d = project(p3d);

        circleNode.setCenterX(p2d.getX());
        circleNode.setCenterY(p2d.getY());
        
        updateCurveColor();
    }

    @Override
    public void setHovered(boolean hovered) {
        super.setHovered(hovered);
        updateCurveColor();
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        updateCurveColor();
    }

    private void updateCurveColor() {
//        curveNode.setStroke(Color.BLACK);
        if (isSelected()) {
            circleNode.setStroke(SELECT_COLOR);
            circleNode.setFill(SELECT_COLOR);
        } else if (isHovered()) {
            circleNode.setStroke(HOVER_COLOR);
            circleNode.setFill(HOVER_COLOR);
        } else {
            circleNode.setStroke(HIDDEN_COLOR);
            circleNode.setFill(HIDDEN_COLOR);
        }
    }
    
    public Vertex vertex() {
        return vertex;
    }
    
    @Override
    public void translate(Point3D translation) {
        if (null != vertex) {

            vertex.transform(Matrix3.createTranslation(
                    translation.getX(),
                    translation.getY(),
                    translation.getZ()));
            
            repaint();
        }
    }
    
    @Override
    public Node getPhysicalNode() {
        return circleNode;
    }
}
