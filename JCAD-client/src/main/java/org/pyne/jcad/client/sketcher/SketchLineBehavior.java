package org.pyne.jcad.client.sketcher;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.pyne.jcad.client.viewer.CurveProjectedView;
import org.pyne.jcad.client.viewer.VertexProjectedView;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.NurbsCurve;

/**
 *
 * @author Matthew Pyne
 */
public class SketchLineBehavior extends SketchBehavior {

    private final EventHandler<MouseEvent> mousePressedHandler;
    private final EventHandler<MouseEvent> mouseReleasedHandler;
    private final EventHandler<MouseEvent> mouseDraggedHandler;
    private final EventHandler<KeyEvent> keyTypedHandler;

    private CurveProjectedView line;
    private Point3DC firstPoint;

    public SketchLineBehavior(SketcherController controller) {
        super(controller);
        this.mousePressedHandler = event -> mousePressed(event);
        this.mouseReleasedHandler = event -> mouseRelease(event);
        this.mouseDraggedHandler = event -> mouseDragging(event);
        this.keyTypedHandler = event -> keyTyped(event);
    }

    @Override
    public void attach(SketchView view, Node eventReciever) {
        this.sketchView = view;
        this.eventReciever = eventReciever;

        if (null == eventReciever
                || null == view) {
            return;
        }

        eventReciever.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
        eventReciever.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
        eventReciever.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedHandler);
        eventReciever.addEventHandler(KeyEvent.KEY_TYPED, keyTypedHandler);
    }

    @Override
    public void detach() {
        if (null == eventReciever) {
            return;
        }

        eventReciever.removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedHandler);
        eventReciever.removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedHandler);
        eventReciever.removeEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedHandler);
        eventReciever.removeEventHandler(KeyEvent.KEY_TYPED, keyTypedHandler);

        line = null;
        sketchView = null;
    }

    private void keyTyped(KeyEvent event) {
        if (event.getCode() == KeyCode.ESCAPE) {
            detach();
        }
    }

    private void mousePressed(MouseEvent event) {
        firstPoint = getDrawPoint(event);

        NurbsCurve curve = CurvePrimitives.line(firstPoint, firstPoint);

        line = controller.addCurve(curve);
        line.setMouseTransparent(true);
    }

    private void mouseRelease(MouseEvent event) {
        line.setMouseTransparent(false);
        detach();
    }

    private void mouseDragging(MouseEvent event) {
        if (null == line) {
            return;
        }

        Point3DC point = getDrawPoint(event);
        ((NurbsCurve) line.curve()).setData(CurvePrimitives.line(firstPoint, point));
        line.repaint();
        
//        VertexProjectedView firstPointView = new VertexProjectedView(, line.context());
    }
}
