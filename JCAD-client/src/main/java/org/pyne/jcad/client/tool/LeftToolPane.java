package org.pyne.jcad.client.tool;

import goryachev.fxdock.FxDockPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.pyne.jcad.client.JCadFxDockGenerator;

public class LeftToolPane extends FxDockPane {

    Pane viewContainer;

    public LeftToolPane() {
        super(Id());

        viewContainer = new FxDockVPane();

        setClosable(false);
        setTitle("Tools");
        setContent(viewContainer);
        viewContainer.setId(Id());
//        viewContainer.prefWidthProperty().bind(widthProperty());
//        viewContainer.prefHeightProperty().bind(heightProperty());
    }

    public static String Id() {
        return JCadFxDockGenerator.LEFT_TOOLS;
    }
}
