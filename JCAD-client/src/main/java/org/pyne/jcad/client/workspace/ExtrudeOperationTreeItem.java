package org.pyne.jcad.client.workspace;

import org.pyne.jcad.client.ClientInjector;
import org.pyne.jcad.client.ClientInjectorImp;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.core.entity.ExtrudeOperation;

/**
 *
 * @author Matthew Pyne
 */
public class ExtrudeOperationTreeItem extends OperationTreeItem {

    public ExtrudeOperationTreeItem(ExtrudeOperation extrude) {
        super(extrude);
    }

    @Override
    public void onDoubleClicked() {
        ExtrudeOperationEditor extrudeEditor = JCadApp.PLUGIN_MANAGER.getPlugin(ExtrudeOperationEditor.class);

        extrudeEditor.setExtrude((ExtrudeOperation) getOperation());

        extrudeEditor.show();
    }
}
