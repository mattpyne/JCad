package org.pyne.jcad.client.viewer;

import java.util.Optional;
import java.util.stream.Stream;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 *
 * @author Matthew Pyne
 */
public class ViewExplorer {

    /**
     * Search Sketcher for selected sketches.
     *
     * @param view
     * @return
     */
    public static Stream<View> streamSelected(Node view) {
        return ViewUtils.streamNodeBy(view, View.class).filter(View::isSelected);
    }

    /**
     * Search Sketcher for a hovered sketch.
     *
     * @param view
     * @return
     */
    public static Optional<View> hovered(Node view) {
        return ViewUtils.streamNodeBy(view, View.class)
                .filter(View::isHovered)
                .findAny();
    }

    /**
     * Searches for the Parent View of aClass
     *
     * @param <T>
     * @param view
     * @param aClass
     * @return
     */
    public static <T extends View> Optional<T> parentViewBy(Node view, Class<T> aClass) {
        Parent parent = view.getParent();

        if (null == parent) {
            return Optional.empty();
        }

        if (aClass.isInstance(parent)) {
            return Optional.of((T) parent);
        }

        return parentViewBy(parent, aClass);
    }

}
