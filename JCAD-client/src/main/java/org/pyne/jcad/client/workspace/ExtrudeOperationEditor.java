package org.pyne.jcad.client.workspace;

import goryachev.fxdock.FxDockPane;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.ClientInjectorImp;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.sculptor.Sculptor;
import org.pyne.jcad.client.viewer.Viewer;
import org.pyne.jcad.core.entity.Constructor;
import org.pyne.jcad.core.entity.ExtrudeOperation;
import org.pyne.jcad.core.math.brep.Loop;

import javax.inject.Inject;
import java.util.List;

@Plugin(name = "ExtrudeOperationEditor")
public class ExtrudeOperationEditor extends FxDockPane {
    private ExtrudeOperation extrude;

    private ExpressionEditor amountField;
    private ExpressionEditor xDirField;
    private ExpressionEditor yDirField;
    private ExpressionEditor zDirField;
    private ExpressionEditor tapperField;
    private ExpressionEditor offsetField;
    private final Button acceptButton;
    private final Button cancelButton;
    private final Button deleteButton;

    private ListView<String> errors;
    private boolean doNotUpdate;

    @Inject
    public ExtrudeOperationEditor(ClientInjectorImp clientInjector) {
        super("ExtrudeOperationEditor");
        setId("ExtrudeOperationEditor");

        managedProperty().bind(visibleProperty());

        GridPane grid = new GridPane();
        grid.setHgap(5);
        grid.setVgap(5);
        setPadding(new Insets(5, 10, 5, 10));

        amountField = new ExpressionEditor("Amount", e -> updateExtrusion());
        xDirField = new ExpressionEditor("X Direction", e -> updateExtrusion());
        yDirField = new ExpressionEditor("Y Direction", e -> updateExtrusion());
        zDirField = new ExpressionEditor("Z Direction", e -> updateExtrusion());
        tapperField = new ExpressionEditor("Tapper", e -> updateExtrusion());
        offsetField = new ExpressionEditor("Offset", e -> updateExtrusion());

        errors = new ListView<>();

        acceptButton = new Button("Accept");
        acceptButton.setOnAction((event) -> onAccept());

        cancelButton = new Button("Cancel");
        cancelButton.setOnAction((event) -> onCancel());

        deleteButton = new Button("Delete");
        deleteButton.setOnAction((event) -> onDelete());

        grid.add(amountField, 0, 0);
        grid.add(xDirField, 0, 1);
        grid.add(yDirField, 0, 2);
        grid.add(zDirField, 0, 3);
        grid.add(tapperField, 0, 4);
        grid.add(offsetField, 0, 5);

//        add(errors, 0, 6);
        grid.add(new HBox(acceptButton, cancelButton, deleteButton), 0, 6);

        setContent(grid);
        setVisible(false);

        clientInjector.addToLeftPane(this);
    }

    private void refreshView() {
        if (null == extrude) {
            return;
        }

        doNotUpdate = true;
        amountField.setExpression(extrude.getAmount());
        xDirField.setExpression(extrude.getxDir());
        yDirField.setExpression(extrude.getyDir());
        zDirField.setExpression(extrude.getzDir());
        tapperField.setExpression(extrude.getTapper());
        offsetField.setExpression(extrude.getOffset());

        errors.setItems(FXCollections.observableList(extrude.getErrorMessages()));

        doNotUpdate = false;
        updateExtrusion();
    }

    public void show() {
        setVisible(true);

        Platform.runLater(() -> amountField.requestFocus());
    }

    public void setExtrude(ExtrudeOperation extrude) {
        this.extrude = extrude;

        this.extrude.setObserver(JCadApp.testing_DisplayFaceEdges());

        refreshView();
    }

    private void onCancel() {
        setVisible(false);

        JCadApp.PLUGIN_MANAGER.getPlugin(Sculptor.class)
                .controller()
                .clearPreview();
    }

    private void onDelete() {
        setVisible(false);

        JCadApp.DOCUMENT_MODEL.getModel()
                .getComponent(Constructor.class)
                .ifPresent(constructor -> {
                    constructor.operations().remove(extrude);
                    constructor.update();

                    JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class)
                            .controller()
                            .refreshView();

                    JCadApp.PLUGIN_MANAGER.getPlugin(Sculptor.class)
                            .controller()
                            .clearPreview();
                });
    }

    private void onAccept() {
        updateExtrusion();

        JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class)
                .controller()
                .updateView(extrude);

        JCadApp.PLUGIN_MANAGER.getPlugin(Sculptor.class)
                .controller()
                .clearPreview();

        setVisible(false);
    }

    protected void updateExtrusion() {
        try {

            if (doNotUpdate) {
                return;
            }

            if (extrude.getLoops().isEmpty()) {
                List<Loop> selectedLoops = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class)
                        .controller()
                        .getSelectedLoops();

                extrude.getLoops().addAll(selectedLoops);
            }

            JCadApp.PLUGIN_MANAGER.getPlugin(Sculptor.class)
                    .controller()
                    .preview(extrude.topoOp());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

