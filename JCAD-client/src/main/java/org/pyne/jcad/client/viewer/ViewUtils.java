package org.pyne.jcad.client.viewer;

import com.sun.javafx.geom.PickRay;
import com.sun.javafx.geom.Vec3d;
import com.sun.javafx.geom.transform.Affine3D;
import com.sun.javafx.scene.NodeHelper;
import com.sun.javafx.scene.input.PickResultChooser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.stream.Stream;

import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.input.PickResult;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.transform.Transform;
import org.pyne.jcad.client.JCadApp;

/**
 * @author Matthew Pyne
 */
public class ViewUtils {

    public static Point3D rayCastPt(Camera camera, Point2D scenePt, Scene scene) {

        if (null == scene) {
            return Point3D.ZERO;
        }

        Pane viewModel = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class).view3dPane();

        Affine3D localToSceneAffine = new Affine3D();
        Transform transform = camera.getLocalToSceneTransform();
        localToSceneAffine.setTransform(
                transform.getMxx(),
                transform.getMxy(),
                transform.getMxz(),
                transform.getTx(),
                transform.getMyx(),
                transform.getMyy(),
                transform.getMyz(),
                transform.getTy(),
                transform.getMzx(),
                transform.getMzy(),
                transform.getMzz(),
                transform.getTz());

        double x = scenePt.getX();
        double y = scenePt.getY();
        double height = scene.getHeight();

        Point3D p = viewModel.sceneToLocal(new Point3D(x, y, height));
        x = p.getX();
        y = p.getY();
        height = p.getZ();
        PickRay pickRay = PickRay.computeParallelPickRay(x, y, height,
                localToSceneAffine,
                0.1, 1000, null);
        final double mag = pickRay.getDirectionNoClone().length();
        pickRay.getDirectionNoClone().normalize();
        PickResultChooser r = new PickResultChooser();

        try {
            Method privateStringMethod = Region.class.getDeclaredMethod(
                    "doPickNodeLocal", PickRay.class, PickResultChooser.class);
            privateStringMethod.setAccessible(true);

            privateStringMethod.invoke(viewModel, pickRay, r);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

//        viewModel.doPickNodeLocal(pickRay, r);
        Point3D modelPoint;
        PickResult res = r.toPickResult();
        if (res != null) {
            modelPoint = res.getIntersectedPoint();
        } else {
            //TODO: is this the intersection with projection plane?
            Vec3d o = pickRay.getOriginNoClone();
            Vec3d d = pickRay.getDirectionNoClone();
            modelPoint = new Point3D(
                    o.x + mag * d.x,
                    o.y + mag * d.y,
                    o.z + mag * d.z);
        }

        return modelPoint;
    }

    public static ArrayList<Node> getAllChildren(Node node) {
        ArrayList<Node> nodes = new ArrayList<>();

        if (!(node instanceof Parent)) {

            return nodes;
        }

        addAllDescendents((Parent) node, nodes);
        return nodes;
    }

    public static <T extends Object> Stream<T> streamNodeBy(Node node, Class<T> clazz) {
        return getAllChildren(node).stream()
                .filter(clazz::isInstance)
                .map(clazz::cast);
    }

    private static void addAllDescendents(Parent parent, ArrayList<Node> nodes) {
        for (Node node : parent.getChildrenUnmodifiable()) {
            nodes.add(node);
            if (node instanceof Parent) {
                addAllDescendents((Parent) node, nodes);
            } else if (node instanceof SubScene) {
                Parent root = ((SubScene) node).getRoot();
                nodes.add(root);
                addAllDescendents(root, nodes);
            }
        }
    }

    public static Camera mainViewCamera() {

        SubScene subScene = NodeHelper.getSubScene(JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class).viewModel());

        if (null == subScene) {
            return null;
        }

        return subScene.getCamera();
    }
}
