package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.Identification;

/**
 *
 * @author Matthew Pyne
 */
public class IdentificationTreeItem extends ComponentTreeItem {


    public IdentificationTreeItem(Identification id) {
        super(id);
    }

    @Override
    public void onDoubleClicked() {
        new IdentificationDialog((Identification) component());
    }

}
