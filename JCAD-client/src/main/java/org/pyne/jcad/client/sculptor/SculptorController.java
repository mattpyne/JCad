package org.pyne.jcad.client.sculptor;

import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.viewer.TopoViewFactory;
import org.pyne.jcad.client.viewer.View;
import org.pyne.jcad.client.viewer.Viewer;
import org.pyne.jcad.core.math.brep.Topo;

/**
 *
 * @author Matthew Pyne
 */
public class SculptorController {

    private final Sculptor sculptor;

    private final ExtrudeBehavior extrudeBehavior;

    private View previewView;

    public SculptorController(Sculptor sculptor) {
        this.sculptor = sculptor;
        this.extrudeBehavior = new ExtrudeBehavior(this);

        sculptor.extrudeButton().setOnAction((event) -> {
            Viewer viewer = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class);
            extrudeBehavior.attach(viewer.viewModel());
        });
    }

    public void preview(Topo topo) {
        Group view3DGroup = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class).viewModel();

        clearPreview();

        TopoViewFactory factory = new TopoViewFactory();
        
        previewView = factory.create(topo);
        previewView.setMouseTransparent(true);
        previewView.setMaterial(new PhongMaterial(new Color(0.7, 0.2, 0.1, 0.5)));
        previewView.setDepthTest(DepthTest.DISABLE);
        view3DGroup.getChildren().add(previewView);
    }

    public void clearPreview() {
        if (null != previewView) {
            
            Group view3DGroup = JCadApp.PLUGIN_MANAGER.getPlugin(Viewer.class).viewModel();
            
            view3DGroup.getChildren().remove(previewView);
        }
    }
}
