package org.pyne.jcad.client.viewer;

import java.util.List;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.core.math.brep.Shell;

/**
 *
 * @author Matthew
 */
public final class Viewer2DController {

    private final Viewer2D view2d;
    private final Viewer2DMouseBehavior mouseBehavior;

    public Viewer2DController(Viewer2D sketcher) {
        this.view2d = sketcher;
        mouseBehavior = new Viewer2DMouseBehavior();
        mouseBehavior.register(sketcher);
    }

    public Viewer2DMouseBehavior mouseBehavior() {
        return mouseBehavior;
    }

    public void repaintView() {
        view2d.streamNodeBy(ProjectedView.class)
                .forEach(ProjectedView::repaint);
    }

    public void recalculateView() {
        view2d.clear();

        View view = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class)
                .controller()
                .getSubject();

        if (null == view) {
            return;
        }

        List<ShellView> shellViews = view.nodeBy(ShellView.class);
        
        shellViews.stream()
                .map(ShellView::shell)
                .flatMap(Shell::streamEdges)
                .map(e -> new EdgeProjectedView(e, view))
                .forEach(edgeSketch -> {
                    view2d.getChildren().add(edgeSketch);
                    mouseBehavior().makeSelectable(edgeSketch);
                    mouseBehavior().makeHoverable(edgeSketch);
                    edgeSketch.repaint();
                });
        
        shellViews.stream()
                .map(ShellView::shell)
                .flatMap(Shell::streamVertices)
                .map(v -> new VertexProjectedView(v, view))
                .forEach(vertexSketch -> {
                    view2d.getChildren().add(vertexSketch);
                    mouseBehavior().makeSelectable(vertexSketch);
                    mouseBehavior().makeHoverable(vertexSketch);
                    vertexSketch.repaint();
                });
    }

    public void disable() {
        if (view2d.getParent() != null) {
            JCadApp.PLUGIN_MANAGER
                    .getPlugin(Viewer.class)
                    .viewPane()
                    .getChildren()
                    .remove(view2d);
        }
    }
   
    public void enable() {
        if (view2d.getParent() == null) {
            JCadApp.PLUGIN_MANAGER
                    .getPlugin(Viewer.class)
                    .viewPane()
                    .getChildren()
                    .add(view2d);
        }
    }

    public Viewer2D view() {
        return view2d;
    }

}
