package org.pyne.jcad.client.viewer;

import org.pyne.jcad.core.math.brep.*;

/**
 *
 * @author Matthew Pyne
 */
public class TopoViewFactory {

    public View create(Topo topo) {

        if (topo instanceof Edge) {

            return null;// new EdgeView((Edge) topo);
        } else if (topo instanceof Shell) {

            return new ShellView((Shell) topo);
        } else if (topo instanceof CompoundShell) {

            return new CompoundShellView((CompoundShell) topo);
        } else if (topo instanceof CompoundTopo) {

            return new CompoundTopoView((CompoundTopo) topo);
        }

        return null;
    }
}
