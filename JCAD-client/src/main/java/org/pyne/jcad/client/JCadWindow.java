package org.pyne.jcad.client;

import goryachev.fx.*;
import goryachev.fxdock.FxDockFramework;
import goryachev.fxdock.FxDockWindow;
import goryachev.fxdock.internal.FxDockSplitPane;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import java.io.File;
import static goryachev.common.util.GlobalSettings.save;
import org.pyne.jcad.client.tool.TopTool;

public class JCadWindow extends FxDockWindow {

    public static String TITLE = "JCad";

    public static final FxAction quitApplicationAction = new FxAction(FxDockFramework::exit);
    public static FxAction loadModelAction;
    public static FxAction saveModelAction;

    public final static FileChooser fileChooser = new FileChooser();
    public static JCadWindow mainWin;
    
    public final Label statusField = new Label();

    public JCadWindow() {
        setTop(createMenu());
        setBottom(createStatusBar());
        setTitle(JCadWindow.TITLE);
        mainWin = this;
    }

    protected BorderPane createMenu() {
        loadModelAction = new FxAction(this::loadModel);
        saveModelAction = new FxAction(this::saveModel);

        BorderPane pane = new BorderPane();

        FxMenuBar m = new FxMenuBar();

        // file
        m.menu("File");
        m.item("Load", loadModelAction);
        m.item("Save", saveModelAction);
        m.item("Close Window", closeWindowAction);
        m.separator();
        m.item("Quit Application", quitApplicationAction);

        // window
        m.menu("Window");

        // help
        m.menu("Help");

        pane.setTop(m);

        pane.setBottom(new TopTool());

        return pane;
    }

    protected Node createStatusBar() {
        BorderPane p = new BorderPane();
        p.setLeft(statusField);
        return p;
    }

    public static JCadWindow openDefault() {
        JCadWindow win = new JCadWindow();

        FxDockSplitPane main = new FxDockSplitPane();
        main.setId("main");
        
        win.setContent(main);
        win.setMaximized(true);
        win.open();

        return win;
    }

    public void loadModel() {

        File file = fileChooser.showOpenDialog(this);

        JCadApp.DOCUMENT_MODEL.loadData(file);
    }

    public void saveModel() {

        File file = fileChooser.showSaveDialog(this);

        JCadApp.DOCUMENT_MODEL.saveData(file);
    }

    @Override
    public void confirmClosing(OnWindowClosing ch) {
        if (ch.isSaveAll()) {
            save();
            return;
        } else if (ch.isDiscardAll()) {
            return;
        }

        toFront();

        FxDialog d = new FxDialog(this);
        d.setTitle("Save Changes?");
        d.setContentText("This is an example of a dialog shown when closing a window.");

        Object save = d.addButton("Save");
        Object saveAll = null;
        if (ch.isClosingMultipleWindows()) {
            saveAll = d.addButton("Save All");
        }
        d.addButton("Discard");
        Object discardAll = null;
        if (ch.isClosingMultipleWindows()) {
            discardAll = d.addButton("Discard All");
        }
        Object cancel = d.addButton("Cancel", ButtonBar.ButtonData.APPLY);

        d.showAndWait();
        Object rv = d.getResult();

        if (rv == cancel) {
            ch.setCancelled();
        } else if (rv == save) {
            save();
        } else if (rv == saveAll) {
            ch.setSaveAll();
            save();
        } else if (rv == discardAll) {
            ch.setDiscardAll();
        }
    }
}
