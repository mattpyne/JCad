package org.pyne.jcad.client.tool;

import goryachev.fxdock.FxDockPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.pyne.jcad.client.JCadFxDockGenerator;

public class RightToolPane extends FxDockPane {

    Pane viewContainer;

    public RightToolPane() {
        super(Id());

        viewContainer = new AnchorPane();

        setClosable(false);
        setTitle("Tools");
        setContent(viewContainer);
        viewContainer.setId(Id());
    }

    public static String Id() {
        return JCadFxDockGenerator.RIGHT_TOOLS;
    }
}
