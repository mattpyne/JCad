package org.pyne.jcad.client.viewer;

import org.pyne.jcad.core.math.brep.CompoundTopo;

public class CompoundTopoView extends View {

    private final CompoundTopo topo;

    public CompoundTopoView(CompoundTopo topo) {
        this.topo = topo;
        topo.topos().forEach(s -> getChildren().add(new TopoViewFactory().create(s)));
    }

    public CompoundTopo getTopo() {
        return topo;
    }

}
