package org.pyne.jcad.client.viewer;

import java.util.List;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Edge;

/**
 *
 * @author Matthew Pyne
 */
public class EdgeProjectedView extends ProjectedView {

    private static final Color HOVER_COLOR = Color.DEEPSKYBLUE;
    private static final Color SELECT_COLOR = Color.AQUA;
    private static final Color HIDDEN_COLOR = new Color(0, 0, 0, 0);
    private Color overrideColor;

    private final Edge edge;
    private final Polyline curveNode;

    public EdgeProjectedView(Edge edge, View context) {
        super(context);
        this.edge = edge;

        curveNode = new Polyline();
        curveNode.setStrokeWidth(4);
        getChildren().add(curveNode);
    }

    @Override
    public void repaint() {

        if (null == edge) {
            return;
        }

        List<Point3DC> points = edge.halfEdge1().tessellate();

        Double[] point2ds = new Double[points.size() * 2];
        for (int i = 0; i < points.size(); i++) {
            Point3DC p3d = points.get(i);
            Point2D p2d = project(p3d);

            point2ds[2 * i] = p2d.getX();
            point2ds[2 * i + 1] = p2d.getY();
        }

        curveNode.getPoints().setAll(point2ds);
        updateCurveColor();
    }

    @Override
    public void setHovered(boolean hovered) {
        super.setHovered(hovered);
        updateCurveColor();
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        updateCurveColor();
    }

    private void updateCurveColor() {
        if (isSelected()) {
            curveNode.setStroke(SELECT_COLOR);
        } else if (isHovered()) {
            curveNode.setStroke(HOVER_COLOR);
        } else if (null != overrideColor) {
            curveNode.setStroke(overrideColor);
        } else {
            curveNode.setStroke(HIDDEN_COLOR);
        }
    }

    @Override
    public void translate(Point3D translation) {
        if (null != edge) {

            edge.transform(Matrix3.createTranslation(
                    translation.getX(),
                    translation.getY(),
                    translation.getZ()));

            repaint();
        }
    }

    @Override
    public Node getPhysicalNode() {
        return curveNode;
    }
    
    public void setOverrideColor(Color color) {
        overrideColor = color;
    }
}
