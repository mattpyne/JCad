package org.pyne.jcad.client.workspace;

import javafx.scene.control.TreeItem;
import org.pyne.jcad.core.entity.Entity;

/**
 *
 * @author Matthew
 */
public class EntityTreeItem extends TreeItem {
    
    public EntityTreeItem(Entity entity) {
        super("Entity");
        
        entity.getComponents().stream()
                .map(ComponentTreeItemFactory::create)
                .forEach(compItem -> getChildren().add(compItem));
    }
    
}
