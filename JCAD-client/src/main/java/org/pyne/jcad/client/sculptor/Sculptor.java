package org.pyne.jcad.client.sculptor;

import com.pixelduke.control.ribbon.RibbonGroup;
import javafx.scene.control.Button;
import javax.inject.Inject;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.ClientInjector;
import org.pyne.jcad.client.ClientInjectorImp;

/**
 *
 * @author Matthew Pyne
 */
@Plugin(name = "Sculptor")
public class Sculptor {

    private final SculptorController controller;
    private final Button extrudeButton;

    @Inject
    public Sculptor(ClientInjectorImp clientInjector) {
        super();

        RibbonGroup group = new RibbonGroup();
        group.setTitle("Sculptor");

        extrudeButton = new Button("Extrude");
        group.getNodes().add(extrudeButton);
        
        clientInjector.addToTopTool(group, "Home");

        controller = new SculptorController(this);
    }

    public Button extrudeButton() {
        return extrudeButton;
    }

    public SculptorController controller() {
        return controller;
    }
}
