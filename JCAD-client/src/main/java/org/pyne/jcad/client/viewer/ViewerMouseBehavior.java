package org.pyne.jcad.client.viewer;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.core.math.CadMath;

/**
 * @author matt pyne
 */
public class ViewerMouseBehavior implements EventHandler<MouseEvent> {

    private double mousePosX;
    private double mousePosY;
    private double mouseOldX;
    private double mouseOldY;
    private double mouseDeltaY;
    private double mouseDeltaX;
    private boolean enabled;
    private boolean modifiedCamera;

    //We'll use custom Rotate transforms to manage the coordinate conversions
    private final ViewerController controller;

    /**
     * @param controller
     */
    public ViewerMouseBehavior(ViewerController controller) {
        this.controller = controller;
        enabled = true;
    }

    public void enable(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void handle(MouseEvent me) {

        if (!enabled) {
            return;
        }

        me.consume();

        Viewer2D viewer2D = JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class);
        Viewer2DController viewer2DController = viewer2D
                .controller();

        viewer2D.streamNodeBy(ProjectedView.class)
                .forEach(sketch -> sketch.setHovered(false));

        if (MouseEvent.MOUSE_PRESSED.equals(me.getEventType())) {

            viewer2DController.disable();

//            if (!me.isControlDown()) {
                viewer2D.streamNodeBy(ProjectedView.class)
                        .forEach(sketch -> sketch.setSelected(false));
//            }

            mousePressed(me);
        } else if (MouseEvent.MOUSE_DRAGGED.equals(me.getEventType())) {

            viewer2DController.disable();

            mouseDragged(me);
        } else if (MouseEvent.MOUSE_RELEASED.equals(me.getEventType())) {

            viewer2DController.enable();

            if (modifiedCamera) {

                modifiedCamera = false;
                viewer2DController.repaintView();
            }
        }
    }

    /**
     * @param me
     */
    private void mouseDragged(MouseEvent me) {
        mouseOldX = mousePosX;
        mouseOldY = mousePosY;
        mousePosX = me.getSceneX();
        mousePosY = me.getSceneY();
        mouseDeltaX = (mousePosX - mouseOldX);
        mouseDeltaY = (mousePosY - mouseOldY);

        double modifier = 10.0;
        double modifierFactor = 0.1;

        if (me.isControlDown()) {
            modifier = 1;
        }
        if (me.isShiftDown()) {
            modifier = 50.0;
        }
        if (me.isPrimaryButtonDown()) {
            if (me.isAltDown()) { //roll

                Rotate rotateZ = controller.rotateZ();
                rotateZ.setAngle(((rotateZ.getAngle() + mouseDeltaX * modifierFactor * modifier * 2.0) % 360 + 540) % 360 - 180);
                modifiedCamera = true;
            } else {

                Rotate rotateY = controller.rotateY();
                rotateY.setAngle(((rotateY.getAngle() + mouseDeltaX * modifierFactor * modifier * 2.0) % 360 + 540) % 360 - 180);

                Rotate rotateX = controller.rotateX();
                rotateX.setAngle(CadMath.clamp(
                        (((rotateX.getAngle() - mouseDeltaY * modifierFactor * modifier * 2.0) % 360 + 540) % 360 - 180),
                        -90, 90)); // -
                modifiedCamera = true;
            }

        } else if (me.isSecondaryButtonDown()) {

//            AnchorPane pane = (AnchorPane) me.getSource();
//            pane.setTranslateX(pane.getTranslateX() + mouseDeltaX);
//            pane.setTranslateY(pane.getTranslateY() + mouseDeltaY);
            Translate translate = controller.translate();
            translate.setX(translate.getX() + mouseDeltaX);
            translate.setY(translate.getY() + mouseDeltaY);
            modifiedCamera = true;
        }

    }

    /**
     * @param me
     */
    private void mousePressed(MouseEvent me) {
        mousePosX = me.getSceneX();
        mousePosY = me.getSceneY();
        mouseOldX = me.getSceneX();
        mouseOldY = me.getSceneY();

        setPickState(me.getPickResult());
    }

    private void setPickState(PickResult result) {
        if (result.getIntersectedNode() == null) {

            System.out.println("Scene\n\n"
                    + result.getIntersectedPoint() + "\n"
                    + result.getIntersectedTexCoord() + "\n"
                    + result.getIntersectedFace() + "\n"
                    + String.format("%.1f", result.getIntersectedDistance()));
        } else {

            System.out.println(result.getIntersectedNode().getId() + "\n"
                    + result.getIntersectedNode() + "\n"
                    + result.getIntersectedPoint() + "\n"
                    + result.getIntersectedTexCoord() + "\n"
                    + result.getIntersectedFace() + "\n"
                    + String.format("%.1f", result.getIntersectedDistance()));
        }
    }

}
