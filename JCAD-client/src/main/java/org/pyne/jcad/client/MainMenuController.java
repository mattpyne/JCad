package org.pyne.jcad.client;

import java.io.File;
import javafx.fxml.FXML;

/**
 *
 * @author matts
 */
public class MainMenuController {

    @FXML
    public void onLoadModel() {

        File file = JCadWindow.fileChooser.showOpenDialog(JCadWindow.mainWin);

        JCadApp.DOCUMENT_MODEL.loadData(file);
    }

    @FXML
    public void onSaveModel() {

        File file = JCadWindow.fileChooser.showSaveDialog(JCadWindow.mainWin);

        JCadApp.DOCUMENT_MODEL.saveData(file);
    }

    @FXML
    public void onSketch() {
        
    }

}
