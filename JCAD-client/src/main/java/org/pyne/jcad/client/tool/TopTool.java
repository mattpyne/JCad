package org.pyne.jcad.client.tool;

import com.pixelduke.control.Ribbon;

/**
 *
 * @author Matthew
 */
public class TopTool extends Ribbon {
    
    public TopTool() {
        super();
        setId("TopTool");
        
        setMaxHeight(-1);
        setMaxWidth(-1);
        setMinHeight(Double.NEGATIVE_INFINITY);
        setMinWidth(Double.NEGATIVE_INFINITY);
    }
}
