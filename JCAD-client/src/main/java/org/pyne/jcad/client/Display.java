package org.pyne.jcad.client;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.pyne.jcad.core.math.BBox;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.MeshData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Display {
    static int winWidth = 1600;
    static int winHeight = 1000;
    static int winBorder = 50;


    public static void display(Surface surface, List<Loop> loops, List<HalfEdge> newEdges) {
        Group geomGroup = new Group();

        List<Point3DC> points = new ArrayList<>();
        List<List<Point3DC>> edgePoints = new ArrayList<>();
        for (HalfEdge edge : newEdges) {
            List<Point3DC> uvPoints = CadMath.convertPointsToUV(surface, edge.tessellate());
            edgePoints.add(uvPoints);
            points.addAll(uvPoints);
        }

        for (Loop loop : loops) {

            points.addAll(loop.tessellateUV());
        }

        BBox box = bounds(points);
        Point3DC translation = translation(box);
        double scale = scale(box);

        for (List<Point3DC> ep : edgePoints) {
            drawPathWithArrows(
                    ep,
                    geomGroup,
                    translation,
                    scale,
                    false);
        }


        for (Loop loop : loops) {
            drawPathWithArrows(
                    loop.tessellateUV(),
                    geomGroup,
                    translation,
                    scale,
                    loop.isClosed());
        }

        displayGeom(geomGroup);
    }

    public static void display(Surface surface, List<HalfEdge> edges) {
        Group geomGroup = new Group();

        List<Point3DC> points = new ArrayList<>();
        List<List<Point3DC>> edgePoints = new ArrayList<>();
        for (HalfEdge edge : edges) {
            List<Point3DC> uvPoints = CadMath.convertPointsToUV(surface, edge.tessellate());
            edgePoints.add(uvPoints);
            points.addAll(uvPoints);
        }
        BBox box = bounds(points);
        Point3DC translation = translation(box);
        double scale = scale(box);

        for (List<Point3DC> ep : edgePoints) {
            drawPathWithArrows(
                    ep,
                    geomGroup,
                    translation,
                    scale,
                    false);
        }

        displayGeom(geomGroup);
    }

    public static void display(Loop loop) {
        Group geomGroup = new Group();

        List<Point3DC> points = loop.tessellateUV();

        BBox box = bounds(points);
        Point3DC translation = translation(box);
        double scale = scale(box);

        drawPathWithArrows(
                points,
                geomGroup,
                translation,
                scale,
                loop.isClosed());

        displayGeom(geomGroup);
    }

    public static void displayUVPathAndTriangles(List<Point3DC> outerPointLoop, Surface surf) {
//        if (true) {
//            return;
//        }
        BBox box = bounds(outerPointLoop);

        Point3DC translation = translation(box);
        double scale = scale(box);

        Group geomGroup = new Group();
        drawUVSurfTess(
                surf,
                geomGroup,
                translation,
                scale);

        drawPathWithArrows(
                outerPointLoop,
                geomGroup,
                translation,
                scale,
                true);

        displayGeom(geomGroup);
    }

    private static void displayGeom(Group geomGroup) {
        StackPane root = new StackPane();
        root.getChildren().add(geomGroup);

        // Create operator
        AnimatedZoomOperator zoomOperator = new AnimatedZoomOperator();

        root.setOnScroll(event -> {
            double zoomFactor = 1.5;
            if (event.getDeltaY() <= 0) {
                // zoom out
                zoomFactor = 1 / zoomFactor;
            }
            zoomOperator.zoom(root, zoomFactor, event.getSceneX(), event.getSceneY());
            double finalZoomFactor = root.getScaleX();
            geomGroup.getChildren()
                    .stream()
                    .filter(Arrow.class::isInstance)
                    .map(Arrow.class::cast)
                    .forEach(ea -> ea.setStrokeWidth(Math.min(1. / finalZoomFactor, 1)));
        });

        Scene scene = new Scene(root, winWidth, winHeight);
        Stage dialog = new Stage();
        dialog.setScene(scene);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.showAndWait();
    }

    private static Point3DC translation(BBox box) {
        return box.min();
    }

    private static double scale(BBox box) {
        double scale =  (winWidth - winBorder * 2) / box.width();
        double scale2 = (winHeight - winBorder * 2) / box.height();
        if (scale > scale2) {
            scale = scale2;
        }
        return scale;
    }

    private static BBox bounds(List<Point3DC> outerPointLoop) {
        BBox box = new BBox();
        for (int i = 0; i < outerPointLoop.size(); i++) {
            Point3DC p1 = outerPointLoop.get(i);
            box.checkPoint(p1);
        }
        return box;
    }

    private static void drawUVSurfTess(
            Surface surf,
            Group geomGroup,
            Point3DC translation,
            double scale) {
        MeshData tess = surf.tessellate();

        surf.tessellate()
                .faces
                .stream()
                .filter(Objects::nonNull)
                .map(f -> f.stream()
                        .map(i -> surf.createWorkingPoint(
                                tess.uvs.get(i.intValue()),
                                new Point3DC(tess.points.get(i.intValue()))))
                        .collect(Collectors.toList()))
                .forEach(tri -> drawPathWithArrows(tri, geomGroup, translation, scale, true));
    }

    private static void drawPathWithArrows(
            List<Point3DC> path,
            Group geomGroup,
            Point3DC translation,
            double scale,
            boolean closePath) {

        double R = 0.;
        for (int i = 0; i < path.size(); i++) {
            int j = (i + 1) % path.size();
            if (j == 0 && !closePath) {
                return;
            }
            Point3DC p1 = path.get(i);
            Point3DC p2 = path.get(j);

            R = R + 1. / ((double) path.size());
            R = Math.min(1., R);
            Color color = Color.color(R, 0, 0);

            Arrow arrow = new Arrow();
            arrow.setColor(color);
            arrow.setStartX((p1.x() - translation.getX()) * scale + winBorder);
            arrow.setStartY((p1.y() + translation.getY()) * scale + winBorder);
            arrow.setEndX((p2.x() - translation.getX()) * scale + winBorder);
            arrow.setEndY((p2.y() + translation.getY()) * scale + winBorder);

            geomGroup.getChildren().add(arrow);
        }
    }

}


class Arrow extends Group {

    private final Line line;
    private final Line arrow1;
    private final Line arrow2;

    public Arrow() {
        this(new Line(), new Line(), new Line());
    }

    private static final double arrowLength = 10;
    private static final double arrowWidth = 3;

    private Arrow(Line line, Line arrow1, Line arrow2) {
        super(line, arrow1, arrow2);
        this.line = line;
        line.setStrokeWidth(1.0);
        line.setStrokeType(StrokeType.CENTERED);
        this.arrow1 = arrow1;
        arrow1.setStrokeWidth(1.0);
        arrow1.setStrokeType(StrokeType.CENTERED);
        this.arrow2 = arrow2;
        arrow2.setStrokeWidth(1.0);
        arrow2.setStrokeType(StrokeType.CENTERED);
        InvalidationListener updater = o -> {
            double ex = getEndX();
            double ey = getEndY();
            double sx = getStartX();
            double sy = getStartY();

            arrow1.setEndX(ex);
            arrow1.setEndY(ey);
            arrow2.setEndX(ex);
            arrow2.setEndY(ey);

            if (ex == sx && ey == sy) {
                // arrow parts of length 0
                arrow1.setStartX(ex);
                arrow1.setStartY(ey);
                arrow2.setStartX(ex);
                arrow2.setStartY(ey);
            } else {
                double factor = arrowLength / Math.hypot(sx - ex, sy - ey);
                double factorO = arrowWidth / Math.hypot(sx - ex, sy - ey);

                // part in direction of main line
                double dx = (sx - ex) * factor;
                double dy = (sy - ey) * factor;

                // part ortogonal to main line
                double ox = (sx - ex) * factorO;
                double oy = (sy - ey) * factorO;

                arrow1.setStartX(ex + dx - oy);
                arrow1.setStartY(ey + dy + ox);
                arrow2.setStartX(ex + dx + oy);
                arrow2.setStartY(ey + dy - ox);
            }
        };

        // add updater to properties
        startXProperty().addListener(updater);
        startYProperty().addListener(updater);
        endXProperty().addListener(updater);
        endYProperty().addListener(updater);
        updater.invalidated(null);
    }

    // start/end properties

    public final void setStartX(double value) {
        line.setStartX(value);
    }

    public final double getStartX() {
        return line.getStartX();
    }

    public final DoubleProperty startXProperty() {
        return line.startXProperty();
    }

    public final void setStartY(double value) {
        line.setStartY(value);
    }

    public final double getStartY() {
        return line.getStartY();
    }

    public final DoubleProperty startYProperty() {
        return line.startYProperty();
    }

    public final void setEndX(double value) {
        line.setEndX(value);
    }

    public final double getEndX() {
        return line.getEndX();
    }

    public final DoubleProperty endXProperty() {
        return line.endXProperty();
    }

    public final void setEndY(double value) {
        line.setEndY(value);
    }

    public final double getEndY() {
        return line.getEndY();
    }

    public final DoubleProperty endYProperty() {
        return line.endYProperty();
    }

    public final void setColor(Color c) {
        line.setFill(c);
        arrow1.setFill(c);
        arrow2.setFill(c);
        line.setStroke(c);
        arrow2.setStroke(c);
        arrow1.setStroke(c);
    }

    public void setStrokeWidth(double min) {
        line.setStrokeWidth(min);
        arrow1.setStrokeWidth(min);
        arrow2.setStrokeWidth(min);
    }
}

class AnimatedZoomOperator {

    private Timeline timeline;

    public AnimatedZoomOperator() {
        this.timeline = new Timeline(60);
    }

    public void zoom(Node node, double factor, double x, double y) {
        // determine scale
        double oldScale = node.getScaleX();
        double scale = oldScale * factor;
        double f = (scale / oldScale) - 1;

        // determine offset that we will have to move the node
        Bounds bounds = node.localToScene(node.getBoundsInLocal());
        double dx = (x - (bounds.getWidth() / 2 + bounds.getMinX()));
        double dy = (y - (bounds.getHeight() / 2 + bounds.getMinY()));

        // timeline that scales and moves the node
        timeline.getKeyFrames().clear();
        timeline.getKeyFrames().addAll(
                new KeyFrame(Duration.millis(200), new KeyValue(node.translateXProperty(), node.getTranslateX() - f * dx)),
                new KeyFrame(Duration.millis(200), new KeyValue(node.translateYProperty(), node.getTranslateY() - f * dy)),
                new KeyFrame(Duration.millis(200), new KeyValue(node.scaleXProperty(), scale)),
                new KeyFrame(Duration.millis(200), new KeyValue(node.scaleYProperty(), scale))
        );
        timeline.play();
    }
}
