package org.pyne.jcad.client.viewer;

import goryachev.fxdock.FxDockPane;
import javafx.geometry.Insets;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

import javax.inject.Inject;

import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.*;

@Plugin(name = "Viewer")
public class Viewer extends FxDockPane implements DocumentModelChangedListener {

    public static final String MODEL_VIEW_ID = "MODEL_VIEW_ID";

    private final BorderPane viewPane;
    private final SubScene subScene;
    private final Group viewModel;
    private final Camera camera;

    private final BorderPane view3dPane;

    private final ViewerController controller;

    @Inject
    public Viewer(ClientInjectorImp clientInjector, Viewer2D viewer2D) {
        super(Id());

        viewPane = new BorderPane();
        view3dPane = new BorderPane();
        viewModel = new Group();
        viewModel.setId(MODEL_VIEW_ID);

        subScene = new SubScene(viewModel, 1000, 500, true,
                SceneAntialiasing.BALANCED);

        subScene.widthProperty().bind(viewPane.widthProperty());
        subScene.heightProperty().bind(viewPane.heightProperty());

        viewModel.layoutXProperty().bind(viewPane.widthProperty().divide(2));
        viewModel.layoutYProperty().bind(viewPane.heightProperty().divide(2));

        camera = new PerspectiveCamera(true);
        subScene.setCamera(camera);
        camera.setNearClip(0.001);
        camera.setFarClip(100);
        camera.setEffect(new Lighting(new Light.Point()));

        // Set the scene's background color as a gradient from light to dark
        // grey
        Stop[] stops = new Stop[]{
                new Stop(0, Color.rgb(166, 166, 166)),
                new Stop(1, Color.rgb(83, 83, 83))};
        setBackground(new Background(new BackgroundFill(
                new LinearGradient(0, 0, 0, 1, true,
                        CycleMethod.NO_CYCLE, stops),
                new CornerRadii(0), new Insets(0))));

        view3dPane.getChildren().add(subScene);
        viewPane.getChildren().add(view3dPane);
        viewPane.getChildren().add(viewer2D);

        controller = new ViewerController(this);

        JCadApp.DOCUMENT_MODEL.listen(this);

        setClosable(false);
        setTitle("Viewer");
        setContent(viewPane);

        clientInjector.addPaneToMainSplitPane(this);
    }

    public Pane viewPane() {
        return viewPane;
    }

    public Pane view3dPane() {
        return view3dPane;
    }

    public Camera camera() {
        return camera;
    }

    public Group viewModel() {
        return viewModel;
    }

    public SubScene subScene() {
        return subScene;
    }

    public static String Id() {
        return JCadFxDockGenerator.VIEWER;
    }

    public ViewerController controller() {
        return controller;
    }

    @Override
    public void onModelChanged() {
        controller.updateView(JCadApp.DOCUMENT_MODEL.getModel());
    }
}
