package org.pyne.jcad.client.viewer;

import java.util.List;
import javafx.scene.transform.Translate;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.HalfEdge;

/**
 *
 * @author Matthew Pyne
 */
public class EdgeView extends View {

    private final Edge edge;

    public EdgeView(Edge edge) {
        this.edge = edge;

        List<Point3DC> points = edge.halfEdge1().tessellate();

        for (int i = 0; i < points.size() - 1; i++) {

            getChildren().add(new Line3D(points.get(i), points.get(i + 1)));
            
//            Line3D selectLine = new Line3D(points.get(i), points.get(i + 1));
//            selectLine.setDrawMode(DrawMode.FILL);
//            selectLine.setMaterial(new PhongMaterial(Color.web("#00000000")));
//            selectLine.setThickness(10);
//            getChildren().add(selectLine);
        }

        Point3DC offset = Point3DC.ZERO;

        if (goodSurfaceNormal(edge.halfEdge1())) {
            
            Point3DC normal1 = edge.halfEdge1()
                    .loop()
                    .face()
                    .surface()
                    .normal(points.get(0));

            offset = normal1.times(0.001);
        }

        if (goodSurfaceNormal(edge.halfEdge2())) {
            
            Point3DC normal2 = edge.halfEdge2()
                    .loop()
                    .face()
                    .surface()
                    .normal(points.get(0));
            
            offset = offset.plus(normal2.times(0.001));
        }

        getTransforms().add(new Translate(offset.x(), offset.y(), offset.z()));
    }

    public Edge getEdge() {
        return edge;
    }

    private boolean goodSurfaceNormal(HalfEdge he) {
        return null != he.loop() 
                && null != he.loop().face()
                && null != he.loop().face().surface();
    }

}
