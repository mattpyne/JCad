package org.pyne.jcad.client.viewer;

import java.util.List;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.MeshView;
import org.pyne.jcad.client.Display;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.creators.BrepTessellator;
import org.pyne.jcad.core.math.brep.Face;

/**
 * @author Matthew Pyne
 */
public class FaceView extends View {

    private final Face face;
    private final MeshView mesh;

    public FaceView(Face face) {
        this.face = face;

        List<List<Point3DC>> surfaceTriangles = BrepTessellator.tessellate(face);

        mesh = new MeshView(JavaFxTriangles.toJavaFXMeshSimple(surfaceTriangles));
        mesh.setCullFace(CullFace.NONE);

        PhongMaterial material = new PhongMaterial(new Color(0.7, 0.7, 0.7, 1));
        mesh.setMaterial(material);
        getChildren().add(mesh);

    }

    public Face face() {
        return face;
    }

    @Override
    public Node getPhysicalNode() {
        return mesh;
    }

    public void setMaterial(PhongMaterial phongMaterial) {
        mesh.setMaterial(phongMaterial);
    }

}
