package org.pyne.jcad.client.viewer;

import java.util.List;
import javafx.scene.shape.TriangleMesh;
import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author Matthew Pyne
 */
public class JavaFxTriangles {

    /**
     * JavaOne requires triangular polygons.
     *
     * @param triangles
     * @return
     */
    public static TriangleMesh toJavaFXMeshSimple(List<List<Point3DC>> triangles) {

        TriangleMesh mesh = new TriangleMesh();

        int counter = 0;
        for (List<Point3DC> p : triangles) {

            Point3DC firstPoint = p.get(2);

            mesh.getPoints().addAll(
                    (float) firstPoint.x(),
                    (float) firstPoint.y(),
                    (float) firstPoint.z());

            mesh.getTexCoords().addAll(0);                                      // texture (not covered)
            mesh.getTexCoords().addAll(0);

            Point3DC secondPoint = p.get(1);

            mesh.getPoints().addAll(
                    (float) secondPoint.x(),
                    (float) secondPoint.y(),
                    (float) secondPoint.z());

            mesh.getTexCoords().addAll(0);                                      // texture (not covered)
            mesh.getTexCoords().addAll(0);

            Point3DC thingPoint = p.get(0);

            mesh.getPoints().addAll(
                    (float) thingPoint.x(),
                    (float) thingPoint.y(),
                    (float) thingPoint.z());

            mesh.getTexCoords().addAll(0);                                      // texture (not covered)
            mesh.getTexCoords().addAll(0);

            mesh.getFaces().addAll(
                    counter, // first vertex
                    0, // texture (not covered)
                    counter + 1, // second vertex
                    0, // texture (not covered)
                    counter + 2, // third vertex
                    0 // texture (not covered)
            );
            counter += 3;
        }

        return mesh;
    }
}
