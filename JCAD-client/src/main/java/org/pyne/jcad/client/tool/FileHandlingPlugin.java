package org.pyne.jcad.client.tool;

import com.pixelduke.control.ribbon.RibbonGroup;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.inject.Inject;

import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.ClientInjectorImp;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.JCadWindow;
import org.pyne.jcad.client.ClientInjector;

/**
 * @author Matthew Pyne
 */
@Plugin(name = "FileHandler")
public class FileHandlingPlugin {

    @Inject
    public FileHandlingPlugin(ClientInjectorImp clientInjector) {
        super();

        RibbonGroup fileGroup = new RibbonGroup();
        fileGroup.setTitle("File");

        Image openFileImage = new Image(getClass().getResourceAsStream("icons8_Open_32px_3.png"));
        Button openFileButton = new Button("Open", new ImageView(openFileImage));
        openFileButton.setContentDisplay(ContentDisplay.TOP);
        openFileButton.setWrapText(true);
        openFileButton.setStyle("big");
        openFileButton.setOnAction(this::onLoadModel);
        fileGroup.getNodes().add(openFileButton);

        Image newImage = new Image(getClass().getResourceAsStream("icons8_File_32px_1.png"));
        Button newButton = new Button("New", new ImageView(newImage));
        newButton.setContentDisplay(ContentDisplay.TOP);
        newButton.setWrapText(true);
        newButton.setStyle("big");
//        newButton.setOnAction(this::onLoadModel);
        fileGroup.getNodes().add(newButton);

        Image saveFileImage = new Image(getClass().getResourceAsStream("icons8_Save_32px.png"));
        Button saveFileButton = new Button("Save", new ImageView(saveFileImage));
        saveFileButton.setContentDisplay(ContentDisplay.TOP);
        saveFileButton.setWrapText(true);
        saveFileButton.setStyle("big");
        saveFileButton.setOnAction(this::onSaveModel);
        fileGroup.getNodes().add(saveFileButton);

        Image saveAsFileImage = new Image(getClass().getResourceAsStream("icons8_Save_as_32px.png"));
        Button saveAsFileButton = new Button("Save as", new ImageView(saveAsFileImage));
        saveAsFileButton.setContentDisplay(ContentDisplay.TOP);
        saveAsFileButton.setWrapText(true);
        saveAsFileButton.setStyle("big");
//        saveAsFileButton.setOnAction(this::onLoadModel);
        fileGroup.getNodes().add(saveAsFileButton);

        clientInjector.addToTopToolStart(fileGroup, "Home");
    }

    private void onLoadModel(ActionEvent event) {

        File file = JCadWindow.fileChooser.showOpenDialog(JCadWindow.mainWin);

        if (null != file) {
            JCadApp.DOCUMENT_MODEL.loadData(file);
        }

    }

    private void onSaveModel(ActionEvent event) {

        File file = JCadWindow.fileChooser.showSaveDialog(JCadWindow.mainWin);

        if (null != file) {
            JCadApp.DOCUMENT_MODEL.saveData(file);
        }
    }
}
