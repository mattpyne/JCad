package org.pyne.jcad.client.workspace;

import javafx.beans.InvalidationListener;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.pyne.jcad.core.entity.Expression;

public class ExpressionEditor extends GridPane {

    private Expression expression;

    private final Label label;
    private final TextField valueField;
    private final TextField evalField;

    public ExpressionEditor(String labelStr, Expression expression, InvalidationListener listener) {
        this(labelStr, listener);
        this.expression = expression;

        refresh();
    }

    public ExpressionEditor(String labelStr, InvalidationListener listener) {
        label = new Label(labelStr);

        evalField = new TextField();
        evalField.setDisable(true);

        valueField = new TextField();
        valueField.setPromptText(labelStr);
        valueField.textProperty().addListener((event -> {
            expression.setValue(valueField.getText());
            if (expression.checkSyntax()) {
                evalField.setText(String.format("%.3f", expression.eval()));
            } else {
                evalField.setText("Syntax Error");
            }
        }));
        valueField.textProperty().addListener(listener);

        add(label, 0, 0);
        add(evalField, 0, 1);
        add(valueField, 1, 1);
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
        refresh();
    }

    private void refresh() {
        if (null != expression) {
            valueField.setText(expression.getValue());
            evalField.setText(String.format("%.4f", expression.eval()));
        }
    }

}
