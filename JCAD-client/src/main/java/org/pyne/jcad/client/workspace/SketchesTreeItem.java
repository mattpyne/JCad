package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.Sketches;

/**
 *
 * @author Matthew
 */
public class SketchesTreeItem extends ComponentTreeItem {

    public SketchesTreeItem(Sketches component) {
        super(component);

        component.stream()
                .map(SketchTreeItem::new)
                .forEach(opItem -> getChildren().add(opItem));
    }

}
