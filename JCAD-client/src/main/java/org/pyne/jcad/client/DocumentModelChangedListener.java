package org.pyne.jcad.client;

public interface DocumentModelChangedListener {

    void onModelChanged();
}
