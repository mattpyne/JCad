package org.pyne.jcad.client.viewer;

import com.sun.javafx.geom.Vec3d;
import com.sun.javafx.scene.CameraHelper;
import com.sun.javafx.scene.NodeHelper;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.SubScene;
import org.pyne.jcad.client.FxNodeSearchable;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author Matthew Pyne
 */
public abstract class ProjectedView extends View implements FxNodeSearchable {

    private View context;
    private Camera camera;

    public ProjectedView(View context) {
        super();
        this.context = context;
    }

    public abstract void repaint();

    public View context() {
        return context;
    }

    public void setContext(View context) {
        this.context = context;
    }

    protected Camera camera() {
        if (null == camera) {
            camera = JCadApp.PLUGIN_MANAGER
                    .getPlugin(Viewer.class)
                    .camera();
        }
        return camera;
    }
    
    public Point3DC unProject(Point3DC scenePt) {
        return unProject(new Point3D(scenePt.x(), scenePt.y(), scenePt.z()));
    }
    
    public Point3DC unProject(Point3D scenePt) {
        Point3D temp = ViewUtils.rayCastPt(
                camera(),
                new Point2D(scenePt.getX(), scenePt.getY()),
                getScene());
        return new Point3DC(temp.getX(), temp.getY(), temp.getZ());
    }
    
    public Point2D project(Point3DC modelPt) {
        return CameraHelper.project(camera(), context().localToScene(modelPt.x(), modelPt.y(), modelPt.z()));
    }

    public abstract void translate(Point3D translation);

}
