package org.pyne.jcad.client.viewer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.tool.ModelViewPlugin;
import org.pyne.jcad.client.workspace.EntityTreeViewer;
import org.pyne.jcad.core.entity.Constructor;
import org.pyne.jcad.core.entity.Entity;
import org.pyne.jcad.core.entity.Geometry;
import org.pyne.jcad.core.entity.IOperation;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Topo;
import org.pyne.jcad.core.math.geom.ParametricCurve;

public class ViewerController {

    private final Viewer viewer;

    private final ViewerMouseBehavior mouseBehavior;
    private final ViewerScrollBehavior scrollBehavior;

    private final Rotate rotateX = new Rotate(0, Rotate.X_AXIS);
    private final Rotate rotateY = new Rotate(0, Rotate.Y_AXIS);
    private final Rotate rotateZ = new Rotate(0, Rotate.Z_AXIS);
    private final Translate translate = new Translate();

    /**
     * The gizmo containing the three lines that will display the axes.
     */
    private TransformGizmo axes;

    /**
     * The wireframe square that displays the XZ plane.
     */
    private Box plane;

    /**
     * Model in view
     */
    private EntityView subject;

    /**
     *
     * @param viewer
     */
    public ViewerController(Viewer viewer) {

        this.viewer = viewer;

        Pane viewerPane = viewer.view3dPane();

        mouseBehavior = new ViewerMouseBehavior(this);
        viewerPane.addEventHandler(MouseEvent.ANY, mouseBehavior);

        scrollBehavior = new ViewerScrollBehavior(this);
        viewerPane.addEventHandler(ScrollEvent.ANY, scrollBehavior);

        viewer.viewModel()
                .getTransforms()
                .addAll(translate, rotateX, rotateY, rotateZ);
    }

    public Viewer viewer() {
        return viewer;
    }

    public void updateView(Entity entity) {

        entity.update();

        viewer.viewModel().getChildren().clear();

        subject = new EntityView(entity);
        viewer.viewModel().getChildren().add(subject);

        JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer2D.class)
                .controller()
                .recalculateView();

        resetView();
//        setupSceneInternals(subject);
    }

    public EntityView getSubject() {
        return subject;
    }

    public void setSubject(EntityView subject) {
        this.subject = subject;
    }

    public void refreshView() {
        subject.refresh();

        JCadApp.PLUGIN_MANAGER.getPlugin(EntityTreeViewer.class)
                .controller()
                .refreshView();
    }

    public void refreshColor() {
        subject.refreshColor();
    }

    public void refreshGeometry() {
        subject.refresh();
    }

//    protected void setupSceneInternals(Group node) {
//        // Create scene plane for frame of reference.
//        Bounds subjectBounds = subject.getBoundsInLocal();
//        plane = new Box(subjectBounds.getWidth() * 1.5, 0, subjectBounds.getDepth() * 1.5);
//        plane.setMouseTransparent(true);
//        plane.setDrawMode(DrawMode.LINE);
//        plane.setMaterial(new PhongMaterial(Color.WHITE));
//
//        axes = new TransformGizmo(subjectBounds.getWidth());
//        axes.showHandles(true);
//
//        node.getChildren().addAll(axes, plane);
//    }
    public Rotate rotateX() {
        return rotateX;
    }

    public Rotate rotateY() {
        return rotateY;
    }

    public Rotate rotateZ() {
        return rotateZ;
    }

    public Translate translate() {
        return translate;
    }

    public ViewerMouseBehavior mouseBehavior() {
        return mouseBehavior;
    }

    public ViewerScrollBehavior scrollBehavior() {
        return scrollBehavior;
    }

    /**
     * Returns all selected views. 3d and 2d.
     *
     * @return
     */
    public List<View> getSelectedViews() {
        return ViewExplorer.streamSelected(viewer.viewPane()).collect(Collectors.toList());
    }

    /**
     * Returns all selected views. 3d and 2d.
     *
     * @return
     */
    public List<Loop> getSelectedLoops() {
        List<View> selectedViews = getSelectedViews();

        List<Edge> edges = selectedViews.stream()
                .filter(CurveProjectedView.class::isInstance)
                .map(CurveProjectedView.class::cast)
                .map(CurveProjectedView::curve)
                .flatMap(curve -> {
                    if (curve.getType() == ParametricCurve.TYPE.CIRCLE
                            || (curve.getType() == ParametricCurve.TYPE.ARC
                            && curve.point(curve.domain().min.doubleValue()).equals(curve.point(curve.domain().max.doubleValue())))) {

                        List<ParametricCurve> split = curve.split(0.5);

                        return Stream.of(
                                new Edge(new BrepCurve(split.get(0))),
                                new Edge(new BrepCurve(split.get(1))));
                    }
                    return Stream.of(new Edge(new BrepCurve(curve)));
                })
                .collect(Collectors.toList());

        List<Loop> edgeLoops = new ArrayList<>();

        for (Edge edge : edges) {

            boolean attachedEdge = false;
            outerloop:
            for (Loop edgeLoop : edgeLoops) {
                for (HalfEdge he : edgeLoop.halfEdges()) {

                    if (he.vertexA().equals(edge.halfEdge1().vertexA())) {
                        edgeLoop.addEdge(edge.halfEdge2());
                        attachedEdge = true;
                        break outerloop;
                    } else if (he.vertexA().equals(edge.halfEdge1().vertexB())) {
                        edgeLoop.addEdge(edge.halfEdge1());
                        attachedEdge = true;
                        break outerloop;
                    } else if (he.vertexB().equals(edge.halfEdge1().vertexA())) {
                        edgeLoop.addEdge(edge.halfEdge1());
                        attachedEdge = true;
                        break outerloop;
                    } else if (he.vertexB().equals(edge.halfEdge1().vertexB())) {
                        edgeLoop.addEdge(edge.halfEdge2());
                        attachedEdge = true;
                        break outerloop;
                    }
                }
            }

            if (!attachedEdge) {
                Loop loop = new Loop();
                loop.addEdge(edge.halfEdge1());
                edgeLoops.add(loop);
            }
        }

        List<Loop> loops = new ArrayList<>();
        for (Loop edgeLoop : edgeLoops) {
            edgeLoop.link();

            if (edgeLoop.isClosed()) {
                loops.add(edgeLoop);
            }
        }

        selectedViews.stream()
                .filter(FaceView.class::isInstance)
                .map(FaceView.class::cast)
                .map(FaceView::face)
                .flatMap(Face::streamLoops)
                .forEach(loop -> loops.add(loop));

        return loops;
    }

    public void resetView() {
        viewer.viewPane().setScaleX(1.);
        viewer.viewPane().setScaleY(1.);
        viewer.viewPane().setScaleZ(1.);
        viewer.viewPane().setTranslateX(0);
        viewer.viewPane().setTranslateY(0);
        viewer.viewPane().setTranslateZ(0);
        viewer.viewPane().setRotate(0);

        Bounds boundsInLocal = subject.getBoundsInLocal();
        viewer.camera().setTranslateX(viewer.getWidth() / 2);
        viewer.camera().setTranslateY(viewer.getHeight() / 2.);
        viewer.camera().setTranslateZ(-boundsInLocal.getDepth() * 4);

        viewer.camera().setFarClip(boundsInLocal.getDepth() * 100);
        viewer.camera().setNearClip(boundsInLocal.getDepth() / 100);

        rotateX.setAngle(0);
        rotateY.setAngle(0);
        rotateZ.setAngle(0);
        translate.setX(0.);
        translate.setY(0.);
        translate.setZ(0.);
    }

    public void updateView(IOperation operation) {
        Entity entity = getSubject().entity();
        entity.getComponent(Constructor.class).ifPresent(constructor -> {

            int indexOp = constructor.operations().indexOf(operation);

            if (-1 == indexOp) {

                constructor.add(operation);
                operation.onApply(entity);

                refreshGeometry();

                JCadApp.PLUGIN_MANAGER.getPlugin(EntityTreeViewer.class)
                        .controller()
                        .refreshView();
            } else {

                entity.getComponent(Geometry.class).ifPresent(geom -> {

                    Topo topoAt = constructor.operations().get(indexOp - 1).topoAt();
                    if (null != topoAt) {
                        geom.setTopo(topoAt);

                        operation.onApply(entity);

                        refreshGeometry();
                    }
                });
            }
        });

    }

}
