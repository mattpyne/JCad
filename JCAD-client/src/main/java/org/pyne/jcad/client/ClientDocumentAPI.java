package org.pyne.jcad.client;

import javax.inject.Inject;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.core.ModelMaker;
import org.pyne.jcad.core.entity.Connections;
import org.pyne.jcad.core.entity.Entity;

/**
 *
 * @author Matthew Pyne
 */
@Plugin(name = "ClientDocumentAPI")
public class ClientDocumentAPI {
    
    private final ModelMaker modelMaker;
    
    @Inject
    public ClientDocumentAPI(ModelMaker modelMaker) {
        this.modelMaker = modelMaker;
    }
    
    /**
     * Adds a new entity to the documentModel of the client. Either will
     * add a new root entity to the document or to the root entity of the
     * document. Will all ways return a new entity.
     * 
     * @return 
     */
    public Entity addNewEntity() {
        
        Entity rootEntity = JCadApp.DOCUMENT_MODEL.getModel();
        if (null == rootEntity) {
            
            Entity newEntity = modelMaker.entityFactory().createEmpty();
            
            JCadApp.DOCUMENT_MODEL.setModel(newEntity);
            
            return newEntity;
        }
        
        Connections connections = rootEntity.getComponent(Connections.class)
                .orElseGet(() -> {
                    
                    Connections connec = new Connections();
                    
                    rootEntity.addComponent(connec);
                    
                    return connec;
                });

        Entity newEntity = modelMaker.entityFactory().createEmpty();
        
        connections.add(newEntity);
        
        return newEntity;
    }

    public DocumentModel getDocument() {
        return JCadApp.DOCUMENT_MODEL;
    }
}
