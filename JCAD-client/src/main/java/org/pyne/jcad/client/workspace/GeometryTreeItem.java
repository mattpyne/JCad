package org.pyne.jcad.client.workspace;

import org.pyne.jcad.core.entity.Geometry;

/**
 *
 * @author Matthew Pyne
 */
public class GeometryTreeItem extends ComponentTreeItem {
    
    public GeometryTreeItem(Geometry geom) {
        super(geom);
    }
    
}
