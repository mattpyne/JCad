package org.pyne.jcad.client.viewer;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import org.pyne.jcad.client.FxNodeSearchable;

/**
 *
 * @author Matthew Pyne
 */
public abstract class View extends Group implements FxNodeSearchable {

    private boolean selected;
    private boolean hovered;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isHovered() {
        return hovered;
    }

    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

    public Node getPhysicalNode() {
        return this;
    }

    public void setMaterial(PhongMaterial phongMaterial) {
        nodeBy(MeshView.class).forEach(mv -> mv.setMaterial(phongMaterial));
    }

}
