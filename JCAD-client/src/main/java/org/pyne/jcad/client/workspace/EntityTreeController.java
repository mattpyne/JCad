package org.pyne.jcad.client.workspace;

import javafx.scene.control.TreeItem;
import org.pyne.jcad.client.ClientDocumentAPI;
import org.pyne.jcad.client.DocumentModel;
import org.pyne.jcad.core.entity.Constructor;
import org.pyne.jcad.core.entity.Entity;
import org.pyne.jcad.core.entity.IOperation;
import org.pyne.jcad.core.entity.Sketch;
import org.pyne.jcad.core.entity.Sketches;

/**
 *
 * @author Matthew Pyne
 */
public class EntityTreeController {

    private final EntityTreeViewer viewer;
    private final ClientDocumentAPI documentAPI;

    public EntityTreeController(EntityTreeViewer viewer, ClientDocumentAPI documentAPI) {
        this.viewer = viewer;
        this.documentAPI = documentAPI;

        // Setup listener to be informed when document has changed
        documentAPI.getDocument().listen(this::refreshView);

        viewer.treeView().setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                handleSelectOn(viewer.treeView()
                        .getSelectionModel()
                        .getSelectedItem());
            }
        });

        viewer.newEntityButton().setOnAction(e -> {
            documentAPI.addNewEntity();
            refreshView();
        });
    }

    private void handleSelectOn(TreeItem selectedItem) {
        if (selectedItem instanceof Clickable) {

            ((Clickable) selectedItem).onDoubleClicked();
        }
    }

    public void addOperation(Entity entity, IOperation operation) {
        entity.getComponent(Constructor.class)
                .ifPresent(constructor -> constructor.add(operation));

        refreshView();
    }
    
    public void addSketch(Entity entity, Sketch sketch) {
        entity.getComponent(Sketches.class)
                .filter(sketches -> !sketches.contains(sketch))
                .ifPresent(sketches -> sketches.add(sketch));

        refreshView();
    }

    public void refreshView() {
        // Add the tree view for the current Entity
        DocumentModel doc = documentAPI.getDocument();
        Entity model = doc.getModel();

        if (null == model) {
            return;
        }

        TreeItem rootItem = new EntityTreeItem(model);
        rootItem.setExpanded(true);

        viewer.treeView().setRoot(rootItem);
    }
}
