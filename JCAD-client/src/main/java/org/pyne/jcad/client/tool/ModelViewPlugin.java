package org.pyne.jcad.client.tool;

import com.pixelduke.control.ribbon.RibbonGroup;
import com.pixelduke.control.ribbon.RibbonItem;
import javafx.event.Event;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javax.inject.Inject;
import org.plugface.core.annotations.Plugin;
import org.pyne.jcad.client.ClientInjector;
import org.pyne.jcad.client.ClientInjectorImp;
import org.pyne.jcad.client.JCadApp;
import org.pyne.jcad.client.viewer.Viewer;
import org.pyne.jcad.client.viewer.Viewer2D;

/**
 *
 * @author Matthew Pyne
 */
@Plugin(name = "ModelView")
public class ModelViewPlugin {
    
    @Inject
    public ModelViewPlugin(ClientInjectorImp clientInjector) {
        super();
        
        RibbonGroup modelView = new RibbonGroup();
        modelView.setTitle("View");
        
        Button resetButton = new Button("Reset");
        resetButton.setOnAction(this::onResetView);
        modelView.getNodes().add(resetButton);

        RibbonItem viewTool = new RibbonItem();
        viewTool.setLabel("View Angle: ");
        modelView.getNodes().add(viewTool);
        
        ComboBox<String> viewsCombo = new ComboBox<>();
        viewsCombo.getItems().addAll("Top", "Bottom", "Right", "Left", "Front", "Back", "Offset");
        viewsCombo.setValue("Offset");
        viewTool.setItem(viewsCombo);
        
        clientInjector.addToTopTool(modelView, "Home");
    }
    
    public void onResetView(Event event) {
        JCadApp.PLUGIN_MANAGER
                .getPlugin(Viewer.class)
                .controller()
                .resetView();
    }
        
}
