/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.CartesianPoint_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class CartesianPointDeserializerTest {
    
    public CartesianPointDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class CartesianPointDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#82=CARTESIAN_POINT('',(29.0,-15.0,50.0));";
        StepFile file = new StepFile();
        CartesianPointDeserializer instance = new CartesianPointDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, file);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof CartesianPoint_stp);
        
        CartesianPoint_stp cp = (CartesianPoint_stp) result.get();
        
        assertEquals(29.0, cp.x(), 0.001);
        assertEquals(-15.0, cp.y(), 0.001);
        assertEquals(50.0, cp.z(), 0.001);
    }
    
    /**
     * Test of deserialize method, of class CartesianPointDeserializer.
     */
    @Test
    public void testDeserialize2() {
        System.out.println("deserialize");
        String entity = "#581=CARTESIAN_POINT('',(58.0,-20.0,38.0));";
        StepFile file = new StepFile();
        CartesianPointDeserializer instance = new CartesianPointDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, file);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof CartesianPoint_stp);
        
        CartesianPoint_stp cp = (CartesianPoint_stp) result.get();
        
        assertEquals(58.0, cp.x(), 0.001);
        assertEquals(-20.0, cp.y(), 0.001);
        assertEquals(38.0, cp.z(), 0.001);
    }
    
    /**
     * Test of deserialize method, of class CartesianPointDeserializer.
     */
    @Test
    public void testDeserialize3() {
        System.out.println("deserialize");
        String entity = "#519=CARTESIAN_POINT('',(25.0,0.0,30.0));";
        StepFile file = new StepFile();
        CartesianPointDeserializer instance = new CartesianPointDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, file);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof CartesianPoint_stp);
        
        CartesianPoint_stp cp = (CartesianPoint_stp) result.get();
        
        assertEquals(25.0, cp.x(), 0.001);
        assertEquals(0.0, cp.y(), 0.001);
        assertEquals(30.0, cp.z(), 0.001);
    }
    
}
