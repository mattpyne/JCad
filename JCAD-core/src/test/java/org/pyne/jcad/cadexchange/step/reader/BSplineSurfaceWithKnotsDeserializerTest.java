package org.pyne.jcad.cadexchange.step.reader;

import org.junit.Test;
import org.pyne.jcad.cadexchange.step.BSplineSurfaceWithKnots_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.core.math.CadMath;

import java.util.Optional;

import static org.junit.Assert.*;

public class BSplineSurfaceWithKnotsDeserializerTest {
    @Test
    public void deserialize1() {
        System.out.println("deserialize BSplineSurfaceWithKnots 1");
        String entity = "#581 = B_SPLINE_SURFACE_WITH_KNOTS(" +
                "''," +
                "3," +
                "1," +
                "((#582,#583),(#584,#585),(#586,#587),(#588,#589),(#590,#591),(#592,#593),(#594,#595),(#596,#597),(#598,#599),(#600,#601),(#602,#603),(#604,#605),(#606,#607),(#608,#609),(#610,#611),(#612,#613),(#614,#615),(#616,#617))," +
                ".UNSPECIFIED.," +
                ".F.," +
                ".F.," +
                ".F.," +
                "(4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4)," +
                "(2,2)," +
                "(0.,1.287464523577E-02,2.574929047153E-02,3.862393570729E-02,5.149858094306E-02,6.437322617882E-02,7.724787141458E-02,9.012251665035E-02,0.102997161886,0.115871807122,0.128746452358,0.141621097593,0.154495742829,0.167370388065,0.180245033301,0.193119678536)," +
                "(0.,1.)," +
                ".UNSPECIFIED.);";

        StepFile stepFile = new StepFile();
        BSplineSurfaceWithKnotsDeserializer instance = new BSplineSurfaceWithKnotsDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof BSplineSurfaceWithKnots_stp);

        BSplineSurfaceWithKnots_stp surface = (BSplineSurfaceWithKnots_stp) result.get();
        assertEquals(3, surface.degreeU());
        assertEquals(1, surface.degreeV());

        assertEquals(18, surface.controlPointsListRef().size());
        assertEquals("#582", surface.controlPointsListRef().get(0).get(0).getRef());
        assertEquals("#583", surface.controlPointsListRef().get(0).get(1).getRef());
        assertEquals("#584", surface.controlPointsListRef().get(1).get(0).getRef());
        assertEquals("#585", surface.controlPointsListRef().get(1).get(1).getRef());
        assertEquals("#586", surface.controlPointsListRef().get(2).get(0).getRef());
        assertEquals("#587", surface.controlPointsListRef().get(2).get(1).getRef());


        assertEquals(".UNSPECIFIED.", surface.surfaceForm());
        assertFalse(surface.isClosedU());
        assertFalse(surface.isClosedV());
        assertFalse(surface.isSelfIntersecting());

        assertEquals(16, surface.knotMultiplicitiesU().size());
        assertEquals(4, surface.knotMultiplicitiesU().get(0).intValue());
        assertEquals(1, surface.knotMultiplicitiesU().get(1).intValue());
        assertEquals(4, surface.knotMultiplicitiesU().get(15).intValue());

        assertEquals(2, surface.knotMultiplicitiesV().size());
        assertEquals(2, surface.knotMultiplicitiesV().get(0).intValue());
        assertEquals(2, surface.knotMultiplicitiesV().get(1).intValue());


        assertEquals(16, surface.knotsU().size());
        assertEquals(0., surface.knotsU().get(0), CadMath.EPSILON);
        assertEquals(1.287464523577E-02, surface.knotsU().get(1), CadMath.EPSILON);
        assertEquals(2.574929047153E-02, surface.knotsU().get(2), CadMath.EPSILON);
        assertEquals(0.180245033301, surface.knotsU().get(14), CadMath.EPSILON);
        assertEquals(0.193119678536, surface.knotsU().get(15), CadMath.EPSILON);

        assertEquals(2, surface.knotsV().size());
        assertEquals(0., surface.knotsV().get(0), CadMath.EPSILON);
        assertEquals(1., surface.knotsV().get(1), CadMath.EPSILON);

        assertEquals(".UNSPECIFIED.", surface.knotSpec());

    }

    @Test
    public void deserialize2() {
        System.out.println("deserialize BSplineSurfaceWithKnots 2");
        String entity = "#124 = B_SPLINE_SURFACE_WITH_KNOTS(" +
                "''," +
                "3," +
                "1," +
                "((#125,#126),(#127,#128),(#129,#130),(#131,#132),(#133,#134),(#135,#136),(#137,#138),(#139,#140),(#141,#142),(#143,#144),(#145,#146),(#147,#148),(#149,#150),(#151,#152),(#153,#154),(#155,#156),(#157,#158),(#159,#160))," +
                ".UNSPECIFIED.," +
                ".F.," +
                ".F.," +
                ".F.," +
                "(4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4)," +
                "(2,2)," +
                "(0.,1.526422254643E-02,3.052844509285E-02,4.579266763927E-02,6.10568901857E-02,7.632111273212E-02,9.158533527855E-02,0.106849557825,0.122113780371,0.137378002918,0.152642225464,0.167906448011,0.183170670557,0.198434893104,0.21369911565,0.228963338196)," +
                "(0.,1.)," +
                ".UNSPECIFIED.);";

        StepFile stepFile = new StepFile();
        BSplineSurfaceWithKnotsDeserializer instance = new BSplineSurfaceWithKnotsDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof BSplineSurfaceWithKnots_stp);

        BSplineSurfaceWithKnots_stp surface = (BSplineSurfaceWithKnots_stp) result.get();
        assertEquals(3, surface.degreeU());
        assertEquals(1, surface.degreeV());

        assertEquals(18, surface.controlPointsListRef().size());
        assertEquals("#125", surface.controlPointsListRef().get(0).get(0).getRef());
        assertEquals("#126", surface.controlPointsListRef().get(0).get(1).getRef());
        assertEquals("#127", surface.controlPointsListRef().get(1).get(0).getRef());
        assertEquals("#128", surface.controlPointsListRef().get(1).get(1).getRef());
        assertEquals("#129", surface.controlPointsListRef().get(2).get(0).getRef());
        assertEquals("#130", surface.controlPointsListRef().get(2).get(1).getRef());


        assertEquals(".UNSPECIFIED.", surface.surfaceForm());
        assertFalse(surface.isClosedU());
        assertFalse(surface.isClosedV());
        assertFalse(surface.isSelfIntersecting());

        assertEquals(16, surface.knotMultiplicitiesU().size());
        assertEquals(4, surface.knotMultiplicitiesU().get(0).intValue());
        assertEquals(1, surface.knotMultiplicitiesU().get(1).intValue());
        assertEquals(4, surface.knotMultiplicitiesU().get(15).intValue());

        assertEquals(2, surface.knotMultiplicitiesV().size());
        assertEquals(2, surface.knotMultiplicitiesV().get(0).intValue());
        assertEquals(2, surface.knotMultiplicitiesV().get(1).intValue());


        assertEquals(16, surface.knotsU().size());
        assertEquals(0., surface.knotsU().get(0), CadMath.EPSILON);
        assertEquals(1.526422254643E-02, surface.knotsU().get(1), CadMath.EPSILON);
        assertEquals(3.052844509285E-02, surface.knotsU().get(2), CadMath.EPSILON);
        assertEquals(0.21369911565, surface.knotsU().get(14), CadMath.EPSILON);
        assertEquals(0.228963338196, surface.knotsU().get(15), CadMath.EPSILON);

        assertEquals(2, surface.knotsV().size());
        assertEquals(0., surface.knotsV().get(0), CadMath.EPSILON);
        assertEquals(1., surface.knotsV().get(1), CadMath.EPSILON);

        assertEquals(".UNSPECIFIED.", surface.knotSpec());
    }
}