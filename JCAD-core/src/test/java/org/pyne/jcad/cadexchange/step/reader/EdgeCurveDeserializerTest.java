package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import org.pyne.jcad.cadexchange.step.EdgeCurve_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class EdgeCurveDeserializerTest {
    
    public EdgeCurveDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class EdgeCurveDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#94=EDGE_CURVE('',#83,#90,#93,.F.);";
        StepFile stepFile = new StepFile();
        EdgeCurveDeserializer instance = new EdgeCurveDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);
        
        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof EdgeCurve_stp);
        
    }
    
}
