package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.Line_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class LineDeserializerTest {

    public LineDeserializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class LineDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#79=LINE('',#73,#78);";
        StepFile stepFile = new StepFile();
        LineDeserializer instance = new LineDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof Line_stp);
    }

}
