/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class StepEntityDeserializerTest {
    
    public StepEntityDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of entityType method, of class StepEntityDeserializer.
     */
    @Test
    public void testEntityType() {
        System.out.println("entityType");
        String entity = "#77=DIRECTION('',(0.0,-1.0,0.0));";
        String expResult = "DIRECTION";
        String result = StepEntityDeserializer.entityType(entity);
        assertEquals(expResult, result);
    }

    /**
     * Test of entityRef method, of class StepEntityDeserializer.
     */
    @Test
    public void testEntityRef() {
        System.out.println("entityRef");
        String entity = "#77=DIRECTION('',(0.0,-1.0,0.0));";
        String expResult = "#77";
        String result = StepEntityDeserializer.entityRef(entity);
        assertEquals(expResult, result);
    }

    /**
     * Test of isEntity method, of class StepEntityDeserializer.
     */
    @Test
    public void testIsEnity() {
        System.out.println("isEntity");
        String entity = "77=DIRECTION('',(0.0,-1.0,0.0));";
        boolean expResult = false;
        boolean result = StepEntityDeserializer.isEntity(entity);
        assertEquals(expResult, result);
        
        entity = "#77=DIRECTION('',(0.0,-1.0,0.0));";
        expResult = true;
        result = StepEntityDeserializer.isEntity(entity);
        assertEquals(expResult, result);
    }

    public class StepEntityDeserializerImpl extends StepEntityDeserializer {

        public Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
            return null;
        }
    }
    
}
