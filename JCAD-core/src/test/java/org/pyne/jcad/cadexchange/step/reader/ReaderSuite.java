/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pyne.jcad.cadexchange.step.reader;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Matthew
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({org.pyne.jcad.cadexchange.step.reader.EdgeCurveDeserializerTest.class, org.pyne.jcad.cadexchange.step.reader.StepReaderTest.class, org.pyne.jcad.cadexchange.step.reader.StepEntityDeserializerTest.class, org.pyne.jcad.cadexchange.step.reader.CartesianPointDeserializerTest.class, org.pyne.jcad.cadexchange.step.reader.VertexPointDeserializerTest.class, org.pyne.jcad.cadexchange.step.reader.VectorDeserializerTest.class})
public class ReaderSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
