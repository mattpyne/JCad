package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.OrientedEdge_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class OrientatedEdgeDeserializerTest {
    
    public OrientatedEdgeDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class OrientatedEdgeDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#95=ORIENTED_EDGE('',*,*,#94,.T.);";
        StepFile stepFile = new StepFile();
        OrientatedEdgeDeserializer instance = new OrientatedEdgeDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);
        
        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof OrientedEdge_stp);
        
        OrientedEdge_stp edge = (OrientedEdge_stp) result.get();
        
        assertTrue(edge.orientation());
    }
    
}
