package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class AdvancedFaceDeserializerTest {
    
    public AdvancedFaceDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class AdvancedFaceDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#227=ADVANCED_FACE('',(#201,#221),#226,.T.);";
        StepFile stepFile = new StepFile();
        AdvancedFaceDeserializer instance = new AdvancedFaceDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);
        
        assertTrue(result.isPresent());
    }
    
}
