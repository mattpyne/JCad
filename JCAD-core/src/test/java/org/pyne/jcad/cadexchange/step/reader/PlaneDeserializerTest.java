package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class PlaneDeserializerTest {
    
    public PlaneDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class PlaneDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#149=PLANE('',#148);";
        StepFile stepFile = new StepFile();
        PlaneDeserializer instance = new PlaneDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);
        
        assertTrue(result.isPresent());
    }
    
}
