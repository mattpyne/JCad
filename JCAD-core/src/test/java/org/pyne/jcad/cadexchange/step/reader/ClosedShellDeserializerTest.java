package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class ClosedShellDeserializerTest {

    public ClosedShellDeserializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class ClosedShellDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#759=CLOSED_SHELL('',(#72,#150,#170,#227,#277));";
        StepFile stepFile = new StepFile();
        ClosedShellDeserializer instance = new ClosedShellDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
    }

}
