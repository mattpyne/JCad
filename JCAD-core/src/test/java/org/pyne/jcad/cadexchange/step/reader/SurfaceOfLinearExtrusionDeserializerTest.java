package org.pyne.jcad.cadexchange.step.reader;

import org.junit.Test;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.cadexchange.step.SurfaceOfLinearExtrusion_stp;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SurfaceOfLinearExtrusionDeserializerTest {

    @Test
    public void testDeserialize() {
        System.out.println("surfaceOfLinearExtrusion Deserialization");
        String entity = "#206 = SURFACE_OF_LINEAR_EXTRUSION('',#207,#211);";
        StepFile stepFile = new StepFile();
        SurfaceOfLinearExtrusionDeserializer instance = new SurfaceOfLinearExtrusionDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof SurfaceOfLinearExtrusion_stp);

        SurfaceOfLinearExtrusion_stp surface = (SurfaceOfLinearExtrusion_stp) result.get();

        assertEquals("", surface.getName());
        assertEquals("#207", surface.getSweptCurveRef().getRef());
        assertEquals("#211", surface.getExtrusionAxisRef().getRef());
    }

}