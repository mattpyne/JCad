/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pyne.jcad.cadexchange.step.reader;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import org.pyne.jcad.cadexchange.step.StepRegex;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.PathMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.*;

/**
 *
 * @author Matthew
 */
public class StepReaderTest {

    public StepReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of read method, of class StepReader.
     */
    @Test
    public void testRead() throws Exception {
        System.out.println("read");
        File file = new File(ClassLoader.getSystemResource("step203example.stp").toURI());
        assertTrue(file.exists());
        StepReader instance = new StepReader();
        Topo result = instance.read(file);

        assertTrue(result instanceof Shell);

        Shell shell = (Shell) result;

        assertEquals(25, shell.faces().size());
        
        shell.faces()
                .stream()
                .filter(f -> f.getInnerLoops().isEmpty())
                .forEach(face -> {
                
                    List<HalfEdge> edges = face.getOuterLoop().halfEdges();
                    for (int i = 0; i < edges.size(); i++) {
                        int j = (i + 1) % edges.size();
                        
                        HalfEdge hei = edges.get(i);
                        List<Point3DC> tessi = hei.tessellate();
                        HalfEdge hej = edges.get(j);
                        List<Point3DC> tessj = hej.tessellate();
                        
                        if (!CadMath.eq(tessi.get(tessi.size() - 1), tessj.get(0), 0.1)) {
                            System.out.println("Wrong!");
                        }
                    }
                });
        
        Face face2Holes = shell.faces()
                .stream()
                .filter(f -> f.getInnerLoops().size() == 2)
                .findAny()
                .orElse(null);
        
        assertNotNull(face2Holes);
        
        assertTrue(face2Holes.surface().normal(Point3DC.ZERO).equals(Point3DC.Y_ONE));
        assertTrue(PathMath.isCCW(face2Holes.getOuterLoop().tessellateUV()));
        assertFalse(PathMath.isCCW(face2Holes.getInnerLoops().get(0).tessellateUV()));
        assertFalse(PathMath.isCCW(face2Holes.getInnerLoops().get(1).tessellateUV()));
        
        assertFalse(PathMath.isSelfIntersected(face2Holes.getOuterLoop().tessellateUV()));
        assertFalse(PathMath.isSelfIntersected(face2Holes.getInnerLoops().get(0).tessellateUV()));
        assertFalse(PathMath.isSelfIntersected(face2Holes.getInnerLoops().get(1).tessellateUV()));
        
        Face face1Hole = shell.faces()
                .stream()
                .filter(f -> f.getInnerLoops().size() == 1)
                .findAny()
                .orElse(null);
        
        assertNotNull(face1Hole);
        
        assertFalse(face1Hole.surface().normal(Point3DC.ZERO).equals(Point3DC.Y_ONE));
        assertTrue(PathMath.isCCW(face1Hole.getOuterLoop().tessellateUV()));
        assertFalse(PathMath.isCCW(face1Hole.getInnerLoops().get(0).tessellateUV()));
        
        assertFalse(PathMath.isSelfIntersected(face1Hole.getOuterLoop().tessellateUV()));
        assertFalse(PathMath.isSelfIntersected(face1Hole.getInnerLoops().get(0).tessellateUV()));
        
        assertTrue(face1Hole.surface().normal(Point3DC.ZERO).equals(Point3DC.Y_NEG_ONE));
        
        List<Face> facesWithNoHoles= shell.faces()
                .stream()
                .filter(f -> f.getInnerLoops().isEmpty())
                .collect(Collectors.toList());
        
        assertEquals(23, facesWithNoHoles.size());
    }

    @Test
    public void testRead2() throws Exception {
        System.out.println("read2");
        File file = new File(ClassLoader.getSystemResource("stepy.step").toURI());
        assertTrue(file.exists());
        StepReader instance = new StepReader();
        Topo result = instance.read(file);

        assertTrue(result instanceof Shell);

        Shell shell = (Shell) result;

        shell.bounds();

    }

    @Test
    public void testRead3() throws Exception {
        System.out.println("read3");
        File file = new File(ClassLoader.getSystemResource("stepycolors.step").toURI());
        assertTrue(file.exists());
        StepReader instance = new StepReader();
        Topo result = instance.read(file);

        assertTrue(result instanceof CompoundTopo);

        CompoundTopo compoundShell = (CompoundTopo) result;

        for (Topo topo : compoundShell.topos()) {

            assertTrue(topo instanceof Shell);
            ((Shell)topo).bounds();
        }

    }

    @Test
    public void getSetSelect_GeometricRepresentationContext3() {
        String entity  = "#87 = ( GEOMETRIC_REPRESENTATION_CONTEXT(3) \n" +
                "GLOBAL_UNCERTAINTY_ASSIGNED_CONTEXT((#91)) GLOBAL_UNIT_ASSIGNED_CONTEXT(\n" +
                "(#88,#89,#90)) REPRESENTATION_CONTEXT('Context #1',\n" +
                "  '3D Context with UNIT and UNCERTAINTY') );";

        List<String> setSelects = StepRegex.getSetSelects(entity);


        assertEquals("GEOMETRIC_REPRESENTATION_CONTEXT(3)", setSelects.get(0));
        assertEquals("GLOBAL_UNCERTAINTY_ASSIGNED_CONTEXT((#91))", setSelects.get(1));
        assertEquals("GLOBAL_UNIT_ASSIGNED_CONTEXT(\n(#88,#89,#90))", setSelects.get(2));
        assertEquals("REPRESENTATION_CONTEXT('Context #1',\n  '3D Context with UNIT and UNCERTAINTY')", setSelects.get(3));
    }

    @Test
    public void getInternalVariables_BoundedCurve() {
        String entity = "#806 =( BOUNDED_CURVE ( ) " +
                " B_SPLINE_CURVE ( 3, ( #813, #1273, #693, #884 ), .UNSPECIFIED., .F., .F. ) " +
                " B_SPLINE_CURVE_WITH_KNOTS ( ( 4, 4 ), ( 0.4059348648088311196, 2.735642982953586877 ), .UNSPECIFIED. ) " +
                " CURVE ( ) " +
                " GEOMETRIC_REPRESENTATION_ITEM ( ) " +
                " RATIONAL_B_SPLINE_CURVE ( ( 1.000000000000000000, 0.5965897505645079146, 0.5965897505645079146, 1.000000000000000000 ) ) " +
                " REPRESENTATION_ITEM ( '' )  );";

        List<String> varsBoundedCurve = StepRegex.variables(entity, "BOUNDED_CURVE");
        assertEquals(1, varsBoundedCurve.size());
        assertEquals(varsBoundedCurve.get(0), "");

        List<String> varsBSplineCurve = StepRegex.variables(entity, "B_SPLINE_CURVE");
        assertEquals(5, varsBSplineCurve.size());
        assertEquals("3", varsBSplineCurve.get(0));
        assertEquals("( #813, #1273, #693, #884 )", varsBSplineCurve.get(1));
        assertEquals(".UNSPECIFIED.", varsBSplineCurve.get(2));
        assertEquals(".F.", varsBSplineCurve.get(3));
        assertEquals(".F.", varsBSplineCurve.get(4));

        List<String> varsBSplineCurveWithKnots = StepRegex.variables(entity, "B_SPLINE_CURVE_WITH_KNOTS");
        assertEquals(3, varsBSplineCurveWithKnots.size());
        assertEquals("( 4, 4 )", varsBSplineCurveWithKnots.get(0));
        assertEquals("( 0.4059348648088311196, 2.735642982953586877 )", varsBSplineCurveWithKnots.get(1));
        assertEquals(".UNSPECIFIED.", varsBSplineCurveWithKnots.get(2));

        List<String> varsCurve = StepRegex.variables(entity, "CURVE");
        assertEquals(1, varsCurve.size());
        assertEquals("", varsCurve.get(0));

        List<String> varsGeomRepItem = StepRegex.variables(entity, "GEOMETRIC_REPRESENTATION_ITEM");
        assertEquals(1, varsGeomRepItem.size());
        assertEquals("", varsGeomRepItem.get(0));

        List<String> varsRationBSplineCurve= StepRegex.variables(entity, "RATIONAL_B_SPLINE_CURVE");
        assertEquals(1, varsRationBSplineCurve.size());
        assertEquals("( 1.000000000000000000, 0.5965897505645079146, 0.5965897505645079146, 1.000000000000000000 )", varsRationBSplineCurve.get(0));

        List<String> varsRepItem= StepRegex.variables(entity, "REPRESENTATION_ITEM");
        assertEquals(1, varsRepItem.size());
        assertEquals("''", varsRepItem.get(0));
    }
}
