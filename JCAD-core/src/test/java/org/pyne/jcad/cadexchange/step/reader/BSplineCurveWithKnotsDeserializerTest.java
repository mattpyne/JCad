package org.pyne.jcad.cadexchange.step.reader;

import org.junit.Test;
import org.pyne.jcad.cadexchange.step.BSplineCurveWithKnots_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.core.math.CadMath;

import java.util.Optional;

import static org.junit.Assert.*;

public class BSplineCurveWithKnotsDeserializerTest {

    @Test
    public void deserialize() {
        System.out.println("deserialize");
        String entity = "#207 = B_SPLINE_CURVE_WITH_KNOTS('',2,(#208,#209,#210),.UNSPECIFIED.,.F.,.F.,(3,3),(0.,0.35734717013),.PIECEWISE_BEZIER_KNOTS.);";
        StepFile stepFile = new StepFile();
        BSplineCurveWithKnotsDeserializer instance = new BSplineCurveWithKnotsDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof BSplineCurveWithKnots_stp);

        BSplineCurveWithKnots_stp curve = (BSplineCurveWithKnots_stp) result.get();
        assertEquals(2, curve.degree());

        assertEquals(3, curve.controlPointsListRef().size());
        assertEquals("#208", curve.controlPointsListRef().get(0).getRef());
        assertEquals("#209", curve.controlPointsListRef().get(1).getRef());
        assertEquals("#210", curve.controlPointsListRef().get(2).getRef());

        assertEquals(".UNSPECIFIED.", curve.curveForm());
        assertFalse(curve.isClosedCurve());
        assertFalse(curve.isSelfIntersecting());

        assertEquals(2, curve.knotMultiplicities().size());
        assertEquals(3, curve.knotMultiplicities().get(0).intValue());
        assertEquals(3, curve.knotMultiplicities().get(1).intValue());

        assertEquals(2, curve.knots().size());
        assertEquals(0., curve.knots().get(0), CadMath.EPSILON);
        assertEquals(0.35734717013, curve.knots().get(1), CadMath.EPSILON);

        assertEquals(".PIECEWISE_BEZIER_KNOTS.", curve.knotSpec());
    }

}