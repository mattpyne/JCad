package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.cadexchange.step.VertexPoint_stp;

/**
 *
 * @author Matthew
 */
public class VertexPointDeserializerTest {
    
    public VertexPointDeserializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class VertexPointDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#76=VERTEX_POINT('',#74);";
        StepFile stepFile = new StepFile();
        VertexPointDeserializer instance = new VertexPointDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);
        
        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof VertexPoint_stp);
    }
    
}
