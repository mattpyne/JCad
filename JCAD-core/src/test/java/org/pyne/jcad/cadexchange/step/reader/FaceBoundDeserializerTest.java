package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.FaceBound_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 *
 * @author Matthew
 */
public class FaceBoundDeserializerTest {

    public FaceBoundDeserializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class FaceBoundDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#66=FACE_BOUND('',#65,.F.);";
        StepFile stepFile = new StepFile();
        FaceBoundDeserializer instance = new FaceBoundDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof FaceBound_stp);

        FaceBound_stp faceBound = (FaceBound_stp) result.get();

        assertFalse(faceBound.orientation());
    }

}
