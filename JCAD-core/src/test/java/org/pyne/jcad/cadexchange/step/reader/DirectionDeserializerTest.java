package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import org.pyne.jcad.cadexchange.step.Direction_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

/**
 * @author Matthew
 */
public class DirectionDeserializerTest {

    public DirectionDeserializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of deserialize method, of class DirectionDeserializer.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        String entity = "#91=DIRECTION('',(0.0,-1.0,0.0));";
        StepFile stepFile = new StepFile();
        DirectionDeserializer instance = new DirectionDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof Direction_stp);

        Direction_stp dir = (Direction_stp) result.get();

        assertEquals(0.0, dir.x(), 0.001);
        assertEquals(-1.0, dir.y(), 0.001);
        assertEquals(0.0, dir.z(), 0.001);
    }

    @Test
    public void testDeserialize2() {
        System.out.println("deserialize2");
        String entity = "#137 = DIRECTION('',(1.224646799147E-16,1.,3.061616997868E-16));";
        StepFile stepFile = new StepFile();
        DirectionDeserializer instance = new DirectionDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof Direction_stp);

        Direction_stp dir = (Direction_stp) result.get();

        assertEquals(1.224646799147E-16, dir.x(), 0.001);
        assertEquals(1.0, dir.y(), 0.001);
        assertEquals(3.061616997868E-16, dir.z(), 0.001);
    }

    @Test
    public void testDeserialize3() {
        System.out.println("deserialize3");
        String entity = "#57 = DIRECTION('',(0.,-1.));";
        StepFile stepFile = new StepFile();
        DirectionDeserializer instance = new DirectionDeserializer();
        Optional<StepEntity> result = instance.deserialize(entity, stepFile);

        assertTrue(result.isPresent());
        assertTrue(result.get() instanceof Direction_stp);

        Direction_stp dir = (Direction_stp) result.get();

        assertEquals(0, dir.x(), 0.001);
        assertEquals(-1.0, dir.y(), 0.001);
        assertEquals(3.061616997868E-16, dir.z(), 0.001);
    }

}
