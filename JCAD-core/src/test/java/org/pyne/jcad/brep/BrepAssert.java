package org.pyne.jcad.brep;

import static org.junit.Assert.assertTrue;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.eval.BrepValidator;

/**
 *
 * @author Matthew
 */
public class BrepAssert {

    public static void assertShellValid(Shell extrusion) {
        assertTrue(BrepValidator.isValidFaceBounds(extrusion.streamFaces()));
        assertTrue(BrepValidator.isValidFaces(extrusion.streamFaces()));
        assertTrue(BrepValidator.isValidEdges(extrusion.streamEdges()));
    }
}
