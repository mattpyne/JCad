package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.brep.eval.PointOnFace;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.brep.creators.BrepEnclose;
import org.pyne.jcad.core.math.geom.CurvePrimitives;

/**
 *
 * @author Matthew Pyne
 */
public class BrepMathTest {

    public BrepMathTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of pointOnFace method, of class BrepMath.
     */
    @Test
    public void testPointOnFace_center() {
        System.out.println("pointOnFace");
        BrepSurface surf = BrepEnclose.loft(new BrepCurve(CurvePrimitives.line(Point3DC.ZERO, Point3DC.X_ONE)),
                new BrepCurve(CurvePrimitives.line(Point3DC.Y_ONE, Point3DC.X_ONE.plus(Point3DC.Y_ONE))));

        BrepBuilder b1 = new BrepBuilder();
        Face face = b1.face(surf)
                .loop(new Vertex(Point3DC.ZERO),
                        new Vertex(Point3DC.X_ONE),
                        new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE)),
                        new Vertex(Point3DC.ZERO))
                .buildFace();

        // center of plane
        Point3DC pt = Point3DC.X_ONE.divided(2).plus(Point3DC.Y_ONE.divided(2));

        boolean expResult = true;
        boolean result = PointOnFace.point(face, pt);
        assertEquals(expResult, result);
    }

    /**
     * Test of pointOnFace method, of class BrepMath.
     */
    @Test
    public void testPointOnFace_outside() {
        System.out.println("pointOnFace outside");
        BrepSurface surf = BrepEnclose.loft(new BrepCurve(CurvePrimitives.line(Point3DC.ZERO, Point3DC.X_ONE)),
                new BrepCurve(CurvePrimitives.line(Point3DC.Y_ONE, Point3DC.X_ONE.plus(Point3DC.Y_ONE))));

        BrepBuilder b1 = new BrepBuilder();
        Face face = b1.face(surf)
                .loop(new Vertex(Point3DC.ZERO),
                        new Vertex(Point3DC.X_ONE),
                        new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE)),
                        new Vertex(Point3DC.ZERO))
                .buildFace();

        // center of plane
        Point3DC pt = Point3DC.Z_ONE.times(5);

        boolean expResult = false;
        boolean result = PointOnFace.point(face, pt);
        assertEquals(expResult, result);
    }
}
