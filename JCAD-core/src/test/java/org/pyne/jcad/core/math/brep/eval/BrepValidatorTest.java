package org.pyne.jcad.core.math.brep.eval;

import java.util.Collection;
import java.util.stream.Stream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.brep.creators.BrepEnclose;
import org.pyne.jcad.core.math.brep.creators.ShellPrimitives;
import org.pyne.jcad.core.math.geom.CurvePrimitives;

/**
 *
 * @author Matthew
 */
public class BrepValidatorTest {
    
    public BrepValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isValid method, of class BrepValidator.
     */
    @Test
    public void empty() {
        System.out.println("isValid");
        Shell shell = new Shell();
        boolean expResult = false;
        
        boolean result = BrepValidator.isValid(shell);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValid method, of class BrepValidator.
     */
    @Test
    public void oneFace() {
        System.out.println("isValid");
        BrepSurface surf = BrepEnclose.loft(new BrepCurve(CurvePrimitives.line(Point3DC.ZERO, Point3DC.X_ONE)),
                        new BrepCurve(CurvePrimitives.line(Point3DC.Y_ONE, Point3DC.X_ONE.plus(Point3DC.Y_ONE))));
        
        BrepBuilder b1 = new BrepBuilder();
        Face face1 = b1.face(surf)
                       .loop(new Vertex(Point3DC.ZERO), 
                               new Vertex(Point3DC.X_ONE),
                               new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE)),
                               new Vertex(Point3DC.ZERO))
                       .buildFace();
        
        Shell shell = new Shell();
        shell.addFace(face1);
        
        boolean expResult = false;
        
        boolean result = BrepValidator.isValid(shell);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValid method, of class BrepValidator.
     */
    @Test
    public void cube() {
        System.out.println("isValid");
        
        Shell shell = ShellPrimitives.box(4, 4, 4);
        
        boolean expResult = true;
        
        boolean result = BrepValidator.isValid(shell);
        assertEquals(expResult, result);
    }
}
