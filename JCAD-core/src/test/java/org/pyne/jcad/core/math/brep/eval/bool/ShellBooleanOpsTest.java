package org.pyne.jcad.core.math.brep.eval.bool;

import haxe.lang.EmptyObject;
import haxe.root.Array;
import java.util.ArrayList;
import java.util.Arrays;
import org.pyne.jcad.core.math.brep.creators.ShellPrimitives;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.entity.ExtrudeOperation;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Transform;
import org.pyne.jcad.core.math.Transformer;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import verb.core.BoundingBox;
import verb.core.Vec;

/**
 *
 * @author Matthew Pyne
 */
public class ShellBooleanOpsTest {

    public ShellBooleanOpsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of union method, of class ShellBooleanOps.
     */
    @Test
    public void testUnion() {
        System.out.println("union");
        Shell shell1 = ShellPrimitives.box(50, 50, 50, null);
        name(shell1, "A");
        Shell shell2 = ShellPrimitives.box(25, 25, 100, null);
        name(shell2, "B");

        ShellBooleanOps instance = new ShellBooleanOps();
        Shell result = instance.union(shell1, shell2);

        assertEquals(16, result.faces().size());
    }

    private void name(Shell shell1, String b) {
        for (int i = 0; i < shell1.faces().size(); i++) {
            Face face = shell1.faces().get(i);
            face.setId(i + b);

            for (int j = 0; j < face.halfEdges().size(); j++) {
                face.halfEdges().get(j).setId(i + b);
                face.halfEdges().get(j).edge().curve().setId(i + b);
            }
        }
    }

    /**
     * Test of union method, of class ShellBooleanOps.
     */
    @Test
    public void testUnion_OverLapping() {
        System.out.println("union");
        Shell shell1 = ShellPrimitives.box(50, 50, 50, null);
        Shell shell2 = ShellPrimitives.box(25, 25, 100, null);
        new Transformer().translate(shell2, 12.5, 0, 0);
        ShellBooleanOps instance = new ShellBooleanOps();
        Shell result = instance.union(shell1, shell2);

        assertEquals(14, result.faces().size());
    }

    /**
     * Test of union method, of class ShellBooleanOps.
     */
    @Test
    public void testUnion_OverLapping_Intersection() {
        System.out.println("union intersection overlapping");
        Shell shellA = ShellPrimitives.box(50, 50, 50, null);
        Shell shellB = ShellPrimitives.box(25, 25, 100, null);
        new Transformer().translate(shellB, 12.5, 0, 0);

        int edgeCountBefore1 = shellA.edges().size();
        int edgeCountBefore2 = shellB.edges().size();

        assertEquals(12, edgeCountBefore1);
        assertEquals(12, edgeCountBefore2);

        ShellBooleanOps instance = new ShellBooleanOps();
        ShellBooleanOps.TYPE type = ShellBooleanOps.TYPE.UNION;

        shellA = instance.prepareWorkingCopy(shellA);
        shellB = instance.prepareWorkingCopy(shellB);

        List<Face> workingFaces = instance.collectFaces(shellA, shellB);
        instance.initOperationData(workingFaces);
        instance.mergeVertices(shellA, shellB);
        instance.initVertexFactory(shellA, shellB);

        HashMap<Edge, List<VertexIntersection>> isecs = instance.findEdgeIntersections(shellA, shellB);

        assertEquals(4, isecs.size());
        isecs.values().forEach(vertexInts -> assertEquals(2, vertexInts.size()));

        int edgeCountBeforeAfter1 = shellA.edges().size();
        int edgeCountBeforeAfter2 = shellB.edges().size();

        assertEquals(12, edgeCountBeforeAfter1);
        assertEquals(12, edgeCountBeforeAfter2);

        instance.splitAtIntersections(isecs);

        int edgeCountAfter1 = shellA.edges().size();
        int edgeCountAfter2 = shellB.edges().size();

        assertEquals(16, edgeCountAfter1);
        assertEquals(16, edgeCountAfter2);

        List<MergeFacesInfo> mergedFaces = instance.mergeOverlappingFaces(shellA, shellB, type);

        instance.intersectFaces(shellA, shellB, mergedFaces, type);

        int edgeCountIntFaceAfter1 = shellA.edges().size();
        int edgeCountIntFaceAfter2 = shellB.edges().size();

        int countFaceWith3NewEdges = 0;
        int countFaceWith2NewEdges = 0;
        int countFaceWithNonZeroNewEdges = 0;
        for (Face face : workingFaces) {

            if (face.op.newEdges.size() == 3) {
                countFaceWith3NewEdges++;
            }
            if (face.op.newEdges.size() == 2) {
                countFaceWith2NewEdges++;
            }
            if (!face.op.newEdges.isEmpty()) {
                countFaceWithNonZeroNewEdges++;
            }
        }

//        assertEquals(2, countFaceWith3NewEdges);
//        assertEquals(3, countFaceWith2NewEdges);
//        assertEquals(5, countFaceWithNonZeroNewEdges);
//        
//        assertEquals(32, edgeCountIntFaceAfter1);
//        assertEquals(40, edgeCountIntFaceAfter2);
//        
        workingFaces = instance.replaceMergedFaces(workingFaces, mergedFaces);
        for (Face workFace : workingFaces) {
            workFace.op.initGraph();
        }
        for (Face workFace : workingFaces) {
            workFace.op.detectedLoops = instance.detectLoops(workFace.surface(), workFace.op);
            System.out.println("detectedLoops: " + workFace.op.detectedLoops.size());
        }
        for (Face workFace : workingFaces) {
            workFace.op.detectedLoops.forEach(l -> l.link());
        }

        instance.removeInvalidLoops(workingFaces);

        List<Face> faces = new ArrayList();
        for (Face face : workingFaces) {
            instance.loopsToFaces(face, face.op.detectedLoops, faces);
        }

        System.out.println("Number of faces made from loops: " + faces.size());

        faces = instance.filterFace(faces);

        System.out.println("Number of faces after filtered: " + faces.size());

        Shell result = new Shell();
        faces.forEach(face -> {
            face.setShell(result);
            result.faces().add(face);
        });

    }

    @Test
    public void testSubtract1() {
        System.out.println("subtract");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 480,
                new Matrix3().translate(20.0, 20.0, 1.0)));
        shell = shell.subtract(ShellPrimitives.box(30, 30, 480,
                new Matrix3().translate(53, 50.0, 1.0)));
        shell = shell.subtract(ShellPrimitives.box(30, 30, 480,
                new Matrix3().translate(86, 50.0, 1.0)));

        assertEquals(18, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 4).count());
        assertEquals(16, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractTopOffset() {
        System.out.println("subtract Top");
        Shell shell = ShellPrimitives.box(500, 500, 500);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 1000, Transform.translate(0.0, 0.0, 260)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractTopOffsetCylinder() {
        System.out.println("subtract Top");
        Shell shell = ShellPrimitives.box(500, 500, 500);
        shell = shell.subtract(ShellPrimitives.cylinder(30, 1000, Transform.translate(0.0, 0.0, 260)));

        assertEquals(9, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(8, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractIntersectingCylinders() {
        System.out.println("Subtract intersecting cylinders");
        Shell shell = ShellPrimitives.box(500, 500, 500);
        shell = shell.subtract(ShellPrimitives.cylinder(60, 1000, Transform.rotation(Math.PI / 2,1.0, 0.0, 0.0)));
        shell = shell.subtract(ShellPrimitives.cylinder(30, 1000));

        assertEquals(12, shell.faces().size());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractTopOffsetCylinder2() {
        System.out.println("subtract Top");
        Shell shell = ShellPrimitives.box(500, 500, 500);
        shell = shell.subtract(ShellPrimitives.cylinder(30, 1000, Transform.translate(0.0, 0.0, 240)));

        assertEquals(8, shell.faces().size());
        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }


    @Test
    public void testSubtractTop() {
        System.out.println("subtract Top");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 30, Transform.translate(0.0, 0.0, 125)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractBottom() {
        System.out.println("subtract Bottom");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 30, Transform.translate(0.0, 0.0, -125)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractLeft() {
        System.out.println("subtract Left");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 30, Transform.translate(-125.0, 0.0, 0.0)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractRight() {
        System.out.println("subtract Right");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 30, Transform.translate(125.0, 0.0, 0.0)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractFront() {
        System.out.println("subtract Front");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 30, Transform.translate(0.0, 125.0, 0.0)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubtractBack() {
        System.out.println("subtract back");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 30, Transform.translate(0.0, -125.0, 0.0)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractWithCutIntersection() {
        System.out.println("2 box subtract intersections");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.box(30, 30, 250, null));
        shell = shell.subtract(ShellPrimitives.box(30, 30, 250,
                new Matrix3().translate(10, 20, 0)));

        assertEquals(14, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(12, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractWithCutIntersection_2() {
        System.out.println("2 box subtract intersections");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.box(30, 30, 300, null));
        shell = shell.subtract(ShellPrimitives.box(30, 30, 500,
                new Matrix3().translate(10, 20, 0)));

        assertEquals(14, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(12, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractWithCutIntersection1() {
        System.out.println("2 box subtract overlap intersections");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.box(30, 30, 250, null));
        shell = shell.subtract(ShellPrimitives.box(30, 30, 250,
                new Matrix3().translate(10, 30, 0)));

        assertEquals(14, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(12, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractWithCutIntersection2() {
        System.out.println("2 box subtract intersections 2");
        Shell shell = ShellPrimitives.box(250, 250, 250);
        shell = shell.subtract(ShellPrimitives.box(30, 30, 250));

        assertEquals(10, shell.faces().size());
        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(8, shell.faces().stream().filter(f -> f.loops().size() == 1).count());

        System.out.println("==========================");
        shell = shell.subtract(ShellPrimitives.box(30, 30, 250,
                new Matrix3().translate(10, 29, 0)));

        assertEquals(14, shell.faces().size());
        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(12, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSimpleBoxCylinderUnion() {
        Shell shell = ShellPrimitives.box(10, 10, 10);

        shell = shell.union(ShellPrimitives.cylinder(3, 25));

        assertEquals(12, shell.faces().size());
        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSimpleBoxBoxUnion() {
        Shell shell = ShellPrimitives.box(10, 10, 10);

        Debug.checkEdgesValid(shell);
        Debug.checkLoopsValid(shell);

        shell = shell.union(ShellPrimitives.box(5, 5, 5).transformed(new Matrix3().translate(0, 0, 5)));

        assertEquals(11, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSimpleBoxCylinderUnionNotThrough() {
        Shell shell = ShellPrimitives.box(10, 10, 10);

        shell = shell.union(ShellPrimitives.cylinder(3, 5).transformed(new Matrix3().translate(0, 0, 5)));

        assertEquals(9, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(8, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSimpleBoxCylinderUnionNotThrough_1() {

        for (int i = 0; i < 10; i++) {
            Shell shell = ShellPrimitives.box(1000, 1000, 1000);

            shell = shell.union(ShellPrimitives.cylinder(300, 500).transformed(new Matrix3().translate(0, 100, -500)));

            assertEquals(9, shell.faces().size());
            assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
            assertEquals(8, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
        }
    }
    
    @Test
    public void testSimpleBoxCylinderExtrudedUnionNotThrough_2() {

        Shell shell = ShellPrimitives.box(100, 100, 100, null);

        for (int i = 0; i < shell.faces().size(); i++) {
            shell.faces().get(i).setId(i + "A");
        }
        shell = shell.subtract(ShellPrimitives.cylinder(10, 10,
                new Matrix3().translate(0, 0, 50.0)));

        assertEquals(9, shell.faces().size());
        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(8, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
        
        Curve circle = CurvePrimitives.circle(
                Point3DC.xyz(50, 14, 20),
                Point3DC.Y_ONE,
                Point3DC.Z_ONE,
                10);
        
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        ExtrudeOperation extrudeOperation = new ExtrudeOperation();
        extrudeOperation.setAmountValue(10);
        extrudeOperation.getxDir().setValue("1");
        extrudeOperation.getyDir().setValue("0");
        extrudeOperation.getzDir().setValue("0");
        extrudeOperation.setLoops(Arrays.asList(loop));
        
        Shell extrusion = (Shell) extrudeOperation.topoOp();
        
        for (int i = 0; i < extrusion.faces().size(); i++) {
            extrusion.faces().get(i).setId(i + "B");
        }
        
        System.out.println("=============================================");
        shell = shell.union(extrusion);

        assertEquals(12, shell.faces().size());
        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(10, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractCylinder() {
        System.out.println("cylinder subtract");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.cylinder(30, 300));

        assertEquals(8, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractCylinder_Box() {
        System.out.println("Cylinder box subtract");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.cylinder(30, 300));
        shell = shell.subtract(ShellPrimitives.box(50, 50, 500, new Matrix3().translate(20, 0, 0)));

        assertEquals(11, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(9, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractCylinderTransformed() {
        System.out.println("Cylinder box subtract");
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.cylinder(30, 300, Transform.translate(20, 0, 0)));

        assertEquals(8, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractCylinderTransformed2() {
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.cylinder(30, 300, Transform.translate(20, 50, 0)));

        assertEquals(8, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSubstractCylinderCylinder() {
        Shell shell = ShellPrimitives.box(250, 250, 250, null);
        shell = shell.subtract(ShellPrimitives.cylinder(30, 300));

        assertEquals(8, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());

        shell = shell.subtract(ShellPrimitives.cylinder(30, 300, Transform.translate(10, 0, 0)));

        assertEquals(10, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(8, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void cylinderJustTouchRectSubtract() {
        Shell shell = ShellPrimitives.box(250, 250, 250, null);

        shell = shell.subtract(ShellPrimitives.box(30, 30, 480,
                new Matrix3().translate(20.0, 20.0, 1.0)));

        shell = shell.subtract(ShellPrimitives.cylinder(30, 480,
                new Matrix3().translate(65, 20.0, 1.0)));

        assertEquals(8, shell.faces().size());

        assertEquals(2, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }
    
    @Test
    public void cylinderCylinderUnion() {
        Shell shell = ShellPrimitives.cylinder(30, 30, null);

        shell = shell.union(ShellPrimitives.cylinder(30, 30,
                new Matrix3().translate(20.0, 0, 0)));

        assertEquals(6, shell.faces().size());

        assertEquals(6, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void boxBoxIntersectSubtract() {
        Shell shell = ShellPrimitives.box(100, 100, 100, null);

        shell = shell.subtract(ShellPrimitives.box(20, 20, 20,
                new Matrix3().translate(0, 0, 50.0)));

        shell = shell.subtract(ShellPrimitives.box(20, 20, 20,
                new Matrix3().translate(5, 5, 50.0)));

        assertEquals(15, shell.faces().size());

        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(14, shell.faces().stream().filter(f -> f.loops().size() == 1).count());

        shell = shell.subtract(ShellPrimitives.box(20, 20, 20,
                new Matrix3().translate(10, 10, 50.0)));

        assertEquals(19, shell.faces().size());

        assertEquals(1, shell.faces().stream().filter(f -> f.loops().size() == 2).count());
        assertEquals(18, shell.faces().stream().filter(f -> f.loops().size() == 1).count());
    }

    @Test
    public void testSpeed() {
        System.out.println("Speed test");

        long startTime = new Date().getTime();

        int numOps = 100;
        Shell shell = ShellPrimitives.box(1000, 250, 250, null);

        for (int i = 0; i < numOps; i++) {

            shell = shell.subtract(ShellPrimitives.box(2, 2, 400, new Matrix3().translate(3 * i - 490, 0, 0)));
        }

        System.out.println((new Date().getTime() - startTime) / 1000. + " seconds to do " + numOps + " cuts");
    }

    @Test
    public void testSpeedCylinders() {
        System.out.println("Speed test cylinders");

        long startTime = new Date().getTime();

        int numOps = 100;
        Shell shell = ShellPrimitives.box(1000, 250, 250, null);

        for (int i = 0; i < numOps; i++) {

            shell = shell.subtract(ShellPrimitives.cylinder(1, 400, new Matrix3().translate(3 * i - 490, 0, 0)));
        }

        System.out.println((new Date().getTime() - startTime) / 1000. + " seconds to do " + numOps + " cuts");
    }

    @Test
    public void testSpeedCylindersIntersections() {
        System.out.println("Speed test cylinders intersections");

        long startTime = new Date().getTime();

        int numOps = 100;
        Shell shell = ShellPrimitives.box(1000, 250, 250, null);

        for (int i = 0; i < numOps; i++) {

            shell = shell.subtract(ShellPrimitives.cylinder(1, 400, new Matrix3().translate(1 * i - 490, 0, 0)));
        }

        System.out.println((new Date().getTime() - startTime) / 1000. + " seconds to do " + numOps + " cuts");
    }

    /**
     * Surface of the face no longer matches the loop. Still works overall but
     * the surface will never change. May lead to confusion or performance
     * issues.
     *
     * @param shell
     */
    public static void shellHasSurfaceErr(Shell shell) {
        for (Face face : shell.faces()) {
            faceSurfaceNoMatch(face);
        }
    }

    /**
     * Surface of the face no longer matches the loop. Still works overall but
     * the surface will never change. May lead to confusion or performance
     * issues.
     *
     * @param face
     */
    public static void faceSurfaceNoMatch(Face face) {
        BoundingBox loopBounds = new BoundingBox(EmptyObject.EMPTY);
        for (Loop loop : face.loops()) {
            loopBounds.add(loop.bounds().max);
            loopBounds.add(loop.bounds().min);
        }

        BoundingBox surfBounds = face.surface().bounds();

        if (!Vec.isZero(Vec.sub(((Array<Number>) (Array) loopBounds.min), ((Array<Number>) (Array) surfBounds.min)))
                || !Vec.isZero(Vec.sub(((Array<Number>) (Array) loopBounds.max), ((Array<Number>) (Array) surfBounds.max)))) {

            System.out.println("HEre");
        }
    }

}
