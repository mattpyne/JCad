package org.pyne.jcad.core.math;

import eu.mihosoft.vvecmath.Vector3d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matthew
 */
public class PathMathTest {
    
    public PathMathTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testtest() {
        List<Double> doubles = Arrays.asList(4.0, 100.0, 1.0, 20.0, 70.0)
                .stream()
                .sorted((d1, d2) -> Double.compare(d1, d2))
                .collect(Collectors.toList());

        System.out.println(Arrays.toString(doubles.toArray()));
    }

    /**
     * Test of normalOfCCWSeg method, of class PathMath.
     */
    @Test
    public void testNormalOfCCWSeg() {
        System.out.println("normalOfCCWSeg");
        List<Point3DC> ccwSequence = null;
        Point3DC expResult = null;
        Point3DC result = PathMath.normalOfCCWSeg(ccwSequence);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isCCW method, of class PathMath.
     */
    @Test
    public void testIsCCW() {
        System.out.println("isCCW");
        List<Point3DC> path2D = null;
        boolean expResult = false;
        boolean result = PathMath.isCCW(path2D);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of area method, of class PathMath.
     */
    @Test
    public void testArea() {
        System.out.println("area");
        List<Point3DC> path2D = null;
        double expResult = 0.0;
        double result = PathMath.area(path2D);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code
        //  and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of startPathClosestToZero method, of class PathMath.
     */
    @Test
    public void testStartPathClosestToZero() {
        System.out.println("startPathClosestToZero");
        List<Point3DC> path = null;
        List<Point3DC> expResult = null;
        List<Point3DC> result = PathMath.startPathClosestToZero(path);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isSelfIntersected method, of class PathMath.
     */
    @Test
    public void testIsSelfIntersected() {
        System.out.println("isSelfIntersected");
        
        // RECT
        
        List<Point3DC> points = new ArrayList<>();
        points.add(Point3DC.ZERO);
        points.add(Point3DC.X_ONE);
        points.add(Point3DC.X_ONE.plus(Point3DC.Y_ONE));
        points.add(Point3DC.Y_ONE);
        boolean expResult = false;
        boolean result = PathMath.isSelfIntersected(points);
        assertEquals(expResult, result);
        
        points = new ArrayList<>();
        points.add(Point3DC.ZERO);
        points.add(Point3DC.X_ONE);
        points.add(Point3DC.Y_ONE);
        points.add(Point3DC.X_ONE.plus(Point3DC.Y_ONE));
        expResult = true;
        result = PathMath.isSelfIntersected(points);
        assertEquals(expResult, result);
    }
    
}
