package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.brep.eval.OverLappingFaces;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.brep.creators.BrepEnclose;
import org.pyne.jcad.core.math.geom.CurvePrimitives;

/**
 *
 * @author Matthew Pyne
 */
public class OverLappingFacesTest {
    
    public OverLappingFacesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of overlaps method, of class OverLappingFaces.
     */
    @Test
    public void testOverlaps_Exact() {
        System.out.println("overlaps");
        BrepSurface surf = BrepEnclose.loft(new BrepCurve(CurvePrimitives.line(Point3DC.ZERO, Point3DC.X_ONE)),
                        new BrepCurve(CurvePrimitives.line(Point3DC.Y_ONE, Point3DC.X_ONE.plus(Point3DC.Y_ONE))));
        
        BrepBuilder b1 = new BrepBuilder();
        Face face1 = b1.face(surf)
                       .loop(new Vertex(Point3DC.ZERO), 
                               new Vertex(Point3DC.X_ONE),
                               new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE)),
                               new Vertex(Point3DC.ZERO))
                       .buildFace();
        
        Face face2 = b1.face(surf)
                       .loop(new Vertex(Point3DC.ZERO), 
                               new Vertex(Point3DC.X_ONE),
                               new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE)),
                               new Vertex(Point3DC.ZERO))
                       .buildFace();
        
        boolean expResult = true;
        boolean result = OverLappingFaces.overlaps(face1, face2);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of overlaps method, of class OverLappingFaces.
     */
    @Test
    public void testOverlaps_Offset() {
        System.out.println("overlaps");
        BrepSurface surf1 = BrepEnclose.loft(new BrepCurve(CurvePrimitives.line(Point3DC.ZERO, Point3DC.X_ONE)),
                        new BrepCurve(CurvePrimitives.line(Point3DC.Y_ONE, Point3DC.X_ONE.plus(Point3DC.Y_ONE))));
        
        BrepSurface surf2 = BrepEnclose.loft(new BrepCurve(CurvePrimitives.line(Point3DC.ZERO.plus(Point3DC.Z_ONE), Point3DC.X_ONE.plus(Point3DC.Z_ONE))),
                        new BrepCurve(CurvePrimitives.line(Point3DC.Y_ONE.plus(Point3DC.Z_ONE), Point3DC.X_ONE.plus(Point3DC.Y_ONE).plus(Point3DC.Z_ONE))));
        
        
        BrepBuilder b1 = new BrepBuilder();
        Face face1 = b1.face(surf1)
                       .loop(new Vertex(Point3DC.ZERO), 
                               new Vertex(Point3DC.X_ONE),
                               new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE)),
                               new Vertex(Point3DC.ZERO))
                       .buildFace();
        
        // offset in the z
        Face face2 = b1.face(surf2)
                       .loop(new Vertex(Point3DC.ZERO.plus(Point3DC.Z_ONE)), 
                               new Vertex(Point3DC.X_ONE.plus(Point3DC.Z_ONE)),
                               new Vertex(Point3DC.X_ONE.plus(Point3DC.Y_ONE).plus(Point3DC.Z_ONE)),
                               new Vertex(Point3DC.ZERO.plus(Point3DC.Z_ONE)))
                       .buildFace();
        
        boolean expResult = false;
        boolean result = OverLappingFaces.overlaps(face1, face2);
        assertEquals(expResult, result);
    }
    
}
