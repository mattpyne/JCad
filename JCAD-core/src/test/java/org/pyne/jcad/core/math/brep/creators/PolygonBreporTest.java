package org.pyne.jcad.core.math.brep.creators;

import eu.mihosoft.jcsg.CSG;
import haxe.lang.Runtime;
import haxe.root.Array;
import org.junit.*;
import org.pyne.jcad.core.csg.BrepToCSG;
import org.pyne.jcad.cadexchange.STLReader;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Topo;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.BoundingBox;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PolygonBreporTest {

    public PolygonBreporTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     */
    @Test
    public void testBoxToPolygonsBackToBrep() {
        System.out.println();

        Shell box = ShellPrimitives.box(10, 10, 10);

        CSG csgBox = new BrepToCSG().fromBrep(box);

        Topo result = new PolygonBrepor(csgBox.getPolygons(), 0.1).execute();

        assertTrue(result instanceof Shell);

        BoundingBox resultBounds = ((Shell) result).bounds();
        BoundingBox expBounds = box.bounds();

        assertEquals(box.faces().size(), ((Shell) result).faces().size());
        assertArrayEquals(expBounds.max, resultBounds.max);
        assertArrayEquals(expBounds.min, resultBounds.min);
    }

    /**
     */
    @Test
    public void testBoxToPolygonsBackToBrep2() {
        System.out.println();

        Shell box = ShellPrimitives.box(10, 10, 10);

        box = box.union(ShellPrimitives.box(6, 4, 25));

        CSG csgBox = new BrepToCSG().fromBrep(box);

        Topo result = new PolygonBrepor(csgBox.getPolygons(), 0.1).execute();

        assertTrue(result instanceof Shell);

        BoundingBox resultBounds = ((Shell) result).bounds();
        BoundingBox expBounds = box.bounds();

        assertEquals(box.faces().size(), ((Shell) result).faces().size());
        assertArrayEquals(expBounds.max, resultBounds.max);
        assertArrayEquals(expBounds.min, resultBounds.min);
    }

    /**
     */
    @Test
    public void testBoxToPolygonsBackToBrep3() {
        System.out.println();

        Shell box = ShellPrimitives.cylinder(3, 25);

        CSG csgBox = new BrepToCSG().fromBrep(box);

        Topo result = new PolygonBrepor(csgBox.getPolygons(), 0.1).execute();

        assertTrue(result instanceof Shell);

        BoundingBox resultBounds = ((Shell) result).bounds();
        BoundingBox expBounds = box.bounds();

//        assertEquals(box.faces().size(), ((Shell) result).faces().size());
        assertArrayEquals(expBounds.max, resultBounds.max);
        assertArrayEquals(expBounds.min, resultBounds.min);
    }

    /**
     */
    @Test
    public void testBoxToPolygonsBackToBrep4() {
        System.out.println();

        Shell box = ShellPrimitives.box(10, 10, 10);

        box = box.union(ShellPrimitives.cylinder(3, 25));

        CSG csgBox = new BrepToCSG().fromBrep(box);

        Topo result = new PolygonBrepor(csgBox.getPolygons(), 0.1).execute();

        assertTrue(result instanceof Shell);

        BoundingBox resultBounds = ((Shell) result).bounds();
        BoundingBox expBounds = box.bounds();

//        assertEquals(box.faces().size(), ((Shell) result).faces().size());
        assertArrayEquals(expBounds.max, resultBounds.max);
        assertArrayEquals(expBounds.min, resultBounds.min);
    }


    @Test
    public void testBoxToPolygonsBackToBrep6() throws IOException {
        System.out.println();

        URL stlUrl = ClassLoader.getSystemResource("cylinder.stl");
        assertNotNull(stlUrl);

        Shell box = (Shell) STLReader.read(new File(stlUrl.getFile()));



//        BoundingBox resultBounds = ((Shell) result).bounds();
//        BoundingBox expBounds = box.bounds();

//        assertEquals(box.faces().size(), ((Shell) result).faces().size());
//        assertArrayEquals(expBounds.max, resultBounds.max);
//        assertArrayEquals(expBounds.min, resultBounds.min);
    }

    public static void assertArrayEquals(Array<? extends Object> a1, Array<? extends Object> a2) {

        assertEquals(a1.length, a2.length);

        for (int i = 0; i < a1.length; i++) {

            assertEquals(Runtime.toDouble(a1.get(i)), Runtime.toDouble(a2.get(i)), Tolerance.TOLERANCE);
        }
    }

}
