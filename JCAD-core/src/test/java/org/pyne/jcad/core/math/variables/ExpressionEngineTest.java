package org.pyne.jcad.core.math.variables;

import org.junit.*;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.PathMath;
import org.pyne.jcad.core.math.Point3DC;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExpressionEngineTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test() {
        System.out.println("test");

        Argument height = new Argument("height=2");
        Argument corner = new Argument("corner=0.1");
        Argument thickness = new Argument("thickness=0.01");
        Function height2 = new Function("h2", "height*2", "height");
        Expression someValue = new Expression("h2 - corner * 2", corner, thickness, height2, height);

//        assertEquals(4, new Expression("height2", corner, height, thickness, height2).calculate(), CadMath.EPSILON);
        assertEquals(3.8, someValue.calculate(), CadMath.EPSILON);

        height.setArgumentValue(3);

        assertEquals(5.8, someValue.calculate(), CadMath.EPSILON);
        assertEquals(6, new Expression("height2", height2).calculate(), CadMath.EPSILON);

    }
}
