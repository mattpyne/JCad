package org.pyne.jcad.core.math.brep.eval;

import org.junit.Test;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.SurfacePrimitives;

import static org.junit.Assert.assertEquals;

public class BrepReconstructerTest {

    @Test
    public void reconstruct_SingleFace() {
        Shell shell = new BrepBuilder()
                .face(
                        new BrepSurface(
                                SurfacePrimitives.loft(
                                        CurvePrimitives.line(
                                                new Point3DC(0., 0., 0.),
                                                new Point3DC(1., 0., 0.)),
                                        CurvePrimitives.line(
                                                new Point3DC(1., 1., 0.),
                                                new Point3DC(0., 1., 0.))
                                )
                        )
                )
                .loop(
                        new Vertex(0., 0., 0.),
                        new Vertex(1., 0., 0),
                        new Vertex(1., 1., 0.),
                        new Vertex(0., 1., 0.)
                )
                .build();


        assertEquals(1, shell.streamLoops().count());
        assertEquals(4, shell.streamEdges().count());
        assertEquals(4, shell.vertexs().size());

        shell = BrepReconstructer.reassemble(shell);

        assertEquals(1, shell.streamLoops().count());
        assertEquals(4, shell.getHalfEdges().size());
        assertEquals(4, shell.vertexs().size());

    }

}