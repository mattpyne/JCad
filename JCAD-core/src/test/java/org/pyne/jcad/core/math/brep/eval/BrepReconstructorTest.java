package org.pyne.jcad.core.math.brep.eval;

import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.cadexchange.step.reader.StepReader;
import org.pyne.jcad.core.math.brep.Shell;

/**
 *
 * @author Matthew
 */
public class BrepReconstructorTest {
    
    public BrepReconstructorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of reassemble method, of class BrepReconstructer.
     */
    @Test
    public void stepImportReassembledCorrectly() throws Exception {
        System.out.println("stepImportReassembledCorrectly");
        File file = new File(ClassLoader.getSystemResource("step203example.stp").toURI());
        assertTrue(file.exists());
        StepReader instance = new StepReader();
        Shell stepShell = (Shell) instance.read(file);

        assertTrue(BrepValidator.isValid(stepShell));
    }

}
