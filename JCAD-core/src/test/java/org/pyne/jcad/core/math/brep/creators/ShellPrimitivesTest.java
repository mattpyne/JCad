package org.pyne.jcad.core.math.brep.creators;

import eu.mihosoft.vvecmath.Vector3d;
import haxe.root.Array;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.PathMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.BoundingBox;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
public class ShellPrimitivesTest {

    public ShellPrimitivesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of box method, of class Primitives.
     */
    @Test
    public void testBox() {
        System.out.println("box");
        double w = 1.0;
        double h = 2.0;
        double d = 4.0;
        Shell result = ShellPrimitives.box(w, h, d, null);

        Set<Loop> loops = result.faces()
                .stream()
                .flatMap(f -> f.loops().stream())
                .collect(Collectors.toCollection(HashSet::new));

        assertEquals(6, loops.size());

        Set<Loop> twinLoops = result.faces()
                .stream()
                .flatMap(f -> f.halfEdges().stream())
                .map(e -> e.loop())
                .collect(Collectors.toCollection(HashSet::new));
        assertEquals(6, loops.size());

        Set<Loop> allLoops = new HashSet<>();
        allLoops.addAll(loops);
        allLoops.addAll(twinLoops);

        assertEquals(6, allLoops.size());

        for (Loop loop : loops) {
            assertTrue(twinLoops.contains(loop));
        }

        List<Face> facesInZUP = result.faces()
                .stream()
                .filter(face -> Tolerance.veq(face.surface().normal(Point3DC.ZERO), Vector3d.Z_ONE))
                .collect(Collectors.toList());
        assertEquals(1, facesInZUP.size());
        Face topFace = facesInZUP.get(0);
        assertTrue(Tolerance.ueq(2., topFace.getVertices().get(0).point().z()));
        assertTrue(PathMath.isCCW(topFace.getOuterLoop().getPoints()));

        List<Face> facesInZDown = result.faces()
                .stream()
                .filter(face -> Tolerance.veq(face.surface().normal(Point3DC.ZERO), Vector3d.Z_ONE.negated()))
                .collect(Collectors.toList());
        assertEquals(1, facesInZDown.size());
        Face bottomFace = facesInZDown.get(0);
        assertTrue(Tolerance.ueq(-2., bottomFace.getVertices().get(0).point().z()));
        assertTrue(!PathMath.isCCW(bottomFace.getOuterLoop().getPoints()));

        assertOutwardFaceDirections(result);
    }

    /**
     * Test of box method, of class ShellPrimitives.
     */
    @Test
    public void testBox_4args() {
        System.out.println("box");
        double width = 3.0;
        double height = 8.0;
        double depth = 10.0;
        Matrix3 transform = new Matrix3().translate(5, 2, 1);
        Shell result = ShellPrimitives.box(width, height, depth, transform);

        BoundingBox bounds = result.bounds();

        assertEquals(new Point3DC(6.5, 6, 6), new Point3DC(bounds.max));
        assertEquals(new Point3DC(3.5, -2, -4), new Point3DC(bounds.min));
    }

    /**
     * Test of box method, of class ShellPrimitives.
     */
    @Test
    public void testBox_4args_2() {
        System.out.println("box");
        Shell result = ShellPrimitives.box(30, 30, 250, new Matrix3().translate(10, 29, 0));

        BoundingBox bounds = result.bounds();

        assertEquals(new Point3DC(15 + 10, 15 + 29, 125), new Point3DC(bounds.max));
        assertEquals(new Point3DC(-15 + 10, -15 + 29, -125), new Point3DC(bounds.min));

        assertOutwardFaceDirections(result);
    }

    /**
     * Test of box method, of class ShellPrimitives.
     */
    @Test
    public void testBox_3args() {
        System.out.println("box");
        double width = 3.0;
        double height = 8.0;
        double depth = 10.0;
        Shell result = ShellPrimitives.box(width, height, depth);

        BoundingBox bounds = result.bounds();
        assertEquals(new Point3DC(1.5, 4, 5), new Point3DC(bounds.max));
        assertEquals(new Point3DC(-1.5, -4, -5), new Point3DC(bounds.min));

        assertOutwardFaceDirections(result);
    }

    /**
     * Test of cylinder method, of class ShellPrimitives.
     */
    @Test
    public void testCylinder() {
        System.out.println("cylinder");
        double radius = 3.0;
        double height = 10.0;
        Shell result = ShellPrimitives.cylinder(radius, height);

        BoundingBox bounds = result.bounds();
        assertEquals(new Point3DC(3, 3, 5), new Point3DC(bounds.max));
        assertEquals(new Point3DC(-3, -3, -5), new Point3DC(bounds.min));

        assertOutwardFaceDirections(result);
    }

    @Test
    public void testCircleSplit() {
        System.out.println("split circle");
        ParametricCurve c = CurvePrimitives.circle(Point3DC.ZERO, 10);
        List<ParametricCurve> curves = c.split(0.5);

        ParametricCurve c1 = curves.get(0);
        ParametricCurve c2 = curves.get(1);

        Point3DC p00 = c1.point(0);
        Point3DC p01 = c1.point(1.);
        Point3DC p10 = c2.point(0);
        Point3DC p11 = c2.point(1.);
        c1.asNurbs().getVerb()._data.knots = Array.from(new Number[]{0, 0, 0, 0.5, 0.5, 1, 1, 1});
        Point3DC cp00 = c1.point(c1.param(Point3DC.X_ONE.times(20)));
        Point3DC cp01 = c1.point(c1.param(Point3DC.X_ONE.times(-20)));
        Point3DC cp10 = c2.point(c2.param(Point3DC.X_ONE.times(20)));
        Point3DC cp11 = c2.point(c2.param(Point3DC.X_ONE.times(-20)));
    }

    private void assertOutwardFaceDirections(Shell result) {

        BoundingBox b = result.bounds();
        Point3DC center = new Point3DC(Vec.mul(0.5, Vec.add((Array<Number>) (Object) b.max, (Array<Number>) (Object) b.min)));

        // Check if all faces point away from the origin
        for (Face face : result.faces()) {
            Point3DC pointOnFace = face.surface().middlePt();
            Point3DC normal = face.surface().normal(pointOnFace);

            Point3DC dirFromOrigToPt = pointOnFace.minus(center).normalized();

            double dotNDirOrig = normal.dot(dirFromOrigToPt);

            assertTrue(dotNDirOrig > 0);
        }
    }
}
