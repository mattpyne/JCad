package org.pyne.jcad.core.math;

import eu.mihosoft.vvecmath.Vector3d;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.creators.ShellPrimitives;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.geom.CurvePrimitives;

/**
 *
 * @author Matthew
 */
public class TransformerTest {

    public TransformerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of translate method, of class Transformer.
     */
    @Test
    public void testTranslate_Shell() {
        System.out.println("translate");
        Shell box = ShellPrimitives.box(10, 10, 10, null);
        Face topo = box.faces().get(0);
        
        double x = 1.0;
        double y = 0.0;
        double z = 0.0;
        Transformer instance = new Transformer();
        instance.translate(topo, x, y, z);

    }

    /**
     * Test of translate method, of class Transformer.
     */
    @Test
    public void testTranslate_Edge() {
        System.out.println("translate edge");
        Point3DC p1 = Point3DC.ZERO;
        Point3DC p2 = Point3DC.X_ONE.times(2);
        Edge topo = new Edge(new BrepCurve(CurvePrimitives.line(p1, p2)), new Vertex(p1), new Vertex(p2));
        double x = 1.0;
        double y = 0.0;
        double z = 0.0;
        Transformer instance = new Transformer();
        instance.translate(topo, x, y, z);
        Vector3d p1After = topo.curve().point(0);
        Vector3d p2After = topo.curve().point(1);
        assertTrue(p1After.equals(new Point3DC(1, 0, 0)));
        assertTrue(p2After.equals(new Point3DC(3, 0, 0)));
        assertTrue(topo.halfEdge1().getPos1().equals(new Point3DC(1, 0, 0)));
        assertTrue(topo.halfEdge1().getPos2().equals(new Point3DC(3, 0, 0)));
        assertTrue(topo.halfEdge2().getPos2().equals(new Point3DC(1, 0, 0)));
        assertTrue(topo.halfEdge2().getPos1().equals(new Point3DC(3, 0, 0)));
        
        x = 0.0;
        y = 1.0;
        z = 0.0;
        instance = new Transformer();
        instance.translate(topo, x, y, z);
        p1After = topo.curve().point(0);
        p2After = topo.curve().point(1);
        assertTrue(p1After.equals(new Point3DC(1, 1, 0)));
        assertTrue(p2After.equals(new Point3DC(3, 1, 0)));
        assertTrue(topo.halfEdge1().getPos1().equals(new Point3DC(1, 1, 0)));
        assertTrue(topo.halfEdge1().getPos2().equals(new Point3DC(3, 1, 0)));
        assertTrue(topo.halfEdge2().getPos2().equals(new Point3DC(1, 1, 0)));
        assertTrue(topo.halfEdge2().getPos1().equals(new Point3DC(3, 1, 0)));
        
        x = 0.0;
        y = 0.0;
        z = 1.0;
        instance = new Transformer();
        instance.translate(topo, x, y, z);
        p1After = topo.curve().point(0);
        p2After = topo.curve().point(1);
        assertTrue(p1After.equals(new Point3DC(1, 1, 1)));
        assertTrue(p2After.equals(new Point3DC(3, 1, 1)));
        assertTrue(topo.halfEdge1().getPos1().equals(new Point3DC(1, 1, 1)));
        assertTrue(topo.halfEdge1().getPos2().equals(new Point3DC(3, 1, 1)));
        assertTrue(topo.halfEdge2().getPos2().equals(new Point3DC(1, 1, 1)));
        assertTrue(topo.halfEdge2().getPos1().equals(new Point3DC(3, 1, 1)));
    }

}
