package org.pyne.jcad.core.math.brep.creators;

import eu.mihosoft.vvecmath.Vector3d;
import java.util.List;
import org.junit.Test;
import org.pyne.jcad.brep.BrepAssert;
import org.pyne.jcad.core.entity.ExtrudeOperation;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.ParametricCurve;

/**
 *
 * @author Matthew Pyne
 */
public class ExtrudeTest {

    @Test
    public void extrudeLinearZ() {
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();

        Face face = BrepFaceEvolve.EvolveOneFace(
                BrepBuilder.createBoundingNurbs(circle.tessellate(), null),
                loop);

        Shell extrusion = BrepEnclose.extrude(face, Point3DC.Z_ONE);

        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeLinearNegZ() {
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();

        Face face = BrepFaceEvolve.EvolveOneFace(
                BrepBuilder.createBoundingNurbs(circle.tessellate(), null),
                loop);

        Shell extrusion = BrepEnclose.extrude(face, Point3DC.Z_ONE.times(-1));

        BrepAssert.assertShellValid(extrusion);
    }

    @Test
    public void extrudeLinearZX() {
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();

        Face face = BrepFaceEvolve.EvolveOneFace(
                BrepBuilder.createBoundingNurbs(circle.tessellate(), null),
                loop);

        Shell extrusion = BrepEnclose.extrude(face, Point3DC.Z_ONE.plus(Vector3d.X_ONE));

        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeLinearNegZX() {
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();

        Face face = BrepFaceEvolve.EvolveOneFace(
                BrepBuilder.createBoundingNurbs(circle.tessellate(), null),
                loop);

        Shell extrusion = BrepEnclose.extrude(face, Point3DC.Z_ONE.plus(Vector3d.X_ONE).times(-1));

        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeOpTestZ() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue("10");
        operation.getxDir().setValue("0");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeOpTestOffsetZ() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue(10);
        operation.setOffsetValue(1);
        operation.getxDir().setValue("0");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeOpTestNegZ() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue(10);
        operation.getxDir().setValue("0");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("-1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeOpTestZX() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue(10);
        operation.getxDir().setValue("1");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }

    @Test
    public void extrudeOpTestOffsetZX() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue(10);
        operation.setOffsetValue(2);
        operation.getxDir().setValue("1");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeOpTestNegZX() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue(10);
        operation.getxDir().setValue("-1");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("-1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }
    
    @Test
    public void extrudeOpTestOffsetNegZX() {
        ExtrudeOperation operation = new ExtrudeOperation();
        
        Curve circle = CurvePrimitives.circle(Point3DC.ZERO, 5);
        List<ParametricCurve> curves = circle.split(0.5);

        Loop loop = new Loop();
        loop.addEdge(new Edge(new BrepCurve(curves.get(0))).halfEdge1());
        loop.addEdge(new Edge(new BrepCurve(curves.get(1))).halfEdge1());
        loop.link();
        
        operation.setAmountValue(10);
        operation.setOffsetValue(2);
        operation.getxDir().setValue("-1");
        operation.getyDir().setValue("0");
        operation.getzDir().setValue("-1");
        operation.getLoops().add(loop);
        
        Shell extrusion = (Shell) operation.topoOp();
        
        BrepAssert.assertShellValid(extrusion);
    }
}
