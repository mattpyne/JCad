/**
 * NurbsCurveTrans -- A GeomTransform that has a NurbsCurve for a child.
 *
 * Copyright (C) 2009-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.nurbs;

import geomss.geom.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} object that refers to a {@link NurbsCurve} object and
 * masquerades as a NurbsCurve object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 30, 2009
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class NurbsCurveTrans extends NurbsCurve implements GeomTransform<NurbsCurve> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private NurbsCurve _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link NurbsCurveTrans} instance holding the specified
     * {@link NurbsCurve} and {@link GTransform}.
     *
     * @param child The NurbsCurve that is the child of this transform element (may not be
     * <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     * <code>null</code>).
     * @return the transform element having the specified values.
     */
    public static NurbsCurveTrans newInstance(NurbsCurve child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "NURBS curve", child.getPhyDimension()));

        NurbsCurveTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation represented by this element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     *
     * @return The total transformation represented by the entire chain of transform
     * objects.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     * <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this element.
     */
    @Override
    public NurbsCurve getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of this object with any transformations or subranges removed
     * (applied).
     */
    @Override
    public BasicNurbsCurve copyToReal() {

        //  Transform the child curve's control points.
        FastTable<ControlPoint> transCPList = (FastTable)getControlPoints();

        //  Create a new curve from the transformed control point list.
        BasicNurbsCurve crv = BasicNurbsCurve.newInstance(transCPList, _child.getKnotVector());
        crv.setName(_child.getName());
        crv.putAllUserData(_child.getAllUserData());

        FastTable.recycle(transCPList);
        return crv;
    }

    /**
     * Return an immutable version of this NURBS curve.
     *
     * @return an immutable version of this curve.
     */
    @Override
    public BasicNurbsCurve immutable() {
        return copyToReal();
    }
    
    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation returns the number of control points in the child NURBS curve.
     */
    @Override
    public int size() {
        return _child.size();
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Return a list of transformed control points for this curve.
     *
     * @return the ordered control points
     */
    @Override
    public List<ControlPoint> getControlPoints() {

        //  Loop over all the control points in the child curve and transform each in turn.
        FastTable<ControlPoint> transCPList = FastTable.newInstance();

        StackContext.enter();
        try {
            FastTable<ControlPoint> cPoly = (FastTable<ControlPoint>)_child.getControlPoints();
            int size = cPoly.size();
            for (int i = 0; i < size; ++i) {
                ControlPoint cp = cPoly.get(i);

                //  Transform the control point's geomssic point by this transformation.
                Point pnt = cp.getPoint().getTransformed(_TM).copyToReal();

                //  Create a transformed control point.
                ControlPoint newCP = ControlPoint.valueOf(pnt, cp.getWeight());
                transCPList.add(StackContext.outerCopy(newCP));
            }

        } finally {
            StackContext.exit();
        }

        return transCPList;
    }

    /**
     * Return the knot vector of this curve.
     *
     * @return The knot vector.
     */
    @Override
    public KnotVector getKnotVector() {
        return _child.getKnotVector();
    }

    /**
     * Return the degreeU of the NURBS curve.
     *
     * @return degreeU of curve
     */
    @Override
    public int getDegree() {
        return _child.getDegree();
    }

    /**
     * Calculate a point on the curve for the given parametric distance along the curve,
     * <code>p(s)</code>.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return the calculated point
     */
    @Override
    public Point getRealPoint(double s) {
        return _TM.transform(_child.getRealPoint(s));
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the curve for the given parametric distance along
     * the curve, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s Parametric distance to calculate derivatives for (0.0 to 1.0 inclusive).
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     * 2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at the
     * specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, int grade) {
        BasicNurbsCurve trans = copyToReal();
        List<Vector<Length>> ders = trans.getSDerivatives(s, grade);
        BasicNurbsCurve.recycle(trans);
        return ders;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        BasicNurbsCurve trans = copyToReal();
        Point minPoint = trans.getBoundsMin();
        BasicNurbsCurve.recycle(trans);
        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        BasicNurbsCurve trans = copyToReal();
        Point maxPoint = trans.getBoundsMax();
        BasicNurbsCurve.recycle(trans);
        return maxPoint;
    }

    /**
     * Returns the unit in which the control points in this curve are stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this curve but stated in the specified unit.
     *
     * @param unit The length unit of the curve to be returned. May not be null.
     * @return An equivalent to this curve but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public NurbsCurveTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        return NurbsCurveTrans.newInstance(_child.to(unit), _TM);
    }

    /**
     * Return the equivalent of this curve converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the curve to return. MUST equal 3.
     * @return The equivalent of this curve converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public NurbsCurveTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Returns a copy of this NurbsCurveTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public NurbsCurveTrans copy() {
        return copyOf(this);
    }

    /**
     * Compares this NurbsCurveTrans against the specified object for strict equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this transform is identical to that transform;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        NurbsCurveTrans that = (NurbsCurveTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<NurbsCurveTrans> XML = new XMLFormat<NurbsCurveTrans>(NurbsCurveTrans.class) {

        @Override
        public NurbsCurveTrans newInstance(Class<NurbsCurveTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, NurbsCurveTrans obj) throws XMLStreamException {
            NurbsCurve.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            NurbsCurve child = xml.getNext();
            obj._child = child;

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(NurbsCurveTrans obj, OutputElement xml) throws XMLStreamException {
            NurbsCurve.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<NurbsCurveTrans> FACTORY = new ObjectFactory<NurbsCurveTrans>() {
        @Override
        protected NurbsCurveTrans create() {
            return new NurbsCurveTrans();
        }

        @Override
        protected void cleanup(NurbsCurveTrans obj) {
            obj.reset();
            obj._TM = null;
            if (Objects.nonNull(obj._childChangeListener))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    /**
     * Recycles a <code>NurbsCurveTrans</code> instance immediately (on the stack when
     * executing in a StackContext).
     * 
     * @param instance The instance to be recycled.
     */
    public static void recycle(NurbsCurveTrans instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static NurbsCurveTrans copyOf(NurbsCurveTrans original) {
        NurbsCurveTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

    /**
     * Do not allow the default constructor to be used.
     */
    private NurbsCurveTrans() { }

}
