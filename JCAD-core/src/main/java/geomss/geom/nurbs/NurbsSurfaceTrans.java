/**
 * NurbsSurfaceTrans -- A GeomTransform that has a NurbsSurface for a child.
 *
 * Copyright (C) 2010-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.nurbs;

import geomss.geom.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} object that refers to a {@link NurbsSurface} object and
 * masquerades as a NurbsSurface object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 14, 2010
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class NurbsSurfaceTrans extends NurbsSurface implements GeomTransform<NurbsSurface> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private NurbsSurface _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new MyChangeListener(this);

    //  The following supports an optimization that caches the "copyToReal" version of the child
    //  in order to speed up many calculations.
    private BasicNurbsSurface _realChild;

    /**
     * Returns a 3D {@link NurbsSurfaceTrans} instance holding the specified
     * {@link NurbsSurface} and {@link GTransform}.
     *
     * @param child     The NurbsSurface that is the child of this transform element (may
     *                  not be <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     */
    public static NurbsSurfaceTrans newInstance(NurbsSurface child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "NURBS surface", child.getPhyDimension()));

        NurbsSurfaceTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;
        obj._realChild = obj.privateCopyToReal();

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    //  A change listener that recreates the realChild geometry as well as
    //  passing the child's change event on to any listeners.
    private static class MyChangeListener extends ForwardingChangeListener {

        private final NurbsSurfaceTrans thisTrans;

        public MyChangeListener(NurbsSurfaceTrans trans) {
            super(trans);
            this.thisTrans = trans;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            //  Re-create the internal real copy of the child geometry.
            thisTrans.privateCopyToReal();
            super.stateChanged(e);
        }
    }

    /**
     * Returns the transformation represented by this transformation element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (!transform.equals(_TM)) {
            //  Create a new "real" child surface.
            _TM = transform;
            _realChild = privateCopyToReal();
            fireChangeEvent();
        }

    }

    /**
     * Returns the child object transformed by this transform element.
     */
    @Override
    public NurbsSurface getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public NurbsSurface copyToReal() {
        return _realChild;
    }

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation returns the number of control points in the child NURBS surface.
     */
    @Override
    public int size() {
        return _child.size();
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Return a matrix or network of control points for this surface.
     *
     * @return the ordered control points
     */
    @Override
    public ControlPointNet getControlPoints() {
        return _realChild.getControlPoints();
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     */
    private BasicNurbsSurface privateCopyToReal() {
        StackContext.enter();
        try {
            //  Transform the child surface's control points.
            ControlPointNet transCPNet = privateGetControlPoints();

            //  Create a new surface from the transformed control point list.
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(transCPNet,
                    _child.getSKnotVector().copy(), _child.getTKnotVector().copy());
            srf.setName(_child.getName());
            srf.putAllUserData(_child.getAllUserData());

            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a matrix or network of control points for this surface.
     *
     * @return the ordered control points
     */
    private ControlPointNet privateGetControlPoints() {

        //  Loop over all the control points in the child surface and transform each in turn.
        ControlPointNet cpNet = _child.getControlPoints();

        FastTable<FastTable<ControlPoint>> transCPNet = FastTable.newInstance();
        int ncol = cpNet.getNumberOfColumns();
        for (int i = 0; i < ncol; ++i) {
            List<ControlPoint> tbl = cpNet.getColumn(i);
            FastTable<ControlPoint> transCPList = FastTable.newInstance();
            transCPNet.add(transCPList);

            int nrow = tbl.size();
            for (int j = 0; j < nrow; ++j) {
                ControlPoint cp = tbl.get(j);

                //  Transform the control point's geometric point by this transformation.
                Point pnt = _TM.transform(cp.getPoint()).copyToReal();

                //  Create a transformed control point.
                ControlPoint newCP = ControlPoint.valueOf(pnt, cp.getWeight());
                transCPList.add(newCP);
            }
        }

        //  Create a new control point net from the transformed control point list.
        ControlPointNet cpNet2 = ControlPointNet.valueOf(transCPNet);

        return cpNet2;
    }

    /**
     * Return the control point matrix size in the s-direction (down a column of control
     * points).
     *
     * @return The control point matrix size in the s-direction.
     */
    @Override
    public int getNumberOfRows() {
        return _child.getNumberOfRows();
    }

    /**
     * Return the control point matrix size in the t-direction (across the columns of
     * control points).
     *
     * @return The control point matrix size in the t-direction.
     */
    @Override
    public int getNumberOfColumns() {
        return _child.getNumberOfColumns();
    }

    /**
     * Return the s-direction knot vector of this surface.
     *
     * @return The s-knot vector.
     */
    @Override
    public KnotVector getSKnotVector() {
        return _child.getSKnotVector();
    }

    /**
     * Return the t-direction knot vector of this surface.
     *
     * @return The t-knot vector.
     */
    @Override
    public KnotVector getTKnotVector() {
        return _child.getTKnotVector();
    }

    /**
     * Return the s-degreeU of the NURBS surface.
     *
     * @return s-degreeU of surface
     */
    @Override
    public int getSDegree() {
        return _child.getSDegree();
    }

    /**
     * Return the t-degreeU of the NURBS surface.
     *
     * @return t-degreeU of surface
     */
    @Override
    public int getTDegree() {
        return _child.getTDegree();
    }

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param s 1st parametric dimension distance to get the point for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to get the point for (0.0 to 1.0
     *          inclusive).
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(double s, double t) {
        return _realChild.getRealPoint(s, t);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     1st parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param t     2nd parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param grade The maximum grade to calculate the u-derivatives for (1=1st
     *              derivative, 2=2nd derivative, etc)
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade, boolean scaled) {
        return _realChild.getSDerivatives(s, t, grade, scaled);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s     1st parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param t     2nd parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param grade The maximum grade to calculate the v-derivatives for (1=1st
     *              derivative, 2=2nd derivative, etc)
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade, boolean scaled) {
        return _realChild.getTDerivatives(s, t, grade, scaled);
    }

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param s 1st parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    @Override
    public Vector<Length> getTwistVector(double s, double t) {
        return _realChild.getTwistVector(s, t);
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        Point minPoint = _realChild.getBoundsMin();
        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        Point maxPoint = _realChild.getBoundsMax();
        return maxPoint;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     */
    @Override
    public NurbsSurfaceTrans getTransformed(GTransform transform) {
        return NurbsSurfaceTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the unit in which the control points in this curve are stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this surface but stated in the specified unit.
     *
     * @param unit the length unit of the surface to be returned. May not be null.
     * @return an equivalent to this surface but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public NurbsSurfaceTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        return NurbsSurfaceTrans.newInstance(_child.to(unit), _TM);
    }

    /**
     * Return the equivalent of this surface converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the surface to return. MUST equal 3.
     * @return The equivalent of this surface converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public NurbsSurfaceTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Returns a copy of this NurbsSurfaceTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public NurbsSurfaceTrans copy() {
        return NurbsSurfaceTrans.copyOf(this);
    }

    /**
     * Compares this NurbsSurfaceTrans against the specified object for strict equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this transform is identical to that transform;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        NurbsSurfaceTrans that = (NurbsSurfaceTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Holds the default XML representation. For example:
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<NurbsSurfaceTrans> XML = new XMLFormat<NurbsSurfaceTrans>(NurbsSurfaceTrans.class) {

        @Override
        public NurbsSurfaceTrans newInstance(Class<NurbsSurfaceTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, NurbsSurfaceTrans obj) throws XMLStreamException {
            NurbsSurface.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            NurbsSurface child = xml.getNext();
            obj._child = child;
            obj._realChild = obj.privateCopyToReal();

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(NurbsSurfaceTrans obj, OutputElement xml) throws XMLStreamException {
            NurbsSurface.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<NurbsSurfaceTrans> FACTORY = new ObjectFactory<NurbsSurfaceTrans>() {
        @Override
        protected NurbsSurfaceTrans create() {
            return new NurbsSurfaceTrans();
        }

        @Override
        protected void cleanup(NurbsSurfaceTrans obj) {
            obj.reset();
            obj._TM = null;
            if (nonNull(obj._childChangeListener))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
            obj._realChild = null;
        }
    };

    /**
     * Recycles a <code>NurbsSurfaceTrans</code> instance immediately (on the stack when
     * executing in a StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(NurbsSurfaceTrans instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static NurbsSurfaceTrans copyOf(NurbsSurfaceTrans original) {
        NurbsSurfaceTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        obj._realChild = original._realChild.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    private NurbsSurfaceTrans() { }

}
