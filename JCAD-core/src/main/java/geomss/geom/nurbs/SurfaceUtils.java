/**
 * SurfaceUtils -- Utility methods for working with NURBS surfaces.
 * 
 * Copyright (C) 2009-2015, by Joseph A. Huwaldt. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 * 
 * This source file is based on, but heavily modified from, the LGPL jgeom (Geometry
 * Library for Java) code by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import geomss.geom.GeomList;
import java.util.List;
import static java.util.Objects.requireNonNull;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.util.FastTable;

/**
 * A collection of utility methods for working with NURBS surfaces.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: September 10, 2010
 * @version November 28, 2015
 */
public final class SurfaceUtils {

    /**
     * The resource bundle for this package.
     */
    //private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    /**
     * Elevate the degreeU of a surface to the specified degreeU in the S & T directions. If
     * the specified degrees are both &le; the current degreeU of the surface in each
     * direction, then no change is made and a reference to the input surface is returned.
     *
     * @param srf     The surface to have the degrees elevated. May not be null.
     * @param degreeS The desired degreeU for the surface to be elevated to in the
     *                S-direction.
     * @param degreeT The desired degreeU for the surface to be elevated to in the
     *                T-direction.
     * @return The input NurbsSurface with the S & T degreeU elevated to the specified
     *         degrees.
     */
    public static NurbsSurface elevateToDegree(NurbsSurface srf, int degreeS, int degreeT) {
        int pS = srf.getSDegree();
        int pT = srf.getTDegree();
        if (degreeS <= pS && degreeT <= pT)
            return srf;

        int tS = degreeS - pS;
        int tT = degreeT - pT;
        return degreeElevate(srf, tS, tT);
    }

    /**
     * Elevate the degreeU of a surface by the specified number of times in the S and T
     * directions.
     *
     * @param srf The surface to have the degrees elevated. May not be null.
     * @param tS  The number of times to elevate the degreeU in the S-direction.
     * @param tT  The number of times to elevate the degreeU in the T-direction.
     * @return The input NurbsSurface with the S & T degreeU elevated by the specified
     *         amount.
     */
    public static NurbsSurface degreeElevate(NurbsSurface srf, int tS, int tT) {
        srf = degreeElevateS(requireNonNull(srf), tS);
        srf = degreeElevateT(srf, tT);
        return srf;
    }

    /**
     * Elevate the degreeU of a surface by the specified number of times in the
     * S-direction.
     *
     * @param srf The surface to have the S-degreeU elevated.
     * @param t   The number of times to elevate the degreeU in the S-direction.
     * @return The input NurbsSurface with the S-degreeU elevated by the specified amount.
     */
    private static NurbsSurface degreeElevateS(NurbsSurface srf, int t) {
        if (t <= 0)
            return srf;

        StackContext.enter();
        try {
            KnotVector kv = null;
            FastTable<List<ControlPoint>> cpNetList = FastTable.newInstance();

            //  Degree elevate each defining column curve.
            int numCols = srf.getNumberOfColumns();
            for (int i = 0; i < numCols; ++i) {
                BasicNurbsCurve c1 = srf.getSCurve(i);
                NurbsCurve c2 = CurveUtils.degreeElevate(c1, t);
                kv = c2.getKnotVector();
                cpNetList.add(c2.getControlPoints());
            }

            //  Create a new surface from the new knot vector and set of control points.
            ControlPointNet cpNet = ControlPointNet.valueOf(cpNetList);
            BasicNurbsSurface newSrf = BasicNurbsSurface.newInstance(cpNet, kv, srf.getTKnotVector());
            return StackContext.outerCopy(newSrf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Elevate the degreeU of a surface by the specified number of times in the
     * T-direction.
     *
     * @param srf The surface to have the T-degreeU elevated.
     * @param t   The number of times to elevate the degreeU in the T-direction.
     * @return The input NurbsSurface with the T-degreeU elevated by the specified amount.
     */
    private static NurbsSurface degreeElevateT(NurbsSurface srf, int t) {
        if (t <= 0)
            return srf;

        StackContext.enter();
        try {
            KnotVector kv = null;
            FastTable<List<ControlPoint>> cpNetList = FastTable.newInstance();

            //  Degree elevate each defining row curve.
            int numRows = srf.getNumberOfRows();
            for (int j = 0; j < numRows; ++j) {
                BasicNurbsCurve c1 = srf.getTCurve(j);
                NurbsCurve c2 = CurveUtils.degreeElevate(c1, t);
                kv = c2.getKnotVector();
                cpNetList.add(c2.getControlPoints());
            }

            //  Create a new surface from the new knot vector and set of control points.
            ControlPointNet cpNet = ControlPointNet.valueOf(cpNetList).transpose();
            BasicNurbsSurface newSrf = BasicNurbsSurface.newInstance(cpNet, srf.getSKnotVector(), kv);
            return StackContext.outerCopy(newSrf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Decompose a NURBS surface into a list of lists of Bezier patches. The returned list
     * contains lists of NURBS surfaces that represent each Bezier patch. Each list
     * contains patches in the s-direction (t-direction is across the lists).
     *
     * @param srf The surface to be decomposed into Bezier patches. May not be null.
     * @return A list of lists of Bezier patches that have a degreeU of at least 2.
     */
    public static GeomList<GeomList<BasicNurbsSurface>> decomposeToBezier(NurbsSurface srf) {
        int numCrvRows = srf.getNumberOfRows();

        //  Create knot vectors for the Bezier patches.
        KnotVector bezierKVs = CurveUtils.bezierKnotVector(max(srf.getSDegree(), 2));
        KnotVector bezierKVt = CurveUtils.bezierKnotVector(max(srf.getTDegree(), 2));

        //  First, loop over the rows of the control point network and extract Bezier segments in the T-direction.
        FastTable<List<? extends List<ControlPoint>>> strips = FastTable.newInstance();
        ControlPointNet cpNet = srf.getControlPoints();
        for (int rowIdx = 0; rowIdx < numCrvRows; ++rowIdx) {
            List<ControlPoint> cps = cpNet.getRow(rowIdx);
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, srf.getTKnotVector());
            GeomList<BasicNurbsCurve> segments = CurveUtils.decomposeToBezier(crv);

            //  Turn curve segments into a list of control point lists.
            FastTable<List<ControlPoint>> cpSegments = FastTable.newInstance();
            int nSegs = segments.size();
            for (int i = 0; i < nSegs; ++i) {
                BasicNurbsCurve seg = segments.get(i);
                cpSegments.add(seg.getControlPoints());
            }

            strips.add(cpSegments);
        }

        int numPatchCols = strips.get(0).size();
        int numColsPerPatch = strips.get(0).get(0).size();

        //  Create the output list of lists.
        GeomList<GeomList<BasicNurbsSurface>> patches = GeomList.newInstance();
        for (int i = 0; i < numPatchCols; ++i) {
            patches.add(GeomList.newInstance());
        }

        StackContext.enter();
        try {
            //  Loop over the all the columns of Bezier patches in this surface.
            FastTable<ControlPoint> cps = FastTable.newInstance();
            for (int patchCol = 0; patchCol < numPatchCols; ++patchCol) {

                //  Loop over all the columns of control points in this Bezier patch column.
                FastTable<List<? extends List<ControlPoint>>> patchSegmentLists = FastTable.newInstance();
                for (int colIdx = 0; colIdx < numColsPerPatch; ++colIdx) {
                    //  Create a curve in s-direction from a column of control points from the Bezier segments in this patch column.
                    for (int rowIdx = 0; rowIdx < numCrvRows; ++rowIdx) {
                        cps.add(strips.get(rowIdx).get(patchCol).get(colIdx));
                    }
                    BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, srf.getSKnotVector());

                    //  Decompose into Bezier segments in the s-direction.
                    GeomList<BasicNurbsCurve> segments = CurveUtils.decomposeToBezier(crv);

                    //  Turn curve segments into a list of control point lists.
                    FastTable<List<ControlPoint>> cpSegments = FastTable.newInstance();
                    int nSegs = segments.size();
                    for (int i = 0; i < nSegs; ++i) {
                        BasicNurbsCurve seg = segments.get(i);
                        cpSegments.add(seg.getControlPoints());
                    }

                    //  Store the segments in patch segments lists.
                    patchSegmentLists.add(cpSegments);

                    //  Cleanup
                    cps.reset();
                }

                //  Rearrange segments into control point networks for each patch.
                int numPatchRows = patchSegmentLists.get(0).size();

                GeomList<BasicNurbsSurface> patchSrfList = patches.get(patchCol);
                FastTable<List<ControlPoint>> patchArray = FastTable.newInstance();
                for (int patchRow = 0; patchRow < numPatchRows; ++patchRow) {

                    for (int colIdx = 0; colIdx < numColsPerPatch; ++colIdx) {
                        patchArray.add(patchSegmentLists.get(colIdx).get(patchRow));
                    }

                    // Create the control point network.
                    cpNet = ControlPointNet.valueOf(patchArray);
                    patchArray.reset();

                    //  Create a surface for this patch.
                    BasicNurbsSurface patch = BasicNurbsSurface.newInstance(cpNet, bezierKVs, bezierKVt);
                    patchSrfList.add(StackContext.outerCopy(patch));
                }
            }

        } finally {
            StackContext.exit();
        }

        return patches;
    }

    /**
     * Returns a list containing the parameters at the start (and end) of each Bezier
     * patch of the specified NURBS surface in the specified direction. This first element
     * of this list will always be zero and the last will always be 1.
     *
     * @param srf The NURBS surface to extract the Bezier patches for. May not be null.
     * @param dir The direction to return the start parameters in (0=S, 1=T).
     * @return A list containing the parameters at the start (and end) of each Bezier
     *         patch of the specified NURBS surface in the specified direction.
     */
    public static FastTable<Double> getBezierStartParameters(NurbsSurface srf, int dir) {

        //  Extract the knot vector and curve degreeU.
        KnotVector kv;
        if (dir == 0)
            kv = srf.getSKnotVector();
        else
            kv = srf.getTKnotVector();
        int degree = kv.getDegree();

        //  Create a list of interior knotsU.
        FastTable<Double> knots = FastTable.newInstance();
        int size = kv.length() - degree;
        double oldS = -1;
        for (int i = degree; i < size; ++i) {
            double s = kv.getValue(i);
            if (s != oldS) {
                knots.add(s);
                oldS = s;
            }
        }

        return knots;
    }

}
