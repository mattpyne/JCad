/**
 * CurveFactory -- A factory for creating NURBS curves.
 *
 * Copyright (C) 2009-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 *
 * This source file is based on, but heavily modified from, the LGPL jgeom (Geometry
 * Library for Java) code, NurbsCreator.java, by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import geomss.geom.*;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import java.util.List;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ImmortalContext;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.util.FastTable;
import org.apache.commons.math3.linear.*;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A collection of methods for creating NURBS curves.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Samuel Gerber, Date: May 14, 2009, Version 1.0.
 * @version October 12, 2017
 */
public final class CurveFactory {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    /**
     * The length threshold for a set of points being considered a degenerate curve.
     */
    private static final double EPSC = 1e-10;

    //  The knot vector for a semi-circle.
    private static final double[] SC_KNOTS = {0, 0, 0, 0.5, 1, 1, 1};

    //  The knot vector for a full circle or ellipse.
    private static final double[] FC_KNOTS = {0, 0, 0, 0.25, 0.25, 0.5, 0.5, 0.75, 0.75, 1, 1, 1};

    //  A generic geometry space tolerance
    private static final Parameter<Length> GTOL;

    static {
        ImmortalContext.enter();
        try {
            GTOL = Parameter.valueOf(1e-8, SI.METER);
        } finally {
            ImmortalContext.exit();
        }
    }

    private CurveFactory() { }

    /**
     * Create a degenerate {@link NurbsCurve} of the specified degreeU that represents a
     * point in space.
     *
     * @param degree The degreeU of the curve to create.
     * @param p0     The location for the degenerate curve. May not be null.
     * @return A BasicNurbsCurve that represents a point.
     */
    public static BasicNurbsCurve createPoint(int degree, GeomPoint p0) {
        requireNonNull(p0);
        StackContext.enter();
        try {
            //  Create an appropriate knot vector.
            FastTable<Float64> kvList = FastTable.newInstance();
            for (int i = 0; i <= degree; ++i) {
                kvList.add(Float64.ZERO);
            }
            for (int i = 0; i <= degree; ++i) {
                kvList.add(Float64.ONE);
            }

            KnotVector kv = KnotVector.newInstance(degree, Float64Vector.valueOf(kvList));

            //  Create an appropriate control point list.
            ControlPoint cp0 = ControlPoint.valueOf(p0.immutable(), 1);
            FastTable<ControlPoint> cps = FastTable.newInstance();
            for (int i = 0; i < kv.length() - degree - 1; ++i) {
                cps.add(cp0);
            }

            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a degenerate {@link NurbsCurve} of the specified degreeU that represents a
     * point in space with the indicated number of co-located control points.
     *
     * @param degree The degreeU of the curve to create.
     * @param numCP  The number of control points to use in the degenerate curve.
     * @param p0     The location for the degenerate curve. May not be null.
     * @return A BasicNurbsCurve that represents a point.
     */
    public static BasicNurbsCurve createPoint(int degree, int numCP, GeomPoint p0) {
        requireNonNull(p0);
        StackContext.enter();
        try {
            //  Use an even parameter spacing.
            double[] uk = ArrayFactory.DOUBLES_FACTORY.array(numCP);
            int numCPm1 = numCP - 1;
            for (int i = 0; i < numCP; ++i) {
                double s = (double)(i) / numCPm1;
                uk[i] = s;
            }

            //  Create an appropriate knot vector.
            KnotVector uKnots = buildInterpKnotVector(uk, numCP, degree);

            //  Create an appropriate control point list.
            ControlPoint cp0 = ControlPoint.valueOf(p0.immutable(), 1);
            FastTable<ControlPoint> cps = FastTable.newInstance();
            for (int i = 0; i < numCP; ++i) {
                cps.add(cp0);
            }

            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, uKnots);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a 1-degreeU straight line {@link NurbsCurve} that represents the shortest
     * distance, in Euclidean space between the two input points.
     *
     * @param p0 The start of the line. May not be null.
     * @param p1 The end of the line. May not be null.
     * @return A BasicNurbsCurve that represents a line between the input points.
     */
    public static BasicNurbsCurve createLine(GeomPoint p0, GeomPoint p1) {
        StackContext.enter();
        try {
            //  Make both points have the dimension of the highest dimension point.
            int numDims = Math.max(p0.getPhyDimension(), p1.getPhyDimension());
            p0 = p0.toDimension(numDims);
            p1 = p1.toDimension(numDims);

            FastTable<ControlPoint> cps = FastTable.newInstance();
            cps.add(ControlPoint.valueOf(p0.immutable(), 1));
            cps.add(ControlPoint.valueOf(p1.immutable(), 1));
            KnotVector kv = KnotVector.newInstance(1, 0., 0., 1., 1.);

            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a straight line {@link NurbsCurve} that represents the shortest distance, in
     * Euclidean space between the two input points with the specified degreeU.
     *
     * @param degree The degreeU of the NurbsCurve to return.
     * @param p0     The start of the line. May not be null.
     * @param p1     The end of the line. May not be null.
     * @return A BasicNurbsCurve of the specified degreeU that represents a line between
     *         the input points.
     */
    public static BasicNurbsCurve createLine(int degree, GeomPoint p0, GeomPoint p1) {
        requireNonNull(p0);
        requireNonNull(p1);
        return (BasicNurbsCurve)CurveUtils.elevateToDegree(createLine(p0, p1), degree);
    }

    /**
     * Create a semi-circle {@link NurbsCurve} with the specified normal vector around the
     * given origin with radius <code>r</code>. The curve returned has a control polygon
     * which has 4 points and the shape of a rectangle.
     *
     * @param o Origin to create a semi-circle around. May not be null.
     * @param r Radius of the semi-circle May not be null.
     * @param n A unit plane normal vector for the plane containing the circle. If
     *          <code>null</code> is passed, a default normal vector that points along the
     *          Z axis is used.
     * @return A BasicNurbsCurve that represents a semi-circle
     * @throws DimensionException if the origin point or normal do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createSemiCircle(GeomPoint o, Parameter<Length> r, GeomVector<Dimensionless> n)
            throws DimensionException {

        requireNonNull(r);
        int numDims = o.getPhyDimension();
        if (nonNull(n) && n.getPhyDimension() > numDims)
            numDims = n.getPhyDimension();
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin or normal", numDims));

        StackContext.enter();
        try {
            if (nonNull(n)) {
                o = o.toDimension(numDims);
                n = n.toDimension(numDims);
            }

            //  Make sure the normal vector exists and is a unit vector.
            if (numDims == 2)
                n = null;
            else if (isNull(n))
                n = GeomUtil.makeZVector(numDims);
            else
                n = n.toUnitVector();

            //  Create the yhat and xhat vectors (orthogonal Y and X in the plane of the circle).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(n);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(n, yhat);

            //  Create a list of control points.
            FastTable<ControlPoint> cpList = FastTable.newInstance();
            cpList.setSize(4);

            double rValue = r.getValue(SI.METER);

            //  Set 0 (bottom) and 3 (top) control points.
            //  p = 0,r
            Point p = Point.valueOf(yhat);
            p = p.times(rValue);
            cpList.set(3, ControlPoint.valueOf(o.plus(p), 1));
            cpList.set(0, ControlPoint.valueOf(o.plus(p.opposite()), 1));   //  p = 0,-r

            //  Set 1 (bottom right) and 2 (top right) control points.
            //  p = r, r
            p = Point.valueOf(xhat.plus(yhat));
            p = p.times(rValue);
            cpList.set(2, ControlPoint.valueOf(o.plus(p), 0.5));

            //  p = r, -r
            p = Point.valueOf(xhat.plus(yhat.opposite()));
            p = p.times(rValue);
            cpList.set(1, ControlPoint.valueOf(o.plus(p), 0.5));

            //  Create the NURBS curve.
            KnotVector kv = KnotVector.newInstance(2, SC_KNOTS);
            return StackContext.outerCopy(BasicNurbsCurve.newInstance(cpList, kv));

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a full-circle {@link NurbsCurve} around the given origin/center with radius
     * <code>r</code>. The NurbsCurve returned has a control polygon which has 9 control
     * points and the shape of a square.
     *
     * @param o Origin or center to create the full-circle around. May not be null.
     * @param r Radius of the full-circle. May not be null.
     * @param n A unit plane normal vector for the plane containing the circle. If
     *          <code>null</code> is passed, a default normal vector that points along the
     *          Z axis is used.
     * @return A BasicNurbsCurve for a full-circle
     * @throws DimensionException if the origin point or normal do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createCircle(GeomPoint o, Parameter<Length> r, GeomVector<Dimensionless> n)
            throws DimensionException {
        requireNonNull(r);
        int numDims = o.getPhyDimension();
        if (nonNull(n) && n.getPhyDimension() > numDims)
            numDims = n.getPhyDimension();
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin or normal", numDims));

        StackContext.enter();
        try {
            if (nonNull(n)) {
                o = o.toDimension(numDims);
                n = n.toDimension(numDims);
            }

            //  Make sure the normal vector exists and is a unit vector.
            if (numDims == 2)
                n = null;
            else if (isNull(n))
                n = GeomUtil.makeZVector(numDims);
            else
                n = n.toUnitVector();

            //  Create the yhat and xhat vectors (orthogonal Y and X in the plane of the circle).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(n);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(n, yhat);

            //  Now just create a circle from the center, radius, and direction vectors.
            BasicNurbsCurve crv = createEllipseCrv(o, r, r, xhat, yhat);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a full-circle {@link NurbsCurve} that passes through the input (not
     * co-linear) points. The curve will be parameterized with 0 at the starting point
     * with the parameterization increasing toward the last point. The NurbsCurve returned
     * has a control polygon which has 9 control points and the shape of square.
     *
     * @param p1 The 1st of the points that define the circle (must not be colinear with
     *           the other two). May not be null.
     * @param p2 The 2nd of the points that define the circle (must not be colinear with
     *           the other two). May not be null.
     * @param p3 The 3rd of the points that define the circle (must not be colinear with
     *           the other two). May not be null.
     * @return A BasicNurbsCurve for a full-circle
     * @throws DimensionException if one of the 3 points does not have at least 2 physical
     * dimensions.
     * @throws IllegalArgumentException if the input points are co-linear.
     */
    public static BasicNurbsCurve createCircle(GeomPoint p1, GeomPoint p2, GeomPoint p3) throws DimensionException {
        requireNonNull(p1);
        requireNonNull(p2);
        requireNonNull(p3);
        
        //  Determine the circle parameters from 3 points.
        CircleInfo circle = GeomUtil.threePointCircle(p1, p2, p3);

        //  Now just create a circle from the center, radius, and direction vectors.
        return createEllipseCrv(circle.center, circle.radius, circle.radius, circle.xhat, circle.yhat);
    }

    /**
     * Create a full-circle {@link NurbsCurve} that is approximately centered on the
     * specified origin point and passes through the two input points. The origin point is
     * used to determine the plane that the circle lies in and is used to approximate the
     * center of the circle. The true origin/center of the circle is calculated to ensure
     * that the circle passes through the supplied edge points. The curve is parameterized
     * such that 0 is at the first point and the parameterization increases toward the 2nd
     * point. The NurbsCurve returned has a control polygon which has 9 control points and
     * the shape of square.
     *
     * @param o  Approximate origin or center to create the full-circle around. May not be
     *           null.
     * @param p1 The 1st of the points that define the circle. May not be null.
     * @param p2 The 2nd of the points that define the circle. May not be null.
     * @return A BasicNurbsCurve for a full-circle
     * @throws DimensionException if the origin or 2 points do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createCircleO(GeomPoint o, GeomPoint p1, GeomPoint p2) throws DimensionException {
        requireNonNull(o);
        requireNonNull(p1);
        requireNonNull(p2);
        
        //  Determine the circle parameters from a center and 2 points.
        CircleInfo circle = GeomUtil.centerTwoPointCircle(o, p1, p2);

        //  Now just create a circle from the center, radius, and direction vectors.
        return createEllipseCrv(circle.center, circle.radius, circle.radius, circle.xhat, circle.yhat);
    }

    /**
     * Create a full-ellipse {@link NurbsCurve} around the given origin/center with
     * semi-major axis length <code>a</code> and semi-minor axis length <code>b</code>.
     * The NurbsCurve returned has a control polygon which has 9 control points and the
     * shape of a rectangle.
     *
     * @param o Origin or center to create the full-ellipse around. May not be null.
     * @param a Semi-major axis length. May not be null.
     * @param b Semi-minor axis length. May not be null.
     * @param n A unit plane normal vector for the plane containing the circle. If
     *          <code>null</code> is passed, a default normal vector that points along the
     *          Z axis is used.
     * @return A BasicNurbsCurve for a full-ellipse
     * @throws DimensionException if the origin point or normal do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createEllipse(GeomPoint o, Parameter<Length> a, Parameter<Length> b,
            GeomVector<Dimensionless> n) throws DimensionException {
        requireNonNull(a);
        requireNonNull(b);
        
        int numDims = o.getPhyDimension();
        if (nonNull(n) && n.getPhyDimension() > numDims)
            numDims = n.getPhyDimension();
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin or normal", numDims));

        StackContext.enter();
        try {
            if (nonNull(n)) {
                o = o.toDimension(numDims);
                n = n.toDimension(numDims);
            }

            //  Make sure the normal vector exists and is a unit vector.
            if (numDims == 2)
                n = null;
            else if (isNull(n))
                n = GeomUtil.makeZVector(numDims);
            else
                n = n.toUnitVector();

            //  Create the yhat and xhat vectors (orthogonal Y and X in the plane of the circle).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(n);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(n, yhat);

            //  Now just create a circle from the center, radius, and direction vectors.
            BasicNurbsCurve crv = createEllipseCrv(o, a, b, xhat, yhat);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a full-ellipse {@link NurbsCurve} around the given origin/center with
     * semi-major axis length <code>a</code> and semi-minor axis length <code>b</code>.
     * The NurbsCurve returned has a control polygon which has 9 control points and the
     * shape of a rectangle.
     *
     * @param o    Origin or center to create the full-circle around. May not be null.
     * @param a    Semi-major axis length. May not be null.
     * @param b    Semi-minor axis length. May not be null.
     * @param xhat A unit vector indicating the "X" direction in the plane of the ellipse.
     *             This must be orthogonal to yhat. May not be null.
     * @param yhat A unit vector indicating the "Y" direction in the plane of the ellipse.
     *             This must be orthogonal to xhat. May not be null.
     * @return A BasicNurbsCurve for a full-ellipse
     */
    private static BasicNurbsCurve createEllipseCrv(GeomPoint o, Parameter<Length> a, Parameter<Length> b,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat)
            throws DimensionException {
        StackContext.enter();
        try {
            double w = SQRT2 / 2.0;
            double aValue = a.getValue(SI.METER);
            double bValue = b.getValue(SI.METER);

            //  Create a list of control points.
            FastTable<ControlPoint> cpList = FastTable.newInstance();
            cpList.setSize(9);

            //  Set 0 (right-start), 4 (left), and 8 (right-end) control points.
            //  p = a,0
            Point p = Point.valueOf(xhat);
            p = p.times(aValue);
            cpList.set(0, ControlPoint.valueOf(o.plus(p), 1));
            cpList.set(4, ControlPoint.valueOf(o.plus(p.opposite()), 1));   //  p = -a,0
            cpList.set(8, ControlPoint.valueOf(o.plus(p), 1));

            //  Set 1 (upper right) and 5 (lower left) control points.
            //  p = a,b
            p = Point.valueOf(xhat.times(aValue).plus(yhat.times(bValue)));
            cpList.set(1, ControlPoint.valueOf(o.plus(p), w));
            cpList.set(5, ControlPoint.valueOf(o.plus(p.opposite()), w));   //  p = -a,-b

            //  Set 2 (top), and 6 (bottom)control points.
            //  p = 0,b
            p = Point.valueOf(yhat);
            p = p.times(bValue);
            cpList.set(2, ControlPoint.valueOf(o.plus(p), 1));
            cpList.set(6, ControlPoint.valueOf(o.plus(p.opposite()), 1));   //  p = 0,-b

            //  Set 3 (upper left) and 7 (lower right) control points.
            //  p = a,-b
            p = Point.valueOf(xhat.times(aValue).plus(yhat.times(-bValue)));
            cpList.set(7, ControlPoint.valueOf(o.plus(p), w));
            cpList.set(3, ControlPoint.valueOf(o.plus(p.opposite()), w));   //  p = -a,b

            //  Create the NURBS curve.
            KnotVector kv = KnotVector.newInstance(2, FC_KNOTS);
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a circular arc {@link NurbsCurve} about the specified origin, with the
     * specified radius and angular start and stop values.
     *
     * @param o      The origin to create the arc around. May not be null.
     * @param r      The radius of the circular arc. May not be null.
     * @param n      A unit plane normal vector for the plane containing the circle. If
     *               <code>null</code> is passed, a default normal vector that points
     *               along the Z axis is used.
     * @param thetaS The start angle of the arc (relative to the X axis).
     * @param thetaE The end angle of the arc (relative to the X axis). If the end angle
     *               is smaller than the start angle, it is increased by increments of
     *               2*PI until it is larger than the start angle.
     * @return A BasicNurbsCurve for a circular arc
     * @throws DimensionException if the origin point or normal do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createCircularArc(GeomPoint o, Parameter<Length> r, GeomVector<Dimensionless> n,
            Parameter<Angle> thetaS, Parameter<Angle> thetaE) throws DimensionException {
        requireNonNull(r);
        int numDims = o.getPhyDimension();
        if (nonNull(n) && n.getPhyDimension() > numDims)
            numDims = n.getPhyDimension();
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin or normal", numDims));

        StackContext.enter();
        try {
            if (nonNull(n)) {
                o = o.toDimension(numDims);
                n = n.toDimension(numDims);
            }

            //  Make sure the normal vector exists and is a unit vector.
            if (numDims == 2)
                n = null;
            else if (isNull(n))
                n = GeomUtil.makeZVector(numDims);
            else
                n = n.toUnitVector();

            //  Create the yhat and xhat vectors (orthogonal Y and X in the plane of the circle).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(n);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(n, yhat);

            BasicNurbsCurve crv = createEllipticalArcCrv(o, r, r, xhat, yhat,
                    thetaS.getValue(SI.RADIAN), thetaE.getValue(SI.RADIAN));
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a circular arc {@link NurbsCurve} that passes through the input (not
     * co-linear) points. The curve will be parameterized with 0 at the starting point and
     * 1 at the last point.
     *
     * @param p1 The 1st of the points that define the arc (must not be colinear with the
     *           other two). May not be null.
     * @param p2 The 2nd of the points that define the arc (must not be colinear with the
     *           other two). May not be null.
     * @param p3 The 3rd of the points that define the arc (must not be colinear with the
     *           other two). May not be null.
     * @return A BasicNurbsCurve for a circular arc through the input points
     * @throws DimensionException if one of the 3 points does not have at least 2 physical
     * dimensions.
     * @throws IllegalArgumentException if the input points are co-linear.
     */
    public static BasicNurbsCurve createCircularArc(GeomPoint p1, GeomPoint p2, GeomPoint p3) throws DimensionException {
        requireNonNull(p1);
        requireNonNull(p2);
        requireNonNull(p3);
        
        //  Determine the circle parameters from 3 points.
        CircleInfo circle = GeomUtil.threePointCircle(p1, p2, p3);

        //  Now just create a circle from the center, radius, and direction vectors.
        return createEllipticalArcCrv(circle.center, circle.radius, circle.radius, circle.xhat, circle.yhat,
                0, circle.angle.getValue(SI.RADIAN));
    }

    /**
     * Create a circular arc {@link NurbsCurve} that passes through the input points and
     * that has the input tangents at the 1st point. The curve will be parameterized with
     * 0 at the starting point and 1 at the last point.
     *
     * @param p1 The 1st of the points that define the arc. May not be null.
     * @param t1 The tangent vector at p1 (must not point at p2). May not be null.
     * @param p2 The 2nd of the points that define the arc. May not be null.
     * @return A BasicNurbsCurve for a circular arc through the input points with the
     *         input tangent vector at the start.
     * @throws DimensionException if one of the 3 points does not have at least 2 physical
     * dimensions.
     */
    public static BasicNurbsCurve createCircularArc(GeomPoint p1, GeomVector t1, GeomPoint p2)
            throws DimensionException {
        requireNonNull(p1);
        requireNonNull(t1);
        requireNonNull(p2);
        
        //  Determine the circle parameters from 3 points.
        CircleInfo circle = GeomUtil.twoPointTangentCircle(p1, t1, p2);

        //  Now just create a circle from the center, radius, and direction vectors.
        return createEllipticalArcCrv(circle.center, circle.radius, circle.radius, circle.xhat, circle.yhat,
                0, circle.angle.getValue(SI.RADIAN));
    }

    /**
     * Create a circular arc {@link NurbsCurve} that is approximately centered on the
     * specified origin point and passes through the two input points. The origin point is
     * used to determine the plane that the circle lies in and is used to approximate the
     * center of the circle. The true origin/center of the circle is calculated to ensure
     * that the circle passes through the supplied edge points. The curve is parameterized
     * such that 0 is at the first point and 1 is at the last point. The NurbsCurve
     * returned has a control polygon which has 9 control points and the shape of square.
     *
     * @param o  Approximate origin or center to create the arc around. May not be null.
     * @param p1 The 1st of the points that define the arc. May not be null.
     * @param p2 The 2nd of the points that define the arc. May not be null.
     * @return A BasicNurbsCurve for a circular arc through the input points.
     * @throws DimensionException if the origin or 2 points do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createCircularArcO(GeomPoint o, GeomPoint p1, GeomPoint p2) throws DimensionException {
        requireNonNull(o);
        requireNonNull(p1);
        requireNonNull(p2);
        
        //  Determine the circle parameters from a center and 2 points.
        CircleInfo circle = GeomUtil.centerTwoPointCircle(o, p1, p2);

        //  Now just create a circle from the center, radius, and direction vectors.
        return createEllipticalArcCrv(circle.center, circle.radius, circle.radius, circle.xhat, circle.yhat,
                0, circle.angle.getValue(SI.RADIAN));
    }

    /**
     * Create an elliptical arc {@link NurbsCurve} about the specified origin, with the
     * specified semi-major axis length, semi-minor axis length and angular start and stop
     * values.
     *
     * @param o      The origin to create the arc around. May not be null.
     * @param a      The ellipse semi-major axis length (half the ellipse diameter in the
     *               xhat direction). May not be null.
     * @param b      The ellipse semi-minor axis length (half the ellipse diameter in the
     *               yhat direction). May not be null.
     * @param xhat   A unit vector indicating the "X" or semi-major axis direction in the
     *               plane of the ellipse. This must be orthogonal to yhat. May not be
     *               null.
     * @param yhat   A unit vector indicating the "Y" or semi-minor direction in the plane
     *               of the ellipse. This must be orthogonal to xhat. May not be null.
     * @param thetaS The start angle of the arc (relative to the xhat axis). May not be
     *               null.
     * @param thetaE The end angle of the arc (relative to the xhat axis). If the end
     *               angle is smaller than the start angle, it is increased by increments
     *               of 2*PI until it is larger than the start angle. May not be null.
     * @return A BasicNurbsCurve for an elliptical arc
     * @throws DimensionException if the origin point or axes do not have at least 2
     * physical dimensions.
     */
    public static BasicNurbsCurve createEllipticalArc(
            GeomPoint o, Parameter<Length> a, Parameter<Length> b,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat,
            Parameter<Angle> thetaS, Parameter<Angle> thetaE) throws DimensionException {
        int numDims = GeomUtil.maxPhyDimension(o, xhat, yhat);
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin and axis vector", numDims));
        requireNonNull(a);
        requireNonNull(b);
        requireNonNull(thetaS);
        requireNonNull(thetaE);
        
        StackContext.enter();
        try {
            o = o.toDimension(numDims);
            xhat = xhat.toDimension(numDims);
            yhat = yhat.toDimension(numDims);

            //  Make sure the axis vectors are unit vectors with no origin offset.
            xhat = xhat.toUnitVector();
            xhat.setOrigin(Point.newInstance(numDims));
            yhat = yhat.toUnitVector();
            yhat.setOrigin(Point.newInstance(numDims));

            BasicNurbsCurve crv = createEllipticalArcCrv(o, a, b, xhat, yhat, thetaS.getValue(SI.RADIAN), thetaE.getValue(SI.RADIAN));
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a canonical elliptical arc {@link NurbsCurve} about the specified origin,
     * with the specified semi-major and semi-minor axes and angular start and stop
     * values.
     *
     * @param o    The origin to create the arc around. May not be null.
     * @param a    The ellipse semi-major (or minor if b is major) axis. May not be null.
     * @param b    The ellipse semi-minor (or major if a is minor) axis. May not be null.
     * @param xhat A unit vector indicating the "X" direction in the plane of the ellipse.
     *             This must be orthogonal to yhat. May not be null.
     * @param yhat A unit vector indicating the "Y" direction in the plane of the ellipse.
     *             This must be orthogonal to xhat. May not be null.
     * @param ths  The start angle of the arc (relative to the X axis) in radians.
     * @param the  The end angle of the arc (relative to the X axis) in radians. If the
     *             end angle is smaller than the start angle, it is increased by
     *             increments of 2*PI until it is larger than the start angle.
     * @return A BasicNurbsCurve for an elliptical arc
     */
    private static BasicNurbsCurve createEllipticalArcCrv(GeomPoint o, Parameter<Length> a, Parameter<Length> b,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat,
            double ths, double the) throws DimensionException {

        //  Reference: Piegl, L., Tiller, W., The Nurbs Book, 2nd Edition, Springer-Verlag, Berlin, 1997.
        //  Algorithm: 7.1, pg. 308.
        StackContext.enter();
        try {
            //  Get ths and the in the range 0 - 2*PI.
            ths = GeomUtil.bound2Pi(ths);
            the = GeomUtil.bound2Pi(the);

            //  Make sure the end angle is > than the start angle.
            if (the <= ths)
                the += TWO_PI;
            double theta = the - ths;

            //  How many arc segments are required?
            int narcs = 4;
            if (theta <= HALF_PI)
                narcs = 1;
            else if (theta <= PI)
                narcs = 2;
            else if (theta <= PI + HALF_PI)
                narcs = 3;

            double dtheta = theta / narcs;
            int num = 2 * narcs;                            //  num+1 control points.
            double w1 = cos(dtheta / 2);                    //  dtheta/2 is the base angle
            double aValue = a.getValue(SI.METER);
            double bValue = b.getValue(SI.METER);
            Unit<Length> unit = o.getUnit();

            //  Create a list of control points.
            FastTable<ControlPoint> cpList = FastTable.newInstance();
            cpList.setSize(num + 1);

            //  Create the starting control point:  p0 = a*cos(ths), b*sin(ths)
            double sinths = sin(ths), cosths = cos(ths);
            MutablePoint p0 = MutablePoint.valueOf(xhat.times(aValue * cosths).plus(yhat.times(bValue * sinths))).to(unit);
            cpList.set(0, ControlPoint.valueOf(o.plus(p0), 1));

            //  Create starting perpendicular vector:   t0 = -a*sin(ths), b*cos(ths)
            Vector<Dimensionless> t0 = xhat.times(-aValue * sinths).plus(yhat.times(bValue * cosths)).toUnitVector();

            int index = 0;
            double angle = ths;
            MutablePoint p1 = MutablePoint.newInstance(o.getPhyDimension(), unit);
            MutablePoint p2 = MutablePoint.newInstance(o.getPhyDimension(), unit);
            for (int i = 1; i <= narcs; i++) {
                angle += dtheta;
                double sina = sin(angle);
                double cosa = cos(angle);

                //  Create segment end control point:   p2 = a*cos(angle), b*sin(angle)
                p2.set(xhat.times(aValue * cosa).plus(yhat.times(bValue * sina)));
                cpList.set(index + 2, ControlPoint.valueOf(o.plus(p2), 1));

                //  Create end perpendicular vector:    t2 = -a*sin(angle), b*cos(angle)
                Vector<Dimensionless> t2 = xhat.times(-aValue * sina).plus(yhat.times(bValue * cosa)).toUnitVector();

                //  Find the intersection of the two perpendiculars.
                GeomUtil.lineLineIntersect(p0, t0, p2, t2, GTOL, null, p1, null);

                //  Create the intermediate control point.
                cpList.set(index + 1, ControlPoint.valueOf(o.plus(p1), w1));

                index += 2;
                if (i < narcs) {
                    p0.set(p2);
                    t0 = t2;
                }
            }

            //  Create the knot vector.
            FastTable<Float64> uKnot = FastTable.newInstance();
            int j = num + 1;
            uKnot.setSize(j + 3);
            for (int i = 0; i < 3; i++) {
                uKnot.set(i, Float64.ZERO);
                uKnot.set(i + j, Float64.ONE);
            }
            switch (narcs) {
                case 1:
                    break;
                case 2:
                    uKnot.set(3, Float64.valueOf(0.5));
                    uKnot.set(4, Float64.valueOf(0.5));
                    break;
                case 3:
                    uKnot.set(3, Float64.valueOf(1. / 3.));
                    uKnot.set(4, Float64.valueOf(1. / 3.));
                    uKnot.set(5, Float64.valueOf(2. / 3.));
                    uKnot.set(6, Float64.valueOf(2. / 3.));
                    break;
                case 4:
                    uKnot.set(3, Float64.valueOf(0.25));
                    uKnot.set(4, Float64.valueOf(0.25));
                    uKnot.set(5, Float64.valueOf(0.5));
                    uKnot.set(6, Float64.valueOf(0.5));
                    uKnot.set(7, Float64.valueOf(0.75));
                    uKnot.set(8, Float64.valueOf(0.75));
                    break;
            }

            //  Create the NURBS curve.
            KnotVector kv = KnotVector.newInstance(2, Float64Vector.valueOf(uKnot));
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a planar parabolic arc {@link NurbsCurve} that joins two end points with the
     * specified tangent vectors at each end point. If the input tangent vectors are
     * either parallel or not coplanar, then a straight line between the two end points is
     * returned. If the intersection of the two input tangent vectors does not fall
     * between the two input end points, then a straight line between the two end points
     * is returned.
     *
     * @param p0 The starting end of the arc. May not be null.
     * @param t0 The tangent vector at the starting end of the arc. May not be null.
     * @param p2 The stopping end of the arc. May not be null.
     * @param t2 The tangent vector at the stopping end of the arc. May not be null.
     * @return A BasicNurbsCurve for a parabolic arc between the input points.
     * @throws DimensionException if the input points and tangents do not have at least 2
     * physical dimensions.
     * @see #createBlend
     */
    public static BasicNurbsCurve createParabolicArc(GeomPoint p0, GeomVector<Dimensionless> t0,
            GeomPoint p2, GeomVector<Dimensionless> t2) throws DimensionException {
        requireNonNull(p0);
        requireNonNull(t0);
        requireNonNull(p2);
        requireNonNull(t2);
        
        //  Get everything into a consistant set of dimensions.
        int numDims = GeomUtil.maxPhyDimension(p0, t0, p2, t2);
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input points and tangent vectors", numDims));

        StackContext.enter();
        try {
            Unit<Length> unit = p0.getUnit();
            Point nullPnt = Point.newInstance(numDims);
            p0 = p0.toDimension(numDims);
            t0 = t0.toDimension(numDims).toUnitVector();
            t0.setOrigin(nullPnt);
            p2 = p2.toDimension(numDims).to(unit);
            t2 = t2.toDimension(numDims).toUnitVector();
            t2.setOrigin(nullPnt);

            //  Find the intersection of the two tangent vector lines.
            MutablePoint p1 = MutablePoint.newInstance(numDims, unit);
            MutablePoint s1 = MutablePoint.newInstance(2, unit);
            IntersectType type = GeomUtil.lineLineIntersect(p0, t0, p2, t2, GTOL, s1, p1, null);
            if (type != IntersectType.INTERSECT) {
                //  The input tangent vectors are not co-planar or they are parellel.
                return StackContext.outerCopy(CurveFactory.createLine(2, p0, p2));
            }

            //  Make sure the interesection falls between the two input points.
            //  Parametric positions returned from lineLineIntersect() are scaled such that
            //  0.0 is the line origin point and any other value represents the signed distance
            //  that the intersection point is away from the origin along the direction vector.
            double s0 = s1.getValue(0);     //  Distance intersection is away from 1st line origin.
            double s2 = s1.getValue(1);     //  Distance intersection is away from 2nd line origin.
            if (s0 < 0 || s2 > 0) {
                //  The intersection does not fall between the two input points.
                return StackContext.outerCopy(CurveFactory.createLine(2, p0, p2));
            }

            //  Create the control points.
            FastTable<ControlPoint> cps = FastTable.newInstance();
            cps.add(ControlPoint.valueOf(p0.immutable(), 1));
            cps.add(ControlPoint.valueOf(p1.immutable(), 1));
            cps.add(ControlPoint.valueOf(p2.immutable(), 1));

            // Create the knot vector.
            KnotVector kv = KnotVector.newInstance(2, 0., 0., 0., 1., 1., 1.);

            //  Create the curve.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create the first quadrant of a common exponent super-ellipse {@link NurbsCurve}
     * about the specified origin, with the specified semi-major and semi-minor axes and
     * exponent. Other quadrants can be represented by reflection. A common exponent
     * super-elliptic arc can be described using the following point:<br>
     * <code>(x/a)^n + (y/b)^n = 1</code><br>
     * where a is the semi-major and b is the semi-minor axis, and n is the exponent of
     * the super-ellipse (n > 0). Special cases include a circle (with a = b, and n = 2),
     * an ellipse (with a ≠ b, and n = 2) and a straight line (n = 1). If n is large, a
     * box is approximated. This type of super-ellipse is represented exactly by a NURBS
     * curve.
     *
     * @param o    The origin to create the arc around. May not be null.
     * @param a    The super-ellipse semi-major (or minor if b is major) axis. May not be
     *             null.
     * @param b    The super-ellipse semi-minor (or major if a is minor) axis. May not be
     *             null.
     * @param n    The exponent for the super-ellipse (must be > 0).
     * @param nhat A unit plane normal vector for the plane containing the super-ellipse.
     *             If <code>null</code> is passed, a default normal vector that points
     *             along the Z axis is used.
     * @return A BasicNurbsCurve for the 1st quadrant of a super-ellipse.
     */
    public static BasicNurbsCurve createSuperEllipticalArc(
            GeomPoint o, Parameter<Length> a, Parameter<Length> b, double n,
            GeomVector<Dimensionless> nhat) throws DimensionException {
        requireNonNull(a);
        requireNonNull(b);
        
        int numDims = o.getPhyDimension();
        if (nonNull(nhat) && nhat.getPhyDimension() > numDims)
            numDims = nhat.getPhyDimension();
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin or normal", numDims));

        StackContext.enter();
        try {
            if (nonNull(nhat)) {
                o = o.toDimension(numDims);
                nhat = nhat.toDimension(numDims);
            }

            //  Make sure the normal vector exists and is a unit vector.
            if (numDims == 2)
                nhat = null;
            else if (isNull(nhat))
                nhat = GeomUtil.makeZVector(numDims);
            else
                nhat = nhat.toUnitVector();

            //  Create the yhat and xhat vectors (orthogonal Y and X in the plane of the circle).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(nhat);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(nhat, yhat);

            BasicNurbsCurve crv = createSuperEllipticalArc(o, a, b, n, xhat, yhat);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create the first quadrant of a common exponent super-ellipse {@link NurbsCurve}
     * about the specified origin, with the specified semi-major and semi-minor axes and
     * exponent. Other quadrants can be represented by reflection. A super-elliptic arc
     * can be described using the following point:<br>
     * <code>(x/a)^n + (y/b)^n = 1</code><br>
     * where a is the semi-major and b is the semi-minor axis, and n is the exponent of
     * the super-ellipse (n > 0). Special cases include a circle (with a = b, and n = 2),
     * an ellipse (with a ≠ b, and n = 2) and a straight line (n = 1). If n is large, a
     * box is approximated. This type of super-ellipse is represented exactly by a NURBS
     * curve.
     *
     * @param o    The origin to create the arc around. May not be null.
     * @param a    The super-ellipse semi-major (or minor if b is major) axis. May not be
     *             null.
     * @param b    The super-ellipse semi-minor (or major if a is minor) axis. May not be
     *             null.
     * @param n    The exponent for the super-ellipse (must be > 0).
     * @param xhat A unit vector indicating the "X" or semi-major axis direction in the
     *             plane of the super-ellipse. This must be orthogonal to yhat. May not be
     *             null.
     * @param yhat A unit vector indicating the "Y" or semi-minor axis direction in the
     *             plane of the super-ellipse. This must be orthogonal to xhat. May not be
     *             null.
     * @return A BasicNurbsCurve for the 1st quadrant of a super-ellipse.
     */
    public static BasicNurbsCurve createSuperEllipticalArc(
            GeomPoint o, Parameter<Length> a, Parameter<Length> b, double n,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat) throws DimensionException {
        requireNonNull(o);
        requireNonNull(a);
        requireNonNull(b);
        requireNonNull(xhat);
        requireNonNull(yhat);
        
        //  Reference:  Handbook of Grid Generation, Chapter 30, Section 30.3.5
        //              and Joe Huwaldt's hand written derivations.
        if (n <= MathTools.EPS) {
            throw new IllegalArgumentException(MessageFormat.format(RESOURCES.getString("seExponent"), "n"));
        }

        int numDims = GeomUtil.maxPhyDimension(o, xhat, yhat);
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin and axis vector", numDims));

        StackContext.enter();
        try {
            o = o.toDimension(numDims);
            xhat = xhat.toDimension(numDims);
            yhat = yhat.toDimension(numDims);

            //  Make sure the axis vectors are unit vectors with no origin offset.
            xhat = xhat.toUnitVector();
            yhat = yhat.toUnitVector();

            //  Compute the weight for the middle control point.
            double aValue = a.getValue(SI.METER);
            double bValue = b.getValue(SI.METER);
            double rhoD = sqrt(aValue * aValue + bValue * bValue);
            double rhoM = 0.5 * rhoD;
            double rhoh = rhoD / pow(2, 1.0 / n);
            double w1 = (rhoh - rhoM) / (rhoD - rhoh);

            //  Create a list of control points.
            FastTable<ControlPoint> cpList = FastTable.newInstance();

            //  Create the starting control point:  p0 = a, 0
            Point p0 = Point.valueOf(xhat.times(aValue));
            cpList.add(ControlPoint.valueOf(o.plus(p0), 1));

            if (w1 >= 0) {
                //  Create the middle control point:  p1 = a, b
                Point p1 = Point.valueOf(xhat.times(aValue).plus(yhat.times(bValue)));
                cpList.add(ControlPoint.valueOf(o.plus(p1), w1));

            } else {
                //  Use the origin as the control point:    p1 = o
                //  And change the rhoh definition to use inverted exponents.
                rhoh = rhoD / pow(2, n);
                w1 = (rhoh - rhoM) / (rhoD - rhoh);
                cpList.add(ControlPoint.valueOf(o.immutable(), w1));
            }

            //  Create ending control point:  p2 = 0, b
            Point p2 = Point.valueOf(yhat.times(bValue));
            cpList.add(ControlPoint.valueOf(o.plus(p2), 1));

            //  Create the knot vector.
            KnotVector kv = KnotVector.newInstance(2, 0., 0., 0., 1., 1., 1.);

            //  Create the NURBS curve.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, kv).to(o.getUnit());
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create the first quadrant of a general super-ellipse {@link NurbsCurve} about the
     * specified origin, with the specified semi-major and semi-minor axes and exponents.
     * Other quadrants can be represented by reflection. A general super-elliptic arc can
     * be described using the following point:<br>
     * <code>(x/a)^m + (y/b)^n = 1</code><br>
     * where a is the semi-major and b is the semi-minor axis, and m & n are the exponents
     * of the super-ellipse (m & n > 0). Special cases include a circle (with a = b, and m
     * = n = 2), an ellipse (with a ≠ b, and m = n = 2) and a straight line (m = n = 1).
     * If m & n are large, a box is approximated. This type of super-ellipse is generally
     * only approximated by a NURBS curve to the specified tolerance. If the input
     * exponents are equal, then an exact representation is returned.
     *
     * @param o    The origin to create the arc around. May not be null.
     * @param a    The super-ellipse semi-major (or minor if b is major) axis. May not be
     *             null.
     * @param b    The super-ellipse semi-minor (or major if a is minor) axis. May not be
     *             null.
     * @param m    The exponent on the x/a term of the super-ellipse point (must be >
     *             0).
     * @param n    The exponent for the y/b term of the super-ellipse point (must be >
     *             0).
     * @param xhat A unit vector indicating the "X" or semi-major axis direction in the
     *             plane of the super-ellipse. This must be orthogonal to yhat. May not be
     *             null.
     * @param yhat A unit vector indicating the "Y" or semi-minor axis direction in the
     *             plane of the super-ellipse. This must be orthogonal to xhat. May not be
     *             null.
     * @param tol  A geometric tolerance on how accurately the NURBS curve must represent
     *             the true super-ellipse. If m == n, then an exact representation is
     *             returned and "tol" is ignored, otherwise tol may not be null.
     * @return A BasicNurbsCurve for the 1st quadrant of a super-ellipse.
     */
    public static BasicNurbsCurve createSuperEllipticalArc(
            GeomPoint o, Parameter<Length> a, Parameter<Length> b, double m, double n,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat, Parameter<Length> tol) 
            throws DimensionException, IllegalArgumentException, SingularMatrixException, ParameterException {
        requireNonNull(o);
        requireNonNull(a);
        requireNonNull(b);
        requireNonNull(xhat);
        requireNonNull(yhat);
        
        //  Reference:  Handbook of Grid Generation, Chapter 30, Section 30.3.5
        //              and Joe Huwaldt's hand written derivations.
        if (m <= MathTools.EPS) {
            throw new IllegalArgumentException(MessageFormat.format(RESOURCES.getString("seExponent"), "m"));
        }
        if (n <= MathTools.EPS) {
            throw new IllegalArgumentException(MessageFormat.format(RESOURCES.getString("seExponent"), "n"));
        }

        if (MathTools.isApproxEqual(m, n)) {
            //  A common exponent super-ellipse is more accurate.
            return createSuperEllipticalArc(o, a, b, n, xhat, yhat);
        }

        int numDims = GeomUtil.maxPhyDimension(o, xhat, yhat);
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin and axis vector", numDims));

        requireNonNull(tol);
        StackContext.enter();
        try {
            o = o.toDimension(numDims);
            xhat = xhat.toDimension(numDims);
            yhat = yhat.toDimension(numDims);

            //  Make sure the axis vectors are unit vectors with no origin offset.
            xhat = xhat.toUnitVector();
            yhat = yhat.toUnitVector();

            //  Compute the weight for the middle control point.
            double aValue = a.getValue(SI.METER);
            double bValue = b.getValue(SI.METER);

            //  Create an initial, high resolution curve.
            int size = 51;
            double theta = 0.;
            double dTheta = 90. / (size - 1);
            PointString pnts = PointString.newInstance();
            GeomList tangents = GeomList.newInstance();
            for (int i = 0; i < size; ++i) {
                //  Get the parametric position on the super-ellipse.
                double thetaR = toRadians(theta);
                double ctheta = cos(thetaR);
                double stheta = sin(thetaR);

                //  Compute the coordinates of a point on the super-ellipse.
                double xh = aValue * pow(abs(ctheta), 2. / m);
                double yh = bValue * pow(abs(stheta), 2. / n);
                Point p = o.plus(Point.valueOf(xhat.times(xh).plus(yhat.times(yh))));
                pnts.add(p);

                //  Compute the local slope of the super-ellipse.
                //  dz/dy = -b/x*m/n*(x/a)^m*(1 - (x/a)^m))^(1/n-1)
                //      Unfortunately, this point breaks down at theta=0 (xh = aValue).
                double xh_am = pow(xh / aValue, m);
                double dz_dy = -bValue / xh * m / n * xh_am * pow(1 - xh_am, 1. / n - 1.);
                Vector t;
                if (Double.isInfinite(dz_dy)) {
                    t = Vector.valueOf(yhat.times(-1 * Math.signum(dz_dy))).toUnitVector();

                } else if (abs(dz_dy) < 1e-15) {
                    if (i == 0) {
                        //  Numerically approximate it.
                        double dthetaR = 0.017453292519943 / 2;     //  1/2 degreeU
                        double xi = aValue * pow(abs(cos(dthetaR)), 2. / m);
                        double yi = bValue * pow(abs(sin(dthetaR)), 2. / n);
                        t = Vector.valueOf(xhat.times(xi - aValue).plus(yhat.times(yi))).toUnitVector();

                    } else
                        t = Vector.valueOf(xhat.times(-1)).toUnitVector();

                } else {
                    t = Vector.valueOf(xhat.times(-1).plus(yhat.times(-dz_dy))).toUnitVector();
                }
                //t.setOrigin(p);
                tangents.add(t);

                theta = theta + dTheta;
            }

            //  Create the NURBS curve.
            //  Fit a curve to the calculated points using the calculated slopes.
            BasicNurbsCurve crv = fitPoints(2, pnts, tangents, true, 1.0).to(o.getUnit());

            //  Thin down the high resolution curve to the required tolerance.
            crv = CurveUtils.thinKnotsToTolerance(crv, tol);

            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create the first quadrant of a general super-ellipse {@link NurbsCurve} about the
     * specified origin, with the specified semi-major and semi-minor axes and exponents.
     * Other quadrants can be represented by reflection. A general super-elliptic arc can
     * be described using the following point:<br>
     * <code>(x/a)^m + (y/b)^n = 1</code><br>
     * where a is the semi-major and b is the semi-minor axis, and m & n are the exponents
     * of the super-ellipse. Special cases include a circle (with a = b, and m = n = 2),
     * an ellipse (with a ≠ b, and m = n = 2) and a straight line (m = n = 1). If m & n
     * are large, a box is approximated. This type of super-ellipse is generally only
     * approximated by a NURBS curve to the specified tolerance. If the input exponents
     * are equal, then an exact representation is returned.
     *
     * @param o    The origin to create the arc around. May not be null.
     * @param a    The super-ellipse semi-major (or minor if b is major) axis. May not be null.
     * @param b    The super-ellipse semi-minor (or major if a is minor) axis. May not be null.
     * @param m    The exponent on the x/a term of the super-ellipse point (must be >
     *             0).
     * @param n    The exponent for the y/b term of the super-ellipse point (must be >
     *             0).
     * @param nhat A unit plane normal vector for the plane containing the super-ellipse.
     *             If <code>null</code> is passed, a default normal vector that points
     *             along the Z axis is used.
     * @param tol  A geometric tolerance on how accurately the NURBS curve must represent
     *             the true super-ellipse. If m == n, then an exact representation is
     *             returned and "tol" is ignored, otherwise "tol" may not be null..
     * @return A BasicNurbsCurve for the 1st quadrant of a super-ellipse.
     */
    public static BasicNurbsCurve createSuperEllipticalArc(
            GeomPoint o, Parameter<Length> a, Parameter<Length> b, double m, double n,
            GeomVector<Dimensionless> nhat, Parameter<Length> tol) 
            throws DimensionException, IllegalArgumentException, SingularMatrixException, ParameterException {
        
        int numDims = o.getPhyDimension();
        if (nonNull(nhat) && nhat.getPhyDimension() > numDims)
            numDims = nhat.getPhyDimension();
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input origin or normal", numDims));

        StackContext.enter();
        try {
            if (nonNull(nhat)) {
                o = o.toDimension(numDims);
                nhat = nhat.toDimension(numDims);
            }

            //  Make sure the normal vector exists and is a unit vector.
            if (numDims == 2)
                nhat = null;
            else if (isNull(nhat))
                nhat = GeomUtil.makeZVector(numDims);
            else
                nhat = nhat.toUnitVector();

            //  Create the yhat and xhat vectors (orthogonal Y and X in the plane of the circle).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(nhat);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(nhat, yhat);

            BasicNurbsCurve crv = createSuperEllipticalArc(o, a, b, m, n, xhat, yhat, tol);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a potentially non-planar cubic Bezier {@link NurbsCurve} that joins or
     * blends two end points with the specified tangent vectors at each end point. If unit
     * vectors are supplied, then they will be scaled by the distance between the end
     * points and the multiplicative factor provided (tanlen).
     *
     * @param p1       The starting end of the arc. May not be null.
     * @param t1       The tangent vector at the starting end of the arc. May not be null.
     * @param p2       The stopping end of the arc. May not be null.
     * @param t2       The tangent vector at the stopping end of the arc. May not be null.
     * @param unitFlag A flag indicating that the tangents are all unit vectors if
     *                 <code>true</code>.
     * @param tanlen   A multiplicative factor for the tangent (must be greater than 0 or
     *                 the function aborts). Used only if <code>unitFlat == true</code>.
     *                 Applied to the distance between end points to scale the tangent
     *                 vectors.
     * @return A BasicNurbsCurve that represents a Bezier curve between the input points
     *         with the input tangent vectors.
     * @throws DimensionException if the input points and tangents do not have at least 2
     * physical dimensions.
     * @see #createParabolicArc
     */
    public static BasicNurbsCurve createBlend(GeomPoint p1, GeomVector t1,
            GeomPoint p2, GeomVector t2, boolean unitFlag, double tanlen) throws DimensionException {
        requireNonNull(p1);
        requireNonNull(p2);
        requireNonNull(t1);
        requireNonNull(t2);
        
        int degree = 3;
        if (unitFlag && tanlen <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("posMultFactorErr"), tanlen));

        int numDims = GeomUtil.maxPhyDimension(p1, t1, p2, t2);
        if (numDims < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input points and tangent vectors", numDims));

        StackContext.enter();
        try {
            //  Determine the distance between the two points.
            Parameter<Length> dist = p1.distance(p2);
            if (dist.getValue() < EPSC)
                return StackContext.outerCopy(createPoint(degree, p1));

            //  Get everything into a consistant set of dimensions.
            p1 = p1.toDimension(numDims);
            p2 = p2.toDimension(numDims);
            t1 = t1.toDimension(numDims);
            t2 = t2.toDimension(numDims);
            if (unitFlag) {
                dist = dist.times(tanlen);
                t1 = t1.toUnitVector().times(dist);
                t2 = t2.toUnitVector().times(dist);
            }

            t1 = t1.opposite();

            //  Find intermediate control points.
            Point p3 = p1.minus(Point.valueOf(t1));
            Point p4 = p2.minus(Point.valueOf(t2));

            //  Create the control points vector.
            int numCP = degree + 1;
            FastTable<ControlPoint> cps = FastTable.newInstance();

            cps.add(ControlPoint.valueOf(p1.immutable(), 1));
            cps.add(ControlPoint.valueOf(p3, 1));
            cps.add(ControlPoint.valueOf(p4, 1));
            cps.add(ControlPoint.valueOf(p2.immutable(), 1));

            // Create a Bezier curve knot vector of the require degreeU.
            FastTable<Float64> U = FastTable.newInstance();
            int numKnots = numCP + degree + 1;
            int numKnotso2 = numKnots / 2;
            for (int i = 0; i < numKnotso2; ++i) {
                U.add(Float64.ZERO);
            }
            for (int i = numKnotso2; i < numKnots; ++i) {
                U.add(Float64.ONE);
            }
            KnotVector kv = KnotVector.newInstance(degree, Float64Vector.valueOf(U));

            //  Create the curve.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a {@link NurbsCurve} of the specified degreeU that is fit to, interpolates,
     * or passes through the specified list of geometry points in the input order. This is
     * a global interpolation of the points, meaning that if one of the input points is
     * moved, it will affect the entire curve, not just the local portion near the point.
     *
     * @param degree The degreeU of the NURBS curve to create (must be > 0 and &le; the
     *               number of data points).
     * @param points The string of GeomPoint objects to pass the curve through. May not be
     *               null.
     * @return A NurbsCurve fit to the input list of points.
     * @throws IllegalArgumentException if the degreeU is &le; 0 or &ge; the number of
     * points input.
     * @throws SingularMatrixException if the decomposed matrix is singular.
     * @see #approxPoints
     */
    public static BasicNurbsCurve fitPoints(int degree, PointString<? extends GeomPoint> points)
            throws IllegalArgumentException, SingularMatrixException, ParameterException {

        StackContext.enter();
        try {
            if (degree <= 0)
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("posDegreeErr"), degree));

            int numPoints = points.size();
            if (degree >= numPoints)
                throw new IllegalArgumentException(RESOURCES.getString("numPointsLTDegree") + " np = "
                        + numPoints + ", d = " + degree + ".");

            if (points.length().getValue() < EPSC) {
                //  We have a degenerate curve.
                BasicNurbsCurve curve = createPoint(degree, points.size(), points.get(0));
                return StackContext.outerCopy(curve);
            }

            //  Turn the points into an array of parameter values along the curve.
            double[] uk = parameterizeString(points);
            parameterizationCheck(numPoints, uk);

            //  Generate the knot vector for interpolation.
            KnotVector uKnots = buildInterpKnotVector(uk, numPoints, degree);

            //  Now fit a curve to the points using the just calculated parameterization and knot vector.
            BasicNurbsCurve curve = fitPoints(degree, points, uk, uKnots);
            return StackContext.outerCopy(curve);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a {@link NurbsCurve} of the specified degreeU that is fit to, interpolates,
     * or passes through the specified list of geometry points in the input order. This is
     * a global interpolation of the points, meaning that if one of the input points is
     * moved, it will affect the entire curve, not just the local portion near the point.
     *
     * @param degree The degreeU of the NURBS curve to create (must be > 0 and &le; the
     *               number of data points).
     * @param points The string of GeomPoint objects to pass the curve through. May not be
     *               null.
     * @param uk     The parameterization array to use for each of the input points. Must
     *               be monotonically increasing and must include 0 and 1. May not be
     *               null.
     * @param uKnots The knot vector to use with the output curve. May not be null.
     * @return A NurbsCurve fit to the input list of points.
     * @throws IllegalArgumentException if the degreeU is &le; 0 or &ge; the number of
     * points input or the parameterization array is invalid.
     * @throws SingularMatrixException if the decomposed matrix is singular.
     * @see #approxPoints(int, int, geomss.geom.PointString, double[],
     * geomss.geom.nurbs.KnotVector)
     * @see #parameterizeString(geomss.geom.PointString)
     * @see #buildInterpKnotVector(double[], int, int)
     */
    public static BasicNurbsCurve fitPoints(int degree, PointString<? extends GeomPoint> points,
            double[] uk, KnotVector uKnots) throws IllegalArgumentException, SingularMatrixException {

        //  This follows the basic process outlined here:
        //      http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/INT-APP/CURVE-INT-global.html
        //  Basically, you form a matrix of basis function coefficients (N).  The data
        //  points are in matrix (D) and you solve for the control point locations (P).
        //      D = N * P   -->  P = N^-1 * D
        
        requireNonNull(uKnots);
        if (degree <= 0) {
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("posDegreeErr"), degree));
        }

        int numPoints = points.size();
        if (degree >= numPoints) {
            throw new IllegalArgumentException(RESOURCES.getString("numPointsLTDegree") + " np = "
                    + numPoints + ", d = " + degree + ".");
        }
        if (uk.length < numPoints)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("numItemMissmatchErr"), "points", numPoints, 
                    "parameter values", uk.length));
        if (!MathTools.isApproxEqual(uk[0], 0.0))
            throw new IllegalArgumentException(RESOURCES.getString("paramArrNotZeroErr"));
        if (!MathTools.isApproxEqual(uk[numPoints-1],1.0))
            throw new IllegalArgumentException(RESOURCES.getString("paramArrNotOneErr"));
        for (int i=1; i < numPoints; ++i) {
            if (uk[i] <= uk[i-1])
                throw new IllegalArgumentException(RESOURCES.getString("paramNotMonotonicErr"));
        }

        StackContext.enter();
        try {
            //  Create the basis function coefficients matrix N.
            RealMatrix Nmat = createInterpBFMatrix(uKnots, uk, numPoints, degree);

            //  Create the matrix of data points (each row is a point, each column is a dimension).
            int numDims = points.getPhyDimension();
            double[] tmpArr = new double[numDims];
            RealMatrix Dmat = MatrixUtils.createRealMatrix(numPoints, numDims);
            Unit<Length> refUnits = points.getUnit();
            for (int i = 0; i < numPoints; ++i) {
                GeomPoint point = points.get(i).to(refUnits);
                Dmat.setRow(i, point.toArray(tmpArr));
            }

            //  Solve for the matrix of control points:  P = N^-1 * D
            DecompositionSolver solver = new LUDecomposition(Nmat).getSolver();
            RealMatrix Pmat = solver.solve(Dmat);

            //  Create a list of control points from the matrix of control point positions.
            FastTable<ControlPoint> cpList = pntMatrix2ControlPoints(Pmat, refUnits);

            //  Make sure that the 1st and the last point are exactly those from the input list.
            cpList.set(0, ControlPoint.valueOf(points.get(0).to(refUnits).copyToReal(), 1));
            cpList.set(cpList.size() - 1, ControlPoint.valueOf(points.get(points.size() - 1).to(refUnits).copyToReal(), 1));

            //  Create a new curve object.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, uKnots);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a {@link NurbsCurve} of the specified degreeU that is fit to, interpolates,
     * or passes through the specified list of geometry points constrained to the input
     * list of tangent vectors at each point in the input order. This is a global
     * interpolation of the points, meaning that if one of the input points is moved, it
     * will affect the entire curve, not just the local portion near the point.
     *
     * @param degree   The degreeU of the NURBS curve to create (must be > 0 and &lt; the
     *                 number of data points).
     * @param points   The string of Geom Point objects to pass the curve through. May not
     *                 be null.
     * @param tangents A list of tangent vectors, one for each point in "points". May not
     *                 be null.
     * @param unitFlag A flag indicating that the derivatives are all unit vectors if
     *                 <code>true</code>.
     * @param tanlen   A multiplicative factor for the tangent (must be greater than 0 or
     *                 the function aborts and should be near 1.0). Used only if
     *                 <code>unitFlag == true</code>.
     * @return A NurbsCurve fit to the input list of points and tangent vectors.
     * @throws IllegalArgumentException if the degreeU is &le; 0 or &ge: the number of
     * points input.
     * @throws SingularMatrixException if the decomposed matrix is singular.
     * @see #approxPoints
     */
    public static BasicNurbsCurve fitPoints(int degree, PointString<? extends GeomPoint> points,
            List<GeomVector> tangents, boolean unitFlag, double tanlen)
            throws IllegalArgumentException, SingularMatrixException, ParameterException {

        if (degree <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("posDegreeErr"), degree));

        if (tanlen <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("posMultFactorErr"), tanlen));

        int numPoints = points.size();
        if (degree >= numPoints)
            throw new IllegalArgumentException(RESOURCES.getString("numPointsLTDegree") + " np = "
                    + numPoints + ", d = " + degree + ".");

        if (numPoints != tangents.size())
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("numItemMissmatchErr"), "points", numPoints,
                    "tangents", tangents.size()));

        StackContext.enter();
        try {
            Parameter chordLength = points.length();
            if (chordLength.getValue() < EPSC) {
                //  We have a degenerate curve.
                return StackContext.outerCopy(createPoint(degree, points.get(0)));
            }
            if (unitFlag)
                chordLength = chordLength.times(tanlen);

            int n = 2 * numPoints;

            //  Turn the points into an array of parameter values along the curve.
            double[] uk = parameterizeString(points);
            parameterizationCheck(numPoints, uk);

            //  Generate the knot vector for interpolation.
            KnotVector uKnots;
            int m = n + degree;
            int uLength = m + 1;
            double U[];
            switch (degree) {
                case 2:
                    U = ArrayFactory.DOUBLES_FACTORY.array(uLength);
                    for (int i = 0; i <= degree; ++i) {
                        U[i] = 0;
                        U[uLength - 1 - i] = 1;
                    }
                    for (int i = 0; i < numPoints - 1; ++i) {
                        U[2 * i + degree] = uk[i];
                        U[2 * i + degree + 1] = (uk[i] + uk[i + 1]) / 2;
                    }

                    uKnots = array2KnotVector(degree, U, uLength);
                    break;

                case 3:
                    U = ArrayFactory.DOUBLES_FACTORY.array(uLength);
                    for (int i = 0; i <= degree; ++i) {
                        U[i] = 0;
                        U[uLength - 1 - i] = 1;
                    }
                    for (int i = 1; i < numPoints - 1; ++i) {
                        U[degree + 2 * i] = (2 * uk[i] + uk[i + 1]) / 3;
                        U[degree + 2 * i + 1] = (uk[i] + 2 * uk[i + 1]) / 3;
                    }
                    U[4] = uk[1] / 2;
                    U[uLength - degree - 2] = (uk[numPoints - 1] + 1) / 2;

                    uKnots = array2KnotVector(degree, U, uLength);
                    break;

                default:
                    int uk2Length = 2 * numPoints;
                    double uk2[] = ArrayFactory.DOUBLES_FACTORY.array(uk2Length);
                    for (int i = 0; i < numPoints - 1; ++i) {
                        uk2[2 * i] = uk[i];
                        uk2[2 * i + 1] = (uk[i] + uk[i + 1]) / 2;
                    }
                    uk2[uk2Length - 2] = (uk2[uk2Length - 1] + uk2[uk2Length - 3]) / 2;
                    uKnots = buildInterpKnotVector(uk2, uk2Length, degree);
                    break;
            }

            //  Create the basis function coefficients matrix N.
            
            //  Create a list of lists of zeros of the right dimensions.
            RealMatrix Nmat = MatrixUtils.createRealMatrix(n, n);

            //  Now fill in the basis function values.
            for (int i = 0; i < numPoints - 1; i++) {

                //  Get the basis function values and their 1st derivatives for
                //  this span from the knot vector.
                int span = uKnots.findSpan(uk[i]);
                double tmpD[][] = uKnots.basisFunctionDerivatives(span, uk[i], 1);

                //  Copy the basis function values into the matrix.
                int row1 = 2 * i;
                int row2 = row1 + 1;
                int pos = span - degree;
                for (int j = 0; j <= degree; ++j, ++pos) {
                    double value = tmpD[0][j];
                    Nmat.setEntry(row1, pos, value);
                    double deriv = tmpD[1][j];
                    Nmat.setEntry(row2, pos, deriv);
                }

            }
            Nmat.setEntry(0, 0, 1);                 //  N(0,0) = 1
            Nmat.setEntry(1, 0, -1);                //  N(1,0) = -1
            Nmat.setEntry(1, 1, 1);                 //  N(1,1) = 1
            Nmat.setEntry(n - 2, n - 2, -1);        //  N(n-2,n-2) = -1
            Nmat.setEntry(n - 2, n - 1, 1);         //  N(n-2,n-1) = 1
            Nmat.setEntry(n - 1, n - 1, 1);         //  N(n-1,n-1) = 1

            //  Create the matrix of data points & derivatives (each row is a point/derivative,
            //  each column is a dimension).
            Unit<Length> refUnits = points.getUnit();
            int numDims = points.getPhyDimension();
            double[] tmpArr = new double[numDims];
            RealMatrix Dmat = MatrixUtils.createRealMatrix(n, numDims);
            int row = 0;
            for (int i = 0; i < numPoints; ++i, ++row) {
                GeomPoint qp = points.get(i).to(refUnits);
                GeomVector dp = tangents.get(i);
                Dmat.setRow(row, qp.toArray(tmpArr));
                if (unitFlag)
                    dp = dp.times(chordLength);
                dp = (GeomVector)dp.to(refUnits);
                ++row;
                Dmat.setRow(row, dp.toArray(tmpArr));
            }

            double d0Factor = uKnots.getValue(degree + 1) / degree;
            double dnFactor = (1 - uKnots.getValue(uKnots.length() - degree - 2)) / degree;
            GeomVector dp0 = tangents.get(0);
            GeomVector dpn = tangents.get(numPoints - 1);
            if (unitFlag) {
                dp0 = dp0.times(chordLength).to(refUnits);
                dpn = dpn.times(chordLength).to(refUnits);
            }
            GeomPoint qpn = points.get(numPoints - 1).to(refUnits);
            Dmat.setRow(1, dp0.times(d0Factor).toArray(tmpArr));
            Dmat.setRow(n - 2, dpn.times(dnFactor).toArray(tmpArr));
            Dmat.setRow(n - 1, qpn.toArray(tmpArr));

            //  Solve for the matrix of control points:  P = N^-1 * D
            DecompositionSolver solver = new LUDecomposition(Nmat).getSolver();
            RealMatrix Pmat = solver.solve(Dmat);

            //  Create a list of control points from the matrix of control point positions.
            FastTable<ControlPoint> cpList = pntMatrix2ControlPoints(Pmat, refUnits);

            //  Create a new curve object.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, uKnots);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Turn a string of points into a list of parameter values along a curve through those
     * points. Uses the chord length method. The returned array was created using
     * ArrayFactor.DOUBLES_FACTORY and can be recycled.
     *
     * @param points A string of points to be parameterized for use in a curve. May not be
     *               null.
     * @return An array of the parameterizations for each point in the points string.
     *         WARNING: the array will likely be longer than the length of the points
     *         string and the additional values will be garbage. The returned array can be
     *         recycled with ArrayFactor.DOUBLES_FACTORY.recycle(arr).
     */
    public static double[] parameterizeString(PointString<? extends GeomPoint> points) {
        //  This uses the Chord Length method which is described here:
        //      http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/INT-APP/PARA-chord-length.html

        Unit<Length> refUnit = points.get(0).getUnit();
        int numPoints = points.size();
        int n = numPoints - 1;

        double[] uk = ArrayFactory.DOUBLES_FACTORY.array(numPoints);
        uk[0] = 0;
        uk[n] = 1;

        StackContext.enter();
        try {
            double tmp[] = ArrayFactory.DOUBLES_FACTORY.array(n);

            double d = 0;
            for (int k = 1; k <= n; k++) {
                int km1 = k - 1;
                Parameter<Length> distance = points.get(k).distance(points.get(km1));
                double distV = distance.getValue(refUnit);
                d += distV;
                tmp[km1] = distV;
            }
            if (abs(d) < EPSC) {
                //  The points form a degenerate curve of zero length.
                //  Just evenly space out parameters.
                for (int i = 1; i < n; ++i) {
                    double u = (double)(i) / n;
                    uk[i] = u;
                }

            } else {
                //  The points form a curve of finite length.
                int nm1 = n - 1;
                for (int i = 0, ip1 = 1; i < nm1; ++i, ++ip1) {
                    uk[ip1] = uk[i] + tmp[i] / d;
                }
            }

        } finally {
            StackContext.exit();
        }

        return uk;
    }

    /**
     * Check the array of parameterization values and reports any problems by throwing
     * a ParameterException.
     * 
     * @param size The number of parameters in the array.
     * @param uk The array of parameterization values to check.
     * @throws ParameterException if there is a problem with the parameterization array.
     */
    public static void parameterizationCheck(int size, double[] uk) throws ParameterException {
        double sm1 = uk[0];
        double s = uk[1];
        if (s < sm1) {
            System.out.println("sm1 = " + sm1 + ", s = " + s);
            throw new ParameterException(RESOURCES.getString("paramNotMonotonicErr"));
        }
        
        for (int i=2; i < size; ++i) {
            double sp1 = uk[i];
            if (sp1 < s) {
                System.out.println("s = " + s + ", sp1 = " + sp1);
                throw new ParameterException(RESOURCES.getString("paramNotMonotonicErr"));
            }
            
            if (sp1 - s <= MathTools.EPS || s - sm1 <= MathTools.EPS)
                throw new ParameterException(RESOURCES.getString("pntsToClose"));
            
            //  Prepare for next loop.
            sm1 = s;
            s = sp1;
        }
    }
    
    /**
     * Create a {@link NurbsCurve} of the specified degreeU that is fit to, interpolates,
     * or passes through the specified list of control points in the input order. This is
     * a global interpolation of the points, meaning that if one of the input points is
     * moved, it will affect the entire curve, not just the local portion near the point.
     *
     * @param degree The degreeU of the NURBS curve to create (must be > 0 and &lt; the
     *               number of data points).
     * @param points The list of Control Point objects to pass the curve through. May not
     *               be null.
     * @param uk     The parameterization to use for the input points. May not be null.
     * @param uKnots The knot vector to use with the output curve. May not be null.
     * @return A NurbsCurve fit to the input list of points.
     * @throws IllegalArgumentException if any of the inputs are invalid.
     * @throws SingularMatrixException if the decomposed matrix is singular.
     * @see #approxPoints
     */
    static BasicNurbsCurve fitControlPoints(int degree, List<ControlPoint> points,
            double[] uk, KnotVector uKnots) throws IllegalArgumentException, SingularMatrixException {

        //  This follows the basic process outlined here:
        //      http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/INT-APP/CURVE-INT-global.html
        //  Basically, you form a matrix of basis function coefficients (N).  The data
        //  points are in matrix (D) and you solve for the control point locations (P).
        //      D = N * P   -->  P = N^-1 * D
        requireNonNull(uk);
        requireNonNull(uKnots);
        if (degree <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("posDegreeErr"), degree));

        int numPoints = points.size();
        if (degree >= numPoints)
            throw new IllegalArgumentException(RESOURCES.getString("numPointsLTDegree") + " np = "
                    + numPoints + ", d = " + degree + ".");

        StackContext.enter();
        try {
            //  Create the basis function coefficients matrix N.
            RealMatrix Nmat = createInterpBFMatrix(uKnots, uk, numPoints, degree);

            //  Create the matrix of data points (each row is a point, each column is a dimension).
            int numDims = points.get(0).getPhyDimension();
            double[] tmpArr = new double[numDims + 1];
            RealMatrix Dmat = MatrixUtils.createRealMatrix(numPoints, numDims + 1);
            Unit<Length> refUnits = points.get(0).getUnit();
            for (int i = 0; i < numPoints; ++i) {
                ControlPoint point = points.get(i).to(refUnits);
                Dmat.setRow(i, point.toArray(tmpArr));
            }

            //  Solve for the matrix of control points:  P = N^-1 * D
            DecompositionSolver solver = new LUDecomposition(Nmat).getSolver();
            RealMatrix Pmat = solver.solve(Dmat);

            //  Create a list of control points from the matrix of control point coordinates.
            FastTable<ControlPoint> cpList = cpMatrix2ControlPoints(Pmat, refUnits);

            //  Create a new curve object.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, uKnots);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a vector of knotsU to go with the list of parameter values for a curve that
     * passes through a list of points. This uses the knot averaging technique.
     *
     * @param uk        The parameter values for each point in the curve. May not be null.
     * @param numPoints The total number of points in the curve (and parameter array).
     * @param p         The degreeU of the NURBS curve.
     * @return A vector of knotsU of degreeU p that are parameterized for use with the input
     *         list of point parameter values.
     */
    public static KnotVector buildInterpKnotVector(double uk[], int numPoints, int p) {
        requireNonNull(uk);
        int m = numPoints + p;
        int n = numPoints - 1;
        int uLength = m + 1;
        double u[] = ArrayFactory.DOUBLES_FACTORY.array(uLength);

        for (int i = 0; i <= p; i++) {
            u[i] = 0;
            u[uLength - 1 - i] = 1;
        }
        for (int j = 1; j <= n - p; j++) {
            double sum = 0;
            for (int i = j; i <= j + p - 1; i++) {
                sum += uk[i];
            }
            u[j + p] = sum / p;
        }

        KnotVector kv = array2KnotVector(p, u, uLength);
        ArrayFactory.DOUBLES_FACTORY.recycle(u);

        return kv;
    }

    /**
     * Convert an array of doubles into a KnotVector.
     *
     * @param p       The degreeU of the KnotVector to create.
     * @param u       The array of doubles to be converted.
     * @param uLength The number of elements from "u" to use in the knot vector.
     * @return A KnotVector of degreeU "p" made up from the "uLength" values of "u".
     */
    private static KnotVector array2KnotVector(int p, double[] u, int uLength) {

        StackContext.enter();
        try {
            FastTable<Float64> kvValues = FastTable.newInstance();

            for (int i = 0; i < uLength; ++i)
                kvValues.add(Float64.valueOf(u[i]));
            KnotVector kv = KnotVector.newInstance(p, Float64Vector.valueOf(kvValues));
            return StackContext.outerCopy(kv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a numPoints by numPoints sized matrix that contains the basis function
     * values for each control point for each parametric position input.
     * <pre>
     *    N = [ N0,p(t0)  N1,p(t0)  ...  Nn,p(t0)]
     *        [ N0,p(t1)  N1,p(t1)  ...  Nn,p(t1)]
     *        [   .          .              .    ]
     *        [ N0,p(tn)  N1,p(tn)  ...  Nn,p(tn)]
     * </pre>
     *
     * @param knots     The knot vector for a NURBS curve.
     * @param tk        A list of parametric parameter values.
     * @param numPoints The number of parametric values (the size of the data in "tk").
     * @param degree    The degreeU of the NURBS curve.
     */
    private static RealMatrix createInterpBFMatrix(KnotVector knots, double[] tk, int numPoints, int degree) {

        //  Create an array of zeros of the right dimensions.
        RealMatrix Nmat = MatrixUtils.createRealMatrix(numPoints, numPoints);

        //  Now fill in the basis function values.
        for (int i = 0; i < numPoints; ++i) {

            //  Get the basis function values for this span from the knot vector.
            int span = knots.findSpan(tk[i]);
            double tmp[] = knots.basisFunctions(span, tk[i]);

            //  Copy the basis function values into the matrix.
            int pos = span - degree;
            for (int j = 0; j <= degree; ++j, ++pos) {
                Nmat.setEntry(i, pos, tmp[j]);
            }

            ArrayFactory.DOUBLES_FACTORY.recycle(tmp);
        }

        return Nmat;
    }

    /**
     * Return a list of control points from the input matrix of control point positions
     * (points in rows, dimensions in columns).
     */
    private static FastTable<ControlPoint> pntMatrix2ControlPoints(RealMatrix Pmat, Unit units) {
        //  Create a list of control points from the matrix of control point positions.
        FastTable<ControlPoint> cpList = FastTable.newInstance();
        int numPoints = Pmat.getRowDimension();
        for (int i = 0; i < numPoints; ++i) {
            Point pnt = Point.valueOf(units, Pmat.getRow(i));
            cpList.add(ControlPoint.valueOf(pnt, 1));
        }
        return cpList;
    }

    /**
     * Return a list of control points from the input matrix of control point coordinates
     * (points in rows, dimensions in columns).
     */
    private static FastTable<ControlPoint> cpMatrix2ControlPoints(RealMatrix Pmat, Unit units) {

        //  Create a list of control points from the matrix of control point positions.
        FastTable<ControlPoint> cpList = FastTable.newInstance();
        int numPoints = Pmat.getRowDimension();
        for (int i = 0; i < numPoints; i++) {
            ControlPoint pnt = ControlPoint.valueOf(units, Pmat.getRow(i));
            cpList.add(pnt);
        }

        return cpList;
    }

    /**
     * Create a {@link NurbsCurve} of the specified degreeU that approximates, in a least
     * squared error sense, the specified list of geometry points in the input order. This
     * is a global approximation of the points, meaning that if one of the input points is
     * moved, it will affect the entire curve, not just the local portion near the point.
     *
     * @param degree The degreeU of the NURBS curve to create (must be &gt; 0 and &lt; the
     *               number of data points).
     * @param nCP    The number of control points in the new curve (must be &gt; 1 and
     *               &lt; the number of data points).
     * @param points The string of GeomPoint objects to be approximated by the curve. May
     *               not be null.
     * @return A NurbsCurve approximation to the input list of points.
     * @throws SingularMatrixException if the decomposed matrix is singular.
     * @throws IllegalArgumentException if the any of the inputs is invalid.
     * @see #fitPoints
     */
    public static BasicNurbsCurve approxPoints(int degree, int nCP, PointString<? extends GeomPoint> points)
            throws IllegalArgumentException, SingularMatrixException, ParameterException {

        if (degree <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("posDegreeErr"), degree));

        int numPoints = points.size();
        if (degree >= numPoints)
            throw new IllegalArgumentException(RESOURCES.getString("numPointsLTDegree") + " np = "
                    + numPoints + ", d = " + degree + ".");

        if (nCP <= 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numPointsLEOneErr"), "S"));

        if (nCP >= numPoints)
            throw new IllegalArgumentException(RESOURCES.getString("numPointsGTnumCPErr"));

        //  Check for a degenerate curve that collapses to a point.
        if (points.length().getValue() < EPSC) {
            return createPoint(degree, points.get(0));
        }

        //  Check for the degenerate case of a degreeU=1 curve having only 2 points (a straight line).
        if (nCP - 2 <= 0) {
            return createLine(points.get(0), points.get(numPoints - 1));
        }

        //  Turn the points into an array of parameter values along the curve.
        double[] ub = parameterizeString(points);
        parameterizationCheck(numPoints, ub);

        //  Generate the knot vector for approximation.
        KnotVector uKnots = buildApproxKnotVector(ub, numPoints, nCP, degree);

        //  Create the approximating curve.
        BasicNurbsCurve crv = approxPoints(degree, nCP, points, ub, uKnots);
        ArrayFactory.DOUBLES_FACTORY.recycle(ub);

        return crv;
    }

    /**
     * Create a {@link NurbsCurve} of the specified degreeU that approximates, in a least
     * squared error sense, the specified list of geometry points in the input order. This
     * is a global approximation of the points, meaning that if one of the input points is
     * moved, it will affect the entire curve, not just the local portion near the point.
     *
     * @param degree The degreeU of the NURBS curve to create (must be > 0 and &lt; the
     *               number of data points).
     * @param nCP    The number of control points in the new curve (must be > 1 and &lt;
     *               the number of data points).
     * @param points The string of GeomPoint objects to be approximated by the curve.
     * @return A NurbsCurve approximation to the input list of points.
     * @throws SingularMatrixException if the decomposed matrix is singular.
     * @throws IllegalArgumentException if the any of the inputs is invalid.
     */
    private static BasicNurbsCurve approxPoints(int degree, int nCP, PointString<? extends GeomPoint> points,
            double[] ub, KnotVector uKnots) throws IllegalArgumentException, SingularMatrixException {
        //  Reference: Piegl, L., Tiller, W., The Nurbs Book, 2nd Edition, Springer-Verlag, Berlin, 1997.
        //  Based on discussion in section 9.4.1 on page 410.

        StackContext.enter();
        try {
            //  Determine the units used by the list of points (or at least by the 1st one).
            Unit<Length> refUnits = points.getUnit();

            //  Create the basis function coefficients matrix N.
            int numPoints = points.size();
            RealMatrix Nmat = createApproxBFMatrix(uKnots, ub, numPoints, nCP, degree);

            //  Create an Rk vector that represents Eqn. 9.63, pg. 411.
            int nm1 = nCP - 1;
            int mm1 = numPoints - 1;
            Point Q0 = points.get(0).to(refUnits).immutable();
            Point Qmm1 = points.get(mm1).to(refUnits).immutable();
            FastTable<Point> Rk = FastTable.newInstance();
            for (int i = 0; i < numPoints; i++) {
                //  Rk[i] = Q[i]-N(i,0)*Q[0]-N(i,n-1)*Q[m-1];
                GeomPoint Qi = points.get(i).to(refUnits);
                double Ni0 = Nmat.getEntry(i, 0);
                double Ninm1 = Nmat.getEntry(i, nm1);   //  N(i,n-1)
                Point Rki = Qi.minus(Q0.times(Ni0)).minus(Qmm1.times(Ninm1));
                Rk.add(Rki);
            }

            //  Setup the R matrix of data points (each row is a point, each column is a dimension).
            int numDims = Q0.getPhyDimension();
            RealMatrix Rmat = MatrixUtils.createRealMatrix(nCP, numDims);
            double[] tmpArr = new double[numDims];
            Rmat.setRow(0, Q0.toArray(tmpArr));              //  R[0] = Q[0];
            Rmat.setRow(nm1, Qmm1.toArray(tmpArr));          //  R[n-1] = Q[m-1];
            Point ZERO = Point.newInstance(numDims, refUnits);
            for (int i = 0; i < nCP; ++i) {
                Point Ri = ZERO;

                for (int k = 1; k < mm1; ++k) {
                    //  R[i] += N(k,i)*rk[k];
                    double Nki = Nmat.getEntry(k, i);
                    Ri = Ri.plus(Rk.get(k).times(Nki));
                }

                Rmat.setRow(i, Ri.toArray(tmpArr));
            }

            // Solve for the matrix of control points: N^T*N*P = R  ==>  P = (N^T*N)^-1 * R = M^-1 * R
            RealMatrix Mmat = Nmat.transpose().multiply(Nmat);  //  M = N^T*N
            DecompositionSolver solver = new LUDecomposition(Mmat).getSolver();
            RealMatrix Pmat = solver.solve(Rmat);

            //  Create a list of control points from the matrix of control point positions.
            FastTable<ControlPoint> cpList = pntMatrix2ControlPoints(Pmat, refUnits);
            cpList.set(0, ControlPoint.valueOf(Q0, 1));
            cpList.set(nCP - 1, ControlPoint.valueOf(Qmm1, 1));

            //  Create a new curve object.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpList, uKnots);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a vector of knotsU to go with the list of parameter values for a curve that
     * approximates a list of points.
     *
     * @param ub        The parameter values for each point in the curve. May not be null.
     * @param numPoints The total number of points in the curve (and parameter array).
     * @param nCP       The number of control points in the new curve.
     * @param p         The degreeU of the NURBS curve.
     */
    static KnotVector buildApproxKnotVector(double ub[], int numPoints, int nCP, int p) {
        //  Similar to Eqn 9.69 in NURBS book (pg. 412), but modified slighlty.

        requireNonNull(ub);
        StackContext.enter();
        try {
            int uLength = nCP + p + 1;

            //  Initialize the ends of the knot vector.
            double u[] = ArrayFactory.DOUBLES_FACTORY.array(uLength);
            for (int i = 0; i <= p; i++) {
                u[i] = 0;
                u[uLength - 1 - i] = 1;
            }

            double d = ((double)numPoints) / nCP;
            for (int j = 1; j < nCP - p; j++) {
                u[p + j] = 0;

                for (int k = j; k < j + p; ++k) {
                    double kd = k * d;
                    int i = (int)kd;
                    double a = kd - i;
                    int i2 = (int)((k - 1) * d);
                    u[p + j] += a * ub[i2] + (1 - a) * ub[i];
                }
                u[p + j] /= p;
            }

            FastTable<Float64> kvValues = FastTable.newInstance();
            for (int i = 0; i < uLength; ++i) {
                kvValues.add(Float64.valueOf(u[i]));
            }

            KnotVector kv = KnotVector.newInstance(p, Float64Vector.valueOf(kvValues));
            return StackContext.outerCopy(kv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a numPoints by nCP sized matrix that contains the basis function values for
     * each control point for each parametric position input.
     * <pre>
     *    N = [ N0,p(u0)  N1,p(u0)  ...  Nn,p(u0)]
     *        [ N0,p(u1)  N1,p(u1)  ...  Nn,p(u1)]
     *        [   .          .              .    ]
     *        [ N0,p(um)  N1,p(um)  ...  Nn,p(um)]
     * </pre>
     *
     * @param knots     The knot vector for a NURBS curve.
     * @param ub        A list of parametric parameter values for each data point.
     * @param numPoints The number of points in the input data set.
     * @param nCP       The number of control points for the output curve.
     * @param degree    The degreeU of the NURBS curve.
     */
    private static RealMatrix createApproxBFMatrix(KnotVector knots, double[] ub, int numPoints, int nCP, int degree) {
        //  Builds up Eqn. 9.66 in the NURBS book (pg. 411).

        RealMatrix Nmat = MatrixUtils.createRealMatrix(numPoints, nCP);
        Nmat.setEntry(0, 0, 1);                      // N(0,0) = 1.0;
        Nmat.setEntry(numPoints - 1, nCP - 1, 1);   //  N(m-1,n-1) = 1.0;

        //  Fill in the basis function values.
        for (int i = 0; i < numPoints; i++) {

            //  Get the basis function values for the ith span from the knot vector.
            int span = knots.findSpan(ub[i]);
            double funs[] = knots.basisFunctions(span, ub[i]);

            //  Copy the basis function values into the matrix.
            int pos = span - degree;
            for (int j = 0; j <= degree; ++j, ++pos) {
                Nmat.setEntry(i, pos, funs[j]);
            }

            ArrayFactory.DOUBLES_FACTORY.recycle(funs);
        }

        return Nmat;

    }

}
