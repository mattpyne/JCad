/**
 * KnotVector -- A collection of knot values used in a NURBS curve.
 * 
 * Copyright (C) 2009-2015, by Joseph A. Huwaldt. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 * 
 * This source file is based on, but slightly modified from, the LGPL jgeom (Geometry
 * Library for Java) by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.ValueType;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * An immutable collection of knot values that are associated with a NURBS curve or
 * surface.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Samuel Gerber, Date: May 14, 2009, Version 1.0.
 * @version November 28, 2015
 */
@SuppressWarnings("serial")
public class KnotVector implements Cloneable, XMLSerializable, ValueType {

    /*
     * References:
     *  1.) Piegl, L., Tiller, W., The Nurbs Book, 2nd Edition, Springer-Verlag, Berlin, 1997.
     */
    
    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    private boolean _open;          //  Indicates if the knot vector is open or closed.
    private Float64Vector _knots;   //  The vector of knot values.
    private int _degree;            //  The degreeU of the curve.
    private int _nu;

    /**
     * Create a KnotVector from the given knot values of the desired degreeU.
     *
     * @param degree degreeU of NURBS curve
     * @param knots knot values. May not be null.
     * @return A KnotVector consisting of the given knotsU and degreeU.
     * @throws IllegalArgumentException if the knot vector is not valid.
     */
    public static KnotVector newInstance(int degree, Float64Vector knots) {
        int numKnots = knots.getDimension();
        for (int i = 1; i < numKnots; i++) {
            if (knots.getValue(i - 1) > knots.getValue(i)) {
                throw new IllegalArgumentException(RESOURCES.getString("knotsNotIncreasingErr"));
            }
        }
        for (int i = 0; i < numKnots; ++i) {
            double kv = knots.getValue(i);
            if (kv > 1.0 || kv < 0.0) {
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("invalidKnotValue"), kv));
            }
        }

        KnotVector o = FACTORY.object();

        o._knots = knots;
        o._degree = degree;
        o._nu = knots.getDimension() - degree - 2;

        //Check if it is an open knot vector
        o._open = determineIfOpen(o);

        return o;
    }

    /**
     * Method that determines if the specified KnotVector is open or closed and returns
     * true if it is open.
     */
    private static boolean determineIfOpen(KnotVector o) {
        Float64Vector knots = o._knots;
        int degree = o._degree;

        boolean open = true;
        for (int k = 0; k < degree && open == true; k++) {
            if (knots.getValue(k) != knots.getValue(k + 1)) {
                open = false;
            }
        }
        int m = knots.getDimension() - 1;
        for (int k = m; k > m - degree && open == true; k--) {
            if (knots.getValue(k) != knots.getValue(k - 1)) {
                open = false;
            }
        }

        return open;
    }

    /**
     * Create a KnotVector from the given list of knot values of the desired degreeU.
     *
     * @param degree degreeU of NURBS curve
     * @param knots A list of knot values.  May not be null.
     * @return A KnotVector consisting of the given knotsU and degreeU.
     * @throws IllegalArgumentException if the knot vector is not valid.
     */
    public static KnotVector newInstance(int degree, List<Double> knots) throws IllegalArgumentException {
        FastTable<Float64> kvList = FastTable.newInstance();
        for (double value : knots) {
            kvList.add(Float64.valueOf(value));
        }
        Float64Vector kv = Float64Vector.valueOf(kvList);
        return newInstance(degree, kv);
    }

    /**
     * Create a KnotVector from the given knot values of the desired degreeU.
     *
     * @param degree degreeU of NURBS curve
     * @param knots knot values.  May not be null.
     * @return A KnotVector consisting of the given knotsU and degreeU.
     * @throws IllegalArgumentException if the knot vector is not valid.
     */
    public static KnotVector newInstance(int degree, double... knots) throws IllegalArgumentException {
        return newInstance(degree, Float64Vector.valueOf(knots));
    }

    /**
     * Return a knot vector that can be used to create a Bezier curve segment of the
     * specified degreeU using the BasicNurbsCurve class.
     *
     * @param degree The degreeU of the knot vector to return.
     * @return A knot vector appropriate for creating a Bezier curve segment.
     */
    public static KnotVector bezierKnotVector(int degree) {

        StackContext.enter();
        try {
            FastTable<Float64> floatLst = FastTable.newInstance();
            for (int i = 0; i <= degree; ++i) {
                floatLst.add(Float64.ZERO);
            }
            for (int i = 0; i <= degree; ++i) {
                floatLst.add(Float64.ONE);
            }
            KnotVector kvBezier = KnotVector.newInstance(degree, Float64Vector.valueOf(floatLst));
            FastTable.recycle(floatLst);

            return StackContext.outerCopy(kvBezier);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the span (position of corresponding knot values in knot vector) a given
     * parameter value belongs to.
     *
     * @param s parameter value to find the span for
     * @return Position of span.
     */
    public int findSpan(double s) {
        //  Algorithm A2.1 from Ref. 1.
        if (s >= _knots.getValue(_nu + 1)) {
            return _nu;
        }

        int low = _degree;
        int high = _nu + 1;
        int mid = (low + high) / 2;
        while ((s < _knots.getValue(mid) || s >= _knots.getValue(mid + 1)) && low < high) {
            if (s < _knots.getValue(mid)) {
                high = mid;
            } else {
                low = mid;
            }
            mid = (low + high) / 2;
        }

        return mid;
    }

    /**
     * Returns the basis function values for the given parameter value (Nik(s)). This
     * function first calculates the span which is needed in order to calculate the basis
     * functions values.
     *
     * @param s Parameter value to calculate basis functions for.
     * @return basis function values. WARNING: the returned array will likely be longer
     * than [0..degreeU], so do NOT depend on array.length in any iterations over the
     * array!. The additional array elements will contain garbage and should not be used.
     * The returned array was allocated using
     * javolution.context.ArrayFactory.DOUBLES_FACTORY and could be recycled by the user
     * when no longer needed.
     */
    public double[] basisFunctions(double s) {
        return basisFunctions(findSpan(s), s);
    }

    /**
     * Returns the unweighted basis function values for the given parameter value
     * (Nik(s)), when the span that the parameter value lies in is already known.
     *
     * @param span The span <code>s</code> lies in
     * @param s The parameter value to calculate basis functions for.
     * @return basis function values. WARNING: the returned array will likely be longer
     * than [0..degreeU], so do NOT depend on array.length in any iterations over the
     * array!. The additional array elements will contain garbage and should not be used.
     * The returned array was allocated using
     * javolution.context.ArrayFactory.DOUBLES_FACTORY and could be recycled by the user
     * when no longer needed.
     */
    public double[] basisFunctions(int span, double s) {
        //  Algorithm A2.2 from Ref. 1.
        int degree = _degree;
        int order = degree + 1;
        double res[] = ArrayFactory.DOUBLES_FACTORY.array(order);
        res[0] = 1;
        double left[] = ArrayFactory.DOUBLES_FACTORY.array(order);
        left[0] = 0;
        double right[] = ArrayFactory.DOUBLES_FACTORY.array(order);
        right[0] = 0;

        for (int j = 1; j < order; j++) {
            left[j] = s - _knots.getValue(span + 1 - j);
            right[j] = _knots.getValue(span + j) - s;
            double saved = 0;
            for (int r = 0; r < j; r++) {
                int jmr = j - r;
                int rp1 = r + 1;
                double tmp = res[r] / (right[rp1] + left[jmr]);
                res[r] = saved + right[rp1] * tmp;
                saved = left[jmr] * tmp;
            }
            res[j] = saved;
        }

        //  Clean up before leaving.
        ArrayFactory.DOUBLES_FACTORY.recycle(left);
        ArrayFactory.DOUBLES_FACTORY.recycle(right);

        return res;
    }

    /**
     * Calculates all the derivatives of all the unweighted basis functions from
     * <code>0</code> up to the given grade, <code>d^{grade}Nik(s)/d^{grade}s</code>.
     * <p>
     * Examples:<br>
     * 1st derivative (grade = 1), this returns <code>[Nik(s), dNik(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[Nik(s), dNik(s)/ds, d^2Nik(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param span The span <code>s</code> lies in
     * @param s The parameter value to calculate basis functions for.
     * @param grade The grade to calculate the derivatives for (1=1st derivative, 2=2nd
     * derivative, etc).
     * @return Basis function derivative values. WARNING: the returned array is recycled
     * and will likely be longer than [0..grade+1][0..degreeU+1], so do NOT depend on
     * array.length in any iterations over the array!. The additional array elements will
     * contain garbage and should not be used. The returned array could be recycled by
     * calling <code>KnotVector.recycle2DArray(arr)</code> when no longer needed.
     * @throws IllegalArgumentException if the grade is < 0.
     * @see #basisFunctions
     * @see #recycle2DArray
     */
    public double[][] basisFunctionDerivatives(int span, double s, int grade) {
        //  Algorithm: A2.3, Ref. 1, pg. 72.
        if (grade < 0) {
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));
        }
        int degree = _degree;
        int order = degree + 1;
        int gradeP1 = grade + 1;

        //  double[][] ders = new double[grade+1][degreeU+1];
        double[][] ders = CurveUtils.allocate2DArray(gradeP1, order);           //  Outside of StackContext.

        StackContext.enter();
        try {
            //  double[][] nds = new double[degreeU + 1][degreeU + 1];
            double[][] nds = new2DZeroedArray(order, order);
            nds[0][0] = 1.0;

            //  double[] left = new double[degreeU + 1];
            //  double[] right = new double[degreeU + 1];
            double[] left = ArrayFactory.DOUBLES_FACTORY.array(order);
            left[0] = 0;
            double[] right = ArrayFactory.DOUBLES_FACTORY.array(order);
            right[0] = 0;

            for (int j = 1; j < order; j++) {
                left[j] = s - _knots.getValue(span + 1 - j);
                right[j] = _knots.getValue(span + j) - s;
                double[] ndsj = nds[j];
                double saved = 0.0;
                for (int r = 0; r < j; r++) {
                    int jmr = j - r;
                    int rp1 = r + 1;
                    //  Lower triangle
                    ndsj[r] = right[rp1] + left[jmr];         //  nds[j][r] = right[rp1] + left[jmr];
                    double temp = nds[r][j - 1] / ndsj[r];    //  temp = nds[r][j - 1] / nds[j][r];
                    //  Upper triangle
                    nds[r][j] = saved + right[rp1] * temp;
                    saved = left[jmr] * temp;
                }
                ndsj[j] = saved;      //  nds[j][j] = saved;
            }

            //  Load the basis functions.
            for (int j = order-1; j >= 0; --j) {
                ders[0][j] = nds[j][degree];
            }

            //  double[][] a = new double[2][degreeU + 1];
            double[][] a = new2DZeroedArray(2, order);

            //  This section computes the derivatives (Ref. 1, Eqn. 2.9).
            for (int r = 0; r < order; r++) {
                int s1 = 0, s2 = 1; //  Alternate rows in array a
                a[0][0] = 1.0;

                //  Loop to compute the kth derivative.
                for (int k = 1; k < gradeP1; k++) {
                    double[] as1 = a[s1];
                    double[] as2 = a[s2];

                    double d = 0.0;
                    int rk = r - k;
                    int pk = degree - k;
                    int pkp1 = pk + 1;
                    double[] ndspkp1 = nds[pkp1];
                    if (r >= k) {
                        as2[0] = as1[0] / ndspkp1[rk];    //  a[s2][0] = a[s1][0] / nds[pkp1][rk];
                        d = as2[0] * nds[rk][pk];
                    }
                    int j1, j2;
                    if (rk >= -1)
                        j1 = 1;
                    else
                        j1 = -rk;

                    if (r - 1 <= pk)
                        j2 = k - 1;
                    else
                        j2 = degree - r;

                    for (int j = j1; j <= j2; j++) {
                        int rkpj = rk + j;
                        as2[j] = (as1[j] - as1[j - 1]) / ndspkp1[rkpj]; // a[s2][j] = (a[s1][j] - a[s1][j - 1]) / nds[pkp1][rkpj];
                        d += as2[j] * nds[rkpj][pk];
                    }
                    if (r <= pk) {
                        as2[k] = -as1[k - 1] / ndspkp1[r];    //  a[s2][k] = -a[s1][k - 1] / nds[pkp1][r];
                        d += as2[k] * nds[r][pk];
                    }
                    ders[k][r] = d;

                    //  Switch rows.
                    j1 = s1;
                    s1 = s2;
                    s2 = j1;
                }

            }   //  Next r

            //  Multiply through by the correct factors (Ref. 1, Eq. 2.9).
            int r = degree;
            for (int k = 1; k < gradeP1; k++) {
                @SuppressWarnings("MismatchedReadAndWriteOfArray")
                double[] dersk = ders[k];
                for (int j = order-1; j >= 0; --j) {
                    dersk[j] *= r;      //  ders[k][j] *= r;
                }
                r *= (degree - k);
            }

        } finally {
            StackContext.exit();
        }

        return ders;
    }

    /**
     * Allocate a 2D array using factory methods and fill it with zeros.
     */
    private static double[][] new2DZeroedArray(int rows, int cols) {
        double[][] arr = CurveUtils.allocate2DArray(rows, cols);
        //  Factory allocated arrays are not necessarily zeroed.
        double[] arr0 = arr[0];
        for (int j = cols-1; j >= 0; --j) {
            arr0[j] = 0.;
        }
        for (int i = 1; i < rows; ++i) {
            System.arraycopy(arr0, 0, arr[i], 0, cols);
        }
        return arr;
    }

    /**
     * Return the length of the knot vector (nu).
     *
     * @return The length of the knot vector.
     */
    public int getNu() {
        return _nu;
    }

    /**
     * Return the number of elements in the knot vector.
     *
     * @return The number of elements in the knot vector.
     */
    public int length() {
        return _knots.getDimension();
    }

    /**
     * Return the knot values as an vector of <code>Float64</code> values.
     *
     * @return the vector of knot values
     */
    public Float64Vector getAll() {
        return _knots;
    }

    /**
     * Return the knot at the specified index.
     *
     * @param i Index to get knot value for
     * @return the knot value at index <code>i</code>
     */
    public Float64 get(int i) {
        return _knots.get(i);
    }

    /**
     * Return the knot value at a specific index as a <code>double</code>.
     *
     * @param i Index to get knot value for
     * @return the knot value at index <code>i</code> returned as a <code>double</code>.
     */
    public double getValue(int i) {
        return _knots.getValue(i);
    }

    /**
     * Return the degreeU of the KnotVector
     *
     * @return Degree of the KnotVector
     */
    public int getDegree() {
        return _degree;
    }

    /**
     * Return the number of segments in the knot vector.
     * 
     * @return The number of segments in the knot vector.
     */
    public int getNumberOfSegments() {
        int seg = 0;
        double u = _knots.getValue(0);
        int size = _knots.getDimension();
        for (int i = 1; i < size; i++) {
            double kv = _knots.getValue(i);
            if (u != kv) {
                seg++;
                u = kv;
            }
        }
        return seg;
    }

    /**
     * Return <code>true</code> if the knot vector is open and <code>false</code> if it is
     * closed.
     *
     * @return true if the knot vector is open.
     */
    public boolean isOpen() {
        return _open;
    }

    /**
     * Return <code>true</code> if this KnotVector contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the knot
     * values are NaN or Inf.
     *
     * @return true if the KnotVector contains valid and finite values.
     */
    public boolean isValid() {
        int length = length();
        for (int i = 0; i < length; ++i) {
            Float64 value = _knots.get(i);
            if (value.isNaN() || value.isInfinite()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Find the multiplicity of the knot with the specified index in this knot vector.
     *
     * @param index the index of the knot to observe (the largest index of a repeated
     * series of knotsU).
     * @return the multiplicity of the knot
     */
    public int findMultiplicity(int index) {
        int s = 1;
        int order = getDegree() + 1;
        for (int i = index; i > order; --i) {
            if (getValue(i) <= getValue(i - 1)) {
                ++s;
            } else {
                return s;
            }
        }
        return s;
    }

    /**
     * Return a copy of this knot vector with the parameterization reversed.
     *
     * @return A copy of this KnotVector with the parameterization reversed.
     */
    public KnotVector reverse() {
        StackContext.enter();
        try {
            FastTable<Float64> values = FastTable.newInstance();
            for (int i = length() - 1; i >= 0; --i) {
                values.add(Float64.ONE.minus(get(i)));      //  values.add(1.0 - getValue(i));
            }
            KnotVector kv = KnotVector.newInstance(getDegree(), Float64Vector.valueOf(values));
            return StackContext.outerCopy(kv);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a copy of this KnotVector instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public KnotVector copy() {
        return copyOf(this);
    }

    /**
     * Returns a copy of this KnotVector instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     * @throws java.lang.CloneNotSupportedException Never thrown.
     */
    @Override
    @SuppressWarnings("CloneDoesntCallSuperClone")
    public Object clone() throws CloneNotSupportedException {
        return copy();
    }

    /**
     * Compares this ControlPoint against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        KnotVector that = (KnotVector)obj;
        return this._degree == that._degree
                && this._knots.equals(that._knots);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _degree;
        hash = hash * 31 + var_code;

        var_code = _knots.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    /**
     * Returns the text representation of this knot vector that consists of the degreeU
     * followed by the knot values. For example:
     * <pre>
     *   {degreeU=2,{0.0, 0.0, 0.0, 1.0, 1.0, 1.0}}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append("{degreeU=");
        tmp.append(_degree);
        tmp.append(",");
        tmp.append(_knots.toText());
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns the string representation of this knot vector that consists of the degreeU
     * followed by the knot values. For example:
     * <pre>
     *   {degreeU=2,{0.0, 0.0, 0.0, 1.0, 1.0, 1.0}}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public String toString() {
        return toText().toString();
    }

    /**
     * Recycle any 2D array of doubles that was created by this classes factory methods.
     *
     * @param arr The array to be recycled. The array must have been created by this class
     * or by CurveUtils.allocate2DArray()!
     */
    public static void recycle2DArray(double[][] arr) {
        CurveUtils.recycle2DArray(arr);
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<KnotVector> XML = new XMLFormat<KnotVector>(KnotVector.class) {

        @Override
        public KnotVector newInstance(Class<KnotVector> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, KnotVector obj) throws XMLStreamException {
            int degree = xml.getAttribute("degreeU", 1);

            FastTable<Float64> valueList = FastTable.newInstance();
            while (xml.hasNext()) {
                Float64 value = xml.getNext();
                valueList.add(value);
            }

            Float64Vector knots = Float64Vector.valueOf(valueList);
            int numKnots = knots.getDimension();

            //  Check for valid inputs.
            for (int i = 1; i < numKnots; i++) {
                if (knots.getValue(i - 1) > knots.getValue(i)) {
                    throw new XMLStreamException(RESOURCES.getString("knotsNotIncreasingErr"));
                }
            }
            for (int i = 0; i < numKnots; ++i) {
                double kv = knots.getValue(i);
                if (kv > 1.0 || kv < 0.0) {
                    throw new XMLStreamException(
                            MessageFormat.format(RESOURCES.getString("invalidKnotValue"), kv));
                }
            }

            obj._degree = degree;
            obj._knots = knots;
            obj._nu = numKnots - degree - 2;

            obj._open = determineIfOpen(obj);
        }

        @Override
        public void write(KnotVector obj, OutputElement xml) throws XMLStreamException {

            xml.setAttribute("degreeU", obj._degree);
            int size = obj._knots.getDimension();
            for (int i = 0; i < size; ++i) {
                xml.add(obj._knots.get(i));
            }

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    protected KnotVector() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<KnotVector> FACTORY = new ObjectFactory<KnotVector>() {
        @Override
        protected KnotVector create() {
            return new KnotVector();
        }
    };

    @SuppressWarnings("unchecked")
    private static KnotVector copyOf(KnotVector original) {
        KnotVector o = FACTORY.object();
        o._open = original._open;
        o._knots = original._knots.copy();
        o._degree = original._degree;
        o._nu = original._nu;
        return o;
    }
}
