/**
 * SurfaceFactory -- A factory for creating NURBS surfaces.
 * 
 * Copyright (C) 2010-2017, by Joseph A. Huwaldt. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 * 
 * This source file is based on, but heavily modified from, the LGPL jgeom (Geometry
 * Library for Java) code, NurbsCreator.java, by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import geomss.geom.*;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ImmortalContext;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.util.FastTable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A collection of methods for creating NURBS surfaces
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: June 15, 2010
 * @version October 22, 2017
 */
public final class SurfaceFactory {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    /**
     * The length threshold for a set of points being considered a degenerate curve.
     */
    private static final double EPSC = 1e-10;

    //  A generic geometry space tolerance
    private static final Parameter<Length> GTOL;

    static {
        ImmortalContext.enter();
        try {
            GTOL = Parameter.valueOf(1e-8, SI.METER);
        } finally {
            ImmortalContext.exit();
        }
    }

    private SurfaceFactory() { }

    /**
     * Create a sphere {@link NurbsSurface} about the specified center point with the
     * specified radius.
     *
     * @param center The center of the sphere. May not be null.
     * @param radius The radius of the sphere. May not be null.
     * @return A BasicNurbsSurface representing the sphere.
     */
    public static BasicNurbsSurface createSphere(GeomPoint center, Parameter<Length> radius) {
        requireNonNull(radius);
        StackContext.enter();
        try {
            //  Make sure center point is at least 3D.
            Point o = center.immutable();
            int numDims = o.getPhyDimension();
            if (numDims < 3) {
                o = o.toDimension(3);
                numDims = 3;
            }

            //  Create a polar axis.
            Vector<Dimensionless> polarAxis = GeomUtil.makeZVector(numDims);
            polarAxis.setOrigin(o);

            //  Create a normal vector for the semi-circle.
            Vector<Dimensionless> n = GeomUtil.makeYVector(numDims);

            //  Create the sphere cross-section.
            NurbsCurve xsec = CurveFactory.createSemiCircle(o, radius, n);

            //  Revolve into a sphere.
            BasicNurbsSurface srf = createRevolvedSurface(polarAxis, xsec, 0, TWO_PI);
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a torus {@link NurbsSurface} about the specified center point and axis. The
     * surface will be parameterized such that the "s" direction is around the
     * cross-section of the torus and the t-direction is around the axis.
     *
     * @param center      The center of the torus. May not be null.
     * @param axis        The axis to place the torus about. May not be null.
     * @param innerRadius The inner radius of the torus. May not be null.
     * @param outerRadius The outer radius of the torus. May not be null.
     * @return A BasicNurbsSurface representing the torus.
     */
    public static BasicNurbsSurface createTorus(GeomPoint center, GeomVector<Dimensionless> axis,
            Parameter<Length> innerRadius, Parameter<Length> outerRadius) throws DimensionException {
        requireNonNull(innerRadius);
        
        //  Make sure the input axis is at least 3 dimensional.
        int numDims = axis.getPhyDimension();
        if (numDims < 3)
            axis = axis.toDimension(3);
        numDims = center.getPhyDimension();
        if (numDims < 3)
            center = center.toDimension(3);
        if (center.getPhyDimension() != axis.getPhyDimension())
            throw new DimensionException(RESOURCES.getString("pointAxisDimErr"));

        StackContext.enter();
        try {
            //  Create the yhat & xhat vector (Y & X axes, orthogonal to axis and each other,
            //  in the plane of the torus).
            Vector<Dimensionless> yhat = GeomUtil.calcYHat(axis);
            Vector<Dimensionless> xhat = GeomUtil.calcXHat(axis, yhat);

            //  The radius of the torus cross-section.
            Parameter<Length> xsec_radius = outerRadius.minus(innerRadius).divide(2);

            //  Find the center of the x-section.
            Point xsec_c = center.plus(Point.valueOf(xhat.times(innerRadius.plus(xsec_radius))));

            //  Create a x-section circle.
            NurbsCurve xsec = CurveFactory.createCircle(xsec_c, xsec_radius, yhat);

            //  Revolve into a torus.
            BasicNurbsSurface srf = createRevolvedSurface(axis, xsec, 0, TWO_PI);
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a surface of revolution {@link NurbsSurface} by sweeping the specified curve
     * about the specified axis. The surface will be parameterized such that the curve
     * defines the "s" direction and the revolution around the axis is the "t" direction.
     *
     * @param axis  The axis to sweep the curve about (the vector gives direction; use
     *              axis.setOrigin() to set a point on the axis). May not be null.
     * @param curve The curve to be revolved around the axis. May not be null.
     * @return A BasicNurbsSurface for the input curve revolved around the input axis.
     */
    public static BasicNurbsSurface createRevolvedSurface(GeomVector<Dimensionless> axis, NurbsCurve curve)
            throws DimensionException {
        return createRevolvedSurface(axis, curve, Parameter.ZERO_ANGLE, Parameter.TWOPI_ANGLE);
    }

    /**
     * Create a surface of revolution {@link NurbsSurface} by sweeping the specified curve
     * about the specified axis with the specified curve and angular start and stop values
     * (relative to the curve and axis). The surface will be parameterized such that the
     * curve defines the "s" direction and the revolution around the axis is the "t"
     * direction.
     *
     * @param axis  The axis to sweep the curve about (the vector gives direction; use
     *              axis.setOrigin() to set a point on the axis). May not be null.
     * @param curve The curve to be revolved around the axis. May not be null.
     * @param theta The angle to revolve the curve through (starting at the curve's
     *              position). May not be null.
     * @return A BasicNurbsSurface for the input curve revolved around the input axis.
     */
    public static BasicNurbsSurface createRevolvedSurface(GeomVector<Dimensionless> axis, NurbsCurve curve,
            Parameter<Angle> theta) throws DimensionException {
        return createRevolvedSurface(axis, curve, Parameter.ZERO_ANGLE, theta);
    }

    /**
     * Create a surface of revolution {@link NurbsSurface} by sweeping the specified curve
     * about the specified axis with the specified curve and angular start and stop values
     * (relative to the curve and axis). The surface will be parameterized such that the
     * curve defines the "s" direction and the revolution around the axis is the "t"
     * direction.
     *
     * @param axis   The axis to sweep the curve about (the vector gives direction; use
     *               axis.setOrigin() to set a point on the axis). May not be null.
     * @param curve  The curve to be revolved around the axis. May not be null.
     * @param thetaS The start angle to revolve the curve through (starting at the curve's
     *               position). May not be null.
     * @param thetaE The end angle to revolve the curve through (starting at the curve's
     *               position). If the end angle is smaller than the start angle, it is
     *               increased by increments of 2*PI until it is larger than the start
     *               angle. May not be null.
     * @return A BasicNurbsSurface for the input curve revolved around the input axis.
     */
    public static BasicNurbsSurface createRevolvedSurface(GeomVector<Dimensionless> axis, NurbsCurve curve,
            Parameter<Angle> thetaS, Parameter<Angle> thetaE) throws DimensionException {
        int numDims = curve.getPhyDimension();
        if (numDims < 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast3"), "curve", numDims));
        if (axis.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("crvAxisDimErr"));

        return createRevolvedSurface(axis, curve, thetaS.getValue(SI.RADIAN), thetaE.getValue(SI.RADIAN));
    }

    /**
     * Create a surface of revolution {@link NurbsSurface} by sweeping the specified curve
     * about the specified axis with the specified curve and angular start and stop values
     * (relative to the curve and axis). The surface will be parameterized such that the
     * curve defines the "s" direction and the revolution around the axis is the "t"
     * direction.
     *
     * @param axis  The axis to sweep the curve about (the vector gives direction; use
     *              axis.setOrigin() to set a point on the axis). May not be null.
     * @param curve The curve to be revolved around the axis. May not be null.
     * @param ths   The start angle to revolve the curve through (starting at the curve's
     *              position) in radians.
     * @param the   The end angle to revolve the curve through (starting at the curve's
     *              position) in radians. If the end angle is smaller than the start
     *              angle, it is increased by increments of 2*PI until it is larger than
     *              the start angle.
     * @return A BasicNurbsSurface for the input curve revolved around the input axis.
     */
    private static BasicNurbsSurface createRevolvedSurface(GeomVector<Dimensionless> axis, NurbsCurve curve,
            double ths, double the) {
        requireNonNull(axis);
        requireNonNull(curve);
        
        StackContext.enter();
        try {
            //  Get ths and the in the range 0 - 2*PI.
            ths = GeomUtil.bound2Pi(ths);
            the = GeomUtil.bound2Pi(the);

            //  Make sure the end angle is > than the start angle.
            if (the <= ths)
                the += TWO_PI;
            double theta = the - ths;

            //  How many arc segments are required?
            int narcs = 4;
            if (theta <= HALF_PI)
                narcs = 1;
            else if (theta <= PI)
                narcs = 2;
            else if (theta <= PI + HALF_PI)
                narcs = 3;

            //  Pre-calculate sine and cosine tables.
            double[] cos = ArrayFactory.DOUBLES_FACTORY.array(narcs + 1);
            double[] sin = ArrayFactory.DOUBLES_FACTORY.array(narcs + 1);
            double angle = ths;
            double dtheta = theta / narcs;
            for (int i = 0; i <= narcs; ++i) {
                sin[i] = sin(angle);
                cos[i] = cos(angle);
                angle += dtheta;
            }
            double sinths = sin(ths), cosths = cos(ths);
            double wm = cos(dtheta / 2);

            //  Get the curve's control points.
            List<ControlPoint> pj = curve.getControlPoints();
            int pjSize = pj.size();

            //  Create a list of control points for the surface being generated.
            FastTable<FastTable<ControlPoint>> pij = FastTable.newInstance();
            int num = 2 * narcs + 1;
            for (int j = 0; j < num; ++j) {
                FastTable<ControlPoint> tbl = FastTable.newInstance();
                tbl.setSize(pjSize);
                pij.add(tbl);
            }

            //  Some handy constants for use later.
            Unit<Length> unit = curve.getUnit();
            int numDims = curve.getPhyDimension();
            GeomPoint axisOrigin = axis.getOrigin();

            //  Loop over each of the curves control points.
            MutablePoint p1 = MutablePoint.newInstance(numDims, unit);
            for (int j = 0; j < pjSize; ++j) {

                //  Get the curve control point.
                ControlPoint pjj = pj.get(j);
                double pjjw = pjj.getWeight();
                Point pjjp = pjj.getPoint();

                //  Determine the center of the circle for this curve control point.
                Point center = GeomUtil.pointLineClosest(pjjp, axisOrigin, axis).to(unit);

                // Determine the X-axis direction.
                Vector<Length> xhatd = pjjp.minus(center).toGeomVector();
                Parameter<Length> radius = xhatd.mag();         //  Circle radius
                Vector<Dimensionless> xhat;
                if (radius.getValue(unit) < EPSC) {
                    xhat = GeomUtil.calcYHat(axis); //  X-axis in arbitrary direction perpendicular to axis.
                    radius = Parameter.ZERO_LENGTH;
                } else {
                    xhat = xhatd.toUnitVector();
                }

                //  Determine the Y-axis direction.
                Vector<Dimensionless> yhat = (Vector<Dimensionless>)axis.cross(xhat);

                //  Create the starting control point
                Vector<Dimensionless> p0v = xhat.times(cosths).plus(yhat.times(sinths));
                MutablePoint p0 = MutablePoint.valueOf(center.plus(Point.valueOf(p0v.times(radius))));
                pij.get(0).set(j, ControlPoint.valueOf(p0.immutable(), pjjw));

                //  Create starting perpendicular vector
                Vector<Dimensionless> t0 = xhat.times(-sinths).plus(yhat.times(cosths));

                //  Loop over all the required arcs around the revolution.
                int index = 0;
                for (int i = 1; i <= narcs; i++) {

                    //  Create segment end control point: p2 = r*(xhat*cos[i] + yhat*sin[i])
                    p0v = xhat.times(cos[i]).plus(yhat.times(sin[i]));
                    Point p2 = center.plus(Point.valueOf(p0v.times(radius)));
                    pij.get(index + 2).set(j, ControlPoint.valueOf(p2, pjjw));

                    //  Create end perpendicular vector:    t2 = -xhat*r*sin[i] + yhat*r*cos[i]
                    Vector<Dimensionless> t2 = xhat.times(-sin[i]).plus(yhat.times(cos[i]));

                    //  Find the intersection of the two perpendiculars.
                    GeomUtil.lineLineIntersect(p0, t0, p2, t2, GTOL, null, p1, null);

                    //  Create the intermediate control point.
                    pij.get(index + 1).set(j, ControlPoint.valueOf(p1.immutable(), wm * pjjw));

                    index += 2;
                    if (i < narcs) {
                        p0.set(p2);
                        t0 = t2;
                    }
                }
            }

            //  Turn the list into a network of control points for the surface.
            ControlPointNet cpnet = ControlPointNet.valueOf(pij);

            //  Create the knot vector around the revolution.
            FastTable<Float64> tKnots = FastTable.newInstance();
            int j = 3 + 2 * (narcs - 1);
            tKnots.setSize(j + 3);
            for (int i = 0; i < 3; i++) {
                tKnots.set(i, Float64.ZERO);
                tKnots.set(i + j, Float64.ONE);
            }
            switch (narcs) {
                case 2:
                    tKnots.set(3, Float64.valueOf(0.5));
                    tKnots.set(4, Float64.valueOf(0.5));
                    break;
                case 3:
                    tKnots.set(3, Float64.valueOf(1. / 3.));
                    tKnots.set(4, Float64.valueOf(1. / 3.));
                    tKnots.set(5, Float64.valueOf(2. / 3.));
                    tKnots.set(6, Float64.valueOf(2. / 3.));
                    break;
                case 4:
                    tKnots.set(3, Float64.valueOf(0.25));
                    tKnots.set(4, Float64.valueOf(0.25));
                    tKnots.set(5, Float64.valueOf(0.5));
                    tKnots.set(6, Float64.valueOf(0.5));
                    tKnots.set(7, Float64.valueOf(0.75));
                    tKnots.set(8, Float64.valueOf(0.75));
                    break;
            }

            //  Create the NURBS surface.
            KnotVector kvt = KnotVector.newInstance(2, Float64Vector.valueOf(tKnots));
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpnet, curve.getKnotVector(), kvt);
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create a surface that interpolates, is skinned or lofted, through the input list of
     * curves. This creates a surface in NURBS form that passes through the input list of
     * curves to within the specified tolerance. The degreeU in the parametric "s"
     * direction (along the input curves) will be determined by the highest degreeU input
     * curve. The degreeU in the "t" direction (across the input curves) is specified. If
     * the input curves are all NURBS curves, then the "tol" parameter is ignored.
     * Otherwise, "tol" is used to make a NURBS approximation of the input curves. It is
     * assumed that all the input curves are oriented in an appropriate direction and
     * order to create the desired skinned surface.
     *
     * @param crvs     A list of curves to be skinned. May not be null.
     * @param tDegree The degreeU in the t-direction of the NURBS surface to create (must
     *                be &gt; 0 and &lt; the number of curves in the list).
     * @param tol     The greatest possible difference between the input list of curves
     *                and the NURBS surface returned. May not be null or zero.
     * @return A BasicNurbsSurface that interpolates the input list of curves.
     */
    public static BasicNurbsSurface createSkinnedSurface(List<? extends Curve> crvs, int tDegree, Parameter<Length> tol) {
        requireNonNull(tol);
        int numCrvs = crvs.size();
        if (numCrvs < 2)
            throw new IllegalArgumentException(RESOURCES.getString("skinnedNotEnoughCrvs"));
        if (tDegree >= numCrvs)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("incDefiningCrvCount"), "skinned surface", tDegree + 1, numCrvs));

        //StackContext.enter();
        try {

            //  We have to create a list of compatible NURBS curves to create a NURBS surface.
            GeomList<NurbsCurve> newCrvs = generateCompatibleCurves(crvs, tol);

            //  Extact the S knot vector (in common to all the compatible curves).
            KnotVector sKnots = newCrvs.getFirst().getKnotVector();

            //  Extract the control point network.
            FastTable<FastTable<ControlPoint>> crvCParr = FastTable.newInstance();
            for (int i = 0; i < numCrvs; ++i) {
                NurbsCurve crv = newCrvs.get(i);
                crvCParr.add((FastTable<ControlPoint>)crv.getControlPoints());
            }
            int numCPs = crvCParr.get(0).size();

            //  Create a knot vector in the T direction by averaging the chord-type
            //  distance between the curve control points.
            double[] d = ArrayFactory.DOUBLES_FACTORY.array(numCPs);
            for (int i = 0; i < numCPs; ++i) {
                d[i] = 0;
                for (int k = 1; k < numCrvs; ++k) {
                    ControlPoint Qk = crvCParr.get(k).get(i);
                    ControlPoint Qkm1 = crvCParr.get(k - 1).get(i);
                    double distV = Qk.distance(Qkm1);
                    d[i] += distV;
                }
            }
            double[] tParams = ArrayFactory.DOUBLES_FACTORY.array(numCrvs);
            tParams[0] = 0;
            for (int k = 1; k < numCrvs; ++k) {
                tParams[k] = 0;
                for (int i = 0; i < numCPs; ++i) {
                    ControlPoint Qk = crvCParr.get(k).get(i);
                    ControlPoint Qkm1 = crvCParr.get(k - 1).get(i);
                    double distV = Qk.distance(Qkm1);
                    tParams[k] += distV / d[i];
                }
                tParams[k] /= numCPs;
                tParams[k] += tParams[k - 1];
            }
            tParams[numCrvs - 1] = 1;

            KnotVector tKnots = CurveFactory.buildInterpKnotVector(tParams, numCrvs, tDegree);

            //  Convert to the surface representation.
            FastTable<FastTable<ControlPoint>> newCPLst = FastTable.newInstance();
            FastTable<ControlPoint> pnts = FastTable.newInstance();
            for (int i = 0; i < numCPs; ++i) {

                //  Get the control points on each definining section.
                pnts.clear();
                for (int k = 0; k < numCrvs; ++k) {
                    pnts.add(crvCParr.get(k).get(i));
                }

                //  Fit a curve to the control points in the T-direction.
                BasicNurbsCurve iCrv = CurveFactory.fitControlPoints(tDegree, pnts, tParams, tKnots);

                //  Extract the control points of the i-curve.
                FastTable<ControlPoint> newCPs = (FastTable<ControlPoint>)iCrv.getControlPoints();

                //  Save off the new control points.
                newCPLst.add(newCPs);
            }

            //  Create the control point network for the surface.
            ControlPointNet cpNet = ControlPointNet.valueOf(newCPLst).transpose();

            //  Create the surface.
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpNet, sKnots, tKnots);
            return srf; //StackContext.outerCopy(srf);

        } finally {
            //StackContext.exit();
        }
    }

    /**
     * Generate and return a list of NURBS surface compatible curves from the supplied
     * list of curves.
     */
    private static GeomList<NurbsCurve> generateCompatibleCurves(List<? extends Curve> crvs, Parameter<Length> tol) {

        //  Convert all the section curves to NURBS curves first.
        int maxDeg = 0;
        GeomList<NurbsCurve> lst = GeomList.newInstance();
        int numCrvs = crvs.size();
        for (int i = 0; i < numCrvs; ++i) {
            Curve crv = crvs.get(i);

            //  Convert member curve to NURBS.
            NurbsCurve nCrv = crv.toNurbs(tol);
            lst.add(nCrv);

            //  Find the highest degreeU among the member curves.
            if (nCrv.getDegree() > maxDeg)
                maxDeg = nCrv.getDegree();
        }

        //  Raise all the section curves to the same degreeU.
        GeomList<NurbsCurve> nCrvs = GeomList.newInstance();
        for (int i = 0; i < numCrvs; ++i) {
            NurbsCurve crv = lst.get(i);
            nCrvs.add(CurveUtils.elevateToDegree(crv, maxDeg));
        }

        //  Merge the knot vectors of all the section curves into a Union knot vector.
        Float64Vector U = nCrvs.get(0).getKnotVector().getAll();
        for (int i = 1; i < numCrvs; ++i) {
            Float64Vector Ui = nCrvs.get(i).getKnotVector().getAll();
            U = CurveUtils.knotVectorUnion(U, Ui);
        }

        //  Merge the union knot vector with the knot vector of each section curve.
        GeomList<NurbsCurve> compCrvs = GeomList.newInstance();
        for (int i = 0; i < numCrvs; ++i) {
            NurbsCurve crv = nCrvs.get(i);
            NurbsCurve ncrv = crv.mergeKnotVector(U);
            compCrvs.add(ncrv);
        }

        //  Finally, convert all the curves to the same units.
        Unit<Length> refUnit = compCrvs.get(0).getUnit();
        GeomList<NurbsCurve> outCrvs = GeomList.newInstance();
        outCrvs.add(compCrvs.get(0));
        for (int i = 1; i < numCrvs; ++i)
            outCrvs.add(compCrvs.get(i).to(refUnit));

        return outCrvs;
    }

    /**
     * Create a Transfinite Interpolation (TFI) NURBS surface, or Coons patch, from the
     * specified boundary curves. This creates a surface in NURBS form that uses the input
     * set of curves as boundaries on all sides to within the specified tolerance. The
     * degreeU in each parametric dimension will be determined by the highest degreeU of the
     * input curves. If the input curves are all NURBS curves, then the "tol" parameter is
     * ignored. Otherwise, "tol" is used to make a NURBS approximation of the input
     * curves. It is assumed that all the input curves are oriented in an appropriate
     * direction and order to create the desired TFI surface. The corner points are
     * checked for coincidence.
     *
     * @param s0crv The curve that represents the s=0 boundary of the TFI surface. May not
     *              be null.
     * @param t0crv The curve that represents the t=0 boundary of the TFI surface. May not
     *              be null.
     * @param s1crv The curve that represents the s=1 boundary of the TFI surface. May not
     *              be null.
     * @param t1crv The curve that represents the t=1 boundary of the TFI surface. May not
     *              be null.
     * @param tol   The greatest possible difference between the input list of curves and
     *              the NURBS surface returned. May not be null or zero.
     * @return A BasicNurbsSurface that forms a TFI surface from the input curves.
     */
    public static BasicNurbsSurface createTFISurface(Curve s0crv, Curve t0crv, Curve s1crv, Curve t1crv,
            Parameter<Length> tol) throws IllegalArgumentException, ParameterException {
        //  Reference:  Handbook of Grid Generation, Chapter 30, Section 30.3.8

        //StackContext.enter();
        try {
            //  Check to make sure all the corner points are properly coincident.
            Point p00 = s0crv.getRealPoint(0);
            Point p1 = t0crv.getRealPoint(0);
            if (p00.distance(p1).isGreaterThan(tol)) {
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("s00t00NotCoincident"),
                                p00.distance(p1).toString()));
            }
            Point p10 = t0crv.getRealPoint(1);
            p1 = s1crv.getRealPoint(0);
            if (p10.distance(p1).isGreaterThan(tol)) {
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("s10t01NotCoincident"),
                                p10.distance(p1).toString()));
            }
            Point p11 = s1crv.getRealPoint(1);
            p1 = t1crv.getRealPoint(1);
            if (p11.distance(p1).isGreaterThan(tol)) {
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("s11t11NotCoincident"),
                                p11.distance(p1).toString()));
            }
            Point p01 = t1crv.getRealPoint(0);
            p1 = s0crv.getRealPoint(1);
            if (p01.distance(p1).isGreaterThan(tol)) {
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("s01t10NotCoincident"),
                                p01.distance(p1).toString()));
            }

            //  Create a list for the two curves in the S direction.
            GeomList<Curve> sCrvs = GeomList.newInstance();
            sCrvs.add(t0crv, t1crv);

            //  Create a skinned intermediate surface in the S direction.
            NurbsSurface Ps = createSkinnedSurface(sCrvs, 1, tol);

            //  Create a list for the two curves in the T direction.
            GeomList<Curve> tCrvs = GeomList.newInstance();
            tCrvs.add(s0crv, s1crv);

            //  Create a skinned intermediate surface in the T direction.
            NurbsSurface Pt = createSkinnedSurface(tCrvs, 1, tol).transpose();

            //  Create a flat intermediate surface between the corner points.
            PointString<Point> str1 = PointString.valueOf(p00, p10);
            PointString<Point> str2 = PointString.valueOf(p01, p11);
            PointArray<Point> arr = PointArray.valueOf(str1, str2);
            NurbsSurface PsPt = fitPoints(1, 1, arr);

            //  Get the max degreeU in the s and t direcitons.
            int maxDegS = Ps.getSDegree();
            if (PsPt.getSDegree() > maxDegS)
                maxDegS = PsPt.getSDegree();
            if (Pt.getSDegree() > maxDegS)
                maxDegS = Pt.getSDegree();
            int maxDegT = Ps.getTDegree();
            if (PsPt.getTDegree() > maxDegT)
                maxDegT = PsPt.getTDegree();
            if (Pt.getTDegree() > maxDegT)
                maxDegT = Pt.getTDegree();

            //  Raise all the surfaces to the same degreeU in the S & T directions.
            PsPt = SurfaceUtils.elevateToDegree(PsPt, maxDegS, maxDegT);
            Ps = SurfaceUtils.elevateToDegree(Ps, maxDegS, maxDegT);
            Pt = SurfaceUtils.elevateToDegree(Pt, maxDegS, maxDegT);

            //  Merge the knot vectors of all the surfaces into a Union knot vector in each direction.
            Float64Vector Us = PsPt.getSKnotVector().getAll();
            Float64Vector Ui = Ps.getSKnotVector().getAll();
            Us = CurveUtils.knotVectorUnion(Us, Ui);
            Ui = Pt.getSKnotVector().getAll();
            Us = CurveUtils.knotVectorUnion(Us, Ui);

            Float64Vector Ut = PsPt.getTKnotVector().getAll();
            Ui = Ps.getTKnotVector().getAll();
            Ut = CurveUtils.knotVectorUnion(Ut, Ui);
            Ui = Pt.getTKnotVector().getAll();
            Ut = CurveUtils.knotVectorUnion(Ut, Ui);

            //  Merge the union knot vectors with the knot vectors of each surface.
            PsPt = PsPt.mergeSKnotVector(Us);
            PsPt = PsPt.mergeTKnotVector(Ut);
            Ps = Ps.mergeSKnotVector(Us);
            Ps = Ps.mergeTKnotVector(Ut);
            Pt = Pt.mergeSKnotVector(Us);
            Pt = Pt.mergeTKnotVector(Ut);

            //  Get the control point networks for the intermediate surfaces.
            ControlPointNet cpNet = Ps.getControlPoints();
            ControlPointNet cpNetPt = Pt.getControlPoints();
            ControlPointNet cpNetPsPt = PsPt.getControlPoints();

            //  Construct a new control point network where:
            //      Points:  P = Ps + Pt - PsPt
            //      Weights: w = Ws * Wt
            FastTable<FastTable<ControlPoint>> cpNetArr = FastTable.newInstance();
            int rows = cpNet.getNumberOfRows();
            int cols = cpNet.getNumberOfColumns();
            for (int t = 0; t < cols; ++t) {
                FastTable<ControlPoint> cpLst = FastTable.newInstance();
                cpNetArr.add(cpLst);
                for (int s = 0; s < rows; ++s) {
                    ControlPoint cp = cpNet.get(s, t);
                    double wPs = cp.getWeight();
                    ControlPoint cpPt = cpNetPt.get(s, t);
                    double wPt = cpPt.getWeight();
                    ControlPoint cpPsPt = cpNetPsPt.get(s, t);
                    cp = cp.plus(cpPt);
                    cp = cp.minus(cpPsPt);
                    cp = cp.changeWeight(wPs * wPt);
                    cpLst.add(cp);
                }
            }

            //  Construct a new surface with the new control point network.
            cpNet = ControlPointNet.valueOf(cpNetArr);
            BasicNurbsSurface newSrf = BasicNurbsSurface.newInstance(cpNet,
                    Ps.getSKnotVector(), Ps.getTKnotVector());

            return newSrf;  //StackContext.outerCopy(newSrf);

        } finally {
            //StackContext.exit();
        }
    }

    /**
     * Create a {@link NurbsSurface} of the specified degrees that is fit to,
     * interpolates, or passes through the specified array of geometry points in the input
     * order. This is a global interpolation of the points, meaning that if one of the
     * input points is moved, it will affect the entire surface, not just the local
     * portion near the point.
     *
     * @param sDegree The degreeU in the s-direction (down the strings of points) of the
     *                NURBS surface to create (must be &gt; 0 and &lt; the number of
     *                points in each string).
     * @param tDegree The degreeU in the t-direction (across the rows of strings) of the
     *                NURBS surface to create (must be &gt; 0 and &lt; the number of
     *                strings in the array).
     * @param points  The array of GeomPoint objects to pass the surface through:
     *                points[col idx][row idx] -> [t][s]. May not be null.
     * @return A BasicNurbsSurface fit to the input array of points.
     * @throws ParameterException if there is a problem with the parameterization
     * resulting from fitting a curve through the supplied points.
     * @see #approxPoints
     */
    public static BasicNurbsSurface fitPoints(int sDegree, int tDegree, PointArray<? extends GeomPoint> points) 
            throws IllegalArgumentException, ParameterException {
        int nt = points.size();
        if (nt < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("zeroLengthListErr"),
                            "points"));
        if (tDegree >= nt)
            throw new IllegalArgumentException(RESOURCES.getString("numColsLEQDegErr"));
        int ns = points.get(0).size();
        if (ns < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numGTZero"),
                            "number of rows", ns));
        if (sDegree >= ns)
            throw new IllegalArgumentException(RESOURCES.getString("numRowsLEPDegErr"));
        for (int i = 0; i < nt; ++i) {
            if (points.get(i).size() != ns)
                throw new IllegalArgumentException(RESOURCES.getString("numPointsInEachColErr"));
        }

        //StackContext.enter();
        try {
            //  Turn the points into an array of parameter values on the surface.
            double[][] st = parameterizeArray(points);
            
            //  Check for bad parameterization.
            CurveFactory.parameterizationCheck(ns, st[0]);
            CurveFactory.parameterizationCheck(nt, st[1]);

            //  Generate the knot vectors.
            KnotVector sKnots = CurveFactory.buildInterpKnotVector(st[0], ns, sDegree);
            KnotVector tKnots = CurveFactory.buildInterpKnotVector(st[1], nt, tDegree);

            //  Generate the matrix of control points.
            FastTable<FastTable<ControlPoint>> rv = FastTable.newInstance();
            for (int i = 0; i < ns; i++) {
                PointString tmp = PointString.newInstance();
                for (int l = 0; l < nt; l++) {
                    tmp.add(points.get(l).get(i));
                }

                NurbsCurve curve = CurveFactory.fitPoints(tDegree, tmp);
                rv.add((FastTable<ControlPoint>)curve.getControlPoints());
            }

            FastTable<FastTable<ControlPoint>> cp = FastTable.newInstance();
            for (int l = 0; l < nt; l++) {
                PointString tmp = PointString.newInstance();
                for (int i = 0; i < ns; i++) {
                    tmp.add(rv.get(i).get(l).getPoint());
                }

                NurbsCurve curve = CurveFactory.fitPoints(sDegree, tmp);
                cp.add((FastTable<ControlPoint>)curve.getControlPoints());
            }

            //  Create the surface.
            ControlPointNet cpNet = ControlPointNet.valueOf(cp);
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpNet, sKnots, tKnots);
            return srf; //StackContext.outerCopy(srf);

        } finally {
            //StackContext.exit();
        }
    }

    /**
     * Turn an array of points into an array of parameter values on a surface through
     * those points. The returned array was created using ArrayFactor.DOUBLES_FACTORY and
     * can be recycled.
     */
    private static double[][] parameterizeArray(PointArray<? extends GeomPoint> points) {
        //  This uses the Chord Length method which is described here:
        //      http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/INT-APP/PARA-chord-length.html

        Unit<Length> refUnit = points.get(0).get(0).getUnit();
        int rows = points.get(0).size();
        int n = rows - 1;
        int cols = points.size();
        int m = cols - 1;
        double[] sk = newZeroedDoubleArray(rows);           //  sk = new double[rows];
        double[] tk = newZeroedDoubleArray(cols);           //  tk = new double[cols];

        StackContext.enter();
        try {
            double[] cds = ArrayFactory.DOUBLES_FACTORY.array(rows * cols); //  cds = new double[rows*cols];

            int num = cols;
            sk[n] = 1;
            for (int l = 0; l < cols; ++l) {
                double total = 0;

                for (int k = 1; k < rows; k++) {
                    int km1 = k - 1;
                    Parameter<Length> distance = points.get(l).get(k).distance(points.get(l).get(km1));
                    double distV = distance.getValue(refUnit);
                    cds[k] = distV;
                    total += distV;
                }
                if (abs(total) <= EPSC) {
                    //  The points form a degenerate curve of zero length.

                    //  Just evenly space out parameters.
                    for (int k = 1; k < n; ++k) {
                        double s = (double)(k) / n;
                        sk[k] += s;
                    }

                } else {
                    //  The points form a curve of finite length.
                    double d = 0;
                    for (int k = 1; k < n; k++) {
                        d += cds[k];
                        sk[k] += d / total;
                    }
                }
            }
            if (num > 0) {
                for (int k = 1; k < n; k++) {
                    sk[k] /= num;
                }
            }

            num = rows;
            tk[m] = 1;
            for (int l = 0; l < rows; l++) {
                double total = 0;
                for (int k = 1; k < cols; k++) {
                    int km1 = k - 1;
                    Parameter<Length> distance = points.get(k).get(l).distance(points.get(km1).get(l));
                    double distV = distance.getValue(refUnit);
                    cds[k] = distV;
                    total += distV;
                }
                if (abs(total) <= EPSC) {
                    //  The points form a degenerate curve of zero length.

                    //  Just evenly space out parameters.
                    for (int k = 1; k < m; ++k) {
                        double t = (double)(k) / n;
                        tk[k] += t;
                    }

                } else {
                    //  The points form a curve of finite length.
                    double d = 0;
                    for (int k = 1; k < m; k++) {
                        d += cds[k];
                        tk[k] += d / total;
                    }
                }
            }
            if (num > 0) {
                for (int k = 1; k < m; k++) {
                    tk[k] /= num;
                }
            }

        } finally {
            StackContext.exit();
        }

        //  Create the output array of arrays.
        double[][] res = DOUBLEDOUBLE_FACTORY.array(2);                 //  res = new double[2][]
        res[0] = sk;
        res[1] = tk;

        return res;
    }

    /**
     * Create a {@link NurbsSurface} of the specified degrees that approximates, in a
     * least squares sense, the specified array of geometry points in the input order.
     * This is a global approximation of the points, meaning that if one of the input
     * points is moved, it will affect the entire surface, not just the local portion near
     * the point.
     *
     * @param sDegree The degreeU in the s-direction of the NURBS surface to create (must
     *                be &gt; 0 and &lt; the number of points in each string).
     * @param tDegree The degreeU in the t-direction of the NURBS surface to create (must
     *                be &gt; 0 and &lt; the number of strings in the array).
     * @param nS      The number of surface control points to use in the parametric S
     *                direction (must be &gt; 1 and &lt; the number of points in each
     *                string).
     * @param nT      The number of surface control points to use in the parametric T
     *                direction (must be &gt; 1 and &lt; the number of strings in the
     *                array).
     * @param points  The array of GeomPoint objects approximate a surface through:
     *                points[col idx][row idx] -> [t][s]. May not be null.
     * @return A BasicNurbsSurface approximating the input array of points.
     * @throws ParameterException if there is a problem with the parameterization that
     * results from approximating the supplied points.
     * @see #fitPoints
     */
    public static BasicNurbsSurface approxPoints(int sDegree, int tDegree, int nS, int nT,
            PointArray<? extends GeomPoint> points) throws IllegalArgumentException, ParameterException {
        int mt = points.size();
        if (mt < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("zeroLengthListErr"),
                            "points"));
        if (tDegree <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numGTZero"),
                            "degreeU in the T-direction", tDegree));
        if (tDegree >= mt)
            throw new IllegalArgumentException(RESOURCES.getString("numColsLEQDegErr"));
        int ms = points.get(0).size();
        if (ms < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numGTZero"),
                            "number of rows", ms));
        if (sDegree <= 0)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numGTZero"),
                            "degreeU in the S-direction", sDegree));
        if (sDegree >= ms)
            throw new IllegalArgumentException(RESOURCES.getString("numRowsLEPDegErr"));
        if (nS <= 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numPointsLEOneErr"), "S"));
        if (nT <= 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numPointsLEOneErr"), "T"));
        if (nS >= ms)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numDPLEnumCPErr"), "S"));
        if (nT >= mt)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("numDPLEnumCPErr"), "T"));
        for (int i = 0; i < mt; ++i) {
            if (points.get(i).size() != ms)
                throw new IllegalArgumentException(RESOURCES.getString("numPointsInEachColErr"));
        }

        StackContext.enter();
        try {
            //  Turn the points into an array of parameter values on the surface.
            double[][] st = parameterizeArray(points);
            
            //  Check the parameterizations for correctness.
            CurveFactory.parameterizationCheck(nS, st[0]);
            CurveFactory.parameterizationCheck(nT, st[1]);

            //  Generate the knot vectors.
            KnotVector sKnots = CurveFactory.buildApproxKnotVector(st[0], ms, nS, sDegree);
            KnotVector tKnots = CurveFactory.buildApproxKnotVector(st[1], mt, nT, tDegree);

            //  Generate the matrix of control points.
            FastTable<FastTable<ControlPoint>> rv = FastTable.newInstance();
            for (int i = 0; i < ms; i++) {
                PointString tmp = PointString.newInstance();
                for (int l = 0; l < mt; l++) {
                    tmp.add(points.get(l).get(i));
                }

                NurbsCurve curve = CurveFactory.approxPoints(tDegree, nT, tmp);
                rv.add((FastTable<ControlPoint>)curve.getControlPoints());
            }

            FastTable<FastTable<ControlPoint>> cp = FastTable.newInstance();
            for (int l = 0; l < nT; l++) {
                PointString tmp = PointString.newInstance();
                for (int i = 0; i < ms; i++) {
                    List<ControlPoint> cpLst = rv.get(i);
                    ControlPoint ctrlp = cpLst.get(l);
                    tmp.add(ctrlp.getPoint());
                }

                NurbsCurve curve = CurveFactory.approxPoints(sDegree, nS, tmp);
                cp.add((FastTable<ControlPoint>)curve.getControlPoints());
            }

            //  Create and return the surface.
            ControlPointNet cpNet = ControlPointNet.valueOf(cp);
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpNet, sKnots, tKnots);
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new double array with the values zeroed. The returned instance is created
     * using ArrayFactory.DOUBLES_FACTORY and may be recycled.
     */
    private static double[] newZeroedDoubleArray(int size) {
        double[] arr = ArrayFactory.DOUBLES_FACTORY.array(size);
        for (int i = 0; i < size; ++i)
            arr[i] = 0;
        return arr;
    }

    private static final ArrayFactory<double[][]> DOUBLEDOUBLE_FACTORY = new ArrayFactory<double[][]>() {
        @Override
        protected double[][] create(int size) {
            return new double[size][];
        }
    };

}
