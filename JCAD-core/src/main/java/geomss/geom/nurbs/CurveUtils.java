/**
 * CurveUtils -- Utility methods for working with NURBS curves.
 *
 * Copyright (C) 2009-2016, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 *
 * This source file is based on, but heavily modified from, the LGPL jgeom (Geometry
 * Library for Java) code by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import geomss.geom.*;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ImmortalContext;
import javolution.context.StackContext;
import static javolution.lang.MathLib.max;
import static javolution.lang.MathLib.min;
import javolution.util.FastTable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A collection of utility methods for working with NURBS curves.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Samuel Gerber, Date: May 14, 2009, Version 1.0.
 * @version August 4, 2016
 */
public final class CurveUtils {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    private static final Parameter<Angle> ANGULAR_TOLERANCE;

    static {
        ImmortalContext.enter();
        try {
            ANGULAR_TOLERANCE = Parameter.valueOf(0.01, SI.RADIAN);

        } finally {
            ImmortalContext.exit();
        }
    }

    /**
     * Elevate the degreeU of a curve to the specified degreeU. If the specified degreeU is
     * &le; the current degreeU of the curve, then no change is made and a reference to the
     * input curve is returned.
     *
     * @param curve  The curve to have the degreeU elevated. May not be null.
     * @param degree The desired degreeU for the curve to be elevated to.
     * @return The input NurbsCurve with the degreeU elevated to the specified degreeU.
     */
    public static NurbsCurve elevateToDegree(NurbsCurve curve, int degree) {
        int p = curve.getDegree();
        if (degree <= p)
            return curve;

        int t = degree - p;
        return degreeElevate(curve, t);
    }

    /**
     * Elevate the degreeU of a curve by the specified number of times.
     *
     * @param curve The curve to have the degreeU elevated. May not be null.
     * @param t     The number of times to elevate the degreeU.
     * @return The input NurbsCurve with the degreeU elevated by the specified amount.
     */
    public static NurbsCurve degreeElevate(NurbsCurve curve, int t) {
        //  This is A5.9 from The NURBS Book, pg. 206.
        requireNonNull(curve);
        if (t <= 0)
            return curve;

        StackContext.enter();
        try {
            Unit<Length> unit = curve.getUnit();
            int dim = curve.getPhyDimension();
            KnotVector U = curve.getKnotVector();
            List<ControlPoint> Pw = curve.getControlPoints();

            int n = Pw.size() - 1;
            int p = curve.getDegree();
            int m = n + p + 1;
            int ph = p + t;
            int ph2 = ph / 2;

            // Coefficients for degreeU elevating the Bezier segment
            double[][] bezalfs = allocate2DArray(ph + 1, p + 1);
            // pth-degreeU Bezier control points of the current segment
            ControlPoint[] bpts = CONTROLPOINTARRAY_FACTORY.array(p + 1);
            // (p+t)th-degreeU Bezier control points of the  current segment
            ControlPoint[] ebpts = CONTROLPOINTARRAY_FACTORY.array(ph + 1);
            // leftmost control points of the next Bezier segment
            ControlPoint[] Nextbpts = CONTROLPOINTARRAY_FACTORY.array(p - 1);
            // Knot instertion alphas
            double[] alphas = ArrayFactory.DOUBLES_FACTORY.array(p - 1);

            // Compute the binomial coefficients
            double[][] Bin = allocate2DArray(ph + 1, ph2 + 1);
            binomialCoef(Bin, ph + 1, ph2 + 1);

            // Compute Bezier degreeU elevation coefficients
            bezalfs[0][0] = bezalfs[ph][p] = 1.0;
            for (int i = 1; i <= ph2; i++) {
                double inv = 1.0 / Bin[ph][i];
                int mpi = min(p, i);
                for (int j = max(0, i - t); j <= mpi; j++) {
                    bezalfs[i][j] = inv * Bin[p][j] * Bin[t][i - j];
                }
            }

            for (int i = ph2 + 1; i < ph; i++) {
                int mpi = min(p, i);
                for (int j = max(0, i - t); j <= mpi; j++) {
                    bezalfs[i][j] = bezalfs[ph - i][p - j];
                }
            }

            // Allocate more control points and knot vector space than necessary
            ControlPoint[] Qw = CONTROLPOINTARRAY_FACTORY.array(Pw.size() + Pw.size() * t);
            double[] Uh = ArrayFactory.DOUBLES_FACTORY.array(Pw.size() + Pw.size() * t + ph + 1);

            int mh = ph;
            int kind = ph + 1;
            double ua = U.getValue(0);
            double ub;
            int r = -1;
            int oldr;
            int a = p;
            int b = p + 1;
            int cind = 1;
            int rbz, lbz;

            Qw[0] = Pw.get(0);
            for (int i = 0; i <= ph; i++) {
                Uh[i] = ua;
            }

            // Initialize the first Bezier segment
            for (int i = 0; i <= p; i++) {
                bpts[i] = Pw.get(i);
            }

            while (b < m) { // Big loop thru knot vector
                int i = b;
                //  while (b < m && U.getValue(b) == U.getValue(b+1))
                while (b < m && MathTools.isApproxEqual(U.getValue(b), U.getValue(b + 1))) {
                    ++b;
                }
                int mul = b - i + 1;
                mh += mul + t;
                ub = U.getValue(b);
                oldr = r;
                r = p - mul;
                //  Insert knot u(b) r times
                if (oldr > 0)
                    lbz = (oldr + 2) / 2;
                else
                    lbz = 1;
                if (r > 0)
                    rbz = ph - (r + 1) / 2;
                else
                    rbz = ph;
                if (r > 0) {
                    // Insert knot to get Bezier segment
                    double numer = ub - ua;
                    for (int k = p; k > mul; k--) {
                        alphas[k - mul - 1] = numer / (U.getValue(a + k) - ua);
                    }
                    for (int j = 1; j <= r; j++) {
                        int save = r - j;
                        int s = mul + j;
                        for (int k = p; k >= s; k--) {
                            //  bpts[k] = alphas[k-s]*bpts[k] + (1.0-alphas[k-s])*bpts[k-1];
                            bpts[k] = applyAlpha(bpts[k], bpts[k - 1], alphas[k - s]);
                        }
                        Nextbpts[save] = bpts[p];
                    }
                }   //  End of inert knot

                for (i = lbz; i <= ph; i++) { // Degree elevate Bezier,  only the points lbz,...,ph are used
                    ebpts[i] = ControlPoint.newInstance(dim, unit); //  ebpts[i] = 0.0;
                    int mpi = min(p, i);
                    for (int j = max(0, i - t); j <= mpi; j++) {
                        //  ebpts[i] += bezalfs(i,j)*bpts[j]
                        ControlPoint q1 = bpts[j].applyWeight();
                        q1 = q1.times(bezalfs[i][j]);
                        ControlPoint q2 = ebpts[i].applyWeight();
                        q2 = q2.plus(q1);
                        ebpts[i] = q2.getHomogeneous();
                    }
                }   //  End of degreeU elevating Bezier.

                if (oldr > 1) { // Must remove knot u=U[a] oldr times
                    // if(oldr>2) // Alphas on the right do not change
                    //  alfj = (ua-U[kind-1])/(ub-U[kind-1]);
                    int first = kind - 2;
                    int last = kind;
                    double den = ub - ua;
                    double bet = (ub - Uh[kind - 1]) / den;
                    for (int tr = 1; tr < oldr; tr++) { // Knot removal loop
                        i = first;
                        int j = last;
                        int kj = j - kind + 1;
                        while (j - i > tr) { // Loop and compute the new control points for one removal step
                            if (i < cind) {
                                double alf = (ub - Uh[i]) / (ua - Uh[i]);
                                //  Qw[i] = alf*Qw[i] + (1.0-alf)*Qw[i-1]
                                Qw[i] = applyAlpha(Qw[i], Qw[i - 1], alf);
                            }
                            if (j >= lbz) {
                                if (j - tr <= kind - ph + oldr) {
                                    double gam = (ub - Uh[j - tr]) / den;
                                    //  ebpts[kj] = gam*ebpts[kj] + (1.0-gam)*ebpts[kj+1];
                                    ebpts[kj] = applyAlpha(ebpts[kj], ebpts[kj + 1], gam);

                                } else {
                                    //  ebpts[kj] = bet*ebpts[kj]+(1.0-bet)*ebpts[kj+1];
                                    ebpts[kj] = applyAlpha(ebpts[kj], ebpts[kj + 1], bet);
                                }
                            }
                            ++i;
                            --j;
                            --kj;
                        }
                        --first;
                        ++last;
                    }
                }   //  End removing knot u=U[a]

                if (a != p) // load the knot u=U[a]
                    for (i = 0; i < ph - oldr; i++) {
                        Uh[kind++] = ua;
                    }
                for (int j = lbz; j <= rbz; j++) { // load control points onto the curve
                    Qw[cind++] = ebpts[j];
                }

                if (b < m) {
                    System.arraycopy(Nextbpts, 0, bpts, 0, r);
                    for (int j = r; j <= p; j++) {
                        bpts[j] = Pw.get(b - p + j);
                    }
                    a = b;
                    ++b;
                    ua = ub;

                } else {
                    for (i = 0; i <= ph; i++) {
                        Uh[kind + i] = ub;
                    }
                }
            }   //  End of while-loop (b < m)

            //  Create the final lists of control points and knot vector.
            FastTable<ControlPoint> newP = FastTable.newInstance();
            n = mh - ph;
            for (int i = 0; i < n; ++i) {
                newP.add(Qw[i]);
            }

            FastTable<Float64> newKVList = FastTable.newInstance();
            m = n + ph + 1;
            for (int i = 0; i < m; ++i) {
                newKVList.add(Float64.valueOf(Uh[i]));
            }
            KnotVector newKV = KnotVector.newInstance(ph, Float64Vector.valueOf(newKVList));

            //  Create the new curve.
            BasicNurbsCurve newCrv = BasicNurbsCurve.newInstance(newP, newKV);
            return StackContext.outerCopy(newCrv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Decompose a NURBS curve into a list of Bezier segments. The returned list contains
     * NURBS curves each of which represents a single Bezier segment.
     *
     * @param curve The curve to be decomposed into Bezier segments. May not be null.
     * @return A list of Bezier curve segments (will be at least degreeU = 2).
     */
    public static GeomList<BasicNurbsCurve> decomposeToBezier(NurbsCurve curve) {
        //  Algorithm A5.6, pg. 173, Ref. 1.

        GeomList<BasicNurbsCurve> output = GeomList.newInstance();    //  Created outside of StackContext.
        StackContext.enter();
        try {
            //  Make sure the curve degreeU is > 1.
            if (curve.getDegree() == 1)
                curve = degreeElevate(curve, 1);

            List<ControlPoint> Pw = curve.getControlPoints();
            KnotVector SP = curve.getKnotVector();
            int numKnots = SP.length();
            int p = SP.getDegree();

            FastTable<FastTable<ControlPoint>> Qw = FastTable.newInstance();
            FastTable<ControlPoint> Qwnb = FastTable.newInstance();
            Qw.add(Qwnb);
            for (int i = 0; i <= p; ++i) {
                Qwnb.add(Pw.get(i));
            }

            int m = numKnots - 1;
            int a = p;
            int b = p + 1;
            int nb = 0;
            while (b < m) {
                int i = b;
                // while (b < m && SP[b] == SP[b+1])
                while (b < m && MathTools.isApproxEqual(SP.getValue(b), SP.getValue(b + 1))) {
                    ++b;
                }
                int mult = b - i + 1;
                if (mult < p) {
                    double numer = SP.getValue(b) - SP.getValue(a); //  Numerator of alpha.

                    //  Compute and store alphas.
                    double[] alphas = ArrayFactory.DOUBLES_FACTORY.array(p - mult);
                    for (int j = p; j > mult; --j) {
                        alphas[j - mult - 1] = numer / (SP.getValue(a + j) - SP.getValue(a));
                    }

                    //  Insert knot r times.
                    int r = p - mult;
                    for (int j = 1; j <= r; ++j) {
                        int save = r - j;
                        int si = mult + j;  //  This many new points.
                        for (int k = p; k >= si; --k) {
                            double alpha = alphas[k - si];
                            //  Qwnb[k] = alpha*Qwnb[k] + (1.0 - alpha)*Qwnb[k-1];
                            Qwnb.set(k, applyAlpha(Qwnb.get(k), Qwnb.get(k - 1), alpha));
                        }
                        if (b < numKnots) {
                            //  Control point of next segment.
                            if (Qw.size() <= nb + 1) {
                                FastTable<ControlPoint> tmp = FastTable.newInstance();
                                Qw.add(tmp);
                            }
                            FastTable<ControlPoint> Qwnbp1 = Qw.get(nb + 1);
                            if (Qwnbp1.size() <= save)
                                Qwnbp1.setSize(save + 1);
                            Qwnbp1.set(save, Qwnb.get(p));
                        }
                    }
                    ArrayFactory.DOUBLES_FACTORY.recycle(alphas);

                    //  Bezier segment completed.
                    ++nb;
                    if (Qw.size() <= nb) {
                        FastTable tmp = FastTable.newInstance();
                        tmp.setSize(p + 1);
                        Qw.add(tmp);
                    }
                    Qwnb = Qw.get(nb);
                    if (b < numKnots) {
                        //  Initialize for next segment.
                        if (Qwnb.size() <= p)
                            Qwnb.setSize(p + 1);
                        for (i = p - mult; i <= p; ++i) {
                            Qwnb.set(i, Pw.get(b - p + i));
                        }
                        a = b;
                        ++b;
                    }
                }
            }

            //  Turn the list of lists of control points into a list of curves.
            //  Build up a knot vector for the new Bezier segments.
            KnotVector bezierKV = bezierKnotVector(p);

            int size = Qw.size();
            for (int i = 0; i < size; ++i) {
                FastTable<ControlPoint> cpLst = Qw.get(i);
                BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cpLst, bezierKV);
                output.add(StackContext.outerCopy(crv));        //  "output" is outside of StackContext.
            }

        } finally {
            StackContext.exit();
        }

        return output;
    }

    /**
     * Returns a list containing the parameters at the start (and end) of each Bezier
     * segment of the specified NURBS curve. This first element of this list will always
     * be zero and the last will always be 1.
     *
     * @param crv The NURBS curve to return the Bezier segment start parameters for. May
     *            not be null.
     * @return A list containing the parameters of the start and end of each Bezier
     *         segment.
     */
    public static FastTable<Double> getBezierStartParameters(NurbsCurve crv) {

        //  Extract the knot vector and curve degreeU.
        KnotVector kv = crv.getKnotVector();
        int degree = kv.getDegree();

        //  Create a list of interior knotsU.
        FastTable<Double> knots = FastTable.newInstance();
        int size = kv.length() - degree;
        double oldS = -1;
        for (int i = degree; i < size; ++i) {
            double s = kv.getValue(i);
            if (s != oldS) {
                knots.add(s);
                oldS = s;
            }
        }

        return knots;
    }

    /**
     * Return a knot vector that can be used to create a Bezier curve segment of the
     * specified degreeU using the BasicNurbsCurve class.
     *
     * @param degree The degreeU of the knot vector to return.
     * @return A knot vector for creating a Bezier curve segment of the specified degreeU.
     */
    public static KnotVector bezierKnotVector(int degree) {
        return KnotVector.bezierKnotVector(degree);
    }

    /**
     * Method that applies the specified factor to the two specified breakpoints in the
     * form: <code>P = alpha*P1 + (1-alpha)*P2</code>.
     */
    private static ControlPoint applyAlpha(ControlPoint P1, ControlPoint P2, double alpha) {
        ControlPoint term1 = P1.times(alpha);
        ControlPoint term2 = P2.times(1.0 - alpha);
        ControlPoint P = term1.plus(term2);
        return P;
    }

    /**
     * Connect together or "concatenate" a list of curves end-to-start. The end of each
     * curve (u=1) is connected to the start (u=0) of the next curve. No check is made for
     * the curve ends being near each other. The resulting curve will have the physical
     * dimension of the highest dimension curve in the input list. The resulting curve
     * will have the degreeU of the highest degreeU curve in the input list.
     *
     * @param curves The list of curves to be connected together. May not be null.
     * @return A single {@link BasicNurbsCurve} made up of the input curves connected
     *         together end-to-start.
     * @throws IllegalArgumentException if there are less than 2 curves.
     */
    public static BasicNurbsCurve connectCurves(List<NurbsCurve> curves) throws IllegalArgumentException {
        //  Reference:  Yu, T., Soni, B., "Handbook of Grid Generation", Section 30.3.4

        int numCurves = curves.size();
        if (numCurves < 2) {
            throw new IllegalArgumentException(RESOURCES.getString("reqTwoCurves"));
        }

        StackContext.enter();
        try {
            //  Determine the highest degreeU and dimension curve in the list.
            int degree = 0;
            int dim = 0;
            for (int i = 0; i < numCurves; ++i) {
                NurbsCurve crv = curves.get(i);
                if (crv.getDegree() > degree) {
                    degree = crv.getDegree();
                }
                if (crv.getPhyDimension() > dim) {
                    dim = crv.getPhyDimension();
                }
            }

            //  Create a knot list and put a degreeU+1 touple knot at the start.
            FastTable<Float64> knots = FastTable.newInstance();
            for (int i = 0; i <= degree; i++) {
                knots.add(Float64.ZERO);
            }

            //  Create a list of control points.
            FastTable<ControlPoint> cps = FastTable.newInstance();

            //  Loop over all the curves in the input list.
            boolean firstPass = true;
            double w1 = 1;
            for (int i = 0; i < numCurves; ++i) {
                NurbsCurve crv = curves.get(i);

                //  Make sure the curve has the highest physical dimension.
                crv = crv.toDimension(dim);

                //  Elevate the curve to the highest degreeU (if required).
                crv = elevateToDegree(crv, degree);

                //  Get the knot vector for this curve.
                KnotVector u = crv.getKnotVector();
                int uLength = u.length();

                //  Add to the knot vector we are building.
                double start = knots.getLast().doubleValue() - u.getValue(0);
                int end = uLength - degree - 1;
                for (int j = degree + 1; j < end; j++) {
                    knots.addLast(Float64.valueOf(start + u.getValue(j)));
                }

                //  Add a degreeU+1 touple knot at the junction of the curves and at the end.
                Float64 value = Float64.valueOf(start + u.getValue(uLength - 1));
                for (int j = 0; j < degree; j++) {
                    knots.addLast(value);
                }

                //  Create the list of control points we are building.
                List<ControlPoint> pts = crv.getControlPoints();
                double wfactor = 1;
                if (firstPass) {
                    //  Add the 1st control point on the 1st curve only.
                    cps.addLast(pts.get(0));
                    firstPass = false;

                } else {
                    //  Determine the weight scaling factor for the next curve segment.
                    double w2 = pts.get(0).getWeight();
                    wfactor = w1 / w2;
                }

                //  Add the remaining control points while scaling the weights to match at the joint point.
                int numCP = pts.size();
                for (int j = 1; j < numCP; ++j) {
                    ControlPoint pt = pts.get(j);
                    double w2 = pt.getWeight();
                    pt = pt.changeWeight(w2 * wfactor);
                    cps.addLast(pt);
                }

                //  Keep the weight of the last point for use in the next curve.
                w1 = cps.getLast().getWeight();
            }
            knots.addLast(knots.getLast());

            //  Convert list of knot values into a KnotVector.
            Float64Vector knotsV = Float64Vector.valueOf(knots);
            knotsV = knotsV.times(1. / knots.getLast().doubleValue());  //  Normalize to a parameter length of 1.0.
            KnotVector kv = KnotVector.newInstance(degree, knotsV);

            //  Create and return the concatenated curve.
            BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kv);
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Connect together or "concatenate" a list of curves end-to-start. The end of each
     * curve (u=1) is connected to the start (u=0) of the next curve. No check is made for
     * the curve ends being near each other. The resulting curve will have the physical
     * dimension of the highest dimension curve in the input list. The resulting curve
     * will have the degreeU of the highest degreeU curve in the input list.
     *
     * @param curves The list of curves to be connected together. May not be null.
     * @return A single {@link BasicNurbsCurve} made up of the input curves connected
     *         together end-to-start.
     * @throws IllegalArgumentException if there are less than 2 curves.
     */
    public static BasicNurbsCurve connectCurves(NurbsCurve... curves)
            throws IllegalArgumentException {
        FastTable<NurbsCurve> list = FastTable.newInstance();
        list.addAll(Arrays.asList(requireNonNull(curves)));

        BasicNurbsCurve crv = connectCurves(list);

        return crv;
    }

    /**
     * Return a fillet curve between two curves. Parameter values indicate the points
     * between which the fillet is to be produced. The output curve will have the physical
     * dimension of the highest dimension of the two input curves.
     *
     * @param crv1   The 1st input curve. May not be null.
     * @param crv2   The 2nd input curve. May not be null.
     * @param eps    The geometry tolerance to use when determining if this a planar
     *               curve. If null is passed, then the curve would have be be exactly
     *               planar to be a circular or conic segment.
     * @param sEnd1  Parameter value on the first curve telling that the part of the curve
     *               lying on this side of sFil1 shall not be replaced by the fillet.
     * @param sFil1  Parameter value of the starting point of the fillet on the first
     *               curve.
     * @param sEnd2  Parameter value on the second curve telling that the part of the
     *               curve lying on this side of sFil2 shall not be replaced by the
     *               fillet.
     * @param sFil2  Parameter value of the starting point of the fillet on the second
     *               curve.
     * @param itype  Indicator of type of fillet. = 1 - Circle, interpolating point and
     *               tangent on first curve and the point on the 2nd curve, but not the
     *               tangent on curve 2. = 2 - Planar conic if possible else - A generic
     *               polynomial segment.
     * @param degree Degree of fillet curve to return.
     * @return A {@link BasicNurbsCurve} fillet curve.
     */
    @SuppressWarnings("null")
    public static BasicNurbsCurve createFillet(Curve crv1, Curve crv2, Parameter<Length> eps,
            double sEnd1, double sFil1, double sEnd2, double sFil2,
            int itype, int degree) {

        //  The points and tangents specified by the input parameter values
        //  are calculated, then the interpolation curve of type given by
        //  the  fillet indicator is calculated if possible. If a conic is
        //  specified and not possible to make, a cubic spline segment is
        //  made in stead.
        
        StackContext.enter();
        try {
            //  Convert both curves to the same physical dimension (the highest of the two).
            int dim = Math.max(crv1.getPhyDimension(), crv2.getPhyDimension());
            crv1 = crv1.toDimension(dim);
            crv2 = crv2.toDimension(dim);

            //  Get position and tangent of fillet point on the 1st curve.
            List<GeomVector<Length>> ders = crv1.getSDerivatives(sFil1, 1);
            GeomVector<Length> p1v = ders.get(0);
            Point p1 = p1v.getOrigin().copyToReal();
            GeomVector<Length> pu1 = ders.get(1);

            if (sEnd1 >= sFil1) {
                //  Reverse the direction of the tangent.
                pu1 = pu1.opposite();
            }

            //  Get position and tangent of fillet point on the 2nd curve.
            ders = crv2.getSDerivatives(sFil2, 1);
            GeomVector<Length> p2v = ders.get(0);
            Point p2 = p2v.getOrigin().copyToReal();
            GeomVector<Length> pu2 = ders.get(1);

            if (sEnd2 < sFil2) {
                //  Reverse the direction of the tangent.
                pu2 = pu2.opposite();
            }

            //  First, make sure p1 and p2 are not colocated.
            //  If they are colocated, return a degenerate curve.
            Parameter<Length> dend = p1.distance(p2);
            if (dend.isApproxZero())
                return StackContext.outerCopy(CurveFactory.createPoint(degree, p1));

            //  Test if the points and tangents describe a planar curve or do we have a 3D curve?
            Vector sdum = null;
            boolean plane = true;
            if (dim > 2) {
                //  Find deviation from a plane by projecting a vector from p1 to p2
                //  onto the normal formed by the cross product of the tangent vectors.
                //  If the project is anything other than zero, the points and tangent
                //  vectors are not in the same plane.
                sdum = pu1.cross(pu2).toUnitVector();
                Vector p1p2 = p1.minus(p2).toGeomVector();
                Parameter proj = p1p2.dot(sdum);
                if (proj.isLargerThan(eps))
                    plane = false;
            }

            //  Test if conic is in fact a circle.
            if (plane && itype != 1) {
                //  Project pu1 onto pu2.
                Parameter dum = pu1.dot(pu2);
                Parameter sum1 = pu1.mag();
                Parameter sum2 = pu2.mag();

                Parameter<Angle> sang;
                if (sum1.isApproxZero() || sum2.isApproxZero())
                    sang = Parameter.ZERO_ANGLE;
                else {
                    Parameter tcos = dum.divide(sum1.times(sum2));
                    tcos = tcos.min(Parameter.ONE);
                    sang = Parameter.acos(tcos);
                }

                if (Parameter.PI_ANGLE.minus(sang).isLessThan(ANGULAR_TOLERANCE)) {
                    //  Opposite parallel start and end tangents.
                    Vector norder1 = p2.minus(p1).toGeomVector();

                    if (norder1.angle(pu1).minus(Parameter.HALFPI_ANGLE).abs().isLessThan(ANGULAR_TOLERANCE))
                        itype = 1;

                } else if (sang.isLargerThan(ANGULAR_TOLERANCE)) {
                    //  Not parallel start and end tangents.

                    Vector norder1, norder2;
                    if (dim == 2) {
                        norder1 = Vector.valueOf(pu1.get(1), pu1.get(0).opposite()).toUnitVector();
                        norder2 = Vector.valueOf(pu2.get(1), pu2.get(0).opposite()).toUnitVector();

                    } else {
                        norder1 = sdum.cross(pu1);
                        norder2 = sdum.cross(pu2);
                    }

                    norder1 = norder1.minus(norder2);
                    norder2 = p2.minus(p1).toGeomVector();

                    if (norder1.angle(norder2).isLessThan(ANGULAR_TOLERANCE))
                        itype = 1;
                }
            }

            BasicNurbsCurve crv;
            if (plane && (itype == 2 || itype == 1)) {
                //  The curve is planar, interpolate with a conic.
                if (itype == 1)
                    //  Circular arc.
                    crv = CurveFactory.createCircularArc(p1, pu1, p2);
                else
                    //  Parabolic arc.
                    crv = CurveFactory.createParabolicArc(p1, pu1.toUnitVector(), p2, pu2.toUnitVector());

            } else {
                //  Polynomial fillet.
                crv = CurveFactory.createBlend(p1, pu1.toUnitVector(), p2, pu2.toUnitVector(), true, 0.5);
            }

            //  Elevate to the desired degreeU.
            BasicNurbsCurve output = (BasicNurbsCurve)elevateToDegree(crv, degree);
            return StackContext.outerCopy(output);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a circular fillet curve between two planar non-parallel lines. The output
     * curve will have the physical dimension of the highest dimension of the two input
     * lines. The corner where the fillet is applied depends on the directions of the
     * input lines. It is the corner that is to the left of each input line when standing
     * at the start of the line and looking toward the end of the line.
     *
     * @param p1     The 1st end of the 1st input line. May not be null.
     * @param p2     The 2nd end of the 1st input line. May not be null.
     * @param p3     The 1st end of the 2nd input line. May not be null.
     * @param p4     The 2nd end of the 2nd input line. May not be null.
     * @param radius The radius of the circular fillet. May not be null.
     * @param eps    The geometry tolerance to use in determining if the input line
     *               segments are co-planar. If null is passed, the input lines must be
     *               exactly co-planar.
     * @return A {@link BasicNurbsCurve} circular arc fillet curve.
     */
    public static BasicNurbsCurve createFillet(GeomPoint p1, GeomPoint p2, GeomPoint p3, GeomPoint p4,
            Parameter<Length> radius, Parameter<Length> eps) {
        requireNonNull(p1);
        requireNonNull(p2);
        requireNonNull(p3);
        requireNonNull(p4);
        requireNonNull(radius);

        //  Convert both lines to the same physical dimension (the highest of the two).
        int dim = GeomUtil.maxPhyDimension(p1, p2, p3, p4);
        if (dim < 2)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"),
                            "input points", dim));

        StackContext.enter();
        try {
            int numDims = dim;
            if (dim < 3)
                dim = 3;    //  Use a min. of 3 dimension in calculations.
            p1 = p1.toDimension(dim);
            p2 = p2.toDimension(dim);
            p3 = p3.toDimension(dim);
            p4 = p4.toDimension(dim);

            //  Make sure the lines are not degenerate.
            if (p1.distance(p2).isLessThan(eps) || p3.distance(p4).isLessThan(eps)) {
                throw new IllegalArgumentException(RESOURCES.getString("degLineInput"));
            }

            //  Extract the tangents of the two lines.
            Vector<Length> t1 = p2.toGeomVector().minus(p1.toGeomVector());
            Vector<Length> t2 = p4.toGeomVector().minus(p3.toGeomVector());

            //  Test if the points and tangents describe co-planar lines?
            //  Find deviation from a plane by projecting a vector from p1 to p3
            //  onto the normal formed by the cross product of the tangent vectors.
            //  If the project is anything other than zero, the points and tangent
            //  vectors are not in the same plane.
            Vector<Dimensionless> nhat = t1.cross(t2).toUnitVector();
            Vector p1p3 = p1.minus(p3).toGeomVector();
            if (p1p3.mag().isApproxZero())
                p1p3 = p1.minus(p4).toGeomVector();
            Parameter proj = p1p3.dot(nhat);
            if (proj.isLargerThan(eps))
                throw new IllegalArgumentException(RESOURCES.getString("nonCoPlanarLines"));

            //  Determine if the two lines are parallel.
            Parameter a = t1.dot(t1);
            Parameter b = t1.dot(t2);
            Parameter c = t2.dot(t2);
            Parameter denom = a.times(c).minus(b.times(b));     //  denom = a*c - b^2
            if (denom.isApproxZero())
                throw new IllegalArgumentException(RESOURCES.getString("parallelLinesInput"));

            //  Offset the 1st line by "radius" amount in the direction of the 2nd line.
            Vector<Dimensionless> yhat = nhat.cross(t1).toUnitVector();
            Point offset = Point.valueOf(yhat.times(radius));
            Point p1p = p1.plus(offset);

            //  Offset the 2nd line by "radius" amount in the direction of the 1st line.
            yhat = nhat.cross(t2).toUnitVector();
            offset = Point.valueOf(yhat.times(radius));
            Point p3p = p3.plus(offset);

            //  The intersection of the two lines is the center of the fillet arc.
            MutablePoint pc = MutablePoint.newInstance(dim, p1.getUnit());
            GeomUtil.lineLineIntersect(p1p, t1, p3p, t2, eps, null, pc, null);

            //  Find the point on the 1st line that is closest to the fillet arc center.
            Point pa = GeomUtil.pointLineClosest(pc, p1, t1.toUnitVector());

            //  Find the point on the 2nd line that is closest to the fillet arc center.
            Point pb = GeomUtil.pointLineClosest(pc, p3, t2.toUnitVector());

            //  Create a circular arc from the center and end points.
            BasicNurbsCurve crv = CurveFactory.createCircularArcO(pc.immutable(), pa, pb);

            crv = crv.toDimension(numDims); //  May need to convert to 2D at the end.
            return StackContext.outerCopy(crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a blending curve between two curve ends. Parameter values indicate which
     * ends of the input curves the blend curve is to touch. The output curve will be a
     * conic section if possible otherwise it will be a polynomial curve and will have the
     * physical dimension of the highest dimension of the two input curves.
     *
     * @param crv1   The 1st input curve. May not be null.
     * @param crv2   The 2nd input curve. May not be null.
     * @param eps    The geometry tolerance to use when determining if the output is a
     *               planar curve. If null is passed, then the curve would have be be
     *               exactly planar to be a circular or conic segment.
     * @param sEnd1  Parameter value on crv1 indicating which end of curve 1 should start
     *               the blend.
     * @param sEnd2  Parameter value on crv2 indicating which end of curve 2 should end
     *               the blend.
     * @param degree Degree of blend curve to return.
     * @return A {@link BasicNurbsCurve} blending curve between the input curves.
     */
    public static BasicNurbsCurve createBlend(Curve crv1, Curve crv2, Parameter<Length> eps,
            double sEnd1, double sEnd2, int degree) {
        requireNonNull(crv1);
        requireNonNull(crv2);

        //  Sort out end points.
        double fil1 = 1, end1 = 0;
        if (sEnd1 < 0.5) {
            fil1 = 0;
            end1 = 1;
        }
        double fil2 = 1, end2 = 0;
        if (sEnd2 < 0.5) {
            fil2 = 0;
            end2 = 1;
        }

        return createFillet(crv1, crv2, eps, end1, fil1, end2, fil2, 2, degree);
    }

    /**
     * Return a NurbsCurve, with the same degreeU as the input curve, which is an
     * approximate arc-length re-parameterization of the input NurbsCurve.
     *
     * @param crv The NurbsCurve to be approximately re-parameterized. May not be null.
     * @param tol The geometric tolerance to use when re-parameterizing the input curve.
     *  May not be null or zero.
     * @return The arc-length parameterized curve that is approximately the same as the
     *         input curve.
     */
    public static BasicNurbsCurve arcLengthParameterize(NurbsCurve crv, Parameter<Length> tol) {
        requireNonNull(tol);
        
        StackContext.enter();
        try {
            //  Create a list of equally spaced fractional arc-lengths to sample the curve at.
            int p = crv.getDegree();
            int numSegs = getBezierStartParameters(crv).size();
            if (numSegs > 25)
                //  Don't let it get carried away.
                numSegs = 25;
            FastTable<Double> parPos = (FastTable<Double>)GridSpacing.linear((p + 1) * numSegs);

            //  Extract a grid of points on the original curve using arc-length spacing.
            PointString<SubrangePoint> str = crv.extractGrid(GridRule.ARC, parPos);

            //  Fit a curve through the parametric positions of each arc-length position
            //  on the original curve.  This curve translates from arc-length spacing
            //  to the original parametric spacing.
            PointString<GeomPoint> pstr = PointString.newInstance();
            int size = str.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint pnt = str.get(i);
                pstr.add(pnt.getParPosition());
            }
            BasicNurbsCurve pcrv = CurveFactory.fitPoints(3, pstr);

            //  Create a subrange curve that maps the fractional arc-length positions
            //  to the original parametric positions.
            SubrangeCurve scrv = SubrangeCurve.newInstance(crv, pcrv);

            //  Convert the subrange curve back to a NURBS curve.
            str = scrv.gridToTolerance(tol);
            BasicNurbsCurve ncrv = CurveFactory.fitPoints(p, str);
            return StackContext.outerCopy(ncrv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Attempt to minimize the number of knotsU in the specified NURBS curve while
     * maintaining the same shape (to within tolerance) of the original curve.
     *
     * @param original The original curve to have the knotsU thinned to tolerance. May not be null.
     * @param tol      The allowable tolerance between the original curve and the curve
     *                 with the knotsU thinned. May not be null.
     * @return A curve that represents the original curve to within the given tolerance
     *         but with fewer knotsU. If no knotsU could be removed, the original curve is
     *         returned.
     */
    public static BasicNurbsCurve thinKnotsToTolerance(BasicNurbsCurve original, Parameter<Length> tol) {

        StackContext.enter();
        try {
            int degree = original.getDegree();
            int ki = degree + 1;

            //  Adjust the tolerance to conservatively make up for the multiple passes through
            //  refinement below (where errors get stacked up).
            Parameter<Length> tol2 = tol.divide(original.getKnotVector().length() * degree);

            //  Loop over each knot and try to remove it.
            NurbsCurve crv = original;
            while (ki < crv.getKnotVector().length() - degree - 1) {
                NurbsCurve ncrv = crv.removeKnot(ki, 3, tol2);
                if (crv == ncrv)
                    ++ki;
                crv = ncrv;
            }

            return StackContext.outerCopy(crv.immutable());
            
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Set up a matrix containing all the binomial coefficients up to the specified row
     * (n) and column (k) values.
     *
     * <pre>
     *    bin(i,j) = (i  j)
     *    The binomical coefficients are defined as follow:
     *       (n)         n!
     *      ( )  =  ------------
     *       (k)       k!(n-k)!       0&le;k&le;n
     *    and the following relationship applies:
     *    (n+1)     (n)   ( n )
     *    ( k ) =   (k) + (k-1)
     * </pre>
     *
     * @param bin  An existing matrix that will be filled in with the binomial
     *             coefficients.
     * @param rows The number of rows (1st index) of the matrix to compute (n).
     * @param cols The number of columns (2nd index) of the matrix to compute (k).
     */
    private static void binomialCoef(double[][] bin, int rows, int cols) {

        //  Make sure the array is initially zeroed.
        double[] row0 = bin[0];
        for (int j = 0; j < cols; ++j)
            row0[j] = 0;
        for (int i = 1; i < rows; ++i) {
            System.arraycopy(row0, 0, bin[i], 0, cols);
        }

        // Setup the first line
        row0[0] = 1.0;
        //for(int k = cols-1; k > 0; --k)
        //  bin[0][k] = 0.0;

        // Setup the other lines
        for (int n = 0; n < rows - 1; n++) {
            int np1 = n + 1;
            @SuppressWarnings("MismatchedReadAndWriteOfArray")
            double[] binnp1 = bin[np1];
            binnp1[0] = 1.0;

            double[] binn = bin[n];
            for (int k = 1; k < cols; k++) {
                if (np1 >= k)
                    binnp1[k] = binn[k] + binn[k - 1];
                //else
                //  bin[n][k] = 0.0;
            }
        }

    }

    /**
     * Return true if the supplied control point polygon is simple and convex.
     *
     * @param P The list of control points to evaluate. May not be null.
     * @return true if the control point polygon is simple and convex.
     */
    public static boolean isSimpleConvexPolygon(List<ControlPoint> P) {
        //  Algorithm 1 from Ma, Hewitt, "Point inversion and project for NURBS curve and surface", CAGD, 2003.

        StackContext.enter();
        try {
            int n = P.size() - 1;
            Point P0 = P.get(0).getPoint();
            Point Pn = P.get(n).getPoint();

            Point Pim1 = P.get(0).getPoint();
            Point Pi = P.get(1).getPoint();
            for (int i = 1; i < n; ++i) {
                Point Pip1 = P.get(i + 1).getPoint();

                //  Calculate project of Pi onto a line from P(i-1) to P(i+1).
                Point V1 = GeomUtil.pointLineClosest(Pi, Pim1, Pip1.minus(Pim1).toGeomVector().toUnitVector());

                //  Generate V1Pi vector.
                Vector<Length> V1Pi = Pi.minus(V1).toGeomVector();

                Parameter R;
                int no2 = n / 2;
                if (i < no2) {
                    Vector<Length> V1Pn = Pn.minus(V1).toGeomVector();
                    R = V1Pi.dot(V1Pn);

                } else {
                    Vector<Length> V1P0 = P0.minus(V1).toGeomVector();
                    R = V1Pi.dot(V1P0);
                }

                if (R.getValue() < -1e-14)      //  (R.getValue() < 0) accounting for typical roundoff errors.
                    //  The polygon is not a simple and convex one.
                    return false;

                //  Prepare for next loop.
                Pim1 = Pi;
                Pi = Pip1;
            }

            //  It is a simple and convex polygon
            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns <code>true</code> if an end point is the closest point in the specified
     * Bezier curve segment to the target point. Returns <code>false</code> if the closest
     * point may be interior to the curve segment.
     *
     * @param point The point to find the closest/farthest point on this curve to/from.
     *              May not be null.
     * @param P     The control point polygon for the Bezier curve. May not be null.
     * @return <code>true</code> if an end point of the specified segment is closest to
     *         the target point.
     */
    public static boolean isBezierEndPointNearest(GeomPoint point, List<ControlPoint> P) {
        //  Algorithm 3 from Ma, Hewitt, "Point inversion and project for NURBS curve and surface", CAGD, 2003
        //  (Logic reversed).

        StackContext.enter();
        try {
            //  Extract points we need from the control polygon.
            Point P0 = P.get(0).getPoint();
            Point P1 = P.get(1).getPoint();
            int n = P.size() - 1;
            Point Pnm1 = P.get(n - 1).getPoint();
            Point Pn = P.get(n).getPoint();

            //  Form vectors.
            Vector<Length> P0P = point.minus(P0).toGeomVector();
            Vector<Length> P0P1 = P1.minus(P0).toGeomVector();
            Vector<Length> PPn = Pn.minus(point).toGeomVector();
            Vector<Length> Pnm1Pn = Pn.minus(Pnm1).toGeomVector();
            Vector<Length> PnP0 = P0.minus(Pn).toGeomVector();
            Vector<Length> PnP = PPn.opposite();

            //  Calculate the dot products.
            Parameter R1 = P0P.dot(P0P1);
            Parameter R2 = PPn.dot(Pnm1Pn);
            Parameter R3 = PnP0.dot(PnP);
            Parameter R4 = PnP0.dot(P0P);

            return R1.getValue() < 0 || R2.getValue() < 0 && R3.times(R4).getValue() > 0;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the union between two knot vectors. This merges two knot vectors together to
     * obtain on combined knot vector. Warning: The results of this method are useless if
     * the knot vectors being merged are not from NURBS curves of the same degreeU.
     *
     * @param Ua The 1st knot vector to merge. May not be null.
     * @param Ub The 2nd knot vector to merge. May not be null.
     * @return The union of Ua and Ub.
     */
    public static Float64Vector knotVectorUnion(Float64Vector Ua, Float64Vector Ub) {

        StackContext.enter();
        try {
            int UaSize = Ua.getDimension();
            int UbSize = Ub.getDimension();
            FastTable<Float64> U = FastTable.newInstance();
            U.setSize(UaSize + UbSize);

            boolean done = false;
            int i = 0, ia = 0, ib = 0;
            while (!done) {
                double Uav = Ua.getValue(ia);
                double Ubv = Ub.getValue(ib);
                double t;
                if (Uav == Ubv) {
                    t = Uav;
                    ++ia;
                    ++ib;
                } else if (Uav < Ubv) {
                    t = Uav;
                    ++ia;
                } else {
                    t = Ubv;
                    ++ib;
                }
                U.set(i++, Float64.valueOf(t));
                done = (ia >= UaSize || ib >= UbSize);
            }
            U.setSize(i);

            //  Create the merged knot vector instance.
            Float64Vector Uv = Float64Vector.valueOf(U);
            return StackContext.outerCopy(Uv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Allocate a recyclable 2D array of double values using factory methods.
     * <p>
     * WARNING: The array returned may not be zeroed and may be larger than the requested
     * number of rows & columns! The array returned by this method can be recycled by
     * recycle2DArray().
     * </p>
     *
     * @param rows The minimum number of rows (1st index) for the returned array.
     * @param cols The minimum number of columns (2nd index) for the returned array.
     * @return A 2D array of doubles allocated using factory methods.
     * @see #recycle2DArray
     */
    public static double[][] allocate2DArray(int rows, int cols) {
        double[][] arr = DOUBLEARRAY_FACTORY.array(rows);
        for (int i = 0; i < rows; ++i) {
            arr[i] = ArrayFactory.DOUBLES_FACTORY.array(cols);
        }
        return arr;
    }

    /**
     * Recycle any 2D array of doubles that was created by allocate2DArray().
     *
     * @param arr The array to be recycled. The array must have been created by this the
     *            allocate2DArray() method! May not be null.
     * @see #allocate2DArray
     */
    public static void recycle2DArray(double[][] arr) {
        int length = arr.length;
        for (int i = 0; i < length; ++i) {
            if (arr[i] != null) {
                ArrayFactory.DOUBLES_FACTORY.recycle(arr[i]);
                arr[i] = null;
            }
        }
        DOUBLEARRAY_FACTORY.recycle(arr);
    }

    /**
     * Factory that creates recyclable arrays that can contain ControlPoint objects.
     */
    private static final ArrayFactory<ControlPoint[]> CONTROLPOINTARRAY_FACTORY = new ArrayFactory<ControlPoint[]>() {
        @Override
        protected ControlPoint[] create(int size) {
            return new ControlPoint[size];
        }
    };

    /**
     * Factory that creates recyclable arrays of doubles.
     */
    private static final ArrayFactory<double[][]> DOUBLEARRAY_FACTORY = new ArrayFactory<double[][]>() {
        @Override
        protected double[][] create(int size) {
            return new double[size][];
        }
    };
}
