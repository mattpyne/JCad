/*
 *   ControlPointNet  -- A network or matrix of control points for a NURBS surface in nD space.
 *
 *   Copyright (C) 2010-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.nurbs;

import geomss.geom.Point;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.ValueType;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;

/**
 * A network or matrix of control points for a NURBS surface in n-dimensional space.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 15, 2010
 * @version November 28, 2015
 */
@SuppressWarnings("serial")
public class ControlPointNet implements Iterable<List<ControlPoint>>, Cloneable, XMLSerializable, ValueType {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    // A list of lists of ControlPoint objects.
    private FastTable<FastTable<ControlPoint>> _matrix;

    /**
     * Returns a {@link ControlPointNet} instance made up of the control points in the
     * specified java matrix.
     *
     * @param cps Matrix of control points: cps[t][s]. s-parameter runs down a column of
     *            points and the t-parameter runs across the columns of points. May not be
     *            null.
     * @return A ControlPointNet object using the data in the specified matrix.
     */
    public static ControlPointNet valueOf(ControlPoint[][] cps) {
        requireNonNull(cps, MessageFormat.format(RESOURCES.getString("paramNullErr"), "cps"));
        int nt = cps.length;
        if (nt < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("zeroLengthListErr"), "cps"));
        int ns = cps[0].length;
        if (ns < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("zeroLengthListErr"), "cps[0]"));
        for (int t = 1; t < nt; ++t) {
            if (cps[t].length != ns)
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("irregMatrixErr"), "cps"));
        }

        Unit<Length> refUnit = cps[0][0].getUnit();
        ControlPointNet P = FACTORY.object();
        P._matrix = FastTable.newInstance();
        for (int t = 0; t < nt; ++t) {
            FastTable<ControlPoint> tbl = FastTable.newInstance();
            for (int s = 0; s < ns; ++s) {
                tbl.add(cps[t][s].to(refUnit));
            }
            P._matrix.add(tbl);
        }

        return P;
    }

    /**
     * Returns a {@link ControlPointNet} instance made up of the control points in the
     * specified list of lists.
     *
     * @param cps List of lists (matrix) of control points: cps.get(t).get(s). s-parameter
     *            runs down a column of points and the t-parameter runs across the columns
     *            of points. May not be null.
     * @return A ControlPointNet object using the data in the specified matrix.
     */
    public static ControlPointNet valueOf(List<? extends List<ControlPoint>> cps) {
        requireNonNull(cps, MessageFormat.format(RESOURCES.getString("paramNullErr"), "cps"));
        int nt = cps.size();
        if (nt < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("zeroLengthListErr"), "cps"));
        int ns = cps.get(0).size();
        if (ns < 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("zeroLengthListErr"), "cps.get(0)"));
        for (int t = 1; t < nt; ++t) {
            if (cps.get(t).size() != ns)
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("irregMatrixErr"), "cps"));
        }

        Unit<Length> refUnit = cps.get(0).get(0).getUnit();
        ControlPointNet P = FACTORY.object();
        P._matrix = FastTable.newInstance();
        for (int t = 0; t < nt; ++t) {
            FastTable<ControlPoint> tbl = FastTable.newInstance();
            int size = cps.get(t).size();
            for (int i = 0; i < size; ++i) {
                ControlPoint cp = cps.get(t).get(i);
                tbl.add(cp.to(refUnit));
            }
            P._matrix.add(tbl);
        }

        return P;
    }

    /**
     * Returns a {@link ControlPointNet} instance made up of the control points contained
     * in the specified network.
     *
     * @param cpNet An existing control point net. The control points from this network
     *              are used to make this one. May not be null.
     * @return A ControlPointNet object using the data in the specified network.
     */
    public static ControlPointNet valueOf(ControlPointNet cpNet) {
        requireNonNull(cpNet, MessageFormat.format(RESOURCES.getString("paramNullErr"), "cpNet"));
        ControlPointNet P = FACTORY.object();
        P._matrix = FastTable.newInstance();
        int ncols = cpNet.getNumberOfColumns();
        for (int i = 0; i < ncols; ++i) {
            List<ControlPoint> column = cpNet.getColumn(i);
            FastTable<ControlPoint> tbl = FastTable.newInstance();
            tbl.addAll(column);
            P._matrix.add(tbl);
        }
        return P;
    }

    /**
     * Returns the total number of control points in this matrix of control points.
     *
     * @return The total number of control points in this network.
     */
    public int size() {
        return _matrix.size() * _matrix.get(0).size();
    }

    /**
     * Return the control point matrix size in the s-direction (down a column of control
     * points).
     *
     * @return The number of rows in this control point network.
     */
    public int getNumberOfRows() {
        return _matrix.get(0).size();
    }

    /**
     * Return the control point matrix size in the t-direction (across the columns of
     * control points).
     *
     * @return The number of columns in this control point network.
     */
    public int getNumberOfColumns() {
        return _matrix.size();
    }

    /**
     * Returns the ControlPoint at the specified s,t position in this matrix.
     *
     * @param s the index in the s-direction (down a column of points).
     * @param t the index in the t-direction (across the columns of points).
     * @return the control point at the s,t position in this matrix.
     * @throws IndexOutOfBoundsException <code>(s &lt; 0) || (s &ge;
     * getNumberOfRows()) || t &lt; 0 || (t &ge; getNumberOfColumns()) )</code>
     */
    public ControlPoint get(int s, int t) {
        return _matrix.get(t).get(s);
    }

    /**
     * Returns a list of ControlPoint objects that represent a single row in this network
     * of control points.
     *
     * @param sIndex The index for the row of control points to return.
     * @return The specified row of control points.
     * @throws IndexOutOfBoundsException <code>sIndex &lt; 0 || (sIndex &ge;
     * getNumberOfRows()) )</code>
     */
    public List<ControlPoint> getRow(int sIndex) {
        FastTable<ControlPoint> list = FastTable.newInstance();
        int ncol = _matrix.size();
        for (int i = 0; i < ncol; ++i) {
            List<ControlPoint> column = _matrix.get(i);
            list.add(column.get(sIndex));
        }
        return list;
    }

    /**
     * Returns a list of ControlPoint objects that represent a single column in this
     * network of control points.
     *
     * @param tIndex The index for the column of control points to return.
     * @return The specified column of control points.
     * @throws IndexOutOfBoundsException <code>tIndex &lt; 0 || (tIndex &ge;
     * getNumberOfColumns()) )</code>
     */
    public List<ControlPoint> getColumn(int tIndex) {
        FastTable<ControlPoint> list = FastTable.newInstance();
        list.addAll(_matrix.get(tIndex));
        return list;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z) of this matrix of ControlPoint objects.
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    public Point getBoundsMin() {
        StackContext.enter();
        try {
            Point minPoint = get(0, 0).getBoundsMin();

            int ncol = _matrix.size();
            for (int i = 0; i < ncol; ++i) {
                List<ControlPoint> tbl = _matrix.get(i);
                int nrow = tbl.size();
                for (int j = 0; j < nrow; ++j) {
                    ControlPoint cp = tbl.get(j);
                    minPoint = minPoint.min(cp.getBoundsMin());
                }
            }
            return StackContext.outerCopy(minPoint);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z) of this matrix of ControlPoint objects.
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    public Point getBoundsMax() {
        StackContext.enter();
        try {
            Point maxPoint = get(0, 0).getBoundsMax();

            int ncol = _matrix.size();
            for (int i = 0; i < ncol; ++i) {
                List<ControlPoint> tbl = _matrix.get(i);
                int nrow = tbl.size();
                for (int j = 0; j < nrow; ++j) {
                    ControlPoint cp = tbl.get(j);
                    maxPoint = maxPoint.max(cp.getBoundsMax());
                }
            }
            return StackContext.outerCopy(maxPoint);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new control point network that is the transpose of this network (the rows
     * & columns are swapped).
     *
     * @return A new ControlPointNet identical to this one but with the rows and columns
     *         transposed.
     */
    public ControlPointNet transpose() {
        //StackContext.enter();
        try {
            FastTable<FastTable<ControlPoint>> nMat = FastTable.newInstance();
            int numCols = getNumberOfColumns();
            int numRows = getNumberOfRows();
            for (int i = 0; i < numRows; ++i) {
                FastTable<ControlPoint> col = FastTable.newInstance();
                nMat.add(col);
                for (int j = 0; j < numCols; ++j) {
                    col.add(get(i, j));
                }
            }

            ControlPointNet nCpNet = ControlPointNet.valueOf(nMat);
            return nCpNet;  //StackContext.outerCopy(nCpNet);

        } finally {
            //StackContext.exit();
        }
    }

    /**
     * Return a new control point network that is identical to this one but with the rows
     * in reverse order.
     *
     * @return A new ControlPointNet identical to this one but with the row order
     *         reversed.
     */
    public ControlPointNet reverseRows() {
        StackContext.enter();
        try {
            FastTable<FastTable<ControlPoint>> nMat = FastTable.newInstance();
            int numCols = getNumberOfColumns();
            int numRows = getNumberOfRows();
            for (int i = 0; i < numCols; ++i) {
                FastTable<ControlPoint> col = FastTable.newInstance();
                nMat.add(col);
                for (int j = numRows - 1; j >= 0; --j) {
                    col.add(_matrix.get(i).get(j));
                }
            }

            ControlPointNet nCpNet = ControlPointNet.valueOf(nMat);
            return StackContext.outerCopy(nCpNet);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new control point network that is identical to this one but with the
     * columns in reverse order.
     *
     * @return A new ControlPointNet identical to this one but with the column order
     *         reversed.
     */
    public ControlPointNet reverseColumns() {
        StackContext.enter();
        try {
            FastTable<List<ControlPoint>> nMat = FastTable.newInstance();
            int numCols = getNumberOfColumns();
            for (int i = numCols - 1; i >= 0; --i) {
                nMat.add(_matrix.get(i));
            }

            ControlPointNet nCpNet = ControlPointNet.valueOf(nMat);
            return StackContext.outerCopy(nCpNet);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the unit in which the control points in this network are stated.
     *
     * @return The unit in which this control point network is stated.
     */
    public Unit<Length> getUnit() {
        return get(0, 0).getUnit();
    }

    /**
     * Returns the equivalent to this control point network but stated in the specified
     * unit.
     *
     * @param unit The length unit of the control point to be returned. May not be null.
     * @return An equivalent to this control point network but stated in the specified
     *         unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    public ControlPointNet to(Unit<Length> unit) throws ConversionException {
        requireNonNull(unit);

        StackContext.enter();
        try {
            FastTable<FastTable<ControlPoint>> nMat = FastTable.newInstance();
            int ncol = _matrix.size();
            for (int i = 0; i < ncol; ++i) {
                List<ControlPoint> lst = _matrix.get(i);
                FastTable<ControlPoint> nLst = FastTable.newInstance();
                int nrow = lst.size();
                for (int j = 0; j < nrow; ++j) {
                    ControlPoint cp = lst.get(j);
                    nLst.add(cp.to(unit));
                }
                nMat.add(nLst);
            }

            ControlPointNet nCpNet = ControlPointNet.valueOf(nMat);
            return StackContext.outerCopy(nCpNet);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the equivalent of this control point network converted to the specified
     * number of physical dimensions. If the number of dimensions is greater than this
     * element, then zeros are added to the additional dimensions. If the number of
     * dimensions is less than this element, then the extra dimensions are simply dropped
     * (truncated). If the new dimensions are the same as the dimension of this element,
     * then this element is simply returned.
     *
     * @param newDim The dimension of the surface to return.
     * @return The equivalent of this control point network converted to the new
     *         dimensions.
     */
    public ControlPointNet toDimension(int newDim) {

        StackContext.enter();
        try {
            FastTable<FastTable<ControlPoint>> nMat = FastTable.newInstance();
            int ncol = _matrix.size();
            for (int i = 0; i < ncol; ++i) {
                List<ControlPoint> lst = _matrix.get(i);
                FastTable<ControlPoint> nLst = FastTable.newInstance();
                int nrow = lst.size();
                for (int j = 0; j < nrow; ++j) {
                    ControlPoint cp = lst.get(j);
                    ControlPoint nCP = ControlPoint.valueOf(cp.getPoint().toDimension(newDim), cp.getWeight());
                    nLst.add(nCP);
                }
                nMat.add(nLst);
            }

            ControlPointNet nCpNet = ControlPointNet.valueOf(nMat);
            return StackContext.outerCopy(nCpNet);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns an iterator over the lists of ControlPoint objects in this network. The
     * iterator returns lists containing the columns of control points.
     */
    @Override
    public Iterator<List<ControlPoint>> iterator() {
        return (Iterator)_matrix.iterator();
    }

    /**
     * Return <code>true</code> if this ControlPointNet contains valid and finite
     * numerical components. A value of <code>false</code> will be returned if any of the
     * control point values are NaN or Inf.
     *
     * @return true if this ControlPointNet contains valid and finite numerical
     *         components.
     */
    public boolean isValid() {
        int ncol = _matrix.size();
        for (int i = 0; i < ncol; ++i) {
            List<ControlPoint> row = _matrix.get(i);
            int nrow = row.size();
            for (int j = 0; j < nrow; ++j) {
                ControlPoint cp = row.get(j);
                if (!cp.isValid())
                    return false;
            }
        }
        return true;
    }

    /**
     * Returns a copy of this ControlPointNet instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public ControlPointNet copy() {
        return copyOf(this);
    }

    /**
     * Returns a copy of this ControlPointNet instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     * @throws java.lang.CloneNotSupportedException Never thrown.
     */
    @Override
    @SuppressWarnings("CloneDoesntCallSuperClone")
    public Object clone() throws CloneNotSupportedException {
        return copy();
    }

    /**
     * Compares this ControlPointNet against the specified object for strict equality
     * (same sized lists of the equal ControlPoints).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        ControlPointNet that = (ControlPointNet)obj;
        return this._matrix.equals(that._matrix);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _matrix.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    /**
     * Returns the text representation of this control point matrix that consists of the
     * the control points listed out.
     *
     * @return the text representation of this geometry element.
     */
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        tmp.append(_matrix.toText());
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns the String representation of this control point matrix that consists of the
     * control points listed out.
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public String toString() {
        return toText().toString();
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<ControlPointNet> XML = new XMLFormat<ControlPointNet>(ControlPointNet.class) {
        @Override
        public ControlPointNet newInstance(Class<ControlPointNet> cls, InputElement xml) throws XMLStreamException {
            ControlPointNet cpNet = FACTORY.object();
            cpNet._matrix = FastTable.newInstance();
            return cpNet;
        }

        @Override
        public void read(InputElement xml, ControlPointNet obj) throws XMLStreamException {

            FastTable<FastTable<ControlPoint>> matrix = FastTable.newInstance();
            while (xml.hasNext()) {
                matrix.add(xml.get("Column", FastTable.class));
            }
            obj._matrix.addAll(matrix);

        }

        @Override
        public void write(ControlPointNet obj, OutputElement xml) throws XMLStreamException {

            for (FastTable<ControlPoint> column : obj._matrix) {
                xml.add(column, "Column", FastTable.class);
            }

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    protected ControlPointNet() {
    }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<ControlPointNet> FACTORY = new ObjectFactory<ControlPointNet>() {
        @Override
        protected ControlPointNet create() {
            return new ControlPointNet();
        }

        @Override
        protected void cleanup(ControlPointNet obj) {
            int ncol = obj._matrix.size();
            for (int i = 0; i < ncol; ++i) {
                FastTable<ControlPoint> col = obj._matrix.get(i);
                col.reset();
            }
            obj._matrix.reset();
            obj._matrix = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static ControlPointNet copyOf(ControlPointNet original) {
        ControlPointNet o = FACTORY.object();
        o._matrix = FastTable.newInstance();
        int ncol = original._matrix.size();
        for (int i = 0; i < ncol; ++i) {
            List<ControlPoint> tbl = original._matrix.get(i);
            FastTable<ControlPoint> tblCopy = FastTable.newInstance();
            int nrow = tbl.size();
            for (int j = 0; j < nrow; ++j) {
                ControlPoint cp = tbl.get(j);
                tblCopy.add(cp.copy());
            }
            o._matrix.add(tblCopy);
        }
        return o;
    }

}
