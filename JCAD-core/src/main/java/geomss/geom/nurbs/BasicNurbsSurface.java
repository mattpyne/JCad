/**
 * BasicNurbsSurface -- A basic implementation of NURBS surface.
 * 
 * Copyright (C) 2010-2015, by Joseph A. Huwaldt. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 * 
 * This source file is based on, but heavily modified from, the LGPL jgeom (Geometry
 * Library for Java) code by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import geomss.geom.Point;
import geomss.geom.Vector;
import jahuwaldt.js.math.BinomialCoef;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.isNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.MathLib;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A basic implementation of a parametric NURBS surface.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Samuel Gerber, Date: June 14, 2010, Version 1.2.
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class BasicNurbsSurface extends NurbsSurface implements ValueType {

    private static final double TOL = 1.0e-14;

    private ControlPointNet _cnet;      //  The network (matrix) of control points.
    private KnotVector _sKnots;         //  The knot vector in the u-direction.
    private KnotVector _tKnots;         //  The knot vector in the v-direciton.

    /*
     *  References:
     *  1.) Michael E. Mortenson, "Geometric Modeling, Third Edition", ISBN:9780831132989, 2008.
     *  2.) Piegl, L., Tiller, W., The Nurbs Book, 2nd Edition, Springer-Verlag, Berlin, 1997.
     */
    
    /**
     * Create a NURBS surface from the given control points, knotsU and degreeU.
     *
     * @param cps Matrix of control points: cps[u][v]. May not be null.
     * @param p   s-degreeU of the NURBS curve
     * @param q   t-degreeU of the NURBS curve
     * @param sK  Knot values in s-direction. May not be null.
     * @param tK  Knot values in t-direction. May not be null.
     * @return A new BasicNurbsSurface from the given control points, knotsU and degreeU.
     * @throws IllegalArgumentException if the knot vectors are not valid.
     */
    public static BasicNurbsSurface newInstance(ControlPoint cps[][], int p, int q, double[] sK, double[] tK) {
        if (cps.length < 1)
            throw new IllegalArgumentException(RESOURCES.getString("noControlPointsErr"));

        if (tK.length != q + cps.length + 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfWrongNumTKnotsErr"),
                            tK.length, q + cps.length + 1));

        if (sK.length != p + cps[0].length + 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfWrongNumSKnotsErr"),
                            sK.length, p + cps[0].length + 1));

        ControlPointNet cpNet = ControlPointNet.valueOf(cps);
        KnotVector sKnots = KnotVector.newInstance(p, sK);
        KnotVector tKnots = KnotVector.newInstance(q, tK);

        return BasicNurbsSurface.newInstance(cpNet, sKnots, tKnots);
    }

    /**
     * Generate a NURBS surface from the given control points and the given knot vectors.
     *
     * @param cpNet  Matrix of control points. May not be null.
     * @param sKnots Knot vector in the s-direction for the surface. May not be null.
     * @param tKnots Knot vector in the t-direction for the surface. May not be null.
     * @return A new BasicNurbsSurface from the given control points and the given knot
     *         vectors.
     * @throws IllegalArgumentException if the knot vector is not consistent with the
     * number of control points.
     */
    public static BasicNurbsSurface newInstance(ControlPointNet cpNet, KnotVector sKnots, KnotVector tKnots) {
        if (cpNet.size() < 1)
            throw new IllegalArgumentException(RESOURCES.getString("noControlPointsErr"));

        if (tKnots.length() != tKnots.getDegree() + cpNet.getNumberOfColumns() + 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfWrongNumTKnotsErr"),
                            tKnots.length(), tKnots.getDegree() + cpNet.getNumberOfColumns() + 1));

        if (sKnots.length() != sKnots.getDegree() + cpNet.getNumberOfRows() + 1)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfWrongNumSKnotsErr"),
                            sKnots.length(), sKnots.getDegree() + cpNet.getNumberOfRows() + 1));

        BasicNurbsSurface o = FACTORY.object();
        o._cnet = ControlPointNet.valueOf(cpNet);
        o._sKnots = sKnots;
        o._tKnots = tKnots;

        return o;
    }

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation returns the number of control points in this NURBS surface.
     *
     * @return The number of child-elements that make up this geometry element.
     */
    @Override
    public int size() {
        return _cnet.size();
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _cnet.get(0, 0).getPhyDimension();
    }

    /**
     * Return a matrix or network of control points for this surface.
     *
     * @return the ordered control points
     */
    @Override
    public ControlPointNet getControlPoints() {
        return _cnet;
    }

    /**
     * Return the control point matrix size in the s-direction (down a column of control
     * points).
     *
     * @return The control point matrix size in the s-direction.
     */
    @Override
    public int getNumberOfRows() {
        return _cnet.getNumberOfRows();
    }

    /**
     * Return the control point matrix size in the t-direction (across the columns of
     * control points).
     *
     * @return The control point matrix size in the t-direction.
     */
    @Override
    public int getNumberOfColumns() {
        return _cnet.getNumberOfColumns();
    }

    /**
     * Return the s-direction knot vector of this surface.
     *
     * @return The s-knot vector.
     */
    @Override
    public KnotVector getSKnotVector() {
        return _sKnots;
    }

    /**
     * Return the t-direction knot vector of this surface.
     *
     * @return The t-knot vector.
     */
    @Override
    public KnotVector getTKnotVector() {
        return _tKnots;
    }

    /**
     * Return the degreeU of the NURBS surface in the s-direction.
     *
     * @return degreeU of surface in s-direction
     */
    @Override
    public int getSDegree() {
        return _sKnots.getDegree();
    }

    /**
     * Return the degreeU of the NURBS surface in the t-direction.
     *
     * @return degreeU of surface in t-direction
     */
    @Override
    public int getTDegree() {
        return _tKnots.getDegree();
    }

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(double s, double t) {
        validateParameter(s, t);

        //  Corner-point interpolation: S(0,0)=P(0,0); S(1,0)=P(n,0); S(0,1)=P(0,m) and S(1,1)=P(n,m).
        if (parNearStart(s, TOL_ST)) {
            if (parNearStart(t, TOL_ST))
                return _cnet.get(0, 0).getPoint();
            else if (parNearEnd(t, TOL_ST))
                return _cnet.get(0, _cnet.getNumberOfColumns() - 1).getPoint();
        } else if (parNearEnd(s, TOL_ST)) {
            if (parNearStart(t, TOL_ST))
                return _cnet.get(_cnet.getNumberOfRows() - 1, 0).getPoint();
            else if (parNearEnd(t, TOL_ST))
                return _cnet.get(_cnet.getNumberOfRows() - 1, _cnet.getNumberOfColumns() - 1).getPoint();
        }

        StackContext.enter();
        try {
            //  Algorithm: A4.3, Ref. 2.
            /*
             This routine calculates the following:
             P(s,t) = sum_{i=1}^{N} sum_{j=1}^M ( Pij*Wij*Nik(s)Njk(t) ) /
             sum_{i=1}^{N} sum_{j=1}^{M} ( Wij*Nik(s)*Njk(t) )
             */
            Unit<Length> unit = getUnit();
            int s_span = _sKnots.findSpan(s);
            int p = _sKnots.getDegree();
            int sind = s_span - p;
            int t_span = _tKnots.findSpan(t);
            int q = _tKnots.getDegree();

            //  Apply the basis functions to each degreeU of the surface in each direction.
            //  Calculate "sw" point as sum_{i=1}^N sum_{j=1}^M (Pij*Wij*Nik(s)*Njk(t)) and
            //      "sw" weight as sum_{i=1}^N sum_{j=1}^M (Wij*Nik(s)*Njk(t)).
            
            //  Get the basis function values in the s-direction (Nk(s)).
            double[] Nks = _sKnots.basisFunctions(s_span, s);

            //  Get the basis function values in the t-direction (Nk(t)).
            double[] Nkt = _tKnots.basisFunctions(t_span, t);

            //  Compute the composite control point.
            ControlPoint sw = ControlPoint.newInstance(getPhyDimension(), unit);
            for (int i = 0; i <= q; i++) {
                ControlPoint temp = ControlPoint.newInstance(getPhyDimension(), unit);
                int tind = t_span - q + i;
                for (int k = 0; k <= p; ++k) {
                    ControlPoint pw = _cnet.get(sind + k, tind);
                    Point pwp = pw.getPoint();
                    double Nksk = Nks[k];
                    double pww = pw.getWeight() * Nksk;
                    pwp = pwp.times(pww);
                    Point tmpp = temp.getPoint();
                    double tmpw = temp.getWeight();
                    temp = ControlPoint.valueOf(tmpp.plus(pwp), tmpw + pww);
                }
                temp = temp.times(Nkt[i]);
                sw = sw.plus(temp);
            }

            //  Convert the control point to a geometric point.
            //  The "sw" weight is the sum of Wi*Ni,k calculated above.
            Point outPoint = sw.getPoint();
            if (outPoint.normValue() > TOL)
                outPoint = outPoint.divide(sw.getWeight());

            return StackContext.outerCopy(outPoint.to(unit));

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the u-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade, boolean scaled) {
        validateParameter(s, t);

        //  Handle roundoff
        s = roundParNearEnds(s);
        t = roundParNearEnds(t);

        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        int s_span = _sKnots.findSpan(s);
        int p = _sKnots.getDegree();
        int sind = s_span - p;
        int ds = MathLib.min(grade, p);
        int t_span = _tKnots.findSpan(t);
        int q = _tKnots.getDegree();
        int gradeP1 = grade + 1;

        Vector[] cOut = Vector.allocateArray(gradeP1);   //  Created outside of StackContext.
        StackContext.enter();
        try {
            //  Get the basis function derivative values in the s-direction (dNk(s)).
            double[][] dNks = _sKnots.basisFunctionDerivatives(s_span, s, ds);

            //  Calculate z = sum_i^N sum_j^M ( Wij*dNik(s)*Njk(t) ) for each basis function derivative and
            //  pnts = sum_i^N sum_j^M ( Pij*Wij*dNik(s)*Njk(t) ) for each control point and basis function derivative.
            int porder = p + 1;
            int qorder = q + 1;
            FastTable<double[]> zList = FastTable.newInstance();
            FastTable<Point[]> pntsList = FastTable.newInstance();
            for (int l = 0; l < qorder; l++) {
                double[] z = newZeroedDoubleArray(gradeP1);
                zList.add(z);
                Point[] pnts = newZeroedPointArray(gradeP1);
                pntsList.add(pnts);
                int tind = t_span - q + l;

                for (int k = 0; k <= ds; ++k) {
                    for (int i = 0; i < porder; i++) {
                        ControlPoint cpi = _cnet.get(sind + i, tind);
                        double Wi = cpi.getWeight();
                        double WiNk = Wi * dNks[k][i];
                        z[k] += WiNk;
                        pnts[k] = pnts[k].plus(cpi.getPoint().times(WiNk));
                    }
                }
            }

            //  Get the basis function values in the t-direction (Nk(t)).
            double[] Nkt = _tKnots.basisFunctions(t_span, t);

            //  Apply Nk(t)
            for (int l = 0; l < qorder; l++) {
                Point[] pnts = pntsList.get(l);
                @SuppressWarnings("MismatchedReadAndWriteOfArray")
                double[] z = zList.get(l);

                for (int k = 0; k <= ds; ++k) {
                    double Nktl = Nkt[l];
                    z[k] *= Nktl;
                    pnts[k] = pnts[k].times(Nktl);
                }
            }

            //  Sum up the lists of values collected above.
            double[] z = sumDerivativeWeights(zList, grade);
            Point[] pnts = sumPoints(pntsList, grade);

            //  Compute the weighted derivatives by stepping through a binomial expansion for each derivative.
            Point[] c = binomial(pnts, z, gradeP1);

            //  Copy the final points out of the StackContext.
            for (int i = gradeP1-1; i >= 0; --i) {
                cOut[i] = StackContext.outerCopy(c[i].toGeomVector());
            }

        } finally {
            StackContext.exit();
        }

        //  Convert the array point points into a list of vectors.
        FastTable<Vector<Length>> output = FastTable.newInstance();
        Point origin = Point.valueOf(cOut[0]);
        for (int i = 0; i < gradeP1; ++i) {
            Vector<Length> v = cOut[i];
            v.setOrigin(origin);
            output.add(v);
        }
        
        Vector.recycleArray(cOut);

        return output;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the v-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade, boolean scaled) {
        validateParameter(s, t);

        //  Handle roundoff
        s = roundParNearEnds(s);
        t = roundParNearEnds(t);

        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        int s_span = _sKnots.findSpan(s);
        int p = _sKnots.getDegree();
        int sind = s_span - p;
        int t_span = _tKnots.findSpan(t);
        int q = _tKnots.getDegree();
        int dt = MathLib.min(grade, q);
        int gradeP1 = grade + 1;

        Vector[] cOut = Vector.allocateArray(gradeP1);   //  Created outside of StackContext.
        StackContext.enter();
        try {
            //  Get the basis function values for a B-Spline (unweighted derivatives).
            double[] Nks = _sKnots.basisFunctions(s_span, s);

            //  Calculate z = sum_i^N sum_j^M ( Wij*Nik(s)*dNjk(t) ) for each basis function derivative and
            //  pnts = sum_i^N sum_j^M ( Pij*Wij*Nik(s)*dNjk(t) ) for each control point and basis function derivative.
            int porder = p + 1;
            int qorder = q + 1;
            FastTable<double[]> zList = FastTable.newInstance();
            FastTable<Point[]> pntsList = FastTable.newInstance();
            for (int l = 0; l < qorder; l++) {
                double[] z = newZeroedDoubleArray(gradeP1);
                zList.add(z);
                Point[] pnts = newZeroedPointArray(gradeP1);
                pntsList.add(pnts);
                int tind = t_span - q + l;

                for (int k = 0; k <= dt; ++k) {
                    for (int i = 0; i < porder; i++) {
                        ControlPoint cpi = _cnet.get(sind + i, tind);
                        double Wi = cpi.getWeight();
                        double WiNk = Wi * Nks[i];
                        z[k] += WiNk;
                        pnts[k] = pnts[k].plus(cpi.getPoint().times(WiNk));
                    }
                }
            }

            //  Get the basis function derivative values in the t-direction (dNk(t)).
            double[][] dNkt = _tKnots.basisFunctionDerivatives(t_span, t, dt);

            //  Apply dNk(t)
            for (int l = 0; l < qorder; l++) {
                Point[] pnts = pntsList.get(l);
                @SuppressWarnings("MismatchedReadAndWriteOfArray")
                double[] z = zList.get(l);

                for (int k = 0; k <= dt; ++k) {
                    double dNktl = dNkt[k][l];
                    z[k] *= dNktl;
                    pnts[k] = pnts[k].times(dNktl);
                }
            }

            //  Sum up the lists of values collected above.
            double[] z = sumDerivativeWeights(zList, grade);
            Point[] pnts = sumPoints(pntsList, grade);

            //  Compute the weighted derivatives by stepping through a binomial expansion for each derivative.
            Point[] c = binomial(pnts, z, gradeP1);

            //  Copy the final points out of the StackContext.
            for (int i = gradeP1-1; i >= 0; --i) {
                cOut[i] = StackContext.outerCopy(c[i].toGeomVector());
            }

        } finally {
            StackContext.exit();
        }

        //  Convert the array point points into a list of vectors.
        FastTable<Vector<Length>> output = FastTable.newInstance();
        Point origin = Point.valueOf(cOut[0]);
        for (int i = 0; i < gradeP1; ++i) {
            Vector<Length> v = cOut[i];
            v.setOrigin(origin);
            output.add(v);
        }
        
        Vector.recycleArray(cOut);

        return output;
    }

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param s 1st parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    @Override
    public Vector<Length> getTwistVector(double s, double t) {
        validateParameter(s, t);

        //  Handle roundoff
        s = roundParNearEnds(s);
        t = roundParNearEnds(t);

        int s_span = _sKnots.findSpan(s);
        int p = _sKnots.getDegree();
        int sind = s_span - p;
        int t_span = _tKnots.findSpan(t);
        int q = _tKnots.getDegree();

        Vector<Length> tvec;
        Point origin;
        StackContext.enter();
        try {
            //  Get the basis function derivative values in the s-direction (dNk(s)).
            double[][] dNks = _sKnots.basisFunctionDerivatives(s_span, s, 1);

            //  Calculate z = sum_i^N sum_j^M ( Wij*dNik(s)*dNjk(t) ) for each basis function derivative and
            //  pnts = sum_i^N sum_j^M ( Pij*Wij*dNik(s)*dNjk(t) ) for each control point and basis function derivative.
            int porder = p + 1;
            int qorder = q + 1;
            FastTable<double[]> zList = FastTable.newInstance();
            FastTable<Point[]> pntsList = FastTable.newInstance();
            for (int l = 0; l < qorder; l++) {
                double[] z = newZeroedDoubleArray(1 + 1);
                zList.add(z);
                Point[] pnts = newZeroedPointArray(1 + 1);
                pntsList.add(pnts);
                int tind = t_span - q + l;

                for (int k = 0; k <= 1; ++k) {
                    for (int i = 0; i < porder; i++) {
                        ControlPoint cpi = _cnet.get(sind + i, tind);
                        double Wi = cpi.getWeight();
                        double WiNk = Wi * dNks[k][i];
                        z[k] += WiNk;
                        pnts[k] = pnts[k].plus(cpi.getPoint().times(WiNk));
                    }
                }
            }

            //  Get the basis function derivative values in the t-direction (dNk(t)).
            double[][] dNkt = _tKnots.basisFunctionDerivatives(t_span, t, 1);

            //  Apply dNk(t)
            for (int l = 0; l < qorder; l++) {
                Point[] pnts = pntsList.get(l);
                @SuppressWarnings("MismatchedReadAndWriteOfArray")
                double[] z = zList.get(l);

                for (int k = 0; k <= 1; ++k) {
                    double dNktl = dNkt[k][l];
                    z[k] *= dNktl;
                    pnts[k] = pnts[k].times(dNktl);
                }
            }

            //  Sum up the lists of values collected above.
            double[] z = sumDerivativeWeights(zList, 1);
            Point[] pnts = sumPoints(pntsList, 1);

            //  Compute the weighted derivatives by stepping through a binomial expansion for each derivative.
            Point[] c = binomial(pnts, z, 1 + 1);

            //  Copy out the vector origin point.
            origin = StackContext.outerCopy(c[0]);

            //  Copy the required derivative point to the outer context.
            tvec = StackContext.outerCopy(c[1].toGeomVector());

        } finally {
            StackContext.exit();
        }

        tvec.setOrigin(origin);

        return tvec;
    }

    /**
     * Compute the weighted derivatives by stepping through a binomial expansion for each
     * derivative. e.g.:
     * <pre>
     *   surface point, c(0) = p(0)/z(0)
     *      1st derivative, c(1) = (p(1) - c(0)*z(1))/z(0),
     *      2nd derivative, c(2) = (p(2) - c(0)*z(2) - 2*c(1)*z(1))/z(0),
     *      3rd derivative, c(3) = (p(3) - c(0)*z(3) - 3*c(1)*z(2) - 3*c(2)*z(1))/z(0),
     *      etc
     * </pre>
     *
     * @param pnts    The list of weighted control points: p[0..grade+1].
     * @param z       The derivative weights (Wi*Nik): z[0..grade+1].
     * @param gradeP1 The maximum grade to calculate the v-derivatives for (1=1st
     *                derivative, 2=2nd derivative, etc) + 1.
     */
    private static Point[] binomial(Point[] pnts, double[] z, int gradeP1) {

        BinomialCoef bin = BinomialCoef.newInstance(gradeP1);   //  Get the binomial coefficients.

        Point[] c = Point.allocateArray(gradeP1);
        for (int k = 0; k < gradeP1; ++k) {
            Point v = pnts[k];
            for (int i = k; i > 0; --i) {
                v = v.minus(c[k - i].times(bin.get(k, i) * z[i]));
            }
            if (v.normValue() > TOL)        //  Handles 0/0 == 0.
                c[k] = v.divide(z[0]);
            else
                c[k] = v.times(0);
        }

        return c;
    }

    /**
     * Return a new double array with the values zeroed. The returned instance is created
     * using ArrayFactory.DOUBLES_FACTORY and may be recycled.
     */
    private static double[] newZeroedDoubleArray(int size) {
        double[] arr = ArrayFactory.DOUBLES_FACTORY.array(size);
        for (int i = size-1; i >= 0; --i) {
            arr[i] = 0;
        }
        return arr;
    }

    /**
     * Return a new array filled with Point objects with their values zeroed. The returned
     * instance is created using Point.allocateArray() and may be recycled.
     * @see Point#allocateArray(int)
     * @see Point#recycleArray(geomss.geom.Point[]) 
     */
    private Point[] newZeroedPointArray(int size) {
        Point[] pnts = Point.allocateArray(size);
        Unit unit = getUnit();
        int dim = getPhyDimension();
        for (int i = size-1; i >= 0; --i)
            pnts[i] = Point.newInstance(dim, unit);
        return pnts;
    }

    /**
     * Sum up the list of point arrays into a single point array.
     *
     * @param pntsList A list of arrays of Point objects, each "grade+1" long.
     * @param grade    The length-1 of each Point array in pntsList.
     */
    private Point[] sumPoints(List<Point[]> pntsList, int grade) {
        int order = pntsList.size();

        Point[] pnts = newZeroedPointArray(grade + 1);
        for (int l = 0; l < order; ++l) {
            Point[] pntsTmp = pntsList.get(l);

            for (int k = grade; k >= 0; --k) {
                pnts[k] = pnts[k].plus(pntsTmp[k]);
            }
        }

        return pnts;
    }

    /**
     * Sum up the list of Wi*Nik arrays into a single array.
     *
     * @param arrList A list of double arrays, each "grade+1" long.
     * @param grade   The length-1 of each double array in arrList.
     */
    private static double[] sumDerivativeWeights(List<double[]> arrList, int grade) {
        int order = arrList.size();

        double[] z = newZeroedDoubleArray(grade + 1);
        for (int l = 0; l < order; ++l) {
            double[] ztmp = arrList.get(l);

            for (int k = grade; k >= 0; --k) {
                z[k] += ztmp[k];
            }
        }

        return z;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no geometry.
     */
    @Override
    public Point getBoundsMin() {
        return _cnet.getBoundsMin();
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        return _cnet.getBoundsMax();
    }

    /**
     * Returns the unit in which the control points in this surface are stated.
     *
     * @return The unit in which the control points in this surface are stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _cnet.getUnit();
    }

    /**
     * Returns the equivalent to this surface but stated in the specified unit.
     *
     * @param unit The length unit of the surface to be returned. May not be null.
     * @return An equivalent to this surface but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public BasicNurbsSurface to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        ControlPointNet nCNet = _cnet.to(unit);
        BasicNurbsSurface srf = BasicNurbsSurface.newInstance(nCNet, getSKnotVector(), getTKnotVector());
        return copyState(srf);  //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Return the equivalent of this surface converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the surface to return.
     * @return The equivalent of this surface converted to the new dimensions.
     */
    @Override
    public BasicNurbsSurface toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        ControlPointNet nCNet = _cnet.toDimension(newDim);
        BasicNurbsSurface srf = BasicNurbsSurface.newInstance(nCNet, getSKnotVector(), getTKnotVector());

        return copyState(srf);  //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Returns a copy of this <code>BasicNurbsSurface</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public BasicNurbsSurface copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public BasicNurbsSurface copyToReal() {
        return copy();
    }

    /**
     * Compares this BasicNurbsSurface against the specified object for strict equality
     * (same values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this object is identical to that object;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        BasicNurbsSurface that = (BasicNurbsSurface)obj;
        return this._cnet.equals(that._cnet)
                && this._sKnots.equals(that._sKnots)
                && this._tKnots.equals(that._tKnots)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_cnet, _sKnots, _tKnots);
    }

    /**
     * Recycles a <code>BasicNurbsSurface</code> instance immediately (on the stack when
     * executing in a StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(BasicNurbsSurface instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<BasicNurbsSurface> XML = new XMLFormat<BasicNurbsSurface>(BasicNurbsSurface.class) {
        @Override
        public BasicNurbsSurface newInstance(Class<BasicNurbsSurface> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, BasicNurbsSurface obj) throws XMLStreamException {
            NurbsSurface.XML.read(xml, obj);     // Call parent read.

            ControlPointNet cpNet = xml.get("CPNet", ControlPointNet.class);
            KnotVector sKnots = xml.get("sKnots", KnotVector.class);
            KnotVector tKnots = xml.get("tKnots", KnotVector.class);

            if (isNull(cpNet) || cpNet.size() < 1)
                throw new XMLStreamException(RESOURCES.getString("noControlPointsErr"));

            if (tKnots.length() != tKnots.getDegree() + cpNet.getNumberOfColumns() + 1)
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("srfWrongNumTKnotsErr"),
                                tKnots.length(), tKnots.getDegree() + cpNet.getNumberOfColumns() + 1));

            if (sKnots.length() != sKnots.getDegree() + cpNet.getNumberOfRows() + 1)
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("srfWrongNumSKnotsErr"),
                                sKnots.length(), sKnots.getDegree() + cpNet.getNumberOfRows() + 1));

            obj._cnet = cpNet;
            obj._sKnots = sKnots;
            obj._tKnots = tKnots;
        }

        @Override
        public void write(BasicNurbsSurface obj, OutputElement xml) throws XMLStreamException {
            NurbsSurface.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._cnet, "CPNet", ControlPointNet.class);
            xml.add(obj._sKnots, "sKnots", KnotVector.class);
            xml.add(obj._tKnots, "tKnots", KnotVector.class);

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private BasicNurbsSurface() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<BasicNurbsSurface> FACTORY = new ObjectFactory<BasicNurbsSurface>() {
        @Override
        protected BasicNurbsSurface create() {
            return new BasicNurbsSurface();
        }

        @Override
        protected void cleanup(BasicNurbsSurface obj) {
            obj.reset();
            obj._cnet = null;
            obj._sKnots = null;
            obj._tKnots = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static BasicNurbsSurface copyOf(BasicNurbsSurface original) {
        BasicNurbsSurface o = FACTORY.object();
        o._cnet = original._cnet.copy();
        o._sKnots = original._sKnots.copy();
        o._tKnots = original._tKnots.copy();
        original.copyState(o);
        return o;
    }

}
