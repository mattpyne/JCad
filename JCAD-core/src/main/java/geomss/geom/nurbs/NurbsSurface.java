/**
 * NurbsSurface -- Implementation and interface in common to all NURBS surfaces.
 *
 * Copyright (C) 2010-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 *
 * This source file is based on, but heavily modified from, the LGPL jgeom (Geometry
 * Library for Java) code by Samuel Gerber, Copyright (C) 2005.
 */
package geomss.geom.nurbs;

import geomss.geom.*;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javolution.context.StackContext;
import javolution.lang.MathLib;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastMap;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * The interface and implementation in common to all NURBS surfaces.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 14, 2010
 * @version January 31, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class NurbsSurface extends AbstractSurface<NurbsSurface> {

    /**
     * Return a matrix or network of control points for this surface.
     *
     * @return the ordered control points
     */
    public abstract ControlPointNet getControlPoints();

    /**
     * Return the control point matrix size in the s-direction (down a column of control
     * points).
     *
     * @return The control point matrix size in the s-direction
     */
    public abstract int getNumberOfRows();

    /**
     * Return the control point matrix size in the t-direction (across the columns of
     * control points).
     *
     * @return The control point matrix size in the t-direction
     */
    public abstract int getNumberOfColumns();

    /**
     * Return the s-direction knot vector of this surface.
     *
     * @return The s-knot vector.
     */
    public abstract KnotVector getSKnotVector();

    /**
     * Return the t-direction knot vector of this surface.
     *
     * @return The t-knot vector.
     */
    public abstract KnotVector getTKnotVector();

    /**
     * Return the s-degreeU of the NURBS surface.
     *
     * @return s-degreeU of surface
     */
    public abstract int getSDegree();

    /**
     * Return the t-degreeU of the NURBS surface.
     *
     * @return t-degreeU of surface
     */
    public abstract int getTDegree();

    /**
     * Return a new surface that is identical to this one but with the transpose of the
     * parameterization of this surface. The S and T directions will be swapped.
     *
     * @return A new surface that is identical to this one but with the transpose of the
     *         parameterization of this surface.
     */
    @Override
    public NurbsSurface transpose() {
        //  Transpose the control point network.
        ControlPointNet oldCPNet = getControlPoints();
        ControlPointNet cpNet = oldCPNet.transpose();

        //  Reverse and swap the knot vectors.
        KnotVector kvT = getSKnotVector().reverse();
        KnotVector kvS = getTKnotVector().reverse();

        //  Construct a new surface.
        BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpNet, kvS, kvT);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return a new surface that is identical to this one, but with the S-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         S-parameterization reversed.
     * @see #reverseT
     */
    @Override
    public NurbsSurface reverseS() {
        //  Reverse the control point network rows.
        ControlPointNet oldCPNet = getControlPoints();
        ControlPointNet cpNet = oldCPNet.reverseRows();

        //  Reverse the S-knot vector.
        KnotVector kvS = getSKnotVector().reverse();

        //  Construct a new surface.
        BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpNet, kvS, getTKnotVector());
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return a new surface that is identical to this one, but with the T-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         T-parameterization reversed.
     * @see #reverseS
     */
    @Override
    public NurbsSurface reverseT() {
        //  Reverse the control point network columns.
        ControlPointNet oldCPNet = getControlPoints();
        ControlPointNet cpNet = oldCPNet.reverseColumns();

        //  Reverse the T-knot vector.
        KnotVector kvT = getTKnotVector().reverse();

        //  Construct a new surface.
        BasicNurbsSurface srf = BasicNurbsSurface.newInstance(cpNet, getSKnotVector(), kvT);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Split this {@link NurbsSurface} at the specified parametric S-position returning a
     * list containing two new surfaces (a lower surface with smaller S-parametric
     * positions than "s" and an upper surface with larger S-parametric positions).
     *
     * @param s The S-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<NurbsSurface> splitAtS(double s) {
        validateParameter(s, 0);
        if (parNearEnds(s, TOL_ST)) {
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("canNotSplitAtEnds"), "surface"));
        }

        GeomList<NurbsSurface> output = GeomList.newInstance();     //  Created outside of StackContext.
        StackContext.enter();
        try {
            //  Construct the upper and lower control point networks and S-knot vector
            //  by splitting each defining S-curve.
            FastTable<List<ControlPoint>> cpMatrixL = FastTable.newInstance();
            FastTable<List<ControlPoint>> cpMatrixU = FastTable.newInstance();
            KnotVector kvSL = getSKnotVector();
            KnotVector kvSU = kvSL;
            int numCols = getNumberOfColumns();
            for (int i = 0; i < numCols; ++i) {
                BasicNurbsCurve crv = getSCurve(i);
                GeomList<NurbsCurve> crvList = crv.splitAt(s);
                cpMatrixL.add(crvList.get(0).getControlPoints());
                cpMatrixU.add(crvList.get(1).getControlPoints());
                kvSL = crvList.get(0).getKnotVector();
                kvSU = crvList.get(1).getKnotVector();
            }

            //  Create the new control point networks.
            ControlPointNet cpNetL = ControlPointNet.valueOf(cpMatrixL);
            ControlPointNet cpNetU = ControlPointNet.valueOf(cpMatrixU);

            //  Construct the new surfaces.
            KnotVector kvT = getTKnotVector();
            BasicNurbsSurface srfL = BasicNurbsSurface.newInstance(cpNetL, kvSL, kvT);
            BasicNurbsSurface srfU = BasicNurbsSurface.newInstance(cpNetU, kvSU, kvT);

            //  Copy surface segments to the outer context.
            output.add(StackContext.outerCopy(srfL));
            output.add(StackContext.outerCopy(srfU));

        } finally {
            StackContext.exit();
        }

        return output;
    }

    /**
     * Split this {@link NurbsSurface} at the specified parametric T-position returning a
     * list containing two new surfaces (a lower surface with smaller T-parametric
     * positions than "t" and an upper surface with larger T-parametric positions).
     *
     * @param t The T-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<NurbsSurface> splitAtT(double t) {
        validateParameter(0, t);
        if (parNearEnds(t, TOL_ST)) {
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("canNotSplitAtEnds"), "surface"));
        }

        GeomList<NurbsSurface> output = GeomList.newInstance();     //  Created outside of StackContext.
        StackContext.enter();
        try {
            //  Construct the upper and lower control point networks and T-knot vector
            //  by splitting each defining S-curve.
            FastTable<List<ControlPoint>> cpMatrixL = FastTable.newInstance();
            FastTable<List<ControlPoint>> cpMatrixU = FastTable.newInstance();
            KnotVector kvTL = getTKnotVector();
            KnotVector kvTU = kvTL;
            int numRows = getNumberOfRows();
            for (int i = 0; i < numRows; ++i) {
                BasicNurbsCurve crv = getTCurve(i);
                GeomList<NurbsCurve> crvList = crv.splitAt(t);
                cpMatrixL.add(crvList.get(0).getControlPoints());
                cpMatrixU.add(crvList.get(1).getControlPoints());
                kvTL = crvList.get(0).getKnotVector();
                kvTU = crvList.get(1).getKnotVector();
            }

            //  Create the new control point networks.
            ControlPointNet cpNetL = ControlPointNet.valueOf(cpMatrixL).transpose();
            ControlPointNet cpNetU = ControlPointNet.valueOf(cpMatrixU).transpose();

            //  Construct the new surfaces.
            KnotVector kvS = getSKnotVector();
            BasicNurbsSurface srfL = BasicNurbsSurface.newInstance(cpNetL, kvS, kvTL);
            BasicNurbsSurface srfU = BasicNurbsSurface.newInstance(cpNetU, kvS, kvTU);

            //  Copy surface segments to the outer context.
            output.add(StackContext.outerCopy(srfL));
            output.add(StackContext.outerCopy(srfU));

        } finally {
            StackContext.exit();
        }

        return output;
    }

    /**
     * Return the T=0 Boundary for this surface as a NURBS curve.
     *
     * @return The T=0 Boundary for this surface as a NURBS curve.
     */
    @Override
    public Curve getT0Curve() {
        return getSCurve(0);
    }

    /**
     * Return the T=1 Boundary for this surface as a NURBS curve.
     *
     * @return The T=1 Boundary for this surface as a NURBS curve.
     */
    @Override
    public Curve getT1Curve() {
        Curve crv = getSCurve(getNumberOfColumns() - 1);
        return crv;
    }

    /**
     * Return the S=0 Boundary for this surface as a NURBS curve.
     *
     * @return The S=0 Boundary for this surface as a NURBS curve.
     */
    @Override
    public Curve getS0Curve() {
        return getTCurve(0);
    }

    /**
     * Return the S=1 Boundary for this surface as a NURBS curve.
     *
     * @return The S=1 Boundary for this surface as a NURBS curve.
     */
    @Override
    public Curve getS1Curve() {
        Curve crv = getTCurve(getNumberOfRows() - 1);
        return crv;
    }

    /**
     * Return the S-curve with the specified index from this surface as a NURBS curve.
     *
     * @param index The T-index for the curve to return from 0 to getNumberOfColumns()-1.
     * @return The <code>BasicNurbsCurve</code> built from the specified column in the
     *         ControlPointNet.
     */
    public BasicNurbsCurve getSCurve(int index) {
        ControlPointNet net = getControlPoints();
        List<ControlPoint> cps = net.getColumn(index);
        BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, getSKnotVector());
        FastTable.recycle((FastTable)cps);
        return crv;
    }

    /**
     * Return the T-curve with the specified index from this surface as a NURBS curve.
     *
     * @param index The S-index for the curve to return from 0 to getNumberOfRows()-1.
     * @return The <code>BasicNurbsCurve</code> built from the specified row in the
     *         ControlPointNet.
     */
    public BasicNurbsCurve getTCurve(int index) {
        ControlPointNet net = getControlPoints();
        List<ControlPoint> cps = net.getRow(index);
        BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, getTKnotVector());
        FastTable.recycle((FastTable)cps);
        return crv;
    }

    /**
     * Create and return a new {@link BasicNurbsSurface NURBS surface} that is
     * geometrically identical to this one but with a new knot inserted at the specified
     * parametric S-location. The parameterization of the new surface is identical to this
     * one, but the new surface will have a new knot at the specified s-location and a new
     * set of control points.
     *
     * @param s The parametric s-position where the new knot should be inserted from 0 to
     *          1.
     * @param r The number of times that a knot should be inserted at the specified
     *          position.
     * @return A new NURBS surface that is identical to this one, but with a new knot
     *         inserted in the specified parametric s-position.
     */
    public BasicNurbsSurface insertSKnot(double s, int r) {
        validateParameter(s,0);

        StackContext.enter();
        try {
            KnotVector kv = null;
            FastTable<List<ControlPoint>> cpNetList = FastTable.newInstance();

            //  Add a knot to each defining column curve.
            int numCols = getNumberOfColumns();
            for (int i = 0; i < numCols; ++i) {
                BasicNurbsCurve c1 = getSCurve(i);
                BasicNurbsCurve c2 = c1.insertKnot(s, r);
                kv = c2.getKnotVector();
                cpNetList.add(c2.getControlPoints());
            }

            //  Create a new surface from the new knot vector and set of control points.
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(ControlPointNet.valueOf(cpNetList), kv, getTKnotVector());
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create and return a new {@link BasicNurbsSurface NURBS surface} that is
     * geometrically identical to this one but with a new knot inserted at the specified
     * parametric T-location. The parameterization of the new surface is identical to this
     * one, but the new surface will have a new knot at the specified T-location and a new
     * set of control points.
     *
     * @param t The parametric T-position where the new knot should be inserted from 0 to
     *          1.
     * @param r The number of times that a knot should be inserted at the specified
     *          position.
     * @return A new NURBS surface that is identical to this one, but with new knotsU
     *         inserted in the specified parametric T-position.
     */
    public BasicNurbsSurface insertTKnot(double t, int r) {
        validateParameter(0,t);

        StackContext.enter();
        try {
            KnotVector kv = null;
            FastTable<FastTable<ControlPoint>> cpNetList = FastTable.newInstance();
            int numCols = getNumberOfColumns() + 1;

            //  Create an empty list of lists of control points.
            int numRows = getNumberOfRows();
            for (int i = 0; i < numCols; ++i) {
                FastTable<ControlPoint> tbl = FastTable.newInstance();
                tbl.setSize(numRows);
                cpNetList.add(tbl);
            }

            //  Add the knot to each defining row curve.
            for (int j = 0; j < numRows; ++j) {
                BasicNurbsCurve c1 = getTCurve(j);
                BasicNurbsCurve c2 = c1.insertKnot(t, r);
                kv = c2.getKnotVector();
                List<ControlPoint> cps = c2.getControlPoints();
                for (int i = 0; i < numCols; ++i) {
                    cpNetList.get(i).set(j, cps.get(i));
                }
            }

            //  Create a new surface from the new knot vector and set of control points.
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(ControlPointNet.valueOf(cpNetList), getSKnotVector(), kv);
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a {@link BasicNurbsSurface NURBS surface} that is geometrically identical to
     * this one but with the specified list of knotsU merged into it's S-knot vector. The
     * parameterization of the new surface is identical to this surface, but the new
     * surface will have any new knotsU at the specified locations added and a new set of
     * control points. If the input knot list matches the knot list in the S direction of
     * this surface, then this surface is returned.
     *
     * @param Um The knot vector to merge with this surface's S-knot vector.
     * @return A NURBS surface that is identical to this one, but with the merger of the
     *         input knot vector and this surface's S-knot vector.
     */
    public NurbsSurface mergeSKnotVector(Float64Vector Um) {

        //  Check to see if any knotsU need inserting.
        Float64Vector iKnots = knotsToInsert(getSKnotVector(), Um);
        if (isNull(iKnots))
            return this;

        NurbsSurface newSrf = this.refineSKnotVector(iKnots);
        copyState(newSrf);  //  Copy over the super-class state for this surface to the new one.

        return newSrf;
    }

    /**
     * Return a {@link BasicNurbsSurface NURBS surface} that is geometrically identical to
     * this one but with the specified list of knotsU merged into it's T-knot vector. The
     * parameterization of the new surface is identical to this surface, but the new
     * surface will have any new knotsU at the specified locations added and a new set of
     * control points. If the input knot list matches the knot list in the T direction of
     * this surface, then this surface is returned.
     *
     * @param Um The knot vector to merge with this surface's T-knot vector. May not be null.
     * @return A NURBS surface that is identical to this one, but with the merger of the
     *         input knot vector and this surface's T knot vector.
     */
    public NurbsSurface mergeTKnotVector(Float64Vector Um) {

        //  Check to see if any knotsU need inserting.
        Float64Vector iKnots = knotsToInsert(getTKnotVector(), requireNonNull(Um));
        if (isNull(iKnots))
            return this;

        NurbsSurface newSrf = this.refineTKnotVector(iKnots);
        copyState(newSrf);  //  Copy over the super-class state for this surface to the new one.

        return newSrf;
    }

    private static Float64Vector knotsToInsert(KnotVector U, Float64Vector Um) {
        StackContext.enter();
        try {
            int Usize = U.length();
            int Umsize = Um.getDimension();

            //  Find the knotsU that need inserting.
            FastTable<Float64> Iknots = FastTable.newInstance();

            boolean done = false;
            int ia = 0, ib = 0;
            while (!done) {
                double Umv = Um.getValue(ib);
                double Uv = U.getValue(ia);
                if (MathLib.abs(Umv - Uv) <= TOL_ST) {      //  Umv == Uv
                    ++ib;
                    ++ia;
                } else {
                    Iknots.add(Float64.valueOf(Umv));
                    ++ib;
                }
                done = (ia >= Usize || ib >= Umsize);
            }

            //  Check to see if any knotsU need inserting.
            if (Iknots.isEmpty())
                return null;

            Float64Vector iKnots = Float64Vector.valueOf(Iknots);
            return StackContext.outerCopy(iKnots);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create and return a new {@link BasicNurbsSurface NURBS surface} that is
     * geometrically identical to this one but with the specified list of knotsU inserted
     * into it's S-knot vector. The parameterization of the new surface is identical to
     * this surface, but the new surface will have the new knotsU at the specified
     * locations in the S-direction and a new set of control points. This is more
     * efficient than repeatedly calling insertSKnot() for multiple knot insertions.
     *
     * @param newKnots The parametric S-locations where the knotsU are to be inserted. May
     *                 not be null.
     * @return A new NURBS surface that is identical to this one, but with the new knotsU
     *         inserted at the specified locations in the parametric S-direction.
     */
    public BasicNurbsSurface refineSKnotVector(double[] newKnots) {
        return refineSKnotVector(Float64Vector.valueOf(requireNonNull(newKnots)));
    }

    /**
     * Create and return a new {@link BasicNurbsSurface NURBS surface} that is
     * geometrically identical to this one but with the specified list of knotsU inserted
     * into it's S-knot vector. The parameterization of the new surface is identical to
     * this surface, but the new surface will have the new knotsU at the specified
     * locations in the S-direction and a new set of control points. This is more
     * efficient than repeatedly calling insertSKnot() for multiple knot insertions.
     *
     * @param newKnots The parametric S-locations where the knotsU are to be inserted. May
     *                 not be null.
     * @return A new NURBS surface that is identical to this one, but with the new knotsU
     *         inserted at the specified locations in the parametric S-direction.
     */
    public BasicNurbsSurface refineSKnotVector(Float64Vector newKnots) {

        //  Check on the validity of the new knotsU.
        int r = newKnots.getDimension() - 1;
        double sOld = 0;
        for (int i = 0; i <= r; ++i) {
            double s = newKnots.getValue(i);
            validateParameter(s,0);
            if (s < sOld) {
                throw new IllegalArgumentException(RESOURCES.getString("knotsNotIncreasingErr"));
            }
            sOld = s;
        }

        StackContext.enter();
        try {
            KnotVector kv = null;
            FastTable<List<ControlPoint>> cpNetList = FastTable.newInstance();

            //  Add the new knotsU to each defining column curve.
            int numCols = getNumberOfColumns();
            for (int i = 0; i < numCols; ++i) {
                BasicNurbsCurve c1 = getSCurve(i);
                BasicNurbsCurve c2 = c1.refineKnotVector(newKnots);
                kv = c2.getKnotVector();
                cpNetList.add(c2.getControlPoints());
            }

            //  Create a new surface from the new knot vector and set of control points.
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(ControlPointNet.valueOf(cpNetList), kv, getTKnotVector());
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Create and return a new {@link BasicNurbsSurface NURBS surface} that is
     * geometrically identical to this one but with the specified list of knotsU inserted
     * into it's T-knot vector. The parameterization of the new surface is identical to
     * this surface, but the new surface will have the new knotsU at the specified
     * locations in the T-direction and a new set of control points. This is more
     * efficient than repeatedly calling insertTKnot() for multiple knot insertions.
     *
     * @param newKnots The parametric T-locations where the knotsU are to be inserted. May
     *                 not be null.
     * @return A new NURBS surface that is identical to this one, but with the new knotsU
     *         inserted at the specified locations in the parametric T-direction.
     */
    public BasicNurbsSurface refineTKnotVector(double[] newKnots) {
        return refineTKnotVector(Float64Vector.valueOf(requireNonNull(newKnots)));
    }

    /**
     * Create and return a new {@link BasicNurbsSurface NURBS surface} that is
     * geometrically identical to this one but with the specified list of knotsU inserted
     * into it's T-knot vector. The parameterization of the new surface is identical to
     * this surface, but the new surface will have the new knotsU at the specified
     * locations in the T-direction and a new set of control points. This is more
     * efficient than repeatedly calling insertTKnot() for multiple knot insertions.
     *
     * @param newKnots The parametric T-locations where the knotsU are to be inserted. May
     *                 not be null.
     * @return A new NURBS surface that is identical to this one, but with the new knotsU
     *         inserted at the specified locations in the parametric T-direction.
     */
    public BasicNurbsSurface refineTKnotVector(Float64Vector newKnots) {

        //  Check on the validity of the new knotsU.
        int r = newKnots.getDimension() - 1;
        double tOld = 0;
        for (int i = 0; i <= r; ++i) {
            double t = newKnots.getValue(i);
            validateParameter(0,t);
            if (t < tOld) {
                throw new IllegalArgumentException(RESOURCES.getString("knotsNotIncreasingErr"));
            }
            tOld = t;
        }

        StackContext.enter();
        try {
            KnotVector kv = null;
            FastTable<FastTable<ControlPoint>> cpNetList = FastTable.newInstance();
            int numCols = getNumberOfColumns() + newKnots.getDimension();

            //  Create an empty list of lists of control points.
            int numRows = getNumberOfRows();
            for (int i = 0; i < numCols; ++i) {
                FastTable<ControlPoint> tbl = FastTable.newInstance();
                tbl.setSize(numRows);
                cpNetList.add(tbl);
            }

            //  Add the knot to each defining row curve.
            for (int j = 0; j < numRows; ++j) {
                BasicNurbsCurve c1 = getTCurve(j);
                BasicNurbsCurve c2 = c1.refineKnotVector(newKnots);
                kv = c2.getKnotVector();
                List<ControlPoint> cps = c2.getControlPoints();
                for (int i = 0; i < numCols; ++i) {
                    cpNetList.get(i).set(j, cps.get(i));
                }
            }

            //  Create a new surface from the new knot vector and set of control points.
            BasicNurbsSurface srf = BasicNurbsSurface.newInstance(ControlPointNet.valueOf(cpNetList), getSKnotVector(), kv);
            return StackContext.outerCopy(srf);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Attempts to remove the knot in the S-direction with the specified index from this
     * NURBS surface the specified number of times. If the knot can not be removed without
     * changing the shape of the surface, then a reference to this surface is returned:
     * <code>if (srf == srf.removeKnot(...)) { no knotsU removed }</code>.
     *
     * @param index       The index of the knot to be removed (degreeU + 1 &le; index &lt;
     *                    knot vector length - degreeU).
     * @param numRemovals The number of times that the knot at "index" should be removed
     *                    (1 &le; num &le; multiplicity of the knot). If numRemovals is
     *                    &gt; multiplicity of the knot, then numRemovals is set to the
     *                    multiplicity of the knot.
     * @param tolerance   The knot(s) specified will be removed as long as the deviation
     *                    of the surface is everywhere less than this tolerance. May not
     *                    be null.
     * @return A new NURBS surface that is identical to this one (to within the specified
     *         tolerance), but with the specified knot in the S-direction removed or a
     *         reference to this surface if the knot could not be removed.
     */
    public NurbsSurface removeSKnot(int index, int numRemovals, Parameter<Length> tolerance) {
        requireNonNull(tolerance);
        
        KnotVector kv = null;
        FastTable<List<ControlPoint>> cpNetList = FastTable.newInstance();

        //  Try to remove a knot from each defining column curve.
        int numCols = getNumberOfColumns();
        for (int i = 0; i < numCols; ++i) {
            BasicNurbsCurve c1 = getSCurve(i);
            BasicNurbsCurve c2 = (BasicNurbsCurve)c1.removeKnot(index, numRemovals, tolerance);
            if (c2 == c1)
                break;
            kv = c2.getKnotVector();
            cpNetList.add(c2.getControlPoints());
        }

        //  If we weren't successfull, then just return a reference to this surface.
        NurbsSurface srf = this;

        //  Were we successfull at removing the knot from all the defining sections?
        if (cpNetList.size() == numCols) {
            //  Create a new surface from the new knot vector and set of control points.
            srf = BasicNurbsSurface.newInstance(ControlPointNet.valueOf(cpNetList), kv, getTKnotVector());
        }

        return srf;
    }

    /**
     * Attempts to remove the knot in the T-direction with the specified index from this
     * NURBS surface the specified number of times. If the knot can not be removed without
     * changing the shape of the surface, then a reference to this surface is returned:
     * <code>if (srf == srf.removeKnot(...)) { no knotsU removed }</code>.
     *
     * @param index       The index of the knot to be removed (degreeU + 1 &le; index &lt;
     *                    knot vector length - degreeU).
     * @param numRemovals The number of times that the knot at "index" should be removed
     *                    (1 &le; num &le; multiplicity of the knot). If numRemovals is
     *                    &gt; multiplicity of the knot, then numRemovals is set to the
     *                    multiplicity of the knot.
     * @param tolerance   The knot(s) specified will be removed as long as the deviation
     *                    of the surface is everywhere less than this tolerance. May not
     *                    be null.
     * @return A new NURBS surface that is identical to this one (to within the specified
     *         tolerance), but with the specified knot in the T-direction removed or a
     *         reference to this surface if the knot could not be removed.
     */
    public NurbsSurface removeTKnot(int index, int numRemovals, Parameter<Length> tolerance) {
        requireNonNull(tolerance);
        
        KnotVector kv = null;
        FastTable<FastTable<ControlPoint>> cpNetList = FastTable.newInstance();
        int numCols = getNumberOfColumns() + 1;

        //  Create an empty list of lists of control points.
        int numRows = getNumberOfRows();
        for (int i = 0; i < numCols; ++i) {
            FastTable<ControlPoint> tbl = FastTable.newInstance();
            tbl.setSize(numRows);
            cpNetList.add(tbl);
        }

        //  Try to remove a knot from each defining row curve.
        boolean successfull = true;
        for (int j = 0; j < numRows; ++j) {
            BasicNurbsCurve c1 = getTCurve(j);
            BasicNurbsCurve c2 = (BasicNurbsCurve)c1.removeKnot(index, numRemovals, tolerance);
            if (c2 != c1) {
                kv = c2.getKnotVector();
                List<ControlPoint> cps = c2.getControlPoints();
                for (int i = 0; i < numCols; ++i) {
                    cpNetList.get(i).set(j, cps.get(i));
                }

            } else {
                successfull = false;
                break;
            }

        }

        //  If we weren't successfull, then just return a reference to this surface.
        NurbsSurface srf = this;

        //  Were we successfull at removing the knot from all the defining sections?
        if (successfull) {
            //  Create a new surface from the new knot vector and set of control points.
            srf = BasicNurbsSurface.newInstance(ControlPointNet.valueOf(cpNetList), getSKnotVector(), kv);
        }

        return srf;
    }

    /**
     * Guess an initial set of points to use when gridding to tolerance. The initial
     * points must include each corner of the surface as well as any discontinuities in
     * the surface. Other than that, they should be distributed as best as possible to
     * indicate areas of higher curvature, but should be generated as efficiently as
     * possible.
     * <p>
     * This implementation returns the start and end points of each Bezier surface patch.
     * It also uses the surface boundary curves as guides for inserting additional
     * interior points.
     * </p>
     *
     * @param tol The maximum distance that a straight line between the final set of
     *            gridded points may deviate from this surface. May not be null or zero.
     * @return An array of subrange points to use as a starting point for the grid to
     *         tolerance algorithms.
     */
    @Override
    protected PointArray<SubrangePoint> guessGrid2TolPoints(Parameter<Length> tol) {
        requireNonNull(tol);
        
        //  Create a list of Bezier segment starting parametric positions ( and the last end position, 1).
        FastTable<FastTable<Double>> params = getPatchParameters();
        FastTable<Double> bParamsS = params.get(0);
        FastTable<Double> bParamsT = params.get(1);
        FastTable.recycle(params);

        //  If there are only start and end points, insert 0.5 to ensure there is always at least one intermediate point.
        if (bParamsS.size() < 3) {
            bParamsS.add(1, 0.5);
        }
        if (bParamsT.size() < 3) {
            bParamsT.add(1, 0.5);
        }

        if (getSDegree() > 1 || getTDegree() > 1) {
            StackContext.enter();
            try {
                //  Extract edge curves and use their parameterizations as a 1st guess.

                BasicNurbsCurve s0crv = (BasicNurbsCurve)this.getS0Curve();
                enrichParPosition(bParamsT, s0crv, tol);

                BasicNurbsCurve s1crv = (BasicNurbsCurve)this.getS1Curve();
                enrichParPosition(bParamsT, s1crv, tol);

                BasicNurbsCurve t0crv = (BasicNurbsCurve)this.getT0Curve();
                enrichParPosition(bParamsS, t0crv, tol);

                BasicNurbsCurve t1crv = (BasicNurbsCurve)this.getT1Curve();
                enrichParPosition(bParamsS, t1crv, tol);

            } finally {
                StackContext.exit();
            }
        }

        //  Turn the lists of parametric positions into an array of subrange points.
        PointArray<SubrangePoint> pntArray = PointArray.newInstance();
        for (double t : bParamsT) {
            PointString<SubrangePoint> str = PointString.newInstance();
            for (double s : bParamsS) {
                SubrangePoint pnt = SubrangePoint.newInstance(this, Point.valueOf(s, t));
                str.add(pnt);
            }
            pntArray.add(str);
        }

        FastTable.recycle(bParamsS);
        FastTable.recycle(bParamsT);

        return pntArray;
    }

    /**
     * Enrich an existing list of parametric positions with those extracted from a curve
     * gridded to the input tolerance.
     *
     * @param parPosLst The existing list of parametric positions on a curve.
     * @param crv       The curve to extract additional parametric positions using the
     *                  specified tolerance.
     * @param tol       The tolerance to use when gridding the specified curve.
     */
    private static void enrichParPosition(List<Double> parPosLst, Curve crv, Parameter<Length> tol) {
        //  Grid the curve to tolerance.
        PointString<SubrangePoint> subPnts = crv.gridToTolerance(tol);

        //  Loop over all the curve points and extract the parametric positions.
        int numPnts = subPnts.size() - 1;
        for (int i = 1; i < numPnts; ++i) {
            SubrangePoint pi = subPnts.get(i);
            double ti = pi.getParPosition().getValue(0);

            //  Add the curve parametric position to the existing list of parametric positions if it is new.
            int idx = Collections.binarySearch(parPosLst, ti);  //  Search for the desired value.
            if (idx < 0) {
                //  The value is not in the list, so insert it.
                idx = -(idx + 1);   //  Convert output from binarySearch to insertion point index.
                parPosLst.add(idx, ti);
            }
        }
    }

    /**
     * Method that guesses the most likely location for the closest or farthest point on a
     * surface and returns that location as a 2D point containing the "s" and "t"
     * parameter values. This is called by getClosest() and getFarthest(). This is
     * required in order for the root-finding algorithm to reliably refine the closest
     * point to the correct location.
     *
     * @param point   The point to find the closest point on this surface to. May not be null.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The 2D parametric point on this surface that is closest to the specified
     *         point.
     */
    @Override
    protected GeomPoint guessClosestFarthest(GeomPoint point, boolean closest) {
        requireNonNull(point);
        //  Inspired by: Ma, Hewitt, "Point inversion and project for NURBS curve and surface", CAGD, 2003.

        //  Create a list of Bezier segment starting parametric positions ( and the last end position, 1).
        FastTable<FastTable<Double>> params = getPatchParameters();
        FastTable<Double> bParamsS = params.get(0);
        FastTable<Double> bParamsT = params.get(1);
        FastTable.recycle(params);

        //  Decompose the surface into Bezier patches.
        GeomList<GeomList<BasicNurbsSurface>> patches = SurfaceUtils.decomposeToBezier(this);

        //  Create a list of potential closest points.
        FastTable<SubrangePoint> potentials = FastTable.newInstance();

        //  Loop over all the Bezier patches.
        int column = 0;
        int ncols = patches.size();
        for (int i = 0; i < ncols; ++i) {
            GeomList<BasicNurbsSurface> patchCol = patches.get(i);
            double t0 = bParamsT.get(column);
            double tWidth = bParamsT.get(column + 1) - t0;

            int row = 0;
            int nrows = patchCol.size();
            for (int j = 0; j < nrows; ++j) {
                BasicNurbsSurface patch = patchCol.get(j);
                double s0 = bParamsS.get(row);
                double sWidth = bParamsS.get(row + 1) - s0;

                //  Determine if this patch is a "valid" patch (simple and convex).
                ControlPointNet patchCPNet = patch.getControlPoints();
                if (isValidCPNet(patchCPNet)) {
                    //  Determine if the boundary curves are closest to the target point.
                    if (isBezierEdgeCurveNearest(point, patchCPNet)) {

                        //  One of the boundary curves is nearest.
                        potentials.add(closestFarthestEdgeCurve(patch, s0, t0, sWidth, tWidth, point, closest));

                    } else {
                        //  An interior point is nearest.
                        potentials.add(closestFarthestInterior(patch, s0, t0, sWidth, tWidth, point, closest));
                    }

                } else {
                    //  Handle invalid patches by just searching for interior points.
                    potentials.add(closestFarthestInterior(patch, s0, t0, sWidth, tWidth, point, closest));
                }

                BasicNurbsSurface.recycle(patch);
                ++row;
            }
            GeomList.recycle(patchCol);
            ++column;
        }
        GeomList.recycle(patches);
        FastTable.recycle(bParamsS);
        FastTable.recycle(bParamsT);

        //  Find the closest/farthest of all the potential points.
        SubrangePoint pOpt = closestFarthestInList(potentials, point, closest);
        FastTable.recycle(potentials);

        //  Extract the parametric position of the closest point.
        GeomPoint parPos = pOpt.getParPosition();

        //  Return the parametric position of the best guess at the closest/farthest point.
        return parPos;
    }

    /**
     * Method that guesses the most likely location for the closest or farthest point on a
     * surface and returns that location as a 2D point containing the "s" and "t"
     * parameter values. This is called by getClosest() and getFarthest(). This is
     * required in order for the root-finding algorithm to reliably refine the closest
     * point to the correct location.
     *
     * @param plane   The plane to find the closest point on this surface to. May not be null.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The 2D parametric point on this surface that is closest to the specified
     *         plane.
     */
    @Override
    protected GeomPoint guessClosestFarthest(GeomPlane plane, boolean closest) {
        requireNonNull(plane);
        
        //  Create a list of Bezier segment starting parametric positions ( and the last end position, 1).
        FastTable<FastTable<Double>> params = getPatchParameters();
        FastTable<Double> bParamsS = params.get(0);
        FastTable<Double> bParamsT = params.get(1);
        FastTable.recycle(params);

        //  Decompose the surface into Bezier patches.
        GeomList<GeomList<BasicNurbsSurface>> patches = SurfaceUtils.decomposeToBezier(this);

        //  Create a list of potential closest points.
        FastTable<SubrangePoint> potentials = FastTable.newInstance();

        //  Loop over all the Bezier patches.
        int column = 0;
        int ncols = patches.size();
        for (int i = 0; i < ncols; ++i) {
            GeomList<BasicNurbsSurface> patchCol = patches.get(i);
            double t0 = bParamsT.get(column);
            double tWidth = bParamsT.get(column + 1) - t0;

            int row = 0;
            int nrows = patchCol.size();
            for (int j = 0; j < nrows; ++j) {
                BasicNurbsSurface patch = patchCol.get(j);
                double s0 = bParamsS.get(row);
                double sWidth = bParamsS.get(row + 1) - s0;

                //  Just search for interior points.
                potentials.add(closestFarthestInterior(patch, s0, t0, sWidth, tWidth, plane, closest));

                BasicNurbsSurface.recycle(patch);
                ++row;
            }
            GeomList.recycle(patchCol);
            ++column;
        }
        GeomList.recycle(patches);
        FastTable.recycle(bParamsS);
        FastTable.recycle(bParamsT);

        //  Find the closest/farthest of all the potential points.
        SubrangePoint pOpt = closestFarthestInList(potentials, plane, closest);
        FastTable.recycle(potentials);

        //  Extract the parametric position of the closest point.
        GeomPoint parPos = pOpt.getParPosition();

        //  Return the parametric position of the best guess at the closest/farthest point.
        return parPos;
    }

    /**
     * Determine if the provided control point network is "valid" (simple and and convex)
     * in both the S and T directions.
     *
     * @param cpNet THe control point network to evaluate.
     */
    private static boolean isValidCPNet(ControlPointNet cpNet) {
        //  Algorithm 2 from Ma, Hewitt, "Point inversion and project for NURBS curve and surface", CAGD, 2003.

        StackContext.enter();
        try {
            //  Detect every control polygon in S direction
            int m = cpNet.getNumberOfColumns();
            for (int i = 0; i < m; ++i) {
                List<ControlPoint> P = cpNet.getColumn(i);      //  Generate a control polygon

                if (!CurveUtils.isSimpleConvexPolygon(P)) {
                    //  Control point net is not valid
                    return false;
                }
            }

            //  Detect every control polygon in T direction
            int n = cpNet.getNumberOfRows();
            for (int i = 0; i < n; ++i) {
                List<ControlPoint> P = cpNet.getRow(i);         //  Generate a control polygon

                if (!CurveUtils.isSimpleConvexPolygon(P)) {
                    //  Control point net is not valid
                    return false;
                }
            }

            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns <code>true</code> if a boundary curve point is the closest/farthest point
     * in the specified Bezier patch to the target point. Returns <code>false</code> if
     * the closest/farthest point may be interior to the Bezier patch.
     *
     * @param point The point to find the closest/farthest point on this surface to/from.
     * @param cpNet The control point network for the Bezier patch.
     * @return <code>true</code> if a boundary curve point of the specified patch is
     *         closest to the target point.
     */
    private static boolean isBezierEdgeCurveNearest(GeomPoint point, ControlPointNet cpNet) {
        //  Algorithm 4 from Ma, Hewitt, "Point inversion and project for NURBS curve and surface", CAGD, 2003
        //  (with logic reversed).
        //StackContext.enter();
        try {
            boolean flag = true;

            //  Detect every control polygon in S direction
            int m = cpNet.getNumberOfColumns();
            for (int i = 0; i < m; ++i) {
                List<ControlPoint> P = cpNet.getColumn(i);      //  Generate a control polygon

                if (!CurveUtils.isBezierEndPointNearest(point, P)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                return true;    //  The nearest point is the point on the boundary curves
            }
            //  Detect every control polygon in T direction
            int n = cpNet.getNumberOfRows();
            for (int i = 0; i < n; ++i) {
                List<ControlPoint> P = cpNet.getRow(i);         //  Generate a control polygon

                if (!CurveUtils.isBezierEndPointNearest(point, P)) {
                    flag = false;
                    break;
                }
            }

            return flag;

        } finally {
            //StackContext.exit();
        }
    }

    /**
     * Find the closest point among the edge curves of the specified Bezier patch and
     * create a subrange point on this surface from that point and return it.
     *
     * @param patch   The Bezier patch to search the edge curves for.
     * @param s0      The parametric S position on this surface of the 0,0 corner of
     *                patch.
     * @param t0      The parametric T position on this surface of the 0,0 corner of
     *                patch.
     * @param sWidth  The parametric width of the patch in the S-direction.
     * @param tWidth  The parametric width of the patch in the T-direction.
     * @param point   The point to find the closest/farthest point on this surface
     *                to/from.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @param A       subrange point on this surface corresponding to the closest boundary
     *                point on the patch.
     */
    private SubrangePoint closestFarthestEdgeCurve(BasicNurbsSurface patch,
            double s0, double t0, double sWidth, double tWidth, GeomPoint point, boolean closest) {

        FastTable<SubrangePoint> potentials = FastTable.newInstance();

        if (closest) {
            //  Looking for closest point.

            //  S=0 side.
            Curve c = patch.getS0Curve();
            GeomPoint parPos = c.getClosest(point, GTOL).getParPosition();
            potentials.add(getPoint(s0, parPos.getValue(0) * tWidth + t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

            //  S=1 side.
            c = patch.getS1Curve();
            parPos = c.getClosest(point, GTOL).getParPosition();
            potentials.add(getPoint(sWidth + s0, parPos.getValue(0) * tWidth + t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

            //  T=0 side.
            c = patch.getT0Curve();
            parPos = c.getClosest(point, GTOL).getParPosition();
            potentials.add(getPoint(parPos.getValue(0) * sWidth + s0, t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

            //  T=1 side.
            c = patch.getT1Curve();
            parPos = c.getClosest(point, GTOL).getParPosition();
            potentials.add(getPoint(parPos.getValue(0) * sWidth + s0, tWidth + t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

        } else {
            //  Looking for farthest point.

            //  S=0 side.
            Curve c = patch.getS0Curve();
            GeomPoint parPos = c.getFarthest(point, GTOL).getParPosition();
            potentials.add(getPoint(s0, parPos.getValue(0) * tWidth + t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

            //  S=1 side.
            c = patch.getS1Curve();
            parPos = c.getFarthest(point, GTOL).getParPosition();
            potentials.add(getPoint(sWidth + s0, parPos.getValue(0) * tWidth + t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

            //  T=0 side.
            c = patch.getT0Curve();
            parPos = c.getFarthest(point, GTOL).getParPosition();
            potentials.add(getPoint(parPos.getValue(0) * sWidth + s0, t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);

            //  T=1 side.
            c = patch.getT1Curve();
            parPos = c.getFarthest(point, GTOL).getParPosition();
            potentials.add(getPoint(parPos.getValue(0) * sWidth + s0, tWidth + t0));
            BasicNurbsCurve.recycle((BasicNurbsCurve)c);
        }

        //  Find the closest/farthest of all the potential points.
        SubrangePoint pOpt = closestFarthestInList(potentials, point, closest);
        FastTable.recycle(potentials);

        return pOpt;
    }

    /**
     * Find the closest point on a grid of points that are interior to the specified
     * Bezier patch and create a subrange point on this surface from that point and add
     * that to the potentials list.
     *
     * @param patch   The Bezier patch to search.
     * @param s0      The parametric S position on this surface of the 0,0 corner of
     *                patch.
     * @param t0      The parametric T position on this surface of the 0,0 corner of
     *                patch.
     * @param sWidth  The parametric width of the patch in the S-direction.
     * @param tWidth  The parametric width of the patch in the T-direction.
     * @param point   The point to find the closest/farthest point on this surface
     *                to/from.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @param A       subrange point on this surface corresponding to the closest grid
     *                point on the patch.
     */
    private SubrangePoint closestFarthestInterior(BasicNurbsSurface patch,
            double s0, double t0, double sWidth, double tWidth, GeomPoint point, boolean closest) {

        //  Find the closest grid point on the patch.
        int gridSize = MathLib.max(patch.getSDegree(), patch.getTDegree()) + 2;
        SubrangePoint p = getClosestFarthestOnGrid(patch, point, closest, gridSize);

        //  Convert to a subrange point on this surface and add to potentials list.
        GeomPoint parPos = p.getParPosition();
        p = getPoint(parPos.getValue(0) * sWidth + s0, parPos.getValue(1) * tWidth + t0);

        return p;
    }

    /**
     * Find the closest point on a grid of points that are interior to the specified
     * Bezier patch and create a subrange point on this surface from that point and add
     * that to the potentials list.
     *
     * @param patch   The Bezier patch to search.
     * @param s0      The parametric S position on this surface of the 0,0 corner of
     *                patch.
     * @param t0      The parametric T position on this surface of the 0,0 corner of
     *                patch.
     * @param sWidth  The parametric width of the patch in the S-direction.
     * @param tWidth  The parametric width of the patch in the T-direction.
     * @param plane   The plane to find the closest/farthest point on this surface
     *                to/from.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @param A       subrange point on this surface corresponding to the closest grid
     *                point to the plane on the patch.
     */
    private SubrangePoint closestFarthestInterior(BasicNurbsSurface patch,
            double s0, double t0, double sWidth, double tWidth, GeomPlane plane, boolean closest) {

        //  Find the closest grid point on the patch.
        int gridSize = MathLib.max(patch.getSDegree(), patch.getTDegree()) + 2;
        SubrangePoint p = getClosestFarthestOnGrid(patch, plane, closest, gridSize);

        //  Convert to a subrange point on this surface and add to potentials list.
        GeomPoint parPos = p.getParPosition();
        p = getPoint(parPos.getValue(0) * sWidth + s0, parPos.getValue(1) * tWidth + t0);

        return p;
    }

    /**
     * Return the closest/farthest point in the specified list of points to the specified
     * point.
     *
     * @param pointList The list of subrange points to search.
     * @param point     The point to find the closest/farthest point on this surface
     *                  to/from.
     * @param closest   Set to <code>true</code> to return the closest point or
     *                  <code>false</code> to return the farthest point.
     * @return The closest/farthest point in the list to/from the target point.
     */
    private static SubrangePoint closestFarthestInList(List<SubrangePoint> pointList, GeomPoint point, boolean closest) {

        SubrangePoint pOpt = pointList.get(0);
        double dOpt = pOpt.distanceValue(point);
        int size = pointList.size();
        for (int i = 1; i < size; ++i) {
            SubrangePoint p = pointList.get(i);
            double dist = p.distanceValue(point);
            if (closest) {
                if (dist < dOpt) {
                    dOpt = dist;
                    pOpt = p;
                }
            } else {
                if (dist > dOpt) {
                    dOpt = dist;
                    pOpt = p;
                }
            }
        }

        return pOpt;
    }

    /**
     * Return the closest/farthest point in the specified list of points to the specified
     * plane.
     *
     * @param pointList The list of subrange points to search.
     * @param plane     The plane to find the closest/farthest point on this surface
     *                  to/from.
     * @param closest   Set to <code>true</code> to return the closest point or
     *                  <code>false</code> to return the farthest point.
     * @return The closest/farthest point in the list to/from the target plane.
     */
    private static SubrangePoint closestFarthestInList(List<SubrangePoint> pointList, GeomPlane plane, boolean closest) {

        SubrangePoint pOpt = pointList.get(0);
        double dOpt = pOpt.distanceValue(plane.getClosest(pOpt));
        int size = pointList.size();
        for (int i = 1; i < size; ++i) {
            SubrangePoint p = pointList.get(i);
            double dist = p.distanceValue(plane.getClosest(p));
            if (closest) {
                if (dist < dOpt) {
                    dOpt = dist;
                    pOpt = p;
                }
            } else {
                if (dist > dOpt) {
                    dOpt = dist;
                    pOpt = p;
                }
            }
        }

        return pOpt;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link geomss.geom.GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public NurbsSurfaceTrans getTransformed(GTransform transform) {
        return NurbsSurfaceTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Return a NURBS surface representation of this surface to within the specified
     * tolerance. This implementation returns a reference to this surface itself since it
     * is already a NURBS surface and the tolerance parameter is ignored.
     *
     * @param tol Ignored. May pass <code>null</code>.
     * @return A reference to this NURBS surface is returned.
     */
    @Override
    public NurbsSurface toNurbs(Parameter<Length> tol) {
        return this;
    }

    /**
     * Return a NURBS surface representation of this surface. An exact representation is
     * returned.
     *
     * @return A reference to this NURBS surface is returned.
     */
    public NurbsSurface toNurbs() {
        return toNurbs(null);
    }

    /**
     * Return <code>true</code> if this NurbsSurface contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the control
     * point values or knotsU are NaN or Inf.
     *
     * @return true if this NurbsSurface contains valid and finite numerical components.
     */
    @Override
    public boolean isValid() {

        //  Check the knot vectors.
        KnotVector kv = getSKnotVector();
        if (!kv.isValid())
            return false;
        kv = getTKnotVector();
        if (!kv.isValid())
            return false;

        //  Check the control points.
        ControlPointNet net = getControlPoints();

        return net.isValid();
    }

    /**
     * Return <code>true</code> if this surface is degenerate (i.e.: has area less than
     * the specified tolerance squared).
     *
     * @param tol The tolerance for determining if this surface is degenerate. May not be
     *            null.
     * @return true if this surface is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        //  The surface has zero area if it's control point network has zero area.

        requireNonNull(tol);
        StackContext.enter();
        try {
            ControlPointNet cpNet = getControlPoints();

            int ncols = cpNet.getNumberOfColumns();
            for (int j = 0; j < ncols; ++j) {
                List<ControlPoint> Pw = cpNet.getColumn(j);
                Parameter<Length> length = Parameter.ZERO_LENGTH.to(getUnit());
                int size = Pw.size();
                Point Pim1 = Pw.get(0).getPoint();
                for (int i = 1; i < size; ++i) {
                    Point Pi = Pw.get(i).getPoint();
                    length = length.plus(Pim1.distance(Pi));
                    Pim1 = Pi;
                }
                //  If any column of the control point network is not zero length,
                //  then the surface is not degenerate.
                if (length.isGreaterThan(tol))
                    return false;
            }

            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return <code>true</code> if this surface is planar or <code>false</code> if it is
     * not.
     *
     * @param tol The geometric tolerance to use in determining if the surface is planar.
     *            May not be null.
     * @return true if this surface is planar.
     */
    @Override
    public boolean isPlanar(Parameter<Length> tol) {
        requireNonNull(tol);
        StackContext.enter();
        try {
            //  If the surface is less than 3D, then it must be planar.
            int numDims = getPhyDimension();
            if (numDims <= 2)
                return true;

            //  If the surface is degenerate (zero area; a point or curve), then check edge curves for being planar.
            if (isDegenerate(tol)) {
                Curve sCrv = getT0Curve();
                Curve tCrv = getS0Curve();
                if (sCrv.isPlanar(tol) && tCrv.isPlanar(tol)) {
                    return true;
                }
            }

            //  Form a plane from the diagonals of the surface's control points.
            ControlPointNet cpNet = getControlPoints();
            int nSm1 = cpNet.getNumberOfRows() - 1;
            int nTm1 = cpNet.getNumberOfColumns() - 1;
            Point p0 = cpNet.get(0, 0).getPoint();
            Point p1 = cpNet.get(nSm1, nTm1).getPoint();
            Vector<Length> d1 = p1.minus(p0).toGeomVector();
            p0 = cpNet.get(nSm1, 0).getPoint();
            p1 = cpNet.get(0, nTm1).getPoint();
            Vector<Length> d2 = p1.minus(p0).toGeomVector();
            Vector<Dimensionless> nhat = d1.cross(d2).toUnitVector();
            nhat.setOrigin(Point.newInstance(numDims));

            //  The curve is planar if the control points are co-planar.
            int ncols = cpNet.getNumberOfColumns();
            for (int i = 0; i < ncols; ++i) {
                List<ControlPoint> col = cpNet.getColumn(i);
                int nrows = col.size();
                for (int j = 0; j < nrows; ++j) {
                    ControlPoint cp = col.get(j);
                    Point p = cp.getPoint();
                    p1 = GeomUtil.pointPlaneClosest(p, p0, nhat);
                    if (p.distance(p1).isGreaterThan(tol))
                        return false;
                }
            }

            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a list of lists containing parameters at the start of logical patches of
     * this surface in each parametric direction. The first list contains the parameters
     * in the "S" direction and the 2nd in the "T" direction. The first element in each
     * list must always be 0.0 and the last element 1.0. These should be good places to
     * break the surface up into pieces for analysis, but otherwise are arbitrary.
     * Subclasses should override this method to provide better patch starting parameters.
     *<p>
     * This implementation returns the parameters at the start of each Bezier patch of
     * this NURBS surface.
     *</p>
     *
     * @return A list of lists containing parameters at the start of logical patches of
     *         this surface in each parametric direction.
     */
    @Override
    protected FastTable<FastTable<Double>> getPatchParameters() {
        FastTable<FastTable<Double>> lsts = FastTable.newInstance();
        lsts.add(SurfaceUtils.getBezierStartParameters(this, 0));
        lsts.add(SurfaceUtils.getBezierStartParameters(this, 1));
        return lsts;
    }

    /**
     * Return the number of points per patch in the "S" direction that should be used when
     * analyzing surface patches by certain methods.
     *<p>
     * This implementation always returns 2*(p + 1) where p is the degreeU of the surface
     * in the "S" direction.
     *</p>
     *
     * @return The number of points per patch in the "S" direction that should be used
     *         when analyzing surface patches.
     */
    @Override
    protected int getNumPointsPerPatchS() {
        return 2 * (getSDegree() + 1);
    }

    /**
     * Return the number of points per patch in the "T" direction that should be used when
     * analyzing surface patches by certain methods.
     *<p>
     * This implementation always returns 2*(p + 1) where p is the degreeU of the surface
     * in the "T" direction.
     *</p>
     *
     * @return The number of points per patch in the "T" direction that should be used
     *         when analyzing surface patches.
     */
    @Override
    protected int getNumPointsPerPatchT() {
        return 2 * (getTDegree() + 1);
    }

    /**
     * Returns the text representation of this geometry element that consists of the name
     * followed by the control point values, followed by the knot vectors.
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }

        ControlPointNet cpNet = getControlPoints();
        tmp.append(cpNet.toText());
        tmp.append(getSKnotVector().toText());
        tmp.append(getTKnotVector().toText());

        if (hasName) {
            tmp.append('}');
        }
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<NurbsSurface> XML = new XMLFormat<NurbsSurface>(NurbsSurface.class) {

        @Override
        public void read(XMLFormat.InputElement xml, NurbsSurface obj) throws XMLStreamException {
            String name = xml.getAttribute("name", (String)null);
            if (nonNull(name))
                obj.setName(name);
            FastMap userData = xml.get("UserData", FastMap.class);
            if (nonNull(userData)) {
                obj.putAllUserData(userData);
                FastMap.recycle(userData);
            }
        }

        @Override
        public void write(NurbsSurface obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            String name = obj.getName();
            if (nonNull(name))
                xml.setAttribute("name", name);
            FastMap userData = (FastMap)obj.getAllUserData();
            if (!userData.isEmpty())
                xml.add(userData, "UserData", FastMap.class);
            FastMap.recycle(userData);
        }
    };

}
