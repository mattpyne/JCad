/*
 *   ControlPoint  -- Holds the floating point coordinates of a NURBS control point in nD space.
 *
 *   Copyright (C) 2009-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.nurbs;

import geomss.geom.DimensionException;
import geomss.geom.Point;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.MathLib;
import javolution.lang.ValueType;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A container that holds the coordinates of a NURBS control point in n-dimensional space.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 14, 2009
 * @version September 9, 2016
 */
@SuppressWarnings("serial")
public class ControlPoint implements Cloneable, XMLSerializable, ValueType {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = geomss.geom.AbstractGeomElement.RESOURCES;

    /**
     * The coordinates of the physical point represented by this control point.
     */
    private Point _point;

    /**
     * The weighting factor.
     */
    private double _weight;

    /**
     * Returns a {@link ControlPoint} instance of the specified dimension with zero meters
     * for each coordinate value and zero weight.
     *
     * @param dim the physical dimension of the point to create (not including the
     *            weighting factor).
     * @return the point having the specified dimension and zero meters for values.
     */
    public static ControlPoint newInstance(int dim) {
        ControlPoint P = ControlPoint.newInstance(Point.newInstance(dim), 0);
        return P;
    }

    /**
     * Returns a {@link ControlPoint} instance of the specified dimension and units with
     * zero for each coordinate value and zero weight.
     *
     * @param dim  the physical dimension of the point to create (not including the
     *             weighting factor).
     * @param unit the unit for the point coordinate values. May not be null.
     * @return the point having the specified dimension & unit and zero for values.
     */
    public static ControlPoint newInstance(int dim, Unit<Length> unit) {
        ControlPoint P = ControlPoint.newInstance(Point.newInstance(dim, requireNonNull(unit)), 0);
        return P;
    }

    /**
     * Returns a 2D {@link ControlPoint} instance holding the specified
     * <code>double</code> values stated in meters.
     *
     * @param x the x value stated in meters.
     * @param y the y value stated in meters.
     * @param w the weighting factor for the control point.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(double x, double y, double w) {
        return valueOf(x, y, w, SI.METER);
    }

    /**
     * Returns a 2D {@link ControlPoint} instance holding the specified
     * <code>double</code> values stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param w    the weighting factor for the control point.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(double x, double y, double w, Unit<Length> unit) {
        ControlPoint P = ControlPoint.newInstance(Point.valueOf(x, y, requireNonNull(unit)), w);
        return P;
    }

    /**
     * Returns a 3D {@link ControlPoint} instance holding the specified
     * <code>double</code> values stated in meters.
     *
     * @param x the x value stated in meters.
     * @param y the y value stated in meters.
     * @param z the z value stated in meters.
     * @param w the weighting factor for the control point.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(double x, double y, double z, double w) {
        return valueOf(x, y, z, w, SI.METER);
    }

    /**
     * Returns a 3D {@link ControlPoint} instance holding the specified
     * <code>double</code> values stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param z    the z value stated in meters.
     * @param w    the weighting factor for the control point.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(double x, double y, double z, double w, Unit<Length> unit) {
        ControlPoint P = ControlPoint.newInstance(Point.valueOf(x, y, z, requireNonNull(unit)), w);
        return P;
    }

    /**
     * Returns a {@link ControlPoint} instance holding the specified <code>Float64</code>
     * values stated in the specified units.
     *
     * @param vector the vector of Float64 values stated with all but the last stated in
     *               the specified unit (the last is assumed to be the weight). May not be
     *               null.
     * @param unit   the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(org.jscience.mathematics.vector.Vector<Float64> vector, Unit<Length> unit) {
        requireNonNull(unit);
        FastTable<Float64> v = FastTable.newInstance();
        int dims = vector.getDimension() - 1;
        for (int i = 0; i < dims; ++i) {
            v.add(vector.get(i));
        }
        Float64Vector f64v = Float64Vector.valueOf(v);
        ControlPoint P = ControlPoint.newInstance(Point.valueOf(f64v, unit), vector.get(dims).doubleValue());
        return P;
    }

    /**
     * Returns a {@link ControlPoint} instance holding the specified <code>Float64</code>
     * values plus weight stated in the specified units.
     *
     * @param vector the vector of Float64 values representing the geometric point stated
     *               in the specified unit. May not be null.
     * @param w      the weighting factor for the control point.
     * @param unit   the unit in which the coordinates are stated. May not be null.
     * @return the control point having the specified values.
     */
    public static ControlPoint valueOf(org.jscience.mathematics.vector.Vector<Float64> vector, double w, Unit<Length> unit) {
        requireNonNull(vector);
        requireNonNull(unit);
        Float64Vector f64v = Float64Vector.valueOf(vector);
        ControlPoint P = ControlPoint.newInstance(Point.valueOf(f64v, unit), w);
        return P;
    }

    /**
     * Returns a {@link ControlPoint} instance holding the specified <code>double</code>
     * values stated in the specified units.
     *
     * @param unit   the unit in which the coordinates are stated. May not be null.
     * @param vector the vector of coordinate values with all but the last stated in the
     *               specified unit (the last is assumed to be the weight). May not be
     *               null.
     * @return the control point having the specified values.
     */
    public static ControlPoint valueOf(Unit<Length> unit, double[] vector) {
        requireNonNull(unit);
        FastTable<Float64> v = FastTable.newInstance();
        int dims = vector.length - 1;
        for (int i = 0; i < dims; ++i) {
            v.add(Float64.valueOf(vector[i]));
        }
        Float64Vector f64v = Float64Vector.valueOf(v);
        ControlPoint P = ControlPoint.newInstance(Point.valueOf(f64v, unit), vector[dims]);
        return P;
    }

    /**
     * Returns a {@link ControlPoint} instance containing the specified Point's data.
     *
     * @param point the Point to be copied into a new ControlPoint. May not be null.
     * @param w     the weighting factor for the control point.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(Point point, double w) {
        ControlPoint P = ControlPoint.newInstance(requireNonNull(point), w);
        return P;
    }

    /**
     * Returns a {@link ControlPoint} instance containing the specified ControlPoint's
     * data.
     *
     * @param point the ControlPoint to be copied into a new ControlPoint. May not be
     *              null.
     * @return the point having the specified values.
     */
    public static ControlPoint valueOf(ControlPoint point) {
        return copyOf(requireNonNull(point));
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation returns the dimensions of the point and does not count the weighting
     * factor.
     *
     * @return The physical dimensions of the control points not counting the weighting
     *         factor.
     */
    public int getPhyDimension() {
        return _point.getPhyDimension();
    }

    /**
     * Returns the value of the Parameter in this point as a <code>double</code>, stated
     * in this point's unit.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &gt;
     * getPhyDimension() - 1)</code>
     */
    public double getValue(int i) {
        return _point.getValue(i);
    }

    /**
     * Return the Point representation of this ControlPoint.
     *
     * @return A Point representation of this control point.
     */
    public Point getPoint() {
        return _point;
    }

    /**
     * Return the weight associated with this control point.
     *
     * @return The weighting factor associated with this control point.
     */
    public double getWeight() {
        return _weight;
    }

    /**
     * Return a new control point that is identical to this one, but with the weight
     * changed to the specified value.
     *
     * @param value The new weighting factor.
     * @return A new ControlPoint identical to this one but with the specified weight.
     */
    public ControlPoint changeWeight(double value) {
        ControlPoint cp = ControlPoint.newInstance(_point, value);
        return cp;
    }

    /**
     * Return a copy of this control point with the values made homogeneous (all the
     * geometric point values are divided by the weight).
     *
     * @return A new ControlPoint with the values made homogeneous.
     */
    public ControlPoint getHomogeneous() {
        Point newPoint = _point.divide(_weight);
        return ControlPoint.newInstance(newPoint, _weight);
    }

    /**
     * Returns a new ControlPoint with the geometric point portion of this control point
     * multiplied by the weight.
     *
     * @return A new ControlPoint with the geometric point multiplied by the weight.
     */
    public ControlPoint applyWeight() {
        Point newPoint = _point.times(_weight);
        return ControlPoint.newInstance(newPoint, _weight);
    }

    /**
     * Returns a new control point with the sum of this point with the one specified.
     *
     * @param that the point to be added to this one. May not be null.
     * @return A new ControlPoint with this + that.
     * @throws DimensionException if point dimensions are different.
     */
    public ControlPoint plus(ControlPoint that) {
        Point pnt = _point.plus(that._point);
        double weight = this._weight + that._weight;
        return ControlPoint.newInstance(pnt, weight);
    }

    /**
     * Returns a new control point with the difference between this point with the one
     * specified.
     *
     * @param that the point to be subtracted from this one. May not be null.
     * @return A new ControlPoint with this - that.
     * @throws DimensionException if point dimensions are different.
     */
    public ControlPoint minus(ControlPoint that) {
        Point pnt = _point.minus(that._point);
        double weight = this._weight - that._weight;
        return ControlPoint.newInstance(pnt, weight);
    }

    /**
     * Returns a new control point with the product of this point with the specified
     * coefficient.
     *
     * @param k the coefficient multiplier.
     * @return A new ControlPoint with this*k.
     */
    public ControlPoint times(double k) {
        Point pnt = _point.times(k);
        double weight = _weight * k;
        return ControlPoint.newInstance(pnt, weight);
    }

    /**
     * Returns a new control point with this point divided by the specified divisor.
     *
     * @param divisor the divisor.
     * @return A new ControlPoint with this / divisor.
     */
    public ControlPoint divide(double divisor) {
        return times(1.0 / divisor);
    }

    /**
     * Returns the norm, magnitude, or length value of the vector from the origin to this
     * control point geometric point (square root of the dot product of the
     * origin-to-this-point vector and itself).
     *
     * @return The magnitude of the vector from the origin to this control point.
     *         geometric point.
     */
    public double normValue() {
        return _point.normValue();
    }

    /**
     * Return the distance (in homogeneous coordinates) between this control point and
     * another (this treats the weight simply as another dimension).
     *
     * @param cp The control point to find the homogeneous distance to. May not be null.
     * @return The homogeneous distance to the specified control point from this one.
     */
    public double distance(ControlPoint cp) {
        double d2 = 0;
        int dim = _point.getPhyDimension();
        Point cpPoint = cp._point;
        if (cpPoint.getPhyDimension() != dim)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));

        for (int i = 0; i < dim; ++i) {
            double v = _point.getValue(i) - cpPoint.getValue(i);
            d2 += v * v;
        }
        double v = _weight - cp._weight;
        d2 += v * v;
        return MathLib.sqrt(d2);
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    public Point getBoundsMin() {
        return _point;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    public Point getBoundsMax() {
        return _point;
    }

    /**
     * Return <code>true</code> if this control point contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf (including the weight).
     *
     * @return true if this control point contains valid and finite values.
     */
    public boolean isValid() {
        return _point.isValid() && !(Double.isNaN(_weight) || Double.isInfinite(_weight));
    }

    /**
     * Returns the unit in which this control point is stated.
     *
     * @return The unit in which this control point is stated.
     */
    public Unit<Length> getUnit() {
        return _point.getUnit();
    }

    /**
     * Returns the equivalent to this control point but stated in the specified unit.
     *
     * @param unit The length unit of the control point to be returned. May not be null.
     * @return An equivalent to this control point but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    public ControlPoint to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        return ControlPoint.newInstance(_point.to(unit), _weight);
    }

    /**
     * Returns the values stored in this control point, with the coordinate points stated
     * in this point's unit and the weight tacked onto the end as a Float64Vector.
     *
     * @return A Float64Vector representation of this control point.
     */
    public Float64Vector toFloat64Vector() {
        StackContext.enter();
        try {

            FastTable<Float64> v = FastTable.newInstance();
            Float64Vector p3dv = _point.toFloat64Vector();
            int numDims = p3dv.getDimension();
            for (int i = 0; i < numDims; ++i) {
                v.add(p3dv.get(i));
            }
            v.add(Float64.valueOf(_weight));
            Float64Vector f64v = Float64Vector.valueOf(v);
            return StackContext.outerCopy(f64v);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the values stored in this control point as a Java array, stated in the
     * current {@link #getUnit units} with the weight tacked onto the end of the array.
     *
     * @return A new array with the control point values copied into it with the
     *          weight at the end.
     */
    public double[] toArray() {
        return toArray(new double[this.getPhyDimension()+1]);
    }

    /**
     * Returns the values stored in this control point, with the coordinate point stated in the current
     * {@link #getUnit units} and the weight tacked onto the end, stored in the input Java array.
     *
     * @param array An existing array that has at least as many elements as the physical
     *              dimension of this point + 1 for the weight.
     * @return A reference to the input array after the control point values have been copied over
     *         to it.
     * @see #getPhyDimension()
     */
    public double[] toArray(double[] array) {
        _point.toArray(array);
        array[_point.getPhyDimension()] = _weight;
        return array;
    }

    /**
     * Returns a copy of this ControlPoint instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public ControlPoint copy() {
        return copyOf(this);
    }

    /**
     * Returns a copy of this ControlPoint instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     * @throws java.lang.CloneNotSupportedException Never thrown.
     */
    @Override
    @SuppressWarnings("CloneDoesntCallSuperClone")
    public Object clone() throws CloneNotSupportedException {
        return copy();
    }

    /**
     * Compares this ControlPoint against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        ControlPoint that = (ControlPoint)obj;
        return this._point.equals(that._point) && this._weight == that._weight;
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = hash * 31 + makeVarCode(_weight);
        hash = hash * 31 + _point.hashCode();
        return hash;
    }

    private static int makeVarCode(double value) {
        long bits = Double.doubleToLongBits(value);
        int var_code = (int)(bits ^ (bits >>> 32));
        return var_code;
    }

    /**
     * Returns the text representation of this control point that consists of the the
     * geometry coordinate values followed by the weighting factor. For example:
     * <pre>
     *   {{10 ft, -3 ft, 4.56 ft}, 1.0}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        tmp.append(_point.toText());
        tmp.append(", ");
        tmp.append(_weight);
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns the String representation of this control point that consists of the the
     * geometry coordinate values followed by the weighting factor. For example:
     * <pre>
     *   {{10 ft, -3 ft, 4.56 ft}, 1.0}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public String toString() {
        return toText().toString();
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<ControlPoint> XML = new XMLFormat<ControlPoint>(ControlPoint.class) {
        @Override
        public ControlPoint newInstance(Class<ControlPoint> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, ControlPoint obj) throws XMLStreamException {

            double weight = xml.getAttribute("weight", 1.0);
            obj._weight = weight;

            Point point = xml.getNext();
            obj._point = point;

        }

        @Override
        public void write(ControlPoint obj, OutputElement xml) throws XMLStreamException {

            xml.setAttribute("weight", obj._weight);
            xml.add(obj._point);

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private ControlPoint() {
    }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<ControlPoint> FACTORY = new ObjectFactory<ControlPoint>() {
        @Override
        protected ControlPoint create() {
            return new ControlPoint();
        }

        @Override
        protected void cleanup(ControlPoint obj) {
            obj._point = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static ControlPoint newInstance(Point point, double weight) {
        ControlPoint o = FACTORY.object();
        o._point = point;
        o._weight = weight;
        return o;
    }

    @SuppressWarnings("unchecked")
    private static ControlPoint copyOf(ControlPoint original) {
        ControlPoint o = FACTORY.object();
        o._point = original._point.copy();
        o._weight = original._weight;
        return o;
    }

}
