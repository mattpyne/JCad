/**
 * SubrangeSurface -- A Surface that is a subrange on another parametric surface.
 *
 * Copyright (C) 2013-2015, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.*;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A subrange or trimmed {@link Surface} that is defined by a set of four 2D boundary
 * curves that define the 4 parametric edges of the trimmed surface on the child surface.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 8, 2013
 * @version November 27, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public class SubrangeSurface extends AbstractSurface<SubrangeSurface> implements Subrange<Surface> {

    /**
     * The number of points used when estimating the twist vector and when estimating the
     * min/max bounding box for this surface.
     */
    private static final int NPTS = 21;

    /**
     * The parametric geometry object that this surface is located on.
     */
    private Surface _child;

    /**
     * The parametric surface defining the parametric range for this surface.
     */
    private Surface _u;

    /**
     * Reference to a change listener for this object's child geometry.
     */
    private ChangeListener _childChangeListener = new MyChangeListener(this);

    /**
     * The minimum bounding point for this subrange surface.
     */
    private transient Point _boundsMin;

    /**
     * The maximum bounding point for this subrange surface.
     */
    private transient Point _boundsMax;

    // The following are cached to improve performance.
    private transient double _s = -1, _t = -1;
    private transient double _uA = 1;         //  uA = uS*uT (product of parametric arc lengths along local s & t directions.
    private Boolean _isDegenerate;

    /**
     * Returns a {@link SubrangeSurface} instance referring to the specified
     * {@link Surface} and the supplied 2D surface in parametric space which maps the
     * SubrangeSurface to the child Surface. The mapping surface in parametric space must
     * be in units of meters and be in the range 0,0 to 1,1.
     *
     * @param child The {@link Surface} that this surface is subranged onto (may not be
     *              <code>null</code>).
     * @param par   A 2D surface in parametric space (between 0,0 and 1,1 in meters) that
     *              maps the parametric positions from the subrange surface to the child
     *              surface. If <code>null</code> is passed, the full parametric boundary
     *              of the child surface is used.
     * @return the subrange surface having the specified mapping to the child surface.
     * @throws DimensionException if the input parametric space surface does not have a
     * parametric dimension equal to 2.
     */
    public static SubrangeSurface newInstance(Surface child, Surface par) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        if (par == null)
            return SubrangeSurface.newInstance(child, 0, 0, 1, 1);

        //  Handle the user inputting a subrange surface on the child surface.
        if (par instanceof SubrangeSurface && ((SubrangeSurface)par).getChild() == child)
            par = ((SubrangeSurface)par).getParPosition();

        int pDim = 2;
        if (par.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        //  Convert the parametric surface to METER units (in case it isn't already).
        par = par.to(SI.METER);

        SubrangeSurface obj = FACTORY.object();
        obj._child = child;
        obj._u = par;
        obj.calcBoundsMinMax();

        if (!(par instanceof Immutable))
            par.addChangeListener(obj._childChangeListener);
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns a {@link SubrangeSurface} instance referring to the specified
     * {@link Surface} and the supplied boundary location curves in parametric space. The
     * boundary curves in parametric space must be in units of meters and be in the range
     * 0,0 to 1,1. The start points of the s0,t0 curves must be coincident. The start of
     * the s1 curve and end of the t0 curve must be coincident. The end of the s1 curve
     * and end of the t1 curve must be coincident. And, the end of the s0 curve and start
     * of the t1 curve must be coincident. The corner points of the parametric curves are
     * checked for proper coincidence.
     *
     * @param child The {@link Surface} that this surface is subranged onto (may not be
     *              <code>null</code>).
     * @param s0    A 2D curve of the parametric position (between 0,0 and 1,1 in meters)
     *              along each parametric dimension or a subrange curve on the child
     *              surface that serves as the s=0 boundary for the subrange surface.
     * @param t0    A 2D curve of the parametric position (between 0,0 and 1,1 in meters)
     *              along each parametric dimension or a subrange curve on the child
     *              surface that serves as the t=0 boundary for the subrange surface.
     * @param s1    A 2D curve of the parametric position (between 0,0 and 1,1 in meters)
     *              along each parametric dimension that serves as the s=1 boundary for
     *              the subrange surface.
     * @param t1    A 2D curve of the parametric position (between 0,0 and 1,1 in meters)
     *              along each parametric dimension or a subrange curve on the child
     *              surface that serves as the t=1 boundary for the subrange surface.
     * @param tol   A tolerance (in parameter space) for how closely the corner points
     *              must match.
     * @return the subrange surface having the specified boundary curves.
     * @throws DimensionException if the input boundary curves are not subranges on child
     * and their parametric dimensions are not equal to 2.
     */
    public static SubrangeSurface newInstance(Surface child, Curve s0, Curve t0, Curve s1, Curve t1, double tol) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(s0, MessageFormat.format(RESOURCES.getString("paramNullErr"), "s0"));
        requireNonNull(s1, MessageFormat.format(RESOURCES.getString("paramNullErr"), "s1"));
        requireNonNull(t0, MessageFormat.format(RESOURCES.getString("paramNullErr"), "t0"));
        requireNonNull(t1, MessageFormat.format(RESOURCES.getString("paramNullErr"), "t1"));

        //  Handle the user inputting subrange curves on the child surface.
        if (s0 instanceof SubrangeCurve && ((SubrangeCurve)s0).getChild() == child)
            s0 = ((SubrangeCurve)s0).getParPosition();
        if (s1 instanceof SubrangeCurve && ((SubrangeCurve)s1).getChild() == child)
            s1 = ((SubrangeCurve)s1).getParPosition();
        if (t0 instanceof SubrangeCurve && ((SubrangeCurve)t0).getChild() == child)
            t0 = ((SubrangeCurve)t0).getParPosition();
        if (t1 instanceof SubrangeCurve && ((SubrangeCurve)t1).getChild() == child)
            t1 = ((SubrangeCurve)t1).getParPosition();

        int pDim = child.getParDimension();
        if (s0.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));
        if (s1.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));
        if (t0.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));
        if (t1.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        //  Convert the parametric curves to METER units (in case they aren't already).
        s0 = s0.to(SI.METER);
        s1 = s1.to(SI.METER);
        t0 = t0.to(SI.METER);
        t1 = t1.to(SI.METER);

        //  Convert the boundary curves into a parametric surface.
        NurbsSurface par = SurfaceFactory.createTFISurface(s0, t0, s1, t1, Parameter.valueOf(tol, SI.METER));
        SubrangeSurface obj = FACTORY.object();
        obj._child = child;
        obj._u = par;
        obj.calcBoundsMinMax();

        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns a SubrangeSurface on the surface <code>child</code> that covers the range
     * of parametric positions from <code>s0,t0</code> to <code>s1,t1</code>.
     *
     * @param child The {@link Surface} object that this surface is subranged onto (may
     *              not be <code>null</code>).
     * @param s0    The parametric position on the child surface that should form the s=0
     *              edge of the subrange surface.
     * @param t0    The parametric position on the child surface that should form the t=0
     *              edge of the subrange surface.
     * @param s1    The parametric position on the child surface that should form the s=1
     *              edge of the subrange surface.
     * @param t1    The parametric position on the child surface that should form the t=1
     *              edge of the subrange surface.
     * @return the subrange surface having the specified range of parametric values on the
     *         child surface.
     */
    public static SubrangeSurface newInstance(Surface child, double s0, double t0, double s1, double t1) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));

        //  Form the desired boundary parametric curves.
        Point p00 = Point.valueOf(s0, t0);
        Point p01 = Point.valueOf(s0, t1);
        Point p11 = Point.valueOf(s1, t1);
        Point p10 = Point.valueOf(s1, t0);
        Curve s0crv = LineSeg.valueOf(p00, p01);
        Curve t0crv = LineSeg.valueOf(p00, p10);
        Curve s1crv = LineSeg.valueOf(p10, p11);
        Curve t1crv = LineSeg.valueOf(p01, p11);

        return SubrangeSurface.newInstance(child, s0crv, t0crv, s1crv, t1crv, GTOL);
    }

    //  A change listener that re-calculates the subrange bounds as well as
    //  passing the child's change event on to any listeners.
    private static class MyChangeListener extends ForwardingChangeListener {

        private final SubrangeSurface target;

        public MyChangeListener(SubrangeSurface geom) {
            super(geom);
            this.target = geom;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            //  Re-calculate the bounds of this subrange curve.
            target.calcBoundsMinMax();
            super.stateChanged(e);
        }
    }

    /**
     * Returns the child object this point is subranged onto.
     */
    @Override
    public ParametricGeometry getChild() {
        return _child;
    }

    /**
     * Returns a 2D surface in parametric space which maps this surface's parametric
     * positions to the child surface.
     *
     * @return A 2D surface in parametric space that maps this surface's parameters to the
     *         child surface.
     */
    @Override
    public Surface getParPosition() {
        return _u;
    }

    /**
     * Sets the range of parametric positions on the child object that this surface refers
     * to. The parametric surface must be 2D, in units of meters and be in the range 0,0
     * to 1,1.
     *
     * @param par The mapping of parametric positions (0,0 to 1,1) from this surface to
     *            the child surface. May not be null.
     */
    @Override
    public void setParPosition(Surface par) {
        requireNonNull(par, MessageFormat.format(RESOURCES.getString("paramNullErr"), "par"));

        //  Handle the user inputting a subrange surface on the child surface.
        if (par instanceof SubrangeSurface && ((SubrangeSurface)par).getChild() == _child)
            par = ((SubrangeSurface)par).getParPosition();

        int pDim = 2;
        if (par.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        //  Convert the parametric surface to METER units (in case it isn't already).
        par = par.to(SI.METER);

        //  Change the parametric surface for this SubrangeSurface.
        if (!(_u instanceof Immutable))
            _u.removeChangeListener(_childChangeListener);

        _u = par;

        if (!(par instanceof Immutable))
            par.addChangeListener(_childChangeListener);

        calcBoundsMinMax();
        _s = -1;
        _t = -1;
        _uA = 1;
        _isDegenerate = null;

        this.fireChangeEvent();
    }

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation returns the size of the child object.
     * 
     * @return The number of child-elements that make up this geometry element.
     */
    @Override
    public int size() {
        return _child.size();
    }

    /**
     * Return the T=0 Boundary for this surface as a curve.
     *
     * @return The T=0 Boundary for this surface as a curve.
     */
    @Override
    public Curve getT0Curve() {
        return SubrangeCurve.newInstance(_child, _u.getT0Curve());
    }

    /**
     * Return the T=1 Boundary for this surface as a curve.
     *
     * @return The T=1 Boundary for this surface as a curve.
     */
    @Override
    public Curve getT1Curve() {
        return SubrangeCurve.newInstance(_child, _u.getT1Curve());
    }

    /**
     * Return the S=0 Boundary for this surface as a curve.
     *
     * @return The S=0 Boundary for this surface as a curve.
     */
    @Override
    public Curve getS0Curve() {
        return SubrangeCurve.newInstance(_child, _u.getS0Curve());
    }

    /**
     * Return the S=1 Boundary for this surface as a curve.
     *
     * @return The S=1 Boundary for this surface as a curve.
     */
    @Override
    public Curve getS1Curve() {
        return SubrangeCurve.newInstance(_child, _u.getS1Curve());
    }

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(double s, double t) {
        validateParameter(s, t);
        Point sT = _u.getRealPoint(s, t);   //  Convert from 0-1 to parametric position range of subrange.
        return _child.getRealPoint(sT);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the uA-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade, boolean scaled) {

        Point sT = _u.getRealPoint(s, t);   //  Convert from 0-1 to parametric position range of subrange.

        //  Calculate the derivatives of the child object.
        List<List<Vector<Length>>> dersLst = _child.getDerivatives(sT, grade);
        Point origin = Point.valueOf(dersLst.get(0).get(0));

        //  Have to account for derivatives in 2-dimensions.
        //  Get the direction cosines of the parametric surface S tangent vector.
        Vector<Dimensionless> utangent = _u.getSDerivative(s, t, 1).toUnitVector();
        double ks = utangent.get(Vector.X).getValue();
        double kt = utangent.get(Vector.Y).getValue();

        //  Get the surface derivatives in the s and t directions.
        List<Vector<Length>> dP_dss = dersLst.get(0);
        List<Vector<Length>> dP_dts = dersLst.get(1);

        //  Create the output array and put in the surface/curve point.
        List<Vector<Length>> ders = dP_dss;

        //  Use the direction cosines to combine the surface s & t direction
        //  derivatives into the parametric surface's s direction: dP_dps = ks*dP_dss + kt*dP_dts
        for (int i = 1; i <= grade; ++i)
            ders.set(i, dP_dss.get(i).times(ks).plus(dP_dts.get(i).times(kt)));

        if (scaled) {
            //  Scale all the derivatives by the ratio of the parametric distance in the s & t
            //  directions to the underlying surface parametric range (uS*uT/(1*1)).
            double uA = calcParametricArcLengthProduct(s, t);
            double f = 1;
            for (int i = 1; i <= grade; ++i) {
                f *= uA;
                ders.set(i, ders.get(i).times(f));
            }
        }

        //  Change the derivative vector origin to be subranges of this surface.
        for (int i = 0; i <= grade; ++i)
            ders.get(i).setOrigin(origin);

        return ders;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the v-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade, boolean scaled) {

        Point sT = _u.getRealPoint(s, t);   //  Convert from 0-1 to parametric position range of subrange.

        //  Calculate the derivatives of the child object.
        List<List<Vector<Length>>> dersLst = _child.getDerivatives(sT, grade);
        Point origin = Point.valueOf(dersLst.get(0).get(0));

        //  Have to account for derivatives in 2-dimensions.
        //  Get the direction cosines of the parametric surface T tangent vector.
        Vector<Dimensionless> utangent = _u.getTDerivative(s, t, 1).toUnitVector();
        double ks = utangent.get(Vector.X).getValue();
        double kt = utangent.get(Vector.Y).getValue();

        //  Get the surface derivatives in the s and t directions.
        List<Vector<Length>> dP_dss = dersLst.get(0);
        List<Vector<Length>> dP_dts = dersLst.get(1);

        //  Create the output array and put in the surface/curve point.
        List<Vector<Length>> ders = dP_dss;

        //  Use the direction cosines to combine the surface s & t direction
        //  derivatives into the parametric surface's s direction: dP_dps = ks*dP_dss + kt*dP_dts
        for (int i = 1; i <= grade; ++i)
            ders.set(i, dP_dss.get(i).times(ks).plus(dP_dts.get(i).times(kt)));

        if (scaled) {
            //  Scale all the derivatives by the ratio of the parametric distance in the s & t
            //  directions to the underlying surface parametric range (uS*uT/(1*1)).
            double uA = calcParametricArcLengthProduct(s, t);
            double f = 1;
            for (int i = 1; i <= grade; ++i) {
                f *= uA;
                ders.set(i, ders.get(i).times(f));
            }
        }

        //  Change the derivative vector origin to be subranges of this surface.
        for (int i = 0; i <= grade; ++i)
            ders.get(i).setOrigin(origin);

        return ders;
    }

    /**
     * Method that calculates and returns the product of the local parametric arc lengths
     * in the s & t directions: uA = uS*uT.
     */
    private double calcParametricArcLengthProduct(double s, double t) {
        if (MathTools.isApproxEqual(s, _s, TOL_ST) && MathTools.isApproxEqual(t, _t, TOL_ST))
            return _uA;

        StackContext.enter();
        try {
            SubrangeCurve scrv = _u.getSCurve(s);
            double uS = scrv.getArcLength(GTOL).getValue();
            SubrangeCurve tcrv = _u.getTCurve(t);
            double uT = tcrv.getArcLength(GTOL).getValue();
            _uA = uS * uT;
            _s = s;
            _t = t;
        } finally {
            StackContext.exit();
        }

        return _uA;
    }

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param s 1st parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    @Override
    public Vector<Length> getTwistVector(double s, double t) {
        validateParameter(s, t);

        StackContext.enter();
        try {
            //  Create a point string to hold the derivatives in the s-direction.
            PointString<Point> sDers = PointString.newInstance();

            //  Space out some points to use for exracting derivatives along a constant-s line.
            List<Double> spacing = GridSpacing.linear(NPTS);

            //  Insert in the input t position to the list.
            for (int i = 1; i < NPTS; ++i) {
                if (spacing.get(i) > t) {
                    if (spacing.get(i - 1) != t)
                        spacing.add(i, t);
                    break;
                }
            }

            //  Compute the s-direction derivatives at each of the t-positions for the input "s" position.
            for (Double tpos : spacing) {
                Vector<Length> der = this.getSDerivative(s, tpos, 1);
                sDers.add(Point.valueOf(der));
            }

            //  Fit a curve to the derivative string and find the t-direction derivative at the input "t" position.
            BasicNurbsCurve tCrv = CurveFactory.fitPoints(3, sDers);
            Vector<Length> tvec = tCrv.getSDerivative(t, 1);
            tvec.setOrigin(getRealPoint(s, t));

            return StackContext.outerCopy(tvec);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new surface that is identical to this one, but with the S-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         S-parameterization reversed.
     * @see #reverseT
     */
    @Override
    public SubrangeSurface reverseS() {
        Surface u = _u.reverseS();

        SubrangeSurface srf = SubrangeSurface.newInstance(_child, u);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return a new surface that is identical to this one, but with the T-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         T-parameterization reversed.
     * @see #reverseS
     */
    @Override
    public SubrangeSurface reverseT() {
        Surface u = _u.reverseT();

        SubrangeSurface srf = SubrangeSurface.newInstance(_child, u);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return a new surface that is identical to this one but with the transpose of the
     * parameterization of this surface. The S and T directions will be swapped.
     *
     * @return A new surface that is identical to this one but with the transpose of the
     *         parameterization of this surface.
     * @see #reverseT
     * @see #reverseS
     */
    @Override
    public SubrangeSurface transpose() {
        Surface u = _u.transpose();

        SubrangeSurface srf = SubrangeSurface.newInstance(_child, u);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Split this {@link SubrangeSurface} at the specified parametric S-position returning
     * a list containing two new surfaces (a lower surface with smaller S-parametric
     * positions than "s" and an upper surface with larger S-parametric positions).
     *
     * @param s The S-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<SubrangeSurface> splitAtS(double s) {
        validateParameter(s, 0);
        if (parNearEnds(s, TOL_ST))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "surface"));

        //  Split the parametric space surface.
        GeomList<Surface> par = _u.splitAtS(s);

        //  Split the surface.
        SubrangeSurface srfL = SubrangeSurface.newInstance(_child, par.get(0));
        SubrangeSurface srfU = SubrangeSurface.newInstance(_child, par.get(1));

        //  Create the output list.
        GeomList<SubrangeSurface> output = GeomList.valueOf(srfL, srfU);
        return output;
    }

    /**
     * Split this {@link SubrangeSurface} at the specified parametric T-position returning
     * a list containing two new surfaces (a lower surface with smaller T-parametric
     * positions than "t" and an upper surface with larger T-parametric positions).
     *
     * @param t The T-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<SubrangeSurface> splitAtT(double t) {
        validateParameter(0, t);
        if (parNearEnds(t, TOL_ST))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "surface"));

        //  Split the parametric space surface.
        GeomList<Surface> par = _u.splitAtT(t);

        //  Split the surface.
        SubrangeSurface srfL = SubrangeSurface.newInstance(_child, par.get(0));
        SubrangeSurface srfU = SubrangeSurface.newInstance(_child, par.get(1));

        //  Create the output list.
        GeomList<SubrangeSurface> output = GeomList.valueOf(srfL, srfU);
        return output;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public SubrangeSurface getTransformed(GTransform transform) {
        requireNonNull(transform);
        return SubrangeSurface.newInstance((Surface)_child.getTransformed(transform), _u);
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        return _boundsMin;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        return _boundsMax;
    }

    /**
     * Calculate the minimum & maximum bounding box corner points of this geometry
     * element.
     */
    private void calcBoundsMinMax() {

        StackContext.enter();
        try {
            //  Space out some points on the subrange surface.
            List<Double> spacing = GridSpacing.linear(NPTS);
            PointArray<SubrangePoint> parr = _u.extractGrid(GridRule.PAR, spacing, spacing, spacing, spacing);
            PointArray<Point> arr = PointArray.newInstance();
            for (int i = 0; i < NPTS; ++i) {
                PointString<Point> str = PointString.newInstance();
                for (int j = 0; j < NPTS; ++j) {
                    Point ppnt = parr.get(i, j).copyToReal();
                    str.add(_child.getRealPoint(ppnt));
                }
                arr.add(str);
            }

            //  Get the bounds of the grid of points.
            _boundsMin = StackContext.outerCopy(arr.getBoundsMin());
            _boundsMax = StackContext.outerCopy(arr.getBoundsMax());

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns the physical dimension of the underlying
     * {@link Surface} objects.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Return the equivalent of this surface converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the surface to return.
     * @return The equivalent of this surface converted to the new dimensions.
     */
    @Override
    public SubrangeSurface toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        return SubrangeSurface.newInstance(_child.toDimension(newDim), _u);
    }

    /**
     * Return a NURBS surface representation of this surface to within the specified
     * tolerance.
     *
     * @param tol The greatest possible difference between this surface and the NURBS
     *            representation returned. May not be null.
     * @return A NURBS surface that represents this surface to within the specified
     *         tolerance.
     */
    @Override
    public NurbsSurface toNurbs(Parameter<Length> tol) {
        requireNonNull(tol);
        StackContext.enter();
        try {
            PointArray<SubrangePoint> arr = this.gridToTolerance(tol);

            //  Fit a cubic NURBS surface to the points.
            int sdeg = 3, tdeg = 3;
            if (arr.size() <= tdeg)
                tdeg = arr.size() - 1;
            if (arr.get(0).size() <= sdeg)
                sdeg = arr.get(0).size() - 1;
            BasicNurbsSurface surface = SurfaceFactory.fitPoints(sdeg, tdeg, arr);
            copyState(surface); //  Copy over the super-class state for this surface to the new one.

            return StackContext.outerCopy(surface);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the unit in which this surface is stated.
     *
     * @return The unit in which this surface is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this surface but stated in the specified unit.
     *
     * @param unit the length unit of the surface to be returned.
     * @return an equivalent to this surface but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public SubrangeSurface to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        //  Convert the curves.
        SubrangeSurface srf = SubrangeSurface.newInstance(_child.to(unit), _u);

        return srf;
    }

    /**
     * Return <code>true</code> if this SubrangeSurface contains valid and finite
     * numerical components. A value of <code>false</code> will be returned if any of the
     * member curves are not valid.
     *
     * @return true if this SubrangeSurface contains valid and finite numerical
     *         components.
     */
    @Override
    public boolean isValid() {
        return _child.isValid() && _u.isValid();
    }

    /**
     * Return <code>true</code> if this surface is degenerate (i.e.: has area less than
     * the specified tolerance squared).
     *
     * @param tol The tolerance for determining if this surface is degenerate. May not be
     *            null.
     * @return true if this surface is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        requireNonNull(tol);
        if (_isDegenerate != null)
            return _isDegenerate;

        _isDegenerate = _u.isDegenerate(tol);
        if (_isDegenerate)
            return true;
        _isDegenerate = _child.isDegenerate(tol);
        return _isDegenerate;
    }

    /**
     * Compares the specified object with this <code>SubrangeSurface</code> for equality.
     * Returns true if and only if both surfaces are of the same type, have the same child
     * surface, and both contain the same boundary curves in the same order.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this surface is identical to that surface;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        SubrangeSurface that = (SubrangeSurface)obj;
        return this._child.equals(that._child)
                && this._u.equals(that._u)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>SubrangeSurface</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_u, _child);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied). This method is not yet implemented and current returns a new
     * SubrangeSurface with the child surface and subrange curves copied to real.
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public SubrangeSurface copyToReal() {
        //  TODO: Add support for this.
        //throw new UnsupportedOperationException( RESOURCES.getString("scCopy2RealNotSupported") );
        return SubrangeSurface.newInstance((Surface)_child.copyToReal(), (Surface)_u.copyToReal());
    }

    /**
     * Returns a copy of this <code>SubrangeSurface</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public SubrangeSurface copy() {
        return copyOf(this);
    }

    /**
     * Returns the text representation of this geometry element.
     *
     * @return The text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        String className = this.getClass().getName();
        tmp.append(className.substring(className.lastIndexOf(".") + 1));

        tmp.append(": {child = {\n");
        tmp.append(_child.toText());
        tmp.append("}\n");
        tmp.append(", u = {\n");
        tmp.append(_u.toText());
        tmp.append("}\n");

        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<SubrangeSurface> XML = new XMLFormat<SubrangeSurface>(SubrangeSurface.class) {

        @Override
        public SubrangeSurface newInstance(Class<SubrangeSurface> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, SubrangeSurface obj) throws XMLStreamException {
            AbstractSurface.XML.read(xml, obj);     // Call parent read.

            Surface par = xml.get("ParPos");
            obj._u = par;
            Surface child = xml.get("Child");
            obj._child = child;
            if (!(par instanceof Immutable))
                par.addChangeListener(obj._childChangeListener);
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
            obj.calcBoundsMinMax();

        }

        @Override
        public void write(SubrangeSurface obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractSurface.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._u, "ParPos");
            xml.add(obj._child, "Child");

        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected SubrangeSurface() { }

    private static final ObjectFactory<SubrangeSurface> FACTORY = new ObjectFactory<SubrangeSurface>() {
        @Override
        protected SubrangeSurface create() {
            return new SubrangeSurface();
        }

        @Override
        protected void cleanup(SubrangeSurface obj) {
            obj.reset();
            if (!(obj._u instanceof Immutable))
                obj._u.removeChangeListener(obj._childChangeListener);
            obj._u = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
            obj._boundsMin = null;
            obj._boundsMax = null;
            obj._isDegenerate = null;
            obj._s = -1;
            obj._t = -1;
            obj._uA = 1;
        }
    };

    /**
     * Recycles a SubrangeSurface instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(SubrangeSurface instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static SubrangeSurface copyOf(SubrangeSurface original) {
        SubrangeSurface obj = FACTORY.object();
        obj._child = original._child.copy();
        obj._u = original._u.copy();
        obj._boundsMax = original._boundsMax.copy();
        obj._boundsMin = original._boundsMin.copy();
        if (!(obj._u instanceof Immutable))
            obj._u.addChangeListener(obj._childChangeListener);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        original.copyState(obj);
        return obj;
    }

}
