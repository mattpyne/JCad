/**
 * GeomVector -- Partial implementation of an n-D vector.
 *
 * Copyright (C) 2009-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.param.ParameterVector;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Quantity;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.lang.MathLib;
import javolution.text.Text;
import javolution.text.TextBuilder;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * Partial implementation of an n-dimensional vector which indicates direction, but not
 * position.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt Date: June 13, 2009
 * @version September 9, 2016
 *
 * @param <Q> The Quantity (unit type) associated with this vector's coordinate values.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class GeomVector<Q extends Quantity> extends AbstractGeomElement<GeomVector>
        implements Transformable<GeomVector> {

    /**
     * Constant used to identify the X coordinate in a 3D vector.
     */
    public static final int X = 0;

    /**
     * Constant used to identify the Y coordinate in a 3D vector.
     */
    public static final int Y = 1;

    /**
     * Constant used to identify the Z coordinate in a 3D vector.
     */
    public static final int Z = 2;

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 0 as a vector is not made up of any other elements.
     */
    @Override
    public int size() {
        return 0;
    }

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 0 as a vector is not parametric.
     */
    @Override
    public int getParDimension() {
        return 0;
    }

    /**
     * Returns the value of a Parameter from this vector.
     *
     * @param i the dimension index.
     * @return the value of the parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public Parameter<Q> get(int i) {
        return Parameter.valueOf(getValue(i), (Unit)getUnit());
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in this vector's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public abstract double getValue(int i);

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in the specified units.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public abstract double getValue(int i, Unit<Q> unit);

    /**
     * Set the origin point for this vector. The origin is used as a reference for drawing
     * the vector and is <i>not</i> a part of the state of this vector and is not used in
     * any calculations with this vector.
     *
     * @param o The origin point to assign to this vector. May not be null.
     */
    public abstract void setOrigin(Point o);

    /**
     * Return the origin point for this vector. If no origin has been explicitly set, then
     * the coordinate system origin in the default units is returned.
     *
     * @return The origin point associated with this vector.
     */
    public abstract Point getOrigin();

    /**
     * Returns the Euclidian norm, magnitude, or length of this vector (square root of the
     * dot product of this vector and itself).
     *
     * @return <code>sqrt(this · this)</code>.
     */
    public Parameter<Q> norm() {
        return Parameter.valueOf(normValue(), (Unit)getUnit());
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector (square root
     * of the dot product of this vector and itself).
     *
     * @return <code>this.norm().doubleValue()</code>.
     */
    public abstract double normValue();

    /**
     * Returns the Euclidian norm, magnitude, or length of this vector (square root of the
     * dot product of this vector and itself).
     *
     * @return <code>sqrt(this · this)</code>.
     */
    public Parameter<Q> mag() {
        return norm();
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector.
     *
     * @return <code>this.norm().doubleValue()</code>.
     */
    public double magValue() {
        return normValue();
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    public abstract Vector<Q> opposite();

    /**
     * Returns the sum of this vector with the one specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    public abstract Vector<Q> plus(GeomVector<Q> that);

    /**
     * Returns the sum of this vector with the parameter specified. The input parameter is
     * added to each component of this vector. The unit of the output vector will be the
     * units of this vector.
     *
     * @param that the parameter to be added to each element of this vector. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    public abstract Vector<Q> plus(Parameter<Q> that);

    /**
     * Returns the difference between this vector and the one specified. The unit of the
     * output vector will be the units of this vector.
     *
     * @param that the vector to be subtracted from this vector. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    public abstract Vector<Q> minus(GeomVector<Q> that);

    /**
     * Subtracts the supplied Parameter from each element of this vector and returns the
     * result. The unit of the output vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from each element of this vector. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    public abstract Vector<Q> minus(Parameter<Q> that);

    /**
     * Returns the product of this vector with the specified coefficient (dimensionless).
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    public abstract Vector<Q> times(double k);

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier. May not be null.
     * @return <code>this · k</code>
     */
    public abstract Vector<? extends Quantity> times(Parameter<?> k);

    /**
     * Returns the dot product (scalar product) of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    public abstract Parameter<? extends Quantity> times(GeomVector<?> that);

    /**
     * Returns the dot product (scalar product) of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    public Parameter<? extends Quantity> dot(GeomVector<?> that) {
        return times(that);
    }

    /**
     * Returns the element-by-element product of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this .* that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     */
    public abstract Vector<? extends Quantity> timesEBE(GeomVector<?> that);

    /**
     * Returns the cross product of two vectors.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this x that</code>
     * @throws DimensionException if
     * <code>(that.getDimension() != this.getDimension())</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    public abstract Vector<? extends Quantity> cross(GeomVector<?> that);

    /**
     * Returns this vector with each element divided by the specified divisor
     * (dimensionless).
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    public abstract Vector<Q> divide(double divisor);

    /**
     * Returns this vector with each element divided by the specified divisor.
     *
     * @param that the divisor. May not be null.
     * @return <code>this / that</code>.
     */
    public abstract Vector<? extends Quantity> divide(Parameter<?> that);

    /**
     * Returns the angle between this vector and the specified vector.
     *
     * @param that the vector to which the angle will be determined. May not be null.
     * @return <code>acos(this · that)/(norm(this)*norm(that))</code>
     */
    public Parameter<Angle> angle(GeomVector<?> that) {
        that = that.to(getUnit());
        double scalar = this.dot(that).getValue();
        double abs1 = this.normValue();
        double abs2 = that.normValue();

        double argument = 1.;
        double dum = abs1 * abs2;
        if (dum >= Parameter.SQRT_EPS)
            argument = scalar / dum;

        if (argument > 1.)
            argument = 1.;
        else if (argument < -1.)
            argument = -1.;

        return Parameter.valueOf(MathLib.acos(argument), SI.RADIAN);
    }

    /**
     * Return an immutable version of this vector.
     *
     * @return An immutable version of this vector.
     */
    public abstract Vector<Q> immutable();

    /**
     * Return <code>true</code> if this GeomVector contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the elements
     * in this vector, including the origin, are invalid.
     */
    @Override
    public boolean isValid() {
        //  Check the origin point.
        GeomPoint o = getOrigin();
        if (!o.isValid())
            return false;

        //  Check the vector elements.
        int dim = getPhyDimension();
        for (int i = 0; i < dim; ++i) {
            double value = getValue(i);
            if (Double.isNaN(value) || Double.isInfinite(value))
                return false;
        }

        return true;
    }

    /**
     * Returns this vector converted to a unit vector by dividing all the vector's
     * elements by the length ({@link #norm}) of this vector.
     *
     * @return This vector converted to a unit vector by dividing all the vector's
     *         elements by the length of this vector.
     */
    public abstract Vector<Dimensionless> toUnitVector();

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMin() {
        Point o = getOrigin();
        Point tip = Point.valueOf(this).plus(o);
        return o.min(tip);
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMax() {
        Point o = getOrigin();
        Point tip = Point.valueOf(this).plus(o);
        return o.max(tip);
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this geometry element. This implementation simply returns
     * <code>getBoundsMin</code> or <code>getBoundsMax</code> depending on the setting of
     * the "max" flag.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public Point getLimitPoint(int dim, boolean max, double tol) {
        if (max)
            return getBoundsMax();
        return getBoundsMin();
    }

    /**
     * Returns a <code>ParameterVector</code> representation of this vector.
     *
     * @return A ParameterVector that is equivalent to this vector.
     */
    public abstract ParameterVector<Q> toParameterVector();

    /**
     * Returns a <code>Float64Vector</code> containing the elements of this vector stated
     * in the current units.
     *
     * @return A Float64Vector that contains the elements of this vector in the current
     *         units.
     */
    public abstract Float64Vector toFloat64Vector();

    /**
     * Returns the values stored in this vector as a Java array, stated in the
     * current {@link #getUnit units}.
     *
     * @return A new array with the vector element values copied into it.
     */
    public double[] toArray() {
        return toArray(new double[this.getPhyDimension()]);
    }

    /**
     * Returns the values stored in this vector, stated in the current
     * {@link #getUnit units} stored in the input Java array.
     *
     * @param array An existing array that has at least as many elements as the physical
     *              dimension of this vector.
     * @return A reference to the input array after the vector elements have been copied
     *         over to it.
     * @see #getPhyDimension()
     */
    public double[] toArray(double[] array) {
        int numDims = this.getPhyDimension();
        for (int i = 0; i < numDims; ++i)
            array[i] = this.getValue(i);
        return array;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public VectorTrans<Q> getTransformed(GTransform transform) {
        return VectorTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Return a copy of this vector converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the vector to return.
     * @return A copy of this vector converted to the new dimensions.
     */
    @Override
    public abstract GeomVector<Q> toDimension(int newDim);

    /**
     * Returns the text representation of this geometry element that consists of the name
     * followed by the vector axis values. For example:
     * <pre>
     *   {v = {{10 ft, -3 ft, 4.56 ft},o={0 m, 0m, 0m}}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {{10 ft, -3 ft, 4.56 ft},o={0 m, 0 m, 0 m}}}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        int dimension = this.getPhyDimension();
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {{");
        } else
            tmp.append("{");
        for (int i = 0; i < dimension; i++) {
            tmp.append(get(i));
            if (i != dimension - 1) {
                tmp.append(", ");
            }
        }
        GeomPoint o = getOrigin();
        if (o == null) {
            tmp.append("},o={null}");
        } else {
            tmp.append("},o=");
            tmp.append(o);
        }

        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns a copy of this GeomVector instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public abstract GeomVector<Q> copy();

    /**
     * Compares this vector against the specified vector for approximate equality
     * (coordinate values approximately equal to this one to within the numerical roundoff
     * tolerance). The origin point is ignored.
     *
     * @param obj The vector object to compare with.
     * @return <code>true</code> if this vector is approximately identical to that vector;
     *         <code>false</code> otherwise.
     */
    public boolean isApproxEqual(GeomVector obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;

        int numDims = getPhyDimension();
        if (obj.getPhyDimension() != numDims)
            return false;

        for (int i = 0; i < numDims; ++i) {
            Parameter thisi = get(i);
            Parameter thati = obj.get(i);
            if (!thisi.isApproxEqual(thati))
                return false;
        }

        return true;
    }

    /**
     * Compares this vector against the specified vector for approximate equality
     * (coordinate values approximately equal to this one to within the numerical roundoff
     * tolerance). The origin point is ignored.
     *
     * @param obj The vector object to compare with.
     * @param tol The amount use to define approximate equality. If <code>null</code> then
     *            exact numerical equality is required.
     * @return <code>true</code> if this vector is approximately identical to that vector;
     *         <code>false</code> otherwise.
     */
    public boolean isApproxEqual(GeomVector obj, Parameter<Q> tol) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;

        int numDims = getPhyDimension();
        if (obj.getPhyDimension() != numDims)
            return false;

        for (int i = 0; i < numDims; ++i) {
            Parameter thisi = get(i);
            Parameter thati = obj.get(i);
            if (!thisi.isApproxEqual(thati, tol))
                return false;
        }

        return true;
    }

}
