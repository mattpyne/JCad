/**
 * TriangleVertData -- An object that packages together information about triangle
 * vertices.
 *
 * Copyright (C) 2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.util.List;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javolution.context.ObjectFactory;

/**
 * An simple container that packages together information about a list of
 * triangles. In particular, it stores a list of non-repeating (unique) vertices
 * and indexes into that list for each triangle, the area of each triangle,
 * and the normal vector for each triangle.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: August 28, 2015
 * @version September 11, 2015
 */
public class TriangleVertData {

    /**
     * The list of unique vertices in this triangle data.
     */
    public PointString<? extends GeomPoint> vertices;
    
    /**
     * The number of triangles in the list.
     */
    public int numTris;
    
    /**
     * The array of indexes into the "vertices" list for the vertices of each triangle.
     * Each set of indices is represented by 3 consecutive integer indexes in a
     * counter-clockwise winding.
     */
    public int[] tris;
    
    /**
     * The surface area of one side of each triangle.
     * The area is always positive, but can be zero.
     */
    public List<Parameter<Area>> areas;
    
    /**
     * The surface normal vector for each triangle.
     * If a triangle is degenerate (zero area), then the
     * normal vector will have zero length.
     */
    public List<GeomVector<Dimensionless>> normals;
    
    /**
     * Construct a new TriangleVertData structure.
     * 
     * @return A new TriangleVertData structure.
     */
    public static TriangleVertData newInstance() {
        return FACTORY.object();
    }

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private TriangleVertData() { }

    private static final ObjectFactory<TriangleVertData> FACTORY = new ObjectFactory<TriangleVertData>() {
        @Override
        protected TriangleVertData create() {
            return new TriangleVertData();
        }

        @Override
        protected void cleanup(TriangleVertData obj) {
            obj.vertices = null;
            obj.tris = null;
            obj.areas = null;
        }
    };
            
}
