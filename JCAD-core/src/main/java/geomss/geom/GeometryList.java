/*
 *   GeometryList  -- Interface in common to all geometry element lists.
 *
 *   Copyright (C) 2002-2015, by Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 **/
package geomss.geom;

import java.util.List;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;

/**
 * Partial implementation of a named list of {@link GeomElement} objects. The list
 * will not accept the addition of <code>null</code> elements.
 * 
 * <p> Modified by: Joseph A. Huwaldt   </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 31, 2000
 * @version November 23, 2015
 * 
 * @param <T> The type of this list.
 * @param <E> The type of elements contained in this list.
 */
public interface GeometryList<T extends GeometryList, E extends GeomElement>
        extends GeomElement<T>, List<E>, Transformable<T> {

    /**
     * Returns the equivalent to this list object but stated in the specified
     * unit.
     *
     * @param unit the length unit of the list object to be returned.
     * @return an equivalent to this list object but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public T to(Unit<Length> unit) throws ConversionException;

    /**
     * Return a copy of this list converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element,
     * then zeros are added to the additional dimensions. If the number of
     * dimensions is less than this element, then the extra dimensions are
     * simply dropped (truncated). If the new dimensions are the same as the
     * dimension of this element, then this element is simply returned.
     *
     * @param newDim The dimension of the list element to return.
     * @return This list element converted to the new dimensions.
     */
    @Override
    public T toDimension(int newDim);

    /**
     * Returns <code>true</code> if this list actually contains any geometry and
     * <code>false</code> if this list is empty or contains only non-geometry
     * items such as empty lists.
     * 
     * @return true if this list actually contains geometry.
     */
    public boolean containsGeometry();

    /**
     * Returns the range of elements in this list from the specified start and
     * ending indexes.
     *
     * @param first index of the first element to return (0 returns the 1st
     * element, -1 returns the last, etc).
     * @param last index of the last element to return (0 returns the 1st
     * element, -1 returns the last, etc).
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    public T getRange(int first, int last);

    /**
     * Returns the first element from this list.
     *
     * @return the first element in this list.
     */
    public E getFirst();

    /**
     * Returns the last element from this list.
     *
     * @return the last element in this list.
     */
    public E getLast();

    /**
     * Returns the element with the specified name from this list.
     *
     * @param name The name of the element we are looking for in the list.
     * @return The element matching the specified name. If the specified element
     *  name isn't found in the list, then <code>null</code> is returned.
     */
    public E get(String name);

    /**
     * Removes the element with the specified name from this list. Shifts any
     * subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     *
     * @param name the name of the element to remove.
     * @return the element previously at the specified position.
     */
    public E remove(String name);

    /**
     * Appends all of the elements in the specified list of arguments to this
     * geometry element list.
     *
     * @param array elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    public boolean add(E... array);

    /**
     * Inserts all of the {@link GeomElement} objects in the specified list of
     * arguments into this list at the specified position. Shifts the element
     * currently at that position (if any) and any subsequent elements to the
     * right (increases their indices). The new elements will appear in this
     * list in the order that they are appeared in the array.
     *
     * @param index index at which to insert first element from the specified array.
     * @param array elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    public boolean add(int index, E... array);

    /**
     * Appends all of the elements in the specified array to this geometry
     * element list. The behavior of this operation is undefined if the
     * specified collection is modified while the operation is in progress.
     *
     * @param arr elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    public boolean addAll(E[] arr);
    
    /**
     * Inserts all of the {@link GeomElement} objects in the specified
     * array into this list at the specified position. Shifts the element
     * currently at that position (if any) and any subsequent elements to the
     * right (increases their indices). The new elements will appear in this
     * list in the order that they are returned by the specified collection's
     * iterator.
     *
     * @param index index at which to insert first element from the specified
     *  collection.
     * @param arr elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    public boolean addAll(int index, E[] arr);
    
    /**
     * Returns an unmodifiable list view associated to this list. Attempts to
     * modify the returned collection result in an UnsupportedOperationException
     * being thrown.
     *
     * @return the unmodifiable view over this list.
     */
    public List<E> unmodifiableList();

    /**
     * Returns a new {@link GeomList} with the elements in this list.
     *
     * @return A new GeomList with the elements in this list.
     */
    public GeomList<E> getAll();

    /**
     * Returns a new {@link GeomList} with the elements in this list in reverse
     * order.
     *
     * @return A new list with the elements in this list in reverse order.
     */
    public T reverse();

    /**
     * Return the index to the 1st geometry element in this list with the
     * specified name. Objects with <code>null</code> names are ignored.
     *
     * @param name The name of the geometry element to find in this list
     * @return The index to the named geometry element or -1 if it is not found.
     */
    public int getIndexFromName(String name);

}
