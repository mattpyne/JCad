/*
 *   LineSegTrans  -- A GeomTransform that has a LineSegment for a child.
 *
 *   Copyright (C) 2012-2018, by Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.NurbsCurve;
import geomss.geom.nurbs.NurbsCurveTrans;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.lang.Immutable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} object that refers to a {@link LineSegment} object
 * and masquerades as a LineSegment object itself.
 *
 * <p>Modified by: Joseph A. Huwaldt</p>
 *
 * @author Joseph A. Huwaldt, Date: January 15, 2012
 * @version April 10, 2018
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class LineSegTrans extends LineSegment implements GeomTransform<LineSegment> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private LineSegment _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link LineSegTrans} instance holding the specified
     * {@link LineSegment} and {@link GTransform}.
     *
     * @param child The LineSegment that is the child of this transform element
     *      (may not be <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *      <code>null</code>).
     * @return the transform element having the specified values.
     */
    public static LineSegTrans newInstance(LineSegment child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"),
                        "line segment", String.valueOf(child.getPhyDimension())));

        LineSegTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;
        child.copyState(obj);

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation from the child object represented by this
     *      element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of
     * GeomTransform objects below this one.
     *
     * @return The total transformation represented by the entire chain of
     *      objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not
     *      be <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this transform element.
     */
    @Override
    public LineSegment getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public LineSegment copyToReal() {
        Point p0 = getStart().copyToReal();
        Point p1 = getEnd().copyToReal();
        LineSeg L = LineSeg.valueOf(p0, p1);
        return copyState(L);    //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Return the starting point of the line segment.
     *
     * @return The starting point of the line segment.
     */
    @Override
    public GeomPoint getStart() {
        return _child.getStart().getTransformed(_TM);
    }

    /**
     * Return the end point of the line segment.
     *
     * @return The end point of the lien segment.
     */
    @Override
    public GeomPoint getEnd() {
        return _child.getEnd().getTransformed(_TM);
    }

    /**
     * Return the dimensional derivative vector for this line segment. The
     * length of this vector is the length of the line segment, the origin is at
     * the start point and the end of the vector is the line end.
     *
     * @return The dimensional derivative vector for this line segment.
     */
    @Override
    public GeomVector<Length> getDerivativeVector() {
        return _child.getDerivativeVector().getTransformed(_TM);
    }

    /**
     * Get unit direction vector for the line segment.
     *
     * @return A unit vector indicating the direction of this line segment.
     */
    @Override
    public GeomVector<Dimensionless> getUnitVector() {
        return _child.getUnitVector().getTransformed(_TM);
    }

    /**
     * Returns the number of child-elements that make up this geometry element.
     * This implementation returns the size in the child curve.
     */
    @Override
    public int size() {
        return _child.size();
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Calculate a point on the curve for the given parametric distance along
     * the curve, <code>p(s)</code>.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0
     *      inclusive).
     * @return the calculated point
     */
    @Override
    public Point getRealPoint(double s) {
        return _TM.transform(_child.getRealPoint(s));
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code>
     * with respect to parametric distance on the curve for the given parametric
     * distance along the curve, <code>d^{grade}p(s)/d^{grade}s</code>. 
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s Parametric distance to calculate derivatives for (0.0 to 1.0
     *      inclusive).
     * @param grade The maximum grade to calculate the derivatives for (1=1st
     *      derivative, 2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at
     *      the specified parametric position.
     * @throws IllegalArgumentException if the grade is < 0.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, int grade) {
        LineSegment trans = copyToReal();
        return trans.getSDerivatives(s, grade);
    }

    /**
     * Return a new curve that is identical to this one, but with the
     * parameterization reversed.
     *
     * @return A new curve that is identical to this one, but with the
     * parameterization reversed.
     */
    @Override
    public LineSegTrans reverse() {
        return LineSegTrans.newInstance(_child.reverse(), _TM);
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner
     * of this geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        LineSegment trans = copyToReal();
        Point minPoint = trans.getBoundsMin();
        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner
     * (e.g.: max X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        LineSegment trans = copyToReal();
        Point maxPoint = trans.getBoundsMax();
        return maxPoint;
    }

    /**
     * Returns transformed version of this element. The returned object
     * implements {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new line segment that is identical to this one with the
     *      specified transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public LineSegTrans getTransformed(GTransform transform) {
        return LineSegTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the unit in which this curve is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this curve but stated in the specified unit.
     *
     * @param unit the length unit of the curve to be returned. May not be null.
     * @return an equivalent to this curve but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public LineSegTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        return LineSegTrans.newInstance(_child.to(unit), _TM);
    }

    /**
     * Return the equivalent of this line segment converted to the specified
     * number of physical dimensions. This implementation will throw an
     * exception if the specified dimension is anything other than 3.
     *
     * @param newDim The dimension of the line segment to return. MUST equal 3.
     * @return The equivalent of this line segment converted to the new
     *      dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other
     *      than 3.
     */
    @Override
    public LineSegTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Return a transformed NURBS curve representation of this line segment. An
     * exact representation is returned and the tolerance parameter is ignored.
     *
     * @param tol Ignored. May pass <code>null</code>.
     * @return A transformed NURBS curve that exactly represents this line
     * segment.
     */
    @Override
    public NurbsCurve toNurbs(Parameter<Length> tol) {
        NurbsCurve curve = NurbsCurveTrans.newInstance(_child.toNurbs(tol), _TM);
        copyState(curve);   //  Copy over the super-class state for this curve to the new one.
        return curve;
    }

    /**
     * Returns a copy of this LineSegTrans instance allocated by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public LineSegTrans copy() {
        return LineSegTrans.copyOf(this);
    }

    /**
     * Compares this LineSegTrans against the specified object for strict
     * equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this transform is identical to that
     *      transform; <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        LineSegTrans that = (LineSegTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<LineSegTrans> XML = new XMLFormat<LineSegTrans>(LineSegTrans.class) {

        @Override
        public LineSegTrans newInstance(Class<LineSegTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, LineSegTrans obj) throws XMLStreamException {
            LineSegment.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            LineSegment child = xml.getNext();
            obj._child = xml.getNext();

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(LineSegTrans obj, OutputElement xml) throws XMLStreamException {
            LineSegment.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<LineSegTrans> FACTORY = new ObjectFactory<LineSegTrans>() {
        @Override
        protected LineSegTrans create() {
            return new LineSegTrans();
        }

        @Override
        protected void cleanup(LineSegTrans obj) {
            obj.reset();
            obj._TM = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    /**
     * Recycles a <code>LineSegTrans</code> instance immediately (on the stack
     * when executing in a StackContext).
     *
     * @param instance The instance to recycle immediately.
     */
    public static void recycle(LineSegTrans instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static LineSegTrans copyOf(LineSegTrans original) {
        LineSegTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    private LineSegTrans() {}

}
