/**
 * LoftedSurface -- A surface defined by a list of cross-section curves.
 *
 * Copyright (C) 2010-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.*;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import java.util.*;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.lang.Immutable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * Represents a "lofted" or "skinned" surface defined from a list of defining
 * {@link Curve curves} that each define a cross-section of the surface. Each curve
 * defines the local parameterization in the "s" direction and the spacing between them
 * defines the parameterization in the "t" direction. Any number of curves may be added to
 * a lofted surface, but all curves must have the same physical dimensions.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 24, 2010
 * @version January 31, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public class LoftedSurface extends AbstractSurface<LoftedSurface> implements GeometryList<LoftedSurface,Curve> {

    //  The list behind this implementation.
    private FastTable<Curve> _crvs;

    //  The degreeU of the surface in the "t" direction.
    private int _q = 1;

    /**
     * Reference to a change listener for this object's child curves.
     */
    private final ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a new, empty, preallocated or recycled <code>LoftedSurface</code> instance
     * (on the stack when executing in a <code>StackContext</code>), that can store a list
     * of {@link Curve} objects. The list is initially empty and therefore the surface is
     * initially undefined.
     *
     * @param q The degreeU of the surface across the defining curves (in the "t"
     *          direction).
     * @return A new empty LoftedSurface.
     */
    public static LoftedSurface newInstance(int q) {
        return newInstance(null, q);
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>LoftedSurface</code> instance
     * (on the stack when executing in a <code>StackContext</code>) with the specified
     * name, that can store a list of {@link Curve} objects. The list is initially empty
     * and therefore the surface is initially undefined.
     *
     * @param name The name to be assigned to this surface (may be <code>null</code>).
     * @param q    The degreeU of the surface across the defining curves (in the "t"
     *             direction).
     * @return A new empty LoftedSurface.
     */
    public static LoftedSurface newInstance(String name, int q) {
        if (q < 1)
            throw new IllegalArgumentException(RESOURCES.getString("tDirDegreeErr"));
        LoftedSurface list = FACTORY.object();
        list._crvs = FastTable.newInstance();
        list._q = q;
        list.setName(name);
        return list;
    }

    /**
     * Return a LoftedSurface made up of the {@link Curve} objects in the specified
     * collection.
     *
     * @param name   The name to be assigned to this surface (may be <code>null</code>).
     * @param q      The degreeU of the surface across the defining curves (in the "t"
     *               direction).
     * @param curves A collection of curves that define the surface. May not be null.
     * @return A new LoftedSurface made up of the curves in the specified collection.
     */
    public static LoftedSurface valueOf(String name, int q, Collection<? extends Curve> curves) {
        requireNonNull(curves);
        LoftedSurface list = LoftedSurface.newInstance(name, q);
        list.addAll(curves);

        return list;
    }

    /**
     * Return a LoftedSurface made up of the {@link Curve} objects in the specified
     * collection.
     *
     * @param q      The degreeU of the surface across the defining curves (in the "t"
     *               direction).
     * @param curves A collection of curves that define the surface. May not be null.
     * @return A new LoftedSurface made up of the curves in the specified collection.
     */
    public static LoftedSurface valueOf(int q, Collection<? extends Curve> curves) {

        LoftedSurface list = LoftedSurface.valueOf(null, q, curves);

        return list;
    }

    /**
     * Return a LoftedSurface made up of the {@link Curve} objects in the specified array.
     *
     * @param name   The name to be assigned to this surface (may be <code>null</code>).
     * @param q      The degreeU of the surface across the defining curves (in the "t"
     *               direction).
     * @param curves An array of curves that define the surface. May not be null.
     * @return A new LoftedSurface made up of the curves in the specified array.
     */
    public static LoftedSurface valueOf(String name, int q, Curve... curves) {
        requireNonNull(curves);
        LoftedSurface list = LoftedSurface.newInstance(name, q);
        list.addAll(Arrays.asList(curves));

        return list;
    }

    /**
     * Return a LoftedSurface made up of the {@link Curve} objects in the specified array.
     *
     * @param q      The degreeU of the surface across the defining curves (in the "t"
     *               direction).
     * @param curves An array of curves that define the surface. May not be null.
     * @return A new LoftedSurface made up of the curves in the specified array.
     */
    public static LoftedSurface valueOf(int q, Curve... curves) {
        return LoftedSurface.valueOf(null, q, curves);
    }

    /**
     * Return the degreeU of the surface in the t-direction (across the defining curves).
     *
     * @return degreeU of surface in t-direction
     */
    public int getTDegree() {
        return _q;
    }

    /**
     * Validate that the surface is properly formed, otherwise throw an exception.
     */
    private void validateSurface() {
        if (size() < _q + 1)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("incDefiningCrvCount"), "LoftedSurface", _q + 1, size()));
    }

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(double s, double t) {
        validateSurface();
        validateParameter(s, t);

        //  Fit a curve through the points on each defining curve at "s".
        BasicNurbsCurve tCrv = createTCurve(s);

        //  Now get the point at the desired "t" position.
        Point p = tCrv.getRealPoint(t);
        BasicNurbsCurve.recycle(tCrv);

        return p;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the u-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade, boolean scaled) {
        validateSurface();
        validateParameter(s, t);
        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        //  Create a list of point strings, each string is for a particular grade.
        FastTable<PointString> sDers = FastTable.newInstance();
        for (int i = 0; i <= grade; ++i)
            sDers.add(PointString.newInstance());

        //  Get the derivatives on each of the defining curves at the input "s" position.
        int size = _crvs.size();
        for (int j = 0; j < size; ++j) {
            Curve crv = _crvs.get(j);
            List<Vector<Length>> ders = crv.getSDerivatives(s, grade);
            for (int i = 0; i <= grade; ++i) {
                Vector<Length> der = ders.get(i);
                sDers.get(i).add(Point.valueOf(der));
            }
        }

        //  Create an array of output derivatives.
        FastTable<Vector<Length>> ders = FastTable.newInstance();

        //  Fit curves to each derivative string and find the derivative at the input "t" position.
        Point origin = this.getRealPoint(s, t);
        for (int i = 0; i <= grade; ++i) {
            BasicNurbsCurve tCrv = CurveFactory.fitPoints(_q, sDers.get(i));
            Vector<Length> der = tCrv.getRealPoint(t).toGeomVector();
            der.setOrigin(origin);
            ders.add(der);
        }

        return ders;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the v-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade, boolean scaled) {
        validateSurface();
        validateParameter(s, t);
        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        //  Fit a curve through the points on each defining curve at "s".
        BasicNurbsCurve tCrv = createTCurve(s);

        //  Get the derivatives on the t-curve at the input "t" position.
        Point origin = this.getRealPoint(s, t);
        List<Vector<Length>> ders = tCrv.getSDerivatives(t, grade);
        int size = ders.size();
        for (int i = 0; i < size; ++i) {
            Vector<Length> v = ders.get(i);
            v.setOrigin(origin);
        }

        return ders;
    }

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param s 1st parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    @Override
    public Vector<Length> getTwistVector(double s, double t) {
        validateSurface();
        validateParameter(s, t);

        //  Create a point string to hold the derivatives in the s-direction on each defining curve.
        PointString<Point> sDers = PointString.newInstance();

        //  Get the s-direction derivatives on each of the defining curves at the input "s" position.
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            Vector der = crv.getSDerivative(s, 1);
            sDers.add(Point.valueOf(der));
        }

        //  Fit a curve to the derivative string and find the t-direction derivative at the input "t" position.
        BasicNurbsCurve tCrv = CurveFactory.fitPoints(_q, sDers);
        Vector<Length> tvec = tCrv.getSDerivative(t, 1);
        Point origin = this.getRealPoint(s, t);
        tvec.setOrigin(origin);

        //  Cleanup
        BasicNurbsCurve.recycle(tCrv);
        PointString.recycle(sDers);

        return tvec;
    }

    /**
     * Return the T=0 Boundary for this surface as a curve.
     *
     * @return The T=0 Boundary for this surface as a curve.
     */
    @Override
    public Curve getT0Curve() {
        return _crvs.get(0);
    }

    /**
     * Return the T=1 Boundary for this surface as a curve.
     *
     * @return The T=1 Boundary for this surface as a curve.
     */
    @Override
    public Curve getT1Curve() {
        return _crvs.get(_crvs.size() - 1);
    }

    /**
     * Return the S=0 Boundary for this surface as a curve.
     *
     * @return The S=0 Boundary for this surface as a curve.
     */
    @Override
    public Curve getS0Curve() {
        return createTCurve(0);
    }

    /**
     * Return the S=1 Boundary for this surface as a curve.
     *
     * @return The S=1 Boundary for this surface as a curve.
     */
    @Override
    public Curve getS1Curve() {
        return createTCurve(1);
    }

    /**
     * Create and return a curve, across the defining curves, at the specified "s"
     * position.
     */
    private BasicNurbsCurve createTCurve(double s) {

        //  Get the points on each of the defining curves at the input "s" position.
        PointString<Point> sPnts = PointString.newInstance();
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            sPnts.add(crv.getRealPoint(s));
        }

        //  Fit a curve through the points on each defining curve.
        BasicNurbsCurve tCrv = CurveFactory.fitPoints(_q, sPnts);
        PointString.recycle(sPnts);

        return tCrv;
    }

    /**
     * Return the parameterizations of each defining section in T at the specified "s"
     * value. The returned array was created using ArrayFactor.DOUBLES_FACTORY and can be
     * recycled.
     */
    private double[] sectionParams(double s) {

        //  Get the points on each of the defining curves at the input "s" position.
        PointString sPnts = PointString.newInstance();
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            sPnts.add(crv.getRealPoint(s));
        }

        //  Parameterize the string of points.
        double[] params = CurveFactory.parameterizeString(sPnts);
        PointString.recycle(sPnts);

        return params;
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns the physical dimension of the underlying
     * {@link Curve} objects or 0 if this list has no Curve objects in it.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        if (isEmpty())
            return 0;
        return get(0).getPhyDimension();
    }

    /**
     * Method that guesses the most likely location for the closest or farthest point on a
     * surface and returns that location as a 2D point containing the "s" and "t"
     * parameter values. This is called by getClosest() and getFarthest(). This is
     * required in order for the root-finding algorithm to reliably refine the closest
     * point to the correct location.
     * <p>
     * This implementation finds the closest/farthest point on each member curve and
     * returns the parametric position of the closest of those.
     * </p>
     *
     * @param point   The point to find the closest point on this surface to. May not be
     *                null.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The 2D parametric point on this surface that is closest to the specified
     *         point.
     */
    @Override
    protected GeomPoint guessClosestFarthest(GeomPoint point, boolean closest) {
        requireNonNull(point);
        if (size() <= 3)
            return super.guessClosestFarthest(point, closest);

        //  Find the closest/furthest point on each member curve to get the parametric s-position.
        SubrangePoint closestPnt = get(0).getClosest(point, GTOL);
        double dOpt = point.distanceValue(closestPnt);
        int size = size();
        int crvIdx = 0;
        for (int i = 1; i < size; ++i) {
            SubrangePoint p = get(i).getClosest(point, GTOL);
            double dist = point.distanceValue(p);
            if (closest) {
                if (dist < dOpt) {
                    dOpt = dist;
                    closestPnt = p;
                    crvIdx = i;
                }
            } else {
                if (dist > dOpt) {
                    dOpt = dist;
                    closestPnt = p;
                    crvIdx = i;
                }
            }
        }
        double s = closestPnt.getParPosition().getValue(0);

        //  Calculate the parametric t-position of the closest defining section.
        FastTable<Double> tmp = FastTable.newInstance();
        Point pm1 = this.get(0).getRealPoint(s);
        double d = 0;
        for (int i = 1; i <= crvIdx; ++i) {
            Point p = this.get(i).getRealPoint(s);
            double distV = p.distanceValue(pm1);
            d += distV;
            tmp.add(distV);
            pm1 = p;
        }

        double t = 0;
        for (int i = 0; i < crvIdx; ++i) {
            t += tmp.get(i) / d;
        }

        return Point.valueOf(s, t);
    }

    /**
     * Return the input index normalized into the range 0 &le; index &lt; size(). This
     * allows negative indexing (-1 referring to the last element in the list), but does
     * not allow wrap-around positive indexing.
     */
    private int normalizeIndex(int index) {
        int size = size();
        while (index < 0)
            index += size;
        return index;
    }

    /**
     * Returns an unmodifiable list view of the curves in this surface. Attempts to modify
     * the returned collection result in an UnsupportedOperationException being thrown.
     *
     * @return the unmodifiable view over this list of curves.
     */
    @Override
    public List<Curve> unmodifiableList() {
        return _crvs.unmodifiable();
    }

    /**
     * Returns <code>true</code> if this collection contains no elements.
     */
    @Override
    public boolean isEmpty() {
        return _crvs.isEmpty();
    }

    /**
     * Returns <code>true</code> if this list actually contains any curves and
     * <code>false</code> if this list is empty.
     *
     * @return true if this list actually contains geometry.
     */
    @Override
    public boolean containsGeometry() {
        return !isEmpty();
    }

    /**
     * Returns the number of elements in this surface (the number of defining curves that
     * make up this surface). If the surface contains more than Integer.MAX_VALUE
     * elements, returns Integer.MAX_VALUE.
     *
     * @return the number of elements in this list of curves.
     */
    @Override
    public int size() {
        return _crvs.size();
    }

    /**
     * Returns the Curve at the specified position in this surface's list of curves.
     *
     * @param index index of element to return (0 returns the 1st element, -1 returns the
     *              last, -2 returns the 2nd from last, etc).
     * @return the Curve at the specified position in this surface.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public Curve get(int index) {
        index = normalizeIndex(index);
        return _crvs.get(index);
    }

    /**
     * Returns the first curve from this surface's list of curves.
     *
     * @return the first curve in this list.
     */
    @Override
    public Curve getFirst() {
        return get(0);
    }

    /**
     * Returns the last curve from this surface's list of curves.
     *
     * @return the last curve in this list.
     */
    @Override
    public Curve getLast() {
        return get(size() - 1);
    }

    /**
     * Returns the range of Curves in this surface from the specified start and ending
     * indexes as a new LoftedSurface.
     *
     * @param first index of the first element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @param last  index of the last element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @return A LoftedSurface made up of the curves in the given range from this surface.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index &ge; size()</code>
     */
    @Override
    public LoftedSurface getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        LoftedSurface list = LoftedSurface.newInstance(getTDegree());
        for (int i = first; i <= last; ++i)
            list.add(get(i));

        return list;
    }

    /**
     * Returns the {@link Curve} with the specified name from this list.
     *
     * @param name The name of the curve we are looking for in the list.
     * @return The curve matching the specified name. If the specified element name
     *         isn't found in the list, then <code>null</code> is returned.
     */
    @Override
    public Curve get(String name) {

        Curve element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.get(index);

        return element;
    }

    /**
     * Returns a view of the portion of this list between fromIndex, inclusive, and
     * toIndex, exclusive. (If fromIndex and toIndex are equal, the returned list is
     * empty.) The returned list is backed by this list, so changes in the returned list
     * are reflected in this list, and vice-versa. The returned list supports all of the
     * optional list operations supported by this list.
     *
     * This method eliminates the need for explicit range operations (of the sort that
     * commonly exist for arrays). Any operation that expects a list can be used as a
     * range operation by passing a subList view instead of a whole list. For example, the
     * following idiom removes a range of values from a list: <code>
     * list.subList(from, to).clear();</code> Similar idioms may be constructed for
     * <code>indexOf</code> and <code>lastIndexOf</code>, and all of the algorithms in the
     * <code>Collections</code> class can be applied to a subList.
     *
     * The semantics of the list returned by this method become undefined if the backing
     * list (i.e., this list) is <i>structurally modified</i> in any way other than via
     * the returned list (structural modifications are those that change the size of this
     * list, or otherwise perturb it in such a fashion that iterations in progress may
     * yield incorrect results).
     *
     * @param fromIndex low endpoint (inclusive) of the subList.
     * @param toIndex   high endpoint (exclusive) of the subList.
     * @return a view of the specified range within this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public List<Curve> subList(int fromIndex, int toIndex) {
        fromIndex = normalizeIndex(fromIndex);
        toIndex = normalizeIndex(toIndex);
        return _crvs.subList(fromIndex, toIndex);
    }

    /**
     * Returns an new {@link GeomList} with the elements in this list.
     *
     * @return A new GeomList with the elements in this list.
     */
    @Override
    public GeomList<Curve> getAll() {
        GeomList<Curve> list = GeomList.newInstance();
        list.addAll(this);
        return list;
    }

    /**
     * Returns an new {@link LoftedSurface} with the curves in this surface in reverse
     * order. This is identical to "reverseT()".
     *
     * @return A new LoftedSurface with the curves in this surface in reverse order.
     * @see #reverseT
     */
    @Override
    public LoftedSurface reverse() {
        LoftedSurface list = LoftedSurface.newInstance(_q);
        copyState(list);
        int size = this.size();
        for (int i = size - 1; i >= 0; --i) {
            list.add(get(i));
        }
        return list;
    }

    /**
     * Returns the index in this list of the first occurrence of the specified element, or
     * -1 if the list does not contain this element.
     *
     * @param element The element to search for.
     * @return the index in this List of the first occurrence of the specified element, or
     *         -1 if the List does not contain this element.
     */
    @Override
    public int indexOf(Object element) {
        return _crvs.indexOf(element);
    }

    /**
     * Returns the index in this list of the last occurrence of the specified element, or
     * -1 if the list does not contain this element. More formally, returns the highest
     * index i such that (o==null ? get(i)==null : o.equals(get(i))), or -1 if there is no
     * such index.
     *
     * @param element The element to search for.
     * @return the index in this list of the last occurrence of the specified element, or
     *         -1 if the list does not contain this element.
     */
    @Override
    public int lastIndexOf(Object element) {
        return _crvs.lastIndexOf(element);
    }

    /**
     * Returns true if this collection contains the specified element. More formally,
     * returns true if and only if this collection contains at least one element e such
     * that (o==null ? e==null : o.equals(e)).
     *
     * @param o object to be checked for containment in this collection.
     * @return <code>true</code> if this collection contains the specified element.
     */
    @Override
    public boolean contains(Object o) {
        return _crvs.contains(o);
    }

    /**
     * Returns true if this collection contains all of the elements in the specified
     * collection.
     *
     * @param c collection to be checked for containment in this collection.
     * @return <code>true</code> if this collection contains all of the elements in the
     *         specified collection.
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return _crvs.containsAll(c);
    }

    /**
     * Return the index to the 1st Curve in this list with the specified name.
     *
     * @param name The name of the Curve to find in this list
     * @return The index to the named Curve or -1 if it is not found.
     */
    @Override
    public int getIndexFromName(String name) {
        if (name == null)
            return -1;

        int result = -1;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = this.get(i);
            String eName = element.getName();
            if (name.equals(eName)) {
                result = i;
                break;
            }
        }
        
        return result;
    }

    /**
     * Inserts the specified {@link Curve} at the specified position in this list. Shifts
     * the element currently at that position (if any) and any subsequent elements to the
     * right (adds one to their indices). Null values are ignored. The input value must
     * have the same physical dimensions as the other items in this list, or an exception
     * is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input value dimensions are different from this
     * list's dimensions.
     */
    @Override
    public void add(int index, Curve value) {
        if (size() > 0 && value.getPhyDimension() != getPhyDimension())
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("incCrvDimension"), "curve", getPhyDimension()));
        index = normalizeIndex(index);

        _crvs.add(index, value);
        if (!(value instanceof Immutable))
            value.addChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Appends the specified {@link Curve} to the end of this surface's list of curves.
     * Null values are ignored. The input value must have the same physical dimensions as
     * the other items in this list, or an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param value the curve to be appended to this list. May not be null.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean add(Curve value) {
        add(size(), value);
        return true;
    }

    /**
     * Appends all of the elements in the specified list of arguments to this geometry
     * element list. The input values must have the same physical dimensions as the other
     * items in this list, or an exception is thrown.
     *
     * @param array elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean add(Curve... array) {
        return add(size(), array);
    }

    /**
     * Inserts all of the {@link Curve} objects in the specified list of arguments into
     * this list at the specified position. Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (increases their indices). The
     * new elements will appear in this list in the order that they are appeared in the
     * array. The input values must have the same physical dimensions as the other items
     * in this list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified array.
     * @param array elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean add(int index, Curve... array) {
        return addAll(index, Arrays.asList(requireNonNull(array)));
    }

    /**
     * Adds all of the curves in the specified collection to this surface. The behavior of
     * this operation is undefined if the specified collection is modified while the
     * operation is in progress. This implies that the behavior of this call is undefined
     * if the specified collection is this collection, and this collection is nonempty.
     * The input elements must have the same physical dimensions as the other items in
     * this list, or an exception is thrown.
     *
     * @param c curves to be inserted into this surface. May not be null.
     * @return <code>true</code> if this surface changed as a result of the call
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(Collection<? extends Curve> c) {
        return addAll(size(), c);
    }

    /**
     * Inserts all of the curves in the specified collection into this surface at the
     * specified position. Shifts the curve currently at that position (if any) and any
     * subsequent curves to the right (increases their indices). The new curves will
     * appear in this list in the order that they are returned by the specified
     * collection's iterator. The behavior of this operation is unspecified if the
     * specified collection is modified while the operation is in progress. Note that
     * this will occur if the specified collection is this list, and it's nonempty.
     * The input elements must have the same physical dimensions as the other items in
     * this list, or an exception is thrown.
     *
     * @param index index at which to insert first curve from the specified collection.
     * @param c     curves to be inserted into this collection. May not be null.
     * @return <code>true</code> if this surface changed as a result of the call
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends Curve> c) {
        requireNonNull(c);
        if (size() > 0) {
            int dim = getPhyDimension();
            for (Curve crv : c) {
                requireNonNull(crv, RESOURCES.getString("collectionElementsNullErr"));
                if (crv.getPhyDimension() != dim)
                    throw new DimensionException(MessageFormat.format(
                            RESOURCES.getString("incCrvDimension"),"curves", getPhyDimension()));
            }
        }
        index = normalizeIndex(index);
        boolean changed = _crvs.addAll(index, c);
        if (changed) {
            for (Curve crv : c) {
                if (!(crv instanceof Immutable))
                    crv.addChangeListener(_childChangeListener);
            }
            fireChangeEvent();  //  Notify change listeners.
        }
        return changed;
    }

    /**
     * Appends all of the curves in the specified array to this LoftedSurface. The
     * behavior of this operation is undefined if the specified collection is modified
     * while the operation is in progress. The input array elements must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param arr curves to be appended onto this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(Curve[] arr) {
        return addAll(size(), arr);
    }

    /**
     * Inserts all of the {@link Curve} objects in the specified array into this list at
     * the specified position. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (increases their indices). The new elements
     * will appear in this list in the order that they are returned by the specified
     * collection's iterator. The input array elements must have the same physical
     * dimensions as the other items in this list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified array.
     * @param arr   elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(int index, Curve[] arr) {
        return addAll(index, Arrays.asList(requireNonNull(arr)));
    }

    /**
     * Replaces the <@link Curve> at the specified position in this surface's list of
     * curves with the specified element. Null elements are ignored. The input element
     * must have the same physical dimensions as the other items in this list, or an
     * exception is thrown.
     *
     * @param index   The index of the element to replace (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The curve to be stored at the specified position. May not be null.
     * @return The curve previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > size()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public Curve set(int index, Curve element) {
        if (size() > 0 && element.getPhyDimension() != getPhyDimension())
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("incCrvDimension"), "curve", getPhyDimension()));
        index = normalizeIndex(index);

        Curve old = _crvs.set(index, element);
        if (!(old instanceof Immutable))
            old.removeChangeListener(_childChangeListener);
        if (!(element instanceof Immutable))
            element.addChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.

        return old;
    }

    /**
     * Removes from this list all the elements that are contained in the specified
     * collection.
     *
     * @param c collection that defines which elements will be removed from this list. May
     *          not be null.
     * @return <code>true</code> if this list changed as a result of the call.
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        boolean modified = false;
        Iterator<?> it = iterator();
        while (it.hasNext()) {
            if (c.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Retains only the elements in this list that are contained in the specified
     * collection. In other words, removes from this list all the elements that are not
     * contained in the specified collection.
     *
     * @param c collection that defines which elements this set will retain. May not be
     *          null.
     * @return <code>true</code> if this list changed as a result of the call.
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        boolean modified = false;
        Iterator<Curve> it = iterator();
        while (it.hasNext()) {
            if (!c.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Removes a single instance of the specified element from this collection, if it is
     * present. More formally, removes an element e such that (o==null ? e==null :
     * o.equals(e)), if this collection contains one or more such elements. Returns true
     * if this collection contained the specified element (or equivalently, if this
     * collection changed as a result of the call).
     *
     * @param o element to be removed from this collection, if present.
     * @return <code>true</code> if this collection changed as a result of the call
     */
    @Override
    public boolean remove(Object o) {
        boolean changed = _crvs.remove(o);
        if (changed) {
            if (o instanceof AbstractGeomElement && !(o instanceof Immutable)) {
                ((AbstractGeomElement)o).removeChangeListener(_childChangeListener);
            }
            fireChangeEvent();
        }
        return changed;
    }

    /**
     * Removes the curve at the specified position in this surface's list of curves.
     * Shifts any subsequent curves to the left (subtracts one from their indices).
     * Returns the curve that was removed from the list.
     *
     * @param index the index of the curve to remove. (0 returns the 1st element, -1
     *              returns the last, -2 returns the 2nd from last, etc).
     * @return the curve previously at the specified position.
     */
    @Override
    public Curve remove(int index) {
        index = normalizeIndex(index);
        Curve old = _crvs.remove(index);
        if (!(old instanceof Immutable))
            old.removeChangeListener(_childChangeListener);
        fireChangeEvent();  //  Notify change listeners.
        return old;
    }

    /**
     * Removes the curve with the specified name from this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that
     * was removed from the list.
     *
     * @param name the name of the element to remove.
     * @return the element previously at the specified position.
     */
    @Override
    public Curve remove(String name) {

        Curve element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.remove(index);

        return element;
    }

    /**
     * Removes all of the curves from this surface. The surface will be empty and
     * undefined after this call returns.
     */
    @Override
    public void clear() {
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            if (!(crv instanceof Immutable))
                crv.removeChangeListener(_childChangeListener);
        }
        _crvs.clear();
        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Returns an iterator over the curves in this surface's list of curves.
     *
     * @return an iterator over this list values.
     */
    @Override
    public java.util.Iterator<Curve> iterator() {
        return _crvs.iterator();
    }

    /**
     * Returns a list iterator over the curves in this surface.
     *
     * @return an iterator over this list values.
     */
    @Override
    public java.util.ListIterator<Curve> listIterator() {
        return _crvs.listIterator();
    }

    /**
     * Returns a list iterator from the specified position.
     *
     * @param index the index of first value to be returned from the list iterator (by a
     *              call to the next method).
     * @return a list iterator of the values in this list starting at the specified
     *         position in this list.
     */
    @Override
    public java.util.ListIterator<Curve> listIterator(int index) {
        return _crvs.listIterator(index);
    }

    /**
     * Returns an array containing all of the elements in this collection.
     */
    @Override
    public Curve[] toArray() {
        return (Curve[])_crvs.toArray();
    }

    /**
     * Returns an array containing all of the elements in this collection. If the
     * collection fits in the specified array, it is returned therein. Otherwise, a new
     * array is allocated with the runtime type of the specified array and the size of
     * this collection.
     *
     * @param <T> The type of elements in this LoftedSurface (curve type).
     * @param a   the array into which the elements of the collection are to be stored, if
     *            it is big enough; otherwise, a new array of the same type is allocated
     *            for this purpose.
     * @return an array containing the elements of the collection.
     */
    @Override
    @SuppressWarnings("SuspiciousToArrayCall")
    public <T> T[] toArray(T[] a) {
        return _crvs.toArray(a);
    }

    /**
     * Return a new surface that is identical to this one, but with the T-parameterization
     * reversed. This is identical to "reverse()".
     *
     * @return A new surface that is identical to this one, but with the
     *         T-parameterization reversed.
     * @see #reverse
     * @see #reverseS
     */
    @Override
    public LoftedSurface reverseT() {
        return reverse();
    }

    /**
     * Return a new surface that is identical to this one, but with the S-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         S-parameterization reversed.
     * @see #reverseT
     */
    @Override
    public LoftedSurface reverseS() {
        LoftedSurface list = LoftedSurface.newInstance(_q);
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = this.get(i);
            list.add(crv.reverse());
        }
        return list;
    }

    /**
     * Return a new surface that is identical to this one but with the transpose of the
     * parameterization of this surface. The S and T directions will be swapped.
     *
     * @return A new surface that is identical to this one but with the transpose of the
     *         parameterization of this surface.
     * @see #reverseT
     * @see #reverseS
     */
    @Override
    public LoftedSurface transpose() {
        LoftedSurface srf = reverseS().reverse();
        return srf;
    }

    /**
     * Split this {@link LoftedSurface} at the specified parametric S-position returning a
     * list containing two new surfaces (a lower surface with smaller S-parametric
     * positions than "s" and an upper surface with larger S-parametric positions). This
     * method splits all the defining section curves to create two new lofted surfaces.
     *
     * @param s The S-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<LoftedSurface> splitAtS(double s) {
        validateSurface();
        validateParameter(s, 0);
        if (parNearEnds(s, TOL_ST))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "surface"));

        LoftedSurface srfL = LoftedSurface.newInstance(_q);
        copyState(srfL);
        LoftedSurface srfU = LoftedSurface.newInstance(_q);
        copyState(srfU);

        //  Split each defining section.
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = this.get(i);
            GeomList<Curve> crvs = crv.splitAt(s);
            srfL.add(crvs.get(0));
            srfU.add(crvs.get(1));
            GeomList.recycle(crvs);
        }

        //  Create the output list.
        GeomList<LoftedSurface> output = GeomList.newInstance();
        output.add(srfL, srfU);

        return output;
    }

    /**
     * Split this surface at the specified parametric T-position returning a list
     * containing two new surfaces (a lower surface with smaller T-parametric positions
     * than "t" and an upper surface with larger T-parametric positions). This method
     * interpolates a new defining section at "t" and uses the defining sections below it
     * and above it to create two new lofted surfaces.
     *
     * @param t The T-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    @SuppressWarnings("null")
    public GeomList<LoftedSurface> splitAtT(double t) {
        validateSurface();
        validateParameter(0, t);
        if (parNearEnds(t, TOL_ST))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "surface"));

        //  Determine the parameterizations of the existing defining sections in T.
        double[] tParams = sectionParams(0);

        //  Find or create the split curve and find the split index.
        Curve splitCrv = null;
        int numCrvs = size();
        int i = 0;
        for (; i < numCrvs; ++i) {
            Curve crv = get(i);
            double ct = tParams[i];

            //  Check for an exact match to the split parameter.
            if (MathTools.isApproxEqual(t, ct)) {
                --i;
                splitCrv = crv;
                break;
            }

            //  Look for a parameter that just exceeds the split parameter.
            if (ct > t) {
                --i;

                //  Create an interpolating curve on this surface at t.
                Point p0 = Point.valueOf(0, t);
                Point p1 = Point.valueOf(1, t);
                splitCrv = SubrangeCurve.newInstance(this, CurveFactory.createLine(p0, p1));
                break;
            }
        }
        ArrayFactory.DOUBLES_FACTORY.recycle(tParams);

        //  Create the lower surface.
        LoftedSurface srfL = LoftedSurface.valueOf(_q, getRange(0, i));
        srfL.add(splitCrv);

        //  Create the upper surface.
        LoftedSurface srfU = LoftedSurface.newInstance(_q);
        srfU.add(splitCrv.copy());
        srfU.addAll(getRange(i + 1, -1));

        //  Create the output list.
        GeomList<LoftedSurface> output = GeomList.newInstance();
        output.add(srfL, srfU);

        return output;
    }

    /**
     * Return a NURBS surface representation of this surface to within the specified
     * tolerance. If the curves making up this surface are NURBS curves, then the
     * resulting NURBS surface is an exact representation of this surface (and tol is
     * ignored). However, if any of the member curves are not NURBS curves, then tol is
     * used to make a NURBS approximation of that curve (and this surface).
     *
     * @param tol The greatest possible difference between this surface and the NURBS
     *            representation returned. May not be null.
     * @return A NURBS surface that represents this surface to within the specified
     *         tolerance.
     */
    @Override
    public NurbsSurface toNurbs(Parameter<Length> tol) {
        validateSurface();

        //  Go off and create a skinned NURBS surface.
        BasicNurbsSurface srf = SurfaceFactory.createSkinnedSurface(_crvs, _q, requireNonNull(tol));
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return <code>true</code> if this LoftedSurface contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the member
     * curves are not valid.
     *
     * @return true if this LoftedSurface contains valid and finite numerical components.
     */
    @Override
    public boolean isValid() {
        //  Make sure there are sufficient member curves.
        if (size() < _q + 1)
            return false;

        //  Make sure all the member curves are valid.
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = this.get(i);
            if (!crv.isValid())
                return false;
        }

        return true;
    }

    /**
     * Return <code>true</code> if this surface is degenerate (i.e.: has area less than
     * the specified tolerance squared).
     *
     * @param tol The tolerance for determining if this surface is degenerate. May not be
     *            null.
     * @return true if this surface is degenerate (i.e.: has area less than the specified
     *         tolerance squared).
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        requireNonNull(tol);
        
        //  A lofted surface is degenerate if all of it's member curves are degenerate.
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = this.get(i);
            if (!crv.isDegenerate(tol))
                return false;
        }
        return true;
    }

    /**
     * Returns a copy of this <code>LoftedSurface</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public LoftedSurface copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public LoftedSurface copyToReal() {
        LoftedSurface newSrf = LoftedSurface.newInstance(_q);
        copyState(newSrf);

        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = this.get(i);
            Curve crv2 = crv.copyToReal();
            newSrf.add(crv2);
        }

        return newSrf;
    }

    /**
     * Returns a transformed version of this element. The returned list of objects
     * implement {@link GeomTransform} and contains transformed versions of the contents
     * of this list as children.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new LoftedSurface that is identical to this one with the specified
     *         transformation applied to member curves.
     * @throws DimensionException if this surface is not 3D.
     */
    @Override
    public LoftedSurface getTransformed(GTransform transform) {
        requireNonNull(transform);
        LoftedSurface list = LoftedSurface.newInstance(getTDegree());
        copyState(list);
        
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve element = _crvs.get(i);
            list._crvs.add((Curve)element.getTransformed(transform));
        }
        return list;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no geometry.
     */
    @Override
    public Point getBoundsMin() {
        if (isEmpty())
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        Point minPoint = _crvs.get(0).getBoundsMin();
        int size = _crvs.size();
        for (int i = 1; i < size; ++i) {
            GeomElement element = _crvs.get(i);
            minPoint = minPoint.min(element.getBoundsMin());
        }

        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        if (isEmpty())
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        Point maxPoint = _crvs.get(0).getBoundsMax();
        int size = _crvs.size();
        for (int i = 1; i < size; ++i) {
            GeomElement element = _crvs.get(i);
            maxPoint = maxPoint.max(element.getBoundsMax());
        }

        return maxPoint;
    }

    /**
     * Returns the unit in which the <I>first</I> curve in this list is stated. If the
     * list contains no curves, then the default unit is returned.
     *
     * @return The unit in which the first curve in this surface is stated or the default
     *         unit if this surface has no defining curves.
     */
    @Override
    public Unit<Length> getUnit() {
        if (isEmpty())
            return GeomUtil.getDefaultUnit();

        return _crvs.get(0).getUnit();
    }

    /**
     * Returns the equivalent to this surface but stated in the specified unit.
     *
     * @param unit the length unit of the surface to be returned. May not be null.
     * @return an equivalent to this surface but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public LoftedSurface to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        //  Convert the curves.
        FastTable<Curve> nCrvs = FastTable.newInstance();
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            nCrvs.add(crv.to(unit));
        }
        LoftedSurface srf = LoftedSurface.valueOf(_q, nCrvs);
        copyState(srf);

        return srf;
    }

    /**
     * Return the equivalent of this surface converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the surface to return.
     * @return This surface converted to the new dimensions.
     */
    @Override
    public LoftedSurface toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        //  Convert the curves.
        FastTable<Curve> nCrvs = FastTable.newInstance();
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            nCrvs.add(crv.toDimension(newDim));
        }
        LoftedSurface srf = LoftedSurface.valueOf(_q, nCrvs);
        copyState(srf);

        return srf;
    }

    /**
     * Compares the specified object with this list of <code>Curve</code> objects for
     * equality. Returns true if and only if both collections are of the same type and
     * both collections contain equal elements in the same order.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this list is identical to that list;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        LoftedSurface that = (LoftedSurface)obj;
        return this._crvs.equals(that._crvs)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>LoftedSurface</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_crvs);
    }

    /**
     * Returns the text representation of this geometry element.
     *
     * @return The text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        String className = this.getClass().getName();
        tmp.append(className.substring(className.lastIndexOf(".") + 1));
        tmp.append(": {");
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        tmp.append("\n");
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement e = this.get(i);
            tmp.append(e.toText());
            if (i < size - 1)
                tmp.append(",\n");
            else
                tmp.append("\n");
        }
        if (hasName)
            tmp.append('}');
        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Resets the internal state of this object to its default values. Subclasses that
     * override this method must call <code>super.reset();</code> to ensure that the state
     * is reset properly.
     */
    @Override
    public void reset() {
        clear();
        _crvs.reset();
        super.reset();
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<LoftedSurface> XML = new XMLFormat<LoftedSurface>(LoftedSurface.class) {

        @Override
        public LoftedSurface newInstance(Class<LoftedSurface> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            LoftedSurface obj = FACTORY.object();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, LoftedSurface obj) throws XMLStreamException {
            int q = Integer.valueOf(xml.getAttribute("q").toString());
            obj._q = q;

            AbstractSurface.XML.read(xml, obj);     // Call parent read.

            FastTable<Curve> lst = xml.get("Contents", FastTable.class);
            obj._crvs = lst;
        }

        @Override
        public void write(LoftedSurface obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            xml.setAttribute("q", Integer.toString(obj._q));

            AbstractSurface.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._crvs, "Contents", FastTable.class);
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected LoftedSurface() { }

    private static final ObjectFactory<LoftedSurface> FACTORY = new ObjectFactory<LoftedSurface>() {
        @Override
        protected LoftedSurface create() {
            return new LoftedSurface();
        }

        @Override
        protected void cleanup(LoftedSurface obj) {
            obj.reset();
            obj._crvs = null;
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(LoftedSurface instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static LoftedSurface copyOf(LoftedSurface original) {
        LoftedSurface obj = FACTORY.object();
        obj._crvs = FastTable.newInstance();
        obj._q = original._q;

        int size = original.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = original.get(i);
            crv = crv.copy();
            obj.add(crv);
        }

        original.copyState(obj);
        return obj;
    }

}
