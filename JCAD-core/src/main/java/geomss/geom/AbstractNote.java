/*
 *   AbstractNote  -- Partial implementation of a note that holds a String located at a point in nD space.
 *
 *   Copyright (C) 2014-2017, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import static java.util.Objects.nonNull;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.text.TypeFormat;

/**
 * Partial implementation of a textual note located at a point in nD space.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 5, 2014
 * @version January 30, 2017
 *
 * @param <T> The sub-type of this AbstractNote.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class AbstractNote<T extends AbstractNote> extends AbstractGeomElement<T> {

    /**
     * The default font used for displaying note objects.
     */
    public static final Font DEFAULT_FONT;

    /**
     * The String encoding of the default font used for displaying note objects.
     */
    protected static final String DEFAULT_FONT_CODE;

    static {
        //  Load and register the list of fonts that should be loaded at run-time.
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        String[] availFontNames = ge.getAvailableFontFamilyNames();
        String[] fontNames = RESOURCES.getString("appFontNames").split(",");
        String[] fontPaths = RESOURCES.getString("appFontPaths").split(",");
        int numFonts = fontNames.length;
        for (int fontIdx = 0; fontIdx < numFonts; ++fontIdx) {
            String name = fontNames[fontIdx];
            String path = fontPaths[fontIdx];

            //  Is the font already provided by the system?
            boolean found = false;
            for (String availName : availFontNames) {
                if (availName.equals(name)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                //  Load the font from the program resources and register it.
                try {
                    loadAndRegisterFont(path, Font.TRUETYPE_FONT);
                } catch (Exception e) {
                    //  Shouldn't happen after development.
                    e.printStackTrace();
                }
            }
        }   //  Next fontIdx

        //  Get the default font name, style and size.
        String defName = RESOURCES.getString("defFontName");
        int defStyle = Font.BOLD;
        int defSize = 12;
        try {
            defStyle = TypeFormat.parseInt(RESOURCES.getString("defFontStyle"));
        } catch (Exception ignore) { /* Ignore, keep default */ }
        try {
            defSize = TypeFormat.parseInt(RESOURCES.getString("defFontSize"));
        } catch (Exception ignore) { /* Ignore, keep default */ }

        //  Save the default font for use elsewhere.
        DEFAULT_FONT = new Font(defName, defStyle, defSize);

        //  Store an encoding of the font name, style & size for use elsewhere.
        DEFAULT_FONT_CODE = encodeFont(defName, defStyle, defSize);
    }

    /**
     * Load the font at the specified path in the program's resources and register it with
     * the Java font management system.
     *
     * @param fontPath The path to the font to be loaded in the program resources.
     * @param fontType The type of the font: Font.TRUETYPE_FONT or Font.TYPE1_FONT.
     * @throws FontFormatException if there is a problem with the font's format.
     * @throws IOException
     */
    @SuppressWarnings("null")
    private static void loadAndRegisterFont(String fontPath, int fontType)
            throws FontFormatException, IOException {

        //  Get a URL to the font file in the program's resources.
        URL fontURL = ClassLoader.getSystemResource(fontPath);

        Font baseFnt;
        try (InputStream is = fontURL.openStream()) {

            //  Create a new font from the file as a 1-point, plain font.
            baseFnt = Font.createFont(fontType, is);

        }

        //  Register the font with the system.
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(baseFnt);
    }

    /**
     * Return the code string for the given font. For example, 12pt Arial with fontStyle
     * == 1 is returned as: Arial-BOLD-12
     *
     * @param fontName  The name of the font family.
     * @param fontStyle The font style code such as Font.PLAIN.
     * @param fontSize  The point size of the font.
     * @return The string encoding of the font.
     */
    protected static String encodeFont(String fontName, int fontStyle, int fontSize) {

        //  Convert the font style code into a style String.
        String styleStr = "PLAIN";
        switch (fontStyle) {
            case Font.BOLD:
                styleStr = "BOLD";
                break;
            case Font.BOLD + Font.ITALIC:
                styleStr = "BOLDITALIC";
                break;
            case Font.ITALIC:
                styleStr = "ITALIC";
                break;
            default:
                break;
        }

        //  Put it all together.
        StringBuilder buf = new StringBuilder(fontName);
        buf.append("-");
        buf.append(styleStr);
        buf.append("-");
        buf.append(fontSize);
        return buf.toString();
    }

    /**
     * Return the text string associated with this note object.
     *
     * @return The text string associated with this note.
     */
    public abstract String getNote();

    /**
     * Return the geometric location of this note in space.
     *
     * @return The geometric location of this note in space.
     */
    public abstract Point getLocation();

    /**
     * Return the font used to display this note.
     *
     * @return The font used to display this note.
     */
    public abstract Font getFont();

    /**
     * Return a new note object identical to this one, but with the specified font.
     *
     * @param font The new font to change this note to display in. May not be null.
     * @return A new note, identical tot his one, but using the specified font.
     */
    public abstract T changeFont(Font font);

    /**
     * Return a new note object identical to this one, but with the specified location in
     * model space.
     *
     * @param location The new location of this note. May not be null.
     * @return A new note, identical tot his one, but using the specified location.
     */
    public abstract T changeLocation(GeomPoint location);

    /**
     * Return the length of the text string associated with this geometry object.
     *
     * @return The length, in characters, of the text string associated with this note.
     */
    public int length() {
        return toString().length();
    }

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 0 as a GeneralNote is not parametric.
     *
     * @return Always returns 0.
     */
    @Override
    public int getParDimension() {
        return 0;
    }

    /**
     * Returns the text representation of this geometry element that consists of the text
     * string followed by the coordinate position. For example:
     * <pre>
     *   {aNote = {"A text string.",{10 ft, -3 ft, 4.56 ft}}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {"A text string.",{10 ft, -3 ft, 4.56 ft}}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        tmp.append("\"");
        tmp.append(toString());
        tmp.append("\",");
        tmp.append(getLocation().toText());
        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

}
