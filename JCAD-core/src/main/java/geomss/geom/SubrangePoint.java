/**
 * SubrangePoint -- A GeomPoint that is a subrange on a parametric object such as a curve
 * or surface.
 *
 * Copyright (C) 2009-2017, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.param.Vector3D;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A {@link GeomPoint} element that refers to a {@link ParametricGeometry} object such as
 * a {@link Curve} or {@link Surface} and a parametric position on that parametric object.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 28, 2009
 * @version October 14, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class SubrangePoint extends GeomPoint implements Subrange<GeomPoint> {

    /**
     * Tolerance allowed on parametric spacing being == 0 or == 1.
     */
    protected static final double TOL_ST = Parameter.EPS;

    /**
     * The parametric distance along the parametric geometry where this point is located.
     */
    private GeomPoint _u;

    /**
     * The parametric geometry object that this point is located on.
     */
    private ParametricGeometry _child;

    /**
     * Reference to a change listener for this object's child geometry.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a {@link SubrangePoint} instance referring to the specified {@link Curve}
     * and parametric location.
     *
     * @param child The {@link Curve} object that this point is subranged onto (may not be
     *              <code>null</code>).
     * @param s     The parametric s-position (between 0 and 1) along curve where the
     *              point is located (may not be <code>null</code>).
     * @return the subrange point having the specified values.
     */
    public static SubrangePoint newInstance(Curve child, double s) {
        if (s < 0 || s > 1)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("crvInvalidSValue"), s));
        return newInstance(child, Point.valueOf(s));
    }

    /**
     * Returns a {@link SubrangePoint} instance referring to the specified {@link Surface}
     * and parametric location.
     *
     * @param child The {@link Surface} object that this point is subranged onto (may not
     *              be <code>null</code>).
     * @param s     The parametric s-position (between 0 and 1) along surface where the
     *              point is located.
     * @param t     The parametric t-position (between 0 and 1) along surface where the
     *              point is located.
     * @return the subrange point having the specified values.
     */
    public static SubrangePoint newInstance(Surface child, double s, double t) {
        if (s < -TOL_ST || s > 1 + TOL_ST)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfInvalidSValue"), s));
        if (t < -TOL_ST || t > 1 + TOL_ST)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfInvalidTValue"), t));
        return newInstance(child, Point.valueOf(s, t));
    }

    /**
     * Returns a {@link SubrangePoint} instance referring to the specified
     * {@link ParametricGeometry} and parametric location.
     *
     * @param child The {@link ParametricGeometry} object that this point is subranged
     *              onto (may not be <code>null</code>).
     * @param par   The parametric position (between 0 and 1) along each parametric
     *              dimension where the point is located (may not be <code>null</code>).
     * @return the subrange point having the specified values.
     * @throws DimensionException if the input child object's parametric dimension is not
     * compatible with the input parametric distance point.
     */
    public static SubrangePoint newInstance(ParametricGeometry child, GeomPoint par) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(par, MessageFormat.format(RESOURCES.getString("paramNullErr"), "par"));
        if (par.getPhyDimension() != child.getParDimension())
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        SubrangePoint obj = FACTORY.object();
        obj._u = par;
        obj._child = child;

        if (!(par instanceof Immutable))
            par.addChangeListener(obj._childChangeListener);
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the parametric position on the child object that this point refers to.
     */
    @Override
    public GeomPoint getParPosition() {
        return _u;
    }

    /**
     * Sets the parametric position on the child object that this point refers to.
     *
     * @param par The parametric position (between 0 and 1) along each parametric
     *            dimension where the point is located (may not be <code>null</code>).
     */
    @Override
    public void setParPosition(GeomPoint par) {
        requireNonNull(par, MessageFormat.format(RESOURCES.getString("paramNullErr"),"par"));
        if (par.getPhyDimension() != _child.getParDimension())
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        if (!(_u instanceof Immutable))
            _u.removeChangeListener(_childChangeListener);

        _u = par;

        if (!(par instanceof Immutable))
            par.addChangeListener(_childChangeListener);

        fireChangeEvent();
    }

    /**
     * Returns the child object this point is subranged onto.
     */
    @Override
    public ParametricGeometry getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed into a concrete {@link GeomPoint}
     * object (information on the parametric position on the child parametric geometry is
     * removed).
     *
     * @return A copy of this object with the subrange removed (applied).
     */
    @Override
    public Point copyToReal() {
        return _child.getRealPoint(_u);
    }

    /**
     * Recycles a <code>SubrangePoint</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(SubrangePoint instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Return an immutable version of this point.
     *
     * @return An immutable version of this point.
     */
    @Override
    public Point immutable() {
        return Point.valueOf(this);
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i) {
        StackContext.enter();
        try {
            Point pTrans = copyToReal();
            double value = pTrans.getValue(i);
            return value;
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * the specified unit.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Length> unit) {
        StackContext.enter();
        try {
            Point pTrans = copyToReal();
            double value = pTrans.getValue(i, requireNonNull(unit));
            return value;
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the square of the Euclidean norm, magnitude, or length value of the vector
     * from the origin to this point (the dot product of the origin-to-this-point vector
     * and itself). This is slightly faster than calling <code>normValue</code> if the
     * squared value is all that is needed.
     *
     * @return <code>this.normSq().getValue()</code>.
     * @see #normValue() 
     */
    @Override
    public double normSqValue() {
        StackContext.enter();
        try {
            Point pTrans = copyToReal();
            double norm2V = pTrans.normSqValue();
            return norm2V;
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Returns the sum of this point with the one specified. The unit of the output point
     * will be the units of this point.
     *
     * @param that the point to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point plus(GeomPoint that) {
        Point pTrans = copyToReal();
        Point point = pTrans.plus(that);
        return point;
    }

    /**
     * Adds the specified parameter to each component of this point. The unit of the
     * output point will be the units of this point.
     *
     * @param that the parameter to be added to each component of this point. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    @Override
    public Point plus(Parameter<Length> that) {
        Point pTrans = copyToReal();
        Point point = pTrans.plus(that);
        return point;
    }

    /**
     * Returns the difference between this point and the one specified. The unit of the
     * output point will be the units of this point.
     *
     * @param that the point to be subtracted from this point. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point minus(GeomPoint that) {
        Point pTrans = copyToReal();
        Point point = pTrans.minus(that);
        return point;
    }

    /**
     * Subtracts the specified parameter from each component of this point. The unit of
     * the output point will be the units of this point.
     *
     * @param that the parameter to be subtracted from each component of this point. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Point minus(Parameter<Length> that) {
        Point pTrans = copyToReal();
        Point point = pTrans.minus(that);
        return point;
    }

    /**
     * Returns the negation of this point (all the values of each dimension negated).
     *
     * @return <code>-this</code>
     */
    @Override
    public Point opposite() {
        Point pTrans = copyToReal();
        Point point = pTrans.opposite();
        return point;
    }

    /**
     * Returns the product of this point with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Point times(double k) {
        Point pTrans = copyToReal();
        Point point = pTrans.times(k);
        return point;
    }

    /**
     * Return <code>true</code> if this point contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     *
     * @return true if this point contains valid and finite numerical components.
     */
    @Override
    public boolean isValid() {
        return _u.isValid() && _child.isValid();
    }

    /**
     * Returns a copy of this SubrangePoint instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public SubrangePoint copy() {
        return SubrangePoint.copyOf(this);
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this point are stated in.
     *
     * @return The unit in which the coordinate values in this point are stated in.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this point but stated in the specified unit.
     *
     * @param unit the length unit of the point to be returned. May not be null.
     * @return an equivalent to this point but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public GeomPoint to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        return copyToReal().to(unit);
    }

    /**
     * Returns a Vector3D representation of this transformed point if possible.
     *
     * @return A Vector3D that is equivalent to this transformed point
     * @throws DimensionException if this point has any number of dimensions other than 3.
     */
    @Override
    public Vector3D<Length> toVector3D() {
        Point pTrans = copyToReal();
        Vector3D<Length> V = pTrans.toVector3D();
        return V;
    }

    /**
     * Returns the values stored in this transformed point, stated in this point's
     * {@link #getUnit unit}, as a Float64Vector.
     *
     * @return A Float64Vector containing the coordinate values in this point.
     */
    @Override
    public Float64Vector toFloat64Vector() {
        StackContext.enter();
        try {
            Point pTrans = copyToReal();
            Float64Vector V = pTrans.toFloat64Vector();
            return V;
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the equivalent of this point converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned. NOTE: The returned point is no longer a SubrangePoint.
     *
     * @param newDim The dimension of the point to return.
     * @return The equivalent of this point converted to the new dimensions.
     */
    @Override
    public GeomPoint toDimension(int newDim) {
        if (newDim == this.getPhyDimension())
            return this;

        GeomPoint newP = copyToReal().toDimension(newDim);
        return newP;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public GeomPointTrans getTransformed(GTransform transform) {
        return GeomPointTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Compares this SubrangePoint against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        SubrangePoint that = (SubrangePoint)obj;
        return this._u.equals(that._u)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_u, _child);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<SubrangePoint> XML = new XMLFormat<SubrangePoint>(SubrangePoint.class) {

        @Override
        public SubrangePoint newInstance(Class<SubrangePoint> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, SubrangePoint obj) throws XMLStreamException {
            GeomPoint.XML.read(xml, obj);     // Call parent read.

            GeomPoint par = xml.get("ParPos");
            obj._u = par;
            ParametricGeometry child = xml.get("Child");
            obj._child = child;

            if (!(par instanceof Immutable))
                par.addChangeListener(obj._childChangeListener);
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(SubrangePoint obj, OutputElement xml) throws XMLStreamException {
            GeomPoint.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._u, "ParPos");
            xml.add(obj._child, "Child");

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private SubrangePoint() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<SubrangePoint> FACTORY = new ObjectFactory<SubrangePoint>() {
        @Override
        protected SubrangePoint create() {
            return new SubrangePoint();
        }

        @Override
        protected void cleanup(SubrangePoint obj) {
            obj.reset();
            if (!(obj._u instanceof Immutable))
                obj._u.removeChangeListener(obj._childChangeListener);
            obj._u = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static SubrangePoint copyOf(SubrangePoint original) {
        SubrangePoint obj = FACTORY.object();
        obj._u = original._u.copy();
        obj._child = original._child.copy();
        if (!(obj._u instanceof Immutable))
            obj._u.addChangeListener(obj._childChangeListener);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        original.copyState(obj);
        return obj;
    }

}
