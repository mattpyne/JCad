/**
 * LinearComboCurve -- Represents a linear combination of two or more curves.
 *
 * Copyright (C) 2015-2017, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.*;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * Represents a linear combination made up of a list of {@link Curve} objects. A linear
 * combination curve is formed by a weighted linear addition of a list of one or more
 * curves. For example: <code>B = W1*Crv_1 + ... + Wn*Crv_n</code> where W1 through Wn are
 * scalar weights. The linear addition is done independently of physical dimension (each
 * dimension is added separately) and the weighted addition is done in parameter space:
 * <code> B = W1*Crv_1(s) + ... + Wn*Crv_n(s)</code>.
 *<p>
 * Any number of curves may be added to a LinearComboCurve, but they all must have the
 * same physical dimensions.
 * </p>
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: September 8, 2015
 * @version January 31, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public class LinearComboCurve extends AbstractCurve<LinearComboCurve> implements LinearCombination<LinearComboCurve,Curve> {

    //  The list of curves being combined.
    private List<Curve> _crvs;

    //  The list of weights applied to each curve.
    private List<Double> _wts;

    /**
     * Reference to a change listener for this object's child curves.
     */
    private final ChangeListener _childChangeListener = new ForwardingChangeListener(this);


    /**
     * Returns a new, empty, preallocated or recycled <code>LinearComboCurve</code>
     * instance (on the stack when executing in a <code>StackContext</code>), that can
     * store a list of {@link Curve} objects and associated weighting factors. The list is
     * initially empty and therefore the linear combination is initially undefined.
     *
     * @return A new empty LinearComboCurve.
     */
    public static LinearComboCurve newInstance() {
        LinearComboCurve list = FACTORY.object();
        list._crvs = FastTable.newInstance();
        list._wts = FastTable.newInstance();
        return list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>LinearComboCurve</code>
     * instance (on the stack when executing in a <code>StackContext</code>), that can
     * store a list of {@link Curve} objects and associated weighting factors. The list is
     * initially empty and therefore the linear combination is initially undefined.
     *
     * @param name The name to be assigned to this LinearComboCurve (may be null).
     * @return A new empty LinearComboCurve.
     */
    public static LinearComboCurve newInstance(String name) {
        LinearComboCurve list = LinearComboCurve.newInstance();
        list.setName(name);
        return list;
    }

    /**
     * Return a LinearComboCurve made up of the {@link Curve} and weight objects in the
     * specified collections.
     *
     * @param curves  A collection of curves that define the LinearComboCurve. May not be
     *                null.
     * @param weights A collection of weights (one for each curve). May not be null and
     *                must have the same size as "curves".
     * @return A new LinearComboCurve made up of the curves and weights in the specified
     *         collections.
     */
    public static LinearComboCurve valueOf(Collection<? extends Curve> curves, Collection<Double> weights) {

        LinearComboCurve list = LinearComboCurve.newInstance();
        list.addAll(requireNonNull(curves), requireNonNull(weights));

        return list;
    }

    /**
     * Return a LinearComboCurve made up of the {@link Curve} objects in the specified
     * array each with a default weight of 1.0.
     *
     * @param curves An array of curves that define the LinearComboCurve. May not be null.
     * @return A new LinearComboCurve made up of the curves in the specified
     *         array each with a default weight of 1.0.
     */
    public static LinearComboCurve valueOf(Curve... curves) {

        LinearComboCurve list = LinearComboCurve.newInstance();
        list.addAll(Arrays.asList(requireNonNull(curves)));

        return list;
    }

    /**
     * Validate that the curve is properly formed, otherwise throw an exception.
     */
    private void validateCurve() {
        if (size() < 1)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("incDefiningCrvCount"), "LoftedSurface", 2, size()));
    }

    /**
     * Return the input index normalized into the range 0 &le; index &lt; size(). This
     * allows negative indexing (-1 referring to the last element in the list), but does
     * not allow wrap-around positive indexing.
     */
    private int normalizeIndex(int index) {
        int size = size();
        while (index < 0)
            index += size;
        return index;
    }

    /**
     * Returns an unmodifiable list view of the curves in this list. Attempts to modify
     * the returned collection result in an UnsupportedOperationException being thrown.
     *
     * @return the unmodifiable view over this list of curves.
     */
    @Override
    public List<Curve> unmodifiableList() {
        return ((FastTable)_crvs).unmodifiable();
    }

    /**
     * Returns an unmodifiable list view of the weights in this list. Attempts to modify
     * the returned collection result in an UnsupportedOperationException being thrown.
     *
     * @return the unmodifiable view over this list of weights.
     */
    @Override
    public List<Double> unmodifiableWeightList() {
        return ((FastTable)_wts).unmodifiable();
    }

    /**
     * Returns <code>true</code> if this collection contains no elements.
     */
    @Override
    public boolean isEmpty() {
        return _crvs.isEmpty();
    }

    /**
     * Returns <code>true</code> if this list actually contains any curves and
     * <code>false</code> if this list is empty.
     *
     * @return true if this list actually contains geometry.
     */
    @Override
    public boolean containsGeometry() {
        return !isEmpty();
    }

    /**
     * Returns the number of {@link Curve} objects that make up this linear combination
     * curve. If the surface contains more than Integer.MAX_VALUE elements, returns
     * Integer.MAX_VALUE.
     *
     * @return the number of elements in this list of curves.
     */
    @Override
    public int size() {
        return _crvs.size();
    }

    /**
     * Returns the Curve at the specified position in this LinearComboCurve.
     *
     * @param index index of curve to return (0 returns the 1st element, -1 returns the
     *              last, -2 returns the 2nd from last, etc).
     * @return the Curve at the specified position in this LinearComboCurve.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public Curve get(int index) {
        index = normalizeIndex(index);
        return _crvs.get(index);
    }

    /**
     * Returns the linear combination weight at the specified position in this
     * LinearComboCurve.
     *
     * @param index index of weight to return (0 returns the 1st element, -1 returns the
     *              last, -2 returns the 2nd from last, etc).
     * @return the linear combination weight at the specified position in this
     *         LinearComboCurve.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public Double getWeight(int index) {
        index = normalizeIndex(index);
        return _wts.get(index);
    }

    /**
     * Returns the first curve from this LinearComboCurve.
     *
     * @return the first curve in this list.
     */
    @Override
    public Curve getFirst() {
        return get(0);
    }

    /**
     * Returns the first linear combination weight from this LinearComboCurve.
     *
     * @return the first weight in this list.
     */
    @Override
    public Double getFirstWeight() {
        return getWeight(0);
    }

    /**
     * Returns the last curve from this LinearComboCurve list of curves.
     *
     * @return the last curve in this list.
     */
    @Override
    public Curve getLast() {
        return get(size() - 1);
    }

    /**
     * Returns the last linear combination weight from this LinearComboCurve.
     *
     * @return the last weight in this list.
     */
    @Override
    public Double getLastWeight() {
        return getWeight(size() - 1);
    }

    /**
     * Returns the range of Curves in this LinearComboCurve from the specified start and ending
     * indexes as a new LinearComboCurve.
     *
     * @param first index of the first element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @param last  index of the last element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @return A LinearComboCurve made up of the curves in the given range from this LinearComboCurve.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index &ge; size()</code>
     */
    @Override
    public LinearComboCurve getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        LinearComboCurve list = LinearComboCurve.newInstance();
        for (int i = first; i <= last; ++i)
            list.add(get(i),getWeight(i));

        return list;
    }

    /**
     * Returns the range of linear combination weights in this LinearComboCurve from the
     * specified start and ending indexes as a List of double values.
     *
     * @param first index of the first element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @param last  index of the last element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @return A List made up of the weights in the given range from this
     *         LinearComboCurve.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index &ge; size()</code>
     */
    @Override
    public List<Double> getWeightRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);
        
        FastTable<Double> list = FastTable.newInstance();
        for (int i = first; i <= last; ++i)
            list.add(getWeight(i));

        return list;
    }

    /**
     * Returns the {@link Curve} with the specified name from this list.
     *
     * @param name The name of the curve we are looking for in the list.
     * @return The curve matching the specified name. If the specified element name
     *         isn't found in the list, then <code>null</code> is returned.
     */
    @Override
    public Curve get(String name) {

        Curve element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.get(index);

        return element;
    }

    /**
     * Returns a view of the portion of this LinearComboCurve between fromIndex,
     * inclusive, and toIndex, exclusive. (If fromIndex and toIndex are equal, the
     * returned LinearComboCurve is empty.) The returned LinearComboCurve is backed by
     * this LinearComboCurve, so changes in the returned LinearComboCurve are reflected in
     * this LinearComboCurve, and vice-versa.
     *
     * This method eliminates the need for explicit range operations (of the sort that
     * commonly exist for arrays). Any operation that expects a list can be used as a
     * range operation by passing a subList view instead of a whole list. For example, the
     * following idiom removes a range of values from a list: <code>
     * list.subList(from, to).clear();</code> Similar idioms may be constructed for
     * <code>indexOf</code> and <code>lastIndexOf</code>, and all of the algorithms in the
     * <code>Collections</code> class can be applied to a subList.
     *
     * The semantics of the list returned by this method become undefined if the backing
     * list (i.e., this list) is <i>structurally modified</i> in any way other than via
     * the returned list (structural modifications are those that change the size of this
     * list, or otherwise perturb it in such a fashion that iterations in progress may
     * yield incorrect results).
     *
     * @param fromIndex low endpoint (inclusive) of the subList.
     * @param toIndex   high endpoint (exclusive) of the subList.
     * @return a view of the specified range within this LinearComboCurve.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public LinearComboCurve subList(int fromIndex, int toIndex) {
        fromIndex = normalizeIndex(fromIndex);
        toIndex = normalizeIndex(toIndex);

        LinearComboCurve crv = FACTORY.object();
        crv._crvs = _crvs.subList(fromIndex, toIndex);
        crv._wts = _wts.subList(fromIndex, toIndex);

        return crv;
    }

    /**
     * Returns an new {@link GeomList} with all the curves in this list.
     *
     * @return A new GeomList with all the curves in this list.
     */
    @Override
    public GeomList<Curve> getAll() {
        GeomList<Curve> list = GeomList.newInstance();
        list.addAll(_crvs);
        return list;
    }

    /**
     * Return a new curve that is identical to this one, but with the
     * parameterization reversed.
     *
     * @return A new curve that is identical to this one, but with the
     * parameterization reversed.
     */
    @Override
    public LinearComboCurve reverse() {
        LinearComboCurve list = LinearComboCurve.newInstance();
        copyState(list);
        int size = this.size();
        for (int i=0; i < size; ++i) {
            list.add(_crvs.get(i).reverse(), _wts.get(i));
        }
        return list;
    }

    /**
     * Returns the index in this list of the first occurrence of the specified
     * {@link Curve}, or -1 if the list does not contain this curve.
     *
     * @param element The Curve to search for.
     * @return the index in this List of the first occurrence of the specified curve, or
     *         -1 if the List does not contain this curve.
     */
    @Override
    public int indexOf(Object element) {
        return _crvs.indexOf(element);
    }

    /**
     * Returns the index in this list of the last occurrence of the specified
     * {@link Curve}, or -1 if the list does not contain this curve. More formally,
     * returns the highest index i such that (o==null ? get(i)==null : o.equals(get(i))),
     * or -1 if there is no such index.
     *
     * @param element The Curve to search for.
     * @return the index in this list of the last occurrence of the specified curve, or -1
     *         if the list does not contain this curve.
     */
    @Override
    public int lastIndexOf(Object element) {
        return _crvs.lastIndexOf(element);
    }

    /**
     * Returns true if this LinearComboCurve contains the specified {@link Curve}. More formally,
     * returns true if and only if this LinearComboCurve's curves contains at least one element e such
     * that (o==null ? e==null : o.equals(e)).
     *
     * @param o object to be checked for containment in this collection.
     * @return <code>true</code> if this collection contains the specified element.
     */
    @Override
    public boolean contains(Object o) {
        return _crvs.contains(o);
    }

    /**
     * Returns true if this collection contains all of the {@link Curve} objects in the specified
     * collection.
     *
     * @param c collection of curves to be checked for containment in this collection.
     * @return <code>true</code> if this collection contains all of the curves in the
     *         specified collection.
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return _crvs.containsAll(c);
    }

    /**
     * Return the index to the 1st Curve in this list with the specified name.
     *
     * @param name The name of the Curve to find in this list
     * @return The index to the named Curve or -1 if it is not found.
     */
    @Override
    public int getIndexFromName(String name) {
        if (name == null)
            return -1;

        int result = -1;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = this.get(i);
            String eName = element.getName();
            if (name.equals(eName)) {
                result = i;
                break;
            }
        }
        
        return result;
    }

    /**
     * Inserts the specified {@link Curve} at the specified position in this list with a
     * default weight of 1.0. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices). Null values are
     * ignored. The input value must have the same physical dimensions as the other items
     * in this list, or an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the Curve to be inserted with a default weight of 1.0.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input value dimensions are different from this
     * list's dimensions.
     */
    @Override
    public void add(int index, Curve value) {
        add(index, value, 1.0);
    }

    /**
     * Inserts the specified {@link Curve} at the specified position in this list. Shifts
     * the element currently at that position (if any) and any subsequent elements to the
     * right (adds one to their indices). Null values are ignored. The input curve must
     * have the same physical dimensions as the other items in this list, or an exception
     * is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param index  The index at which the specified element is to be inserted. (0
     *               returns the 1st element, -1 returns the last, -2 returns the 2nd from
     *               last, etc).
     * @param curve  The Curve to be inserted. May not be null.
     * @param weight The linear combination weight of the curve to be inserted. May not be
     *               null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public void add(int index, Curve curve, Double weight) {
        requireNonNull(weight);
        if (size() > 0 && curve.getPhyDimension() != getPhyDimension())
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("incCrvDimension"), "curve", getPhyDimension()));
        index = normalizeIndex(index);

        _wts.add(index, weight);
        _crvs.add(index, curve);
        if (!(curve instanceof Immutable))
            curve.addChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Appends the specified {@link Curve} to the end of this LinearComboCurve with a
     * default weight of 1.0. Null values are ignored. The input curve must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param curve The curve to be appended to this list with a default weight of 1.0.
     *              May not be null.
     * @return true if this list changed as a result of this call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean add(Curve curve) {
        return add(curve, 1.0);
    }

    /**
     * Appends the specified {@link Curve} to the end of this list of curves. Null values
     * are ignored. The input curve must have the same physical dimensions as the other
     * items in this list, or an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param curve  The curve to be appended to this list. May not be null.
     * @param weight The linear combination weight of the curve to be appended. May not be
     *               null.
     * @return true if this list changed as a result of this call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean add(Curve curve, Double weight) {
        add(size(), curve, weight);
        return true;
    }

    /**
     * Appends all of the elements in the specified list of arguments to this
     * LinearComboCurve with a default weight of 1.0 assigned to each. The input curve
     * must have the same physical dimensions as the other items in this list, or an
     * exception is thrown.
     *
     * @param array the curves to be inserted into this collection with a default weight
     *              of 1.0 assigned to each. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean add(Curve... array) {
        return add(size(), array);
    }

    /**
     * Inserts all of the {@link Curve} objects in the specified list of arguments into
     * this LinearComboCurve at the specified position with a default weight of 1.0
     * assigned to each. Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (increases their indices). The new elements will
     * appear in this list in the order that they are appeared in the array. The input
     * curve must have the same physical dimensions as the other items in this list, or an
     * exception is thrown.
     *
     * @param index index at which to insert first element from the specified array.
     * @param array the curves to be inserted into this collection with a default weight
     *              of 1.0 for each. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean add(int index, Curve... array) {
        return addAll(index, Arrays.asList(requireNonNull(array)));
    }

    /**
     * Adds all of the curves in the specified collection to this LinearComboCurve with a
     * default weight of 1.0 for each. The behavior of this operation is undefined if the
     * specified collection is modified while the operation is in progress. This implies
     * that the behavior of this call is undefined if the specified collection is this
     * collection, and this collection is nonempty. The input curves must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param curves curves to be inserted into this list with a default of 1.0 for each.
     *               May not be null.
     * @return <code>true</code> if this LinearComboCurve changed as a result of the call
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean addAll(Collection<? extends Curve> curves) {
        return addAll(size(), curves);
    }

    /**
     * Inserts all of the curves in the specified collection of curves into this
     * LinearComboCurve with default weights of 1.0 for each. Shifts the curve currently
     * at that position (if any) and any subsequent curves to the right (increases their
     * indices). The new curves will appear in this list in the order that they are
     * returned by the specified collection's iterator. The behavior of this operation is
     * unspecified if the specified collection is modified while the operation is in
     * progress. Note that this will occur if the specified collection is this list, and
     * it's nonempty. The input curves must have the same physical dimensions as the other
     * items in this list, or an exception is thrown.
     *
     * @param index  index at which to insert first curve from the specified collection.
     * @param curves curves to be inserted into this list with default weights of 1.0 for
     *               each. May not be null.
     * @return <code>true</code> if this LinearComboCurve changed as a result of the call
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends Curve> curves) {
        FastTable<Double> weights = FastTable.newInstance();
        Double ONE = 1.0;

        int size = curves.size();
        for (int i=size-1; i >= 0; --i)
            weights.add(ONE);

        boolean output = addAll(index, curves, weights);

        FastTable.recycle(weights);
        return output;
    }

    /**
     * Appends all of the curves in the specified collection to this LinearComboCurve. The
     * behavior of this operation is undefined if the specified collection is modified
     * while the operation is in progress. This implies that the behavior of this call is
     * undefined if the specified collection is this collection, and this collection is
     * nonempty. The input curves must have the same physical dimensions as the other
     * items in this list, or an exception is thrown.
     *
     * @param curves  the curves to be appended onto this list of curves. May not be null.
     * @param weights the linear combination weights associated with all of the curves
     *                being appended. May not be null and must be the same size as
     *                "curves".
     * @return <code>true</code> if this LinearComboCurve changed as a result of the call
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean addAll(Collection<? extends Curve> curves, Collection<Double> weights) {
        return addAll(size(), curves, weights);
    }

    /**
     * Inserts all of the curves in the specified collection and their associated weights
     * into this LinearComboCurve at the specified position. Shifts the curve currently at
     * that position (if any) and any subsequent curves to the right (increases their
     * indices). The new curves will appear in this list in the order that they are
     * returned by the specified collection's iterator. The behavior of this operation is
     * unspecified if the specified collection is modified while the operation is in
     * progress. Note that this will occur if the specified collection is this list, and
     * it's nonempty. The input curves must have the same physical dimensions as the other items
     * in this list, or an exception is thrown.
     *
     * @param index   index at which to insert first curve from the specified collection.
     * @param curves  the curves to be inserted into this linear combination. May not be null.
     * @param weights the linear combination weights associated with each curve being
     *                inserted. May not be null and must be the same size as "curves".
     * @return <code>true</code> if this LinearComboCurve changed as a result of the call
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends Curve> curves, Collection<Double> weights) {
        if (curves.size() != weights.size())
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("lstSameSizeErr"), "curves list", "weights list"));
        if (size() > 0) {
            int dim = getPhyDimension();
            for (Curve crv : curves)
                if (crv.getPhyDimension() != dim)
                    throw new DimensionException(MessageFormat.format(
                            RESOURCES.getString("incCrvDimension"), "curves", getPhyDimension()));
        }
        for (Double wt : weights)
            requireNonNull(wt);

        index = normalizeIndex(index);
        boolean changed = _crvs.addAll(index, curves);
        if (changed) {
            for (Curve crv : curves) {
                if (!(crv instanceof Immutable))
                    crv.addChangeListener(_childChangeListener);
            }
            fireChangeEvent();  //  Notify change listeners.
        }
        _wts.addAll(index,weights);
        return changed;
    }

    /**
     * Appends all of the curves in the specified array to this LinearComboCurve with a
     * default weight of 1.0 assigned to each. The behavior of this operation is undefined
     * if the specified collection is modified while the operation is in progress. The
     * input curves must have the same physical dimensions as the other items in this
     * list, or an exception is thrown.
     *
     * @param arr curves to be appended onto this collection with a default weight of 1.0
     *            for each. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean addAll(Curve[] arr) {
        return addAll(size(), arr);
    }

    /**
     * Inserts all of the {@link Curve} objects in the specified array into this
     * LinearComboCurve at the specified position with a default weight of 1.0 assigned to
     * each. Shifts the element currently at that position (if any) and any subsequent
     * elements to the right (increases their indices). The new elements will appear in
     * this list in the order that they are returned by the specified collection's
     * iterator. The input curves must have the same physical dimensions as the other
     * items in this list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified array.
     * @param arr   the curves to be inserted into this collection with a default weight
     *              of 1.0 for each. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public boolean addAll(int index, Curve[] arr) {
        return addAll(index, Arrays.asList(requireNonNull(arr)));
    }

    /**
     * Replaces the <@link Curve> at the specified position in this list of curves with
     * the specified Curve. The weight at that position is left unchanged. Null elements
     * are ignored. The input curve must have the same physical dimensions as the other
     * items in this list, or an exception is thrown.
     *
     * @param index The index of the curve to replace (0 returns the 1st curve, -1 returns
     *              the last, -2 returns the 2nd from last, etc).
     * @param curve The curve to be stored at the specified position. The weight at that
     *              position is left unchanged. May not be null.
     * @return The curve previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > size()</code>
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public Curve set(int index, Curve curve) {
        if (size() > 0 && curve.getPhyDimension() != getPhyDimension())
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("incCrvDimension"), "curve", getPhyDimension()));
        index = normalizeIndex(index);

        Curve old = _crvs.set(index, curve);
        if (!(old instanceof Immutable))
            old.removeChangeListener(_childChangeListener);
        if (!(curve instanceof Immutable))
            curve.addChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.

        return old;
    }

    /**
     * Replaces the weight at the specified position in this LinearComboCurve with the
     * specified weight. The curve at that position is left unchanged. Null elements are
     * ignored.
     *
     * @param index  The index of the weight to replace (0 returns the 1st element, -1
     *               returns the last, -2 returns the 2nd from last, etc).
     * @param weight The weight to be stored at the specified position. The curve at that
     *               position is left unchanged. May not be null.
     * @return The weight previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > size()</code>
     */
    @Override
    public Double setWeight(int index, Double weight) {
        index = normalizeIndex(index);
        return _wts.set(index, requireNonNull(weight));
    }

    /**
     * Replaces the <@link Curve> and weight at the specified position in this
     * LinearComboCurve with the specified curve and weight. Null elements are ignored.
     * The input curve must have the same physical dimensions as the other items in this
     * list, or an exception is thrown.
     *
     * @param index  The index of the curve and weight to replace (0 returns the 1st
     *               element, -1 returns the last, -2 returns the 2nd from last, etc).
     * @param curve  The curve to be stored at the specified position. May not be null.
     * @param weight The weight to be stored at the specified position. May not be null.
     * @return The curve previously at the specified position in this list. The previous
     *         weight is lost.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > size()</code>
     * @throws DimensionException if the input curve dimensions are different from this
     * list's dimensions.
     */
    @Override
    public Curve set(int index, Curve curve, Double weight) {
        requireNonNull(curve);
        requireNonNull(weight);
        Curve old = set(index, curve);
        setWeight(index, weight);
        return old;
    }

    /**
     * Removes from this list all the {@link Curve} objects that are contained in the
     * specified collection.
     *
     * @param c Collection that defines which curves will be removed from this list. May
     *          not be null.
     * @return <code>true</code> if this list changed as a result of the call.
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        requireNonNull(c);
        boolean modified = false;
        Iterator<?> it = iterator();
        Iterator<Double> itw = _wts.iterator();
        while (it.hasNext()) {
            itw.next();
            if (c.contains(it.next())) {
                it.remove();
                itw.remove();
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Retains only the curves in this list that are contained in the specified
     * collection. In other words, removes from this LinearComboCurve all the
     * {@link Curve} objects that are not contained in the specified collection.
     *
     * @param c Collection that defines which curves this set will retain. May not be
     *          null.
     * @return <code>true</code> if this list changed as a result of the call.
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        requireNonNull(c);
        boolean modified = false;
        Iterator<Curve> it = iterator();
        Iterator<Double> itw = _wts.iterator();
        while (it.hasNext()) {
            itw.next();
            if (!c.contains(it.next())) {
                it.remove();
                itw.remove();
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Removes a single instance of the specified {@link Curve} (and its associated
     * weight) from this collection, if it is present. More formally, removes an element e
     * such that (o==null ? e==null : o.equals(e)), if this collection contains one or
     * more such elements. Returns true if this collection contained the specified curve
     * (or equivalently, if this collection changed as a result of the call).
     *
     * @param o Curve to be removed from this collection (along with its associated
     *          weight), if present.
     * @return <code>true</code> if this collection changed as a result of the call
     */
    @Override
    public boolean remove(Object o) {
        int index = _crvs.indexOf(o);
        boolean changed = _crvs.remove(o);
        if (changed) {
            _wts.remove(index);
            if (o instanceof AbstractGeomElement && !(o instanceof Immutable)) {
                ((AbstractGeomElement)o).removeChangeListener(_childChangeListener);
            }
            fireChangeEvent();
        }
        return changed;
    }

    /**
     * Removes the curve (and its associated weight) at the specified position in this
     * LinearComboCurve. Shifts any subsequent curves and weights to the left (subtracts
     * one from their indices). Returns the curve that was removed from the list; the
     * removed weight is lost.
     *
     * @param index the index of the curve and weight to remove. (0 returns the 1st
     *              element, -1 returns the last, -2 returns the 2nd from last, etc).
     * @return the curve previously at the specified position.
     */
    @Override
    public Curve remove(int index) {
        index = normalizeIndex(index);
        _wts.remove(index);
        Curve old = _crvs.remove(index);
        if (!(old instanceof Immutable))
            old.removeChangeListener(_childChangeListener);
        fireChangeEvent();  //  Notify change listeners.
        return old;
    }

    /**
     * Removes the curve with the specified name from this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that
     * was removed from the list.
     *
     * @param name the name of the element to remove.
     * @return the element previously at the specified position.
     */
    @Override
    public Curve remove(String name) {

        Curve element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.remove(index);

        return element;
    }

    /**
     * Removes all of the curves from this linear combination. The linear combination will
     * be empty and undefined after this call returns.
     */
    @Override
    public void clear() {
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crv = _crvs.get(i);
            if (!(crv instanceof Immutable))
                crv.removeChangeListener(_childChangeListener);
        }
        _crvs.clear();
        _wts.clear();
        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Returns an iterator over the curves in this LinearComboCurve.
     *
     * @return an iterator over this list of curves.
     */
    @Override
    public java.util.Iterator<Curve> iterator() {
        return _crvs.iterator();
    }

    /**
     * Returns a list iterator over the curves in this LinearComboCurve.
     *
     * @return an iterator over this list of curves.
     */
    @Override
    public java.util.ListIterator<Curve> listIterator() {
        return _crvs.listIterator();
    }

    /**
     * Returns a Curve list iterator from the specified position.
     *
     * @param index the index of first Curve to be returned from the list iterator (by a
     *              call to the next method).
     * @return a list iterator of the curves in this list starting at the specified
     *         position in this list.
     */
    @Override
    public java.util.ListIterator<Curve> listIterator(int index) {
        return _crvs.listIterator(index);
    }

    /**
     * Returns an array containing all of the curves in this collection.
     */
    @Override
    public Curve[] toArray() {
        return (Curve[])_crvs.toArray();
    }

    /**
     * Returns an array containing all of the curves in this collection. If the
     * collection fits in the specified array, it is returned therein. Otherwise, a new
     * array is allocated with the runtime type of the specified array and the size of
     * this collection.
     *
     * @param <T> The type of elements in this LinearComboCurve (Curve type).
     * @param a   the array into which the elements of the collection are to be stored, if
     *            it is big enough; otherwise, a new array of the same type is allocated
     *            for this purpose.
     * @return an array containing the curves of this collection.
     */
    @Override
    @SuppressWarnings("SuspiciousToArrayCall")
    public <T> T[] toArray(T[] a) {
        return _crvs.toArray(a);
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns the physical dimension of the underlying Curve
     * objects or 0 if this list has no Curve objects in it.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        if (isEmpty())
            return 0;
        return get(0).getPhyDimension();
    }

    /**
     * Calculate a point on the curve for the given parametric distance along the curve.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return the calculated point
     */
    @Override
    public Point getRealPoint(double s) {
        validateCurve();
        validateParameter(s);

        StackContext.enter();
        try {
            Point p = _crvs.get(0).getRealPoint(s).times(_wts.get(0));
            int size = _crvs.size();
            for (int i = 1; i < size; ++i) {
                Point crvPnt = _crvs.get(i).getRealPoint(s);
                double Wi = _wts.get(i);
                p = p.plus(crvPnt.times(Wi));
            }

            return StackContext.outerCopy(p);
        } finally {
            StackContext.exit();
        }
    }

    /**
    * Calculate all the derivatives from <code>0</code> to <code>grade</code> with respect
    * to parametric distance on the curve for the given parametric distance along the curve,
    * <code>d^{grade}p(s)/d^{grade}s</code>.
    * <p>
    * Example:<br>
    * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
    * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
    * </p>
    *
    * @param s     Parametric distance to calculate derivatives for (0.0 to 1.0 inclusive).
    * @param grade The maximum grade to calculate the derivatives for (1=1st derivative, 2=2nd derivative, etc)
    * @return A list of derivatives up to the specified grade of the curve at the specified parametric position.
    * @throws IllegalArgumentException if the grade is < 0.
    */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, int grade) {
        validateCurve();
        validateParameter(s);
        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        //  Create a list to hold the output.
        List<Vector<Length>> output = _crvs.get(0).getSDerivatives(s, grade);
        double Wi = _wts.get(0);
        for (int g=0; g <= grade; ++g) {
            Vector<Length> v = output.get(g);
            v = v.times(Wi);
            output.set(g, v);
        }

        int size = _crvs.size();
        for (int i=1; i < size; ++i) {
            List<Vector<Length>> ders = _crvs.get(i).getSDerivatives(s, grade);
            Wi = _wts.get(i);
            for (int g = 0; g <= grade; ++g) {
                Vector<Length> v = ders.get(g);
                v = v.times(Wi);
                v = v.plus(output.get(g));
                output.set(g, v);
            }
        }

        //  Set the origin of all the vectors to p(s).
        Point p = Point.valueOf(output.get(0));
        for (int g=0; g <= grade; ++g) {
            output.get(g).setOrigin(p);
        }

        return output;
    }

    /**
     * Split this curve at the specified parametric position returning a list containing
     * two curves (a lower curve with smaller parametric positions than "s" and an upper
     * curve with larger parametric positions).
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!).
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    @Override
    public GeomList<LinearComboCurve> splitAt(double s) {
        validateCurve();
        validateParameter(s);
        if (parNearEnds(s, TOL_S))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "curve"));

        //  Create LinearComboCurve objects for each end.
        LinearComboCurve cl = LinearComboCurve.newInstance();
        LinearComboCurve cu = LinearComboCurve.newInstance();

        //  Split each child curve at the specified position.
        int size = size();
        for (int i=0; i < size; ++i) {
            Curve crvi = _crvs.get(i);
            Double Wi = _wts.get(i);
            GeomList<Curve> splits = crvi.splitAt(s);
            cl.add(splits.get(0),Wi);
            cu.add(splits.get(1),Wi);
        }

        //  Create the output list.
        GeomList output = GeomList.newInstance();
        output.add(cl, cu);

        return output;
    }

    /**
     * Return <code>true</code> if this LinearComboCurve contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values or weights are NaN or Inf.
     *
     * @return true if this line segment contains valid and finite data.
     */
    @Override
    public boolean isValid() {
        int size = _crvs.size();
        for (int i=size-1; i >= 0; --i) {
            Curve crv = _crvs.get(i);
            double w = _wts.get(i);
            if (!crv.isValid() || Double.isInfinite(w) || Double.isNaN(w))
                return false;
        }
        return true;
    }

    /**
     * Returns a transformed version of this element. The returned list of objects
     * implement {@link GeomTransform} and contains transformed versions of the contents
     * of this list as children.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new LinearCombonCurve that is identical to this one with the specified
     *         transformation applied to member curves.
     * @throws DimensionException if this surface is not 3D.
     */
    @Override
    public LinearComboCurve getTransformed(GTransform transform) {
        requireNonNull(transform);
        LinearComboCurve list = LinearComboCurve.newInstance();
        copyState(list);

        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crvi = _crvs.get(i);
            Double Wi = _wts.get(i);
            list.add((Curve)crvi.getTransformed(transform), Wi);
        }
        return list;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no geometry.
     */
    @Override
    public Point getBoundsMin() {
        if (isEmpty())
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        StackContext.enter();
        try {
            Point minPoint = _crvs.get(0).getBoundsMin().times(_wts.get(0));
            int size = _crvs.size();
            for (int i = 1; i < size; ++i) {
                double Wi = _wts.get(i);
                GeomElement element = _crvs.get(i);
                minPoint = minPoint.plus(element.getBoundsMin().times(Wi));
            }

            return StackContext.outerCopy(minPoint);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        if (isEmpty())
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        StackContext.enter();
        try {
            Point maxPoint = _crvs.get(0).getBoundsMax().times(_wts.get(0));
            int size = _crvs.size();
            for (int i = 1; i < size; ++i) {
                double Wi = _wts.get(i);
                GeomElement element = _crvs.get(i);
                maxPoint = maxPoint.plus(element.getBoundsMax().times(Wi));
            }

            return StackContext.outerCopy(maxPoint);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the unit in which the curves in this linear combination curve are stated.
     *
     * @return The unit in which the curves in this LinearComboCurve are stated or the
     *         default unit if this surface has no member curves.
     */
    @Override
    public Unit<Length> getUnit() {
        if (isEmpty())
            return GeomUtil.getDefaultUnit();

        return _crvs.get(0).getUnit();
    }

    /**
     * Returns the equivalent to this LinearComboCurve but stated in the specified unit.
     *
     * @param unit the length unit of the curve to be returned. May not be null.
     * @return an equivalent to this LinearComboCurve but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public LinearComboCurve to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        LinearComboCurve crv = LinearComboCurve.newInstance();
        copyState(crv);

        //  Convert the curves.
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crvi = _crvs.get(i);
            Double Wi = _wts.get(i);
            crv.add(crvi.to(unit), Wi);
        }

        return crv;
    }

    /**
     * Return the equivalent of this LinearComboCurve converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the curve to return.
     * @return This LinearComboCurve converted to the new dimensions.
     */
    @Override
    public LinearComboCurve toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        LinearComboCurve crv = LinearComboCurve.newInstance();
        copyState(crv);

        //  Convert the curves.
        int size = _crvs.size();
        for (int i = 0; i < size; ++i) {
            Curve crvi = _crvs.get(i);
            Double Wi = _wts.get(i);
            crv.add(crvi.toDimension(newDim), Wi);
        }

        return crv;
    }

    /**
     * Return a NURBS curve representation of this curve to within the specified
     * tolerance.
     *
     * @param tol The greatest possible difference between this curve and the NURBS
     *            representation returned. May not be null.
     * @return A NURBS curve that represents this curve to within the specified tolerance.
     */
    @Override
    public NurbsCurve toNurbs(Parameter<Length> tol) {
        StackContext.enter();
        try {

            //  Grid some points onto the curve.
            PointString<SubrangePoint> str = this.gridToTolerance(requireNonNull(tol));

            //  Fit a cubic NURBS curve to the points.
            int deg = 3;
            if (str.size() <= 3)
                deg = str.size() - 1;
            BasicNurbsCurve curve = CurveFactory.fitPoints(deg, str);
            copyState(curve);   //  Copy over the super-class state for this curve to the new one.

            return StackContext.outerCopy(curve);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the text representation of this geometry element.
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        String className = this.getClass().getName();
        tmp.append(className.substring(className.lastIndexOf(".") + 1));
        tmp.append(": {");
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        tmp.append("\n");
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement e = this.get(i);
            tmp.append(e.toText());
            if (i < size - 1)
                tmp.append(",\n");
            else
                tmp.append("\n");
        }
        if (hasName)
            tmp.append('}');
        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Compares the specified object with this list of <code>Curve</code> objects for
     * equality. Returns true if and only if both collections are of the same type and
     * both collections contain equal elements in the same order.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this list is identical to that list;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        LinearComboCurve that = (LinearComboCurve)obj;
        return this._crvs.equals(that._crvs)
                && this._wts.equals(that._wts)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>LinearComboCurve</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_crvs, _wts);
    }

    /**
     * Returns a copy of this <code>LinearComboCurve</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public LinearComboCurve copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public LinearComboCurve copyToReal() {
        LinearComboCurve newCrv = LinearComboCurve.newInstance();
        copyState(newCrv);

        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Curve crvi = this._crvs.get(i);
            Double Wi = this._wts.get(i);
            Curve crv2i = crvi.copyToReal();
            newCrv.add(crv2i,Wi);
        }

        return newCrv;
    }

    /**
     * Resets the internal state of this object to its default values. Subclasses that
     * override this method must call <code>super.reset();</code> to ensure that the state
     * is reset properly.
     */
    @Override
    public void reset() {
        clear();
        ((FastTable)_crvs).reset();
        ((FastTable)_wts).reset();
        super.reset();
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<LinearComboCurve> XML = new XMLFormat<LinearComboCurve>(LinearComboCurve.class) {

        @Override
        public LinearComboCurve newInstance(Class<LinearComboCurve> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            LinearComboCurve obj = FACTORY.object();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, LinearComboCurve obj) throws XMLStreamException {
            AbstractCurve.XML.read(xml, obj);     // Call parent read.

            FastTable<Curve> crvs = xml.get("Contents", FastTable.class);
            FastTable<Double> wts = xml.get("Weights", FastTable.class);
            obj._crvs = crvs;
            obj._wts = wts;
        }

        @Override
        public void write(LinearComboCurve obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractCurve.XML.write(obj, xml);    // Call parent write.

            xml.add((FastTable)obj._crvs, "Contents", FastTable.class);
            xml.add((FastTable)obj._wts, "Weights", FastTable.class);
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected LinearComboCurve() {
    }

    private static final ObjectFactory<LinearComboCurve> FACTORY = new ObjectFactory<LinearComboCurve>() {
        @Override
        protected LinearComboCurve create() {
            return new LinearComboCurve();
        }

        @Override
        protected void cleanup(LinearComboCurve obj) {
            obj.reset();
            obj._crvs = null;
            obj._wts = null;
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(LinearComboCurve instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static LinearComboCurve copyOf(LinearComboCurve original) {
        LinearComboCurve obj = LinearComboCurve.newInstance();

        int size = original.size();
        for (int i = 0; i < size; ++i) {
            Curve crvi = original.get(i).copy();
            Double Wi = original._wts.get(i);
            obj.add(crvi, Wi);
        }

        original.copyState(obj);
        return obj;
    }

}
