/*
 *   PointGeometry  -- Interface for geometry elements that contain only points.
 *
 *   Copyright (C) 2009-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;

/**
 * Defines the interface for {@link GeomElement} objects that contain only points.
 *
 * <p> Modified by: Joseph A. Huwaldt</p>
 *
 * @author Joseph A. Huwaldt, Date: March 31, 2000
 * @version September 3, 2015
 *
 * @param <T> The sub-type of this PointGeometry object.
 */
public interface PointGeometry<T extends PointGeometry> extends GeomElement<T> {

    /**
     * Return the total number of points in this geometry element.
     *
     * @return The total number of points in this geometry element.
     */
    public int getNumberOfPoints();

    /**
     * Returns the equivalent to this point geometry object but stated in the specified
     * unit.
     *
     * @param unit the length unit of the point geometry to be returned.
     * @return an equivalent to this point geometry object but stated in the specified
     *         unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public T to(Unit<Length> unit) throws ConversionException;
}
