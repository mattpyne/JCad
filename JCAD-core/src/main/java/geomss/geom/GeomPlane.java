/*
 *   GeomPlane  -- Represents a 2D plane in n-D space.
 *
 *   Copyright (C) 2009-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javolution.text.Text;
import javolution.text.TextBuilder;

/**
 * The interface and implementation in common to all 2D planes in n-dimensional space.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: June 14, 2009
 * @version November 25, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class GeomPlane extends AbstractGeomElement<GeomPlane> implements Transformable<GeomPlane> {

    /**
     * Return an immutable version of this plane.
     *
     * @return An immutable version of this plane.
     */
    public abstract Plane immutable();

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 0 as a GeomPlane is not made up of any other
     * elements.
     */
    @Override
    public int size() {
        return 0;
    }

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 0 as a GeomPlane is not parametric.
     */
    @Override
    public int getParDimension() {
        return 0;
    }

    /**
     * Return the normal vector for the plane. The normal vector is a unit vector that is
     * perpendicular to the plane.
     *
     * @return The normal vector for the plane.
     */
    public abstract GeomVector<Dimensionless> getNormal();

    /**
     * Return the constant term of the plane point (e.g.: "D" for a 3D plane:
     * <code>A*x + B*y + C*z = D</code>).
     *
     * @return The constant term of the plane point for this plane.
     */
    public abstract Parameter<Length> getConstant();

    /**
     * Return the reference point for this plane. The reference point is an arbitrary
     * point that is contained in the plane and is used as a reference when drawing the
     * plane.
     *
     * @return The reference point for this plane.
     */
    public abstract GeomPoint getRefPoint();

    /**
     * Return a copy of this plane converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the plane to return.
     * @return A copy of this plane converted to the new dimensions.
     */
    @Override
    public abstract GeomPlane toDimension(int newDim);

    /**
     * Return a new Plane that is identical to this plane, but with a different reference
     * point (the plane is shifted to pass through the specified point).
     *
     * @param p The new reference point that the plane should pass through. May not be null.
     * @return A new Plane that is identical to this plane, but with a different reference
     *         point.
     */
    public Plane changeRefPoint(GeomPoint p) {
        Plane pln = Plane.valueOf(getNormal(), p.immutable());
        copyState(pln);
        return pln;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMin() {
        return getRefPoint().immutable();
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMax() {
        return getRefPoint().immutable();
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this geometry element. This implementation simply returns
     * this plane's reference point.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public GeomPoint getLimitPoint(int dim, boolean max, double tol) {
        return getRefPoint();
    }

    /**
     * Return the closest point on this plane to the input point.
     *
     * @param p The point to find the closest point on this plane to. May not be null.
     * @return The point on the plane closest to the input point.
     */
    public Point getClosest(GeomPoint p) {
        Point cp = GeomUtil.pointPlaneClosest(requireNonNull(p), getRefPoint(), getNormal());
        return cp;
    }

    /**
     * Return the intersection between a curve and this plane.
     *
     * @param curve The curve to intersect with this plane. May not be null.
     * @param tol   Fractional tolerance (in parameter space) to refine the point
     *              positions to.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this plane with the specified curve. If no intersection is
     *         found, an empty PointString is returned.
     */
    public PointString intersect(Curve curve, double tol) {
        return curve.intersect(this, tol);
    }

    /**
     * Return the intersection between an infinite line/ray and this plane.
     *
     * @param L0  A point on the line (origin of the line). May not be null.
     * @param Lu  The direction vector for the line (does not have to be a unit vector).
     *            May not be null.
     * @param out The pre-defined output point on the line that will be filled in to
     *            represent the intersection of the line and the plane (not modified if
     *            there is no intersection). If <code>null</code> is passed, then the
     *            point is not calculated, but the return codes of this method are still
     *            valid.
     * @return DISJOINT if the line and plane are disjoint (parallel), INTERSECT if there
     *         is a unique intersection and COINCIDENT if the line is coincident with the
     *         plane.
     */
    public IntersectType intersect(GeomPoint L0, GeomVector Lu, MutablePoint out) throws DimensionException {
        return GeomUtil.linePlaneIntersect(L0, Lu, this, out);
    }

    /**
     * Return the intersection between a line segment and this plane.
     *
     * @param line The line segment to intersect with this plane. May not be null.
     * @return A PointString containing zero or one subrange points on the line made by
     *         the intersection of the line segment with this plane. If no intersection is
     *         found, an empty PointString is returned.
     */
    public PointString<SubrangePoint> intersect(LineSegment line) throws DimensionException {
        return line.intersect(this, 0);
    }

    /**
     * Returns a copy of this GeomPlane instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public abstract GeomPlane copy();

    /**
     * Return <code>true</code> if this GeomPlane contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     */
    @Override
    public boolean isValid() {
        GeomVector<Dimensionless> n = getNormal();
        if (n.isValid()) {
            double C = getConstant().getValue();
            if (!(Double.isNaN(C) || Double.isInfinite(C)))
                return true;
        }
        return false;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new plane that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this plane is not 3D.
     */
    @Override
    public GeomPlaneTrans getTransformed(GTransform transform) {
        return GeomPlaneTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the text representation of this geometry element that consists of the name
     * followed by the point of a plane. For example:
     * <pre>
     *   {aPlane = {1, 0, 0, 2 ft}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {1, 0, 0, 2 ft}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        final int dimension = this.getPhyDimension();
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        GeomVector<Dimensionless> n = getNormal();
        for (int i = 0; i < dimension; i++) {
            tmp.append(n.get(i));
            tmp.append(", ");
        }
        tmp.append(getConstant());
        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

}
