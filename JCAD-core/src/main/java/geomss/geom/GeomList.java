/**
 * GeomList -- A concrete list of GeomElement objects.
 *
 * Copyright (C) 2009-2016, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A concrete list of arbitrary {@link GeomElement} objects.
 * <p>
 * WARNING: This list allows geometry to be stored in different units and with different
 * physical dimensions. If consistent units or dimensions are required, then the user must
 * specifically convert the list items.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 1, 2009
 * @version March 31, 2016
 *
 * @param <E> The type of element stored in this list.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class GeomList<E extends GeomElement> extends AbstractGeomList<GeomList, E> {

    private FastTable<E> _list;

    /**
     * Return the list underlying this geometry list.
     *
     * @return The list underlying this geometry list.
     */
    @Override
    protected FastTable<E> getList() {
        return _list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>GeomList</code> instance (on
     * the stack when executing in a <code>StackContext</code>) that can store a list of
     * {@link GeomElement} objects.
     *
     * @return A new, empty GeomList.
     */
    public static GeomList newInstance() {
        GeomList list = FACTORY.object();
        list._list = FastTable.newInstance();
        return list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>GeomList</code> instance (on
     * the stack when executing in a <code>StackContext</code>) with the specified name,
     * that can store a list of {@link GeomElement} objects.
     *
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @return A new, empty GeomList with the specified name.
     */
    public static GeomList newInstance(String name) {
        GeomList list = GeomList.newInstance();
        list.setName(name);
        return list;
    }

    /**
     * Return a GeomList containing the {@link GeomElement} objects in the specified
     * collection.
     *
     * @param name     The name to be assigned to this list (may be <code>null</code>).
     * @param elements A collection of geometry elements. May not be null.
     * @return A new GeomList containing the elements in the specified collection.
     */
    public static GeomList valueOf(String name, Collection<? extends GeomElement> elements) {
        for (Object element : elements) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof GeomElement))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "GeomList", "GeomElement"));
        }

        GeomList list = GeomList.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a GeomList containing the {@link GeomElement} objects in the specified list.
     *
     * @param name     The name to be assigned to this list (may be <code>null</code>).
     * @param elements A list of geometry elements. May not be null.
     * @return A new GeomList containing the elements in the specified list.
     */
    public static GeomList valueOf(String name, GeomElement... elements) {

        GeomList list = GeomList.newInstance(name);
        list.addAll(Arrays.asList(requireNonNull(elements)));

        return list;
    }

    /**
     * Return a GeomList containing the {@link GeomElement} objects in the specified
     * array.
     *
     * @param elements An array of geometry elements. May not be null.
     * @return A new GeomList containing the elements in the specified array.
     */
    public static GeomList valueOf(GeomElement... elements) {

        GeomList list = GeomList.newInstance();
        list.addAll(Arrays.asList(requireNonNull(elements)));

        return list;
    }

    /**
     * Returns the range of elements in this list from the specified start and ending
     * indexes.
     *
     * @param first index of the first element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @param last  index of the last element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index >= size()</code>
     */
    @Override
    public GeomList getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        GeomList list = GeomList.newInstance();
        for (int i = first; i <= last; ++i)
            list.add(get(i));
        return list;
    }

    /**
     * Returns an new {@link GeomList} with the elements in this list in reverse order.
     *
     * @return A new GeomList with the elements in this list in reverse order.
     */
    @Override
    public GeomList reverse() {
        GeomList list = GeomList.newInstance();
        int size = this.size();
        for (int i = size - 1; i >= 0; --i) {
            list.add(get(i));
        }
        return list;
    }

    /**
     * Return a copy of this list converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this list is simply
     * returned.
     *
     * @param newDim The dimension of the element to return.
     * @return A copy of this list converted to the new dimensions.
     */
    @Override
    public GeomList toDimension(int newDim) {
        GeomList newList = GeomList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.toDimension(newDim));
        }
        return newList;
    }

    /**
     * Returns the equivalent to this list but with <I>all</I> the elements stated in the
     * specified unit.
     *
     * @param unit the length unit of the list to be returned. May not be null.
     * @return an equivalent to this list but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public GeomList to(Unit<Length> unit) throws ConversionException {
        requireNonNull(unit);
        GeomList newList = GeomList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.to(unit));
        }
        return newList;
    }

    /**
     * Returns a copy of this <code>GeomList</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public GeomList copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this list with any transformations or subranges removed.
     */
    @Override
    public GeomList copyToReal() {
        GeomList newList = GeomList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.copyToReal());
        }
        return newList;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains transformed versions of the contents of this
     * list as children. Any list elements that are not transformable will simply be added
     * to the output list without transformation.
     *
     * @param transform The transform to apply to this geometry element. May not be null.
     * @return A transformed version of this geometry element.
     * @throws DimensionException if this element is not 3D.
     */
    @Override
    public GeomList getTransformed(GTransform transform) {
        requireNonNull(transform);
        GeomList list = GeomList.newInstance();
        copyState(list);    //  Copy over the super-class state for this object to the new one.
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E element = this.get(i);
            if (element instanceof Transformable)
                list.add(((Transformable)element).getTransformed(transform));
            else
                list.add(element);
        }
        return list;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<GeomList> XML = new XMLFormat<GeomList>(GeomList.class) {

        @Override
        public GeomList newInstance(Class<GeomList> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            GeomList obj = GeomList.newInstance();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, GeomList obj) throws XMLStreamException {
            AbstractPointGeomList.XML.read(xml, obj);     // Call parent read.
        }

        @Override
        public void write(GeomList obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractPointGeomList.XML.write(obj, xml);    // Call parent write.
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<GeomList> FACTORY = new ObjectFactory<GeomList>() {
        @Override
        protected GeomList create() {
            return new GeomList();
        }

        @Override
        protected void cleanup(GeomList obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a GeomList instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(GeomList instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected GeomList() { }

    private static GeomList copyOf(GeomList<? extends GeomElement> original) {
        GeomList o = GeomList.newInstance();
        original.copyState(o);
        int size = original.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = original.get(i);
            o.add(element.copy());
        }
        return o;
    }
}
