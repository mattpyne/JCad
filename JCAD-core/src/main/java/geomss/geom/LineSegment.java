/*
 *   LineSegment  -- Represents an abstract line segment in n-D space.
 *
 *   Copyright (C) 2013-2018, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.BracketRoot1D;
import jahuwaldt.tools.math.Evaluatable1D;
import jahuwaldt.tools.math.RootException;
import java.text.MessageFormat;
import java.util.List;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.lang.MathLib;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;

/**
 * The interface and implementation in common to all line segments in n-dimensional space.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: January 14, 2012
 * @version April 3, 2018
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class LineSegment extends AbstractCurve<LineSegment> implements PointGeometry<LineSegment> {

    /**
     * Return the starting point of the line segment.
     *
     * @return The starting point for this line segment.
     */
    public abstract GeomPoint getStart();

    /**
     * Return the end point of the line segment.
     *
     * @return The end point of this line segment.
     */
    public abstract GeomPoint getEnd();

    /**
     * Get unit direction vector for the line segment.
     *
     * @return A unit vector indicating the direction of this line segment.
     */
    public abstract GeomVector<Dimensionless> getUnitVector();

    /**
     * Return the dimensional direction/derivative vector for this line segment. The
     * length of this vector is the length of the line segment, the origin is at the start
     * point and the end of the vector is the line end.
     *
     * @return The dimensional derivative vector for this line segment.
     */
    public abstract GeomVector<Length> getDerivativeVector();

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 2 as a LineSegment has two end points.
     *
     * @return The number of points in this line segment (always returns 2).
     */
    @Override
    public int size() {
        return 2;
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of this geometry element.
     */
    @Override
    public int getPhyDimension() {
        return getStart().getPhyDimension();
    }

    /**
     * Return the total number of points in this geometry element.
     *
     * @return Always returns 2.
     */
    @Override
    public int getNumberOfPoints() {
        return 2;
    }

    /**
     * Split this curve at the specified parametric position returning a list containing
     * two curves (a lower curve with smaller parametric positions than "s" and an upper
     * curve with larger parametric positions).
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!).
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    @Override
    public GeomList<LineSegment> splitAt(double s) {
        validateParameter(s);
        s = roundParNearEnds(s);
        if (parNearEnds(s, TOL_S))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "curve"));

        //  Get the point at the specified split point.
        Point p = getRealPoint(s);

        //  Create the curve segments.
        LineSeg cl = LineSeg.valueOf(getStart().immutable(), p);
        LineSeg cu = LineSeg.valueOf(p, getEnd().immutable());

        //  Create the output list.
        GeomList output = GeomList.newInstance();
        output.add(cl, cu);

        return output;
    }

    /**
     * Return the curvature (kappa = 1/rho; where rho = the radius of curvature) of the
     * curve at the parametric position <code>s</code>. This implementation always returns
     * zero.
     *
     * @param s Parametric distance to calculate the curvature for (0.0 to 1.0 inclusive).
     * @return The curvature of the curve at the specified parametric position in units of
     *         1/Length.
     */
    @Override
    public Parameter getCurvature(double s) {
        validateParameter(s);
        return Parameter.valueOf(0, getUnit().inverse());
    }

    /**
     * Return the variation of curvature or rate of change of curvature (VOC or
     * dKappa(s)/ds) at the parametric position <code>s</code>. The VOC for a line segment
     * is always zero.
     *
     * @param s Parametric distance to calculate the variation of curvature for (0.0 to
     *          1.0 inclusive).
     * @return The variation of curvature of the curve at the specified parametric
     *         position in units of 1/Length.
     */
    @Override
    public Parameter getVariationOfCurvature(double s) {
        validateParameter(s);
        return Parameter.valueOf(0, getUnit().inverse());
    }

    /**
     * Return the torsion of the curve at the parametric position <code>s</code>. The
     * torsion is a measure of the rotation or twist about the tangent vector. Torsion for
     * a line segment is always zero.
     *
     * @param s Parametric distance to calculate the torsion for (0.0 to 1.0 inclusive).
     * @return The torsion of the curve at the specified parametric position in units of
     *         1/Length.
     */
    @Override
    public Parameter getTorsion(double s) {
        validateParameter(s);
        return Parameter.valueOf(0, getUnit().inverse());
    }

    /**
     * Return the arc length of a segment of this curve.
     *
     * @param s1  The starting parametric distance along the curve to begin the arc-length
     *            calculation from.
     * @param s2  The ending parametric distance along the curve to end the arc-length
     *            calculation at.
     * @param eps The desired fractional accuracy on the arc-length. For this curve type,
     *            this parameter is ignored and the exact arc length is returned.
     * @return The arc length of the specified segment of the curve.
     */
    @Override
    public Parameter<Length> getArcLength(double s1, double s2, double eps) {
        validateParameter(s1);
        validateParameter(s2);

        return getArcLength(0).times(MathLib.abs(s2 - s1));
    }

    /**
     * Return a subrange point at the position on the curve with the specified arc-length.
     * If the requested arc-length is &le; 0, the start of the curve is returned. If the
     * requested arc length is &gt; the total arc length of the curve, then the end point
     * is returned.
     *
     * @param targetArcLength The target arc length to find in meters. May not be null.
     * @param tol             Fractional tolerance (in parameter space) to refine the
     *                        point position to. For this curve, this parameter is ignored
     *                        and the exact point position is always returned.
     * @return A subrange point on the curve at the specified arc-length position.
     */
    @Override
    public SubrangePoint getPointAtArcLength(Parameter<Length> targetArcLength, double tol) {
        //  Deal with an obvious case first.
        if (targetArcLength.isApproxZero())
            return SubrangePoint.newInstance(this, Point.valueOf(0));

        double arcLengthValue = targetArcLength.getValue(SI.METER);

        //  Calculate the full arc-length of the curve (tolerance is ignored).
        double fullArcLength = getArcLength(0).getValue(SI.METER);

        //  Is the requested arc length > than the actual arc length?
        if (arcLengthValue > fullArcLength)
            return SubrangePoint.newInstance(this, Point.valueOf(1));

        //  The parametric position is the ratio of the desired arc-length to the full arc-length.
        double s = arcLengthValue / fullArcLength;

        return getPoint(s);
    }

    /**
     * Return a string of points that are gridded onto the curve using the specified
     * spacing and gridding rule.
     *
     * @param gridRule Specifies whether the subdivision spacing is applied with respect
     *                 to arc-length in real space or parameter space. May not be null.
     * @param spacing  A list of values used to define the subdivision spacing.
     *                  <code>gridRule</code> specifies whether the subdivision spacing is
     *                 applied with respect to arc-length in real space or parameter
     *                 space. If the spacing is arc-length, then the values are
     *                 interpreted as fractions of the arc length of the curve from 0 to
     *                 1. May not be null.
     * @return A string of subrange points gridded onto the curve at the specified
     *         parametric positions.
     */
    @Override
    public PointString<SubrangePoint> extractGrid(GridRule gridRule, List<Double> spacing) {

        PointString<SubrangePoint> str = PointString.newInstance();

        switch (gridRule) {
            case PAR:
                //  Parametric spacing.
                for (double s : spacing) {
                    if (s > 1)
                        s = 1;
                    else if (s < 0)
                        s = 0;
                    str.add(this.getPoint(s));
                }
                break;

            case ARC:
                //  Arc-length spacing.
                Parameter<Length> fullArcLength = getArcLength(0);
                for (double s : spacing) {
                    SubrangePoint p = getPointAtArcLength(fullArcLength.times(s), 0);
                    str.add(p);
                }
                break;

            default:
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("crvUnsupportedGridRule"),
                                gridRule.toString()));

        }

        return str;
    }

    /**
     * Return a string of points that are gridded onto the curve with the number of points
     * and spacing chosen to result in straight line segments between the points that have
     * mid-points that are all within the specified tolerance of this curve.
     *
     * @param tol The maximum distance that a straight line between gridded points may
     *            deviate from this curve. Ignored by this implementation.
     * @return A string of subrange points gridded onto the curve.
     */
    @Override
    public PointString<SubrangePoint> gridToTolerance(Parameter<Length> tol) {

        //  Create the list of points.
        PointString str = PointString.newInstance();
        str.add(getPoint(0));
        str.add(getPoint(1));

        return str;
    }

    /**
     * Return the intersection between a plane and this curve.
     *
     * @param plane The plane to intersect with this curve. May not be null.
     * @param tol   Fractional tolerance (in parameter space) to refine the point
     *              positions to. For this curve, this parameter is ignored and the exact
     *              point positions are returned.
     * @return A PointString containing zero or one subrange points made by the
     *         intersection of this curve with the specified plane. If no intersection is
     *         found, an empty PointString is returned.
     */
    @Override
    public PointString<SubrangePoint> intersect(GeomPlane plane, double tol) {
        requireNonNull(plane);
        
        //  Intersect the plane with an infinite line.
        MutablePoint p = MutablePoint.newInstance(getPhyDimension(), getUnit());
        IntersectType type = GeomUtil.linePlaneIntersect(getStart(), getDerivativeVector(), plane, p);

        //  Check for no intersection.
        PointString<SubrangePoint> str = PointString.newInstance();
        if (type == IntersectType.DISJOINT)
            return str;

        //  Determine the parametric position of the intersection point.
        Parameter sDir = p.minus(getStart()).toGeomVector().dot(getDerivativeVector());
        if (sDir.getValue() >= 0) {
            //  The intersection is not at a negative parametric position.
            double length = getArcLength(0).getValue();
            double pp0_dist = p.distance(getStart()).getValue();
            if (pp0_dist <= length) {
                //  The intersection is not beyond the end of the line.
                double s = pp0_dist / length;
                str.add(getPoint(s));
            }
        }

        return str;
    }

    /**
     * Return the intersection between a line segment and this curve.
     *
     * @param line The line segment to intersect. May not be null.
     * @param tol  Tolerance (in physical space) to refine the point positions to. A null
     *             value indicates an exact intersection is required.
     * @return A list containing two PointString instances each containing zero or 1
     *         subrange points, on this line segment and the input line segment
     *         respectively, made by the intersection of this line segment with the
     *         specified line segment. If no intersections are found a list of two empty
     *         PointStrings are returned.
     */
    @Override
    public GeomList<PointString<SubrangePoint>> intersect(LineSegment line, Parameter<Length> tol) {
        //  This is simply a single line-line intersection problem.

        GeomList<PointString<SubrangePoint>> output = GeomList.newInstance();
        output.add(PointString.newInstance());
        output.add(PointString.newInstance());
        
        //  Check bounding boxes first.
        if (!GeomUtil.boundsIntersect(this, line))
                return output;
        
        //  Extract the start point and tangent vector of the two lines.
        GeomPoint p1 = this.getStart();
        GeomVector<Length> t1 = this.getDerivativeVector();
        GeomPoint p2 = line.getStart();
        GeomVector<Length> t2 = line.getDerivativeVector();

        //  Do the line-line intersection problem.
        MutablePoint sOut = MutablePoint.newInstance(2);
        IntersectType type = GeomUtil.lineLineIntersect(p1, t1, p2, t2, tol, sOut, null, null);

        //  Collect the outputs.
        if (type == IntersectType.DISJOINT || 
                (sOut.getValue(0) < 0 || sOut.getValue(0) > 1 || 
                sOut.getValue(1) < 0 || sOut.getValue(1) > 1)) {
            MutablePoint.recycle(sOut);
            return output;
        }

        output.get(0).add(getPoint(sOut.getValue(0)));
        output.get(1).add(line.getPoint(sOut.getValue(1)));
        MutablePoint.recycle(sOut);

        return output;
    }

    /**
     * Return a list of brackets that surround possible closest/farthest points. Each of
     * the brackets will be refined using a root solver in order to find the actual
     * closest or farthest points.
     * <p>
     * This implementation sub-divides the curve into 2 equal-sized segments and looks for
     * a single bracket inside each segment.
     * </p>
     *
     * @param point   The point to find the closest/farthest point on this curve to/from
     *                (guaranteed to be the same physical dimension before this method is
     *                called). Ignored by this implementation.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point. Ignored by this
     *                implementation.
     * @param eval    A function that calculates the dot product of (p(s)-q) and ps(s) on
     *                the curve. May not be null.
     * @return A list of brackets that each surround a potential closest/farthest point.
     */
    @Override
    protected List<BracketRoot1D> guessClosestFarthest(GeomPoint point, boolean closest,
            Evaluatable1D eval) {

        //  Find multiple closest/farthest points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {

            brackets = BracketRoot1D.findBrackets(requireNonNull(eval), 0, 1, 2);

        } catch (RootException err) {
            Logger.getLogger(LineSegment.class.getName()).log(Level.WARNING,
                    "Failed to bracket closest/farthest point.", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Return a list of brackets that surround possible points on this curve that are
     * closest to the target geometry object. Each of the brackets will be refined using a
     * root solver in order to find the actual closest points.
     * <p>
     * This implementation sub-divides the curve into 2 equal-sized segments and looks for
     * a single bracket inside each segment.
     * </p>
     *
     * @param eval A function that calculates the dot product of (p(s)-q) and ps(s) on the
     *             curve where "q" is the closest point on the target geometry object.
     *             May not be null.
     * @return A list of brackets that each surround a potential closest point on this
     *         curve.
     */
    @Override
    protected List<BracketRoot1D> guessClosest(PerpPointPlaneEvaluatable eval) {

        //  Find multiple closest/farthest points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {

            brackets = BracketRoot1D.findBrackets(eval, 0, 1, 2);

        } catch (RootException err) {
            Logger.getLogger(LineSegment.class.getName()).log(Level.WARNING,
                    "Failed to bracket closest point.", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this curve.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance (in parameter space) to refine the min/max point
     *            position to.
     * @return The point found on this curve that is the min or max in the specified
     *         coordinate direction.
     */
    @Override
    public SubrangePoint getLimitPoint(int dim, boolean max, double tol) {
        //  Check the input dimension for consistancy.
        int thisDim = getPhyDimension();
        if (dim < 0 || dim >= thisDim)
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("inputDimOutOfRange"), "line"));

        //  Get the start and end points.
        GeomPoint p0 = getStart();
        GeomPoint p1 = getEnd();

        //  Limit point must always be one of the ends of a line segment.
        if (max) {
            if (p0.get(dim).isGreaterThan(p1.get(dim)))
                return getPoint(0);     //  p0
            else
                return getPoint(1);     //  p1

        }
        //  else min
        if (p0.get(dim).isLessThan(p1.get(dim)))
            return getPoint(0);     //  p0

        return getPoint(1);         //  p1
    }

    /**
     * Return the number of points per segment that should be used when analyzing curve
     * segments by certain methods. Subclasses should override this method to provide a
     * more appropriate number of points per segment.
     *<p>
     * This implementation always returns 1.
     *</p>
     * 
     * @return Always returns 1.
     */
    @Override
    protected int getNumPointsPerSegment() {
        return 1;
    }

    /**
     * Return <code>true</code> if this LineSegment contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     *
     * @return true if this line segment contains valid and finite data.
     */
    @Override
    public boolean isValid() {
        return getStart().isValid() && getEnd().isValid();
    }

    /**
     * Returns <code>true</code> if this curve is a line to within the specified
     * tolerance. This implementation always returns <code>true</code> if the line is not
     * degenerate.
     *
     * @param tol The tolerance for determining if this curve is a line. May not be null.
     * @return Always returns <code>true</code> if the line is not degenerate.
     */
    @Override
    public boolean isLine(Parameter<Length> tol) {
        return !isDegenerate(tol);
    }

    /**
     * Return <code>true</code> if this curve is planar or <code>false</code> if it is
     * not. This implementation always returns <code>true</code>
     *
     * @param tol The geometric tolerance to use in determining if the curve is planar.
     *            Ignored by this implementation.
     *
     * @return Always returns true.
     */
    @Override
    public boolean isPlanar(Parameter<Length> tol) {
        return true;
    }

    /**
     * Returns <code>true</code> if this curve is a circular arc to within the specified
     * tolerance. This implementation always returns <code>false</code>.
     *
     * @param tol The tolerance for determining if this curve is a circular arc.
     *            Ignored by this implementation.
     *
     * @return Always returns false.
     */
    @Override
    public boolean isCircular(Parameter<Length> tol) {
        return false;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link geomss.geom.GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new line segment that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public LineSegTrans getTransformed(GTransform transform) {
        return LineSegTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the unit in which this curve is stated.
     *
     * @return The unit in which this curve is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return getStart().getUnit();
    }

    /**
     * Return a NURBS curve representation of this line segment. An exact representation
     * is returned and the tolerance parameter is ignored.
     *
     * @param tol Ignored. May pass <code>null</code>.
     * @return A NURBS curve that exactly represents this line segment.
     */
    @Override
    public NurbsCurve toNurbs(Parameter<Length> tol) {
        NurbsCurve curve = CurveFactory.createLine(getStart(), getEnd());
        copyState(curve);   //  Copy over the super-class state for this curve to the new one.
        return curve;
    }

    /**
     * Return a NURBS curve representation of this line segment. An exact representation
     * is returned.
     *
     * @return A NURBS curve that exactly represents this line segment.
     */
    public NurbsCurve toNurbs() {
        return toNurbs(null);
    }

    /**
     * Returns the text representation of this geometry element that consists of the name
     * followed by the start and end points. For example:
     * <pre>
     *   {aLine = {{0.0 m, 0.0 m},{1.0 , 0.0 }}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {{0.0 m, 0.0 m},{1.0 , 0.0 }}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }

        tmp.append(getStart().toText());
        tmp.append(",");
        tmp.append(getEnd().toText());

        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns the equivalent to this LineSegment object but stated in the specified
     * unit.
     *
     * @param unit the length unit of the line segment to be returned. May not be null.
     * @return an equivalent to this line segment object but stated in the specified
     *         unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public abstract LineSegment to(Unit<Length> unit) throws ConversionException;
    
}
