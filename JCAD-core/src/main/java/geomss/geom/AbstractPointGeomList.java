/**
 * AbstractPointGeomList -- Interface and implementation in common to all point geometry
 * element lists.
 *
 * Copyright (C) 2009-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import static geomss.geom.AbstractGeomElement.RESOURCES;
import static java.util.Objects.requireNonNull;

/**
 * Partial implementation of a named list of {@link PointGeometry} objects.
 * <p>
 * WARNING: This list allows geometry to be stored in different units. If consistent units
 * are required, then the user must specifically convert the list items.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 3, 2009
 * @version January 26, 2017
 *
 * @param <T> The sub-type of this AbstractPointGeomList list.
 * @param <E> The type of PointGeometry element contained in this geometry list.
 */
@SuppressWarnings({"CloneableImplementsClone", "serial"})
public abstract class AbstractPointGeomList<T extends AbstractPointGeomList, E extends PointGeometry>
        extends AbstractGeomList<T, E> implements PointGeometry<T> {

    /**
     * Replaces the <@link PointGeometry> at the specified position in this list with the
     * specified element. Null elements are ignored. The input element must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param index   The index of the element to replace. (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The element to be stored at the specified position. May not be null.
     * @return The element previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > thisSize()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public E set(int index, E element) {
        requireNonNull(element);
        if (size() > 0 && element.getPhyDimension() != this.getPhyDimension())
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        
        return super.set(index, element);
    }

    /**
     * Inserts the specified {@link PointGeometry} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored. The input
     * value must have the same physical dimensions as the other items in this list, or
     * an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > thisSize()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public void add(int index, E value) {
        requireNonNull(value);
        if (size() > 0 && value.getPhyDimension() != this.getPhyDimension())
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        
        super.add(index, value);
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns the physical dimension of the underlying
     * {@link PointGeometry} objects or 0 if this list has no PointGeometry objects in it.
     */
    @Override
    public int getPhyDimension() {
        if (isEmpty())
            return 0;
        return get(0).getPhyDimension();
    }

    /**
     * Return the total number of points in this geometry element.
     */
    @Override
    public int getNumberOfPoints() {
        int sum = 0;
        for (int i = this.size() - 1; i >= 0; --i) {
            PointGeometry e = this.get(i);
            sum += e.getNumberOfPoints();
        }
        return sum;
    }
}
