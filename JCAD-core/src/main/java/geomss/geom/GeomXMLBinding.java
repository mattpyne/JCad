/**
 * GeomXMLBinding -- An XML binding for working with GeomSS geometry objects.
 *
 * Copyright (C) 2013-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.cst.BasicCSTCurve;
import geomss.geom.cst.CSTClassFunction;
import geomss.geom.cst.CSTCurveTrans;
import geomss.geom.cst.CSTShapeFunction;
import geomss.geom.nurbs.*;
import jahuwaldt.js.param.Parameter;
import java.awt.Color;
import javolution.util.FastTable;
import javolution.xml.XMLBinding;
import javolution.xml.XMLFormat;
import javolution.xml.XMLFormat.InputElement;
import javolution.xml.stream.XMLStreamException;

/**
 * This class represents the binding between GeomSS Java classes and their XML
 * representation (XMLFormat); the binding may be shared among multiple
 * XMLObjectReader/XMLObjectWriter instances (is thread-safe). This binding has been
 * customized to consistently represent GeomSS geometry objects in XML format for the
 * purpose of defining a standard format.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 19, 2013
 * @version January 30, 2017
 */
public class GeomXMLBinding extends XMLBinding {

    /**
     * A custom XML format for java.awt.Color objects. The RGB values in the range 0-255
     * for red, green, blue and alpha are stored in the attributes.
     */
    protected static final XMLFormat<Color> colorXML = new XMLFormat<Color>(null) {
        @Override
        public Color newInstance(Class<Color> cls, InputElement xml) throws XMLStreamException {
            int red = xml.getAttribute("red", 0);
            int green = xml.getAttribute("green", 0);
            int blue = xml.getAttribute("blue", 0);
            int alpha = xml.getAttribute("alpha", 255);
            return new Color(red, green, blue, alpha);
        }

        @Override
        public void read(XMLFormat.InputElement xml, Color obj) { /* Do nothing:  Color is immutable. */ }

        @Override
        public void write(Color obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            xml.setAttribute("red", obj.getRed());
            xml.setAttribute("green", obj.getGreen());
            xml.setAttribute("blue", obj.getBlue());
            xml.setAttribute("alpha", obj.getAlpha());
        }
    };
    private static final long serialVersionUID = 1L;

    public GeomXMLBinding() {

        setAlias(String.class, "String");
        setAlias(Integer.class, "Integer");
        setAlias(Long.class, "Long");
        setAlias(Float.class, "Float");
        setAlias(Double.class, "Double");
        setAlias(org.jscience.mathematics.number.Float64.class, "Float64");
        setAlias(Boolean.class, "Boolean");

        setAlias(Parameter.class, "Parameter");
        setAlias(GTransform.class, "GTransform");
        setAlias(Point.class, "Point");
        setAlias(GeomPointTrans.class, "GeomPointTrans");

        setAlias(PointString.class, "PointString");
        setAlias(PointArray.class, "PointArray");
        setAlias(PointComponent.class, "PointComponent");
        setAlias(PointVehicle.class, "PointVehicle");
        setAlias(GeomList.class, "GeomList");

        setAlias(Vector.class, "Vector_stp");
        setAlias(VectorTrans.class, "VectorTrans");

        setAlias(Plane.class, "Plane");
        setAlias(GeomPlaneTrans.class, "GeomPlaneTrans");

        setAlias(LineSeg.class, "LineSeg");
        setAlias(LineSegTrans.class, "LineSegTrans");
        setAlias(BasicNurbsCurve.class, "BasicNurbsCurve");
        setAlias(NurbsCurveTrans.class, "NurbsCurveTrans");
        setAlias(ControlPoint.class, "ControlPoint");
        setAlias(KnotVector.class, "KnotVector");
        setAlias(CSTShapeFunction.class, "CSTShapeFunction");
        setAlias(CSTClassFunction.class, "CSTClassFunction");
        setAlias(BasicCSTCurve.class, "BasicCSTCurve");
        setAlias(CSTCurveTrans.class, "CSTCurveTrans");

        setAlias(LoftedSurface.class, "LoftedSurface");
        setAlias(TFISurface.class, "TFISurface");
        setAlias(BasicNurbsSurface.class, "BasicNurbsSurface");
        setAlias(NurbsSurfaceTrans.class, "NurbsSurfaceTrans");
        setAlias(ControlPointNet.class, "ControlPointNet");

        setAlias(SubrangePoint.class, "SubrangePoint");
        setAlias(SubrangeCurve.class, "SubrangeCurve");
        setAlias(SubrangeSurface.class, "SubrangeSurface");

        setAlias(Note.class, "Note");
        setAlias(NoteTrans.class, "NoteTrans");

        setAlias(Color.class, "Color");
        setAlias(FastTable.class, "FastTable");

    }

    @Override
    public <T> XMLFormat<T> getFormat(Class<T> cls) {
        if (Color.class.equals(cls))
            return (XMLFormat<T>)colorXML;
        return super.getFormat(cls);
    }
}
