/*
 *   ParametricGeometry  -- Interface for geometry elements that have parametric dimensions.
 *
 *   Copyright (C) 2009-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.util.List;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;

/**
 * Defines the interface for {@link GeomElement} objects that have parametric
 * dimensions.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 31, 2000
 * @version November 27, 2015
 *
 * @param <T> The type of this parametric geometry object.
 */
public interface ParametricGeometry<T extends ParametricGeometry> extends GeomElement<T>, Transformable<T> {

    /**
     * Return a subrange point on the parametric geometry for the given
     * parametric distance along the parametric geometry.
     *
     * @param s parametric distance to calculate a point for. Must be a
     * 1-dimensional point with a value in the range 0 to 1.0.
     * @return The subrange point
     */
    public SubrangePoint getPoint(GeomPoint s);

    /**
     * Calculate a point on the parametric geometry for the given parametric
     * distance along the parametric geometry.
     *
     * @param s parametric distance to calculate a point for. Must be a
     * 1-dimensional point with a value in the range 0 to 1.0.
     * @return The calculated point.
     */
    public Point getRealPoint(GeomPoint s);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code>
     * with respect to parametric position(s) on a parametric object for the
     * given parametric position on the object,
     * <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s parametric position to calculate the derivatives for. Must match
     * the parametric dimension of this parametric surface and have each value
     * in the range 0 to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st
     * derivative, 2=2nd derivative, etc)
     * @return A list of lists of derivatives (one list for each parametric
     * dimension). Each list contains derivatives up to the specified grade at
     * the specified parametric position. Example: for a surface list element 0
     * is the array of derivatives int the 1st parametric dimension (s), then
     * 2nd list element is the array of derivatives in the 2nd parametric
     * dimension (t).
     * @throws IllegalArgumentException if the grade is < 0.
     */
    public List<List<Vector<Length>>> getDerivatives(GeomPoint s, int grade);

    /**
     * Returns the closest point on this parametric object to the specified
     * point.
     *
     * @param point The point to find the closest point on this parametric
     * object to.
     * @param tol Fractional tolerance (in parameter space) to refine the point
     * position to.
     * @return The point on this parametric object that is closest to the
     * specified point.
     */
    public SubrangePoint getClosest(GeomPoint point, double tol);

    /**
     * Returns the farthest point on this parametric object from the specified
     * point.
     *
     * @param point The point to find the farthest point on this parametric
     * object from.
     * @param tol Fractional tolerance (in parameter space) to refine the point
     * position to.
     * @return The {@link SubrangePoint} on this parametric object that is
     * farthest from the specified point.
     */
    public SubrangePoint getFarthest(GeomPoint point, double tol);

    /**
     * Return <code>true</code> if this element is degenerate (i.e.: has length or area
     * less than the specified tolerance).
     *
     * @param tol The tolerance for determining if this element is degenerate. May not be
     *            null.
     * @return true if this element is degenerate.
     */
    public boolean isDegenerate(Parameter<Length> tol);

    /**
     * Returns the equivalent to this parametric object but stated in the
     * specified unit.
     *
     * @param unit the length unit of the parametric object to be returned.
     * @return an equivalent to this parametric object but stated in the
     * specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public T to(Unit<Length> unit) throws ConversionException;

    /**
     * Return a copy of this parametric geometry converted to the specified
     * number of physical dimensions. If the number of dimensions is greater
     * than this element, then zeros are added to the additional dimensions. If
     * the number of dimensions is less than this element, then the extra
     * dimensions are simply dropped (truncated). If the new dimensions are the
     * same as the dimension of this element, then this element is simply
     * returned.
     *
     * @param newDim The dimension of the parametric geometry element to return.
     * @return This parametric geometry element converted to the new dimensions.
     */
    @Override
    public T toDimension(int newDim);

    /**
     * Returns a copy of this ParametricGeometry instance
     * {@link javolution.context.AllocatorContext allocated} by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public T copy();
}
