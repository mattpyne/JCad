/**
 * PointString -- A collection of GeomPoint objects that make up a "string of points".
 *
 * Copyright (C) 2003-2018, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A <code>PointString</code> is a collection of {@link GeomPoint} objects that make up a
 * "string of points". Any number of points may be added to a string, but all points in a
 * string must have the same dimensions.
 * <p>
 * WARNING: This list allows geometry to be stored in different units. If consistent units
 * are required, then the user must specifically convert the list items.
 * </p>
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version April 7, 2018
 * 
 * @param <E> The type of GeomPoint contained in this list of points.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class PointString<E extends GeomPoint> extends AbstractPointGeomList<PointString<E>,E> {

    private FastTable<E> _list;

    /**
     * Return the list underlying this geometry list.
     *
     * @return The list underlying this geometry list.
     */
    @Override
    protected FastTable<E> getList() {
        return _list;
    }

    /**
     * Returns a new, preallocated or recycled <code>PointString</code> instance
     * (on the stack when executing in a <code>StackContext</code>), that can
     * store a list of {@link GeomPoint} objects.
     *
     * @return A new PointString instance.
     */
    public static PointString newInstance() {
        PointString list = FACTORY.object();
        list._list = FastTable.newInstance();
        return list;
    }

    /**
     * Returns a new, preallocated or recycled <code>PointString</code> instance
     * (on the stack when executing in a <code>StackContext</code>) with the
     * specified name, that can store a list of {@link GeomPoint} objects.
     *
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @return A new PointString instance with the specified name.
     */
    public static PointString newInstance(String name) {
        PointString list = PointString.newInstance();
        list.setName(name);
        return list;
    }

    /**
     * Return a PointString made up of the {@link GeomPoint} objects in the
     * specified collection.
     *
     * @param <E> The type of GeomPoint contained in this list of points.
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @param elements A collection of points. May not be null.
     * @return A new PointString instance made up of the specified points.
     */
    public static <E extends GeomPoint> PointString<E> valueOf(String name, Collection<E> elements) {
        for (Object element : elements) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof GeomPoint))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "PointString", "GeomPoint"));
        }

        PointString<E> list = PointString.newInstance(name);
        list.addAll(elements);
        
        return list;
    }

    /**
     * Return a PointString made up of the {@link GeomPoint} objects in the specified
     * array.
     *
     * @param <E>      The type of GeomPoint contained in this list of points.
     * @param name     The name to be assigned to this list (may be <code>null</code>).
     * @param elements An array of points. May not be null.
     * @return A new PointString instance made up of the specified points.
     */
    public static <E extends GeomPoint> PointString<E> valueOf(String name, E... elements) {
        requireNonNull(elements);
        PointString<E> list = PointString.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a PointString made up of the {@link GeomPoint} objects in the
     * specified array.
     *
     * @param <E> The type of GeomPoint contained in this list of points.
     * @param elements An array of points. May not be null.
     * @return A new PointString instance made up of the specified points.
     */
    public static <E extends GeomPoint> PointString<E> valueOf(E... elements) {
        return PointString.valueOf(null, elements);
    }

    /**
     * Return the total number of points in this geometry element.
     *
     * @return The total number of points in this geometry element.
     */
    @Override
    public int getNumberOfPoints() {
        return size();
    }
    
    /**
     * Returns the range of elements in this list from the specified start and
     * ending indexes.
     *
     * @param first index of the first element to return (0 returns the 1st
     *  element, -1 returns the last, etc).
     * @param last index of the last element to return (0 returns the 1st
     *  element, -1 returns the last, etc).
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     *  <code>index >= size()</code>
     */
    @Override
    public PointString<E> getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        PointString<E> list = PointString.newInstance();
        for (int i=first; i <= last; ++i)
            list.add(get(i));

        return list;
    }

    /**
     * Returns an new {@link PointString} with the elements in this list in
     * reverse order.
     *
     * @return A new PointString with the elements in this string in reverse order.
     */
    @Override
    public PointString<E> reverse() {
        PointString<E> list = PointString.newInstance();
        copyState(list);
        int size = this.size();
        for (int i=size-1; i >= 0; --i) {
            list.add(get(i));
        }
        return list;
    }

    /**
     * Returns a new {@link PointString} that is identical to this string but
     * with every other point removed. The 1st point (index = 0) and last point
     * (index = size()-1) are always retained. If there are less than 3 points
     * in the string, then a new string is returned that contains the same
     * points as this string.
     *
     * @return A new PointString identical to this one but with every other
     *      point removed.
     */
    public PointString<E> thin() {
        PointString<E> list = PointString.newInstance();
        copyState(list);
        
        int size = size();
        if (size < 3) {
            list.addAll(this);
            return list;
        }
        
        int sizem1 = size - 1;
        for (int i=0; i < sizem1; i += 2) {
            list.add(get(i));
        }
        list.add(get(size-1));
        
        return list;
    }
    
    /**
     * Returns a new {@link PointString} that is identical to this string but
     * with a new point linearly interpolated half-way between each of the
     * existing points. If there are less than 2 points in the string, then a
     * new string is returned that contains the same point as this string.
     *
     * @return A new PointString that is identical to this string but with a new
     * point linearly interpolated half-way between each of the existing points.
     */
    public PointString enrich() {
        PointString list = PointString.newInstance();
        copyState(list);
        
        int size = size();
        if (size < 2) {
            list.add(get(0));
            return list;
        }
        
        GeomPoint p1 = get(0);
        list.add(p1);
        for (int i=1; i < size; ++i) {
            GeomPoint p2 = get(i);
            Point pa = p1.plus(p2).divide(2);   //  Calculate the average point.
            list.add(pa);
            list.add(p2);
            p1 = p2;
        }
        
        return list;
    }
    
    /**
     * Returns the length of this PointString in terms of the sum of the distances between
     * each consecutive point in the string.
     *
     * @return The length of the string: sum(point(i).distance(point(i-1))).
     */
    public Parameter<Length> length() {
        StackContext.enter();
        try {
            int numPts = size();
            Parameter d = Parameter.ZERO_LENGTH.to(get(0).getUnit());
            for (int i = 1; i < numPts; ++i) {
                Parameter<Length> dist = get(i).distance(get(i - 1));
                if (!dist.isApproxZero())
                    d = d.plus(dist);
            }
            return StackContext.outerCopy(d);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Calculate the average or centroid of all the points in this PointString.
     *
     * @return The average or centroid of all the points in this PointString.
     */
    public Point getAverage() {
        return GeomUtil.averagePoints(this);
    }
    
    /**
     * Return <code>true</code> if this string of points is degenerate (i.e.: has length 
     * less than the specified tolerance).
     *
     * @param tol The tolerance for determining if this string of points is degenerate. 
     * May not be null.
     * @return <code>true</code> if this string of points is degenerate
     */
    public boolean isDegenerate(Parameter<Length> tol) {
        requireNonNull(tol);
        Parameter<Length> cLength = length();
        return cLength.compareTo(tol) <= 0;
    }

    /**
     * Returns <code>true</code> if this string of points is a line to within the
     * specified tolerance.
     *
     * @param tol The tolerance for determining if this string of points is a line.
     * @return <code>true</code> if this string of points is a line
     */
    public boolean isLine(Parameter<Length> tol) {
        //  Reference:  Piegl, L.A., Tiller, W., "Computing Offsets of NURBS Curves and Surfaces",
        //              Computer Aided Design 31, 1999, pgs. 147-156.
        requireNonNull(tol);

        StackContext.enter();
        try {
            //  Extract the string end points.
            GeomPoint p0 = get(0), p1 = get(-1);

            //  If the end points are coincident, then it can't be a line.
            Vector<Length> tv = p1.minus(p0).toGeomVector();
            if (!tv.mag().isGreaterThan(tol))
                return false;
            
            //  If there are only two non-coincident points, then it must be a line.
            if (size() == 2)
                return true;
            
            //  Get a unit vector for the potential line.
            Vector<Dimensionless> uv = tv.toUnitVector();

            //  Loop over all the points in the string.
            Parameter c2 = tv.dot(tv);
            int numPnts = size() - 1;
            for (int i = 1; i < numPnts; ++i) {
                GeomPoint pi = get(i);

                //  Check distance of this point from infinite line p0-uv.
                if (GeomUtil.pointLineDistance(pi, p0, uv).isGreaterThan(tol))
                    return false;

                //  See if the point falls in the segment p0 to p1.
                Vector<Length> w = pi.minus(p0).toGeomVector();
                Parameter c1 = w.dot(tv);
                if (c1.getValue() < 0)
                    return false;
                if (c2.isLessThan(c1))
                    return false;
            }

            //  Must be a straight line.
            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Generic/default tolerance for use in root finders, etc.
     */
    public static final double GTOL = 1e-6;

    /**
     * Return <code>true</code> if this string of points is planar or <code>false</code>
     * if it is not.
     *
     * @param tol The geometric tolerance to use in determining if the string of points is
     *            planar.
     * @return <code>true</code> if this string of points is planar
     */
    public boolean isPlanar(Parameter<Length> tol) {
        //  If the string is less than 3D, then it must be planar.
        int numDims = getPhyDimension();
        if (numDims <= 2)
            return true;

        //  If there are 3 or fewer points, then it must be planar.
        int numPnts = size();
        if (numPnts < 3)
            return true;

        StackContext.enter();
        try {
            //  Is this string of points degenerate?
            if (isDegenerate(tol))
                return true;

            //  Is the string of points linear?
            if (isLine(tol))
                return true;

            //  Extract 3 points from the string of points and form a plane form them.
            GeomPoint p0 = get(0);
            int idx1 = numPnts / 3;
            if (idx1 == 0)
                ++idx1;
            int idx2 = 2 * numPnts / 3;
            if (idx2 == idx1)
                ++idx2;
            GeomPoint p1 = get(idx1);
            GeomPoint p2 = get(idx2);
            Vector<Length> v1 = p1.minus(p0).toGeomVector();
            Vector<Length> v2 = p2.minus(p0).toGeomVector();
            Vector n = v1.cross(v2);
            Vector<Dimensionless> nhat;
            if (n.magValue() > GTOL) {
                nhat = n.toUnitVector();
                nhat.setOrigin(Point.newInstance(getPhyDimension()));

            } else {
                //  Try a different set of points on the string.
                idx1 = numPnts / 4;
                if (idx1 == 0)
                    ++idx1;
                idx2 = 3 * numPnts / 4;
                if (idx2 == idx1)
                    ++idx2;
                p1 = get(idx1);
                p2 = get(idx2);
                v1 = p1.minus(p0).toGeomVector();
                v2 = p2.minus(p0).toGeomVector();
                n = v1.cross(v2);
                if (n.magValue() > GTOL) {
                    nhat = n.toUnitVector();
                    nhat.setOrigin(Point.newInstance(getPhyDimension()));
                } else {
                    //  Points from input curve likely form a straight line.
                    nhat = GeomUtil.calcYHat(p1.minus(p2).toGeomVector().toUnitVector());
                    nhat = v2.cross(nhat).toUnitVector();
                }
            }

            //  Loop over all the points in the point string
            //  (except 1st, which was used to define the plane).
            for (int i = 1; i < numPnts; ++i) {
                GeomPoint pi = get(i);

                //  Find deviation from a plane for each point by projecting a vector from p0 to pi
                //  onto the normal for the arbitrarily chosen points on the curve.
                //  If the project is anything other than zero, the points are not
                //  in the same plane.
                Vector<Length> p1pi = pi.minus(p0).toGeomVector();
                if (!p1pi.mag().isApproxZero()) {
                    Parameter proj = p1pi.dot(nhat);
                    if (proj.isLargerThan(tol))
                        return false;
                }
            }
        } finally {
            StackContext.exit();
        }

        return true;
    }
  
    /**
     * Return a new {@link PointString} with the points in this list sorted into ascending
     * order with respect to the specified coordinate dimension. This can also be used to
     * remove duplicate points after the list has been sorted. To sort in descending
     * order, use this method followed immediately by reverse() (e.g.: 
     * <code>str.sort(Point.X,false,null).reverse();</code>).
     *
     * @param dim       The physical coordinate dimension to be sorted (e.g.: 0=X, 1=Y, etc).
     * @param removeDup Duplicate points are removed from the output if this is true.
     * @param tol       The tolerance for identifying duplicate points. If null is passed,
     *                  then essentially exact equality is required (if removeDup is
     *                  false, you may pass null).
     * @return A new PointString with the points in this list sorted into ascending order
     *         with respect to the specified coordinate dimension and possibly with
     *         duplicate points removed.
     */
    public PointString<E> sort(int dim, boolean removeDup, Parameter<Length> tol) {
        //  Create a new PointString and add the points to it.
        PointString<E> str = PointString.newInstance();
        str.addAll(this);
        
        //  Sort the new PointString
        Collections.sort(str, new PntComparator(dim));
        
        //  Remove duplicates if requested.
        if (removeDup) {
            E old = null;
            Iterator<E> it = str.iterator();
            while (it.hasNext()) {
                E p = it.next();
                if (p.isApproxEqual(old, tol))
                    it.remove();
                old = p;
            }
        }
        
        return str;
    }

    /**
     * A Comparator that compares the specified dimension of two points for order.
     */
    private class PntComparator implements Comparator<GeomPoint>, Serializable {
        private final int _dim;

        public PntComparator(int dim) {
            _dim = dim;
        }

        @Override
        public int compare(GeomPoint o1, GeomPoint o2) {
            double v1 = o1.getValue(_dim);
            double v2 = o2.getValue(_dim, o1.getUnit());
            return Double.compare(v1, v2);
        }
    }

    /**
     *  Return the equivalent of this list converted to the specified number
     *  of physical dimensions.  If the number of dimensions is greater than
     *  this element, then zeros are added to the additional dimensions.
     *  If the number of dimensions is less than this element, then
     *  the extra dimensions are simply dropped (truncated).  If
     *  the new dimensions are the same as the dimension of this element,
     *  then this list is simply returned.
     *
     *  @param newDim  The dimension of the element to return.
     *  @return The equivalent of this list converted to the new dimensions.
     */
    @Override
    public PointString toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;
        PointString newList = PointString.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i=0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.toDimension(newDim));
        }
        return newList;
    }

    /**
     * Returns the equivalent to this list but with <I>all</I> the elements stated in the
     * specified unit.
     *
     * @param unit the length unit of the list to be returned. May not be null.
     * @return an equivalent to this list but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public PointString to(Unit<Length> unit) {
        requireNonNull(unit);
        PointString list = PointString.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E e = this.get(i);
            list.add(e.to(unit));
        }
        return list;
    }

    /**
     * Returns a copy of this <code>PointString</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public PointString<E> copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges
     * removed (applied).
     *
     * @return A copy of this object with any transformations or subranges
     * removed (applied).
     */
    @Override
    public PointString<E> copyToReal() {
        PointString<E> newList = PointString.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i=0; i < size; ++i) {
            E element = this.get(i);
            newList.add((E)element.copyToReal());
        }
        return newList;
    }
    
    /**
     * Returns transformed version of this element. The returned object
     * implements {@link GeomTransform} and contains transformed versions of the
     * contents of this list as children.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new PointString that is identical to this one with the
     *      specified transformation applied.
     * @throws DimensionException if this element is not 3D.
     */
    @Override
    public PointString getTransformed(GTransform transform) {
        requireNonNull(transform);
        PointString list = PointString.newInstance();
        copyState(list);
        int size = this.size();
        for (int i=0; i < size; ++i) {
            E element = this.get(i);
            list.add(element.getTransformed(transform));
        }
        return list;
    }

    /**
     * Replaces the {@link GeomPoint} at the specified position in this list with the
     * specified element. Null elements are ignored. The input element must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param index   The index of the element to replace (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The element to be stored at the specified position. May not be null.
     * @return The element previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if <code>index > size()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public E set(int index, E element) {
        return super.set(index, requireNonNull(element));
    }

    /**
     * Inserts the specified {@link GeomPoint} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored. The input
     * value must have the same physical dimensions as the other items in this list, or
     * an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the list is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input value dimensions are different from
     * this list's dimensions.
     */
    @Override
    public void add(int index, E value) {
        super.add(index, requireNonNull(value));
    }

    /**
     * Inserts all of the {@link GeomPoint} objects in the specified collection into this
     * list at the specified position. Shifts the element currently at that position (if
     * any) and any subsequent elements to the right (increases their indices). The new
     * elements will appear in this list in the order that they are returned by the
     * specified collection's iterator. The behavior of this operation is unspecified if
     * the specified collection is modified while the operation is in progress. Note that
     * this will occur if the specified collection is this list, and it's nonempty.  The
     * input elements must have the same physical dimensions as the other items in this
     * list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified collection.
     * @param c     Elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        int thisSize = this.size();
        for (Object element : c) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof GeomPoint))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "PointString", "GeomPoint"));
            if (thisSize != 0) {
                if (((GeomElement)element).getPhyDimension() != this.getPhyDimension())
                    throw new DimensionException(RESOURCES.getString("dimensionErr"));
            }
        }
        return super.addAll(index, c);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<PointString> XML = new XMLFormat<PointString>(PointString.class) {

        @Override
        public PointString newInstance(Class<PointString> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            PointString obj = PointString.newInstance();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, PointString obj) throws XMLStreamException {
            AbstractPointGeomList.XML.read(xml, obj);     // Call parent read.
        }

        @Override
        public void write(PointString obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractPointGeomList.XML.write(obj, xml);    // Call parent write.
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<PointString> FACTORY = new ObjectFactory<PointString>() {
        @Override
        protected PointString create() {
            return new PointString();
        }
        @Override
        protected void cleanup(PointString obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled immediately.
     */
    public static void recycle(PointString instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected PointString() {}

    private static <E2 extends GeomPoint> PointString<E2> copyOf(PointString<E2> original) {
        PointString<E2> o = PointString.newInstance();
        original.copyState(o);
        int size = original.size();
        for (int i=0; i < size; ++i) {
            E2 element = original.get(i);
            o.add((E2)element.copy());
        }
        return o;
    }

    /**
     * Tests the methods in this class.
     *
     * @param args Command-line arguments (not used).
     */
    public static void main(String args[]) {
        System.out.println("Testing PointString:");

        Point p1 = Point.valueOf(1, 4, 6, NonSI.FOOT);
        Point p2 = Point.valueOf(7, 2, 5, NonSI.FOOT);
        Point p3 = Point.valueOf(10, 8, 3, NonSI.FOOT);
        Point p4 = Point.valueOf(12, 11, 9, NonSI.FOOT);
        PointString<Point> str1 = PointString.valueOf("A String", p1, p2, p3, p4);
        str1.putUserData("Creator", "Joe Huwaldt");
        str1.putUserData("Date", "June 18, 2013");
        System.out.println("str1 = " + str1);
        System.out.println("points = ");
        for (GeomPoint point : str1) {
            System.out.println(point);
        }

        Vector<Length> V = Vector.valueOf(SI.METER, 2, 0, 0);
        PointString<?> str2 = str1.getTransformed(GTransform.newTranslation(V));
        System.out.println("\nTranslate str1 by V = " + V);
        System.out.println("str2 = " + str2);
        System.out.println("points = ");
        for (GeomPoint point : str2) {
            System.out.println(point);
        }

        V = Vector.valueOf(NonSI.FOOT, 0,2,0);
        PointString<?> str3 = str2.getTransformed(GTransform.newTranslation(V));
        System.out.println("\nTranslate str2 by V = " + V);
        System.out.println("str3 = " + str3);
        System.out.println("points = ");
        for (GeomPoint point : str3) {
            System.out.println(point);
        }

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new GeomXMLBinding();

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(str1, "PointString", PointString.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}


