/**
 * AbstractSurface -- Represents the implementation in common to all surfaces in nD space.
 *
 * Copyright (C) 2010-2017, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.BasicNurbsSurface;
import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.CurveUtils;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.*;
import static jahuwaldt.tools.math.MathTools.isApproxEqual;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.quantity.Volume;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.*;
import static javolution.lang.MathLib.abs;
import static javolution.lang.MathLib.round;
import static javolution.lang.MathLib.sqrt;
import javolution.util.FastTable;
import org.apache.commons.math3.linear.SingularMatrixException;

/**
 * The interface and implementation in common to all surfaces.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 14, 2010
 * @version November 7, 2017
 *
 * @param <T> The sub-type of this AbstractSurface element.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class AbstractSurface<T extends AbstractSurface> extends AbstractGeomElement<T> implements Surface<T> {

    /**
     * Reference 1: Michael E. Mortenson, "Geometric Modeling, Third Edition",
     * ISBN:9780831132989, 2008.
     */
    
    /**
     * Generic/default tolerance for use in root finders, etc.
     */
    public static final double GTOL = 1e-6;

    //  Square-Root of EPS.
    private static final double SQRT_EPS = Parameter.SQRT_EPS;

    //  A Parameter version of GTOL.
    private static final Parameter<Length> GTOLP;

    static {
        ImmortalContext.enter();
        try {
            GTOLP = Parameter.valueOf(GTOL / 10, SI.METER);
        } finally {
            ImmortalContext.exit();
        }
    }

    /**
     * Tolerance allowed on parametric spacing being == 0 or == 1.
     */
    protected static final double TOL_ST = Parameter.SQRT_EPS;

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 2.
     */
    @Override
    public int getParDimension() {
        return 2;
    }

    /**
     * Checks the input parameters for validity (in the range 0 to 1 for example) and
     * throws an exception if it is not valid.
     *
     * @param s 1st parametric dimension distance (0.0 to 1.0 inclusive).
     * @param t 2nd parametric dimension distance (0.0 to 1.0 inclusive).
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    protected void validateParameter(double s, double t) {
        if (s < -TOL_ST || s > 1 + TOL_ST)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfInvalidSValue"), s));
        if (t < -TOL_ST || t > 1 + TOL_ST)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("srfInvalidTValue"), t));
    }

    /**
     * Checks the input parameters for validity (in the range 0 to 1 for example) and
     * throws an exception if it is not valid.
     *
     * @param st The parametric position. Must be a 2-dimensional point with each value in
     *           the range 0 to 1.0. Units are ignored. May not be null.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    protected void validateParameter(GeomPoint st) {
        if (isNull(st) || st.getPhyDimension() != 2)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("invalidParamDim"), "Surface", 2));
        validateParameter(st.getValue(0), st.getValue(1));
    }

    /**
     * If the input parameter is within tol of 0 or 1, or outside of that bounds, then the
     * output value will be set to exactly 0 or 1. Otherwise the input value will be
     * returned.
     *
     * @param s The parameter to check for approximate end values.
     * @return The input value or 0 and 1 if the input value is within tolerance of the
     *         ends.
     */
    protected static final double roundParNearEnds(double s) {
        return (s < TOL_ST ? 0. : (s > 1. - TOL_ST ? 1. : s));
    }
    
    /**
     * Return true if the input parameter is within tolerance of the start of the
     * parameter range (s &le; tol).
     *
     * @param s   The parameter to check.
     * @param tol The tolerance to use for the check.
     * @return true if the parameter is within tolerance of zero.
     */
    protected static final boolean parNearStart(double s, double tol) {
        return s <= tol;
    }

    /**
     * Return true if the input parameter is within tolerance of the end of the parameter
     * range (s &ge; 1.0 - tol).
     *
     * @param s   The parameter to check.
     * @param tol The tolerance to use for the check.
     * @return true if the parameter is within tolerance of 1.0.
     */
    protected static final boolean parNearEnd(double s, double tol) {
        return s >= 1.0 - tol;
    }

    /**
     * Return true if the input parameter is within tolerance of either the end of the
     * parameter range or outside that bounds (s &le; tol || s &ge; 1.0 - tol).
     *
     * @param s   The parameter to check.
     * @param tol The tolerance to use for the check.
     * @return true if the parameter is within tolerance of 0.0 or 1.0.
     */
    protected static final boolean parNearEnds(double s, double tol) {
        return s <= tol || s >= 1.0 - tol;
    }

    /**
     * Always throws UnsupportedOperationException as this method is not supported for
     * this object type! Subclasses that do support transpose() should override this
     * method.
     *
     * @return A new surface, identical to this one, but with the transpose of the
     *         parameterization.
     * @throws UnsupportedOperationException as this method is not supported on this
     * object type.
     */
    @Override
    public T transpose() {
        throw new UnsupportedOperationException(RESOURCES.getString("srfTransposeUnsupported"));
    }

    /**
     * Split this surface at the specified parametric S-position returning a list
     * containing two new surfaces (a lower surface with smaller S-parametric positions
     * than "s" and an upper surface with larger S-parametric positions).
     *
     * @param st The S-parametric position where this surface should be split (must not be
     *           0 or 1!). Must be a 2-dimensional point with each value in the range 0 to
     *           1, only the 1st value is used. Units are ignored. May not be null.
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<T> splitAtS(GeomPoint st) {
        return splitAtS(st.getValue(0));
    }

    /**
     * Split this surface at the specified parametric T-position returning a list
     * containing two new surfaces (a lower surface with smaller T-parametric positions
     * than "t" and an upper surface with larger T-parametric positions).
     *
     * @param st The T-parametric position where this surface should be split (must not be
     *           0 or 1!). Must be a 2-dimensional point with each value in the range 0 to
     *           1, only the 2nd value is used. Units are ignored. May not be null.
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<T> splitAtT(GeomPoint st) {
        return splitAtT(st.getValue(1));
    }

    /**
     * Return a subrange point on the surface for the given parametric position on the
     * surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @return The subrange point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public SubrangePoint getPoint(double s, double t) {
        validateParameter(s, t);
        s = roundParNearEnds(s);
        t = roundParNearEnds(t);
        return SubrangePoint.newInstance(this, Point.valueOf(s, t));
    }

    /**
     * Return a subrange point on the surface for the given parametric position on the
     * surface.
     *
     * @param st The parametric position to calculate a point for. Must be a 2-dimensional
     *           point with each value in the range 0 to 1.0. Units are ignored.
     * @return The subrange point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public SubrangePoint getPoint(GeomPoint st) {
        validateParameter(st);
        return SubrangePoint.newInstance(this, st);
    }

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param st The parametric position to calculate a point for. Must be a 2-dimensional
     *           point with each value in the range 0 to 1.0. Units are ignored.
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(GeomPoint st) {
        return getRealPoint(st.getValue(0), st.getValue(1));
    }

    /**
     * Return a subrange curve on the surface for the given parametric position curve.
     *
     * @param pcrv A curve in parametric space indicating the parametric positions on the
     *             the surface represented by the subrange curve. Must be 2D with values
     *             between 0 and 1.
     * @return The subrange curve on the surface at the specified parametric curve
     *         positions.
     */
    @Override
    public SubrangeCurve getCurve(Curve pcrv) {
        return SubrangeCurve.newInstance(this, requireNonNull(pcrv));
    }

    /**
     * Return a subrange curve at a constant parametric s-value.
     *
     * @param s The parametric s-position to extract a subrange curve.
     * @return The subrange curve on the surface at the specified s-value.
     * @throws IllegalArgumentException if there is any problem with the parameter value.
     */
    @Override
    public SubrangeCurve getSCurve(double s) {
        validateParameter(s, 0);
        s = roundParNearEnds(s);
        PointString<Point> pstr = PointString.valueOf(Point.valueOf(s, 0), Point.valueOf(s, 1));
        Curve pcrv = CurveFactory.fitPoints(1, pstr);
        PointString.recycle(pstr);
        return getCurve(pcrv);
    }

    /**
     * Return a subrange curve at a constant parametric t-value.
     *
     * @param t The parametric t-position to extract a subrange curve.
     * @return The subrange curve on the surface at the specified t-value.
     * @throws IllegalArgumentException if there is any problem with the parameter value.
     */
    @Override
    public SubrangeCurve getTCurve(double t) {
        validateParameter(0, t);
        t = roundParNearEnds(t);
        PointString<Point> pstr = PointString.valueOf(Point.valueOf(0, t), Point.valueOf(1, t));
        Curve pcrv = CurveFactory.fitPoints(1, pstr);
        PointString.recycle(pstr);
        return getCurve(pcrv);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric position(s) on a parametric object for the given parametric
     * position on the object, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param st    The parametric position to calculate the derivatives for. Must match
     *              the parametric dimension of this parametric surface and have each
     *              value in the range 0 to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of lists of derivatives (one list for each parametric dimension).
     *         Each list contains derivatives up to the specified grade at the specified
     *         parametric position. Example: for a surface list element 0 is the array of
     *         derivatives in the 1st parametric dimension (s), then 2nd list element is
     *         the array of derivatives in the 2nd parametric dimension (t).
     * @throws IllegalArgumentException if the grade is < 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<List<Vector<Length>>> getDerivatives(GeomPoint st, int grade) {
        validateParameter(st);

        List<List<Vector<Length>>> list = FastTable.newInstance();
        list.add(getSDerivatives(st.getValue(0), st.getValue(1), grade, true));
        list.add(getTDerivatives(st.getValue(0), st.getValue(1), grade, true));

        return list;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param st    The parametric position to calculate the derivatives for. Must be a
     *              2-dimensional point with each value in the range 0 to 1.0. Units are
     *              ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(GeomPoint st, int grade) {
        validateParameter(st);
        return getSDerivatives(st.getValue(0), st.getValue(1), grade, true);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     1st parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param t     2nd parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param grade The maximum grade to calculate the u-derivatives for (1=1st
     *              derivative, 2=2nd derivative, etc)
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade) {
        return getSDerivatives(s, t, grade, true);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the u-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public abstract List<Vector<Length>> getSDerivatives(double s, double t, int grade, boolean scaled);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param st    The parametric position to calculate the derivatives for. Must be a
     *              2-dimensional point with each value in the range 0 to 1.0. Units are
     *              ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(GeomPoint st, int grade) {
        validateParameter(st);
        return getTDerivatives(st.getValue(0), st.getValue(1), grade, true);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s     1st parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param t     2nd parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param grade The maximum grade to calculate the v-derivatives for (1=1st
     *              derivative, 2=2nd derivative, etc)
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade) {
        return getTDerivatives(s, t, grade, true);
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the v-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public abstract List<Vector<Length>> getTDerivatives(double s, double t, int grade, boolean scaled);

    /**
     * Calculate a derivative with respect to parametric s-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/ds</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2s</code>; etc.
     * </p>
     *
     * @param st    The parametric position to calculate the derivative for. Must be a
     *              2-dimensional point with each value in the range 0 to 1.0. Units are
     *              ignored.
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *              derivative, etc)
     * @return The specified s-derivative of the surface at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public Vector<Length> getSDerivative(GeomPoint st, int grade) {
        validateParameter(st);
        return getSDerivative(st.getValue(0), st.getValue(1), grade, true);
    }

    /**
     * Calculate a derivative with respect to parametric s-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/ds</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2s</code>; etc.
     * </p>
     *
     * @param s     1st parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param t     2nd parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *              derivative, etc)
     * @return The specified s-derivative of the surface at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public Vector<Length> getSDerivative(double s, double t, int grade) {
        return getSDerivative(s, t, grade, true);
    }

    /**
     * Calculate a derivative with respect to parametric s-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/ds</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2s</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *               derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return The specified s-derivative of the surface at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    protected Vector<Length> getSDerivative(double s, double t, int grade, boolean scaled) {
        List<Vector<Length>> ders = getSDerivatives(s, t, grade, scaled);
        Vector<Length> derivative = ders.get(grade);
        FastTable.recycle((FastTable)ders);
        return derivative;
    }

    /**
     * Calculate a derivative with respect to parametric t-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/dt</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2t</code>; etc.
     * </p>
     *
     * @param st    The parametric position to calculate the derivative for. Must be a
     *              2-dimensional point with each value in the range 0 to 1.0. Units are
     *              ignored.
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *              derivative, etc)
     * @return The specified t-derivative of the surface at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public Vector<Length> getTDerivative(GeomPoint st, int grade) {
        validateParameter(st);
        return getTDerivative(st.getValue(0), st.getValue(1), grade, true);
    }

    /**
     * Calculate a derivative with respect to parametric t-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/dt</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2t</code>; etc.
     * </p>
     *
     * @param s     1st parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param t     2nd parametric dimension distance to calculate derivative for (0.0 to
     *              1.0 inclusive).
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *              derivative, etc)
     * @return The specified t-derivative of the surface at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public Vector<Length> getTDerivative(double s, double t, int grade) {
        return getTDerivative(s, t, grade, true);
    }

    /**
     * Calculate a derivative with respect to parametric t-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/dt</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2t</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *               derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return The specified t-derivative of the surface at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    protected Vector<Length> getTDerivative(double s, double t, int grade, boolean scaled) {
        List<Vector<Length>> ders = getTDerivatives(s, t, grade, scaled);
        Vector<Length> derivative = ders.get(grade);
        FastTable.recycle((FastTable)ders);
        return derivative;
    }

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param st The parametric position to calculate the twist vector for. Must be a
     *           2-dimensional point with each value in the range 0 to 1.0. Units are
     *           ignored.
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    @Override
    public Vector<Length> getTwistVector(GeomPoint st) {
        validateParameter(st);
        return getTwistVector(st.getValue(0), st.getValue(1));
    }

    /**
     * Return the at least 3D normal vector for this surface at the given parametric
     * position (s,t) on this surface.
     *
     * @param s 1st parametric dimension distance to calculate normal for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate normal for (0.0 to 1.0
     *          inclusive).
     * @return The at least 3D normal vector of the surface at the specified parametric
     *         position.
     */
    @Override
    public Vector<Dimensionless> getNormal(double s, double t) {
        //  Ref. 1, Eqn. 12.61.

        StackContext.enter();
        try {
            //  Get the surface position and derivative in the s direction.
            Vector pu = getSDerivative(s, t, 1, false);

            //  Get the surface derivative in the t direction.
            Vector pw = getTDerivative(s, t, 1, false);

            //  Try to deal with collapsed edges by perturbing slightly off the edge.
            if (pu.magValue() < SQRT_EPS) {
                double offset = sqrt(TOL_ST);
                if (t > 1 - offset)
                    offset *= -1;
                pu = getSDerivative(s, t + offset, 1, false);
            }
            if (pw.magValue() < SQRT_EPS) {
                double offset = sqrt(TOL_ST);
                if (s > 1 - offset)
                    offset *= -1;
                pw = getTDerivative(s + offset, t, 1, false);
            }

            //  Make sure the vectors are at least 3D.
            if (getPhyDimension() < 3) {
                pu = pu.toDimension(3);
                pw = pw.toDimension(3);
            }

            //  Normal vector is the cross product of pu and pw vectors.
            Vector n = pu.cross(pw).toUnitVector();

            return StackContext.outerCopy(n);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the normal vector for this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param st The parametric position to calculate the normal for. Must be a
     *           2-dimensional point with each value in the range 0 to 1.0. Units are
     *           ignored.
     * @return The normal vector of the surface at the specified parametric position.
     */
    @Override
    public Vector<Dimensionless> getNormal(GeomPoint st) {
        validateParameter(st);
        return getNormal(st.getValue(0), st.getValue(1));
    }

    /**
     * Return the tangent plane to this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param s 1st parametric dimension distance to calculate tangent plane for (0.0 to
     *          1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate tangent plane for (0.0 to
     *          1.0 inclusive).
     * @return The tangent plane of the surface at the specified parametric position.
     */
    @Override
    public Plane getTangentPlane(double s, double t) {
        //  Ref. 1, Eqn. 12.63.

        //  Get the surface normal at the specified position.
        Vector<Dimensionless> n = getNormal(s, t);

        //  Get the surface point stored in the normal.
        GeomPoint p0 = n.getOrigin();

        //  Create the plane.
        Plane p = Plane.valueOf(n, p0.immutable());

        return p;
    }

    /**
     * Return the tangent plane to this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param st The parametric position to calculate the tangent plane for. Must be a
     *           2-dimensional point with each value in the range 0 to 1.0. Units are
     *           ignored.
     * @return The tangent plane of the surface at the specified parametric position.
     */
    @Override
    public Plane getTangentPlane(GeomPoint st) {
        validateParameter(st);
        return getTangentPlane(st.getValue(0), st.getValue(1));
    }

    /**
     * Returns the Gaussian Curvature for this surface at the given parametric position
     * (s,t) on this surface.
     *
     * @param s 1st parametric dimension distance to calculate curvature for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate curvature for (0.0 to 1.0
     *          inclusive).
     * @return The Gaussian curvature of the surface at the specified parametric position
     *         in units of 1/Length^2.
     */
    @Override
    public Parameter getGaussianCurvature(double s, double t) {
        validateParameter(s, t);
        s = roundParNearEnds(s);
        t = roundParNearEnds(t);

        //  Ref. 1, Eqn. 12.71.
        StackContext.enter();
        try {
            //  Calculate the fundamental form coefficients.
            FastTable<Parameter<Length>> coefs = getFundamentalFormCoeffs(s, t);
            Parameter<Length> E = coefs.get(0);
            Parameter<Length> F = coefs.get(1);
            Parameter<Length> G = coefs.get(2);
            Parameter<Length> L = coefs.get(3);
            Parameter<Length> M = coefs.get(4);
            Parameter<Length> N = coefs.get(5);

            //  Calculate the Gaussian curvature.
            Parameter num = L.times(N).minus(M.times(M));
            Parameter denom = E.times(G).minus(F.times(F));
            Parameter K = num.divide(denom);

            return StackContext.outerCopy(K);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the Gaussian Curvature for this surface at the given parametric position
     * (s,t) on this surface.
     *
     * @param st The parametric position to calculate the curvature for. Must be a
     *           2-dimensional point with each value in the range 0 to 1.0. Units are
     *           ignored.
     * @return The Gaussian curvature of the surface at the specified parametric position
     *         in units of 1/Length^2.
     */
    @Override
    public Parameter getGaussianCurvature(GeomPoint st) {
        validateParameter(st);
        return getGaussianCurvature(st.getValue(0), st.getValue(1));
    }

    /**
     * Returns the Mean Curvature for this surface at the given parametric position (s,t)
     * on this surface.
     *
     * @param st The parametric position to calculate the curvature for. Must be a
     *           2-dimensional point with each value in the range 0 to 1.0. Units are
     *           ignored.
     * @return The Mean curvature of the surface at the specified parametric position in
     *         units of 1/Length.
     */
    @Override
    public Parameter getMeanCurvature(GeomPoint st) {
        validateParameter(st);
        return getMeanCurvature(st.getValue(0), st.getValue(1));
    }

    /**
     * Returns the Mean Curvature for this surface at the given parametric position (s,t)
     * on this surface.
     *
     * @param s 1st parametric dimension distance to calculate curvature for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate curvature for (0.0 to 1.0
     *          inclusive).
     * @return The Mean curvature of the surface at the specified parametric position in
     *         units of 1/Length.
     */
    @Override
    public Parameter getMeanCurvature(double s, double t) {
        validateParameter(s, t);
        s = roundParNearEnds(s);
        t = roundParNearEnds(t);

        //  Ref. 1, Eqn. 12.72.
        StackContext.enter();
        try {
            //  Calculate the fundamental form coefficients.
            FastTable<Parameter<Length>> coefs = getFundamentalFormCoeffs(s, t);
            Parameter<Length> E = coefs.get(0);
            Parameter<Length> F = coefs.get(1);
            Parameter<Length> G = coefs.get(2);
            Parameter<Length> L = coefs.get(3);
            Parameter<Length> M = coefs.get(4);
            Parameter<Length> N = coefs.get(5);

            //  Calculate the Mean Curvature.
            Parameter num = E.times(N).plus(G.times(L)).minus(F.times(M).times(2));
            Parameter denom = E.times(G).minus(F.times(F)).times(2);
            Parameter H = num.divide(denom);

            return StackContext.outerCopy(H);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the coefficients of the 1st and 2nd fundamental forms. The list contains,
     * in this order: E, F, G, L, M, N.
     */
    private FastTable<Parameter<Length>> getFundamentalFormCoeffs(double s, double t) {
        //  Ref. 1, Eqn. 12.51-53 & 12.58-60.

        FastTable<Parameter<Length>> list = FastTable.newInstance();

        //  Get the derivatives for the surface at this parametric position.
        List<Vector<Length>> ders = getSDerivatives(s, t, 2, true);
        Vector pu = ders.get(1);
        Vector puu = ders.get(2);

        Vector puw = getTwistVector(s, t);

        ders = getTDerivatives(s, t, 2, true);
        Vector pw = ders.get(1);
        Vector pww = ders.get(2);

        //  E
        list.add(pu.dot(pu));

        //  F
        list.add(pu.dot(pw));

        //  G
        list.add(pw.dot(pw));

        //  Construct the normal vector.
        Vector n = pu.cross(pw).toUnitVector();

        //  L
        list.add(puu.dot(n));

        //  M
        list.add(puw.dot(n));

        //  N
        list.add(pww.dot(n));

        return list;
    }

    /**
     * Return the surface area of this entire surface.
     *
     * @param eps The desired fractional accuracy on the surface area.
     * @return the surface area of this surface.
     */
    @Override
    public Parameter<Area> getArea(double eps) {
        return getArea(0, 0, 1, 1, eps);
    }

    /**
     * Return the surface area of a portion of this surface.
     *
     * @param st1 The starting parametric position to calculate the area for. Must be a
     *            2-dimensional point with each value in the range 0 to 1.0. Units are
     *            ignored.
     * @param st2 The ending parametric position to calculate the area for. Must be a
     *            2-dimensional point with each value in the range 0 to 1.0. Units are
     *            ignored.
     * @param eps The desired fractional accuracy on the surface area.
     * @return the surface area of a portion of this surface.
     */
    @Override
    public Parameter<Area> getArea(GeomPoint st1, GeomPoint st2, double eps) {
        validateParameter(st1);
        validateParameter(st2);
        return getArea(st1.getValue(0), st1.getValue(1), st2.getValue(0), st2.getValue(1), eps);
    }

    /**
     * Return the surface area of a portion of this surface.
     *
     * @param s1  The starting 1st parametric dimension distance to calculate area for
     *            (0.0 to 1.0 inclusive).
     * @param t1  The starting 2nd parametric dimension distance to calculate area for
     *            (0.0 to 1.0 inclusive).
     * @param s2  The ending 1st parametric dimension distance to calculate area for (0.0
     *            to 1.0 inclusive).
     * @param t2  The ending 2nd parametric dimension distance to calculate area for (0.0
     *            to 1.0 inclusive).
     * @param eps The desired fractional accuracy on the surface area.
     * @return the surface area of a portion of this surface.
     */
    @Override
    public Parameter<Area> getArea(double s1, double t1, double s2, double t2, double eps) {
        validateParameter(s1, t1);
        validateParameter(s2, t2);

        //  Ref. 1, Eqn. 12.79.
        //  If either s1 and s2 are equal or t1 and t2 are equal, then we know the answer already.
        Unit<Area> unit = getUnit().times(getUnit()).asType(Area.class);
        if (isApproxEqual(s1, s2, TOL_ST) || isApproxEqual(t1, t2, TOL_ST))
            return Parameter.ZERO_LENGTH.to(unit);

        //  Make sure that s2 is > s1.
        if (s1 > s2) {
            double temp = s1;
            s1 = s2;
            s2 = temp;
        }

        //  Make sure that t2 is > t1.
        if (t1 > t2) {
            double temp = t1;
            t1 = t2;
            t2 = temp;
        }

        //  Use a concurrent context to evaluate four quarters of the surface at once.
        double sm = (s1 + s2) / 2;
        double tm = (t1 + t2) / 2;
        AreaEvaluatable areaEv1 = AreaEvaluatable.newInstance(this, s1, sm, t1, tm, eps);
        AreaEvaluatable areaEv2 = AreaEvaluatable.newInstance(this, s1, sm, tm, t2, eps);
        AreaEvaluatable areaEv3 = AreaEvaluatable.newInstance(this, sm, s2, t1, tm, eps);
        AreaEvaluatable areaEv4 = AreaEvaluatable.newInstance(this, sm, s2, tm, t2, eps);
        ConcurrentContext.enter();
        double area;
        try {
            //  Integrate to find the area: A = int_t1^t2{ int_s1^s2{ |pu x pw| ds } dt }
            ConcurrentContext.execute(areaEv1);
            ConcurrentContext.execute(areaEv2);
            ConcurrentContext.execute(areaEv3);
            ConcurrentContext.execute(areaEv4);

        } finally {
            //System.out.println("area = " + A + ", numEvals = " + areaEvaluator.numEvaluations);
            ConcurrentContext.exit(); // Waits for all concurrent threads to complete.
            area = areaEv1.getValue() + areaEv2.getValue() + areaEv3.getValue() + areaEv4.getValue();

            AreaEvaluatable.recycle(areaEv1);
            AreaEvaluatable.recycle(areaEv2);
            AreaEvaluatable.recycle(areaEv3);
            AreaEvaluatable.recycle(areaEv4);
        }

        Parameter<Area> A = Parameter.valueOf(area, unit);
        return A;
    }

    /**
     * Return the enclosed volume of this entire surface.
     *
     * @param eps The desired fractional accuracy on the enclosed volume.
     * @return the enclosed volume of this surface.
     */
    @Override
    public Parameter<Volume> getVolume(double eps) {
        return getVolume(0, 0, 1, 1, eps);
    }

    /**
     * Return the enclosed volume of a portion of this surface.
     *
     * @param st1 The starting parametric position to calculate the volume for. Must be a
     *            2-dimensional point with each value in the range 0 to 1.0. Units are
     *            ignored.
     * @param st2 The ending parametric position to calculate the volume for. Must be a
     *            2-dimensional point with each value in the range 0 to 1.0. Units are
     *            ignored.
     * @param eps The desired fractional accuracy on the enclosed volume.
     * @return the enclosed volume of a portion of this surface.
     */
    @Override
    public Parameter<Volume> getVolume(GeomPoint st1, GeomPoint st2, double eps) {
        validateParameter(st1);
        validateParameter(st2);
        return getVolume(st1.getValue(0), st1.getValue(1), st2.getValue(0), st2.getValue(1), eps);
    }

    /**
     * Return the enclosed volume of a portion of this surface.
     *
     * @param s1  The starting 1st parametric dimension distance to calculate volume for
     *            (0.0 to 1.0 inclusive).
     * @param t1  The starting 2nd parametric dimension distance to calculate volume for
     *            (0.0 to 1.0 inclusive).
     * @param s2  The ending 1st parametric dimension distance to calculate volume for
     *            (0.0 to 1.0 inclusive).
     * @param t2  The ending 2nd parametric dimension distance to calculate volume for
     *            (0.0 to 1.0 inclusive).
     * @param eps The desired fractional accuracy on the enclosed volume.
     * @return the enclosed volume of a portion of this surface.
     */
    @Override
    public Parameter<Volume> getVolume(double s1, double t1, double s2, double t2, double eps) {
        validateParameter(s1, t1);
        validateParameter(s2, t2);

        //  Ref. 1, Eqn. 12.82.
        //  If either s1 and s2 are equal or t1 and t2 are equal, then we know the answer already.
        Unit<Volume> unit = getUnit().pow(3).asType(Volume.class);
        if (isApproxEqual(s1, s2, TOL_ST) || isApproxEqual(t1, t2, TOL_ST))
            return Parameter.ZERO_VOLUME.to(unit);

        //  Make sure that s2 is > s1.
        if (s1 > s2) {
            double temp = s1;
            s1 = s2;
            s2 = temp;
        }

        //  Make sure that t2 is > t1.
        if (t1 > t2) {
            double temp = t1;
            t1 = t2;
            t2 = temp;
        }

        //  Use a concurrent context to evaluate four quarters of the surface at once.
        double sm = (s1 + s2) / 2;
        double tm = (t1 + t2) / 2;
        VolumeEvaluatable volEv1 = VolumeEvaluatable.newInstance(this, s1, sm, t1, tm, eps);
        VolumeEvaluatable volEv2 = VolumeEvaluatable.newInstance(this, s1, sm, tm, t2, eps);
        VolumeEvaluatable volEv3 = VolumeEvaluatable.newInstance(this, sm, s2, t1, tm, eps);
        VolumeEvaluatable volEv4 = VolumeEvaluatable.newInstance(this, sm, s2, tm, t2, eps);
        ConcurrentContext.enter();
        double volume;
        try {
            //  Integrate to find the volume:   V = int_t1^t2{ int_s1^s2{ 1/3*(p(s,t) DOT n(s,t) ds } dt }
            ConcurrentContext.execute(volEv1);
            ConcurrentContext.execute(volEv2);
            ConcurrentContext.execute(volEv3);
            ConcurrentContext.execute(volEv4);

        } finally {
            //System.out.println("volume = " + V + ", numEvals = " + volumeEvaluator.numEvaluations);
            ConcurrentContext.exit(); // Waits for all concurrent threads to complete.
            volume = volEv1.getValue() + volEv2.getValue() + volEv3.getValue() + volEv4.getValue();

            VolumeEvaluatable.recycle(volEv1);
            VolumeEvaluatable.recycle(volEv2);
            VolumeEvaluatable.recycle(volEv3);
            VolumeEvaluatable.recycle(volEv4);
        }

        Parameter<Volume> V = Parameter.valueOf(volume, unit);
        return V;
    }

    /**
     * Return an array of SubrangePoint objects that are gridded onto the surface using
     * the specified spacings and gridding rule.  <code>gridRule</code> specifies whether
     * the subdivision spacing is applied with respect to arc-length in real space or
     * parameter space. If the spacing is arc-length, then the values are interpreted as
     * fractions of the arc length of the edge curves from 0 to 1.
     *
     * @param gridRule      Specifies whether the subdivision spacing is applied with
     *                      respect to arc-length in real space or parameter space.
     *                      May not be null.
     * @param bottomSpacing A list of values used to define the subdivision spacing along
     *                      the T=0 parametric boundary of the surface. May not be null.
     * @param topSpacing    An optional list of values used to define the subdivision
     *                      spacing along the T=1 parametric boundary of the surface. If
     *                      <code>null</code> is passed, the bottomSpacing is used for the
     *                      top. If topSpacing is provided, then it must have the same
     *                      number of values as the bottomSpacing.
     * @param leftSpacing   A list of values used to define the subdivision spacing along
     *                      the S=0 parametric boundary of the surface. May not be null.
     * @param rightSpacing  An optional list of values used to define the subdivision
     *                      spacing along the S=1 parametric boundary of the surface. If
     *                      <code>null</code> is passed, the leftSpacing is used for the
     *                      right. If rightSpacing is provided, then it must have the same
     *                      number of values as the leftSpacing.
     * @return An array of SubrangePoint objects gridded onto the surface at the specified
     *         parametric positions.
     */
    @Override
    public PointArray<SubrangePoint> extractGrid(GridRule gridRule, List<Double> bottomSpacing, List<Double> topSpacing,
            List<Double> leftSpacing, List<Double> rightSpacing) {
        requireNonNull(gridRule);
        requireNonNull(bottomSpacing);
        requireNonNull(leftSpacing);
        
        //  Check for valid inputs.
        if (isNull(topSpacing))
            topSpacing = bottomSpacing;
        else if (topSpacing.size() != bottomSpacing.size())
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("lstSameSizeErr"),"topSpacing","bottomSpacing"));

        if (isNull(rightSpacing))
            rightSpacing = leftSpacing;
        else if (rightSpacing.size() != leftSpacing.size())
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("lstSameSizeErr"),"rightSpacing","leftSpacing"));

        PointArray<SubrangePoint> array = null;

        switch (gridRule) {
            case PAR:
                //  Parametric spacing.
                array = parametricGrid(bottomSpacing, topSpacing, leftSpacing, rightSpacing);

                break;

            case ARC:
            //  Arc-length spacing.

                //  Convert the ARC length spacing into parametric spacing along the T=0 & 1 boundary.
                FastTable<Double> parBottomSpacing = convertSubrangeCurveARCSpacing(bottomSpacing, getT0Curve());
                FastTable<Double> parTopSpacing = parBottomSpacing;
                if (topSpacing != bottomSpacing)
                    parTopSpacing = convertSubrangeCurveARCSpacing(topSpacing, getT1Curve());

                //  Convert the ARC length spacing into parametric spacing along the S=0 & 1 boundary.
                FastTable<Double> parLeftSpacing = convertSubrangeCurveARCSpacing(leftSpacing, getS0Curve());
                FastTable<Double> parRightSpacing = parLeftSpacing;
                if (rightSpacing != leftSpacing)
                    parRightSpacing = convertSubrangeCurveARCSpacing(rightSpacing, getS1Curve());

                //  Now grid the surface in parametric space.
                array = parametricGrid(parBottomSpacing, parTopSpacing, parLeftSpacing, parRightSpacing);

                break;

            default:
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("srfUnsupportedGridRule"),
                                gridRule.toString()));
        }

        return array;
    }

    /**
     * Convert a list of arc-length spacing values along a curve into a set of parametric
     * spacings along the curve.
     *
     * @param arcSpacing A list of arc-length spacings along the curve from 0 to 1. The
     *                   contents are replaced on exit by the parametric spacing along the
     *                   curve corresponding to the input arc-length spacing.
     * @param crv        The curve to calculate the parametric spacing along.
     * @return A list of parameter spacings along the curve from 0 to 1.
     */
    private static FastTable<Double> convertSubrangeCurveARCSpacing(List<Double> arcSpacing, Curve crv) {
        FastTable<Double> parSpacing = FastTable.newInstance();

        PointString<SubrangePoint> str = crv.extractGrid(GridRule.ARC, arcSpacing);
        int size = str.size();
        for (int i = 0; i < size; ++i) {
            SubrangePoint p = str.get(i);
            parSpacing.add(p.getParPosition().getValue(0));
            SubrangePoint.recycle(p);
        }
        PointString.recycle(str);

        return parSpacing;
    }

    /**
     * Extract a grid on this surface using a parametric spacing.
     */
    private PointArray<SubrangePoint> parametricGrid(List<Double> bottomSpacing, List<Double> topSpacing,
            List<Double> leftSpacing, List<Double> rightSpacing) {
        PointArray<SubrangePoint> array = PointArray.newInstance();

        //  Interpolate the spacing in the s and t directions using the
        //  indexes along the edges.
        int tSize = leftSpacing.size();
        int sSize = bottomSpacing.size();
        for (int tIdx = 0; tIdx < tSize; ++tIdx) {
            double left = leftSpacing.get(tIdx);
            double right = rightSpacing.get(tIdx);

            PointString<SubrangePoint> str = PointString.newInstance();
            for (int sIdx = 0; sIdx < sSize; ++sIdx) {
                double s = bottomSpacing.get(sIdx);
                if (bottomSpacing != topSpacing)
                    s = interpSpacing(s, topSpacing.get(sIdx), tSize, tIdx);
                double t = left;
                if (leftSpacing != rightSpacing)
                    t = interpSpacing(left, right, sSize, sIdx);

                str.add(getPoint(s, t));
            }
            array.add(str);
        }

        return array;
    }

    /**
     * Linearly interpolate the spacing between one side of the surface and the other
     * based on the index which is incrementing from spacing1 to spacing2.
     */
    private static double interpSpacing(double spacing1, double spacing2, int size, int idx) {
        double f = ((double)idx) / (size - 1);
        double s = (1 - f) * spacing1 + f * spacing2;
        return s;
    }

    /**
     * Return an array of points that are gridded onto the surface with the number of
     * points and spacing chosen to result in straight line segments between the points
     * that have mid-points that are all within the specified tolerance of this surface.
     *
     * @param tol The maximum distance that a straight line between gridded points may
     *            deviate from this surface. May not be null or zero.
     * @return An array of subrange points gridded onto the surface.
     */
    @Override
    public PointArray<SubrangePoint> gridToTolerance(Parameter<Length> tol) {
        Parameter<Area> tol2 = tol.times(tol);
        
        //  Start with just the corner points.
        List<Double> spacing = GridSpacing.linear(2);
        PointArray<SubrangePoint> pntArray = extractGrid(GridRule.PAR, spacing, null, spacing, null);
        FastTable.recycle((FastTable)spacing);
        
        //  Refine in the s-direction (down the strings).
        subdivideSDir(tol2, pntArray);
        
        //  Now, refine in the t-direction (across the strings).
        subdivideTDir(tol2, pntArray);
        
        //  Now guess a set of points to use based on the surface topography.
        PointArray<SubrangePoint> guessedArray = guessGrid2TolPoints(requireNonNull(tol));
        
        //  Check guessed grid-to-tolerance points and add any that fall out of tolerance.
        checkGuessedGrid2TolPoints(pntArray, guessedArray, tol2);
        PointArray.recycle(guessedArray);
        
        //  Re-refine in the s-direction (down the strings).
        subdivideSDir(tol2, pntArray);
        
        //  Finally, re-refine in the t-direction (across the strings).
        subdivideTDir(tol2, pntArray);
        
        return pntArray;
    }

    /**
     * Guess an initial set of points to use when gridding to tolerance. The initial
     * points must include each corner of the surface as well as any discontinuities in
     * the surface. Other than that, they should be distributed as best as possible to
     * indicate areas of higher curvature, but should be generated as efficiently as
     * possible.
     * <p>
     * The default implementation places a 10x10 grid of points equally spaced in
     * parameter space onto the surface. Sub-classes should override this method if they
     * can provide a more efficient/robust method to locate points where there may be
     * curvature discontinuities or areas of high curvature.
     * </p>
     *
     * @param tol The maximum distance that a straight line between gridded points may
     *            deviate from this surface. May not be null or zero.
     * @return An array of subrange points to use as a starting point for the grid to
     *         tolerance algorithms.
     */
    protected PointArray<SubrangePoint> guessGrid2TolPoints(Parameter<Length> tol) {

        //  Create a square grid of subrange points on the surface.
        List<Double> spacing = GridSpacing.linear(10);
        PointArray<SubrangePoint> pntArray = extractGrid(GridRule.PAR, spacing, null, spacing, null);
        FastTable.recycle((FastTable)spacing);

        return pntArray;
    }

    private static final int MAX_POINTS = 10000;

    /**
     * Subdivide the surface in the parametric s-direction starting with an initial guess
     * at the grid of points to be rendered. New points are added to each of the strings
     * in the pntArray object as needed.
     */
    private PointArray<SubrangePoint> subdivideSDir(Parameter<Area> tol2, PointArray<SubrangePoint> pntArray) {
        int nTSeg = pntArray.size();
        int nSSeg = pntArray.get(0).size();
        int numPnts = nTSeg * nSSeg;
        if (numPnts > MAX_POINTS) {
            Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                    MessageFormat.format(RESOURCES.getString("maxPointsWarningMsg"),
                            "surface", this.getID()));
            return pntArray;
        }
        GeomPoint[] pntArr = new GeomPoint[2];

        //  Loop over all the rows/strings in the array.
        outer:
        for (int i = 0; i < nTSeg; ++i) {
            PointString<SubrangePoint> curStr = pntArray.get(i);

            //  Loop over all the columns/points in each string.
            int jp1 = 1;
            for (int j = 0; j < curStr.size() - 1; ++j) {

                //  Subdivide the current segment until the mid-point is less than tol away from the surface.
                boolean distGTtol = true;
                while (distGTtol) {

                    //  Get the current point.
                    SubrangePoint pc = curStr.get(j);
                    GeomPoint stc = pc.getParPosition();

                    //  Get the next point.
                    SubrangePoint pn = curStr.get(jp1);
                    GeomPoint stn = pn.getParPosition();

                    //  Compute the average point half-way on a line from pc to pn.
                    pntArr[0] = pc; pntArr[1] = pn;
                    Point pa = GeomUtil.averagePoints(pntArr);

                    //  Find the middle point on the surface.
                    pntArr[0] = stc;    pntArr[1] = stn;
                    Point stm = GeomUtil.averagePoints(pntArr);     //  Find the average parametric position.
                    SubrangePoint pm = getPoint(stm);

                    //  Compute the distance between the middle point and the average point
                    //  and compare with the tolerance.
                    distGTtol = pa.distanceSq(pm).isGreaterThan(tol2);
                    if (distGTtol) {
                        //  Insert a new column of points into each string in the array.
                        insertInterpolatedColumn(pntArray, j, stm.getValue(0));
                        numPnts += nTSeg;
                        if (numPnts > MAX_POINTS) {
                            Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                    MessageFormat.format(RESOURCES.getString("maxPointsWarningMsg"),
                                            "surface", this.getID()));
                            break outer;
                        }

                    }
                    SubrangePoint.recycle(pm);

                }   //  end while()

                ++jp1;
            }   //  Next j
        }   //  Next i

        return pntArray;
    }

    /**
     * Subdivide the surface in the parametric t-direction starting with an initial guess
     * at the grid of points to be rendered. New columns/strings are added to the pntArray
     * object as needed.
     */
    private PointArray<SubrangePoint> subdivideTDir(Parameter<Area> tol2, PointArray<SubrangePoint> pntArray) {
        int nTSeg = pntArray.size();
        int nSSeg = pntArray.get(0).size();
        int numPnts = nTSeg * nSSeg;
        if (numPnts > MAX_POINTS) {
            Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                    MessageFormat.format(RESOURCES.getString("maxPointsWarningMsg"),
                            "surface", this.getID()));
            return pntArray;
        }
        GeomPoint[] pntArr = new GeomPoint[2];

        //  Loop over all the columns/points in each string of the array.
        outer:
        for (int j = 0; j < nSSeg; ++j) {

            //  Loop over all the rows/strings in the array.
            int ip1 = 1;
            for (int i = 0; i < nTSeg - 1; ++i) {

                //  Subdivide the current segment until the mid-point is less than tol away from the surface.
                boolean distGTtol = true;
                while (distGTtol) {

                    //  Get the current point.
                    SubrangePoint pc = pntArray.get(i).get(j);
                    GeomPoint stc = pc.getParPosition();

                    //  Get the next point.
                    SubrangePoint pn = pntArray.get(ip1).get(j);
                    GeomPoint stn = pn.getParPosition();

                    //  Compute the average point half-way on a line from pc to pn.
                    pntArr[0] = pc; pntArr[1] = pn;
                    Point pa = GeomUtil.averagePoints(pntArr);

                    //  Find the middle point on the surface.
                    pntArr[0] = stc;    pntArr[1] = stn;
                    Point stm = GeomUtil.averagePoints(pntArr);     //  Find the average parametric position.
                    SubrangePoint pm = getPoint(stm);

                    //  Compute the distance between the middle point and the average point
                    //  and compare with the tolerance.
                    distGTtol = pa.distanceSq(pm).isGreaterThan(tol2);
                    if (distGTtol) {
                        //  Insert a new row/string of points into the array.
                        insertInterpolatedRow(pntArray, i, stm.getValue(1));
                        ++nTSeg;
                        numPnts += nSSeg;
                        if (numPnts > MAX_POINTS) {
                            Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                    MessageFormat.format(RESOURCES.getString("maxPointsWarningMsg"),
                                            "surface", this.getID()));
                            break outer;
                        }

                    }
                    SubrangePoint.recycle(pm);

                }   //  end while()

                ++ip1;
            }   //  Next i
        }   //  Next j

        return pntArray;
    }

    /**
     * Insert into the input pntArray a new row of points (new PointString) between rows
     * "i" and "i+1" at parametric position "t".
     *
     * @param pntArray The point array to add the new row of points (string of points) to.
     * @param i        The index on the low side of where the points should be inserted.
     * @param t        The parametric position where the points should be inserted. Must
     *                 be > the parametric "t" position of pntArray.get(i) and &lt; the
     *                 "t" position of pntArray.get(i+1).
     */
    private void insertInterpolatedRow(PointArray<SubrangePoint> pntArray, int i, double t) {

        //  Get the string of points on near where the new row will be inserted (used for "s" positions).
        PointString<SubrangePoint> row1 = pntArray.get(i);

        //  Create an empty string to fill in with interpolated points.
        PointString<SubrangePoint> stra = PointString.newInstance();

        //  Create the interpolated points along a string (row).
        int numPnts = row1.size();
        for (int j = 0; j < numPnts; ++j) {
            SubrangePoint p1 = row1.get(j);
            double s = p1.getParPosition().getValue(0);
            stra.add(getPoint(s, t));
        }

        //  Insert the interpolated row into the input array of points.
        pntArray.add(i+1, stra);
    }

    /**
     * Insert into the input pntArray a new column of points (across all the strings)
     * between columns "j" and "j+1" at parametric position "s".
     *
     * @param pntArray The point array to add the new column of points (across the
     *                 strings) to.
     * @param j        The index on the low side of where the points should be inserted.
     * @param s        The parametric position where the points should be inserted. Must
     *                 be > the parametric "s" position of pntArray.getColumn(j) and &lt;
     *                 the "s" position of pntArray.getColumn(j+1).
     */
    private void insertInterpolatedColumn(PointArray<SubrangePoint> pntArray, int j, double s) {
        
        //  Get the column of points on either side of where the column should be added.
        PointString<SubrangePoint> col1 = pntArray.getColumn(j);
        
       //   Create and insert the interpolated points across the strings (along a column).
       int numPnts = col1.size();
       int jp1 = j + 1;
       for (int i = 0; i < numPnts; ++i) {
           SubrangePoint p1 = col1.get(i);
           double t = p1.getParPosition().getValue(1);
           SubrangePoint pm = getPoint(s, t);
           pntArray.get(i).add(jp1, pm);
       }
       
       PointString.recycle(col1);
    }
    
    /**
     * A Comparator that compares the parametric S-position of a pair of subrange points.
     */
    private static class SComparator implements Comparator<SubrangePoint> {
        @Override
        public int compare(SubrangePoint o1, SubrangePoint o2) {
            double s1 = o1.getParPosition().getValue(0);
            double s2 = o2.getParPosition().getValue(0);
            return Double.compare(s1, s2);
        }
    }
    
    /**
     * A Comparator that compares the parametric T-position of a pair of subrange points.
     */
    private static class TComparator implements Comparator<SubrangePoint> {
        @Override
        public int compare(SubrangePoint o1, SubrangePoint o2) {
            double t1 = o1.getParPosition().getValue(1);
            double t2 = o2.getParPosition().getValue(1);
            return Double.compare(t1, t2);
        }
    }

    private static final SComparator S_CMPRTR = new SComparator();
    private static final TComparator T_CMPRTR = new TComparator();

    /**
     * Check guessed grid-to-tolerance points and add them to pntArray if they
     * are greater than the tolerance distance away from straight line segments
     * between the points already in pntArray.
     * 
     * @param pntArray The existing array of gridded points on this surface. Any
     * points from guessPnts that are more than tol away from straight line
     * segments between these points are added to this array.
     * @param guessPnts The existing array of segment parameter derived guessed points.
     * @param tol2 The square of the maximum distance that a straight line between
     *            gridded points may deviate from this surface. May not be null.
     */
    private void checkGuessedGrid2TolPoints(PointArray<SubrangePoint> pntArray, 
            PointArray<SubrangePoint> guessPnts, Parameter<Area> tol2) {
        
        PointString<SubrangePoint> pntArrayT0 = pntArray.getColumn(0);
        int nSSegs = guessPnts.get(0).size() - 1;
        int nTSegs = guessPnts.size() - 1;
        for (int j=1; j < nTSegs; ++j) {
            //  All the points in this row should have the same T position.
            PointString<SubrangePoint> guessPntsList = guessPnts.get(j);
            SubrangePoint pt = guessPntsList.get(0);
            
            //  Find where this point should fall in the gridded list of points in the T-direction.
            int idxj = Collections.binarySearch(pntArrayT0, pt, T_CMPRTR);
            if (idxj < 0)
                idxj = -idxj - 1;
            
            for (int i=1; i < nSSegs; ++i) {
                SubrangePoint ps = guessPntsList.get(i);
                double s = ps.getParPosition().getValue(0);

                //  Find where this point should fall in the gridded list of points in the S-direction.
                int idx = Collections.binarySearch(pntArray.get(0), ps, S_CMPRTR);
                if (idx < 0) {
                    //  ps is not already in the pntList, so see if it is in tolerance or not.
                    idx = -idx - 1;

                    //  Calculate a point, pa, along the line between p(i) and p(i-1)
                    //  at the position of "s".
                    SubrangePoint pi = pntArray.get(idxj,idx);
                    SubrangePoint pim1 = pntArray.get(idxj,idx-1);
                    Point pa = interpSPoint(pim1,pi,s);

                    //  Compute the distance between the point at "s" and the line seg point
                    //  and compare with the tolerance.
                    boolean distGTtol = pa.distanceSq(ps).isGreaterThan(tol2);
                    if (distGTtol) {
                        //  Insert the new column of points into each of the rows (point strings).
                        insertInterpolatedColumn(pntArray, idx-1, s);
                        if (pntArray.size()*pntArray.get(0).size() >= MAX_POINTS)
                            break;
                    }
                }
                SubrangePoint.recycle(ps);
            }
        }
        PointString.recycle(pntArrayT0);
        
    }
    
    /**
     * Interpolate a point at "s" between the points p1 and p2 (that must have
     * parametric positions surrounding "s").
     *
     * @param p1 Point with a parametric position less than "s".
     * @param p2 Point with a parametric position greater than "s".
     * @param s The parametric position at which a point is to be interpolated
     * between p1 and p2.
     * @return The interpolated point.
     */
    private static Point interpSPoint(SubrangePoint p1, SubrangePoint p2, double s) {
        StackContext.enter();
        try {
            double s1 = p1.getParPosition().getValue(0);
            double s2 = p2.getParPosition().getValue(0);
            Point Ds = p2.minus(p1);
            Point pout = p1.plus(Ds.times((s - s1) / (s2 - s1)));
            return StackContext.outerCopy(pout);
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Returns the closest point on this surface to the specified point.
     *
     * @param point The point to find the closest point on this surface to. May not be null.
     * @param tol   Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is closest to the specified
     *         point.
     */
    @Override
    public SubrangePoint getClosest(GeomPoint point, double tol) {
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Guess a location near enough to the closest point for the root solver
            //  to get us the rest of the way there.
            GeomPoint parNear = guessClosestFarthest(requireNonNull(point), true);

            SubrangePoint p = getClosestFarthest(point, parNear.getValue(0), parNear.getValue(1), tol, true);
            sOut = p.getParPosition().getValue(0);
            tOut = p.getParPosition().getValue(1);
            
        } finally {
            StackContext.exit();
        }
        return getPoint(sOut, tOut);
    }

    /**
     * Returns the closest point on this surface to the specified point near the specified
     * parametric position.
     *
     * @param point The point to find the closest point on this surface to. May not be null.
     * @param nearS The parametric s-position where the search for the closest point
     *              should begin.
     * @param nearT The parametric t-position where the search for the closest point
     *              should begin.
     * @param tol   Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is closest to the specified
     *         point near the specified parametric position.
     */
    @Override
    public SubrangePoint getClosest(GeomPoint point, double nearS, double nearT, double tol) {
        SubrangePoint p = getClosestFarthest(requireNonNull(point), nearS, nearT, tol, true);
        return p;
    }

    /**
     * Returns the array of closest points on this surface to the specified array (list of
     * lists) of points.
     *
     * @param points A list of lists of points to find the closest point on this surface
     *               to. May not be null.
     * @param tol    Fractional tolerance to refine the distance to.
     * @return The {@link PointArray} of {@link SubrangePoint} on this surface that is
     *         closest to the specified list of lists of points.
     */
    @Override
    public PointArray<SubrangePoint> getClosest(List<? extends List<GeomPoint>> points, double tol) {
        PointArray arr = getClosestFarthest(requireNonNull(points), tol, true);
        return arr;
    }

    /**
     * Returns the farthest point on this surface from the specified point.
     *
     * @param point The point to find the farthest point on this surface from. May not be
     *              null.
     * @param tol   Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is farthest from the
     *         specified point.
     */
    @Override
    public SubrangePoint getFarthest(GeomPoint point, double tol) {
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Guess a location near enough to the farthest point for the root solver
            //  to get us the rest of the way there.
            GeomPoint parNear = guessClosestFarthest(requireNonNull(point), false);

            SubrangePoint p = getClosestFarthest(point, parNear.getValue(0), parNear.getValue(1), tol, false);
            sOut = p.getParPosition().getValue(0);
            tOut = p.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }

        return getPoint(sOut, tOut);
    }

    /**
     * Returns the farthest point on this surface from the specified point near the
     * specified parametric position on the surface.
     *
     * @param point The point to find the farthest point on this surface from. May not be
     *              null.
     * @param nearS The parametric s-position where the search for the closest point
     *              should begin.
     * @param nearT The parametric t-position where the search for the closest point
     *              should begin.
     * @param tol   Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is farthest from the
     *         specified point.
     */
    @Override
    public SubrangePoint getFarthest(GeomPoint point, double nearS, double nearT, double tol) {
        SubrangePoint p = getClosestFarthest(requireNonNull(point), nearS, nearT, tol, false);
        return p;
    }

    /**
     * Returns the array of farthest points on this surface from the specified array (list
     * of lists) of points.
     *
     * @param points A list of lists of points to find the farthest point on this surface
     *               from. May not be null.
     * @param tol    Fractional tolerance to refine the distance to.
     * @return The {@link PointArray} of {@link SubrangePoint} on this surface that is
     *         farthest from the specified list of lists of points.
     */
    @Override
    public PointArray<SubrangePoint> getFarthest(List<? extends List<GeomPoint>> points, double tol) {
        PointArray<SubrangePoint> arr = getClosestFarthest(requireNonNull(points), tol, false);
        return arr;
    }

    /**
     * Returns the array of closest/farthest points on this surface to the specified array
     * (list of lists) of points.
     *
     * @param points  A list of lists of points to find the closest/farthest point on this
     *                surface to. May not be null.
     * @param tol     Fractional tolerance to refine the distance to.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The {@link PointArray} of {@link SubrangePoint} on this surface that is
     *         closest/farthest to the specified list of lists of points.
     */
    private PointArray<SubrangePoint> getClosestFarthest(List<? extends List<GeomPoint>> points, double tol, boolean closest) {
        requireNonNull(points);
        PointArray output = PointArray.newInstance();
        boolean firstPass = true;
        SubrangePoint p_old = null;

        //  Loop over all the lists of points.
        int numDims = getPhyDimension();
        int numCols = points.size();
        int numRows = points.get(0).size();
        for (int col = 0; col < numCols; ++col) {
            //  Get this column/string of target points.
            List<GeomPoint> column = points.get(col);

            //  Create a string of output points on the surface.
            PointString<SubrangePoint> str = PointString.newInstance();

            //  Loop over all the points in the supplied string of points and find the closest to each.
            for (int row = 0; row < numRows; ++row) {
                //  Get the target point.
                GeomPoint q = column.get(row);

                //  Make sure the point and surface have the same dimensions.
                if (q.getPhyDimension() > numDims)
                    throw new IllegalArgumentException(MessageFormat.format(
                            RESOURCES.getString("sameOrFewerDimErr"), "points", "surface"));
                else if (q.getPhyDimension() < numDims)
                    q = q.toDimension(numDims);

                double nearS, nearT;
                if (firstPass) {
                    firstPass = false;

                    //  Guess a location near enough to the closest point for the root solver
                    //  to get us the rest of the way there.
                    GeomPoint parNear = guessClosestFarthest(q, closest);
                    nearS = parNear.getValue(0);
                    nearT = parNear.getValue(1);

                } else {
                    //  Assume that the new point will be near the last one.
                    @SuppressWarnings("null")
                    GeomPoint parNear = p_old.getParPosition();
                    nearS = parNear.getValue(0);
                    nearT = parNear.getValue(1);
                }

                //  Get the closest point to the target point.
                SubrangePoint p = getClosestFarthest(q, nearS, nearT, tol, closest);

                //  Add the point to the string of points.
                str.add(p);

                p_old = p;
            }

            //  Add the string of points to the array.
            output.add(str);

            firstPass = true;
        }

        return output;
    }

    /**
     * Returns the closest or farthest point on this surface to the specified point.
     *
     * @param point   The point to find the closest/farthest point on this surface to. May
     *                not be null.
     * @param nearS   The parametric s-position where the search for the closest/farthest
     *                point should begin.
     * @param nearT   The parametric t-position where the search for the closest/farthest
     *                point should begin.
     * @param tol     Fractional tolerance to refine the distance to.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The {@link SubrangePoint} on this surface that is closest/farthest to/from
     *         the specified point.
     */
    private SubrangePoint getClosestFarthest(GeomPoint point, double nearS, double nearT, double tol, boolean closest) {
        validateParameter(nearS, nearT);
        
        double sc, tc;
        StackContext.enter();
        try {
            //  Make sure the point and surface have the same dimensions.
            int numDims = getPhyDimension();
            if (point.getPhyDimension() > numDims)
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "point", "surface"));
            else if (point.getPhyDimension() < numDims)
                point = point.toDimension(numDims);

            //  Create a list of potential closest points and add the edge curve closest points to it.
            FastTable<SubrangePoint> potentials = FastTable.newInstance();
            potentials.add(SubrangePoint.newInstance(this, nearS, nearT));
            if (closest) {
                SubrangePoint p = getT0Curve().getClosest(point, nearS, tol);
                potentials.add(SubrangePoint.newInstance(this, p.getParPosition().getValue(0), 0));
                p = getT1Curve().getClosest(point, nearS, tol);
                potentials.add(SubrangePoint.newInstance(this, p.getParPosition().getValue(0), 1));
                p = getS0Curve().getClosest(point, nearT, tol);
                potentials.add(SubrangePoint.newInstance(this, 0, p.getParPosition().getValue(0)));
                p = getS1Curve().getClosest(point, nearT, tol);
                potentials.add(SubrangePoint.newInstance(this, 1, p.getParPosition().getValue(0)));

            } else {
                SubrangePoint p = getT0Curve().getFarthest(point, nearS, tol);
                potentials.add(SubrangePoint.newInstance(this, p.getParPosition().getValue(0), 0));
                p = getT1Curve().getFarthest(point, nearS, tol);
                potentials.add(SubrangePoint.newInstance(this, p.getParPosition().getValue(0), 1));
                p = getS0Curve().getFarthest(point, nearT, tol);
                potentials.add(SubrangePoint.newInstance(this, 0, p.getParPosition().getValue(0)));
                p = getS1Curve().getFarthest(point, nearT, tol);
                potentials.add(SubrangePoint.newInstance(this, 1, p.getParPosition().getValue(0)));
            }

            SubrangePoint output;
            try {
                //  Find the closest point interior to the surface near nearS and nearT.
                SubrangePoint spnt = getClosestFarthestInterior(point, nearS, nearT, tol, closest);

                //  Store the point from the minimizer.
                potentials.add(spnt);

            } catch (RootException err) {
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Failed to find closest/farthest interior point.", err);

            } finally {
                if (potentials.isEmpty()) {
                    output = getPoint(0, 0);

                } else {
                    //  Loop over the potentials and find the one that is the closest/farthest.
                    output = potentials.get(0);
                    double dOpt = point.distanceValue(output);
                    int size = potentials.size();
                    for (int i = 1; i < size; ++i) {
                        SubrangePoint p = potentials.get(i);
                        double dist = point.distanceValue(p);
                        if (closest) {
                            if (dist < dOpt) {
                                dOpt = dist;
                                output = p;
                            }
                        } else {
                            if (dist > dOpt) {
                                dOpt = dist;
                                output = p;
                            }
                        }
                    }

                }

                //  Store the closest point S & T parameters.
                sc = output.getParPosition().getValue(0);
                tc = output.getParPosition().getValue(1);
            }
        } finally {
            StackContext.exit();
        }

        return getPoint(sc,tc);
    }

    /**
     * Find and return the nearest point interior to the surface. This does not look at
     * edge curves and it assumes all the inputs are properly set up.
     *
     * @param point   The point to find the closest/farthest point on this surface to. May
     *                not be null.
     * @param nearS   The parametric s-position where the search for the closest/farthest
     *                point should begin.
     * @param nearT   The parametric t-position where the search for the closest/farthest
     *                point should begin.
     * @param tol     Fractional tolerance to refine the distance to.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The SubrangePoint on the interior of this surface that is closest/farthest
     *         to/from the specified point.
     * @throws RootException if there is a convergence problem with the root solver.
     */
    private SubrangePoint getClosestFarthestInterior(GeomPoint point, double nearS, double nearT,
            double tol, boolean closest) throws RootException {
        validateParameter(nearS, nearT);
        requireNonNull(point);
        
        double s, t;
        StackContext.enter();
        try {
            //  Create a function that calculates the distance from point to the surface.
            PointSurfaceDistFunction distFunc = PointSurfaceDistFunction.newInstance(this, point, closest);

            //  Use n-dimensional minimizer to locate closest point near "pos".
            double[] x = ArrayFactory.DOUBLES_FACTORY.array(2);
            x[0] = nearS;
            x[1] = nearT;
            Minimization.findND(distFunc, x, 2, tol);
            //System.out.println("numEvals = " + distFunc.numEvaluations);

            //  The minimizer may go slightly past the parametric bounds.  Deal with that.
            s = x[0];
            t = x[1];
            if (s < 0)
                s = 0;
            else if (s > 1)
                s = 1;
            if (t < 0)
                t = 0;
            else if (t > 1)
                t = 1;

        } finally {
            StackContext.exit();
        }

        return getPoint(s,t);
    }

    /**
     * Method that guesses the most likely location for the closest or farthest point on a
     * surface and returns that location as a 2D point containing the "s" and "t"
     * parameter values. This is called by getClosest() and getFarthest(). This is
     * required in order for the root-finding algorithm to reliably refine the closest
     * point to the correct location.
     * <p>
     * Subclasses may override this method to provide a method for estimating the closest
     * point location that is most efficient for that surface type. The default
     * implementation extracts a 9x9 grid of subrange points, finds the closest one and
     * returns it's parametric position.
     * </p>
     *
     * @param point   The point to find the closest point on this surface to. May not be null.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The 2D parametric point on this surface that is approx. closest to the
     *         specified point.
     */
    protected GeomPoint guessClosestFarthest(GeomPoint point, boolean closest) {
        //  Create a 9x9 grid of subrange points on the surface.
        //  Return the parametric position of the closest/farthest grid point.
        return getClosestFarthestOnGrid(this, requireNonNull(point), closest, 9).getParPosition();
    }

    /**
     * Extract a regularly spaced grid of parametric points on the surface and return the
     * grid point that is closest/farthest to/from the target point. This ignores the
     * boundary points as they are handled by extracting the boundary curves.
     *
     * @param srf      The surface to find the closest grid point on.
     * @param point    The point to find the closest point on this surface to.
     * @param closest  Set to <code>true</code> to return the closest point or
     *                 <code>false</code> to return the farthest point.
     * @param gridSize The number of points to space out on the surface in each parametric
     *                 direction.
     * @return The subrange grid point on this surface that is closest to the specified
     *         point.
     */
    protected static SubrangePoint getClosestFarthestOnGrid(Surface srf, GeomPoint point, boolean closest, int gridSize) {
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Create a square grid of subrange points on the surface.
            List<Double> spacing = GridSpacing.linear(gridSize);
            spacing.remove(0);
            spacing.remove(spacing.size() - 1);
            PointArray<SubrangePoint> potentials = srf.extractGrid(GridRule.PAR, spacing, null, spacing, null);

            //  Find the closest/farthest point in the grid.
            SubrangePoint pOpt = potentials.get(0, 0);
            double dOpt2 = point.distanceSqValue(pOpt);
            int cols = potentials.size();
            int rows = potentials.get(0).size();
            for (int i = 0; i < cols; ++i) {
                for (int j = 0; j < rows; ++j) {
                    SubrangePoint p = potentials.get(i, j);
                    double dist2 = point.distanceSqValue(p);
                    if (closest) {
                        if (dist2 < dOpt2) {
                            dOpt2 = dist2;
                            pOpt = p;
                        }
                    } else {
                        if (dist2 > dOpt2) {
                            dOpt2 = dist2;
                            pOpt = p;
                        }
                    }
                }
            }
            sOut = pOpt.getParPosition().getValue(0);
            tOut = pOpt.getParPosition().getValue(1);
            
        } finally {
            StackContext.exit();
        }
        
        //  Return the the closest/farthest point.
        return srf.getPoint(sOut, tOut);
    }

    /**
     * Returns the closest points (giving the minimum distance) between this surface and
     * the specified curve. If the specified curve intersects this surface, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If the specified curve is
     * parallel to this surface (all points are equidistant away), then any point between
     * this surface and the specified curve could be returned as the "closest".
     *
     * @param curve The curve to find the closest points between this surface and that
     *              curve on. May not be null.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return A list containing two SubrangePoint objects that represent the closest
     *         point on this surface (index 0) and the closest point on the specified
     *         curve (index 1).
     */
    @Override
    public PointString<SubrangePoint> getClosest(Curve curve, double tol) {

        //  Make sure that curve and this surface have the same dimensions.
        int numDims = getPhyDimension();
        if (curve.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "curve", "surface"));
        else if (curve.getPhyDimension() < numDims)
            curve = curve.toDimension(numDims);

        //  Use the curve's "getClosest" method to find the closest points between this surface
        //  and the target curve.
        PointString<SubrangePoint> str = curve.getClosest(this, tol);

        //  Reverse the string for output to match this method's contract.
        PointString<SubrangePoint> output = str.reverse();
        PointString.recycle(str);

        return output;
    }

    /**
     * Returns the closest points (giving the minimum distance) between this surface and
     * the specified surface. If the specified surface intersects this surface, then the
     * 1st intersection found is returned (not all the intersections are found and there
     * is no guarantee about which intersection will be returned). If the specified
     * surface is parallel to this surface (all points are equidistant away), then any
     * point between this surface and the specified surface could be returned as the
     * "closest".
     *
     * @param surface The surface to find the closest points between this surface and that
     *                surface on. May not be null.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                position to.
     * @return A list containing two SubrangePoint objects that represent the closest
     *         point on this surface (index 0) and the closest point on the specified
     *         surface (index 1).
     */
    @Override
    public PointString<SubrangePoint> getClosest(Surface surface, double tol) {

        //  Make sure that surface and this surface have the same dimensions.
        int numDims = getPhyDimension();
        if (surface.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "input surface", "surface"));
        else if (surface.getPhyDimension() < numDims)
            surface = surface.toDimension(numDims);

        //  Guess a location near enough to the closest point for the root solver
        //  to get us the rest of the way there.
        GeomPoint parNear = guessClosestFarthest(surface, true);

        //  Find the closest point between the two surfaces.
        PointString<SubrangePoint> p = getClosest(surface, parNear.getValue(0), parNear.getValue(1), tol);

        return p;

    }

    /**
     * Method that guesses the most likely location for the closest or farthest point on a
     * surface and returns that location as a 2D point containing the "s" and "t"
     * parameter values. This is called by getClosest() and getFarthest(). This is
     * required in order for the root-finding algorithm to reliably refine the closest
     * point to the correct location.
     * <p>
     * Subclasses may override this method to provide a method for estimating the closest
     * point location that is most efficient for that surface type. The default
     * implementation extracts a 9x9 grid of subrange points, finds the closest one and
     * returns it's parametric position.
     * </p>
     *
     * @param surface The surface to find the closest point on this surface to. May not be
     *                null.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The 2D parametric point on this surface that is approx. closest to the
     *         specified surface.
     */
    protected GeomPoint guessClosestFarthest(Surface surface, boolean closest) {
        //  Create a 9x9 grid of subrange points on the surface.
        //  Return the parametric position of the closest/farthest grid point.
        return getClosestFarthestOnGrid(this, requireNonNull(surface), closest, 9).getParPosition();
    }

    /**
     * Extract a regularly spaced grid of parametric points on the surface and return the
     * grid point that is closest/farthest to/from the target surface. This ignores the
     * boundary points as they are handled by extracting the boundary curves.
     *
     * @param thisSrf  The surface to find the closest grid point on.
     * @param thatSrf  The surface to find the closest point on this surface to.
     * @param closest  Set to <code>true</code> to return the closest point or
     *                 <code>false</code> to return the farthest point.
     * @param gridSize The number of points to space out on this surface in each
     *                 parametric direction.
     * @return The subrange grid point on this surface that is closest to the specified
     *         surface.
     */
    protected static SubrangePoint getClosestFarthestOnGrid(Surface thisSrf, Surface thatSrf, boolean closest, int gridSize) {
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Create a square grid of subrange points on the surface.
            List<Double> spacing = GridSpacing.linear(gridSize);
            spacing.remove(0);
            spacing.remove(spacing.size() - 1);
            PointArray<SubrangePoint> potentials = thisSrf.extractGrid(GridRule.PAR, spacing, null, spacing, null);

            //  Find the closest/farthest point in the grid.
            SubrangePoint pOpt = potentials.get(0, 0);
            SubrangePoint point = thatSrf.getClosest(pOpt, GTOL);
            double dOpt2 = point.distanceSqValue(pOpt);
            int cols = potentials.size();
            int rows = potentials.get(0).size();
            for (int i = 0; i < cols; ++i) {
                for (int j = 0; j < rows; ++j) {
                    SubrangePoint p = potentials.get(i, j);
                    point = thatSrf.getClosest(p, GTOL);
                    double dist2 = point.distanceSqValue(p);
                    if (closest) {
                        if (dist2 < dOpt2) {
                            dOpt2 = dist2;
                            pOpt = p;
                        }
                    } else {
                        if (dist2 > dOpt2) {
                            dOpt2 = dist2;
                            pOpt = p;
                        }
                    }
                }
            }
            sOut = pOpt.getParPosition().getValue(0);
            tOut = pOpt.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }

        //  Return the the closest/farthest point.
        return thisSrf.getPoint(sOut, tOut);
    }

    /**
     * Returns the closest point on this surface to the specified surface.
     *
     * @param thatSrf The surface to find the closest point on this surface to. May not be
     *                null.
     * @param nearS   The parametric s-position where the search for the closest point
     *                should begin.
     * @param nearT   The parametric t-position where the search for the closest point
     *                should begin.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                position to.
     * @return A list containing SubrangePoint objects representing the closest points on
     *         this surface (index 0) and the target surface (index 1).
     */
    private PointString<SubrangePoint> getClosest(Surface thatSrf, double nearS, double nearT, double tol) {
        double thisS, thisT, thatS, thatT;

        StackContext.enter();
        try {
            SubrangePoint thisOutput, thatOutput;

            //  Create a function that calculates the distance from this surface to that surface.
            SrfSrfDistFunction distFunc = SrfSrfDistFunction.newInstance(this, requireNonNull(thatSrf), tol, true);

            //  Create a list of potential closest points and add the edge curve closest points to it.
            FastTable<PointString<SubrangePoint>> potentials = FastTable.newInstance();
            SubrangePoint p = SubrangePoint.newInstance(this, nearS, nearT);
            PointString<SubrangePoint> str = PointString.valueOf(p, thatSrf.getClosest(p, tol));
            potentials.add(str);

            //  T = 0 curve.
            PointString<SubrangePoint> pc = getT0Curve().getClosest(thatSrf, tol);
            p = SubrangePoint.newInstance(this, pc.get(0).getParPosition().getValue(0), 0);
            str = PointString.valueOf(p, pc.get(1));
            potentials.add(str);

            //  T = 1 curve.
            pc = getT1Curve().getClosest(thatSrf, tol);
            p = SubrangePoint.newInstance(this, pc.get(0).getParPosition().getValue(0), 1);
            str = PointString.valueOf(p, pc.get(1));
            potentials.add(str);

            //  S = 0 curve.
            pc = getS0Curve().getClosest(thatSrf, tol);
            p = SubrangePoint.newInstance(this, 0, pc.get(0).getParPosition().getValue(0));
            str = PointString.valueOf(p, pc.get(1));
            potentials.add(str);

            //  S = 1 curve.
            pc = getS1Curve().getClosest(thatSrf, tol);
            p = SubrangePoint.newInstance(this, 1, pc.get(0).getParPosition().getValue(0));
            str = PointString.valueOf(p, pc.get(1));
            potentials.add(str);

            try {

                //  Use n-dimensional minimizer to locate closest point near "pos".
                double[] x = new double[2];
                x[0] = nearS;
                x[1] = nearT;
                Minimization.findND(distFunc, x, 2, tol);

                //  The minimizer may go slightly past the parametric bounds.  Deal with that.
                double s = x[0], t = x[1];
                if (s < 0)
                    s = 0;
                else if (s > 1)
                    s = 1;
                if (t < 0)
                    t = 0;
                else if (t > 1)
                    t = 1;

                //  Store the point from the minimizer.
                p = SubrangePoint.newInstance(this, s, t);
                str = PointString.valueOf(p, thatSrf.getClosest(p, tol));
                potentials.add(str);

            } catch (RootException err) {
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Failed to find closest point near 'pos'.", err);

            } finally {
                if (potentials.isEmpty()) {
                    thisOutput = getPoint(0, 0);
                    thatOutput = thatSrf.getClosest(thisOutput, tol);

                } else {
                    //  Loop over the potentials and find the one that is the closest.
                    thisOutput = potentials.get(0).get(0);
                    thatOutput = potentials.get(0).get(1);
                    double dOpt = thatOutput.distanceValue(thisOutput);
                    int size = potentials.size();
                    for (int i = 1; i < size; ++i) {
                        p = potentials.get(i).get(0);
                        SubrangePoint q = potentials.get(i).get(1);
                        double dist = q.distanceValue(p);
                        if (dist < dOpt) {
                            dOpt = dist;
                            thisOutput = p;
                            thatOutput = q;
                        }
                    }

                }
            }
            
            //  Gather the output parametric positions.
            thisS = thisOutput.getParPosition().getValue(0);
            thisT = thisOutput.getParPosition().getValue(1);
            thatS = thatOutput.getParPosition().getValue(0);
            thatT = thatOutput.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }

        PointString<SubrangePoint> output = PointString.newInstance();
        output.add(getPoint(thisS, thisT));
        output.add(getPoint(thatS, thatT));

        return output;
    }

    /**
     * Returns the closest point (giving the minimum distance) between this surface and
     * the specified plane. If the specified plane intersects this surface, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If the specified plane is
     * parallel to this surface (all points are equidistant away), then any point between
     * this surface and the specified plane could be returned as the "closest".
     *
     * @param plane The plane to find the closest points between this surface and that
     *              plane on. May not be null.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return A SubrangePoint that represent the closest point on this surface to the
     *         specified plane.
     */
    @Override
    public SubrangePoint getClosest(GeomPlane plane, double tol) {
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Make sure that plane and this surface have the same dimensions.
            int numDims = getPhyDimension();
            if (plane.getPhyDimension() > numDims)
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "plane", "surface"));
            else if (plane.getPhyDimension() < numDims)
                plane = plane.toDimension(numDims);

            //  Guess a location near enough to the closest point for the root solver
            //  to get us the rest of the way there.
            GeomPoint parNear = guessClosestFarthest(plane, true);

            //  Find the closest point between this surface and that plane.
            SubrangePoint p = getClosest(plane, parNear.getValue(0), parNear.getValue(1), tol);
            sOut = p.getParPosition().getValue(0);
            tOut = p.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }
        return getPoint(sOut, tOut);
    }

    /**
     * Method that guesses the most likely location for the closest or farthest point on a
     * surface and returns that location as a 2D point containing the "s" and "t"
     * parameter values. This is called by getClosest() and getFarthest(). This is
     * required in order for the root-finding algorithm to reliably refine the closest
     * point to the correct location.
     * <p>
     * Subclasses may override this method to provide a method for estimating the closest
     * point location that is most efficient for that surface type. The default
     * implementation extracts a 9x9 grid of subrange points, finds the closest one and
     * returns it's parametric position.
     * </p>
     *
     * @param plane   The plane to find the closest point on this surface to. May not be
     *                null.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The 2D parametric point on this surface that is approx. closest to the
     *         specified plane.
     */
    protected GeomPoint guessClosestFarthest(GeomPlane plane, boolean closest) {
        //  Create a 9x9 grid of subrange points on the surface.
        //  Return the parametric position of the closest/farthest grid point.
        return getClosestFarthestOnGrid(this, requireNonNull(plane), closest, 9).getParPosition();
    }

    /**
     * Extract a regularly spaced grid of parametric points on the surface and return the
     * grid point that is closest/farthest to/from the target plane. This ignores the
     * boundary points as they are handled by extracting the boundary curves.
     *
     * @param thisSrf   The surface to find the closest grid point on.
     * @param thatPlane The plane to find the closest point on this surface to.
     * @param closest   Set to <code>true</code> to return the closest point or
     *                  <code>false</code> to return the farthest point.
     * @param gridSize  The number of points to space out on this surface in each
     *                  parametric direction.
     * @return The subrange grid point on this surface that is closest to the specified
     *         plane.
     */
    protected static SubrangePoint getClosestFarthestOnGrid(Surface thisSrf, GeomPlane thatPlane, boolean closest, int gridSize) {
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Create a square grid of subrange points on the surface.
            List<Double> spacing = GridSpacing.linear(gridSize);
            spacing.remove(0);
            spacing.remove(spacing.size() - 1);
            PointArray<SubrangePoint> potentials = thisSrf.extractGrid(GridRule.PAR, spacing, null, spacing, null);
            FastTable.recycle((FastTable) spacing);

            //  Find the closest/farthest point in the grid.
            SubrangePoint pOpt = potentials.get(0, 0);
            Point point = thatPlane.getClosest(pOpt);
            double dOpt2 = point.distanceSqValue(pOpt);
            int cols = potentials.size();
            int rows = potentials.get(0).size();
            for (int i = 0; i < cols; ++i) {
                for (int j = 0; j < rows; ++j) {
                    SubrangePoint p = potentials.get(i, j);
                    point = thatPlane.getClosest(p);
                    double dist2 = point.distanceSqValue(p);
                    if (closest) {
                        if (dist2 < dOpt2) {
                            dOpt2 = dist2;
                            pOpt = p;
                        }
                    } else {
                        if (dist2 > dOpt2) {
                            dOpt2 = dist2;
                            pOpt = p;
                        }
                    }
                }
            }
            sOut = pOpt.getParPosition().getValue(0);
            tOut = pOpt.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }
        //  Return the the closest/farthest point.
        return thisSrf.getPoint(sOut, tOut);
    }

    /**
     * Returns the closest point on this surface to the specified plane.
     *
     * @param thatPlane The plane to find the closest point on this surface to.
     * @param nearS     The parametric s-position where the search for the closest point
     *                  should begin.
     * @param nearT     The parametric t-position where the search for the closest point
     *                  should begin.
     * @param tol       Fractional tolerance (in parameter space) to refine the point
     *                  position to.
     * @return The closest points on this surface to the specified plane.
     */
    private SubrangePoint getClosest(GeomPlane thatPlane, double nearS, double nearT, double tol) {
        double sOut, tOut;

        StackContext.enter();
        try {
            SubrangePoint thisOutput;

            //  Create a function that calculates the distance from this surface to that plane.
            SrfPlaneDistFunction distFunc = SrfPlaneDistFunction.newInstance(this, thatPlane, true);

            //  Create a list of potential closest points and add the edge curve closest points to it.
            FastTable<PointString<SubrangePoint>> potentials = FastTable.newInstance();
            SubrangePoint p = SubrangePoint.newInstance(this, nearS, nearT);
            PointString str = PointString.valueOf(p, thatPlane.getClosest(p));
            potentials.add(str);

            //  T = 0 curve.
            SubrangePoint pc = getT0Curve().getClosest(thatPlane, tol);
            p = SubrangePoint.newInstance(this, pc.getParPosition().getValue(0), 0);
            str = PointString.valueOf(p, thatPlane.getClosest(pc));
            potentials.add(str);

            //  T = 1 curve.
            pc = getT1Curve().getClosest(thatPlane, tol);
            p = SubrangePoint.newInstance(this, pc.getParPosition().getValue(0), 1);
            str = PointString.valueOf(p, thatPlane.getClosest(pc));
            potentials.add(str);

            //  S = 0 curve.
            pc = getS0Curve().getClosest(thatPlane, tol);
            p = SubrangePoint.newInstance(this, 0, pc.getParPosition().getValue(0));
            str = PointString.valueOf(p, thatPlane.getClosest(pc));
            potentials.add(str);

            //  S = 1 curve.
            pc = getS1Curve().getClosest(thatPlane, tol);
            p = SubrangePoint.newInstance(this, 1, pc.getParPosition().getValue(0));
            str = PointString.valueOf(p, thatPlane.getClosest(pc));
            potentials.add(str);

            try {

                //  Use n-dimensional minimizer to locate closest point near "pos".
                double[] x = new double[2];
                x[0] = nearS;
                x[1] = nearT;
                Minimization.findND(distFunc, x, 2, tol);

                //  The minimizer may go slightly past the parametric bounds.  Deal with that.
                double s = x[0], t = x[1];
                if (s < 0)
                    s = 0;
                else if (s > 1)
                    s = 1;
                if (t < 0)
                    t = 0;
                else if (t > 1)
                    t = 1;

                //  Store the point from the minimizer.
                p = SubrangePoint.newInstance(this, s, t);
                str = PointString.valueOf(p, thatPlane.getClosest(p));
                potentials.add(str);

            } catch (RootException err) {
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Failed to find closest point near 'pos'.", err);

            } finally {
                if (potentials.isEmpty()) {
                    thisOutput = getPoint(0, 0);

                } else {
                    //  Loop over the potentials and find the one that is the closest.
                    thisOutput = potentials.get(0).get(0);
                    GeomPoint q = potentials.get(0).get(1);
                    double dOpt2 = q.distanceSqValue(thisOutput);
                    int size = potentials.size();
                    for (int i = 1; i < size; ++i) {
                        p = potentials.get(i).get(0);
                        q = potentials.get(i).get(1);
                        double dist2 = q.distanceSqValue(p);
                        if (dist2 < dOpt2) {
                            dOpt2 = dist2;
                            thisOutput = p;
                        }
                    }

                }
            }
            sOut = thisOutput.getParPosition().getValue(0);
            tOut = thisOutput.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }

        return getPoint(sOut, tOut);
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this surface. This is more accurate than
     * <code>getBoundsMax()</code> & <code>getBoundsMin()</code>, but also typically takes
     * longer to compute.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance (in parameter space) to refine the min/max point
     *            position to.
     * @return The point found on this surface that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public SubrangePoint getLimitPoint(int dim, boolean max, double tol) {
        //  Check the input dimension for consistancy.
        int thisDim = getPhyDimension();
        if (dim < 0 || dim >= thisDim)
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("inputDimOutOfRange"), "surface"));
        double sOut, tOut;

        StackContext.enter();
        try {
            //  Create a vector pointing in the indicated direction.
            MutablePoint p = MutablePoint.newInstance(thisDim);
            p.set(dim, Parameter.valueOf(1, SI.METER));
            Vector<Dimensionless> uv = Vector.valueOf(p).toUnitVector();

            //  Get the bounds of the surface and bias it off 1% in the specified dimension.
            Point boundsMax = getBoundsMax();
            Point boundsMin = getBoundsMin();
            Parameter<Length> range = boundsMax.get(dim).minus(boundsMin.get(dim));
            MutablePoint pp;
            if (max) {
                pp = MutablePoint.valueOf(boundsMax);
                pp.set(dim, pp.get(dim).plus(range.times(0.01)));
            } else {
                pp = MutablePoint.valueOf(boundsMin);
                pp.set(dim, pp.get(dim).minus(range.times(0.01)));
            }

            //  Create a plane 1% of the diagonal off the bounds.
            Plane plane = Plane.valueOf(uv, pp.immutable());

            //  Return the closest point to the specified plane.
            SubrangePoint pnt = getClosest(plane, tol);
            sOut = pnt.getParPosition().getValue(0);
            tOut = pnt.getParPosition().getValue(1);

        } finally {
            StackContext.exit();
        }
        
        return getPoint(sOut, tOut);
    }

    /**
     * Return the intersections between an infinite line and this surface.
     *
     * @param L0   A point on the line (origin of the line). May not be null.
     * @param Ldir The direction vector for the line (does not have to be a unit vector).
     *             May not be null.
     * @param tol  Tolerance (in physical space) to refine the point positions to. May not
     *             be null.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this surface with the specified infinite line. If no
     *         intersection is found, an empty PointString is returned.
     */
    @Override
    public PointString<SubrangePoint> intersect(GeomPoint L0, GeomVector Ldir, Parameter<Length> tol) {
        requireNonNull(L0);
        requireNonNull(Ldir);
        requireNonNull(tol);
        PointString<SubrangePoint> output = PointString.newInstance();

        //  First check to see if the line even comes near the surface.
        if (!GeomUtil.lineBoundsIntersect(L0, Ldir, this, null))
            return output;

        //  Make sure the line and curve have the same dimensions.
        int numDims = getPhyDimension();
        if (L0.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "line", "surface"));
        else if (L0.getPhyDimension() < numDims)
            L0 = L0.toDimension(numDims);
        if (Ldir.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "line", "surface"));
        else if (Ldir.getPhyDimension() < numDims)
            Ldir = Ldir.toDimension(numDims);

        //  Convert the inputs to the units of this surface and to the dimension of this surface.
        Unit<Length> unit = getUnit();
        L0 = L0.toDimension(numDims).to(unit);
        GeomVector Lu = Ldir.toDimension(numDims);

        //  Make sure that the line direction vector is NOT dimensionless.
        if (Dimensionless.UNIT.equals(Lu.getUnit())) {
            Lu = Vector.valueOf(Lu.toFloat64Vector(), unit);
        } else {
            Lu = (GeomVector)Lu.to(unit);
        }
        if (Lu == Ldir) {
            Lu = Ldir.copy();
        }
        Lu.setOrigin(Point.newInstance(numDims, unit));
        tol = tol.to(unit);

        //  Look for intersections using recursive subdivision combined with ND root finding.
        LineSrfIntersect intersector = LineSrfIntersect.newInstance(this, L0, Lu, tol, output);
        output = intersector.intersect();
        LineSrfIntersect.recycle(intersector);

        return output;
    }

    /**
     * Return the intersections between a line segment and this surface.
     *
     * @param line A line segment to intersect with this surface. May not be null.
     * @param tol  Tolerance (in physical space) to refine the point positions to. May not
     *             be null.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this surface and the input line segment respectively,
     *         made by the intersection of this surface with the specified line segment.
     *         If no intersections are found a list of two empty PointStrings are
     *         returned.
     */
    @Override
    public GeomList<PointString<SubrangePoint>> intersect(LineSegment line, Parameter<Length> tol) {
        requireNonNull(line);
        requireNonNull(tol);
        PointString<SubrangePoint> thisOutput = PointString.newInstance();
        PointString<SubrangePoint> thatOutput = PointString.newInstance();
        GeomList<PointString<SubrangePoint>> output = GeomList.newInstance();
        output.add(thisOutput);
        output.add(thatOutput);

        //  First check to see if the line even comes near the surface.
        if (!GeomUtil.lineSegBoundsIntersect(line.getStart(), line.getEnd(), this))
            return output;

        //  Start by intersecting an infinite line with the surface.
        GeomVector Lu = line.getDerivativeVector();
        PointString<SubrangePoint> potentials = intersect(line.getStart(), Lu, tol);

        //  Now throw out any points that are more than "tol" distance beyond the
        //  ends of the line.
        int size = potentials.size();
        for (int i = 0; i < size; ++i) {
            SubrangePoint pnt = potentials.get(i);
            Parameter<Length> distance = GeomUtil.pointLineSegDistance(pnt, line.getStart(), line.getEnd());
            if (!distance.isLargerThan(tol)) {
                thisOutput.add(pnt);
                thatOutput.add(line.getClosest(pnt, GTOL));
            }
        }

        //  Clean up before leaving.
        PointString.recycle(potentials);

        return output;
    }

    /**
     * Return the intersections between a curve and this surface.
     *
     * @param curve The curve to intersect with this surface. May not be null.
     * @param tol   Tolerance (in physical space) to refine the point positions to. May
     *              not be null.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this surface and the input curve respectively, made by
     *         the intersection of this surface with the specified curve. If no
     *         intersections are found a list of two empty PointStrings are returned.
     */
    @Override
    public GeomList<PointString<SubrangePoint>> intersect(Curve curve, Parameter<Length> tol) {
        GeomList<PointString<SubrangePoint>> ints = curve.intersect(this, requireNonNull(tol));
        GeomList<PointString<SubrangePoint>> output = ints.reverse();
        GeomList.recycle(ints);
        return output;
    }

    /**
     * Return the intersections between an infinite plane and this surface.
     *
     * @param plane The infinite plane to intersect with this surface. May not be null.
     * @param tol   Tolerance (in physical space) to refine the point positions to. May not be null.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this surface with the specified infinite plane. If no
     *         intersection is found, an empty PointString is returned.
     */
    @Override
    public GeomList<SubrangeCurve> intersect(GeomPlane plane, Parameter<Length> tol) {
        requireNonNull(tol);
        GeomList<SubrangeCurve> output = GeomList.newInstance();

        //  First check to see if the line even comes near the surface.
        if (!GeomUtil.planeBoundsIntersect(requireNonNull(plane), this))
            return output;

        //  Make sure the plane has the same dimensions as this surface.
        int numDims = getPhyDimension();
        if (plane.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "plane", "surface"));

        //  Convert the inputs to the units of this surface and to the dimension of this surface.
        Unit<Length> unit = getUnit();
        plane = plane.toDimension(numDims).to(unit);
        tol = tol.to(unit);

        //  Look for and trace intersections.
        PlaneSrfIntersect intersector = PlaneSrfIntersect.newInstance(this, plane, tol, output);
        try {
            output = intersector.intersect();

        } catch (RootException err) {
            Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                    "Failed to find intersection with a plane.", err);

        } finally {
            PlaneSrfIntersect.recycle(intersector);
        }

        return output;
    }

    /**
     * Return the intersections between another surface and this surface.
     *
     * @param surface A surface to intersect with this surface. May not be null.
     * @param tol     Tolerance (in physical space) to refine the point positions to. May
     *                not be null.
     * @return A list containing two lists of SubrangeCurve objects. Each list contains
     *         zero or more subrange curves, on this surface and the input surface
     *         respectively, made by the intersection of this surface with the specified
     *         surface. If no intersections are found a list of two empty lists is
     *         returned.
     */
    @Override
    public GeomList<GeomList<SubrangeCurve>> intersect(Surface surface, Parameter<Length> tol) {
        requireNonNull(surface);
        requireNonNull(tol);
        GeomList<SubrangeCurve> thisOutput = GeomList.newInstance();
        GeomList<SubrangeCurve> thatOutput = GeomList.newInstance();
        GeomList<GeomList<SubrangeCurve>> output = GeomList.newInstance();
        output.add(thisOutput);
        output.add(thatOutput);

        //  First check to see if the other surface even comes near this surface.
        if (!GeomUtil.boundsIntersect(this, surface))
            return output;

        //  Make sure the input surface has the same dimensions as this surface.
        int numDims = getPhyDimension();
        if (surface.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "input surface", "surface"));

        //  Convert the inputs to the units of this surface and to the dimension of this surface.
        Unit<Length> unit = getUnit();
        surface = surface.toDimension(numDims).to(unit);
        tol = tol.to(unit);

        //  Look for and trace intersections.
        SrfSrfIntersect intersector = SrfSrfIntersect.newInstance(this,
                (AbstractSurface)surface, tol, thisOutput, thatOutput);
        try {
            intersector.intersect();

        } catch (RootException err) {
            Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                    "Failed to find intersection with a surface.", err);

        } finally {
            SrfSrfIntersect.recycle(intersector);
        }

        return output;
    }

    /**
     * Return a list or lists containing parameters at the start of logical patches of
     * this surface in each parametric direction. The first list contains the parameters
     * in the "S" direction and the 2nd in the "T" direction. The first element in each
     * list must always be 0.0 and the last element 1.0. These should be good places to
     * break the surface up into pieces for analysis, but otherwise are arbitrary.
     * Subclasses should override this method to provide better patch starting parameters.
     * <p>
     * The default implementation always splits the patch into four pieces and returns the
     * following parameters [0, 0.5, 1],[0, 0.50, 1].
     * </p>
     *
     * @return A list of two lists containing parameters at the start of logical patches
     *         of this surface in each parametric direction.
     */
    protected FastTable<FastTable<Double>> getPatchParameters() {
        FastTable<FastTable<Double>> tbls = FastTable.newInstance();
        FastTable<Double> knots = FastTable.newInstance();
        knots.add(0.0);
        knots.add(0.5);
        knots.add(1.0);
        tbls.add(knots);
        knots = FastTable.newInstance();
        knots.add(0.0);
        knots.add(0.5);
        knots.add(1.0);
        tbls.add(knots);
        return tbls;
    }

    /**
     * Return the number of points per patch in the "S" direction that should be used when
     * analyzing surface patches by certain methods. Subclasses should override this
     * method to provide a more appropriate number of points per patch in the "S"
     * direction.
     * <p>
     * The default implementation always returns 8.
     * </p>
     *
     * @return The number of points per patch in the S direction to use for analysis.
     */
    protected int getNumPointsPerPatchS() {
        return 8;   //  2*(3 + 1)
    }

    /**
     * Return the number of points per patch in the "T" direction that should be used when
     * analyzing surface patches by certain methods. Subclasses should override this
     * method to provide a more appropriate number of points per patch in the "T"
     * direction.
     * <p>
     * The default implementation always returns 8.
     * </p>
     *
     * @return The number of points per patch in the T direction to use for analysis.
     */
    protected int getNumPointsPerPatchT() {
        return 8;   //  2*(3 + 1)
    }

    /**
     * Return <code>true</code> if this surface is degenerate (i.e.: has area less than
     * the specified tolerance squared).
     *
     * @param tol The tolerance for determining if this surface is degenerate. May not be null.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        StackContext.enter();
        try {
            Parameter<Area> area = getArea(GTOL * 100);
            return area.compareTo(tol.times(tol)) <= 0;
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return <code>true</code> if this surface is planar or <code>false</code> if it is
     * not.
     *
     * @param tol The geometric tolerance to use in determining if the surface is planar.
     *            May not be null.
     */
    @Override
    public boolean isPlanar(Parameter<Length> tol) {
        requireNonNull(tol);

        //  If the surface is less than 3D, then it must be planar.
        int numDims = getPhyDimension();
        if (numDims <= 2)
            return true;

        StackContext.enter();
        try {
            //  If the surface is degenerate (zero area; a point or curve), then check edge curves for being planar.
            if (isDegenerate(tol)) {
                Curve sCrv = getT0Curve();
                Curve tCrv = getS0Curve();
                if (sCrv.isPlanar(tol) && tCrv.isPlanar(tol)) {
                    return true;
                }
            }

            //  Form a plane from the diagonals of the surface.
            Point p0 = getRealPoint(0, 0);
            Point p1 = getRealPoint(1, 1);
            Vector<Length> d1 = p1.minus(p0).toGeomVector();
            p0 = getRealPoint(1, 0);
            p1 = getRealPoint(0, 1);
            Vector<Length> d2 = p1.minus(p0).toGeomVector();
            Vector<Dimensionless> nhat = d1.cross(d2).toUnitVector();

            //  Get the number of points per patch in the S & T directions.
            int nPntsS = getNumPointsPerPatchS();
            int nPntsT = getNumPointsPerPatchT();

            //  Get the locations of logical patches on this surface.
            FastTable<FastTable<Double>> pParams = getPatchParameters();
            FastTable<Double> sParams = pParams.get(0);
            FastTable<Double> tParams = pParams.get(1);

            //  Enrich the patch parameters by the number of points for each patch.
            sParams = enrichParamList(sParams, nPntsS);
            tParams = enrichParamList(tParams, nPntsT);

            //  Extract a grid of points
            PointArray<SubrangePoint> arr = this.extractGrid(GridRule.PAR, sParams, null, tParams, null);

            //  The surface is planar if all the points in the extracted grid are planar.
            int numStr = arr.size();
            for (int i = 0; i < numStr; ++i) {
                PointString<SubrangePoint> str = arr.get(i);
                int numPnts = str.size();
                for (int j = 0; j < numPnts; ++j) {
                    SubrangePoint sPnt = str.get(j);
                    Point p = sPnt.copyToReal();
                    p1 = GeomUtil.pointPlaneClosest(p, p0, nhat);
                    if (p.distance(p1).isGreaterThan(tol))
                        return false;
                }
            }

            return true;

        } finally {
            StackContext.exit();
        }
    }

    private static FastTable<Double> enrichParamList(List<Double> oldParams, int numPerSeg) {
        //  A list of values evenly or linearly spaced between 0 and 1.
        FastTable<Double> spacing = (FastTable)GridSpacing.linear(numPerSeg);
        spacing.remove(spacing.size() - 1);   //  Remove the last point to prevent duplication below.

        FastTable<Double> newParams = FastTable.newInstance();
        int size = oldParams.size() - 1;
        for (int i = 0; i < size; ++i) {
            double pi = oldParams.get(i);
            double pip1 = oldParams.get(i + 1);
            //  Interpolate in a set of values between each existing value.
            for (Double s : spacing) {
                double p = pi + (pip1 - pi) * s;
                newParams.add(p);
            }
        }
        newParams.add(1.0);

        //  Clean up.
        FastTable.recycle(spacing);

        return newParams;
    }

    /**
     * Return <code>true</code> if this surface is planar or <code>false</code> if it is
     * not.
     *
     * @param epsFT  Flatness tolerance (cosine of the angle allowable between normal
     *               vectors).
     * @param epsELT Edge linearity tolerance (cosine of the angle allowable between edge
     *               vectors).
     */
    @Override
    public boolean isPlanar(double epsFT, double epsELT) {

        StackContext.enter();
        try {
            //  Check to see if the corner and center normals are parallel.
            Vector<Dimensionless> ni = getNormal(0, 0);
            Vector<Dimensionless> nj = getNormal(1, 0);
            if (ni.dot(nj).getValue() <= epsFT)
                return false;
            nj = getNormal(1, 1);
            if (ni.dot(nj).getValue() <= epsFT)
                return false;
            nj = getNormal(0, 1);
            if (ni.dot(nj).getValue() <= epsFT)
                return false;
            nj = getNormal(0.5, 0.5);
            if (ni.dot(nj).getValue() <= epsFT)
                return false;

            //  Check to see if the edge vectors at each corner are parallel.
            Vector<Dimensionless> Ti = getSDerivative(0, 0, 1, false).toUnitVector();
            Vector<Dimensionless> Tj = getSDerivative(1, 0, 1, false).toUnitVector();
            if (Ti.dot(Tj).getValue() < epsELT)
                return false;
            Ti = getSDerivative(0, 1, 1, false).toUnitVector();
            Tj = getSDerivative(1, 1, 1, false).toUnitVector();
            if (Ti.dot(Tj).getValue() < epsELT)
                return false;
            Ti = getTDerivative(1, 0, 1, false).toUnitVector();
            Tj = getTDerivative(1, 1, 1, false).toUnitVector();
            if (Ti.dot(Tj).getValue() < epsELT)
                return false;
            Ti = getTDerivative(0, 1, 1, false).toUnitVector();
            Tj = getTDerivative(0, 0, 1, false).toUnitVector();

            return Ti.dot(Tj).getValue() >= epsELT;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Converts the input parametric position on a surface patch with the specified range
     * of parametric positions into a parametric position on the parent surface.
     *
     * @param s  The parametric position on the surface patch to be converted.
     * @param s0 The starting parametric position of the surface patch on the parent
     *           surface.
     * @param s1 The ending parametric position of the surface patch on the parent
     *           surface.
     * @return The input patch parametric position converted to the parent surface.
     */
    protected static double segmentPos2Parent(double s, double s0, double s1) {
        double s_out = s0 + (s1 - s0) * s;
        if (s_out > 1) {            //  Deal with roundoff issues.
            s_out = 1;
        } else if (s_out < 0) {
            s_out = 0;
        }
        return s_out;
    }

    /**
     * Method that returns true if the specified surface is smaller than the specified
     * tolerance.
     */
    private static boolean isSmall(Surface srf, Parameter<Length> tol) {
        StackContext.enter();
        try {
            Parameter<Length> diag = srf.getBoundsMax().distance(srf.getBoundsMin());
            return tol.isGreaterThan(diag);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Remove any points from the input list that are closer than the given tolerance to
     * each other.
     */
    private static PointString<SubrangePoint> removeClosePoints(PointString<SubrangePoint> input, Parameter<Length> tol) {

        //  Remove any redundant points
        int size = input.size();
        for (int j = 0; j < size; ++j) {
            SubrangePoint p0 = input.get(j);
            for (int i = size - 1; i >= 0; --i) {
                if (i == j)
                    continue;
                SubrangePoint p1 = input.get(i);
                if (p0.distance(p1).isLessThan(tol)) {
                    //  Make sure we have not jumpped a parametric boundary.
                    Point st0 = p0.getParPosition().immutable();
                    double s0 = st0.getValue(0);
                    double t0 = st0.getValue(1);
                    Point st1 = p1.getParPosition().immutable();
                    double s1 = st1.getValue(0);
                    double t1 = st1.getValue(1);
                    if ((s0 > 0.8 && s1 < 0.2) || (s0 < 0.2 && s1 > 0.8))
                        continue;
                    if ((t0 > 0.8 && t1 < 0.2) || (t0 < 0.2 && t1 > 0.8))
                        continue;
                    //  Go ahead and remove the point.
                    input.remove(i);
                }
            }
            size = input.size();
        }

        return input;
    }

    /**
     * An Evaulatable1D that returns the area on a surface integrated in one dimension.
     * This is used to find the surface area of a surface. This is <code>f(x)</code> in:
     * <code>A = int_t1^t2 {f(x)dt} = int_t1^t2{ int_s1^s2{ |pu x pw| ds } dt }</code>.
     * This is also a Runnable that integrates the total area of the surface:
     * <code>A = int_t1^t2{ int_s1^s2{ |pu x pw| ds } dt }</code>
     */
    private static class AreaEvaluatable extends AbstractEvaluatable1D implements Runnable {

        private double _value;
        private double _t1, _t2;

        private double _s1, _s2;
        private double _eps;
        private AreaEvaluatable2 _subAreaEval;
        public int numEvaluations;

        public static AreaEvaluatable newInstance(AbstractSurface surface, double s1, double s2,
                double t1, double t2, double eps) {
            if (surface instanceof GeomTransform)
                surface = (AbstractSurface)surface.copyToReal();
            AreaEvaluatable o = FACTORY.object();
            o._s1 = s1;
            o._s2 = s2;
            o._t1 = t1;
            o._t2 = t2;
            o._eps = eps;
            o._subAreaEval = AreaEvaluatable2.newInstance(surface);
            o.numEvaluations = 0;
            return o;
        }

        /**
         * Run to find the total area of the surface:
         * <code>A = int_t1^t2{ int_s1^s2{ |pu x pw| ds } dt }</code>
         */
        @Override
        public void run() {
            try {
                //  Integrate to find the area: A = int_t1^t2{ int_s1^s2{ |pu x pw| ds } dt }
                _value = Quadrature.adaptLobatto(this, _t1, _t2, _eps);

            } catch (IntegratorException | RootException e) {
                //  Fall back on Gaussian Quadrature.
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Falling back on Gaussian Quadrature for getArea().");
                try {
                    _value = Quadrature.gaussLegendre_Wx1N40(this, _t1, _t2);

                } catch (RootException err) {
                    Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                            "Failed to find area of surface patch.", err);
                }
            }

        }

        /**
         * Returns the total area of the surface in surface area units after a call to
         * run().
         *
         * @return The total area of the surface in surface area units.
         */
        public double getValue() {
            return _value;
        }

        /**
         * Returns the inner integral on the surface at a given value of "t":
         * <code>f(t) = int_s1^s2{ |pu x pw| ds }</code>
         */
        @Override
        public double function(double t) throws RootException {
            double value = 0;
            _subAreaEval.t = t;
            _subAreaEval.numEvaluations = 0;
            try {

                //  Integrate value = int_s1^s2{ |pu x pw| ds }
                value = Quadrature.adaptLobatto(_subAreaEval, _s1, _s2, _eps / 10);

            } catch (IntegratorException | RootException e) {
                //  Fall back on Gaussian Quadrature.
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Falling back on Gaussian Quadrature for AreaEvaluatable.");
                try {
                    value = Quadrature.gaussLegendre_Wx1N40(_subAreaEval, _s1, _s2);

                } catch (RootException err) {
                    throw new RootException(err.getMessage());
                }

            } finally {
                numEvaluations += _subAreaEval.numEvaluations;
            }
            return value;
        }

        public static void recycle(AreaEvaluatable instance) {
            AreaEvaluatable2.recycle(instance._subAreaEval);
            FACTORY.recycle(instance);
        }

        private AreaEvaluatable() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<AreaEvaluatable> FACTORY = new ObjectFactory<AreaEvaluatable>() {
            @Override
            protected AreaEvaluatable create() {
                return new AreaEvaluatable();
            }

            @Override
            protected void cleanup(AreaEvaluatable obj) {
                obj._subAreaEval = null;
            }
        };
    }

    /**
     * An Evaulatable1D that returns the area of an infinitesimally small portion of the
     * surface at (s,t). This is used to find the surface area of a surface. This is
     * <code>f(x)</code> in:
     * <code>A = int_s1^s2 {f(x)dt} = int_s1^s2{ |pu x pw| ds }</code>.
     */
    private static class AreaEvaluatable2 extends AbstractEvaluatable1D {

        private AbstractSurface _srf;
        private int _dim;
        public double t;
        public int numEvaluations;

        public static AreaEvaluatable2 newInstance(AbstractSurface surface) {
            AreaEvaluatable2 o = FACTORY.object();
            o._srf = surface;
            o._dim = surface.getPhyDimension();
            o.numEvaluations = 0;
            return o;
        }

        @Override
        public double function(double s) throws RootException {
            ++numEvaluations;

            StackContext.enter();
            try {
                //  Get the surface derivative in the s direction.
                Vector<Length> pu = _srf.getSDerivative(s, t, 1, true);

                //  Collapsed edges provide no increment in area.
                if (pu.magValue() < SQRT_EPS) {
                    return 0.0;
                }

                //  Gert the surface derivative in the t direction.
                Vector<Length> pw = _srf.getTDerivative(s, t, 1, true);

                //  Collapsed edges provide no increment in area.
                if (pw.magValue() < SQRT_EPS) {
                    return 0.0;
                }

                if (_dim < 3) {
                    //  2D is a special case.
                    double pux = pu.getValue(0), puy = pu.getValue(1);
                    double pwx = pw.getValue(0), pwy = pw.getValue(1);
                    double puxpw = pux * pwy - puy * pwx;
                    return puxpw;
                }

                //  Normal vector is the cross product of pu and pw vectors
                //  (it is also the area swept out by the two tangent vectors).
                Vector n = pu.cross(pw);

                //  Return the magnitude of the normal vector.
                double nmag = n.magValue();
                return nmag;

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(AreaEvaluatable2 instance) {
            FACTORY.recycle(instance);
        }

        private AreaEvaluatable2() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<AreaEvaluatable2> FACTORY = new ObjectFactory<AreaEvaluatable2>() {
            @Override
            protected AreaEvaluatable2 create() {
                return new AreaEvaluatable2();
            }

            @Override
            protected void cleanup(AreaEvaluatable2 obj) {
                obj._srf = null;
            }
        };
    }

    /**
     * An Evaulatable1D that returns the volume on a surface integrated in one dimension.
     * This is used to find the enclosed volume of a surface. This is <code>f(x)</code>
     * in:
     * <code>A = int_t1^t2 {f(x)dt} = int_t1^t2{ int_s1^s2{ 1/3*(p DOT n) ds } dt }</code>.
     * This is also a Runnable that integrates the total volume of the surface:
     * <code>V = int_t1^t2{ int_s1^s2{ 1/3*(p DOT n) ds } dt</code>
     */
    private static class VolumeEvaluatable extends AbstractEvaluatable1D implements Runnable {

        private double _value;
        private double _t1, _t2;

        private double _s1, _s2;
        private double _eps;
        private VolumeEvaluatable2 _subVolumeEval;
        public int numEvaluations;

        public static VolumeEvaluatable newInstance(AbstractSurface surface, double s1, double s2,
                double t1, double t2, double eps) {
            if (surface instanceof GeomTransform)
                surface = (AbstractSurface)surface.copyToReal();
            VolumeEvaluatable o = FACTORY.object();
            o._s1 = s1;
            o._s2 = s2;
            o._t1 = t1;
            o._t2 = t2;
            o._eps = eps;
            o._subVolumeEval = VolumeEvaluatable2.newInstance(surface);
            o.numEvaluations = 0;
            return o;
        }

        /**
         * Run to find the total volume of the surface:
         * <code>V = int_t1^t2{ int_s1^s2{ 1/3*(p DOT n) ds } dt</code>
         */
        @Override
        public void run() {
            try {
                //  Integrate to find the volume:   V = int_t1^t2{ int_s1^s2{ 1/3*(p DOT n) ds } dt
                _value = Quadrature.adaptLobatto(this, _t1, _t2, _eps);

            } catch (IntegratorException | RootException e) {
                //  Fall back on Gaussian Quadrature.
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Fallling back on Gaussian Quadrature for getVolume().");
                try {
                    _value = Quadrature.gaussLegendre_Wx1N40(this, _t1, _t2);

                } catch (RootException err) {
                    Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                            "Failed to find volume of surface patch.", err);
                }
            }

        }

        /**
         * Returns the total volume of the surface in surface volume units after a call to
         * run().
         */
        public double getValue() {
            return _value;
        }

        @Override
        public double function(double t) throws RootException {
            double value = 0;
            _subVolumeEval.t = t;
            _subVolumeEval.numEvaluations = 0;

            try {

                //  Integrate value = 1/3*int_s1^s2{ (p DOT n) ds }
                value = Quadrature.adaptLobatto(_subVolumeEval, _s1, _s2, _eps) / 3;

            } catch (IntegratorException | RootException e) {
                //  Fall back on Gaussian Quadrature.
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Fallling back on Gaussian Quadrature for VolumeEvaluatable.");
                try {
                    value = Quadrature.gaussLegendre_Wx1N40(_subVolumeEval, _s1, _s2) / 3;

                } catch (RootException err) {
                    throw new RootException(err.getMessage());
                }

            } finally {
                numEvaluations += _subVolumeEval.numEvaluations;
            }

            return value;
        }

        public static void recycle(VolumeEvaluatable instance) {
            VolumeEvaluatable2.recycle(instance._subVolumeEval);
            FACTORY.recycle(instance);
        }

        private VolumeEvaluatable() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<VolumeEvaluatable> FACTORY = new ObjectFactory<VolumeEvaluatable>() {
            @Override
            protected VolumeEvaluatable create() {
                return new VolumeEvaluatable();
            }

            @Override
            protected void cleanup(VolumeEvaluatable obj) {
                obj._subVolumeEval = null;
            }
        };
    }

    /**
     * An Evaulatable1D that returns the volume of an infinitesimally small portion of the
     * surface at (s,t). This is used to find the enclosed volume of a surface. This is
     * <code>f(x)</code> in:
     * <code>A = int_s1^s2 {f(x)dt} = int_s1^s2{ 1/3*(p DOT n) ds }</code>.
     */
    private static class VolumeEvaluatable2 extends AbstractEvaluatable1D {

        private AbstractSurface _srf;
        public double t;
        private int _dim;
        public int numEvaluations;

        public static VolumeEvaluatable2 newInstance(AbstractSurface surface) {
            VolumeEvaluatable2 o = FACTORY.object();
            o._srf = surface;
            o._dim = surface.getPhyDimension();
            return o;
        }

        @Override
        public double function(double s) throws RootException {
            ++numEvaluations;

            StackContext.enter();
            try {
                //  Get the derivatives in the s-direction.
                List<Vector<Length>> ders = _srf.getSDerivatives(s, t, 1, true);

                //  Get the surface point (it is in the derivative list).
                Vector<Length> p = ders.get(0);
                if (p.norm().isApproxZero()) {
                    return 0;
                }

                //  Get the surface derivative in the s direction.
                Vector<Length> pu = ders.get(1);

                //  Collapsed edges provide no increment in volume.
                if (pu.magValue() < SQRT_EPS) {
                    return 0.0;
                }

                //  Get the surface derivative in the t direction.
                Vector<Length> pw = _srf.getTDerivative(s, t, 1, true);

                //  Collapsed edges provide no increment in volume.
                if (pw.magValue() < SQRT_EPS) {
                    return 0.0;
                }

                //  Make sure the vectors are at least 3D.
                if (_dim < 3) {
                    p = p.toDimension(3);
                    pu = pu.toDimension(3);
                    pw = pw.toDimension(3);
                }

                //  Normal vector is the cross product of pu and pw vectors
                //  (it is also the area swept out by the two tangent vectors).
                Vector n = pu.cross(pw);

                //  Calculate three times the volume increment.
                double dV = p.dot(n).getValue();

                //  Return the 3xVolume increment.
                return dV;

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(VolumeEvaluatable2 instance) {
            FACTORY.recycle(instance);
        }

        private VolumeEvaluatable2() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<VolumeEvaluatable2> FACTORY = new ObjectFactory<VolumeEvaluatable2>() {
            @Override
            protected VolumeEvaluatable2 create() {
                return new VolumeEvaluatable2();
            }

            @Override
            protected void cleanup(VolumeEvaluatable2 obj) {
                obj._srf = null;
            }
        };
    }

    /**
     * A ScalarFunctionND that calculates the distance squared from a target point to
     * points on the specified surface at s,t. This is intended to only be used by
     * AbstractSurface for finding the closest and farthest points.
     */
    private static class PointSurfaceDistFunction implements ScalarFunctionND {

        private Surface _srf;
        private Point _point;
        private boolean _closest;
        public int numEvaluations;

        public static PointSurfaceDistFunction newInstance(Surface surface, GeomPoint point, boolean closest) {
            if (surface instanceof GeomTransform)
                surface = (Surface)surface.copyToReal();
            PointSurfaceDistFunction o = FACTORY.object();
            o._srf = surface;
            o._point = point.immutable();
            o._closest = closest;
            o.numEvaluations = 0;
            return o;
        }

        @Override
        public double function(double x[]) throws RootException {
            double s = x[0];
            double t = x[1];

            //  Deal with the minimizer going out of bounds of the surface parameterization
            //  by penalizing such points.
            double fs = 1, ft = 1;
            if (s < 0) {
                fs = 1 - s;
                s = 0;
            }
            if (s > 1) {
                fs = fs * s;
                s = 1;
            }
            if (t < 0) {
                ft = 1 - t;
                t = 0;
            }
            if (t > 1) {
                ft = ft * t;
                t = 1;
            }
            fs *= fs;
            ft *= ft;

            StackContext.enter();
            try {
                //  Get the point on the surface at s,t.
                Point p = _srf.getRealPoint(s, t);

                //  Calculate the distance to the surface point from our target point.
                double dist = _point.distanceValue(p);

                if (_closest)
                    //  Penalize the distance if the input point is out of bounds of the surface.
                    dist *= fs * ft;
                else {
                    //  Penalize the distance if the input point is out of bounds of the surface.
                    dist /= fs * ft;

                    //  Change sign on distance to find the farthest point.
                    dist *= -1;
                }

                ++numEvaluations;
                return dist;

            } finally {
                StackContext.exit();
            }
        }

        @Override
        public boolean derivatives(double[] x, double[] dydx) {
            return false;
        }

        public static void recycle(PointSurfaceDistFunction instance) {
            FACTORY.recycle(instance);
        }

        private PointSurfaceDistFunction() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<PointSurfaceDistFunction> FACTORY = new ObjectFactory<PointSurfaceDistFunction>() {
            @Override
            protected PointSurfaceDistFunction create() {
                return new PointSurfaceDistFunction();
            }

            @Override
            protected void cleanup(PointSurfaceDistFunction obj) {
                obj._srf = null;
                obj._point = null;
            }
        };
    }

    /**
     * A ScalarFunctionND that calculates the distance squared from a target point to
     * points on the specified surface at s,t. This is intended to only be used by
     * AbstractSurface for finding the closest and farthest points.
     */
    private static class SrfSrfDistFunction implements ScalarFunctionND {

        private Surface _thisSrf;
        private Surface _thatSrf;
        private double _tol;
        private boolean _closest;
        public int numEvaluations;

        public static SrfSrfDistFunction newInstance(Surface thisSrf, Surface thatSrf, double tol, boolean closest) {
            if (thisSrf instanceof GeomTransform)
                thisSrf = (Surface)thisSrf.copyToReal();
            if (thatSrf instanceof GeomTransform)
                thatSrf = (Surface)thatSrf.copyToReal();
            SrfSrfDistFunction o = FACTORY.object();
            o._thisSrf = thisSrf;
            o._thatSrf = thatSrf;
            o._tol = tol;
            o._closest = closest;
            o.numEvaluations = 0;
            return o;
        }

        @Override
        public double function(double x[]) throws RootException {
            double s = x[0];
            double t = x[1];

            //  Deal with the minimizer going out of bounds of the surface parameterization
            //  by penalizing such points.
            double fs = 1, ft = 1;
            if (s < 0) {
                fs = 1 - s;
                s = 0;
            }
            if (s > 1) {
                fs = fs * s;
                s = 1;
            }
            if (t < 0) {
                ft = 1 - t;
                t = 0;
            }
            if (t > 1) {
                ft = ft * t;
                t = 1;
            }
            fs *= fs;
            ft *= ft;

            StackContext.enter();
            try {
                //  Get the point on the surface at s,t.
                Point p = _thisSrf.getRealPoint(s, t);

                //  Get the closest point on the remote surface.
                SubrangePoint q = _thatSrf.getClosest(p, _tol);

                //  Calculate the distance to the surface point from our target point.
                double dist = q.distanceValue(p);

                if (_closest)
                    //  Penalize the distance if the input point is out of bounds of the surface.
                    dist *= fs * ft;
                else {
                    //  Penalize the distance if the input point is out of bounds of the surface.
                    dist /= fs * ft;

                    //  Change sign on distance to find the farthest point.
                    dist *= -1;
                }

                ++numEvaluations;
                return dist;

            } finally {
                StackContext.exit();
            }
        }

        @Override
        public boolean derivatives(double[] x, double[] dydx) {
            return false;
        }

        public static void recycle(SrfSrfDistFunction instance) {
            FACTORY.recycle(instance);
        }

        private SrfSrfDistFunction() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<SrfSrfDistFunction> FACTORY = new ObjectFactory<SrfSrfDistFunction>() {
            @Override
            protected SrfSrfDistFunction create() {
                return new SrfSrfDistFunction();
            }

            @Override
            protected void cleanup(SrfSrfDistFunction obj) {
                obj._thisSrf = null;
                obj._thatSrf = null;
            }
        };
    }

    /**
     * A ScalarFunctionND that calculates the distance squared from a target plane to
     * points on the specified surface at s,t. This is intended to only be used by
     * AbstractSurface for finding the closest and farthest points.
     */
    private static class SrfPlaneDistFunction implements ScalarFunctionND {

        private Surface _thisSrf;
        private GeomPlane _thatPlane;
        private boolean _closest;
        public int numEvaluations;

        public static SrfPlaneDistFunction newInstance(Surface thisSrf, GeomPlane thatPlane, boolean closest) {
            if (thisSrf instanceof GeomTransform)
                thisSrf = (Surface)thisSrf.copyToReal();
            if (thatPlane instanceof GeomTransform)
                thatPlane = thatPlane.copyToReal();
            SrfPlaneDistFunction o = FACTORY.object();
            o._thisSrf = thisSrf;
            o._thatPlane = thatPlane;
            o._closest = closest;
            o.numEvaluations = 0;
            return o;
        }

        @Override
        public double function(double x[]) throws RootException {
            double s = x[0];
            double t = x[1];

            //  Deal with the minimizer going out of bounds of the surface parameterization
            //  by penalizing such points.
            double fs = 1, ft = 1;
            if (s < 0) {
                fs = 1 - s;
                s = 0;
            }
            if (s > 1) {
                fs = fs * s;
                s = 1;
            }
            if (t < 0) {
                ft = 1 - t;
                t = 0;
            }
            if (t > 1) {
                ft = ft * t;
                t = 1;
            }
            fs *= fs;
            ft *= ft;

            StackContext.enter();
            try {
                //  Get the point on the surface at s,t.
                Point p = _thisSrf.getRealPoint(s, t);

                //  Get the closest point on the remote plane.
                Point q = _thatPlane.getClosest(p);

                //  Calculate the distance to the surface point from our target point.
                double dist = q.distanceValue(p);

                if (_closest)
                    //  Penalize the distance if the input point is out of bounds of the surface.
                    dist *= fs * ft;
                else {
                    //  Penalize the distance if the input point is out of bounds of the surface.
                    dist /= fs * ft;

                    //  Change sign on distance to find the farthest point.
                    dist *= -1;
                }

                ++numEvaluations;
                return dist;

            } finally {
                StackContext.exit();
            }
        }

        @Override
        public boolean derivatives(double[] x, double[] dydx) {
            return false;
        }

        public static void recycle(SrfPlaneDistFunction instance) {
            FACTORY.recycle(instance);
        }

        private SrfPlaneDistFunction() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<SrfPlaneDistFunction> FACTORY = new ObjectFactory<SrfPlaneDistFunction>() {
            @Override
            protected SrfPlaneDistFunction create() {
                return new SrfPlaneDistFunction();
            }

            @Override
            protected void cleanup(SrfPlaneDistFunction obj) {
                obj._thisSrf = null;
                obj._thatPlane = null;
            }
        };
    }

    /**
     * A class used to intersect an line segment and this surface.
     */
    private static class LineSrfIntersect {

        private static final double EPS_FT = 0.9848;      //  Planar flatness angle tolerance : cos(10 deg)
        private static final int MAXDEPTH = 15;

        private AbstractSurface _thisSrf;
        private Point _L0;
        private Vector<Length> _L0v;
        private Vector<Length> _Ldir;
        private Parameter<Length> _tol;
        private PointString<SubrangePoint> _output;
        private PointString<SubrangePoint> _approxPnts;
        private int _numDims;

        public static LineSrfIntersect newInstance(AbstractSurface thisSrf,
                GeomPoint L0, GeomVector<Length> Ldir, Parameter<Length> tol, PointString<SubrangePoint> output) {

            if (thisSrf instanceof GeomTransform)
                thisSrf = (AbstractSurface)thisSrf.copyToReal();

            //  Create an instance of this class and fill in the class variables.
            LineSrfIntersect o = FACTORY.object();
            o._thisSrf = thisSrf;
            o._L0 = L0.immutable();
            o._L0v = L0.toGeomVector();
            o._Ldir = Ldir.immutable();
            o._tol = tol;
            o._output = output;
            o._approxPnts = PointString.newInstance();
            o._numDims = thisSrf.getPhyDimension();

            return o;
        }

        /**
         * Method that actually carries out the intersection adding the intersection
         * points to the list provided in the constructor.
         *
         * @return A list containing a PointString instance containing zero or more
         *         subrange points, on this surface, made by the intersection of this
         *         surface with the specified line. If no intersections are found an empty
         *         PointString is returned.
         */
        public PointString<SubrangePoint> intersect() {

            //  Use a divide and conquer approach to approximating the intersection points.
            //  This method will add approximate intersection points to "_approxPnts".
            divideAndConquerLine(1, _thisSrf, 0, 0, 1, 1);

            //  Refine each approximate intersection.
            try {
                if (_numDims > 3) {
                    //  Method that works for high dimensions, but is slow.
                    lineSrfIntersectHighDim(_approxPnts);

                } else {
                    //  A much faster method is available for 3D.
                    lineSrfIntersect3D(_approxPnts);
                }
            } catch (RootException err) {
                Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                        "Failed to refine line-Surface intersection.", err);

            } finally {
                PointString.recycle(_approxPnts);
                _approxPnts = null;
            }

            //  Remove any duplicate points.
            for (int i = 0; i < _output.size(); ++i) {
                SubrangePoint pi = _output.get(i);
                for (int j = _output.size() - 1; j >= 0; --j) {
                    if (i != j) {
                        SubrangePoint pj = _output.get(j);
                        if (pi.isApproxEqual(pj, _tol)) {
                            _output.remove(j);
                            --j;
                        }
                    }
                }
            }

            return _output;
        }

        /**
         * Uses a recursive "Divide and Conquer" approach to intersecting a surface patch
         * with a line. On each call, the following happens:
         * <pre>
         *      1) The patch is checked to see if it is approx. a flat plane.
         *          1b) If it is, then a line-plane intersection is performed, the
         *              approximate intersection point added to the _approxPnts list and the method exited.
         *      2) The patch is divided into four quadrant patches.
         *          2b) Each patch is tested to see if it's bounding box is intersected by the line.
         *              If it is, then this method is called with that patch recursively.
         *              Otherwise, the method is exited.
         * </pre>
         * A number of class variables are used to pass information to this recursion:
         * <pre>
         *      _thisSrf The full surfaces intersections are being found for.
         *      _L0   A point at the start of the line being intersected with this surface.
         *      _Ldir A direction vector for the line being intersected with this surface.
         *      _tol  The tolerance to use in determining if the geometry is in tolerance.
         *      _approxPnts A list used to collect the approximate subrange intersection points.
         * </pre>
         *
         * @param patch The surface patch being checked for intersection with the line.
         * @param s0    The "s" parametric position of the start of the patch on the
         *              master "this" surface.
         * @param t0    The "t" parametric position of the start of the patch on the
         *              master "this" surface.
         * @param s1    The parametric "s" position of the end of the patch on the master
         *              "this" surface.
         * @param t1    The parametric "t" position of the end of the patch on the master
         *              "this" surface.
         */
        private void divideAndConquerLine(int depth, Surface patch, double s0, double t0, double s1, double t1) {

            //  Check to see if this patch is nearly planar.
            if (depth > MAXDEPTH || patch.isPlanar(EPS_FT, EPS_FT)) { //  Using a fast method that unfortunately ignores "tol".

                //  Form a single quadrilateral panel from the corners of the patch.
                Point A = patch.getRealPoint(0, 0);
                Point B = patch.getRealPoint(0, 1);
                Point C = patch.getRealPoint(1, 1);
                Point D = patch.getRealPoint(1, 0);

                //  Calculate a quad normal vector.
                Vector<Dimensionless> nhat = GeomUtil.quadNormal(A, B, C, D);

                //  Does the input line intersect this quadrilateral panel?
                boolean intersect = GeomUtil.lineQuadIntersect(_L0, _Ldir, A, B, C, D, nhat, null);
                if (intersect) {
                    //  Store a corner of the patch as the approximate intersection point.
                    _approxPnts.add(_thisSrf.getPoint(s0, t0));
                }

            } else {
                //  Subdivide the patch into 4 quadrants.
                GeomList<Surface> leftRight = patch.splitAtS(0.5);
                GeomList<Surface> botTop = leftRight.get(0).splitAtT(0.5);
                Surface p00 = botTop.get(0);        //  Lower Left
                Surface p01 = botTop.get(1);        //  Top Left
                GeomList.recycle(botTop);
                botTop = leftRight.get(1).splitAtT(0.5);
                Surface p10 = botTop.get(0);        //  Lower Right
                Surface p11 = botTop.get(1);        //  Top Right
                GeomList.recycle(botTop);
                GeomList.recycle(leftRight);

                //  Check for possible intersections on the lower-left patch.
                if (GeomUtil.lineBoundsIntersect(_L0, _Ldir, p00, null)) {
                    //  May be an intersection.
                    double sl = s0;
                    double sh = 0.5 * (s0 + s1);
                    double tl = t0;
                    double th = 0.5 * (t0 + t1);

                    //  Recurse down to a finer level of detail.
                    divideAndConquerLine(depth + 1, p00, sl, tl, sh, th);
                }
                if (p00 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p00);

                //  Check for possible intersections on the lower-right patch.
                if (GeomUtil.lineBoundsIntersect(_L0, _Ldir, p10, null)) {
                    //  May be an intersection.
                    double sl = 0.5 * (s0 + s1);
                    double sh = s1;
                    double tl = t0;
                    double th = 0.5 * (t0 + t1);

                    //  Recurse down to a finer level of detail.
                    divideAndConquerLine(depth + 1, p10, sl, tl, sh, th);
                }
                if (p10 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p10);

                //  Check for possible intersections on the top-left patch.
                if (GeomUtil.lineBoundsIntersect(_L0, _Ldir, p01, null)) {
                    //  May be an intersection.
                    double sl = s0;
                    double sh = 0.5 * (s0 + s1);
                    double tl = 0.5 * (t0 + t1);
                    double th = t1;

                    //  Recurse down to a finer level of detail.
                    divideAndConquerLine(depth + 1, p01, sl, tl, sh, th);
                }
                if (p01 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p01);

                //  Check for possible intersections on the top-right patch.
                if (GeomUtil.lineBoundsIntersect(_L0, _Ldir, p11, null)) {
                    //  May be an intersection.
                    double sl = 0.5 * (s0 + s1);
                    double sh = s1;
                    double tl = 0.5 * (t0 + t1);
                    double th = t1;

                    //  Recurse down to a finer level of detail.
                    divideAndConquerLine(depth + 1, p11, sl, tl, sh, th);
                }
                if (p11 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p11);

            }

        }

        /**
         * This high physical dimension method refines the supplied approximate
         * line-surface intersection to be an accurate intersection point stored in
         * _output.
         *
         * @param approxPnts List of approximate intersection points.
         */
        private void lineSrfIntersectHighDim(PointString<SubrangePoint> approxPnts) throws RootException {

            //  Create a list of parametric position points.
            Point[] parPnts = Point.allocateArray(approxPnts.size());
            int numParPnts = 0;

            StackContext.enter();
            try {
                int size = approxPnts.size();
                for (int i = 0; i < size; ++i) {
                    SubrangePoint aPnt = approxPnts.get(i);

                    //  Get a 1st guess at "u" (parametric distance along line.
                    Vector<Length> W = aPnt.toGeomVector().minus(_L0v);
                    double u0 = _Ldir.dot(W).getValue();

                    //  Create a function that is used to iteratively find the intersection point.
                    LineSrfIntersectNDFun evalFun = LineSrfIntersectNDFun.newInstance(_thisSrf, _L0, _Ldir, aPnt);

                    //  Find the intersection point.
                    double u = Roots.findRoot1D(evalFun, u0 * 0.98, u0 * 1.02, GTOL);

                    //  Turn the parametric position along the line to a point on the surface.
                    Point dQu = Point.valueOf(_Ldir.times(u)).plus(_Ldir.getOrigin());
                    Point Qu = _L0.plus(dQu);
                    double nearS = aPnt.getParPosition().getValue(0);
                    double nearT = aPnt.getParPosition().getValue(1);
                    SubrangePoint Pst = _thisSrf.getClosest(Qu, nearS, nearT, GTOL);

                    //  Store parametric position of intersection for later.
                    Point p = Pst.getParPosition().immutable();
                    parPnts[numParPnts++] = StackContext.outerCopy(p);
                }
            } finally {
                StackContext.exit();
            }

            //  Convert list of parametric positions into subrange points.
            for (int i = 0; i < numParPnts; ++i) {
                Point parPnt = parPnts[i];
                SubrangePoint Pst = _thisSrf.getPoint(parPnt);
                _output.add(Pst);
            }
            
            Point.recycleArray(parPnts);
        }

        /**
         * This 3D method refines the supplied approximate line-surface intersections to
         * be accurate intersection points stored in _output. This method ONLY works if
         * the geometry is 3D.
         *
         * @param approxPnts List of approximate intersection points.
         */
        private void lineSrfIntersect3D(PointString<SubrangePoint> approxPnts) throws RootException {

            //  Create a list of parametric position points.
            Point[] parPnts = Point.allocateArray(approxPnts.size());
            int numParPnts = 0;

            StackContext.enter();
            try {
                //  Re-orient the geometry so that the the intersecting line is aligned with the +Z axis.
                GTransform T = GTransform.newTranslation(_L0v.opposite());      //  Translate so origin of line is at origin.
                Vector zAxis = Vector.valueOf(0, 0, 1);
                T = GTransform.valueOf(_Ldir.toUnitVector(), zAxis).times(T);   //  Align with Z axis.
                AbstractSurface thisSrf = (AbstractSurface)_thisSrf.getTransformed(T).copyToReal();

                //  Create a function that is used to iteratively find the intersection points.
                LineSrfIntersect3DFun evalFun = LineSrfIntersect3DFun.newInstance(thisSrf);

                //  Loop over all the approximate intersections and refine them each.
                double[] x = ArrayFactory.DOUBLES_FACTORY.array(2);
                int size = approxPnts.size();
                for (int i = 0; i < size; ++i) {
                    SubrangePoint aPnt = approxPnts.get(i);

                    //  Get the parametric position of a point near a solution.
                    double s = aPnt.getParPosition().getValue(0);
                    double t = aPnt.getParPosition().getValue(1);
                    x[0] = s;
                    x[1] = t;   //  Guess at intersection point.

                    //  Find the intersection point using an N-dimensional root finder.
                    try {
                        boolean check = Roots.findRootsND(evalFun, x, 2);
                        if (!check) {
                            //  Store the intersection point found.
                            s = x[0];
                            t = x[1];   //  Actual intersection point.
                            s = (s > 1 ? 1 : s < 0 ? 0 : s);
                            t = (t > 1 ? 1 : t < 0 ? 0 : t);

                            //  Store parametric position of intersection for later.
                            Point p = Point.valueOf(s, t);
                            parPnts[numParPnts++] = StackContext.outerCopy(p);
                        }
                    } catch (RootException e) {
                        /* Do nothing: Go on to the next approximate point. */
                    }
                }

            } finally {
                StackContext.exit();
            }

            //  Convert list of parametric positions into subrange points.
            for (int i = 0; i < numParPnts; ++i) {
                Point parPnt = parPnts[i];
                SubrangePoint Pst = _thisSrf.getPoint(parPnt);
                _output.add(Pst);
            }
            
            Point.recycleArray(parPnts);
        }

        public static void recycle(LineSrfIntersect instance) {
            FACTORY.recycle(instance);
        }

        private LineSrfIntersect() { }

        private static final ObjectFactory<LineSrfIntersect> FACTORY = new ObjectFactory<LineSrfIntersect>() {
            @Override
            protected LineSrfIntersect create() {
                return new LineSrfIntersect();
            }

            @Override
            protected void cleanup(LineSrfIntersect obj) {
                obj._thisSrf = null;
                obj._L0 = null;
                obj._L0v = null;
                obj._Ldir = null;
                obj._output = null;
                obj._approxPnts = null;
            }
        };

    }

    /**
     * An Evaluatable1D function that calculates the signed distance between a point along
     * an infinite line and a point on a surface. This is used by a 1D root finder to
     * drive that distance to zero.
     */
    private static class LineSrfIntersectNDFun extends AbstractEvaluatable1D {

        private Surface _thisSrf;
        private Point _L0;
        private Vector _Lu;
        private double _nearS, _nearT;
        private boolean firstPass;

        public static LineSrfIntersectNDFun newInstance(Surface thisSrf, GeomPoint L0, GeomVector Ldir, SubrangePoint aPnt) {

            if (thisSrf instanceof GeomTransform)
                thisSrf = (Surface)thisSrf.copyToReal();

            LineSrfIntersectNDFun o = FACTORY.object();
            o._thisSrf = thisSrf;
            o._L0 = L0.immutable();
            o._Lu = Ldir.immutable();
            o._nearS = aPnt.getParPosition().getValue(0);
            o._nearT = aPnt.getParPosition().getValue(1);
            o.firstPass = true;

            return o;
        }

        /**
         * Function that calculates the signed distance between a point on an infinite
         * line [ Q(u) = L0 + Lu*t ] and the closest point on a parametric surface [
         * P(s,t) ].
         *
         * @param u Parametric distance along the infinite line from L0.
         * @return The signed distance between the point on the line at u and the closest
         *         point on the surface.
         */
        @Override
        public double function(double u) throws RootException {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }

            StackContext.enter();
            try {
                //  Compute the point on the line at u.
                Point dQu = Point.valueOf(_Lu.times(u)).plus(_Lu.getOrigin());
                Point Qu = _L0.plus(dQu);

                //  Find the closest point on the surface near "aPnt".
                SubrangePoint Pst = _thisSrf.getClosest(Qu, _nearS, _nearT, GTOL);

                //  Find the signed distance between Pst and Qu.
                Vector<Length> dP = Qu.toGeomVector().minus(Pst.toGeomVector());
                double dPsign = dP.dot(_Lu).getValue(); //  Projection of dP onto Lu.
                dPsign = dPsign >= 0 ? 1 : -1;

                double d = dP.magValue();                   //  Unsigned distance.
                d = d * dPsign;                             //  Signed distance.

                return d;

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(LineSrfIntersectNDFun instance) {
            FACTORY.recycle(instance);
        }

        private LineSrfIntersectNDFun() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<LineSrfIntersectNDFun> FACTORY = new ObjectFactory<LineSrfIntersectNDFun>() {
            @Override
            protected LineSrfIntersectNDFun create() {
                return new LineSrfIntersectNDFun();
            }

            @Override
            protected void cleanup(LineSrfIntersectNDFun obj) {
                obj._thisSrf = null;
                obj._L0 = null;
                obj._Lu = null;
            }
        };
    }

    /**
     * An Evaluatable1D function that calculates the signed X,Y distance between a point
     * along the Z-axis and a point on a surface. This is used by an ND root finder to
     * drive that distance to zero.
     */
    private static class LineSrfIntersect3DFun implements VectorFunction {

        private Surface _thisSrf;

        public static LineSrfIntersect3DFun newInstance(Surface thisSrf) {

            if (thisSrf instanceof GeomTransform)
                thisSrf = (Surface)thisSrf.copyToReal();

            LineSrfIntersect3DFun o = FACTORY.object();
            o._thisSrf = thisSrf;

            return o;
        }

        /**
         * User supplied method that calculates the function d[X,Y] = fn(x[s,t]). This
         * calculates the distance along the X & Y axes from a point on the surface to the
         * Z-axis.
         *
         * @param n The number of variables in the x & y arrays (should always be 2 for
         *          this problem).
         * @param x Independent parameters to the function (s, t), passed in as input.
         * @param d An existing array that is filled in with the outputs of the function
         */
        @Override
        public void function(int n, double x[], double[] d) throws RootException {
            double s = x[0];
            double t = x[1];

            //  Keep s & t in-bounds.
            double ff = 1;  //  Fudge factor for being out of bounds.
            if (s > 1) {
                ff = s;
                s = 1;
            } else if (s < 0) {
                ff = 1 - s;
                s = 0;
            }
            if (t > 1) {
                ff *= t;
                t = 1;
            } else if (t < 0) {
                ff *= 1 - t;
                t = 0;
            }
            ff *= ff;

            StackContext.enter();
            try {
                
                //  Get the point on the surface.
                Point Pst = _thisSrf.getRealPoint(s, t);

                //  Extract distance to the Z-axis from the surface point.
                d[0] = Pst.getValue(0) * ff;
                d[1] = Pst.getValue(1) * ff;

            } finally {
                StackContext.exit();
            }
        }

        /**
         * User supplied method that calculates the Jacobian of the function.
         *
         * @param n   The number of rows and columns in the Jacobian (always 2 for this
         *            problem).
         * @param jac The Jacobian array to be filled in by this function.
         * @return True if the Jacobian was computed by this method, false if it was not.
         */
        @Override
        public boolean jacobian(int n, double[] x, double[][] jac) {
            return false;
        }

        public static void recycle(LineSrfIntersect3DFun instance) {
            FACTORY.recycle(instance);
        }

        private LineSrfIntersect3DFun() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<LineSrfIntersect3DFun> FACTORY = new ObjectFactory<LineSrfIntersect3DFun>() {
            @Override
            protected LineSrfIntersect3DFun create() {
                return new LineSrfIntersect3DFun();
            }

            @Override
            protected void cleanup(LineSrfIntersect3DFun obj) {
                obj._thisSrf = null;
            }
        };
    }

    /**
     * Return a new list of subrange points that contains those points in the input list
     * that are NOT within the specified tolerance of the specified curve.
     *
     * @param pntLst A list of points subranged onto _thisSrf to be compared to the curve.
     * @param crv    A subrange curve on _thisSrf that is to be compared with the points.
     * @param tol    The tolerance for determining if the input points are close to the
     *               input curve.
     * @return A new list of subrange points that are not near the specified curve.
     */
    private static PointString<SubrangePoint> removePntsNearCurve(PointString<SubrangePoint> pntLst, SubrangeCurve crv, Parameter<Length> tol) {

        PointString<SubrangePoint> output = PointString.newInstance();
        int size = pntLst.size();
        for (int i = 0; i < size; ++i) {
            SubrangePoint p1 = pntLst.get(i);
            SubrangePoint p2 = crv.getClosest(p1.immutable(), GTOL);
            Parameter<Length> d = p1.distance(p2);
            if (!d.isLessThan(tol)) {
                output.add(p1);
            }
            SubrangePoint.recycle(p2);
        }

        return output;
    }

    /**
     * Find the index of the point in the given list that is parametrically closest to the
     * specified point. All points must be subranges to the same surface.
     *
     * @param pnt       The point being compared for nearness.
     * @param pointList The list of points subranged to the same surface as "pnt" for
     *                  which the nearest point is to be found.
     * @return Index in the list of the point that is parametrically nearest to "pnt".
     */
    private static int findParNearestInList(SubrangePoint pnt, List<SubrangePoint> pointList) {
        int idx = -1;
        double minD2 = Double.MAX_VALUE;

        GeomPoint pst = pnt.getParPosition();
        int numPnts = pointList.size();
        for (int i = 0; i < numPnts; ++i) {
            SubrangePoint p2 = pointList.get(i);
            double d2 = pst.distanceSqValue(p2.getParPosition());
            if (d2 < minD2) {
                minD2 = d2;
                idx = i;
            }
        }
        
        return idx;
    }

    /**
     * Create a subrange curve from a string of points subranged onto a surface. The
     * parametric curve will be a cubic curve passing through the parametric positions of
     * all the input subrange points.
     *
     * @param tracedPnts A list of points all subranged onto the same surface.
     * @return A subrange curve on the surface that passes through the list of subrange
     *         points.
     * @throws IllegalArgumentException
     */
    private static SubrangeCurve makeSubrangeCurve(PointString<SubrangePoint> tracedPnts) throws IllegalArgumentException {
        if (tracedPnts.size() < 2)
            return null;

        //  Get the surface that the points are subranged onto.
        Surface srf = (Surface)tracedPnts.get(0).getChild();

        int degree = 3;
        BasicNurbsCurve pcrv;
        StackContext.enter();
        try {
            //  Extract the parametric positions of the points.
            PointString<Point> parStr = PointString.newInstance();
            for (SubrangePoint sp : tracedPnts) {
                Point par = sp.getParPosition().immutable();
                parStr.add(par);
            }
            
            //  Thin out any positions that are to close together.
            Parameter ptol2 = Parameter.valueOf(SQRT_EPS, SI.METER).pow(2);
            int size = parStr.size();
            if (size > 2 && parStr.get(0).distanceSq(parStr.get(1)).isLessThan(ptol2)) {
                parStr.remove(1);
                --size;
            }
            Point lastPar = parStr.get(-1);
            for (int i=size-2; i > 0; --i) {
                Point par = parStr.get(i);
                if (par.distanceSq(lastPar).isLessThan(ptol2))
                    parStr.remove(i);
                lastPar = par;
            }
            if (parStr.size() <= degree)
                degree = parStr.size() - 1;

            //  Create a parametric curve for the traced points.
            try {
                pcrv = CurveFactory.fitPoints(degree, parStr);
            } catch (SingularMatrixException e) {
                //  Fall back on just using a straight line segment.
                pcrv = CurveFactory.createLine(parStr.get(0), parStr.get(-1));
            }

            //  Make sure this parameter curve is in-bounds.
            Point pcrvMin = pcrv.getBoundsMin();
            Point pcrvMax = pcrv.getBoundsMax();
            while (pcrvMin.getValue(0) < 0 || pcrvMin.getValue(1) < 0
                    || pcrvMax.getValue(0) > 1 || pcrvMax.getValue(1) > 1) {
                //  Lower the degreeU and try again.
                --degree;
                if (degree == 0) {
                    // Just connect the end points with a line.
                    pcrv = CurveFactory.createLine(parStr.get(0), parStr.get(-1));
                    break;
                }
                pcrv = CurveFactory.fitPoints(degree, parStr);

                pcrvMin = pcrv.getBoundsMin();
                pcrvMax = pcrv.getBoundsMax();
            }

            //  Remove redundant knotsU to within a fine tolerance.
            pcrv = CurveUtils.thinKnotsToTolerance(pcrv, GTOLP);

            pcrv = StackContext.outerCopy(pcrv);

        } finally {
            StackContext.exit();
        }

        //  Create the subrange curve.
        SubrangeCurve scrv = SubrangeCurve.newInstance(srf, pcrv);

        return scrv;
    }

    /**
     * A class used to intersect an infinite plane and the specified surface.
     */
    private static class PlaneSrfIntersect {

        private static final double GTOL100 = GTOL * 100;   //  A coarser version of GTOL.
        private static final double EPS_FT = 0.866;          //  Planar flatness angle tolerance : cos(30 deg)
        private static final double DTHETA = 0.349;         //  Tracing step size angle:  DTheta = 20 deg = 0.349 rad
        private static final int MAXDEPTH = 15;

        private AbstractSurface _thisSrf;
        private GeomPlane _plane;
        private Parameter<Length> _tol;
        private Parameter<Length> _patchSizeTol;
        private Parameter<Length> _eps1;
        private Parameter<Length> _eps2;
        private Parameter<Length> _epsCRT;              //  Curve refinement tolerance.
        private GeomList<SubrangeCurve> _output;
        private PointString<SubrangePoint> _appInteriorPnts;

        public static PlaneSrfIntersect newInstance(AbstractSurface thisSrf,
                GeomPlane plane, Parameter<Length> tol, GeomList<SubrangeCurve> output) {

            if (thisSrf instanceof GeomTransform)
                thisSrf = (AbstractSurface)thisSrf.copyToReal();
            if (plane instanceof GeomTransform)
                plane = plane.copyToReal();

            Parameter<Length> thisSpan = thisSrf.getBoundsMax().distance(thisSrf.getBoundsMin());

            //  Create an instance of this class and fill in the class variables.
            PlaneSrfIntersect o = FACTORY.object();
            o._thisSrf = thisSrf;
            o._plane = plane;
            o._tol = tol;
            o._patchSizeTol = tol.times(10);
            o._eps1 = thisSpan.divide(5000);
            o._eps2 = o._eps1.times(2);
            o._epsCRT = thisSpan.divide(50);
            o._output = output;
            o._appInteriorPnts = PointString.newInstance();

            /*
             System.out.println("tol = " + tol);
             System.out.println("eps1 = " + o._eps1);
             System.out.println("eps2 = " + o._eps2);
             System.out.println("epsCRT = " + o._epsCRT);
             */
            return o;
        }

        /**
         * Method that actually carries out the intersection adding the intersection
         * points to the list provided in the constructor.
         *
         * @return A list containing zero or more subrange curves, on this surface, made
         *         by the intersection of this surface with the specified plane. If no
         *         intersections are found an empty GeomList is returned.
         */
        public GeomList<SubrangeCurve> intersect() throws RootException {

            //  This algorithm is inspired by the Barnhill-Kersey surface-surface intersection
            //  algorithm, but with some fairly major differences.
            //      Barnhill, R.E., and Kersey, S.N., "Marching Method for Parametric Surface/Surface Intersection," CAGD, 7(1-4), June 1990, 257-280.
            //      as discussed in:  Max K. Agoston, Computer Graphics and Geometric Modelling: Implementation & Algorithms
            
            //  The high level outline of the algorithm as implemented is as follows:
            //      1 - Find edge curve intersections to get initial starting points for intersection tracing.
            //      2 - Trace intersections that start at edge points.
            //      3 - Use a divide & conquer approach to finding a spattering of interior intersection points.
            //      4 - Remove any interior points that are associated with the intersection curves already found.
            //      5 - Trace intersections starting at interior points removing any interior points that
            //          are associated with these intersections.
            
            //  Get any edge intersection points.
            //System.out.println("Extracting edge points...");
            PointString<SubrangePoint> edgePoints = getEdgePoints(_thisSrf, _plane);

            //  Begin tracing from the edge points.
            //System.out.println("Tracing from edge points...");
            GeomList<SubrangeCurve> crvSegs = traceEdgePoints(edgePoints, _plane);
            _output.addAll(crvSegs);

            //  Use a divide and conquer approach to find approximate interior intersection points.
            //System.out.println("Finding interior points...");
            divideAndConquerPlane(1, _thisSrf, 0, 0, 1, 1);

            //  Refine each approximate intersection and save as a potential start point
            //  for an internal loop.
            PointString<SubrangePoint> potStartPoints = PointString.newInstance();
            int size = _appInteriorPnts.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint aPnt = _appInteriorPnts.get(i);
                SubrangePoint rpnt = relaxPoint(aPnt, _plane, _tol);
                if (nonNull(rpnt))
                    potStartPoints.add(rpnt);
            }

            //  Remove any points that are near the existing intersection curves.
            //System.out.println("Removing redundant interior points...");
            removeClosePoints(potStartPoints, _tol);
            Parameter<Length> tol2 = _tol.times(100);
            size = _output.size();
            for (int i = 0; i < size; ++i) {
                SubrangeCurve crv = _output.get(i);
                potStartPoints = removePntsNearCurve(potStartPoints, crv, tol2);
            }

            //  Any remaining interior points are assumed to be on interior loops.
            //System.out.println("Tracing from interior points...");
            crvSegs = traceInteriorPoints(potStartPoints, _plane, _patchSizeTol);
            _output.addAll(crvSegs);

            return _output;
        }

        /**
         * Trace intersection curves that start/end at the edges of the surface.
         *
         * @param edgePoints The surface edge points to use as starting points for tracing
         *                   intersection curves.
         * @param plane      The plane being used for intersecting with the surface.
         * @return A list of intersection curves traced from/to each edge point.
         * @throws RootException If there is a convergence problem with a root finder.
         */
        private GeomList<SubrangeCurve> traceEdgePoints(PointString<SubrangePoint> edgePoints,
                GeomPlane plane) throws RootException {

            GeomList<SubrangeCurve> crvSegs = GeomList.newInstance();
            int numPnts = edgePoints.size();

            while (numPnts > 0) {
                SubrangePoint startPnt = edgePoints.get(0);

                //  Trace the intersection starting at "startPnt".
                SubrangeCurve seg = traceSegment(startPnt, plane);
                if (nonNull(seg)) {
                    //  Don't keep degenerate curves (points).
                    if (!seg.isDegenerate(_tol))
                        crvSegs.add(seg);

                    //  Remove the end point from the list.
                    Surface srf = (Surface)startPnt.getChild();
                    SubrangePoint endPoint = srf.getPoint(seg.getParPosition().getRealPoint(1));
                    int idx = findParNearestInList(endPoint, edgePoints);
                    if (idx > 0) {
                        edgePoints.remove(idx);
                        --numPnts;
                    } else {
                        Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                "End point not found.");
                    }
                }

                //  Remove the start point from the list.
                edgePoints.remove(0);
                --numPnts;
            }

            return crvSegs;
        }

        /**
         * Trace intersection curves that start/end on interior points.
         *
         * @param potStartPoints A list of potential start points for intersection
         *                       tracing.
         * @param plane          The plane used to intersect the surface.
         * @param tol            The tolerance to use when deciding if a point belongs to
         *                       an existing intersection curve.
         * @return A list of intersection curves traced from/to the interior points
         *         provided.
         * @throws RootException If there is a convergence problem with a root finder.
         */
        private GeomList<SubrangeCurve> traceInteriorPoints(PointString<SubrangePoint> potStartPoints, GeomPlane plane, Parameter<Length> tol) throws RootException {

            GeomList<SubrangeCurve> crvSegs = GeomList.newInstance();

            while (potStartPoints.size() > 0) {
                SubrangePoint startPnt = potStartPoints.get(0);

                //  Trace the intersection starting at "startPnt".
                SubrangeCurve seg = traceSegment(startPnt, plane);
                if (nonNull(seg)) {
                    //  Don't keep degenerate curves (points).
                    if (!seg.isDegenerate(_tol))
                        crvSegs.add(seg);

                    //  Remove any points that are close to this curve segment.
                    SubrangePoint p = potStartPoints.remove(0); //  The start point obviously.
                    potStartPoints = removePntsNearCurve(potStartPoints, seg, tol);

                } else {
                    SubrangePoint p = potStartPoints.remove(0);
                }
            }

            return crvSegs;
        }

        /**
         * Method that finds all the exact intersections of the edges of the surface (if
         * any).
         */
        private PointString<SubrangePoint> getEdgePoints(Surface srf, GeomPlane plane) {

            PointString<SubrangePoint> output = PointString.newInstance();

            PointString<SubrangePoint> edge = srf.getS0Curve().intersect(plane, GTOL);
            int size = edge.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edge.get(i);
                double t = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf.getPoint(0, t);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge);
            edge = srf.getT0Curve().intersect(plane, GTOL);
            size = edge.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edge.get(i);
                double s = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf.getPoint(s, 0);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge);
            edge = srf.getS1Curve().intersect(plane, GTOL);
            size = edge.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edge.get(i);
                double t = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf.getPoint(1, t);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge);
            edge = srf.getT1Curve().intersect(plane, GTOL);
            size = edge.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edge.get(i);
                double s = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf.getPoint(s, 1);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge);

            //  Remove any redundant points
            removeClosePoints(output, _tol);

            return output;
        }

        /**
         * Uses a recursive "Divide and Conquer" approach to intersecting a surface patch
         * with a plane. On each call, the following happens:
         * <pre>
         *      1) The patch is checked to see if it is approx. a flat plane.
         *          1b) If it is, then a plane-plane intersection is performed, the
         *              approximate intersection points added to the _approxPnts list and the method exited.
         *      2) The patch is divided into four quadrant patches.
         *          2b) Each patch is tested to see if it's bounding box is intersected by the plane.
         *              If it is, then this method is called with that patch recursively.
         *              Otherwise, the method is exited.
         * </pre>
         * A number of class variables are used to pass information to this recursion:
         * <pre>
         *      _thisSrf The full surfaces intersections are being found for.
         *      _plane   The plane being intersected with this surface.
         *      _tol  The tolerance to use in determining if the geometry is in tolerance.
         *      _appInteriorPnts A list used to collect the approximate subrange intersection points.
         * </pre>
         *
         * @param patch The surface patch being checked for intersection with the plane.
         * @param s0    The "s" parametric position of the start of the patch on the
         *              master "this" surface.
         * @param t0    The "t" parametric position of the start of the patch on the
         *              master "this" surface.
         * @param s1    The parametric "s" position of the end of the patch on the master
         *              "this" surface.
         * @param t1    The parametric "t" position of the end of the patch on the master
         *              "this" surface.
         */
        private void divideAndConquerPlane(int depth, Surface patch, double s0, double t0, double s1, double t1) {

            //  Check to see if this patch is nearly planar.
            //  Using a fast method for "isPlanar()" that unfortunately ignores "tol".
            if (depth > MAXDEPTH || patch.isPlanar(EPS_FT, EPS_FT) || isSmall(patch, _patchSizeTol)) {

                //  Form a single quadrilateral panel from the corners of the patch.
                Point A = patch.getRealPoint(0, 0);
                Point B = patch.getRealPoint(1, 0);
                Point C = patch.getRealPoint(1, 1);
                Point D = patch.getRealPoint(0, 1);

                //  Determine if any edge of the quadrilateral polygon intersects with the plane.
                GeomPoint P0 = _plane.getRefPoint();
                GeomVector<Dimensionless> nhat = _plane.getNormal();
                boolean intersects = GeomUtil.planeQuadIntersect(P0, nhat, A, B, C, D);

                //  Store off the approximate intersection point (panel center).
                if (intersects) {
                    double s = segmentPos2Parent(0.5, s0, s1);
                    double t = segmentPos2Parent(0.5, t0, t1);
                    SubrangePoint centerPnt = _thisSrf.getPoint(s, t);
                    _appInteriorPnts.add(centerPnt);
                }

            } else {
                //  Subdivide the patch into 4 quadrants.
                GeomList<Surface> leftRight = patch.splitAtS(0.5);
                GeomList<Surface> botTop = leftRight.get(0).splitAtT(0.5);
                Surface p00 = botTop.get(0);        //  Lower Left
                Surface p01 = botTop.get(1);        //  Top Left
                GeomList.recycle(botTop);
                botTop = leftRight.get(1).splitAtT(0.5);
                Surface p10 = botTop.get(0);        //  Lower Right
                Surface p11 = botTop.get(1);        //  Top Right
                GeomList.recycle(botTop);
                GeomList.recycle(leftRight);

                //  Check for possible intersections on the lower-left patch.
                if (GeomUtil.planeBoundsIntersect(_plane, p00)) {
                    //  May be an intersection.
                    double sl = s0;
                    double sh = 0.5 * (s0 + s1);
                    double tl = t0;
                    double th = 0.5 * (t0 + t1);

                    //  Recurse down to a finer level of detail.
                    divideAndConquerPlane(depth + 1, p00, sl, tl, sh, th);
                }
                if (p00 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p00);

                //  Check for possible intersections on the lower-right patch.
                if (GeomUtil.planeBoundsIntersect(_plane, p10)) {
                    //  May be an intersection.
                    double sl = 0.5 * (s0 + s1);
                    double sh = s1;
                    double tl = t0;
                    double th = 0.5 * (t0 + t1);

                    //  Recurse down to a finer level of detail.
                    divideAndConquerPlane(depth + 1, p10, sl, tl, sh, th);
                }
                if (p10 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p10);

                //  Check for possible intersections on the top-left patch.
                if (GeomUtil.planeBoundsIntersect(_plane, p01)) {
                    //  May be an intersection.
                    double sl = s0;
                    double sh = 0.5 * (s0 + s1);
                    double tl = 0.5 * (t0 + t1);
                    double th = t1;

                    //  Recurse down to a finer level of detail.
                    divideAndConquerPlane(depth + 1, p01, sl, tl, sh, th);
                }
                if (p01 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p01);

                //  Check for possible intersections on the top-right patch.
                if (GeomUtil.planeBoundsIntersect(_plane, p11)) {
                    //  May be an intersection.
                    double sl = 0.5 * (s0 + s1);
                    double sh = s1;
                    double tl = 0.5 * (t0 + t1);
                    double th = t1;

                    //  Recurse down to a finer level of detail.
                    divideAndConquerPlane(depth + 1, p11, sl, tl, sh, th);
                }
                if (p11 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p11);

            }

        }

        private static final int MAXIT = 500;

        /**
         * Relax the approximate intersection point toward the exact intersection until it
         * is within "tol" of the exact intersection.
         *
         * @param approx The approximate surface intersection point being refined or
         *               relaxed.
         * @param plane  The intersecting plane.
         * @param tol    THe tolerance required of the intersection points.
         * @return The input point relaxed onto the intersection between the plane and the
         *         surface to within the tolerance "tol". If the plane is locally tangent
         *         to the surface near the intersection, this may return
         *         <code>null</code>.
         */
        private SubrangePoint relaxPoint(SubrangePoint approx, GeomPlane plane,
                Parameter<Length> tol) throws RootException {

            AbstractSurface srf = (AbstractSurface)approx.getChild();

            double sOut, tOut;
            StackContext.enter();
            try {
                int numDims = srf.getPhyDimension();
                SubrangePoint p = approx;
                Parameter<Length> d = GeomUtil.pointPlaneDistance(p, plane.getRefPoint(), plane.getNormal());
                if (!d.abs().isLessThan(tol)) {

                    Unit<Length> unit = srf.getUnit();
                    MutableVector Lu = MutableVector.newInstance(numDims, Dimensionless.UNIT);
                    MutablePoint L0 = MutablePoint.newInstance(numDims, unit);

                    int iteration = 0;
                    do {
                        //  Get the local tangent plane on the surface.
                        GeomPoint st = p.getParPosition();
                        GeomPlane Ts = srf.getTangentPlane(st);

                        //  Intersect the input plane with the surface tangent plane.
                        IntersectType type = GeomUtil.planePlaneIntersect(plane, Ts, L0, Lu);
                        if (type != IntersectType.INTERSECT) {
                            return null;
                        }

                        //  Find the closest point on the plane intersection line to the previous point.
                        Point pip1 = GeomUtil.pointLineClosest(p, L0, Lu);

                        //  Re-attach this point to the surface.
                        p = srf.getClosestFarthestInterior(pip1, st.getValue(0), st.getValue(1), GTOL100, true);

                        //  Update the distance between this new point and the intersecting plane.
                        d = GeomUtil.pointPlaneDistance(p, plane.getRefPoint(), plane.getNormal());
                        ++iteration;

                    } while (!d.abs().isLessThan(tol) && iteration < MAXIT);

                    if (iteration == MAXIT)
                        Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                "Convergence problem in PlaneSrfIntersect.relaxPoint().");
                }

                //  Copy parametric position of final point to outer context.
                sOut = p.getParPosition().getValue(0);
                tOut = p.getParPosition().getValue(1);

            } finally {
                StackContext.exit();
            }

            return srf.getPoint(sOut, tOut);
        }

        /**
         * Method that traces an intersection segment from the given start point until an
         * edge of the surface is encountered or the start point is re-encountered.
         *
         * @param startPnt The point on the surface to start tracing the intersection
         *                 from.
         * @param plane    The plane that is being intersected with the surface.
         * @return A subrange curve representing this intersection segment.
         */
        private SubrangeCurve traceSegment(SubrangePoint startPnt, GeomPlane plane) throws RootException {
            AbstractSurface srf = (AbstractSurface)startPnt.getChild();

            BasicNurbsCurve pCrv;
            StackContext.enter();
            try {
                PointString<SubrangePoint> tracedPnts = PointString.newInstance();

                Unit<Length> unit = srf.getUnit();
                GeomVector<Dimensionless> n2 = plane.getNormal();
                SubrangePoint p1 = startPnt;
                tracedPnts.add(p1);
                GeomPoint st = p1.getParPosition();     //  Parametric position on the surface of the point p1.
                double s0 = st.getValue(0);
                double t0 = st.getValue(1);

                //  Is the start point an edge point?
                boolean startEdge = false;
                if (parNearEnds(s0, GTOL100) || parNearEnds(t0, GTOL100)) {
                    startEdge = true;
                }

                boolean firstPoint = true;
                int collapsedEdge = -1;
                while (tracedPnts.size() < 1000) {
                    //  Determine the step direction.
                    Vector<Dimensionless> pu = srf.getSDerivative(st.getValue(0), st.getValue(1), 1, false).toUnitVector();
                    if (!pu.isValid()) {
                        //  Step away from a collapsed edge.
                        double t = st.getValue(1);
                        if (t < 0.5) {
                            st = Point.valueOf(SI.METER, 0.5, GTOL100);
                            collapsedEdge = 2;
                        } else {
                            st = Point.valueOf(SI.METER, 0.5, 1. - GTOL100);
                            collapsedEdge = 3;
                        }
                        if (startEdge) {
                            p1 = srf.getPoint(st);
                            s0 = st.getValue(0);
                            t0 = st.getValue(1);
                        }
                        pu = srf.getSDerivative(st.getValue(0), st.getValue(1), 1, false).toUnitVector();
                    }
                    Vector<Dimensionless> pv = srf.getTDerivative(st.getValue(0), st.getValue(1), 1, false).toUnitVector();
                    if (!pv.isValid()) {
                        //  Step away from a collapsed edge.
                        double s = st.getValue(0);
                        if (s < 0.5) {
                            st = Point.valueOf(SI.METER, GTOL100, 0.5);
                            collapsedEdge = 0;
                        } else {
                            st = Point.valueOf(SI.METER, 1. - GTOL100, 0.5);
                            collapsedEdge = 1;
                        }
                        pu = pv = srf.getTDerivative(st.getValue(0), st.getValue(1), 1, false).toUnitVector();
                        if (startEdge) {
                            p1 = srf.getPoint(st);
                            s0 = st.getValue(0);
                            t0 = st.getValue(1);
                        }
                    }
                    Parameter pudotn2 = pu.dot(n2);
                    Parameter pvdotn2 = pv.dot(n2);
                    if (pudotn2.isApproxZero() && pvdotn2.isApproxZero()) {
                        Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                "Surface is tangent to plane in traceSegment().");
                        break;      //  Surface is nearly tangent to plane.  TODO:  Handle this better.
                    }
                    //  Ti = pv*(pu dot n2) - pu*(pv dot n2)
                    Vector<Dimensionless> Ti = pv.times(pudotn2).minus((Vector)pu.times(pvdotn2));
                    Ti = Ti.toUnitVector();

                    //  Determine the step size "L".
                    Point p = p1.plus(Point.valueOf(Ti.times(_eps1)));
                    SubrangePoint x = srf.getClosestFarthestInterior(p, st.getValue(0), st.getValue(1), GTOL, true);

                    //  If "Ti" initially steps us off the edge of the surface, reverse it.
                    if (startEdge) {
                        startEdge = false;
                        double sx = x.getParPosition().getValue(0);
                        double tx = x.getParPosition().getValue(1);
                        if (sx < GTOL || sx >= (1 - GTOL) || tx < GTOL || tx > (1 - GTOL)
                                || abs(sx - s0) > 0.3 || abs(tx - t0) > 0.3) {
                            //  Ti stepped off the edge.
                            Ti = Ti.opposite();
                            p = p1.plus(Point.valueOf(Ti.times(_eps1)));
                            x = srf.getClosestFarthestInterior(p, st.getValue(0), st.getValue(1), GTOL100, true);

                            //  Reverse the intersecting plane normal so this doesn't happen again.
                            n2 = n2.opposite();
                        }
                    }

                    x = relaxPoint(x, plane, _tol);
                    if (isNull(x))
                        break;
                    p = p1.plus(Point.valueOf(Ti.times(_eps2)));
                    SubrangePoint y = srf.getClosestFarthestInterior(p, st.getValue(0), st.getValue(1), GTOL100, true);
                    y = relaxPoint(y, plane, _tol);
                    if (isNull(y))
                        break;

                    //  If the 1st point was on a collapsed edge, go back and fix things up.
                    if (collapsedEdge >= 0) {
                        st = x.getParPosition();
                        switch (collapsedEdge) {
                            case 0:
                                st = Point.valueOf(SI.METER, 0, st.getValue(1));
                                break;
                            case 1:
                                st = Point.valueOf(SI.METER, 1., st.getValue(1));
                                break;
                            case 2:
                                st = Point.valueOf(SI.METER, st.getValue(0), 0);
                                break;
                            case 3:
                                st = Point.valueOf(SI.METER, st.getValue(0), 1.);
                                break;
                        }
                        p1 = srf.getPoint(st);
                        startPnt = p1;
                        tracedPnts.clear();
                        tracedPnts.add(p1);
                        s0 = st.getValue(0);
                        t0 = st.getValue(1);

                        collapsedEdge = -1;
                    }

                    Vector<Length> a = x.minus(p1).toGeomVector();
                    Vector<Length> b = y.minus(p1).toGeomVector();

                    double na = a.normValue();
                    double nb = b.normValue();
                    double namb = a.minus(b).normValue();
                    double naxb = a.cross(b).normValue();

                    Parameter<Length> L;
                    if (naxb > 1e-9) {
                        //  The 3 points are not co-linear.
                        //  r = |a||b||a - b|/(2*|a x b|)
                        double r = 0.5 * na * nb * namb / naxb;
                        L = Parameter.valueOf(r * DTHETA, unit);
                        //System.out.print("L = " + L);
                        if (L.isGreaterThan(_epsCRT))
                            L = _epsCRT;
                    } else {
                        L = _epsCRT;
                    }

                    //  Calculate the new intersection point location.
                    p = p1.plus(Point.valueOf(Ti.times(L)));
                    SubrangePoint p2 = srf.getClosestFarthestInterior(p, st.getValue(0), st.getValue(1), GTOL100, true);
                    p2 = relaxPoint(p2, plane, _tol);
                    if (isNull(p2))
                        break;
                    //System.out.println(", d = " + p2.distance(p));

                    //  Store the new intersection point.
                    tracedPnts.add(p2);

                    //  Is this new point an edge point?
                    st = p2.getParPosition();
                    double s = st.getValue(0);
                    double t = st.getValue(1);
                    if (!firstPoint && (parNearEnds(s, GTOL100) || parNearEnds(t, GTOL100)))
                        break;

                    //  Is this new point near the start point?
                    if (!firstPoint && (st.distanceValue(startPnt.getParPosition()) < 0.1
                            && p2.distance(startPnt).isLessThan(L.times(0.75)))) {
                        tracedPnts.add(startPnt);     //  Repeat the 1st point.
                        break;
                    }

                    p1 = p2;
                    firstPoint = false;
                }

                //  Create the curve segment.
                if (tracedPnts.size() == 1) {
                    st = tracedPnts.get(0).getParPosition();
                    pCrv = StackContext.outerCopy(CurveFactory.createPoint(1, st));

                } else {
                    //  Create the intersection curve from the intersection points.
                    SubrangeCurve scrv = makeSubrangeCurve(tracedPnts);
                    pCrv = StackContext.outerCopy((BasicNurbsCurve)scrv.getParPosition());
                }

            } finally {
                StackContext.exit();
            }

            return SubrangeCurve.newInstance(srf, pCrv);
        }

        public static void recycle(PlaneSrfIntersect instance) {
            FACTORY.recycle(instance);
        }

        private PlaneSrfIntersect() { }

        private static final ObjectFactory<PlaneSrfIntersect> FACTORY = new ObjectFactory<PlaneSrfIntersect>() {
            @Override
            protected PlaneSrfIntersect create() {
                return new PlaneSrfIntersect();
            }

            @Override
            protected void cleanup(PlaneSrfIntersect obj) {
                obj._thisSrf = null;
                obj._plane = null;
                obj._output = null;
                obj._appInteriorPnts = null;
                obj._tol = null;
                obj._patchSizeTol = null;
                obj._eps1 = null;
                obj._eps2 = null;
                obj._epsCRT = null;
            }
        };

    }

    /**
     * A class used to intersect two arbitrary surfaces.
     */
    private static class SrfSrfIntersect {

        private static final double GTOL100 = GTOL * 100;   //  A coarser version of GTOL.
        private static final double EPS_FT = 0.866;          //  Planar flatness angle tolerance : cos(30 deg)
        private static final double DTHETA = 0.1745;        //  Tracing step size angle:  DTheta = 10 deg = 0.1745 rad
        private static final int MAXDEPTH = 10;

        private AbstractSurface _thisSrf;
        private AbstractSurface _thatSrf;
        private Parameter<Length> _tol;
        private Parameter<Length> _patchSizeTol;
        private Parameter<Length> _eps1;
        private Parameter<Length> _eps2;
        private Parameter<Length> _epsCRT;              //  Curve refinement tolerance.
        private GeomList<SubrangeCurve> _thisOut;
        private GeomList<SubrangeCurve> _thatOut;
        private PointString<SubrangePoint> _appInteriorPnts;

        public static SrfSrfIntersect newInstance(AbstractSurface thisSrf,
                AbstractSurface thatSrf, Parameter<Length> tol,
                GeomList<SubrangeCurve> thisOut, GeomList<SubrangeCurve> thatOut) {

            if (thisSrf instanceof GeomTransform)
                thisSrf = (AbstractSurface)thisSrf.copyToReal();
            if (thatSrf instanceof GeomTransform)
                thatSrf = (AbstractSurface)thatSrf.copyToReal();

            Parameter<Length> thisSpan = thisSrf.getBoundsMax().distance(thisSrf.getBoundsMin());
            Parameter<Length> thatSpan = thatSrf.getBoundsMax().distance(thatSrf.getBoundsMin());
            Parameter<Length> minSpan = thisSpan.min(thatSpan);

            //  Create an instance of this class and fill in the class variables.
            SrfSrfIntersect o = FACTORY.object();
            o._thisSrf = thisSrf;
            o._thatSrf = thatSrf;
            o._tol = tol;
            o._patchSizeTol = tol.times(10);
            o._eps1 = minSpan.divide(5000);
            o._eps2 = o._eps1.times(2);
            o._epsCRT = minSpan.divide(100);
            o._thisOut = thisOut;
            o._thatOut = thatOut;
            o._appInteriorPnts = PointString.newInstance();

            /*
             System.out.println("tol = " + tol);
             System.out.println("eps1 = " + o._eps1);
             System.out.println("eps2 = " + o._eps2);
             System.out.println("epsCRT = " + o._epsCRT);
             */
            return o;
        }

        /**
         * Method that actually carries out the intersection adding the intersection
         * points to the lists provided in the constructor.
         */
        public void intersect() throws RootException {

            //  This algorithm is inspired by the Barnhill-Kersey surface-surface intersection
            //  algorithm, but with some fairly major differences.
            //      Barnhill, R.E., and Kersey, S.N., "A Marching Method for Parametric Surface/Surface Intersection," CAGD, 7(1-4), June 1990, 257-280.
            //      as discussed in:  Max K. Agoston, Computer Graphics and Geometric Modelling: Implementation & Algorithms
            
            //  The high level outline of the algorithm as implemented is as follows:
            //      1 - Find edge curve intersections to get initial starting points for intersection tracing.
            //      2 - Trace intersections that start at edge points.
            //      3 - Use a divide & conquer approach to finding a spattering of interior intersection points.
            //      4 - Remove any interior points that are associated with the intersection curves already found.
            //      5 - Trace intersections starting at interior points removing any interior points that
            //          are associated with these intersections.
            
            //  Get any edge intersection points.
            //System.out.println("Extracting edge points...");
            PointString<SubrangePoint> thisEdgePnts = getEdgePoints(_thisSrf, _thatSrf);
            PointString<SubrangePoint> thatEdgePnts = getEdgePoints(_thatSrf, _thisSrf);

            //  Begin tracing from the edge points.
            //System.out.println("Tracing from edge points...");
            //System.out.println("    This surface...");
            GeomList<GeomList<SubrangeCurve>> crvSegs = traceEdgePoints(thisEdgePnts, thatEdgePnts, _thatSrf);
            _thisOut.addAll(crvSegs.get(0));
            _thatOut.addAll(crvSegs.get(1));

            //  Remove any points that are near the existing intersection curves.
            //System.out.println("        Removing redundant interior points...");
            Parameter<Length> tol2 = _tol.times(100);
            for (SubrangeCurve crv : _thatOut) {
                PointString<SubrangePoint> newLst = removePntsNearCurve(thatEdgePnts, crv, tol2);
                thatEdgePnts = newLst;
            }

            //System.out.println("    That surface...");
            crvSegs = traceEdgePoints(thatEdgePnts, thisEdgePnts, _thisSrf);
            _thatOut.addAll(crvSegs.get(0));
            _thisOut.addAll(crvSegs.get(1));

            //  Use a divide and conquer approach to find approximate interior intersection points.
            //System.out.println("Finding interior points...");
            divideAndConquerSrf(1, _thisSrf, 0, 0, 1, 1, _thatSrf, 0, 0, 1, 1);
            //System.out.println("   interior points found = " + _appInteriorPnts.size());

            //  Refine each approximate intersection and save as a potential start point
            //  for an internal loop.
            PointString<SubrangePoint> thisStartPoints = PointString.newInstance();
            PointString<SubrangePoint> thatStartPoints = PointString.newInstance();
            PointString<SubrangePoint> pntLst = PointString.newInstance();
            for (SubrangePoint thisPnt : _appInteriorPnts) {
                SubrangePoint thatPnt = _thatSrf.getClosest(thisPnt, GTOL100);
                relaxPoint(thisPnt, _thatSrf, thatPnt.getParPosition(), pntLst, _tol);
                if (pntLst.size() > 0) {
                    thisStartPoints.add(pntLst.getFirst());
                    thatStartPoints.add(pntLst.getLast());
                }
                pntLst.clear();
                SubrangePoint.recycle(thatPnt);
            }

            //  Remove any points that are near the existing intersection curves.
            //System.out.println("Removing redundant interior points...");
            removeClosePoints(thisStartPoints, _tol);
            removeClosePoints(thatStartPoints, _tol);
            for (SubrangeCurve crv : _thisOut) {
                thisStartPoints = removePntsNearCurve(thisStartPoints, crv, tol2);
                thatStartPoints = removePntsNearCurve(thatStartPoints, crv, tol2);
            }
            for (SubrangeCurve crv : _thatOut) {
                thisStartPoints = removePntsNearCurve(thisStartPoints, crv, tol2);
                thatStartPoints = removePntsNearCurve(thatStartPoints, crv, tol2);
            }

            //  Any remaining interior points are assumed to be on interior loops.
            //System.out.println("Tracing from interior points...");
            //System.out.println("    This surface...");
            //System.out.println("        thisStartPoints.size() = " + thisStartPoints.size());
            crvSegs = traceInteriorPoints(thisStartPoints, _thatSrf, _patchSizeTol);
            _thisOut.addAll(crvSegs.get(0));
            _thatOut.addAll(crvSegs.get(1));

            //  Remove any points near the existing intersection curves.
            GeomList<SubrangeCurve> segLst = crvSegs.getFirst();
            for (SubrangeCurve seg : segLst) {
                thatStartPoints = removePntsNearCurve(thatStartPoints, seg, tol2);
            }
            segLst = crvSegs.getLast();
            for (SubrangeCurve seg : segLst) {
                thatStartPoints = removePntsNearCurve(thatStartPoints, seg, tol2);
            }

            //System.out.println("    That surface...");
            //System.out.println("        thatStartPoints.size() = " + thatStartPoints.size());
            crvSegs = traceInteriorPoints(thatStartPoints, _thisSrf, _patchSizeTol);
            _thatOut.addAll(crvSegs.get(0));
            _thisOut.addAll(crvSegs.get(1));

        }

        /**
         * Trace intersection curves that start/end on interior points.
         *
         * @param thisStartPnts A list of potential start points on "thisSrf" for
         *                      intersection tracing.
         * @param thatSrf       "That" surface being used for intersecting with "this"
         *                      surface.
         * @param tol           The tolerance to use when deciding if a point belongs to
         *                      an existing intersection curve.
         * @return A list of lists of intersection curves traced from/to each edge point
         *         on each surface starting with the edge points on "this" surface.
         * @throws RootException If there is a convergence problem with a root finder.
         */
        private GeomList<GeomList<SubrangeCurve>> traceInteriorPoints(PointString<SubrangePoint> thisStartPnts,
                AbstractSurface thatSrf, Parameter<Length> tol) throws RootException {

            GeomList<GeomList<SubrangeCurve>> output = GeomList.newInstance();
            GeomList<SubrangeCurve> thisSegs = GeomList.newInstance();
            output.add(thisSegs);
            GeomList<SubrangeCurve> thatSegs = GeomList.newInstance();
            output.add(thatSegs);

            while (thisStartPnts.size() > 0) {
                SubrangePoint startPnt = thisStartPnts.get(0);

                //  Trace the intersection starting at "startPnt".
                GeomList<SubrangeCurve> segs = traceSegment(startPnt, thatSrf);
                if (segs.containsGeometry()) {
                    SubrangeCurve thisSeg = segs.get(0);
                    SubrangeCurve thatSeg = segs.get(1);

                    //  Don't add degenerate segments to the output.
                    if (!thisSeg.isDegenerate(_tol))
                        thisSegs.add(thisSeg);
                    if (!thatSeg.isDegenerate(_tol))
                        thatSegs.add(thatSeg);

                    //  Remove any points that are close to this curve segment.
                    SubrangePoint p = thisStartPnts.remove(0); //  The start point obviously.
                    thisStartPnts = removePntsNearCurve(thisStartPnts, thisSeg, tol);

                } else {
                    SubrangePoint p = thisStartPnts.remove(0);
                }
            }

            return output;
        }

        /**
         * Method that finds all the exact intersections of the edges of the surface (if
         * any).
         *
         * @return A list of subrange intersection points found on each edge of the
         *         surface.
         */
        private PointString<SubrangePoint> getEdgePoints(Surface srf1, Surface srf2) {

            PointString<SubrangePoint> output = PointString.newInstance();
            Parameter<Length> tol = _tol.divide(10);

            GeomList<PointString<SubrangePoint>> edge = srf1.getS0Curve().intersect(srf2, tol);
            PointString<SubrangePoint> edgePnts = edge.get(0);
            int size = edgePnts.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edgePnts.get(i);
                double t = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf1.getPoint(0, t);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge.get(0));
            PointString.recycle(edge.get(1));
            GeomList.recycle(edge);

            edge = srf1.getT0Curve().intersect(srf2, tol);
            edgePnts = edge.get(0);
            size = edgePnts.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edgePnts.get(i);
                double s = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf1.getPoint(s, 0);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge.get(0));
            PointString.recycle(edge.get(1));
            GeomList.recycle(edge);

            edge = srf1.getS1Curve().intersect(srf2, tol);
            edgePnts = edge.get(0);
            size = edgePnts.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edgePnts.get(i);
                double t = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf1.getPoint(1, t);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge.get(0));
            PointString.recycle(edge.get(1));
            GeomList.recycle(edge);

            edge = srf1.getT1Curve().intersect(srf2, tol);
            edgePnts = edge.get(0);
            size = edgePnts.size();
            for (int i = 0; i < size; ++i) {
                SubrangePoint cPnt = edgePnts.get(i);
                double s = cPnt.getParPosition().getValue(0);
                SubrangePoint sPnt = srf1.getPoint(s, 1);
                output.add(sPnt);
                SubrangePoint.recycle(cPnt);
            }
            PointString.recycle(edge.get(0));
            PointString.recycle(edge.get(1));
            GeomList.recycle(edge);

            //  Remove any redundant points
            removeClosePoints(output, _tol);

            return output;
        }

        /**
         * Trace intersection curves that start/end at the edges of a surface.
         *
         * @param thisEdgePnts The edge points on "this" surface to use as starting points
         *                     for tracing intersection curves.
         * @param thatEdgePnts The edge points on "that" surface to use as starting points
         *                     for tracing intersection curves.
         * @param thatSrf      "That" surface being used for intersecting with "this"
         *                     surface.
         * @return A list of lists of intersection curves traced from/to each edge point
         *         on each surface starting with the edge points on "this" surface.
         * @throws RootException If there is a convergence problem with a root finder.
         */
        private GeomList<GeomList<SubrangeCurve>> traceEdgePoints(PointString<SubrangePoint> thisEdgePnts,
                PointString<SubrangePoint> thatEdgePnts, AbstractSurface thatSrf) throws RootException {

            Parameter<Length> tol = _tol.times(10);
            GeomList<GeomList<SubrangeCurve>> output = GeomList.newInstance();
            GeomList<SubrangeCurve> thisSegs = GeomList.newInstance();
            output.add(thisSegs);
            GeomList<SubrangeCurve> thatSegs = GeomList.newInstance();
            output.add(thatSegs);

            int thisNumPnts = thisEdgePnts.size();
            while (thisNumPnts > 0) {
                SubrangePoint startPnt = thisEdgePnts.get(0);

                //  Trace the intersection starting at "startPnt".
                GeomList<SubrangeCurve> segs = traceSegment(startPnt, thatSrf);
                if (segs.containsGeometry()) {
                    SubrangeCurve thisSeg = segs.get(0);
                    SubrangeCurve thatSeg = segs.get(1);

                    //  Don't add degenerate segments to the output.
                    if (!thisSeg.isDegenerate(_tol))
                        thisSegs.add(thisSeg);
                    if (!thatSeg.isDegenerate(_tol))
                        thatSegs.add(thatSeg);

                    //  Remove the end point from the lists (if it is in them).
                    Surface thisSrf = (Surface)startPnt.getChild();
                    SubrangePoint endPoint = thisSrf.getPoint(thisSeg.getParPosition().getRealPoint(1));
                    int idx = findParNearestInList(endPoint, thisEdgePnts);
                    if (idx > 0) {
                        SubrangePoint pidx = thisEdgePnts.get(idx);
                        Parameter<Length> d = pidx.distance(endPoint);
                        if (d.isLessThan(tol)) {
                            thisEdgePnts.remove(idx);
                            --thisNumPnts;
                        }
                    }
                    if (thatEdgePnts.size() > 0) {
                        endPoint = thatSrf.getPoint(thatSeg.getParPosition().getRealPoint(1));
                        idx = findParNearestInList(endPoint, thatEdgePnts);
                        if (idx >= 0) {
                            SubrangePoint pidx = thatEdgePnts.get(idx);
                            Parameter<Length> d = pidx.distance(endPoint);
                            if (d.isLessThan(tol)) {
                                thatEdgePnts.remove(idx);
                            }
                        }
                    }
                }
                GeomList.recycle(segs);

                //  Remove the start point from this list.
                thisEdgePnts.remove(0);
                --thisNumPnts;
            }   //  end while

            return output;
        }

        /**
         * Method that traces an intersection segment from the given start point until an
         * edge of either intersecting surface is encountered or the start point is
         * re-encountered.
         *
         * @param startPnt The point on "this" surface to start tracing the intersection
         *                 from.
         * @param thatSrf  "That" surface that is being intersected with "this" surface.
         * @return A pair of subrange curves representing this intersection segment on
         *         each surface (this & that).
         */
        private GeomList<SubrangeCurve> traceSegment(SubrangePoint startPnt, AbstractSurface thatSrf) throws RootException {
            AbstractSurface thisSrf = (AbstractSurface)startPnt.getChild();

            BasicNurbsCurve pCrv1, pCrv2;
            StackContext.enter();
            try {
                PointString<SubrangePoint> thisTracedPnts = PointString.newInstance();
                PointString<SubrangePoint> thatTracedPnts = PointString.newInstance();
                PointString<SubrangePoint> pntLst = PointString.newInstance();

                Unit<Length> unit = thisSrf.getUnit();
                SubrangePoint p1 = startPnt;
                thisTracedPnts.add(p1);
                GeomPoint thisST = p1.getParPosition();     //  Parametric position on the surface of the point p1.
                double s01 = thisST.getValue(0);
                double t01 = thisST.getValue(1);

                SubrangePoint thatStartPnt = thatSrf.getClosest(p1.copyToReal(), GTOL);
                thatTracedPnts.add(thatStartPnt);
                GeomPoint thatST = thatStartPnt.getParPosition(); //  Parametric position on the surface of the point thatP1.
                double s2old = thatST.getValue(0);
                double t2old = thatST.getValue(1);

                //  Is the start point an edge point?
                boolean startEdge = false;
                if (parNearEnds(s01, GTOL100) || parNearEnds(t01, GTOL100)) {
                    startEdge = true;
                }

                boolean firstPoint = true;
                int collapsedEdge = -1;
                boolean flipNormal = false;
                while (thisTracedPnts.size() < 1000) {
                    //  Determine the step direction.
                    Vector<Dimensionless> pu = thisSrf.getSDerivative(thisST.getValue(0),
                            thisST.getValue(1), 1, false).toUnitVector();
                    if (!pu.isValid()) {
                        //  Step away from a collapsed edge.
                        double t = thisST.getValue(1);
                        if (t < 0.5) {
                            thisST = Point.valueOf(SI.METER, thisST.getValue(0), GTOL100);
                            collapsedEdge = 2;
                        } else {
                            thisST = Point.valueOf(SI.METER, thisST.getValue(0), 1. - GTOL100);
                            collapsedEdge = 3;
                        }
                        if (startEdge) {
                            p1 = thisSrf.getPoint(thisST);
                            s01 = thisST.getValue(0);
                            t01 = thisST.getValue(1);
                        }
                        pu = thisSrf.getSDerivative(thisST.getValue(0), thisST.getValue(1), 1, false).toUnitVector();
                    }
                    Vector<Dimensionless> pv = thisSrf.getTDerivative(thisST.getValue(0),
                            thisST.getValue(1), 1, false).toUnitVector();
                    if (!pv.isValid()) {
                        //  Step away from a collapsed edge.
                        double s = thisST.getValue(0);
                        if (s < 0.5) {
                            thisST = Point.valueOf(SI.METER, GTOL100, thisST.getValue(1));
                            collapsedEdge = 0;
                        } else {
                            thisST = Point.valueOf(SI.METER, 1. - GTOL100, thisST.getValue(1));
                            collapsedEdge = 1;
                        }
                        pu = pv = thisSrf.getTDerivative(thisST.getValue(0), thisST.getValue(1), 1, false).toUnitVector();
                        if (startEdge) {
                            p1 = thisSrf.getPoint(thisST);
                            s01 = thisST.getValue(0);
                            t01 = thisST.getValue(1);
                        }
                    }
                    Vector<Dimensionless> n2 = thatSrf.getNormal(thatST);
                    if (flipNormal)
                        n2 = n2.opposite();
                    Parameter pudotn2 = pu.dot(n2);
                    Parameter pvdotn2 = pv.dot(n2);
                    if (pudotn2.isApproxZero() && pvdotn2.isApproxZero()) {
                        Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                "Surfaces are nearly tangent to each other in traceSegment()!");
                        break;      //  Surface is nearly tangent to other surface.  TODO:  Handle this better.
                    }
                    //  Ti = pv*(pu dot n2) - pu*(pv dot n2)
                    Vector<Dimensionless> Ti = pv.times(pudotn2).minus((Vector)pu.times(pvdotn2));
                    Ti = Ti.toUnitVector();

                    //  Determine the step size "L".
                    Point p = p1.plus(Point.valueOf(Ti.times(_eps1)));
                    SubrangePoint x = thisSrf.getClosestFarthestInterior(p, thisST.getValue(0), thisST.getValue(1), GTOL, true);

                    //  If "Ti" initially steps us off the edge of the surface, reverse it.
                    if (startEdge) {
                        startEdge = false;
                        double sx = x.getParPosition().getValue(0);
                        double tx = x.getParPosition().getValue(1);
                        if (parNearEnds(sx, GTOL) || parNearEnds(tx, GTOL)
                                || abs(sx - s01) > 0.3 || abs(tx - t01) > 0.3) {
                            //  Ti stepped off the edge.
                            Ti = Ti.opposite();
                            p = p1.plus(Point.valueOf(Ti.times(_eps1)));
                            x = thisSrf.getClosestFarthestInterior(p, thisST.getValue(0), thisST.getValue(1), GTOL100, true);

                            //  Reverse the intersecting surface normal so this doesn't happen again.
                            flipNormal = true;
                        }
                    }

                    relaxPoint(x, thatSrf, thatST, pntLst, _tol);
                    if (pntLst.isEmpty())
                        break;
                    x = pntLst.get(0);
                    pntLst.clear();
                    p = p1.plus(Point.valueOf(Ti.times(_eps2)));
                    SubrangePoint y = thisSrf.getClosestFarthestInterior(p, thisST.getValue(0), thisST.getValue(1), GTOL100, true);
                    relaxPoint(y, thatSrf, thatST, pntLst, _tol);
                    if (pntLst.isEmpty())
                        break;
                    y = pntLst.get(0);
                    pntLst.clear();

                    //  If the start point was on a collapsed edge, go back and fix things up.
                    if (collapsedEdge >= 0) {
                        thisST = x.getParPosition();
                        switch (collapsedEdge) {
                            case 0:
                                thisST = Point.valueOf(SI.METER, 0, thisST.getValue(1));
                                break;
                            case 1:
                                thisST = Point.valueOf(SI.METER, 1, thisST.getValue(1));
                                break;
                            case 2:
                                thisST = Point.valueOf(SI.METER, thisST.getValue(0), 0);
                                break;
                            case 3:
                                thisST = Point.valueOf(SI.METER, thisST.getValue(0), 1);
                                break;
                        }
                        p1 = thisSrf.getPoint(thisST);
                        s01 = thisST.getValue(0);
                        t01 = thisST.getValue(1);
                        startPnt = p1;
                        thisTracedPnts.clear();
                        thisTracedPnts.add(p1);

                        collapsedEdge = -1;
                    }

                    Vector<Length> a = x.minus(p1).toGeomVector();
                    Vector<Length> b = y.minus(p1).toGeomVector();

                    double na = a.normValue();
                    double nb = b.normValue();
                    double namb = a.minus(b).normValue();
                    double naxb = a.cross(b).normValue();
//                    System.out.print("naxb = " + naxb);
                    Parameter<Length> L;
                    if (naxb > 1e-9) {
                        //  The 3 points are not co-linear.
                        //  r = |a||b||a - b|/(2*|a x b|)
                        double r = 0.5 * na * nb * namb / naxb;
                        L = Parameter.valueOf(r * DTHETA, unit);
//                        System.out.print(", r*DTHETA = " + L);
                        if (L.isGreaterThan(_epsCRT))
                            L = _epsCRT;
                    } else {
                        L = _epsCRT;
                    }
//                    System.out.println(", L = " + L);

                    //  Calculate the new intersection point locations.
                    p = p1.plus(Point.valueOf(Ti.times(L)));
                    SubrangePoint p2 = thisSrf.getClosestFarthestInterior(p, thisST.getValue(0), thisST.getValue(1), GTOL100, true);
                    relaxPoint(p2, thatSrf, thatST, pntLst, _tol);
                    if (pntLst.isEmpty())
                        break;
                    p2 = pntLst.get(0);
                    SubrangePoint thatP2 = pntLst.get(1);
                    pntLst.clear();

                    //  See if the new point on thatSrf has jumped a boundary.
                    thatST = thatP2.getParPosition();
                    double s2 = thatST.getValue(0);
                    double t2 = thatST.getValue(1);
                    if ((s2 > 0.8 && s2old < 0.2) || (s2 < 0.2 && s2old > 0.8)) {
                        //  We have crossed the S boundary.
                        s2 = round(s2old);
                        thatP2 = thatSrf.getPoint(s2, t2);

                        //  Relax the corrected point and the nearest thisSrf point to the intersection.
                        relaxPoint(thatP2, thisSrf, thisST, pntLst, _tol);
                        if (pntLst.isEmpty())
                            break;
                        thatP2 = pntLst.get(0);
                        p2 = pntLst.get(1);
                        pntLst.clear();

                        thatST = thatP2.getParPosition();

                    } else if ((t2 > 0.8 && t2old < 0.2) || (t2 < 0.2 && t2old > 0.8)) {
                        //  We have crossed the T boundary.
                        t2 = round(t2old);
                        thatP2 = thatSrf.getPoint(s2, t2);
                        relaxPoint(thatP2, thisSrf, thisST, pntLst, _tol);
                        if (pntLst.isEmpty())
                            break;
                        thatP2 = pntLst.get(0);
                        p2 = pntLst.get(1);
                        pntLst.clear();
                        thatST = thatP2.getParPosition();
                    }

                    Parameter<Length> d = p2.distance(thatP2);
                    if (d.isLessThan(_tol)) {
                        //  Store the new intersection points.
                        thisTracedPnts.add(p2);
                        thatTracedPnts.add(thatP2);
                    }

                    //  Is this new point an edge point on "this" surface?
                    thisST = p2.getParPosition();
                    double s1 = thisST.getValue(0);
                    double t1 = thisST.getValue(1);
                    if (!firstPoint) {
                        if (parNearEnds(s1, GTOL100) || parNearEnds(t1, GTOL100))
                            break;

                        //  Is this new point an edge point on "that" surface?
                        if (parNearEnds(s2, GTOL100) || parNearEnds(t2, GTOL100))
                            break;

                        //  Is this new point near the start point?
                        if (thisST.distanceValue(startPnt.getParPosition()) < 0.1
                                && p2.distance(startPnt).isLessThan(L.times(0.75))) {
                            thisTracedPnts.add(startPnt);     //  Repeat the 1st point.
                            thatTracedPnts.add(thatStartPnt);
                            break;
                        }
                    }

                    //  Prepare for next step.
                    p1 = p2;
                    s2old = s2;
                    t2old = t2;
                    firstPoint = false;
                }

                //  Create the curve segment on each surface.
                if (thisTracedPnts.size() == 1) {
                    thisST = thisTracedPnts.get(0).getParPosition();
                    pCrv1 = StackContext.outerCopy(CurveFactory.createPoint(1, thisST));

                    thatST = thatTracedPnts.get(0).getParPosition();
                    pCrv2 = StackContext.outerCopy(CurveFactory.createPoint(1, thatST));

                } else {
                    //  Create the intersection curve from the list of intersection points.
                    SubrangeCurve thisCrv = makeSubrangeCurve(thisTracedPnts);
                    SubrangeCurve thatCrv = makeSubrangeCurve(thatTracedPnts);

                    //  Copy the parametric curve's to the outer context.
                    pCrv1 = StackContext.outerCopy((BasicNurbsCurve)thisCrv.getParPosition());
                    pCrv2 = StackContext.outerCopy((BasicNurbsCurve)thatCrv.getParPosition());
                }

            } finally {
                StackContext.exit();
            }

            //  Create the output list and add the final subrange curves to it.
            GeomList<SubrangeCurve> output = GeomList.newInstance();
            output.add(SubrangeCurve.newInstance(thisSrf, pCrv1));
            output.add(SubrangeCurve.newInstance(thatSrf, pCrv2));

            return output;
        }

        private static final int MAXIT = 500;

        /**
         * Relax the approximate intersection point toward the exact intersection until it
         * is within "tol" of the exact intersection.
         *
         * @param p1      The approximate surface intersection point being refined or
         *                relaxed.
         * @param thatSrf The intersecting surface.
         * @param thatST  The parametric position on "thatSrf" near "p1".
         * @param tol     The tolerance required of the intersection points.
         * @param output  A list to which the input point relaxed onto the intersection
         *                between the two surfaces to within the tolerance "tol" will be
         *                added. The 1st point is attached to "this" surface and the 2nd
         *                point is attached to "that" surface. If the surfaces are locally
         *                tangent near the intersection, this may add nothing to the
         *                output list.
         */
        private void relaxPoint(SubrangePoint p1, AbstractSurface thatSrf, GeomPoint thatST,
                PointString<SubrangePoint> output,
                Parameter<Length> tol) throws RootException {

            AbstractSurface thisSrf = (AbstractSurface)p1.getChild();

            Point parPnt1, parPnt2;
            StackContext.enter();
            try {
                SubrangePoint p2 = thatSrf.getClosestFarthestInterior(p1, thatST.getValue(0), thatST.getValue(1), GTOL100, true);

                Parameter<Length> d = p1.distance(p2);
                if (d.isGreaterThan(tol)) {

                    Unit<Length> unit = thisSrf.getUnit();
                    int numDims = thisSrf.getPhyDimension();
                    MutableVector Lu = MutableVector.newInstance(numDims, Dimensionless.UNIT);
                    MutablePoint L0 = MutablePoint.newInstance(numDims, unit);

                    int iteration = 0;
                    do {
                        //  Get the local tangent plane on this surface.
                        GeomPoint thisST = p1.getParPosition();
                        GeomPlane T1 = thisSrf.getTangentPlane(thisST);

                        //  Get the local tangent plane on that surface.
                        thatST = p2.getParPosition();
                        GeomPlane T2 = thatSrf.getTangentPlane(thatST);

                        //  Intersect the surface tangent planes.
                        IntersectType type = GeomUtil.planePlaneIntersect(T1, T2, L0, Lu);
                        if (type != IntersectType.INTERSECT) {
                            return;
                        }

                        //  Find the closest point on the plane intersection line to the previous points.
                        Point q1 = GeomUtil.pointLineClosest(p1, L0, Lu);
                        Point q2 = GeomUtil.pointLineClosest(p2, L0, Lu);
                        Point pip1 = q1.plus(q2).divide(2);

                        //  Re-attach this point to the surfaces.
                        p1 = thisSrf.getClosestFarthestInterior(pip1, thisST.getValue(0), thisST.getValue(1), GTOL100, true);
                        p2 = thatSrf.getClosestFarthestInterior(pip1, thatST.getValue(0), thatST.getValue(1), GTOL100, true);

                        //  Update the distance between these new points.
                        d = p1.distance(p2);
                        //System.out.println("it = " + iteration + ", d = " + d);

                        ++iteration;

                    } while (d.isGreaterThan(tol) && iteration < MAXIT);

                    if (iteration == MAXIT)
                        Logger.getLogger(AbstractSurface.class.getName()).log(Level.WARNING,
                                "Convergence problem in SrfSrfIntersect.relaxPoint().");
                }

                //  Copy the parametric positions of the final points to the outer context.
                parPnt1 = StackContext.outerCopy(p1.getParPosition().immutable());
                parPnt2 = StackContext.outerCopy(p2.getParPosition().immutable());

            } finally {
                StackContext.exit();
            }

            //  Store the relaxed points.
            output.add(thisSrf.getPoint(parPnt1));
            output.add(thatSrf.getPoint(parPnt2));
        }

        /**
         * Uses a recursive "Divide and Conquer" approach to intersecting a surface patch
         * with another surface patch. On each call, the following happens:
         * <pre>
         *      1) Each patch is checked to see if it is approx. a flat plane.
         *          1b) If both are, then a plane-quad intersection is performed, the
         *              approximate intersection points added to the _appInteriorPnts list
         *              and the method exited.
         *      2) Each non-planar patch is divided into four quadrant patches.
         *          2b) Each sub-patch is tested to see if it's bounding box is intersected by any
         *              of the other sub-patches.
         *              If it is, then this method is called with those patches recursively.
         *              Otherwise, the method is exited.
         * </pre> A number of class variables are used to pass information to this
         * recursion:
         * <pre>
         *      _thisSrf The full surfaces intersections are being found for.
         *      _thatSrf The full surface being intersected with this surface.
         *      _tol  The tolerance to use in determining if the geometry is in tolerance.
         *      _appInteriorPnts A list used to collect the approximate subrange intersection
         *          points on _thisSrf.
         * </pre>
         *
         * @param patch1 The first surface patch being checked for intersection with
         *               "that" surface.
         * @param s0_1   The "s" parametric position of the start of patch1 on the master
         *               "this" surface.
         * @param t0_1   The "t" parametric position of the start of patch1 on the master
         *               "this" surface.
         * @param s1_1   The parametric "s" position of the end of patch1 on the master
         *               "this" surface.
         * @param t1_1   The parametric "t" position of the end of patch1 on the master
         *               "this" surface.
         * @param patch2 The 2nd surface patch being checked for intersection with "this"
         *               surface.
         * @param s0_2   The "s" parametric position of the start of patch2 on the master
         *               "that" surface.
         * @param t0_2   The "t" parametric position of the start of patch2 on the master
         *               "that" surface.
         * @param s1_2   The parametric "s" position of the end of patch2 on the master
         *               "that" surface.
         * @param t1_2   The parametric "t" position of the end of patch2 on the master
         *               "that" surface.
         */
        private void divideAndConquerSrf(int depth, Surface patch1, double s0_1, double t0_1, double s1_1, double t1_1,
                Surface patch2, double s0_2, double t0_2, double s1_2, double t1_2) throws RootException {

            boolean p1Planar = patch1.isPlanar(EPS_FT, EPS_FT);   //  Using a fast method that unfortunately ignores "tol".
            boolean p2Planar = patch2.isPlanar(EPS_FT, EPS_FT);

            boolean p1IsSmall = isSmall(patch1, _patchSizeTol);
            boolean p2IsSmall = isSmall(patch2, _patchSizeTol);

            //  Check to see if both patches are nearly planar or to small.
            if (depth > MAXDEPTH || (p1Planar && p2Planar) || (p1IsSmall && p2IsSmall)) {

                //  Form a quadrilateral panel (2 triangles) from the corners of patch1.
                Point A1 = patch1.getRealPoint(0, 0);
                Point B1 = patch1.getRealPoint(1, 0);
                Point C1 = patch1.getRealPoint(1, 1);
                Point D1 = patch1.getRealPoint(0, 1);

                //  Form a quadrilateral panel (2 triangles) from the corners of patch2.
                Point A2 = patch2.getRealPoint(0, 0);
                Point B2 = patch2.getRealPoint(1, 0);
                Point C2 = patch2.getRealPoint(1, 1);
                Point D2 = patch2.getRealPoint(0, 1);

                //  Determine if any edge of patch2 intersects any triangle of patch 1.
                //  Triangle #1 = A1, B1, C1; Triangle #2 = A1, C1, D1
                if (GeomUtil.lineSegTriIntersect(A2, B2, A1, B1, C1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(A2, B2, A1, C1, D1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(B2, C2, A1, B1, C1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(B2, C2, A1, C1, D1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(C2, D2, A1, B1, C1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(C2, D2, A1, C1, D1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(D2, A2, A1, B1, C1, null) == IntersectType.INTERSECT
                        || GeomUtil.lineSegTriIntersect(D2, A2, A1, C1, D1, null) == IntersectType.INTERSECT) {

                    //  Store off the approximate intersection point (patch1 center).
                    double s = segmentPos2Parent(0.5, s0_1, s1_1);
                    double t = segmentPos2Parent(0.5, t0_1, t1_1);
                    SubrangePoint centerPnt = _thisSrf.getPoint(s, t);
                    _appInteriorPnts.add(centerPnt);
                }

                //System.out.println("depth = " + depth);
            } else {
                Surface p00_1 = patch1;
                Surface p00_2 = patch2;

                Surface p01_1 = null, p10_1 = null, p11_1 = null;
                if (!p1Planar) {
                    //  Subdivide the 1st patch into 4 quadrants.
                    GeomList<Surface> leftRight = patch1.splitAtS(0.5);
                    GeomList<Surface> botTop = leftRight.get(0).splitAtT(0.5);
                    p00_1 = botTop.get(0);                  //  Lower Left
                    p01_1 = botTop.get(1);          //  Top Left
                    GeomList.recycle(botTop);
                    botTop = leftRight.get(1).splitAtT(0.5);
                    p10_1 = botTop.get(0);          //  Lower Right
                    p11_1 = botTop.get(1);          //  Top Right
                    GeomList.recycle(botTop);
                    GeomList.recycle(leftRight);
                }

                Surface p01_2 = null, p10_2 = null, p11_2 = null;
                if (!p2Planar) {
                    //  Subdivide the 2nd patch into 4 quadrants.
                    GeomList<Surface> leftRight = patch2.splitAtS(0.5);
                    GeomList<Surface> botTop = leftRight.get(0).splitAtT(0.5);
                    p00_2 = botTop.get(0);                  //  Lower Left
                    p01_2 = botTop.get(1);          //  Top Left
                    GeomList.recycle(botTop);
                    botTop = leftRight.get(1).splitAtT(0.5);
                    p10_2 = botTop.get(0);          //  Lower Right
                    p11_2 = botTop.get(1);          //  Top Right
                    GeomList.recycle(botTop);
                    GeomList.recycle(leftRight);
                }

                //  Mean parametric positions on each patch.
                double sm_1 = 0.5 * (s0_1 + s1_1);
                double tm_1 = 0.5 * (t0_1 + t1_1);
                double sm_2 = 0.5 * (s0_2 + s1_2);
                double tm_2 = 0.5 * (t0_2 + t1_2);

                //  Check for possible intersections on the lower-left patch.
                if (GeomUtil.boundsIntersect(p00_1, p00_2)) {
                    //  May be an intersection.
                    double sl_1 = s0_1;
                    double sh_1 = sm_1;
                    double tl_1 = t0_1;
                    double th_1 = tm_1;
                    double sl_2 = s0_2;
                    double sh_2 = sm_2;
                    double tl_2 = t0_2;
                    double th_2 = tm_2;

                    //  Recurse down to a finer level of detail.
                    divideAndConquerSrf(depth + 1, p00_1, sl_1, tl_1, sh_1, th_1,
                            p00_2, sl_2, tl_2, sh_2, th_2);
                }
                if (!p2Planar) {
                    if (GeomUtil.boundsIntersect(p00_1, p10_2)) {
                        //  May be an intersection.
                        double sl_1 = s0_1;
                        double sh_1 = sm_1;
                        double tl_1 = t0_1;
                        double th_1 = tm_1;
                        double sl_2 = sm_2;
                        double sh_2 = s1_2;
                        double tl_2 = t0_2;
                        double th_2 = tm_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerSrf(depth + 1, p00_1, sl_1, tl_1, sh_1, th_1,
                                p10_2, sl_2, tl_2, sh_2, th_2);
                    }
                    if (GeomUtil.boundsIntersect(p00_1, p01_2)) {
                        //  May be an intersection.
                        double sl_1 = s0_1;
                        double sh_1 = sm_1;
                        double tl_1 = t0_1;
                        double th_1 = tm_1;
                        double sl_2 = s0_2;
                        double sh_2 = sm_2;
                        double tl_2 = tm_2;
                        double th_2 = t1_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerSrf(depth + 1, p00_1, sl_1, tl_1, sh_1, th_1,
                                p01_2, sl_2, tl_2, sh_2, th_2);
                    }
                    if (GeomUtil.boundsIntersect(p00_1, p11_2)) {
                        //  May be an intersection.
                        double sl_1 = s0_1;
                        double sh_1 = sm_1;
                        double tl_1 = t0_1;
                        double th_1 = tm_1;
                        double sl_2 = sm_2;
                        double sh_2 = s1_2;
                        double tl_2 = tm_2;
                        double th_2 = t1_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerSrf(depth + 1, p00_1, sl_1, tl_1, sh_1, th_1,
                                p11_2, sl_2, tl_2, sh_2, th_2);
                    }
                }   //  if !p2Planar
                if (p00_1 != patch1 && p00_1 instanceof BasicNurbsSurface)
                    BasicNurbsSurface.recycle((BasicNurbsSurface)p00_1);

                if (!p1Planar) {
                    //  Check for possible intersections on the lower-right patch.
                    if (GeomUtil.boundsIntersect(p10_1, p00_2)) {
                        //  May be an intersection.
                        double sl_1 = sm_1;
                        double sh_1 = s1_1;
                        double tl_1 = t0_1;
                        double th_1 = tm_1;
                        double sl_2 = s0_2;
                        double sh_2 = sm_2;
                        double tl_2 = t0_2;
                        double th_2 = tm_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerSrf(depth + 1, p10_1, sl_1, tl_1, sh_1, th_1,
                                p00_2, sl_2, tl_2, sh_2, th_2);
                    }
                    if (!p2Planar) {
                        if (GeomUtil.boundsIntersect(p10_1, p10_2)) {
                            //  May be an intersection.
                            double sl_1 = sm_1;
                            double sh_1 = s1_1;
                            double tl_1 = t0_1;
                            double th_1 = tm_1;
                            double sl_2 = sm_2;
                            double sh_2 = s1_2;
                            double tl_2 = t0_2;
                            double th_2 = tm_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p10_1, sl_1, tl_1, sh_1, th_1,
                                    p10_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (GeomUtil.boundsIntersect(p10_1, p01_2)) {
                            //  May be an intersection.
                            double sl_1 = sm_1;
                            double sh_1 = s1_1;
                            double tl_1 = t0_1;
                            double th_1 = tm_1;
                            double sl_2 = s0_2;
                            double sh_2 = sm_2;
                            double tl_2 = tm_2;
                            double th_2 = t1_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p10_1, sl_1, tl_1, sh_1, th_1,
                                    p01_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (GeomUtil.boundsIntersect(p10_1, p11_2)) {
                            //  May be an intersection.
                            double sl_1 = sm_1;
                            double sh_1 = s1_1;
                            double tl_1 = t0_1;
                            double th_1 = tm_1;
                            double sl_2 = sm_2;
                            double sh_2 = s1_2;
                            double tl_2 = tm_2;
                            double th_2 = t1_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p10_1, sl_1, tl_1, sh_1, th_1,
                                    p11_2, sl_2, tl_2, sh_2, th_2);
                        }
                    }   //  if !p2Planar
                    if (p10_1 instanceof BasicNurbsSurface)
                        BasicNurbsSurface.recycle((BasicNurbsSurface)p10_1);

                    //  Check for possible intersections on the top-left patch.
                    if (GeomUtil.boundsIntersect(p01_1, p00_2)) {
                        //  May be an intersection.
                        double sl_1 = s0_1;
                        double sh_1 = sm_1;
                        double tl_1 = tm_1;
                        double th_1 = t1_1;
                        double sl_2 = s0_2;
                        double sh_2 = sm_2;
                        double tl_2 = t0_2;
                        double th_2 = tm_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerSrf(depth + 1, p01_1, sl_1, tl_1, sh_1, th_1,
                                p00_2, sl_2, tl_2, sh_2, th_2);
                    }
                    if (!p2Planar) {
                        if (GeomUtil.boundsIntersect(p01_1, p10_2)) {
                            //  May be an intersection.
                            double sl_1 = s0_1;
                            double sh_1 = sm_1;
                            double tl_1 = tm_1;
                            double th_1 = t1_1;
                            double sl_2 = sm_2;
                            double sh_2 = s1_2;
                            double tl_2 = t0_2;
                            double th_2 = tm_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p01_1, sl_1, tl_1, sh_1, th_1,
                                    p10_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (GeomUtil.boundsIntersect(p01_1, p01_2)) {
                            //  May be an intersection.
                            double sl_1 = s0_1;
                            double sh_1 = sm_1;
                            double tl_1 = tm_1;
                            double th_1 = t1_1;
                            double sl_2 = s0_2;
                            double sh_2 = sm_2;
                            double tl_2 = tm_2;
                            double th_2 = t1_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p01_1, sl_1, tl_1, sh_1, th_1,
                                    p01_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (GeomUtil.boundsIntersect(p01_1, p11_2)) {
                            //  May be an intersection.
                            double sl_1 = s0_1;
                            double sh_1 = sm_1;
                            double tl_1 = tm_1;
                            double th_1 = t1_1;
                            double sl_2 = sm_2;
                            double sh_2 = s1_2;
                            double tl_2 = tm_2;
                            double th_2 = t1_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p01_1, sl_1, tl_1, sh_1, th_1,
                                    p11_2, sl_2, tl_2, sh_2, th_2);
                        }
                    }   //  if !p2Planar
                    if (p01_1 instanceof BasicNurbsSurface)
                        BasicNurbsSurface.recycle((BasicNurbsSurface)p01_1);

                    //  Check for possible intersections on the top-right patch.
                    if (GeomUtil.boundsIntersect(p11_1, p00_2)) {
                        //  May be an intersection.
                        double sl_1 = sm_1;
                        double sh_1 = s1_1;
                        double tl_1 = tm_1;
                        double th_1 = t1_1;
                        double sl_2 = s0_2;
                        double sh_2 = sm_2;
                        double tl_2 = t0_2;
                        double th_2 = tm_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerSrf(depth + 1, p11_1, sl_1, tl_1, sh_1, th_1,
                                p00_2, sl_2, tl_2, sh_2, th_2);
                    }
                    if (p00_2 != patch2 && p00_2 instanceof BasicNurbsSurface)
                        BasicNurbsSurface.recycle((BasicNurbsSurface)p00_2);
                    if (!p2Planar) {
                        if (GeomUtil.boundsIntersect(p11_1, p10_2)) {
                            //  May be an intersection.
                            double sl_1 = sm_1;
                            double sh_1 = s1_1;
                            double tl_1 = tm_1;
                            double th_1 = t1_1;
                            double sl_2 = sm_2;
                            double sh_2 = s1_2;
                            double tl_2 = t0_2;
                            double th_2 = tm_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p11_1, sl_1, tl_1, sh_1, th_1,
                                    p10_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (p10_2 instanceof BasicNurbsSurface)
                            BasicNurbsSurface.recycle((BasicNurbsSurface)p10_2);
                        if (GeomUtil.boundsIntersect(p11_1, p01_2)) {
                            //  May be an intersection.
                            double sl_1 = sm_1;
                            double sh_1 = s1_1;
                            double tl_1 = tm_1;
                            double th_1 = t1_1;
                            double sl_2 = s0_2;
                            double sh_2 = sm_2;
                            double tl_2 = tm_2;
                            double th_2 = t1_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p11_1, sl_1, tl_1, sh_1, th_1,
                                    p01_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (p01_2 instanceof BasicNurbsSurface)
                            BasicNurbsSurface.recycle((BasicNurbsSurface)p01_2);
                        if (GeomUtil.boundsIntersect(p11_1, p11_2)) {
                            //  May be an intersection.
                            double sl_1 = sm_1;
                            double sh_1 = s1_1;
                            double tl_1 = tm_1;
                            double th_1 = t1_1;
                            double sl_2 = sm_2;
                            double sh_2 = s1_2;
                            double tl_2 = tm_2;
                            double th_2 = t1_2;

                            //  Recurse down to a finer level of detail.
                            divideAndConquerSrf(depth + 1, p11_1, sl_1, tl_1, sh_1, th_1,
                                    p11_2, sl_2, tl_2, sh_2, th_2);
                        }
                        if (p11_2 instanceof BasicNurbsSurface)
                            BasicNurbsSurface.recycle((BasicNurbsSurface)p11_2);
                    } //    if !p2Planar
                    if (p11_1 instanceof BasicNurbsSurface)
                        BasicNurbsSurface.recycle((BasicNurbsSurface)p11_1);
                }   //  if !p1Planar
            }

        }

        public static void recycle(SrfSrfIntersect instance) {
            FACTORY.recycle(instance);
        }

        private SrfSrfIntersect() { }

        private static final ObjectFactory<SrfSrfIntersect> FACTORY = new ObjectFactory<SrfSrfIntersect>() {
            @Override
            protected SrfSrfIntersect create() {
                return new SrfSrfIntersect();
            }

            @Override
            protected void cleanup(SrfSrfIntersect obj) {
                obj._thisSrf = null;
                obj._thatSrf = null;
                obj._thisOut = null;
                obj._thatOut = null;
                obj._appInteriorPnts = null;
                obj._tol = null;
                obj._patchSizeTol = null;
                obj._eps1 = null;
                obj._eps2 = null;
                obj._epsCRT = null;
            }
        };

    }

}
