/**
 * Note -- Holds a textual note String located at a point in model space with a fixed size
 * and orientation on the screen.
 *
 * Copyright (C) 2014-2015, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.awt.Font;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.lang.ValueType;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * Represents a textual note located at a point in model space with a fixed size and
 * orientation on the screen.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 5, 2014
 * @version November 26, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class Note extends GenScreenNote implements ValueType {

    /**
     * The text string displayed in the note.
     */
    private String text;

    /**
     * Holds the location of the note and defines it's units.
     */
    private Point location;

    /**
     * The font used to display the note.
     */
    private Font font;

    /**
     * Construct and return a new instance of a Note that uses the specified text string,
     * the specified display font, and is located at the specified location in space.
     *
     * @param text     The text to be displayed in this geometry object. May not be null.
     * @param location The location of this geometry object in model space. May not be null.
     * @param font     The font used to display this note. May not be null.
     * @return A new Note using the specified inputs.
     */
    public static Note valueOf(CharSequence text, GeomPoint location, Font font) {
        requireNonNull(text);
        requireNonNull(location);
        requireNonNull(font);

        Note note = FACTORY.object();
        note.text = text.toString();
        note.location = location.immutable();
        note.font = font;

        return note;
    }

    /**
     * Construct and return a new instance of a Note that uses the specified text string,
     * the default display font at the specified size, and is located at the specified
     * location in space.
     *
     * @param text       The text to be displayed in this geometry object. May not be
     *                   null.
     * @param location   The location of this geometry object in model space. May not be
     *                   null.
     * @param fontPoints The size of the default font in pixels.
     * @return A new Note using the specified inputs.
     */
    public static Note valueOf(CharSequence text, GeomPoint location, int fontPoints) {
        return Note.valueOf(text, location, DEFAULT_FONT.deriveFont((float)fontPoints));
    }

    /**
     * Construct and return a new instance of a Note that uses the specified text string,
     * the default display font, and is located at the specified location in space.
     *
     * @param text     The text to be displayed in this geometry object. May not be null.
     * @param location The location of this geometry object in model space. May not be null.
     * @return A new Note using the specified inputs.
     */
    public static Note valueOf(CharSequence text, GeomPoint location) {
        return Note.valueOf(text, location, DEFAULT_FONT);
    }

    /**
     * Returns a new Note instance that is identical to the specified Note.
     *
     * @param note the Note to be copied into a new Note.  May not be null.
     * @return A new not identical to the input note.
     */
    public static Note valueOf(Note note) {
        return copyOf(requireNonNull(note));
    }

    /**
     * Return the text string associated with this note object.
     *
     * @return The text string associated with this note object.
     */
    @Override
    public String getNote() {
        return text;
    }

    /**
     * Return the location of this note in model space.
     *
     * @return The location of this note in model space.
     */
    @Override
    public Point getLocation() {
        return location;
    }

    /**
     * Return the font used to display this note.
     *
     * @return The font used to display this note.
     */
    @Override
    public Font getFont() {
        return font;
    }

    /**
     * Return an immutable version of this note.
     *
     * @return An immutable version of this note.
     */
    @Override
    public Note immutable() {
        return this;
    }

    /**
     * Return a new note object identical to this one, but with the specified font.
     *
     * @param font The font to use in the copy of this note returned. May not be null.
     * @return A new note object identical to this one, but with the specified font.
     */
    @Override
    public Note changeFont(Font font) {
        Note note = Note.valueOf(text, location, requireNonNull(font));
        copyState(note);
        return note;
    }

    /**
     * Return a new note object identical to this one, but with the specified location in
     * model space.
     *
     * @param location The location for the copy of this note returned. May not be null.
     * @return A new note object identical to this one, but with the specified location in
     *         model space.
     */
    @Override
    public Note changeLocation(GeomPoint location) {
        Note note = Note.valueOf(text, requireNonNull(location), font);
        copyState(note);
        return note;
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation will return the physical dimensions of the point indicating the
     * location of the note in space.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return location.getPhyDimension();
    }

    /**
     * Return <code>true</code> if this Note contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the location
     * coordinate values are NaN or Inf.
     *
     * @return true if this Note contains valid and finite numerical components.
     */
    @Override
    public boolean isValid() {
        return location.isValid();
    }

    /**
     * Returns a copy of this Note instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this note.
     */
    @Override
    public Note copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public Note copyToReal() {
        return copy();
    }

    /**
     * Returns the unit in which the note location Point is stored.
     *
     * @return The unit in which the note location Point is stored.
     */
    @Override
    public final Unit<Length> getUnit() {
        return location.getUnit();
    }

    /**
     * Returns the equivalent to this note but with the location stated in the specified
     * unit.
     *
     * @param unit the length unit of the note to be returned. May not be null.
     * @return an equivalent of this note but with location stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public Note to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        Note note = FACTORY.object();
        note.text = text;
        note.location = location.to(unit);
        copyState(note);

        return note;
    }

    /**
     * Return the equivalent of this note converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the note to return.
     * @return The equivalent to this note converted to the new dimensions.
     */
    @Override
    public Note toDimension(int newDim) {
        int thisDim = this.getPhyDimension();
        if (newDim == thisDim)
            return this;

        //  Convert the underlying geometry.
        Point nloc = location.toDimension(newDim);

        //  Create and return a new note with the new geometry.
        Note note = Note.valueOf(text, nloc, font);
        copyState(note);

        return note;
    }

    /**
     * Compares this Note against the specified object for strict equality (same values
     * and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this note is identical to that note;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        Note that = (Note)obj;
        return this.text.equals(that.text)
                && this.location.equals(that.location)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this Note object.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(text, location);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<Note> XML = new XMLFormat<Note>(Note.class) {

        @Override
        public Note newInstance(Class<Note> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, Note obj) throws XMLStreamException {
            //  Read in the font information.
            String fontStr = xml.getAttribute("font", DEFAULT_FONT_CODE);
            Font font = Font.decode(fontStr);

            GenScreenNote.XML.read(xml, obj);     // Call parent read.

            //  Read in the text string.
            String text = xml.get("Note", String.class);

            //  Read in the location.
            Point location = xml.get("Location", Point.class);

            //  Fill in the object definition.
            obj.text = text;
            obj.font = font;
            obj.location = location;
        }

        @Override
        public void write(Note obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            //  Write out a font string.
            Font font = obj.getFont();
            String fontStr = encodeFont(font.getName(), font.getStyle(), font.getSize());
            xml.setAttribute("font", fontStr);

            GenScreenNote.XML.write(obj, xml);    // Call parent write.

            //  Write out the text string.
            xml.add(obj.getNote(), "Note", String.class);

            //  Write out the location of the note in model space.
            xml.add(obj.getLocation(), "Location", Point.class);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private Note() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<Note> FACTORY = new ObjectFactory<Note>() {
        @Override
        protected Note create() {
            return new Note();
        }

        @Override
        protected void cleanup(Note obj) {
            obj.reset();
            obj.text = null;
            obj.location = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static Note copyOf(Note original) {
        Note obj = FACTORY.object();
        obj.text = original.text;
        obj.location = original.location.copy();
        obj.font = original.font;
        original.copyState(obj);
        return obj;
    }
}
