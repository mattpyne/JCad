/*
 *   GridSpacing  -- Factory methods for creating various grid spacings.
 *
 *   Copyright (C) 2009-2017, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.tools.math.AbstractEvaluatable1D;
import jahuwaldt.tools.math.RootException;
import jahuwaldt.tools.math.Roots;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javolution.context.ObjectFactory;
import javolution.lang.MathLib;
import javolution.util.FastTable;

/**
 * A collection of methods for creating various grid spacing options.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 26, 2009
 * @version January 30, 2017
 */
public class GridSpacing {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = AbstractGeomElement.RESOURCES;

    /**
     * An enumeration of the available grid spacing types.
     */
    public enum GridType {

        LINEAR("linear"),
        SQR("sqr"),
        ONEMSQRT("oneMinusSqrt"),
        ACOS("acos"),
        COS("cos"),
        SQRT("sqrt");

        private static final String[] TITLES;
        static {
            TITLES = RESOURCES.getString("spacingTypeLabels").split(",");
        }

        //  The GridSpacing method signature for the spacing constant.
        private Method spacingMethod;

        GridType(String methodName) {
            requireNonNull(methodName);

            //  Use reflection to get the correct method for this grid type constant.
            try {
                this.spacingMethod = GridSpacing.class.getMethod(methodName, new Class[]{Integer.TYPE});
            } catch (NoSuchMethodException | SecurityException ex) {
                Logger.getLogger(GridType.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public String toString() {
            return TITLES[ordinal()];
        }

        /**
         * Return a list of "n" values between 0 and 1 (inclusive) using this grid spacing
         * type.
         *
         * @param n The number of points to include in the spacing.
         * @return A list of points, of size <code>n</code>, with the spacing of this
         *         GridType.
         */
        public List<Double> spacing(int n) {
            //  Use reflection to call the correct method in the GridSpacing class.
            List<Double> output = null;
            try {
                output = (List<Double>)spacingMethod.invoke(GridSpacing.class, new Object[]{n});
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(GridType.class.getName()).log(Level.SEVERE, null, ex);
            }
            return output;
        }
    }
    
    /**
     * Returns a list of values evenly or linearly spaced between 0 and 1.
     *
     * @param n The number of points to include in the spacing.
     * @return A list of points, of size <code>n</code>, evenly spaced between 0
     * and 1.
     */
    public static List<Double> linear(int n) {
        FastTable<Double> values = FastTable.newInstance();
        int nm1 = n - 1;
        for (int i = 0; i < n; ++i) {
            double s = (double)(i) / nm1;
            values.add(s);
        }
        return values;
    }

    /**
     * Returns a list of values with a cosine spacing between 0 and 1. This
     * distribution tends to group points near the ends of the list.
     *
     * @param n The number of points to include in the spacing.
     * @return A list of points, of size <code>n</code>, with a cosine spacing
     * between 0 and 1.
     */
    public static List<Double> cos(int n) {
        FastTable<Double> values = FastTable.newInstance();
        int nm1 = n - 1;
        for (int i = 0; i < n; ++i) {
            double s = (1 - MathLib.cos(MathLib.PI * i / nm1)) / 2;
            values.add(s);
        }
        return values;
    }

    /**
     * Returns a list of values with an arc-cosine spacing between 0 and 1. This
     * distribution tends to group points near the middle of the list.
     *
     * @param n The number of points to include in the spacing.
     * @return A list of points, of size <code>n</code>, with an arc-cosine
     * spacing between 0 and 1.
     */
    public static List<Double> acos(int n) {
        FastTable<Double> values = FastTable.newInstance();
        int nm1 = n - 1;
        for (int i = 0; i < n; ++i) {
            double s = MathLib.acos(1 - 2. * i / nm1) / MathLib.PI;
            values.add(s);
        }
        return values;
    }

    /**
     * Returns a list of values with a square root spacing between 0 and 1. This
     * distribution tends to group points near the end of the list.
     *
     * @param n The number of points to include in the spacing.
     * @return A list of points, of size <code>n</code>, with a square root
     * spacing between 0 and 1.
     */
    public static List<Double> sqrt(int n) {
        FastTable<Double> values = FastTable.newInstance();
        int nm1 = n - 1;
        for (int i = 0; i < n; ++i) {
            double s = MathLib.sqrt(((double)(i)) / nm1);
            values.add(s);
        }
        return values;
    }

    /**
     * Returns a list of values with a 1-square root spacing between 0 and 1.
     * This distribution tends to group points near the start of the list.
     *
     * @param n The number of points to include in the spacing.
     * @return A list of points, of size <code>n</code>, with a square root
     * spacing between 0 and 1.
     */
    public static List<Double> oneMinusSqrt(int n) {
        FastTable<Double> values = FastTable.newInstance();
        int nm1 = n - 1;
        for (int i = 0; i < n; ++i) {
            double s = 1 - MathLib.sqrt(((double)(i)) / nm1);
            values.add(s);
        }
        Collections.reverse(values);
        return values;
    }

    /**
     * Returns a list of values with a squared spacing between 0 and 1. This
     * distribution tends to group points near the start of the list.
     *
     * @param n The number of points to include in the spacing.
     * @return A list of points, of size <code>n</code>, with a squared spacing
     * between 0 and 1.
     */
    public static List<Double> sqr(int n) {
        FastTable<Double> values = FastTable.newInstance();
        int nm1 = n - 1;
        for (int i = 0; i < n; ++i) {
            double r = ((double)(i)) / nm1;
            double s = r * r;
            values.add(s);
        }
        return values;
    }

    /**
     * Returns a list of values with a hyperbolic tangent spacing between 0 and
     * 1. This distribution tends to group points near the ends of the list
     * depending on the inputs.
     *
     * @param n The number of points to include in the spacing.
     * @param ds1 The fractional arc-length for the 1st segment at the start of
     * the list.
     * @param ds2 The fractional arc-length for the last segment at the end of
     * the list.
     * @return A list of points, of size <code>n</code>, with a hyperbolic
     * tangent spacing between 0 and 1.
     */
    public static List<Double> tanh(int n, double ds1, double ds2) {
        //  Reference: Thompson, J.F., Warsi, Z., Wayne Mastin, C., "Numerical Grid Generation", Chapter 8, pg 218.

        FastTable<Double> values = FastTable.newInstance();

        double A = MathLib.sqrt(ds2/ds1);
        int capI = n - 1;
        double B = 1. / (capI * MathLib.sqrt(ds1 * ds2));
        if (B < 1) {
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("invTanhSpacing"), (int)(1. / MathLib.sqrt(ds1 * ds2))));
        }

        //  Use a root finder to solve the point: sinh(delta)/delta = B for delta.
        //  Initial guess by series expansion.
        double delta = MathLib.sqrt(6. * (B - 1.));
        TanhPEvaluatable sinhFunc = TanhPEvaluatable.newInstance(B);
        try {
            delta = Roots.findRoot1D(sinhFunc, delta / 2, delta * 2, 1e-10);
        } catch (Exception err) {
            Logger.getLogger(GridType.class.getName()).log(Level.SEVERE, null, err);
        }
        TanhPEvaluatable.recycle(sinhFunc);

        //  Create the spacing points.
        values.add(0.);
        for (int i = 1; i < capI; ++i) {
            double ratio = ((double)i) / capI;
            double u = 0.5 * (1. + MathLib.tanh(delta * (ratio - 0.5)) / MathLib.tanh(0.5 * delta));
            double s = u / (A + (1. - A) * u);
            values.add(s);
        }
        values.add(1.);

        return values;
    }

    /**
     * An Evaulatable1D that is used to solve an point related to a tanhp
     * distribution.
     */
    private static class TanhPEvaluatable extends AbstractEvaluatable1D {

        private double _B;

        public static TanhPEvaluatable newInstance(double B) {
            TanhPEvaluatable o = FACTORY.object();
            o._B = B;
            return o;
        }

        /**
         * Calculates: sinh(x)/(x*B) - 1. When this is zero, then the solution
         * has been found.
         */
        @Override
        public double function(double x) throws RootException {
            double func = MathLib.sinh(x) / (x * _B) - 1.0;
            return func;
        }

        public static void recycle(TanhPEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private TanhPEvaluatable() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<TanhPEvaluatable> FACTORY = new ObjectFactory<TanhPEvaluatable>() {
            @Override
            protected TanhPEvaluatable create() {
                return new TanhPEvaluatable();
            }
        };
    }

}
