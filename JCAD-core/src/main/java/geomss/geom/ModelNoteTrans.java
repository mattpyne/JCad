/**
 * ModelNoteTrans -- A GeomTransform that has a GenModelNote for a child.
 *
 * Copyright (C) 2014-2018, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.awt.Font;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.lang.Immutable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} element that refers to a {@link GenModelNote} object and
 * masquerades as a GenModelNote object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 10, 2014
 * @version April 10, 2018
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class ModelNoteTrans extends GenModelNote implements GeomTransform<GenModelNote> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private GenModelNote _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link ModelNoteTrans} instance holding the specified
     * {@link GenModelNote} and {@link GTransform}.
     *
     * @param child     The note that is the child of this transform element (may not be
     *                  <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     * @throws DimensionException if the input element is not 3D.
     */
    public static ModelNoteTrans newInstance(GenModelNote child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(RESOURCES.getString("dimensionNot3").
                    replace("<TYPE/>", "note").replace("<DIM/>", String.valueOf(child.getPhyDimension())));

        ModelNoteTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;
        child.copyState(obj);

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     */
    @Override
    public GenModelNote getChild() {
        return _child;
    }

    /**
     * Return the text string associated with this note object.
     *
     * @return The text string associated with this note object.
     */
    @Override
    public String getNote() {
        return _child.getNote();
    }

    /**
     * Return the location of this note in model space.
     *
     * @return The location of this note in model space.
     */
    @Override
    public Point getLocation() {
        return _TM.transform(_child.getLocation());
    }

    /**
     * Return the vector indicating the horizontal axis direction for the text.
     *
     * @return The vector indicating the horizontal axis direction for the text.
     */
    @Override
    public GeomVector<Dimensionless> getXHat() {
        return _child.getXHat().getTransformed(_TM);
    }

    /**
     * Return the vector indicating the vertical axis direction (or ascent direction) for
     * the text.
     *
     * @return The vector indicating the vertical axis direction (or ascent direction) for
     *         the text.
     */
    @Override
    public GeomVector<Dimensionless> getYHat() {
        return _child.getYHat().getTransformed(_TM);
    }

    /**
     * Return the height of the text box in model units.
     *
     * @return The height of the text box in model units.
     */
    @Override
    public Parameter<Length> getHeight() {
        double zScale = _TM.getScaleVector().getValue(2);
        return _child.getHeight().times(zScale);
    }

    /**
     * Return the font used to display this note.
     *
     * @return The font used to display this note.
     */
    @Override
    public Font getFont() {
        return _child.getFont();
    }

    /**
     * Return an immutable version of this note.
     *
     * @return An immutable version of this note.
     */
    @Override
    public ModelNote immutable() {
        return copyToReal();
    }

    /**
     * Return a new note object identical to this one, but with the specified font.
     *
     * @param font The font for the copy of this note to use. May not be null.
     * @return A new note object identical to this one, but with the specified font.
     */
    @Override
    public ModelNoteTrans changeFont(Font font) {
        requireNonNull(font);
        ModelNoteTrans note = ModelNoteTrans.newInstance(_child.changeFont(font), _TM);
        copyState(note);
        return note;
    }

    /**
     * Return a new note object identical to this one, but with the specified location in
     * model space.
     *
     * @param location The location for the copy of this note.  May not be null.
     * @return A new note object identical to this one, but with the specified location in
     *         model space.
     */
    @Override
    public ModelNoteTrans changeLocation(GeomPoint location) {
        requireNonNull(location);
        ModelNoteTrans note = ModelNoteTrans.newInstance(_child.changeLocation(location), _TM);
        copyState(note);
        return note;
    }

    /**
     * Return a new note object identical to this one, but with the specified height in
     * model space.
     *
     * @param height The height for the copy of this note.  May not be null.
     * @return A new note object identical to this one, but with the specified height in
     *         model space.
     */
    @Override
    public ModelNoteTrans changeHeight(Parameter<Length> height) {
        requireNonNull(height);
        ModelNoteTrans note = ModelNoteTrans.newInstance(_child.changeHeight(height), _TM);
        copyState(note);
        return note;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public ModelNote copyToReal() {
        //  Transform the geometry.
        Point location = getLocation();
        Parameter<Length> height = getHeight();

        //  Create a new note from the old one.
        ModelNote note = _child.immutable().changeLocation(location).changeHeight(height);
        copyState(note);

        return note;
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns 3.
     */
    @Override
    public int getPhyDimension() {
        return 3;
    }

    /**
     * Return <code>true</code> if this Note contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     */
    @Override
    public boolean isValid() {
        return _child.isValid();
    }

    /**
     * Returns a copy of this ModelNoteTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this note.
     */
    @Override
    public ModelNoteTrans copy() {
        return copyOf(this);
    }

    /**
     * Returns the unit in which the note location Point is stored.
     */
    @Override
    public final Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this note but with the location stated in the specified
     * unit.
     *
     * @param unit the length unit of the note to be returned. May not be null.
     * @return an equivalent of this note but with location stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public ModelNoteTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        ModelNoteTrans note = ModelNoteTrans.newInstance(_child.to(unit), _TM);
        copyState(note);
        return note;
    }

    /**
     * Return the equivalent of this note converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the point to return. MUST equal 3.
     * @return The equivalent of this note converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public ModelNoteTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Compares this ModelNoteTrans against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this note is identical to that note;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        ModelNoteTrans that = (ModelNoteTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<ModelNoteTrans> XML = new XMLFormat<ModelNoteTrans>(ModelNoteTrans.class) {

        @Override
        public ModelNoteTrans newInstance(Class<ModelNoteTrans> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, ModelNoteTrans obj) throws XMLStreamException {
            GenModelNote.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            GenModelNote child = xml.getNext();
            obj._child = child;

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);

        }

        @Override
        public void write(ModelNoteTrans obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            GenModelNote.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private ModelNoteTrans() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<ModelNoteTrans> FACTORY = new ObjectFactory<ModelNoteTrans>() {
        @Override
        protected ModelNoteTrans create() {
            return new ModelNoteTrans();
        }

        @Override
        protected void cleanup(ModelNoteTrans obj) {
            obj.reset();
            obj._TM = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static ModelNoteTrans copyOf(ModelNoteTrans original) {
        ModelNoteTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

}
