/**
 * Vector_stp -- Holds the floating point coordinates of an nD vector.
 *
 * Copyright (C) 2009-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.param.ParameterVector;
import java.text.MessageFormat;
import java.util.List;
import static java.util.Objects.nonNull;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.lang.MathLib;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A container that holds the coordinates of an n-dimensional vector which indicates
 * direction, but not position.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: June 13, 2009
 * @version September 10, 2016
 *
 * @param <Q> The Quantity (unit type) associated with this vector's coordinate values.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class Vector<Q extends Quantity> extends GeomVector<Q> implements ValueType {

    /**
     * The coordinate values are stored in this array.
     */
    private transient double[] _data;

    /**
     * The number of dimensions in the data.
     */
    private int _numDims;

    /**
     * Holds the unit of the coordinates.
     */
    private Unit<Q> _unit;

    //  The origin for this vector.
    private Point _origin;

    /**
     * Returns a dimensionless <code>Vector_stp</code> instance of the specified dimension
     * with all the coordinate values set to zero.
     *
     * @param numDims the physical dimension of the vector to create.
     * @return the dimensionless vector having the specified dimension and zeros for
     *         values.
     */
    public static Vector<Dimensionless> newInstance(int numDims) {
        Vector V = Vector.newInstancePvt(numDims, Dimensionless.UNIT);
        
        for (int i = 0; i < numDims; ++i)
            V._data[i] = 0;

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance of the specified dimension and units with
     * all the coordinate values set to zero.
     *
     * @param <Q>     The Quantity (unit type) associated with the new vector's coordinate
     *                values.
     * @param numDims the physical dimension of the vector to create.
     * @param unit    the unit in which the coordinates are stated. May not be null.
     * @return the vector having the specified dimension and zero meters for values.
     */
    public static <Q extends Quantity> Vector<Q> newInstance(int numDims, Unit<Q> unit) {
        Vector V = Vector.newInstancePvt(numDims, unit);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = 0;

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance containing the specified GeomVector values.
     *     
     * @param <Q>    The Quantity (unit type) associated with the new vector's coordinate
     *               values.
     * @param vector The GeomVector to be placed in a new Vector_stp instance. May not be null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector<Q> valueOf(GeomVector<Q> vector) {
        if (vector instanceof Vector)
            return (Vector<Q>)vector;

        Vector V = Vector.newInstancePvt(vector);

        int numDims = V._numDims;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = vector.getValue(i);

        return V;
    }

    /**
     * Returns a dimensionless <code>Vector_stp</code> instance holding the specified
     * <code>double</code> value or values.
     *
     * @param x the dimensionless coordinate values. May not be null.
     * @return the vector having the specified value and dimensionless units.
     */
    public static Vector<Dimensionless> valueOf(double... x) {
        int numDims = x.length;
        Vector V = Vector.newInstancePvt(numDims, Dimensionless.UNIT);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = x[i];

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance holding the specified <code>double</code>
     * value or values stated in the specified units.
     *     
     * @param <Q>  The Quantity (unit type) associated with the new vector's coordinate
     *             values.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @param x    the coordinate values stated in the specified unit. May not be null.
     * @return the vector having the specified value.
     */
    public static <Q extends Quantity> Vector<Q> valueOf(Unit<Q> unit, double... x) {
        int numDims = x.length;
        Vector V = Vector.newInstancePvt(numDims, unit);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = x[i];

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance containing the specified vector of Parameter
     * values with compatible units. All the values are converted to the same units as the
     * 1st value.
     *     
     * @param <Q>    The Quantity (unit type) associated with the new vector's coordinate
     *               values.
     * @param vector the vector of Parameter values stated in the specified unit. May not be null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector<Q> valueOf(org.jscience.mathematics.vector.Vector<Parameter<Q>> vector) {
        int numDims = vector.getDimension();
        Unit<Q> unit = vector.get(0).getUnit();
        Vector V = Vector.newInstancePvt(numDims, unit);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = vector.get(i).getValue(unit);

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance containing the specified list of Parameter
     * values with compatible units. All the values are converted to the same units as the
     * 1st value.
     *
     * @param <Q>    The Quantity (unit type) associated with the new vector's coordinate
     *               values.
     * @param values the list of Parameter values stated in the specified unit. May not be
     *               null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector<Q> valueOf(List<Parameter<Q>> values) {
        int numDims = values.size();
        Unit<Q> unit = values.get(0).getUnit();
        Vector<Q> V = Vector.newInstancePvt(numDims, unit);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = values.get(i).getValue(unit);

        return V;
    }

    /**
     * Returns a {@link Vector} instance containing the specified vector of Float64 values
     * stated in the specified units.
     *
     * @param <Q>    The Quantity (unit type) associated with the new vector's coordinate
     *               values.
     * @param vector the vector of Float64 values stated in the specified unit. May not be
     *               null.
     * @param unit   The unit in which the coordinates are stated. May not be null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector<Q> valueOf(org.jscience.mathematics.vector.Vector<Float64> vector, Unit<Q> unit) {
        int numDims = vector.getDimension();
        Vector V = Vector.newInstancePvt(numDims, unit);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = vector.get(i).doubleValue();

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance holding the specified <code>Parameter</code>
     * values. All the values are converted to the same units as the first value.
     *     
     * @param <Q>    The Quantity (unit type) associated with the new vector's coordinate
     *               values.
     * @param values A list of values to store in the vector. May not be null.
     * @return the vector having the specified values in the units of x.
     */
    public static <Q extends Quantity> Vector<Q> valueOf(Parameter<Q>... values) {
        int numDims = values.length;
        Unit<Q> unit = values[0].getUnit();
        Vector V = Vector.newInstancePvt(numDims, unit);

        for (int i = 0; i < numDims; ++i)
            V._data[i] = values[i].getValue(unit);

        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance holding the specified <code>GeomPoint</code>
     * values.
     *
     * @param point A point who's coordinates are to be stored in the vector (making it a
     *              position vector). May not be null.
     * @return the vector having the specified point coordinate values in it.
     */
    public static Vector<Length> valueOf(GeomPoint point) {
        int numDims = point.getPhyDimension();
        Unit<Length> unit = point.getUnit();
        Vector V = Vector.newInstancePvt(numDims, unit);

        Point pnt = point.immutable();
        for (int i = 0; i < numDims; ++i)
            V._data[i] = pnt.getValue(i, unit);
        V._origin = pnt;

        return V;
    }

    /**
     * Returns the number of physical dimensions of this vector.
     *
     * @return The number of physical dimensions of this vector.
     */
    @Override
    public int getPhyDimension() {
        return _numDims;
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in this vector's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException
     * <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i) {
        return _data[i];
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in the specified units.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException
     * <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Q> unit) {
        double value = _data[i];
        Unit<Q> thisUnit = getUnit();
        if (unit.equals(thisUnit))
            return value;
        UnitConverter cvtr = Parameter.converterOf(thisUnit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            return value;
        }
        return cvtr.convert(value);
    }

    /**
     * Set the origin point for this vector. The origin is used as a reference for drawing
     * the vector and is <i>not</i> a part of the state of this vector and is not used in
     * any calculations with this vector.
     * 
     * @param origin The point to use as the origin of this vector (may not be null).
     */
    @Override
    public void setOrigin(Point origin) {
        requireNonNull(origin, MessageFormat.format(RESOURCES.getString("paramNullErr"), "origin"));
        _origin = origin;
    }

    /**
     * Return the origin point for this vector. If no origin has been explicitly set,
     * then the coordinate system origin in the default units is returned.
     *
     * @return The origin point for this vector.
     */
    @Override
    public Point getOrigin() {
        if (isNull(_origin))
            return Point.newInstance(getPhyDimension());
        return _origin;
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector (square root
     * of the dot product of this vector and itself).
     *
     * @return <code>this.norm().doubleValue()</code>.
     */
    @Override
    public double normValue() {
        double[] data = _data;
        double s2 = data[0];
        s2 *= s2;
        for (int i = 1; i < _numDims; ++i) {
            double vi = data[i];
            s2 += vi * vi;
        }
        return MathLib.sqrt(s2);
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    @Override
    public Vector<Q> opposite() {
        Vector V = newInstancePvt(this);

        int numDims = _numDims;
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = -thisData[i];

        return V;
    }

    /**
     * Returns the sum of this vector with the one specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector<Q> plus(GeomVector<Q> that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit<Q> unit = _unit;

        Vector V = newInstancePvt(this);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] + that.getValue(i, unit);

        return V;
    }

    /**
     * Returns the sum of this vector with the parameter specified. The input parameter is
     * added to each component of this vector. The unit of the output vector will be the
     * units of this vector.
     *
     * @param that the parameter to be added to each element of this vector. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    @Override
    public Vector<Q> plus(Parameter<Q> that) {
        int numDims = _numDims;
        Unit<Q> unit = _unit;

        //  Convert input parameter to the units of this vector.
        double thatValue = that.getValue(unit);

        Vector V = newInstancePvt(this);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] + thatValue;

        return V;
    }

    /**
     * Returns the difference between this vector and the one specified. The unit of the
     * output vector will be the units of this vector.
     *
     * @param that the vector to be subtracted from this vector. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector<Q> minus(GeomVector<Q> that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit<Q> unit = _unit;

        Vector V = newInstancePvt(this);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] - that.getValue(i, unit);

        return V;
    }

    /**
     * Subtracts the supplied Parameter from each element of this vector and returns the
     * result. The unit of the output vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from each element of this vector. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Vector<Q> minus(Parameter<Q> that) {
        int numDims = _numDims;
        Unit<Q> unit = _unit;

        //  Convert input parameter to the units of this vector.
        double thatValue = that.getValue(unit);

        Vector V = newInstancePvt(this);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] - thatValue;

        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient (dimensionless).
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Vector<Q> times(double k) {
        Vector V = newInstancePvt(this);

        int numDims = _numDims;
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] * k;

        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier. May not be null.
     * @return <code>this · k</code>
     */
    @Override
    public Vector<? extends Quantity> times(Parameter<?> k) {
        int numDims = _numDims;
        Unit unit = Parameter.productOf(this.getUnit(), k.getUnit());

        double thatValue = k.getValue();

        Vector V = newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] * thatValue;
        V._origin = _origin;

        return V;
    }

    /**
     * Returns the dot product (scalar product) of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    @Override
    public Parameter<? extends Quantity> times(GeomVector<?> that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit unit = Parameter.productOf(this.getUnit(), that.getUnit());

        Float64 s = this.toFloat64Vector().times(that.toFloat64Vector());

        return Parameter.valueOf(s.doubleValue(), unit);
    }

    /**
     * Returns the element-by-element product of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this .* that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     */
    @Override
    public Vector<? extends Quantity> timesEBE(GeomVector<?> that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit unit = Parameter.productOf(this.getUnit(), that.getUnit());

        Vector V = newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] * that.getValue(i);
        V._origin = _origin;

        return V;
    }

    /**
     * Returns the cross product of two vectors.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this x that</code>
     * @throws DimensionException if
     * <code>(that.getDimension() != this.getDimension())</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    @Override
    public Vector<? extends Quantity> cross(GeomVector<?> that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit unit = Parameter.productOf(this.getUnit(), that.getUnit());

        Float64Vector xv = this.toFloat64Vector().cross(that.toFloat64Vector());

        Vector V = Vector.valueOf(xv, unit);
        V._origin = _origin;

        return V;
    }

    /**
     * Returns this vector with each element divided by the specified divisor
     * (dimensionless).
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    @Override
    public Vector<Q> divide(double divisor) {
        return times(1.0 / divisor);
    }

    /**
     * Returns this vector with each element divided by the specified divisor.
     *
     * @param divisor the divisor. May not be null.
     * @return <code>this / divisor</code>.
     */
    @Override
    public Vector<? extends Quantity> divide(Parameter<?> divisor) {
        return times(divisor.inverse());
    }

    /**
     * Return an immutable version of this vector.
     *
     * @return An immutable version of this vector.
     */
    @Override
    public Vector<Q> immutable() {
        return this;
    }

    /**
     * Returns this vector converted to a unit vector by dividing all the vector's
     * elements by the length ({@link #norm}) of this vector.
     *
     * @return This vector converted to a unit vector by dividing all the vector's
     *         elements by the length of this vector.
     */
    @Override
    public Vector<Dimensionless> toUnitVector() {
        double magnitude = this.normValue();
        if (this.getUnit().equals(Dimensionless.UNIT) && MathLib.abs(magnitude - 1) <= Parameter.SQRT_EPS)
            return (Vector<Dimensionless>)this;
        //if (MathLib.abs(magnitude) <= Parameter.SQRT_EPS)
        //    throw new ArithmeticException(RESOURCES.getString("zeroMagVector"));

        int numDims = _numDims;
        Vector V = newInstancePvt(numDims, Dimensionless.UNIT);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i] / magnitude;
        V._origin = _origin;

        return V;
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this vector are stated
     * in.
     *
     * @return The units in which this vector is stated in.
     */
    @Override
    public Unit getUnit() {
        return _unit;
    }

    /**
     * Returns the equivalent to this vector but stated in the specified unit.
     *
     * @param unit the unit of the vector to be returned. May not be null.
     * @return an equivalent to this vector but stated in the specified unit.
     * @throws ConversionException if the the input unit is not compatible with this unit.
     */
    @Override
    public Vector to(Unit unit) {
        if (_unit == unit || unit.equals(_unit))
            return this;

        UnitConverter cvtr = Parameter.converterOf(_unit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            return this;
        }

        int numDims = _numDims;
        Vector V = Vector.newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = cvtr.convert(thisData[i]);
        V._origin = _origin;

        return V;
    }

    /**
     * Return a copy of this vector converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the vector to return.
     * @return A copy of this vector converted to the new dimensions.
     */
    @Override
    public Vector<Q> toDimension(int newDim) {
        if (newDim < 1)
            throw new IllegalArgumentException(RESOURCES.getString("vectorDimGT0"));
        int thisDim = this.getPhyDimension();
        if (newDim == thisDim)
            return this;

        int numDims = newDim;
        if (newDim > thisDim)
            numDims = thisDim;

        Vector<Q> V = Vector.newInstancePvt(newDim, _unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            V._data[i] = thisData[i];
        for (int i = numDims; i < newDim; ++i)
            V._data[i] = 0;
        V._origin = (isNull(_origin) ? null : _origin.toDimension(newDim));

        return V;
    }

    /**
     * Returns a copy of this Vector_stp instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public Vector<Q> copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public Vector<Q> copyToReal() {
        return copy();
    }

    /**
     * Returns a <code>ParameterVector</code> representation of this vector.
     *
     * @return A ParameterVector that is equivalent to this vector.
     */
    @Override
    public ParameterVector<Q> toParameterVector() {
        return ParameterVector.valueOf(this.toFloat64Vector(), getUnit());
    }

    /**
     * Returns a <code>Float64Vector</code> containing the elements of this vector stated
     * in the current units.
     *
     * @return A Float64Vector that contains the elements of this vector in the current
     *         units.
     */
    @Override
    public Float64Vector toFloat64Vector() {
        FastTable<Float64> table = FastTable.newInstance();
        for (int i = 0; i < _numDims; ++i)
            table.add(Float64.valueOf(_data[i]));

        Float64Vector f64 = Float64Vector.valueOf(table);

        FastTable.recycle(table);
        return f64;
    }

    /**
     * Compares this vector against the specified object for strict equality (same values
     * and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this vector is identical to that vector;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        Vector that = (Vector)obj;
        if (this._numDims != that._numDims)
            return false;
        if (!this._unit.equals(that._unit))
            return false;
        for (int i = 0; i < _numDims; ++i)
            if (this._data[i] != that._data[i])
                return false;

        return super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode();

        hash = hash * 31 + _unit.hashCode();
        for (int i = 0; i < _numDims; ++i) {
            hash = hash * 31 + makeVarCode(_data[i]);
        }

        return hash;
    }

    private static int makeVarCode(double value) {
        long bits = Double.doubleToLongBits(value);
        int var_code = (int)(bits ^ (bits >>> 32));
        return var_code;
    }
    
    /**
     * Recycles a <code>Vector_stp</code> instance immediately (on the stack when executing in
     * a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(Vector instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<Vector> XML = new XMLFormat<Vector>(Vector.class) {

        @Override
        public Vector newInstance(Class<Vector> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, Vector obj) throws XMLStreamException {

            //  Read in the units.
            Unit unit = Unit.valueOf(xml.getAttribute("unit"));
            if (!(Length.UNIT.isCompatible(unit) || Dimensionless.UNIT.isCompatible(unit)))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("incompatibleUnits"),
                                "Vector_stp", "length"));
            obj._unit = unit;

            // Call parent read.
            GeomVector.XML.read(xml, obj);

            //  Read in the origin point (if it is there).
            obj._origin = xml.get("Origin");
            
            //  Read in the vector components.
            FastTable<Float64> valueList = FastTable.newInstance();
            while (xml.hasNext()) {
                Float64 value = xml.getNext();
                valueList.add(value);
            }
            int numDims = valueList.size();
            obj._numDims = numDims;
            obj._data = ArrayFactory.DOUBLES_FACTORY.array(numDims);
            for (int i = 0; i < numDims; ++i)
                obj._data[i] = valueList.get(i).doubleValue();
        }

        @Override
        public void write(Vector obj, OutputElement xml) throws XMLStreamException {
            //  Write out the units.
            xml.setAttribute("unit", obj.getUnit().toString());
            
            // Call parent write.
            GeomVector.XML.write(obj, xml);

            //  Write out the origin point if it is not the coordinate system origin.
            Point o = obj._origin;
            if (nonNull(o) && !o.isApproxEqual(Point.newInstance(obj._numDims)))
                xml.add(obj._origin, "Origin");
            
            //  Write out the vector components.
            int size = obj._numDims;
            for (int i = 0; i < size; ++i)
                xml.add(Float64.valueOf(obj._data[i]));
            
        }
    };

    /**
     * Allocate a recyclable array that can contain Vector_stp objects using factory methods.
     * <p>
     * WARNING: The array returned may <I>not</I> be zeroed. Any object references in the
     * returned array must be assumed to be invalid. Also, the returned array may be
     * larger than the requested size! The array returned by this method can be recycled
     * by recycleArray().
     * </p>
     *
     * @param size The minimum number of elements for the returned array to contain.
     * @return An array that can contain Vector_stp objects allocated using factory methods.
     * @see #recycleArray
     */
    public static Vector[] allocateArray(int size) {
        return VECTORARRAY_FACTORY.array(size);
    }

    /**
     * Recycle an array of Vector_stp objects that was created by Vector_stp.allocateArray().
     *
     * @param arr The array to be recycled. The array must have been created by this the
     *            allocateArray() method!
     * @see #allocateArray
     */
    public static void recycleArray(Vector[] arr) {
        VECTORARRAY_FACTORY.recycle(arr);
    }
    
    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private static ArrayFactory<Vector[]> VECTORARRAY_FACTORY = new ArrayFactory<Vector[]>() {
        @Override
        protected Vector[] create(int size) {
            return new Vector[size];
        }
    };

    private Vector() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<Vector> FACTORY = new ObjectFactory<Vector>() {
        @Override
        protected Vector create() {
            return new Vector();
        }

        @Override
        protected void cleanup(Vector obj) {
            obj.reset();
        }
    };

    private static Vector newInstancePvt(int numDims, Unit unit) {
        Vector V = FACTORY.object();
        V._unit = requireNonNull(unit);
        V._numDims = numDims;
        V._data = ArrayFactory.DOUBLES_FACTORY.array(numDims);
        V._origin = null;
        return V;
    }

    private static Vector newInstancePvt(GeomVector original) {
        int numDims = original.getPhyDimension();
        Unit unit = original.getUnit();
        Vector V = newInstancePvt(numDims, unit);
        V._origin = original.getOrigin();
        return V;
    }

    @SuppressWarnings("unchecked")
    private static <Q extends Quantity> Vector<Q> copyOf(Vector<Q> original) {
        Vector<Q> V = newInstancePvt(original);
        System.arraycopy(original._data, 0, V._data, 0, original._numDims);
        V._origin = (isNull(original._origin) ? null : original._origin.copy());
        original.copyState(V);
        return V;
    }
}
