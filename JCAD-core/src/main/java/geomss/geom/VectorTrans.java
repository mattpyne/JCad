/**
 * VectorTrans -- A GeomTransform that has an GeomVector for a child.
 *
 * Copyright (C) 2009-2018, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.param.ParameterVector;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A {@link GeomTransform} element that refers to a {@link GeomVector} object and
 * masquerades as a GeomVector object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 13, 2009
 * @version April 10, 2018
 *
 * @param <Q> The Quantity (unit type) of the elements of this vector.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class VectorTrans<Q extends Quantity> extends GeomVector<Q> implements GeomTransform<GeomVector> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * Just the rotational and scale portion of the transformation.
     */
    private GTransform _TM_RotScale;

    /**
     * The object that is the child of this transform.
     */
    private GeomVector<Q> _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link VectorTrans} instance holding the specified {@link GeomVector}
     * and {@link GTransform}.
     *
     * @param <Q>       The Quantity (unit type) of this vector.
     * @param child     The vector that is the child of this transform element (may not be
     *                  <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     * @throws DimensionException if the input element is not 3D.
     */
    public static <Q extends Quantity> VectorTrans<Q> newInstance(GeomVector<Q> child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "vector", child.getPhyDimension()));

        VectorTrans<Q> obj = FACTORY.object();
        obj._TM = transform;
        obj._TM_RotScale = GTransform.valueOf(transform.getRotationScale());
        obj._child = child;
        child.copyState(obj);

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        _TM_RotScale = GTransform.valueOf(transform.getRotationScale());
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     */
    @Override
    public GeomVector<Q> getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public Vector<Q> copyToReal() {
        Unit<Q> unit = (Unit<Q>)_child.getUnit();
        Float64Vector dirV;
        StackContext.enter();
        try {

            //  The direction of the vector is transformed by rotation and scaling only, not by translation!
            Point pt2 = _TM_RotScale.transform(Point.valueOf(_child));

            FastTable<Float64> values = FastTable.newInstance();
            int dim = pt2.getPhyDimension();
            for (int i = 0; i < dim; ++i) {
                values.add(Float64.valueOf(pt2.getValue(i)));
            }
            dirV = Float64Vector.valueOf(values);
            dirV = StackContext.outerCopy(dirV);

        } finally {
            StackContext.exit();
        }

        Vector<Q> V = Vector.valueOf(dirV, unit);
        this.copyState(V);

        //  The origin is transformed by rotation, scale, AND translation.
        V.setOrigin(_TM.transform(_child.getOrigin()));

        return V;
    }

    /**
     * Recycles a <code>VectorTrans</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(VectorTrans instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns 3.
     */
    @Override
    public int getPhyDimension() {
        return 3;
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in this vector's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i) {
        Vector pTrans = copyToReal();
        double value = pTrans.getValue(i);
        Vector.recycle(pTrans);
        return value;
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in the specified units.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Q> unit) {
        requireNonNull(unit);
        Vector pTrans = copyToReal();
        double value = pTrans.getValue(i, unit);
        Vector.recycle(pTrans);
        return value;
    }

    /**
     * Set the origin point for this vector. The origin is used as a reference for drawing
     * the vector and is <i>not</i> a part of the state of this vector and is not used in
     * any calculations with this vector.
     * 
     * @param origin The new origin point for the vector.  May not be null.
     */
    @Override
    public void setOrigin(Point origin) {
        requireNonNull(origin, MessageFormat.format(RESOURCES.getString("paramNullErr"), "origin"));
        _child.setOrigin(_TM.transpose().transform(origin));
    }

    /**
     * Return the origin point for this vector. If no origin has been explicitly set, then
     * the coordinate system origin is returned.
     *
     * @return The origin point for this vector.
     */
    @Override
    public Point getOrigin() {
        return _TM.transform(_child.getOrigin());
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of the vector from the origin
     * to this point (square root of the dot product of the origin-to-this-point vector
     * and itself).
     *
     * @return <code>this.norm().doubleValue()</code>.
     */
    @Override
    public double normValue() {
        Vector pTrans = copyToReal();
        double normV = pTrans.normValue();
        Vector.recycle(pTrans);
        return normV;
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    @Override
    public Vector<Q> opposite() {
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.opposite();
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the sum of this vector with the one specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector<Q> plus(GeomVector<Q> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.plus(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the sum of this vector with the parameter specified. The input parameter is
     * added to each component of this vector. The unit of the output vector will be the
     * units of this vector.
     *
     * @param that the parameter to be added to each element of this vector. May not be null.
     * @return <code>this + that</code>.
     */
    @Override
    public Vector<Q> plus(Parameter<Q> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.plus(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the difference between this vector and the one specified. The unit of the
     * output vector will be the units of this vector.
     *
     * @param that the vector to be subtracted from this vector. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector<Q> minus(GeomVector<Q> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.minus(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Subtracts the supplied Parameter from each element of this vector and returns the
     * result. The unit of the output vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from each element of this vector. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Vector<Q> minus(Parameter<Q> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.minus(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient (dimensionless).
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Vector<Q> times(double k) {
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.times(k);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier. May not be null.
     * @return <code>this · k</code>
     */
    @Override
    public Vector<? extends Quantity> times(Parameter<?> k) {
        requireNonNull(k);
        Vector pTrans = copyToReal();
        Vector V = pTrans.times(k);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the dot product (scalar product) of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    @Override
    public Parameter<? extends Quantity> times(GeomVector<?> that) {
        requireNonNull(that);
        Vector pTrans = copyToReal();
        Parameter P = pTrans.times(that);
        Vector.recycle(pTrans);
        return P;
    }

    /**
     * Returns the element-by-element product of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this .* that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     */
    @Override
    public Vector<? extends Quantity> timesEBE(GeomVector<?> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector V = pTrans.timesEBE(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the cross product of two vectors.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this x that</code>
     * @throws DimensionException if
     * <code>(that.getDimension() != this.getDimension())</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    @Override
    public Vector<? extends Quantity> cross(GeomVector<?> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector V = pTrans.cross(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns this vector with each element divided by the specified divisor
     * (dimensionless).
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    @Override
    public Vector<Q> divide(double divisor) {
        Vector<Q> pTrans = copyToReal();
        Vector<Q> V = pTrans.divide(divisor);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns this vector with each element divided by the specified divisor.
     *
     * @param that the divisor. May not be null.
     * @return <code>this / that</code>.
     */
    @Override
    public Vector<? extends Quantity> divide(Parameter<?> that) {
        requireNonNull(that);
        Vector<Q> pTrans = copyToReal();
        Vector V = pTrans.divide(that);
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Return an immutable version of this vector.
     *
     * @return An immutable version of this vector.
     */
    @Override
    public Vector<Q> immutable() {
        return copyToReal();
    }

    /**
     * Returns this vector converted to a unit vector by dividing all the vector's
     * elements by the length ({@link #norm}) of this vector.
     *
     * @return This vector converted to a unit vector.
     */
    @Override
    public Vector<Dimensionless> toUnitVector() {
        Vector<Q> pTrans = copyToReal();
        Vector<Dimensionless> V = pTrans.toUnitVector();
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this vector are stated
     * in.
     */
    @Override
    public Unit getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this vector but stated in the specified unit.
     *
     * @param unit the unit of the vector to be returned. May not be null.
     * @return an equivalent to this vector but stated in the specified unit.
     * @throws ConversionException if the the input unit is not compatible with this unit.
     */
    @Override
    public GeomVector to(Unit unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        GeomVector newV = _child.to(unit);
        VectorTrans result = VectorTrans.newInstance(newV, _TM);
        this.copyState(result);

        return result;
    }

    /**
     * Returns a <code>ParameterVector</code> representation of this vector.
     *
     * @return A ParameterVector that is equivalent to this vector.
     */
    @Override
    public ParameterVector<Q> toParameterVector() {
        Vector<Q> pTrans = copyToReal();
        ParameterVector<Q> V = pTrans.toParameterVector();
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns a <code>Float64Vector</code> containing the elements of this vector stated
     * in the current units.
     *
     * @return A Float64Vector that contains the elements of this vector in the current
     *         units.
     */
    @Override
    public Float64Vector toFloat64Vector() {
        Vector<Q> pTrans = copyToReal();
        Float64Vector V = pTrans.toFloat64Vector();
        Vector.recycle(pTrans);
        return V;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     * 
     * @param transform The transform to apply to this vector. May not be null.
     */
    @Override
    public VectorTrans<Q> getTransformed(GTransform transform) {
        return VectorTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Return the equivalent of this vector converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the vector to return. MUST equal 3.
     * @return The equivalent of this vector converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public VectorTrans<Q> toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Compares this VectorTrans against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        VectorTrans that = (VectorTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Returns a copy of this VectorTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public VectorTrans<Q> copy() {
        return copyOf(this);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<VectorTrans> XML = new XMLFormat<VectorTrans>(VectorTrans.class) {

        @Override
        public VectorTrans newInstance(Class<VectorTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, VectorTrans obj) throws XMLStreamException {
            GeomVector.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            obj._TM_RotScale = GTransform.valueOf(obj._TM.getRotationScale());
            GeomVector child = xml.getNext();
            obj._child = child;

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(VectorTrans obj, OutputElement xml) throws XMLStreamException {
            GeomVector.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private VectorTrans() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<VectorTrans> FACTORY = new ObjectFactory<VectorTrans>() {
        @Override
        protected VectorTrans create() {
            return new VectorTrans();
        }

        @Override
        protected void cleanup(VectorTrans obj) {
            obj.reset();
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
        }
    };

    @SuppressWarnings("unchecked")
    private static VectorTrans copyOf(VectorTrans original) {
        VectorTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._TM_RotScale = original._TM_RotScale.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

}
