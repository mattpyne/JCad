/**
 * AbstractGeomList -- Interface and implementation in common to all geometry element
 * lists.
 *
 * Copyright (C) 2002-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * Partial implementation of a list of {@link GeomElement} objects. The list will not
 * accept the addition of <code>null</code> elements.
 * <p>
 * WARNING: This list allows geometry to be stored in different units and with different
 * physical dimensions. If consistent units or dimensions are required, then the user must
 * specifically convert the list items.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 31, 2000
 * @version January 30, 2017
 *
 * @param <T> The sub-type of this AbstractGeomList.
 * @param <E> The type of GeomElement contained in this list.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class AbstractGeomList<T extends AbstractGeomList, E extends GeomElement>
        extends AbstractGeomElement<T> implements GeometryList<T, E> {

    /**
     * Reference to a change listener for this object's child curves.
     */
    private final ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Return the list underlying this geometry list.
     *
     * @return The list underlying this geometry list.
     */
    protected abstract FastTable<E> getList();

    /**
     * Return the input index normalized into the range 0 &le; index &lt; size(). This
     * allows negative indexing (-1 referring to the last element in the list), but does
     * not allow wrap-around positive indexing.
     *
     * @param index The index to be normalized (put into range 0 &le; index &lt; size()).
     * @return The normalized index.
     */
    protected int normalizeIndex(int index) {
        int size = size();
        while (index < 0)
            index += size;
        return index;
    }

    /**
     * Returns the number of elements in this list. If the list contains more than
     * Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
     *
     * @return the number of elements in this list.
     */
    @Override
    public int size() {
        return getList().size();
    }

    /**
     * Returns <code>true</code> if this collection contains no elements.
     */
    @Override
    public boolean isEmpty() {
        return getList().isEmpty();
    }

    /**
     * Returns <code>true</code> if this list actually contains any geometry and
     * <code>false</code> if this list is empty or contains only non-geometry items such
     * as empty lists.
     *
     * @return true if this list actually contains geometry.
     */
    @Override
    public boolean containsGeometry() {
        FastTable<E> lst = getList();
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = lst.get(i);
            if (element instanceof GeometryList) {
                GeometryList subLst = (GeometryList)element;
                if (subLst.containsGeometry())
                    return true;
            } else
                return true;
        }
        return false;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return (0 returns the 1st element, -1 returns the
     *              last, -2 returns the 2nd from last, etc).
     * @return the element at the specified position in this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public E get(int index) {
        index = normalizeIndex(index);
        return getList().get(index);
    }

    /**
     * Returns the range of elements in this list from the specified start and ending
     * indexes.
     *
     * @param first index of the first element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @param last  index of the last element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public abstract T getRange(int first, int last);

    /**
     * Returns a view of the portion of this list between fromIndex, inclusive, and
     * toIndex, exclusive. (If fromIndex and toIndex are equal, the returned list is
     * empty.) The returned list is backed by this list, so changes in the returned list
     * are reflected in this list, and vice-versa. The returned list supports all of the
     * optional list operations supported by this list.
     *
     * This method eliminates the need for explicit range operations (of the sort that
     * commonly exist for arrays). Any operation that expects a list can be used as a
     * range operation by passing a subList view instead of a whole list. For example, the
     * following idiom removes a range of values from a list: <code>
     * list.subList(from, to).clear();</code> Similar idioms may be constructed for
     * <code>indexOf</code> and <code>lastIndexOf</code>, and all of the algorithms in the
     * <code>Collections</code> class can be applied to a subList.
     *
     * The semantics of the list returned by this method become undefined if the backing
     * list (i.e., this list) is <i>structurally modified</i> in any way other than via
     * the returned list (structural modifications are those that change the size of this
     * list, or otherwise perturb it in such a fashion that iterations in progress may
     * yield incorrect results).
     *
     * @param fromIndex low endpoint (inclusive) of the subList.
     * @param toIndex   high endpoint (exclusive) of the subList.
     * @return a view of the specified range within this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        fromIndex = normalizeIndex(fromIndex);
        toIndex = normalizeIndex(toIndex);
        return getList().subList(fromIndex, toIndex);
    }

    /**
     * Returns the first element from this list.
     *
     * @return the first element in this list.
     */
    @Override
    public E getFirst() {
        return get(0);
    }

    /**
     * Returns the last element from this list.
     *
     * @return the last element in this list.
     */
    @Override
    public E getLast() {
        return get(size() - 1);
    }

    /**
     * Returns the element with the specified name from this list.
     *
     * @param name The name of the element we are looking for in the list.
     * @return The element matching the specified name. If the specified element name
     *         isn't found in the list, then <code>null</code> is returned.
     */
    @Override
    public E get(String name) {

        E element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.get(index);

        return element;
    }

    /**
     * Replaces the <@link GeomElement> at the specified position in this list with the
     * specified element. Null elements are ignored. If the input element units are not
     * the same as this list, the element is converted to the units of this list.
     *
     * @param index   The index of the element to replace (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The element to be stored at the specified position.
     *                May not be null.
     * @return The element previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if <code>index > size()</code>
     */
    @Override
    public E set(int index, E element) {
        requireNonNull(element);
        if (!(element instanceof GeomElement))
            throw new ClassCastException(MessageFormat.format(
                    RESOURCES.getString("listElementTypeErr"), "list", "GeomElement"));
        index = normalizeIndex(index);

        E old = getList().set(index, element);

        if (!(old instanceof Immutable))
            old.removeChangeListener(_childChangeListener);
        if (!(element instanceof Immutable))
            element.addChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.

        return old;
    }

    /**
     * Inserts the specified {@link GeomElement} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored.
     * <p>
     * Note: If this method is used, concurrent access must be synchronized (the list is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     */
    @Override
    public void add(int index, E value) {
        requireNonNull(value);
        if (!(value instanceof GeomElement))
            throw new ClassCastException(MessageFormat.format(
                    RESOURCES.getString("listElementTypeErr"), "list", "GeomElement"));
        index = normalizeIndex(index);
        getList().add(index, value);

        if (!(value instanceof Immutable))
            value.addChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.

    }

    /**
     * Appends the specified {@link GeomElement} to the end of this list. Null values are
     * ignored.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param value the element to be inserted. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean add(E value) {
        add(size(), value);
        return true;
    }

    /**
     * Adds all of the elements in the specified collection to this geometry element list.
     * The behavior of this operation is undefined if the specified collection is modified
     * while the operation is in progress. (This implies that the behavior of this call is
     * undefined if the specified collection is this collection, and this collection is
     * nonempty.)
     *
     * @param c elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean addAll(Collection<? extends E> c) {
        return addAll(size(), c);
    }

    /**
     * Inserts all of the {@link GeomElement} objects in the specified collection into
     * this list at the specified position. Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (increases their indices). The
     * new elements will appear in this list in the order that they are returned by the
     * specified collection's iterator. The behavior of this operation is unspecified if
     * the specified collection is modified while the operation is in progress. (Note that
     * this will occur if the specified collection is this list, and it's nonempty.)
     *
     * @param index index at which to insert first element from the specified collection.
     * @param c     elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        for (Object element : c) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof GeomElement))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "list", "GeomElement"));
        }
        index = normalizeIndex(index);
        boolean changed = getList().addAll(index, c);
        if (changed) {
            for (E element : c) {
                if (!(element instanceof Immutable))
                    element.addChangeListener(_childChangeListener);
            }
            fireChangeEvent();
        }
        return changed;
    }

    /**
     * Appends all of the elements in the specified array to this geometry element list. The
     * behavior of this operation is undefined if the specified collection is modified
     * while the operation is in progress.
     *
     * @param arr elements to be appended onto this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean addAll(E[] arr) {
        return addAll(size(), arr);
    }

    /**
     * Inserts all of the {@link GeomElement} objects in the specified array into this
     * list at the specified position. Shifts the element currently at that position (if
     * any) and any subsequent elements to the right (increases their indices). The new
     * elements will appear in this list in the order that they are returned by the
     * specified collection's iterator.
     *
     * @param index index at which to insert first element from the specified array.
     * @param arr   elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean addAll(int index, E[] arr) {
        return addAll(index, Arrays.asList(requireNonNull(arr)));
    }

    /**
     * Appends all of the elements in the specified list of arguments to this geometry
     * element list.
     *
     * @param array elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean add(E... array) {
        return add(size(), array);
    }

    /**
     * Inserts all of the {@link GeomElement} objects in the specified list of arguments
     * into this list at the specified position. Shifts the element currently at that
     * position (if any) and any subsequent elements to the right (increases their
     * indices). The new elements will appear in this list in the order that they are
     * appeared in the array.
     *
     * @param index index at which to insert first element from the specified array.
     * @param array elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean add(int index, E... array) {
        return addAll(index, Arrays.asList(requireNonNull(array)));
    }

    /**
     * Removes the element at the specified position in this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that
     * was removed from the list.
     *
     * @param index the index of the element to remove. (0 returns the 1st element, -1
     *              returns the last, -2 returns the 2nd from last, etc).
     * @return the element previously at the specified position.
     */
    @Override
    public E remove(int index) {
        index = normalizeIndex(index);
        E old = getList().remove(index);

        if (!(old instanceof Immutable))
            old.removeChangeListener(_childChangeListener);

        fireChangeEvent();  //  Notify change listeners.

        return old;
    }

    /**
     * Removes a single instance of the specified element from this collection, if it is
     * present (optional operation). More formally, removes an element e such that
     * <code>(o==null ? e==null : o.equals(e))</code>, if this collection contains one or
     * more such elements. Returns true if this collection contained the specified element
     * (or equivalently, if this collection changed as a result of the call).
     *
     * @param o element to be removed from this collection, if present.
     * @return <code>true</code> if this collection changed as a result of the call.
     */
    @Override
    public boolean remove(Object o) {
        boolean changed = getList().remove(o);
        if (changed) {
            if (o instanceof AbstractGeomElement && !(o instanceof Immutable)) {
                ((AbstractGeomElement)o).removeChangeListener(_childChangeListener);
            }
            fireChangeEvent();
        }
        return changed;
    }

    /**
     * Removes the element with the specified name from this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that
     * was removed from the list.
     *
     * @param name the name of the element to remove.
     * @return the element previously at the specified position.
     */
    @Override
    public E remove(String name) {

        E element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.remove(index);

        return element;
    }

    /**
     * Removes all of the elements from this collection. The collection will be empty
     * after this call returns.
     */
    @Override
    public void clear() {
        int size = size();
        for (int i = 0; i < size; ++i) {
            E element = get(i);
            if (!(element instanceof Immutable))
                element.removeChangeListener(_childChangeListener);
        }
        getList().clear();

        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Returns an iterator over the elements in this list.
     *
     * @return an iterator over this list values.
     */
    @Override
    public java.util.Iterator<E> iterator() {
        return getList().iterator();
    }

    /**
     * Returns a list iterator over the elements in this list.
     *
     * @return an iterator over this list values.
     */
    @Override
    public java.util.ListIterator<E> listIterator() {
        return getList().listIterator();
    }

    /**
     * Returns a list iterator from the specified position.
     *
     * @param index the index of first value to be returned from the list iterator (by a
     *              call to the next method).
     * @return a list iterator of the values in this table starting at the specified
     *         position in this list.
     */
    @Override
    public java.util.ListIterator<E> listIterator(int index) {
        return getList().listIterator(index);
    }

    /**
     * Returns an unmodifiable list view associated to this list. Attempts to modify the
     * returned collection result in an UnsupportedOperationException being thrown.
     *
     * @return the unmodifiable view over this list.
     */
    @Override
    public List<E> unmodifiableList() {
        return getList().unmodifiable();
    }

    /**
     * Returns a new {@link GeomList} with the elements in this list.
     *
     * @return A new GeomList with the elements in this list.
     */
    @Override
    public GeomList<E> getAll() {
        GeomList<E> list = GeomList.newInstance();
        list.addAll(this);
        return list;
    }

    /**
     * Removes from this list all the elements that are contained in the specified
     * collection.
     *
     * @param c collection that defines which elements will be removed from this list.
     * @return <code>true</code> if this list changed as a result of the call.
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = getList().removeAll(c);
        if (changed) {
            for (Object o : c) {
                if (o instanceof AbstractGeomElement && !(o instanceof Immutable))
                    ((AbstractGeomElement)o).removeChangeListener(_childChangeListener);
            }
            fireChangeEvent();
        }
        return changed;
    }

    /**
     * Retains only the elements in this list that are contained in the specified
     * collection. In other words, removes from this list all the elements that are not
     * contained in the specified collection.
     *
     * @param c collection that defines which elements this set will retain. May not be null.
     * @return <code>true</code> if this list changed as a result of the call.
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = getList().retainAll(requireNonNull(c));
        if (changed)
            fireChangeEvent();
        return changed;
    }

    /**
     * Returns an new {@link GeomList} with the elements in this list in reverse order.
     */
    @Override
    public abstract T reverse();

    /**
     * Returns the index in this list of the first occurrence of the specified element, or
     * -1 if the list does not contain this element.
     *
     * @param element The element to search for.
     * @return the index in this List of the first occurrence of the specified element, or
     *         -1 if the List does not contain this element.
     */
    @Override
    public int indexOf(Object element) {
        return getList().indexOf(element);
    }

    /**
     * Returns the index in this list of the last occurrence of the specified element, or
     * -1 if the list does not contain this element. More formally, returns the highest
     * index i such that (o==null ? get(i)==null : o.equals(get(i))), or -1 if there is no
     * such index.
     *
     * @param element The element to search for.
     * @return the index in this list of the last occurrence of the specified element, or
     *         -1 if the list does not contain this element.
     */
    @Override
    public int lastIndexOf(Object element) {
        return getList().lastIndexOf(element);
    }

    /**
     * Return the index to the 1st geometry element in this list with the specified name.
     * Objects with <code>null</code> names are ignored.
     *
     * @param name The name of the geometry element to find in this list
     * @return The index to the named geometry element or -1 if it is not found.
     */
    @Override
    public int getIndexFromName(String name) {
        if (name == null)
            return -1;

        List<E> list = getList();
        int result = -1;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = list.get(i);
            String eName = element.getName();
            if (name.equals(eName)) {
                result = i;
                break;
            }
        }
        return result;
    }

    /**
     * Returns true if this collection contains the specified element. More formally,
     * returns true if and only if this collection contains at least one element e such
     * that (o==null ? e==null : o.equals(e)).
     *
     * @param o object to be checked for containment in this collection.
     * @return <code>true</code> if this collection contains the specified element.
     */
    @Override
    public boolean contains(Object o) {
        return getList().contains(o);
    }

    /**
     * Returns true if this collection contains all of the elements in the specified
     * collection.
     *
     * @param c collection to be checked for containment in this collection. May not be null.
     * @return <code>true</code> if this collection contains all of the elements in the
     *         specified collection.
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return getList().containsAll(requireNonNull(c));
    }

    /**
     * Returns an array containing all of the elements in this collection.
     */
    @Override
    public Object[] toArray() {
        return getList().toArray();
    }

    /**
     * Returns an array containing all of the elements in this collection. If the
     * collection fits in the specified array, it is returned therein. Otherwise, a new
     * array is allocated with the runtime type of the specified array and the size of
     * this collection.
     *
     * @param <T> The type of elements in this collection.
     * @param a   the array into which the elements of the collection are to be stored, if
     *            it is big enough; otherwise, a new array of the same type is allocated
     *            for this purpose.
     * @return an array containing the elements of the collection.
     */
    @Override
    @SuppressWarnings("SuspiciousToArrayCall")
    public <T> T[] toArray(T[] a) {
        return getList().toArray(a);
    }

    /**
     * Return <code>true</code> if this geometry list contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the elements
     * in this list are invalid.
     */
    @Override
    public boolean isValid() {
        List<E> lst = getList();
        int size = lst.size();
        for (int i = 0; i < size; ++i) {
            E element = lst.get(i);
            if (!element.isValid())
                return false;
        }
        return true;
    }

    /**
     * Returns the unit in which the <I>first</I> geometry element in this list is stated.
     * If the list contains no geometry elements, then the default unit is returned.
     */
    @Override
    public Unit<Length> getUnit() {
        if (isEmpty() || !containsGeometry())
            return GeomUtil.getDefaultUnit();
        return elementWithGeometry().getUnit();
    }

    /**
     * Compares the specified object with this list of <code>GeomElement</code> objects
     * for equality. Returns true if and only if both collections are of the same type and
     * both collections contain equal elements in the same order.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this list is identical to that list;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        AbstractGeomList that = (AbstractGeomList)obj;
        return this.getList().equals(that.getList())
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>AbstractGeomList</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        hash = hash * 31 + getList().hashCode();
        hash = hash * 31 + super.hashCode();

        return hash;
    }

    /**
     * Returns the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        String className = this.getClass().getName();
        tmp.append(className.substring(className.lastIndexOf(".") + 1));
        tmp.append(": {\n");
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement e = this.get(i);
            if (e instanceof AbstractGeomList) {
                className = e.getClass().getName();
                tmp.append(className.substring(className.lastIndexOf(".") + 1));
                String name = e.getName();
                if (nonNull(name)) {
                    tmp.append("(\"");
                    tmp.append(name);
                    tmp.append("\")");
                }
                tmp.append(": ");
                tmp.append(RESOURCES.getString("size"));
                tmp.append(" = ");
                tmp.append(((AbstractGeomList)e).size());

            } else {
                tmp.append(e.toText());
            }
            if (i < size - 1)
                tmp.append(",\n");
            else
                tmp.append("\n");
        }
        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns 0.
     */
    @Override
    public int getPhyDimension() {
        return 0;
    }

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 0.
     */
    @Override
    public int getParDimension() {
        return 0;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z). The physical dimension of the
     * returned point will be that of the highest physical dimension object in this list.
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no geometry.
     */
    @Override
    public Point getBoundsMin() {

        //  Find any element that contains geometry.
        GeomElement geom = elementWithGeometry();
        if (isNull(geom))
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        StackContext.enter();
        try {
            Point minPoint = geom.getBoundsMin();
            int dim = minPoint.getPhyDimension();

            int size = this.size();
            for (int i = 0; i < size; ++i) {
                GeomElement element = this.get(i);
                try {

                    //  Get the element's minimum point.
                    Point emin = element.getBoundsMin();

                    //  Ensure that the physical dimensions match.
                    int eDim = emin.getPhyDimension();
                    if (eDim > dim) {
                        minPoint = minPoint.toDimension(eDim);
                        dim = eDim;
                    } else if (eDim < dim)
                        emin = emin.toDimension(dim);

                    //  Track the minimum point.
                    minPoint = minPoint.min(emin);
                } catch (Exception ignored) {
                }   //  Ignore elements that contain no geometry.
            }

            return StackContext.outerCopy(minPoint);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z). The physical dimension of the returned point will be that of the
     * highest physical dimension object in this list.
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {

        //  Find any element that contains geometry.
        GeomElement geom = elementWithGeometry();
        if (isNull(geom))
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        StackContext.enter();
        try {
            Point maxPoint = geom.getBoundsMax();
            int dim = maxPoint.getPhyDimension();

            int size = this.size();
            for (int i = 0; i < size; ++i) {
                GeomElement element = this.get(i);
                try {
                    //  Get the element's maximum point.
                    Point emax = element.getBoundsMax();

                    //  Ensure that the physical dimensions match.
                    int eDim = emax.getPhyDimension();
                    if (eDim > dim) {
                        maxPoint = maxPoint.toDimension(eDim);
                        dim = eDim;
                    } else if (eDim < dim)
                        emax = emax.toDimension(dim);

                    //  Track the maximum point.
                    maxPoint = maxPoint.max(emax);
                } catch (Exception ignored) {
                }   //  Ignore elements that contain no geometry.
            }

            return StackContext.outerCopy(maxPoint);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction for the geometry in this list.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     */
    @Override
    public GeomPoint getLimitPoint(int dim, boolean max, double tol) {

        //  Find any element that contains geometry.
        GeomElement geom = elementWithGeometry();
        if (isNull(geom))
            throw new IndexOutOfBoundsException(RESOURCES.getString("listNoGeometry"));

        //  Get the limit of whatever geometry element was returned from this list.
        GeomPoint limPnt = geom.getLimitPoint(dim, max, tol);
        Parameter<Length> lim = limPnt.get(dim);

        //  Loop over all the elements in this list, get their limit points and find the global limit point.
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = this.get(i);
            if (!geom.equals(element)) {
                try {
                    //  First check to see if the bounds make it possible for this element to
                    //  contain the limit point.
                    if (max) {
                        Point pmax = element.getBoundsMax();
                        if (!pmax.get(dim).isGreaterThan(lim))
                            continue;
                    } else {
                        Point pmin = element.getBoundsMin();
                        if (!pmin.get(dim).isLessThan(lim))
                            continue;
                    }
                    //  It is possible that this element could contain the limit point,
                    //  so search for this element's limit point.
                    GeomPoint limPnt2 = element.getLimitPoint(dim, max, tol);
                    if ((max && limPnt2.get(dim).isGreaterThan(lim)) || (!max && limPnt2.get(dim).isLessThan(lim))) {
                        limPnt = limPnt2;
                        lim = limPnt.get(dim);
                    }
                } catch (Exception ignore) {
                    //  Ignore elements that contain no geometry.
                }
            }
        }

        //  Return the global limit point.
        return limPnt;
    }

    /**
     * Resets the internal state of this object to its default values. Subclasses that
     * override this method must call <code>super.reset();</code> to ensure that the state
     * is reset properly.
     */
    @Override
    public void reset() {
        getList().reset();
        super.reset();
    }

    /**
     * Returns an element from this list that actually contains geometry or null if there
     * is none.
     */
    private E elementWithGeometry() {
        FastTable<E> lst = getList();
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E element = lst.get(i);
            if (element instanceof AbstractGeomList) {
                AbstractGeomList subLst = (AbstractGeomList)element;
                element = (E)subLst.elementWithGeometry();
                if (element != null)
                    return element;
            } else
                return element;
        }
        return null;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<AbstractGeomList> XML = new XMLFormat<AbstractGeomList>(AbstractGeomList.class) {

        @Override
        public void read(XMLFormat.InputElement xml, AbstractGeomList obj) throws XMLStreamException {
            AbstractGeomElement.XML.read(xml, obj);     // Call parent read.
            FastTable lst = xml.get("Contents", FastTable.class);
            obj.addAll(lst);
        }

        @Override
        public void write(AbstractGeomList obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractGeomElement.XML.write(obj, xml);    // Call parent write.
            xml.add(obj.getList(), "Contents", FastTable.class);
        }
    };

}
