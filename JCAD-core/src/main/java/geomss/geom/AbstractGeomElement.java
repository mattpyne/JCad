/**
 * AbstractGeomElement -- Partial implementation of the GeomElement interface.
 *
 * Copyright (C) 2002-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.util.*;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javolution.lang.Reusable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastMap;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A partial implementation of the {@link GeomElement} interface.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 31, 2000
 * @version January 30, 2017
 *
 * @param <T> The sub-type of this AbstractGeomElement element.
 */
@SuppressWarnings("serial")
public abstract class AbstractGeomElement<T extends AbstractGeomElement> implements GeomElement<T>, Reusable {

    /**
     * The key used to store a J3DGeomGroup instance in the user data of the supplied
     * GeomElement object.
     */
    private static final String USERDATA_KEY = "J3DGeomGroup";

    /**
     * The resource bundle for this package.
     */
    public static final ResourceBundle RESOURCES
            = ResourceBundle.getBundle("geomss.geom.GeomResources", java.util.Locale.getDefault());

    //  The next ID number to return.
    private static long _nextID = 0;

    //  The unique ID number assigned to this instance.
    private final long _id = newID();

    /**
     * Name of this geometry element.
     */
    private String _name;

    /**
     * The list of change listeners.
     */
    private transient List<ChangeListener> _changeListeners = null;

    //  Reference data for this element.
    private HashMap<Object, Object> _userData = null;

    /**
     * Returns a new and unique ID number that can be used by objects in the geometry
     * package to identify themselves.
     */
    private synchronized static long newID() {
        long id = _nextID++;
        return id;
    }

    /**
     * Return the unique ID number of this geometry element.
     */
    @Override
    public long getID() {
        return _id;
    }

    /**
     * Return the name of this geometry element. If the name is not defined, then
     * <code>null</code> is returned.
     */
    @Override
    public String getName() {
        return _name;
    }

    /**
     * Change the name of this geometry element to the specified name (may be
     * <code>null</code> to clear the name and make it undefined).
     */
    @Override
    public void setName(String name) {
        if (!Objects.equals(name, _name)) {
            _name = name;
            fireChangeEvent();  //  Notify change listeners.
        }
    }

    /**
     * Add a listener that is notified of changes to the state of this element.
     */
    @Override
    public synchronized void addChangeListener(ChangeListener listener) {
        requireNonNull(listener);
        if (isNull(_changeListeners))
            _changeListeners = new CopyOnWriteArrayList();
        if (!_changeListeners.contains(listener))
            _changeListeners.add(listener);
    }

    /**
     * Remove a listener from receiving notifications of changes to the state of this
     * element.
     */
    @Override
    public void removeChangeListener(ChangeListener listener) {
        if (nonNull(_changeListeners))
            _changeListeners.remove(listener);
    }

    /**
     * Fire a ChangeEvent to any interested listeners indicating that the state of this
     * geometry element has changed in some way.
     */
    protected void fireChangeEvent() {
        if (nonNull(_changeListeners) && _changeListeners.size() > 0) {
            ChangeEvent event = new ChangeEvent(this);
            Iterator<ChangeListener> it = _changeListeners.iterator();
            while (it.hasNext()) {
                ChangeListener listener = it.next();
                listener.stateChanged(event);
            }
        }
    }

    /**
     * Returns a copy of this GeomElement instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this geometry element.
     * @throws java.lang.CloneNotSupportedException Never thrown.
     * @see #copy() 
     */
    @Override
    @SuppressWarnings("CloneDoesntCallSuperClone")
    public Object clone() throws CloneNotSupportedException {
        return copy();
    }


    /**
     * Compares the specified object with this object for equality. Returns true if and
     * only if both AbstractGeomElement objects have the same name.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this object is identical to that object;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        AbstractGeomElement that = (AbstractGeomElement)obj;
        return Objects.equals(this._name, that._name);
    }

    /**
     * Returns the hash code for this <code>AbstractGeomElement</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return Objects.hash(_name);
    }

    /**
     * Returns the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append("{type=");
        tmp.append(this.getClass().getName());
        tmp.append(", id=");
        tmp.append(getID());
        tmp.append(", name=");
        tmp.append(_name);
        tmp.append(", size=");
        tmp.append(size());
        tmp.append(", phyDim=");
        tmp.append(getPhyDimension());
        tmp.append(", parDim=");
        tmp.append(getParDimension());
        tmp.append(", boundsMin=");
        tmp.append(getBoundsMin());
        tmp.append(", boundsMax=");
        tmp.append(getBoundsMax());
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns a string representation of this geometry element.
     */
    @Override
    public String toString() {
        return toText().toString();
    }

    /**
     * Compares this geometry element with the specified element for order (where order is
     * determined by the element's name). Returns a negative integer, zero, or a positive
     * integer as this object is less than, equal to, or greater than the specified
     * object. This method delegates to the String.compareTo() method using the string
     * representation of this element.
     *
     * @param otherElement The geometry element this one is being compared to.
     */
    @Override
    public int compareTo(Object otherElement) {
        String thisStr = this.toString();
        String otherStr = otherElement.toString();
        return thisStr.compareTo(otherStr);
    }

    /**
     * Return any user defined object associated with this geometry element and the
     * specified key. If there is no user data with the specified key, then
     * <code>null</code> is returned.
     *
     * @param key the key whose associated value is to be returned. May not be null.
     * @return The user data associated with "key" or null if the specified key could not
     *         be found.
     */
    @Override
    public Object getUserData(Object key) {
        requireNonNull(key);
        if (isNull(_userData))
            return null;
        return _userData.get(key);
    }

    /**
     * Returns a new Map containing all the user objects associated with this geometry
     * element.
     */
    @Override
    public Map<Object, Object> getAllUserData() {
        if (isNull(_userData))
            return FastMap.newInstance();
        FastMap<Object, Object> map = FastMap.newInstance();
        map.putAll(_userData);
        map.remove(USERDATA_KEY); //  Do not return this internal use user data to the user.
        return map;
    }

    /**
     * Set the user defined object associated with this geometry element and the specified
     * key. This can be used to store any type of information with a geometry element that
     * could be useful.
     *
     * @param key   the key with which the specified value is to be associated. May not be
     *              null.
     * @param value the value to be associated with the specified key. May not be null.
     */
    @Override
    public void putUserData(Object key, Object value) {
        requireNonNull(key);
        requireNonNull(value);
        if (isNull(_userData))
            _userData = new HashMap();
        _userData.put(key, value);
    }

    /**
     * Add all the user defined data in the supplied Map to this objects map of user
     * defined data.
     *
     * @param data The Map of user defined data to be added to this object's map of user
     *             defined data.
     */
    @Override
    public void putAllUserData(Map data) {
        if (isNull(data) || data.isEmpty())
            return;
        if (isNull(_userData))
            _userData = new HashMap();
        _userData.putAll(data);
    }

    /**
     * Removes the entry for the specified user object key if present.
     *
     * @param key the key whose mapping is to be removed from the map. May not be null.
     */
    @Override
    public void removeUserData(Object key) {
        requireNonNull(key);
        if (isNull(_userData))
            return;
        
        _userData.remove(key);
    }

    /**
     * Resets the internal state of this object to its default values. Subclasses that
     * override this method must call <code>super.reset();</code> to ensure that the state
     * is reset properly.
     */
    @Override
    public void reset() {
        _name = null;
        if (nonNull(_changeListeners))
            _changeListeners.clear();
        
        if (nonNull(_userData))
            _userData.clear();
    }

    /**
     * Copy the state of this GeomElement object to the target object. This is required to
     * make a shallow copy of this object or to make an object that has the same features
     * as this object.
     * <p>
     * This implementation copies the object's name and user data.
     * </p>
     *
     * @param <T>    The type of this geometry element.
     * @param target The target AbstractGeomElement object.
     * @return A reference to <code>target</code> is returned for convenience.
     */
    protected <T extends AbstractGeomElement> T copyState(T target) {
        target.setName(getName());
        if (nonNull(_userData) && !_userData.isEmpty()) {
            Map<Object, Object> userData = getAllUserData();
            target.putAllUserData(userData);
        }
        return target;
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<AbstractGeomElement> XML
            = new XMLFormat<AbstractGeomElement>(AbstractGeomElement.class) {

                @Override
                public void read(XMLFormat.InputElement xml, AbstractGeomElement obj) throws XMLStreamException {
                    String name = xml.getAttribute("name", (String)null);
                    obj.setName(name);
                    FastMap userData = xml.get("UserData", FastMap.class);
                    if (nonNull(userData)) {
                        obj.putAllUserData(userData);
                        FastMap.recycle(userData);
                    }
                }

                @Override
                public void write(AbstractGeomElement obj, XMLFormat.OutputElement xml) throws XMLStreamException {
                    String name = obj.getName();
                    if (nonNull(name))
                        xml.setAttribute("name", name);
                    FastMap userData = (FastMap)obj.getAllUserData();
                    if (!userData.isEmpty())
                        xml.add(userData, "UserData", FastMap.class);
                }
            };

}
