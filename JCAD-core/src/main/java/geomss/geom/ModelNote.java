/*
 *   ModelNote  -- Holds a textual note String located at a point in model space with a specified
 *                  size and orientation in model space.
 *
 *   Copyright (C) 2014-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.DCMatrix;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.param.Rotation;
import java.awt.Font;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.lang.ValueType;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * Represents a textual note located at a point in model space with a specified size and
 * orientation in model space.
 *
 * <p>
 * Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 10, 2014
 * @version November 26, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class ModelNote extends GenModelNote implements ValueType {

    /**
     * The text string displayed in the note.
     */
    private String text;

    /**
     * Holds the direction that the text flows in space.
     */
    private Vector<Dimensionless> xhat;

    /**
     * Holds the direction, normal to xhat, that represents the vertical direction of the
     * text (the text ascent direction).
     */
    private Vector<Dimensionless> yhat;

    /**
     * Holds the location of the note in model space.
     */
    private Point location;

    /**
     * The font used to display the note.
     */
    private Font font;

    /**
     * The approximate height of the text in model units.
     */
    private Parameter<Length> height;

    /**
     * Construct and return a new instance of a ModelNote that uses the specified text
     * string, the specified display font, is located at the location and orientation in
     * space specified by the input vectors, and has the specified height in model units.
     *
     * @param text     The text to be displayed in this geometry object. May not be null.
     * @param xhat     The vector indicating the direction that left-to-right text flows
     *                 in space. May not be null.
     * @param yhat     The vector, orthogonal to xhat, indicating the vertical direction
     *                 (or ascent) of the text. May not be null.
     * @param location The location of this geometry object in model space. May not be null.
     * @param font     The font used to display this note. May not be null.
     * @param height   The height of the text box in model units. May not be null.
     * @return A new ModelNote using the specified inputs.
     */
    public static ModelNote valueOf(CharSequence text,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat, GeomPoint location,
            Font font, Parameter<Length> height) {
        requireNonNull(text);
        requireNonNull(font);

        //  Make sure the physical dimension and units are compatible.
        Unit<Length> unit = height.getUnit();
        int dim = GeomUtil.maxPhyDimension(requireNonNull(xhat), requireNonNull(yhat), requireNonNull(location));
        if (dim < 2)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"), "ModelNote", dim));
        xhat = xhat.toDimension(dim);
        yhat = yhat.toDimension(dim);
        location = location.toDimension(dim).to(unit);

        //  Make certain that yhat is orthogonal to xhat.
        GeomVector n = xhat.cross(yhat);
        yhat = n.cross(xhat).toUnitVector();

        ModelNote note = FACTORY.object();
        note.text = text.toString();
        note.xhat = xhat.immutable();
        note.yhat = yhat.immutable();
        note.location = location.immutable();
        note.font = font;
        note.height = height;

        return note;
    }

    /**
     * Construct and return a new instance of a ModelNote that uses the specified text
     * string, the default display font at the specified size, and is located at the
     * location and orientation in space specified by the input plane.
     *
     * @param text     The text to be displayed in this geometry object. May not be null.
     * @param xhat     The vector indicating the direction that left-to-right text flows
     *                 in space. May not be null.
     * @param yhat     The vector, orthogonal to xhat, indicating the vertical direction
     *                 (or ascent) of the text. May not be null.
     * @param location The location of this geometry object in model space. May not be null.
     * @param height   The height of the text box in model units. May not be null.
     * @return A new ModelNote using the specified inputs.
     */
    public static ModelNote valueOf(CharSequence text,
            GeomVector<Dimensionless> xhat, GeomVector<Dimensionless> yhat, GeomPoint location,
            Parameter<Length> height) {
        return ModelNote.valueOf(text, xhat, yhat, location, DEFAULT_FONT, height);
    }

    /**
     * Construct and return a new instance of a ModelNote that uses the specified text
     * string, the default display font at the specified size, and is located parallel to
     * the the XY plane with the xhat in the X-axis direction, yhat in the Y-axis
     * direction.
     *
     * @param text     The text to be displayed in this geometry object. May not be null.
     * @param location The location of this geometry object in model space. May not be null.
     * @param height   The height of the text box in model units. May not be null.
     * @return A new ModelNote using the specified inputs.
     */
    public static ModelNote valueOf(CharSequence text, GeomPoint location, Parameter<Length> height) {
        int dim = location.getPhyDimension();
        if (dim < 2)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"), "ModelNote", dim));

        Vector<Dimensionless> xhat = Vector.valueOf(1, 0).toDimension(dim);
        Vector<Dimensionless> yhat = Vector.valueOf(0, 1).toDimension(dim);
        return ModelNote.valueOf(text, xhat, yhat, location, DEFAULT_FONT, height);
    }

    /**
     * Construct and return a new instance of a ModelNote that uses the specified text
     * string, the default display font at the specified size, and is located at the
     * specified location with the text plane oriented using the specified orientation
     * rotation.
     *
     * @param text        The text to be displayed in this geometry object. May not be
     *                    null.
     * @param orientation The orientation of the text relative to X,Y & Z 3D space axes.
     *                    May not be null.
     * @param location    The location of this geometry object in model space. The
     *                    physical dimension must be &le; 3. If it is less than 3, it will
     *                    be increased to 3. If &gt; 3, an exception is thrown. May not be
     *                    null.
     * @param height      The height of the text box in model units. May not be null.
     * @throws IllegalArgumentException if the physical dimension of the input location
     * point is greater than 3.
     * @return A new ModelNote using the specified inputs.
     */
    public static ModelNote valueOf(CharSequence text, Rotation orientation, GeomPoint location, Parameter<Length> height) {
        int dim = location.getPhyDimension();
        if (dim < 3)
            location = location.toDimension(3);
        else if (dim > 3)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("rotationDimensionLTE3"), dim));

        //  Convert the arbitrary rotation to a direction-cosine matrix.
        DCMatrix dcm = orientation.toDCM();

        //  Convert the columns of the DCM matrix to xhat & yhat
        Float64Vector col0 = dcm.getColumn(Point.X);
        Float64Vector col1 = dcm.getColumn(Point.Y);
        Vector<Dimensionless> xhat = Vector.valueOf(col0, Dimensionless.UNIT);
        Vector<Dimensionless> yhat = Vector.valueOf(col1, Dimensionless.UNIT);

        return ModelNote.valueOf(text, xhat, yhat, location, height);
    }

    /**
     * Returns a new ModelNote instance that is identical to the specified ModelNote.
     *
     * @param note the ModelNote to be copied into a new ModelNote.  May not be null.
     * @return A new ModelNote identical to the input note (a copy or clone).
     */
    public static ModelNote valueOf(ModelNote note) {
        return copyOf(requireNonNull(note));
    }

    /**
     * Return the text string associated with this note object.
     *
     * @return The text string associated with this note object.
     */
    @Override
    public String getNote() {
        return text;
    }

    /**
     * Return the vector indicating the horizontal axis direction for the text.
     *
     * @return The vector indicating the horizontal axis direction for the text.
     */
    @Override
    public Vector<Dimensionless> getXHat() {
        return xhat;
    }

    /**
     * Return the vector indicating the vertical axis direction (or ascent direction) for
     * the text.
     *
     * @return The vector indicating the vertical axis direction (or ascent direction) for
     *         the text.
     */
    @Override
    public Vector<Dimensionless> getYHat() {
        return yhat;
    }

    /**
     * Return the location of this note in space.
     *
     * @return The location of this note in space.
     */
    @Override
    public Point getLocation() {
        return location;
    }

    /**
     * Return the height of the text box in model units.
     *
     * @return The height of the text box in model units.
     */
    @Override
    public Parameter<Length> getHeight() {
        return height;
    }

    /**
     * Return the font used to display this note.
     *
     * @return The font used to display this note.
     */
    @Override
    public Font getFont() {
        return font;
    }

    /**
     * Return an immutable version of this note.
     *
     * @return An immutable version of this note.
     */
    @Override
    public ModelNote immutable() {
        return this;
    }

    /**
     * Return a new note object identical to this one, but with the specified font.
     *
     * @param font The font for the new copy of this note.  May not be null.
     * @return A new note object identical to this one, but with the specified font.
     */
    @Override
    public ModelNote changeFont(Font font) {
        ModelNote note = ModelNote.valueOf(text, xhat, yhat, location, requireNonNull(font), height);
        copyState(note);
        return note;
    }

    /**
     * Return a new note object identical to this one, but with the specified location in
     * model space. The returned note will have a different physical dimension from this
     * one if the input location has a different physical dimension from the original
     * location.
     *
     * @param location The location for the new copy of this note. May note be null.
     * @return A new note object identical to this one, but with the specified location in
     *         model space.
     */
    @Override
    public ModelNote changeLocation(GeomPoint location) {
        ModelNote note = ModelNote.valueOf(text, xhat, yhat, requireNonNull(location), font, height);
        copyState(note);
        return note;
    }

    /**
     * Return a new note object identical to this one, but with the specified height in
     * model space.
     *
     * @param height The height of the new copy of this note. May note be null.
     * @return A new note object identical to this one, but with the specified height in
     *         model space.
     */
    @Override
    public ModelNote changeHeight(Parameter<Length> height) {
        ModelNote note = ModelNote.valueOf(text, xhat, yhat, location, font, height.to(getUnit()));
        copyState(note);
        return note;
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation will return the physical dimensions of the plane indicating the
     * location and orientation of the note in space.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return location.getPhyDimension();
    }

    /**
     * Return <code>true</code> if this ModelNote contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the location
     * coordinate values are NaN or Inf.
     *
     * @return true if this ModelNote contains valid and finite numerical components.
     */
    @Override
    public boolean isValid() {
        return xhat.isValid() && yhat.isValid() && location.isValid();
    }

    /**
     * Returns a copy of this ModelNote instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this note.
     */
    @Override
    public ModelNote copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public ModelNote copyToReal() {
        return copy();
    }

    /**
     * Returns the unit in which the note height and location are stored.
     *
     * @return The unit in which the note height and location are stored.
     */
    @Override
    public final Unit<Length> getUnit() {
        return height.getUnit();
    }

    /**
     * Returns the equivalent to this note but with the location stated in the specified
     * unit.
     *
     * @param unit the length unit of the note to be returned. May not be null.
     * @return an equivalent of this note but with location stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public ModelNote to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        ModelNote note = FACTORY.object();
        note.text = text;
        note.xhat = xhat;
        note.yhat = yhat;
        note.location = location.to(unit);
        note.height = height.to(unit);
        copyState(note);

        return note;
    }

    /**
     * Return the equivalent of this note converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the note to return.
     * @return The equivalent to this note converted to the new dimensions.
     */
    @Override
    public ModelNote toDimension(int newDim) {
        int thisDim = this.getPhyDimension();
        if (newDim == thisDim)
            return this;
        if (newDim < 2)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"), "ModelNote", newDim));

        //  Convert the underlying geometry.
        Vector<Dimensionless> newXHat = xhat.toDimension(newDim);
        Vector<Dimensionless> newYHat = yhat.toDimension(newDim);
        Point newLoc = location.toDimension(newDim);

        //  Create and return a new note with the new geometry.
        ModelNote note = ModelNote.valueOf(text, newXHat, newYHat, newLoc, font, height);
        copyState(note);

        return note;
    }

    /**
     * Compares this ModelNote against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this note is identical to that note;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        ModelNote that = (ModelNote)obj;
        return this.text.equals(that.text)
                && this.height.equals(that.height)
                && this.xhat.equals(that.xhat)
                && this.yhat.equals(that.yhat)
                && this.location.equals(that.location)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this ModelNote object.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(text, xhat, yhat, location, height);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<ModelNote> XML = new XMLFormat<ModelNote>(ModelNote.class) {

        @Override
        public ModelNote newInstance(Class<ModelNote> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, ModelNote obj) throws XMLStreamException {
            //  Read in the font information.
            String fontStr = xml.getAttribute("font", DEFAULT_FONT_CODE);
            Font font = Font.decode(fontStr);

            GenModelNote.XML.read(xml, obj);     // Call parent read.

            //  Read in the text string.
            String text = xml.get("Note", String.class);

            //  Read in the height.
            Parameter<Length> height = xml.get("Height", Parameter.class);

            //  Read in the location & orientation of the note in model space.
            Vector<Dimensionless> xhat = xml.get("XHat", Vector.class);
            Vector<Dimensionless> yhat = xml.get("YHat", Vector.class);
            Point location = xml.get("Location", Point.class);

            // Make sure the physical dimensions and units are consistent.
            Unit<Length> unit = height.getUnit();
            int dim = GeomUtil.maxPhyDimension(xhat, yhat, location);
            if (dim < 2)
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("dimensionNotAtLeast2"), "ModelNote", dim));
            xhat = xhat.toDimension(dim);
            yhat = yhat.toDimension(dim);
            location = location.toDimension(dim).to(unit);

            //  Fill in the object definition.
            obj.text = text;
            obj.font = font;
            obj.xhat = xhat;
            obj.yhat = yhat;
            obj.location = location;
            obj.height = height;
        }

        @Override
        public void write(ModelNote obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            //  Write out a font string.
            Font font = obj.getFont();
            String fontStr = encodeFont(font.getName(), font.getStyle(), font.getSize());
            xml.setAttribute("font", fontStr);

            GenModelNote.XML.write(obj, xml);    // Call parent write.

            //  Write out the text string.
            xml.add(obj.getNote(), "Note", String.class);

            //  Write out note height.
            xml.add(obj.getHeight(), "Height", Parameter.class);

            //  Write out the location & orientation of the note in model space.
            xml.add(obj.getXHat(), "XHat", Vector.class);
            xml.add(obj.getYHat(), "YHat", Vector.class);
            xml.add(obj.getLocation(), "Location", Point.class);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private ModelNote() { }

    @SuppressWarnings("unchecked")
    private static ModelNote copyOf(ModelNote original) {
        ModelNote obj = FACTORY.object();
        obj.text = original.text;
        obj.xhat = original.xhat.copy();
        obj.yhat = original.yhat.copy();
        obj.location = original.location.copy();
        obj.height = original.height.copy();
        obj.font = original.font;
        original.copyState(obj);
        return obj;
    }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<ModelNote> FACTORY = new ObjectFactory<ModelNote>() {
        @Override
        protected ModelNote create() {
            return new ModelNote();
        }

        @Override
        protected void cleanup(ModelNote obj) {
            obj.reset();
            obj.text = null;
            obj.xhat = null;
            obj.yhat = null;
            obj.location = null;
            obj.height = null;
            obj.font = null;
        }
    };

}
