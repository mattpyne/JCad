/**
 * SubrangeCurve -- A Curve that is a subrange on a parametric object such as a curve or
 * surface.
 *
 * Copyright (C) 2009-2015, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link Curve} element that refers to a {@link ParametricGeometry} object such as a
 * {@link Curve} or {@link Surface} and a parametric curve on that parametric object.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 28, 2009
 * @version November 27, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class SubrangeCurve extends AbstractCurve<SubrangeCurve> implements Subrange<Curve> {

    /**
     * The parametric distance along the parametric geometry where this curve is located.
     */
    private Curve _u;

    /**
     * The parametric geometry object that this curve is located on.
     */
    private ParametricGeometry _child;

    /**
     * Reference to a change listener for this object's child geometry.
     */
    private ChangeListener _childChangeListener = new MyChangeListener(this);

    /**
     * The minimum bounding point for this subrange curve.
     */
    private Point _boundsMin;

    /**
     * The maximum bounding point for this subrange curve.
     */
    private Point _boundsMax;

    /**
     * The arc-length for the parametric curve.
     */
    private double uL;

    /**
     * Returns a {@link SubrangeCurve} instance referring to the specified
     * {@link ParametricGeometry} and parametric location.
     *
     * @param child The {@link ParametricGeometry} object that this curve is subranged
     *              onto (may not be <code>null</code>).
     * @param par   A curve of the parametric position (between 0 and 1) along each
     *              parametric dimension where the curve is located (may not be
     *              <code>null</code>).
     * @return the subrange curve having the specified values.
     * @throws DimensionException if the input child object's parametric dimension is not
     * compatible with the input parametric distance point.
     */
    public static SubrangeCurve newInstance(ParametricGeometry child, Curve par) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(par, MessageFormat.format(RESOURCES.getString("paramNullErr"), "par"));
        int pDim = child.getParDimension();
        if (par.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        SubrangeCurve obj = FACTORY.object();
        obj._u = par;
        obj._child = child;
        if (!(par instanceof Immutable))
            par.addChangeListener(obj._childChangeListener);
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);
        obj.calcBoundsMinMax();

        return obj;
    }

    //  A change listener that re-calculates the subrange bounds as well as
    //  passing the child's change event on to any listeners.
    private static class MyChangeListener extends ForwardingChangeListener {

        private final SubrangeCurve target;

        public MyChangeListener(SubrangeCurve geom) {
            super(geom);
            this.target = geom;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            //  Re-calculate the bounds of this subrange curve.
            target.calcBoundsMinMax();
            super.stateChanged(e);
        }
    }

    /**
     * Returns the parametric position on the child object that this curve refers to.
     *
     * @return The parametric position on the child object that this curve refers to.
     */
    @Override
    public Curve getParPosition() {
        return _u;
    }

    /**
     * Sets the parametric position on the child object that this curve refers to.
     *
     * @param par The parametric position (between 0 and 1) along each parametric
     *            dimension where the curve is located (may not be <code>null</code>).
     */
    @Override
    public void setParPosition(Curve par) {
        requireNonNull(par, MessageFormat.format(RESOURCES.getString("paramNullErr"), "par"));
        int pDim = _child.getParDimension();
        if (par.getPhyDimension() != pDim)
            throw new DimensionException(RESOURCES.getString("scIncParDim"));

        if (!(_u instanceof Immutable))
            _u.removeChangeListener(_childChangeListener);

        _u = par;

        if (!(par instanceof Immutable))
            par.addChangeListener(_childChangeListener);

        calcBoundsMinMax();
        fireChangeEvent();
    }

    /**
     * Returns the child object this point is subranged onto.
     */
    @Override
    public ParametricGeometry getChild() {
        return _child;
    }

    /**
     * Recycles a <code>SubrangeCurve</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(SubrangeCurve instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation returns the size of the child object.
     *
     * @return The number of child-elements that make up this geometry element.
     */
    @Override
    public int size() {
        return _child.size();
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of this element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Calculate a point on the curve for the given parametric distance along the curve,
     * <code>p(s)</code>.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return the calculated point
     */
    @Override
    public Point getRealPoint(double s) {
        Point sT = _u.getRealPoint(s);  //  Convert from 0-1 to parametric position range of _u curve.
        Point p = _child.getRealPoint(sT);
        return p;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the curve for the given parametric distance along
     * the curve, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     Parametric distance to calculate derivatives for (0.0 to 1.0
     *              inclusive).
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, int grade) {
        Point sT = _u.getRealPoint(s);  //  Convert from 0-1 to parametric position range of _u curve.

        //  Calculate the derivatives of the child object.
        List<List<Vector<Length>>> dersLst = _child.getDerivatives(sT, grade);
        Point origin = Point.valueOf(dersLst.get(0).get(0));

        //  Sort out what to output.
        List<Vector<Length>> ders = null;
        if (_child.getParDimension() == 1)
            ders = dersLst.get(0);

        else if (_child.getParDimension() == 2) {
            //  Have to account for derivatives in 2-dimensions.

            //  Get the direction cosines of the parametric curve tangent vector.
            Vector<Dimensionless> utangent = _u.getTangent(s);
            double ks = utangent.get(Vector.X).getValue();
            double kt = utangent.get(Vector.Y).getValue();

            //  Get the surface derivatives in the s and t directions.
            List<Vector<Length>> dP_dss = dersLst.get(0);
            List<Vector<Length>> dP_dts = dersLst.get(1);

            //  Create the output array and put in the surface/curve point.
            ders = dP_dss;

            //  Use the direction cosines to combine the surface s & t direction
            //  derivatives into the curve's s direction: dP_dsc = ks*dP_dss + kt*dP_dts
            for (int i = 1; i <= grade; ++i)
                ders.set(i, dP_dss.get(i).times(ks).plus(dP_dts.get(i).times(kt)));

        } else
            throw new UnsupportedOperationException(
                    MessageFormat.format(RESOURCES.getString("scUnsupportedParDim"),
                            _child.getParDimension()));

        //  Scale all the derivatives by the ratio of parameter arc length to the
        //  normal parameter range (uL/1).
        double f = 1;
        for (int i = 1; i <= grade; ++i) {
            f *= uL;
            ders.set(i, ders.get(i).times(f));
        }

        //  Set the derivative vector origin points.
        for (int i = 0; i <= grade; ++i)
            ders.get(i).setOrigin(origin);

        return ders;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        return _boundsMin;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        return _boundsMax;
    }

    private static final int NPTS = 21;

    /**
     * Calculate the minimum & maximum bounding box corner points of this geometry
     * element.
     */
    private void calcBoundsMinMax() {
        StackContext.enter();
        try {
            //  Space out some points on the subrange curve.
            List<Double> spacing = GridSpacing.linear(NPTS);
            PointString<SubrangePoint> pstr = _u.extractGrid(GridRule.PAR, spacing);
            PointString<Point> str = PointString.newInstance();
            for (int i = 0; i < NPTS; ++i) {
                Point ppnt = pstr.get(i).copyToReal();
                str.add(_child.getRealPoint(ppnt));
            }

            //  Get the bounds of the grid of points.
            _boundsMin = StackContext.outerCopy(str.getBoundsMin());
            _boundsMax = StackContext.outerCopy(str.getBoundsMax());

            //  Compute the arc length of the parametric curve and store it for later use.
            uL = _u.getArcLength(GTOL * 100).getValue();

        } finally {
            StackContext.exit();
        }

    }

    /**
     * Return <code>true</code> if this SubrangeCurve contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     *
     * @return true if this element contains valid and finite numerical values.
     */
    @Override
    public boolean isValid() {
        return _u.isValid() && _child.isValid();
    }

    /**
     * Return <code>true</code> if this curve is degenerate (i.e.: has length less than
     * the specified tolerance).
     *
     * @param tol The tolerance for determining if this curve is degenerate. May not be
     *            null.
     * @return true if this curve is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        if (_u.isDegenerate(requireNonNull(tol)))
            return true;
        return _child.isDegenerate(tol);
    }

    /**
     * Returns the unit in which this curves values are stated.
     *
     * @return The unit that this object is stated in.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this curve but stated in the specified unit.
     *
     * @param unit the length unit of the curve to be returned. May not be null.
     * @return an equivalent to this curve but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public SubrangeCurve to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        SubrangeCurve crv = SubrangeCurve.newInstance(_child.to(unit), _u);
        return copyState(crv);
    }

    /**
     * Return the equivalent of this curve converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the curve to return.
     * @return The equivalent of this curve converted to the new dimensions.
     */
    @Override
    public SubrangeCurve toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        SubrangeCurve crv = SubrangeCurve.newInstance(_child.toDimension(newDim), _u);
        return copyState(crv);
    }

    /**
     * Return a NURBS curve representation of this curve to within the specified
     * tolerance.
     *
     * @param tol The greatest possible difference between this curve and the NURBS
     *            representation returned. May not be null.
     * @return A NURBS curve that represents this curve to within the specified tolerance.
     */
    @Override
    public NurbsCurve toNurbs(Parameter<Length> tol) {
        requireNonNull(tol);
        StackContext.enter();
        try {

            //  Grid some points onto the curve.
            PointString<SubrangePoint> str = this.gridToTolerance(tol);

            //  Fit a cubic NURBS curve to the points.
            int deg = 3;
            if (str.size() <= 3)
                deg = str.size() - 1;
            BasicNurbsCurve curve = CurveFactory.fitPoints(deg, str);
            copyState(curve);   //  Copy over the super-class state for this curve to the new one.

            return StackContext.outerCopy(curve);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new curve that is identical to this one, but with the parameterization
     * reversed.
     *
     * @return A new curve identical to this one, but with the parameterization reversed.
     */
    @Override
    public SubrangeCurve reverse() {
        SubrangeCurve crv = SubrangeCurve.newInstance(_child, _u.reverse());
        return copyState(crv);
    }

    /**
     * Split this {@link SubrangeCurve} at the specified parametric position returning a
     * list containing two curves (a lower curve with smaller parametric positions than
     * "s" and an upper curve with larger parametric positions).
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!).
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    @Override
    public GeomList<SubrangeCurve> splitAt(double s) {

        //  Split the parametric position curve.
        GeomList<Curve> pCrvs = _u.splitAt(s);

        //  Create the output list.
        GeomList<SubrangeCurve> output = GeomList.newInstance();

        //  Create the lower curve.
        output.add(SubrangeCurve.newInstance(_child, pCrvs.get(0)));
        output.add(SubrangeCurve.newInstance(_child, pCrvs.get(1)));

        GeomList.recycle(pCrvs);

        return output;
    }

    /**
     * Returns a copy of this {@link SubrangeCurve} instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this curve.
     */
    @Override
    public SubrangeCurve copy() {
        return SubrangeCurve.copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied). This method is not yet implemented and current returns a new
     * SubrangeCurve with the child and subrange curve geometry copied to real.
     *
     * @return A copy of this object with any transformations or subranges removed
     *         (applied).
     */
    @Override
    public SubrangeCurve copyToReal() {
        //  TODO: Add support for this.
        //throw new UnsupportedOperationException( RESOURCES.getString("scCopy2RealNotSupported") );
        return SubrangeCurve.newInstance((ParametricGeometry)_child.copyToReal(), _u.copyToReal());
    }

    /**
     * Returns transformed version of this element. The returned object implements is a
     * new SubrangeCurve instance with the transformed version of this object's child as
     * it's child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public SubrangeCurve getTransformed(GTransform transform) {
        requireNonNull(transform);
        return SubrangeCurve.newInstance((ParametricGeometry)_child.getTransformed(transform), _u);
    }

    /**
     * Compares this SubrangeCurve against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        SubrangeCurve that = (SubrangeCurve)obj;
        return this._u.equals(that._u)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return  31*super.hashCode() + Objects.hash(_u, _child);
    }

    /**
     * Returns the text representation of this geometry element.
     *
     * @return A text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        String className = this.getClass().getName();
        tmp.append(className.substring(className.lastIndexOf(".") + 1));

        tmp.append(": {child = {\n");
        tmp.append(_child.toText());
        tmp.append("}\n");
        tmp.append(", u = {\n");
        tmp.append(_u.toText());
        tmp.append("}\n");

        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<SubrangeCurve> XML = new XMLFormat<SubrangeCurve>(SubrangeCurve.class) {

        @Override
        public SubrangeCurve newInstance(Class<SubrangeCurve> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, SubrangeCurve obj) throws XMLStreamException {
            AbstractCurve.XML.read(xml, obj);     // Call parent read.

            Curve par = xml.get("ParPos");
            obj._u = par;
            ParametricGeometry child = xml.get("Child");
            obj._child = child;
            if (!(par instanceof Immutable))
                par.addChangeListener(obj._childChangeListener);
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
            obj.calcBoundsMinMax();

        }

        @Override
        public void write(SubrangeCurve obj, OutputElement xml) throws XMLStreamException {
            AbstractCurve.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._u, "ParPos");
            xml.add(obj._child, "Child");

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private SubrangeCurve() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<SubrangeCurve> FACTORY = new ObjectFactory<SubrangeCurve>() {
        @Override
        protected SubrangeCurve create() {
            return new SubrangeCurve();
        }

        @Override
        protected void cleanup(SubrangeCurve obj) {
            obj.reset();
            if (!(obj._u instanceof Immutable))
                obj._u.removeChangeListener(obj._childChangeListener);
            obj._u = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
            obj._boundsMax = null;
            obj._boundsMin = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static SubrangeCurve copyOf(SubrangeCurve original) {
        SubrangeCurve obj = FACTORY.object();
        obj._u = original._u.copy();
        obj._child = original._child.copy();
        obj._boundsMax = original._boundsMax.copy();
        obj._boundsMin = original._boundsMin.copy();
        if (!(obj._u instanceof Immutable))
            obj._u.addChangeListener(obj._childChangeListener);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        original.copyState(obj);
        return obj;
    }

}
