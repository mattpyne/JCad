/*
 * MeshUtil  -- Static utility methods used to work with triangular meshes.
 *
 * Copyright (C) 2018, by Joseph A. Huwaldt
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package geomss.geom;

import static geomss.geom.GeomUtil.SQRT_EPS;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import jahuwaldt.util.PointRegionQuadTree;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.StackContext;

/**
 * A collection of static methods used to work with triangular unstructured meshes.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 4, 2018
 * @version April 11, 2018
 */
public final class MeshUtil {
    
    private static final boolean DEBUG = false;
    
    private static final double SQRT3 = Math.sqrt(3);
    private static final double HALF_SQRT3 = 0.5*SQRT3; //  Height of equilateral triangle with side length of 1.
    private static final double IDEAL_AR = 3./SQRT3;    //  Aspect ratio of an equilateral triangle.
    
    
    /**
     * Generate an unstructured mesh/grid on a planar region defined by the input boundary
     * points. This version of this method uses a default minimum triangle area and does
     * not apply any smoothing.
     *
     * @param boundaryPnts The ordered boundary points for the region being meshed. The
     *                     physical dimension of the points must be either 2 or 3. The
     *                     points must be planar and must form a closed loop with the
     *                     "inside" to the left of the unit vectors between each pair of
     *                     points (when looking down the unit vector).
     * @param tol          The geometric tolerance on the input points.
     * @return A list of triangles that represent the final unstructured mesh.
     * @see #advancingFrontPlanar(geomss.geom.PointString, jahuwaldt.js.param.Parameter, jahuwaldt.js.param.Parameter, jahuwaldt.js.param.Parameter) 
     */
    public static TriangleList<Triangle> advancingFrontPlanar(PointString<? extends GeomPoint> boundaryPnts, Parameter<Length> tol) {
        Parameter<Area> minArea = tol.times(tol);
        return advancingFrontPlanar(boundaryPnts, tol, minArea, null);
    }
    
    /**
     * Generate an unstructured mesh/grid on a planar region defined by the input boundary
     * points.
     *
     * @param boundaryPnts The ordered boundary points for the region being meshed. The
     *                     physical dimension of the points must be either 2 or 3. The
     *                     points must be planar and must form a closed loop with the
     *                     "inside" to the left of the unit vectors between each pair of
     *                     points (when looking down the unit vector).
     * @param tol          The geometric tolerance on the input points.
     * @param minArea      The minimum area triangle to allow in the mesh.
     * @param smoothingEps The tolerance for stopping the smoothing process. Smoothing
     *                     stops when the maximum node position change in the mesh is less
     *                     than this value. If this value is null or zero, then no
     *                     smoothing is done.
     * @return A list of triangles that represent the final unstructured mesh.
     */
    @SuppressWarnings({"null", "SleepWhileInLoop"})
    public static TriangleList<Triangle> advancingFrontPlanar(PointString<? extends GeomPoint> boundaryPnts, 
            Parameter<Length> tol, Parameter<Area> minArea, Parameter<Length> smoothingEps) {
        
        //  Reference: The VGRID algorithm.
        //  Parikh,P., Pirzadeh, S., "Package for 3D Unstructured Grid Generation", NASA-CR-182090, Sept. 1990.
        
        TriangleList<Triangle> output = TriangleList.newInstance();
        int numPnts = boundaryPnts.size();
        if (numPnts < 3)
            return output;
        
        int ndim = boundaryPnts.getPhyDimension();
        if (ndim > 3 || ndim < 2)
            throw new IllegalArgumentException("Front physical dimension must be either 2 or 3.");

        if (numPnts == 3) {
            //  Degenerate case with only 3 points input (a single triangle output).
            Triangle tri = Triangle.valueOf(boundaryPnts);
            output.add(tri);
            return output;
        }
        
        //  The string of points must form a closed loop.
        if (!boundaryPnts.get(0).isApproxEqual(boundaryPnts.get(-1), tol))
            throw new IllegalArgumentException("Front points must form a closed loop with the 1st and last points equal in position.");
       
        if (numPnts == 4) {
            //  Degenerate case with only 4 points input (1st point == last point; a single triangle output).
            Triangle tri = Triangle.valueOf(boundaryPnts.getRange(0,2));
            output.add(tri);
            return output;
        }
        
        Triangle[] tri_arr = null;
        StackContext.enter();
        try {
            TriangleList<Triangle> mesh = TriangleList.newInstance();
            
            GTransform T_ZN = GTransform.IDENTITY;
            Parameter<Length> Zpos = null;
            if (ndim == 3) {
                //  Figure out if the front is planar.
                if (!boundaryPnts.isPlanar(tol))
                    throw new IllegalArgumentException("The boundary front must be planar to within tol.");

                //  Compute the normal vector for the point string.
                Vector<Dimensionless> nhat = getPointStringNormal(boundaryPnts);

                if (!nhat.get(Point.Z).isApproxEqual(Parameter.ONE)) {
                    //  Not already in the XY plane, so rotate it.

                    //  Create a unit vector in the coord. system Z axis direction.
                    Vector<Dimensionless> zhat = Vector.valueOf(Dimensionless.UNIT, 0, 0, 1);

                    //  Determine the rotation from nhat to zhat (Z wrt N).
                    T_ZN = GTransform.valueOf(nhat, zhat);

                    //  Transform the front from an arbitrary plane to parallelt to the XY plane.
                    boundaryPnts = boundaryPnts.getTransformed(T_ZN).copyToReal();
                }

                //  Get the Z-position of the planar point string.
                Zpos = boundaryPnts.get(0).get(Point.Z);

                //  Create a transformation for translating the Z position to 0.
                Parameter<Length> ZERO = Parameter.ZERO_LENGTH;
                T_ZN = GTransform.newTranslation(ZERO, ZERO, Zpos.opposite()).times(T_ZN);

                //  Convert the front to 2D (drop the Z component).
                boundaryPnts = boundaryPnts.toDimension(2);
            }

            //  Set up consistent units.
            Unit<Length> unit = boundaryPnts.getUnit();
            tol = tol.to(unit);
            minArea = minArea.to((Unit<Area>)unit.pow(2));
            
            //  Convert front points to be immutable and in the same units.
            //  Last point is redundant since it must be equal to the 1st.
            PointString<Point> boundaryNodes = PointString.newInstance();
            numPnts = numPnts - 1;
            for (int i = 0; i < numPnts; ++i)
                boundaryNodes.add(boundaryPnts.get(i).immutable().to(unit));

            //  Get the bounds of the boundary front points.
            Point pMin = boundaryNodes.getBoundsMin();
            Vector<Length> diag = Vector.valueOf(boundaryNodes.getBoundsMax().minus(pMin));

            //  Convert the list of input front points into a Heap List of line segments (faces)
            //  where the length of the segments determines the order in the list.
            //  Also link all the points to the faces they are connected to.
            PriorityQueue<LineSeg> front = createFrontHeapList(boundaryNodes);

            //  Determine the maximum starting front face length.
            double Lmax = 0;
            for (LineSeg face : front) {
                double L = face.getArcLength(0).getValue();
                if (L > Lmax)
                    Lmax = L;
            }

            //  Place all the existing points in a quad-tree.
            double tolv = tol.to(unit).getValue();
            PointRegionQuadTree<Point> tree = new PointRegionQuadTree(
                    pMin.minus(Point.valueOf(tol, tol)), diag.getValue(Point.X) + 2 * tolv, diag.getValue(Point.Y) + 2 * tolv);
            for (Point p : boundaryNodes) {
                boolean r = tree.insert(p);
                if (!r)
                    System.out.println("Error, couldn't add point " + p);
            }

            //  Create a list to store all of the iterior nodes in the mesh.
            GeomList<Point> interiorNodes = GeomList.newInstance();

            //  Loop over all the front faces until they have all been removed.
            double Rv = 0;
            while (!front.isEmpty()) {
                //  Find the next face to be removed from the front (the shortest one) 
                //  and remove it from the front.
                LineSeg face = front.poll();

                //  Determine a "best point" for the next triangle to be formed.
                //  Form stretched equilateral triangle with the face as the short side.
                Point p0 = face.getStart();
                Point p1 = face.getEnd();
                Point pmid = p0.plus(p1).divide(2);
                Vector<Dimensionless> face_n = GeomUtil.normal2D(face.getTangent(0));
                double Lface = p0.distanceValue(p1);
                double stretching = 1.5;
                double h = stretching * Lface * HALF_SQRT3;                // Height of equilateral triangle: h = 0.5*sqrt(3)*L
                Point pbest = pmid.plus(Point.valueOf(face_n.times(Parameter.valueOf(h, unit))));

                //  Find the adjacent face(s) to the face being removed.
                GeomList<LineSeg> adj_faces = getAdjacentFaces(face, face_n);

                //  Find all the close points to the new point.
                Rv = Math.max(Rv, 1.5 * Math.max(Lmax, h));
                double xmin = pbest.getValue(Point.X) - Rv;
                double ymin = pbest.getValue(Point.Y) - Rv;
                List<Point> closePoints = tree.queryRange(xmin, ymin, 2 * Rv, 2 * Rv);

                //  Filter out the end points of the current face.
                closePoints.remove(p0);
                closePoints.remove(p1);

                //  Find all the faces associated with all the closest points.
                List<LineSeg> closeFaces = GeomList.newInstance();
                for (Point p : closePoints)
                    addUniqueFaces(closeFaces, (List)p.getUserData("faces_AF"));

                //  Filter the close points to remove any that can not be the "best" point.
                filterClosePoints(closePoints, p0, face_n, pbest, adj_faces);

                int size = closePoints.size();
                if (size > 1)
                    //  Order the candidate points by apex angle (largest to smallest).
                    closePoints.sort(new ApexAngleComparator(face));

                //  Starting from the top of the list of close points, check to see if the
                //  new element formed by the point would cross any existing faces. If it does
                //  not cross, then take that point, otherwise move down the list.
                Parameter<Length> minD = face.getArcLength(0).times(0.5);
                Point pbest_orig = pbest;
                pbest = null;   //  Will cause exception to be thrown if there are no valid points.
                for (int i = 0; i < size; ++i) {
                    Point p = closePoints.get(i);

                    boolean hasIntersect = false;
                    for (LineSeg cface : closeFaces) {
                        if (lineSegsIntersect(cface, p, p0) || lineSegsIntersect(cface, p, p1)) {
                            hasIntersect = true;
                            break;
                        }

                        //  Try to avoid a newly selected point falling too close to an existing face.
                        if (size > 1 && p == pbest_orig) {
                            if (isPointNearLineSeg(cface, p, minD)) {
                                hasIntersect = true;
                                break;
                            }
                        }
                    }
                    if (!hasIntersect) {
                        pbest = p;
                        break;
                    }
                }
                if (pbest == null)
                    throw new NullPointerException("pbest is null -- problem with algorithm");

                if (pbest == pbest_orig)
                    //  A new point is being used.
                    interiorNodes.add(pbest);

                //  Is the best point associated with any faces already.
                GeomList<LineSeg> faces = (GeomList<LineSeg>)pbest.getUserData("faces_AF");
                if (faces == null) {
                    //  Got our initial guess point.

                    //  Create two new faces.
                    LineSeg f1 = LineSeg.valueOf(p0, pbest);
                    LineSeg f2 = LineSeg.valueOf(pbest, p1);
                    front.add(f1);
                    front.add(f2);

                    //  Update the linked-list of points-to-faces.
                    GeomList<LineSeg>p0Faces = (GeomList)p0.getUserData("faces_AF");
                    p0Faces.remove(face);
                    p0Faces.add(f1);
                    GeomList<LineSeg>p1Faces = (GeomList)p1.getUserData("faces_AF");
                    p1Faces.remove(face);
                    p1Faces.add(f2);
                    GeomList<LineSeg> pbFaces = GeomList.newInstance();
                    pbFaces.add(f1);
                    pbFaces.add(f2);
                    pbest.putUserData("faces_AF", pbFaces);
                    pbest.putUserData("active_AF", Boolean.TRUE);

                    //  Add the new point to the tree.
                    boolean r = tree.insert(pbest);
                    if (!r)
                        System.out.println("Error, couldn't add new point " + pbest);

                } else {
                    //  Got a point that is already on the front.

                    GeomList<LineSeg> p0Faces = (GeomList)p0.getUserData("faces_AF");
                    p0Faces.remove(face);
                    GeomList<LineSeg>p1Faces = (GeomList)p1.getUserData("faces_AF");
                    p1Faces.remove(face);
                    GeomList<LineSeg> pbFaces = (GeomList<LineSeg>)pbest.getUserData("faces_AF");
                    LineSeg f1 = commonFace(pbFaces, p0Faces);
                    if (f1 == null) {
                        //  Create a new face.
                        f1 = LineSeg.valueOf(p0, pbest);

                        //  Add the new face to the front.
                        front.add(f1);
                        p0Faces.add(f1);
                        pbFaces.add(f1);

                    } else {
                        //  Face already exists and will be removed from front by placing this triangle.
                        front.remove(f1);
                        p0Faces.remove(f1);
                        pbFaces.remove(f1);

                    }

                    LineSeg f2 = commonFace(pbFaces, p1Faces);
                    if (f2 == null) {
                        //  Create a new face.
                        f2 = LineSeg.valueOf(pbest, p1);

                        //  Add the new face to the front.
                        front.add(f2);
                        p1Faces.add(f2);
                        pbFaces.add(f2);

                    } else {
                        //  Face already exists and will be removed from front by placing this triangle.
                        front.remove(f2);
                        p1Faces.remove(f2);
                        pbFaces.remove(f2);

                    }

                    //  If a point no longer has any faces in it's faces list, then it is
                    //  no longer on the active front.
                    if (p0Faces.isEmpty())
                        p0.putUserData("active_AF", Boolean.FALSE);
                    if (p1Faces.isEmpty())
                        p1.putUserData("active_AF", Boolean.FALSE);
                }

                //  Construct a new triangle.
                Triangle tri = Triangle.valueOf(p0, p1, pbest);
                mesh.add(tri);

                //  Assign this new triangle to each of it's verticies.
                assignTriangle2Node(p0, tri);
                assignTriangle2Node(p1, tri);
                assignTriangle2Node(pbest, tri);

            }   //  end while(!front.isEmpty())

            //  Determine some geometric characteristics for each triangle.
            calcTriangleChar(mesh);

            //  Size before cleaning.
            int size = mesh.size();

            //  Clean the mesh by removing any triangles that are too small in area.
            removeSmallAreas(mesh, boundaryNodes, interiorNodes, minArea);

            //  Clean the mesh by removing short triangle edegs.
            removeShortEdges(mesh, boundaryNodes, interiorNodes, 0.35);
            
            //  Smooth the mesh if requested.
            smoothUnstructuredMesh2D(mesh, interiorNodes, smoothingEps);
            
            //  Remove all the user data from the triangle nodes.
            for (Point p : boundaryNodes) {
                p.removeUserData("active_AF");
                p.removeUserData("faces_AF");
                p.removeUserData("tris_AF");
            }
            for (Point p : interiorNodes) {
                p.removeUserData("active_AF");
                p.removeUserData("faces_AF");
                p.removeUserData("tris_AF");
            }

            //  Convert the geometry back to 3D and original orientation if necessary.
            if (Zpos != null)
                mesh = mesh.toDimension(3).getTransformed(T_ZN.transpose()).copyToReal();

            //  Copy the mesh triangles to the outer context.
            size = mesh.size();
            tri_arr = new Triangle[size];
            for (int i=0; i < size; ++i) {
                Triangle t = mesh.get(i);
                tri_arr[i] = StackContext.outerCopy(t);
            }
            
        } finally {
            StackContext.exit();
        }

        //  Add the array of triangles to the outut list.
        output.addAll(tri_arr);
        
        return output;
    }

    /**
     * Filter out any points from the input closePoints list that are not on the active
     * front, on the wrong side of the current face (p0, and face_n) or that are on the
     * wrong side of any adjacent faces to the current face.
     * 
     * @param closePoints   The list of close points to be filtered.
     * @param p0            The starting point for the current face.
     * @param face_n        The normal vector for the current face.
     * @param pbest         A guess at the "best" next point.
     * @param adj_faces     The list of adjacent faces to the current face.
     */
    private static void filterClosePoints(List<Point> closePoints, Point p0, 
            Vector<Dimensionless> face_n, Point pbest, GeomList<LineSeg> adj_faces) {
        //  Filter out any close points that are not on the front or that are on the
        //  wrong side of the face.
        int size = closePoints.size();
        for (int i = size - 1; i >= 0; --i) {
            Point p = closePoints.get(i);
            
            //  Is the point inactive?
            if (!(Boolean)p.getUserData("active_AF"))
                closePoints.remove(i);
            
            else {
                //  Is the point on the wrong side of the face.
                if (wrongSide(p0, face_n, p))
                    closePoints.remove(i);
            }
        }
        
        //  Add the guess at the best point to the list of candidate points.
        closePoints.add(pbest);
        
        //  Filter any points that are on the wrong side of any of the adjacent faces.
        size = closePoints.size();
        for (int i = size - 1; i >= 0; --i) {
            Point p = closePoints.get(i);
            for (LineSeg adj : adj_faces) {
                Vector<Dimensionless> adj_n = GeomUtil.normal2D(adj.getTangent(0));
                if (wrongSide(adj.getStart(), adj_n, p)) {
                    closePoints.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * Create an initial front of line segments from the ordered boundary nodes.
     * 
     * @param boundaryNodes The initial ordered boundary nodes.
     * @return A priority queue/heap list of the faces of the initial front.
     */
    private static PriorityQueue<LineSeg> createFrontHeapList(PointString<Point> boundaryNodes) {
        //  A PriorityQueue is Java's "Heap List" structure.
        int numPnts = boundaryNodes.size();
        PriorityQueue<LineSeg> front = new PriorityQueue(numPnts, new ShortestLengthComparator());
        GeomList<LineSeg> p0Faces = GeomList.newInstance();
        Point p0 = boundaryNodes.get(0);
        p0.putUserData("faces_AF", p0Faces);
        p0.putUserData("active_AF", Boolean.TRUE);  //  Point is actively a part of the front.
        for (int i = 1; i < numPnts; ++i) {
            GeomList<LineSeg> p1Faces = GeomList.newInstance();
            Point p1 = boundaryNodes.get(i);
            p1.putUserData("faces_AF", p1Faces);
            p1.putUserData("active_AF", Boolean.TRUE);
            
            LineSeg face = LineSeg.valueOf(p0, p1);
            front.add(face);
            p0Faces.add(face);
            p1Faces.add(face);
            p0 = p1;
            p0Faces = p1Faces;
        }
        Point p1 = boundaryNodes.get(0);    //  Because last point must equal 1st point (closed list of points).
        GeomList<LineSeg> p1Faces = (GeomList<LineSeg>)p1.getUserData("faces_AF");
        LineSeg face = LineSeg.valueOf(p0, p1);
        front.add(face);
        p0Faces.add(face);
        p1Faces.add(0, face);
        return front;
    }

    /**
     * Smooth the unstructured mesh by shifting each interior node to the center of the
     * surrounding polygon.
     *
     * @param tris  The list of all the triangles in the mesh.
     * @param nodes The list of all the interior nodes/points in the mesh.
     * @param eps   The tolerance for stopping the smoothing process. Smoothing stops when
     *              the maximum node position change in the mesh is less than this value.
     *              If this value is null or zero, then no smoothing is done.
     */
    private static void smoothUnstructuredMesh2D(TriangleList<Triangle> tris, 
            GeomList<Point> nodes, Parameter<Length> eps) {
        if (eps == null || eps.isApproxZero())
            return;
        
        //  Reference: Naji, H.S., "A C# Algorithm for Triangular Meshes of Highly-Irregular 
        //      2D Domains using Advancing Front Technique", JKAU, V17N2, pp 41-72, 2006.
        
        //  Loop over the smoothing process until the maximum node movement is less than eps.
        double eps2 = eps.getValue(tris.getUnit());
        eps2 = eps2*eps2;
        double ri2max = Double.MAX_VALUE;
        int iterations = 0;
        while (iterations < 100 && ri2max > eps2) {
            ++iterations;
            ri2max = 0;
            int size = nodes.size();
            for (int i=size-1; i >= 0; --i) {
                Point pi = nodes.get(i);
                
                //  Get the triangles surrounding node pi
                GeomList<Triangle> tris_pi = (GeomList)pi.getUserData("tris_AF");

                //  Get the points surrounding pi (the surrounding polygon).
                PointString<Point> etai = PointString.newInstance();
                for (Triangle t : tris_pi) {
                    GeomList<Point> tpnts = (GeomList)t.getAll();
                    addUniquePoints(etai, tpnts);
                    GeomList.recycle(tpnts);
                }
                etai.remove(pi);

                //  The new location for pi is average of surrounding points.
                Point pi2 = etai.getAverage();

                //  Distance squared between old and new interior node position.
                double ri2 = pi2.distanceSqValue(pi);
                ri2max = Math.max(ri2max, ri2);
                
                //  If the distance is greater than threshold, then replace the original node.
                if (ri2 >= eps2) {
                    //  Modify all the triangles associated with pi to replace pi with pi2.
                    replaceVertex(tris, pi, pi2);
                    nodes.remove(pi);
                    nodes.add(pi2);
                }
            }
            
            if (DEBUG) {
                System.out.println("smoothing iterations = " + iterations + ", rmax = " + Math.sqrt(ri2max));
            }
        }
        
    }
    
    /**
     * Replaces a mesh node/vertex with a new point while adjusting all the triangles that
     * share that point.
     * 
     * @param tris  The list of all the triangles in the mesh.
     * @param p1    The point that is being replaced.
     * @param p2    The point that is replacing p1.
     */
    private static void replaceVertex(TriangleList<Triangle> tris, Point p1, Point p2) {
        GeomList<Triangle> tris_p1 = (GeomList)p1.getUserData("tris_AF");
        GeomList<Triangle> tris_p2 = GeomList.newInstance();
        
        //  Loop over all the triangles associated with the point being replaced.
        for (Triangle t : tris_p1) {
            //  Remove this triangle from the triangles list (it is being replaced).
            tris.remove(t);

            //  Replace p1 with p2 in the list of vertices from this triangle.
            GeomList<Point> verts = (GeomList)t.getAll();
            int idx = verts.indexOf(p1);
            verts.remove(idx);
            verts.add(idx, p2);

            //  Create a new triangle with p1 replaced by p2.
            Triangle t2 = Triangle.valueOf(verts);
            calcTriangleChar(t2);
            
            //  Save off the new triangle in the triangle list.
            tris.add(t2);
            tris_p2.add(t2);
            
            //  Add the new triangle to the triangle lists for the other 2 points.
            for (int i=idx+1; i < 3; ++i) {
                Point pi = verts.get(i);
                GeomList<Triangle> tris_pi = (GeomList)pi.getUserData("tris_AF");
                tris_pi.remove(t);
                tris_pi.add(t2);
            }
            for (int i=0; i < idx; ++i) {
                Point pi = verts.get(i);
                GeomList<Triangle> tris_pi = (GeomList)pi.getUserData("tris_AF");
                tris_pi.remove(t);
                tris_pi.add(t2);
            }
            //GeomList.recycle(verts);
        }
        
        //  Associate the list of triangles that use p2 with node p2.
        p2.putUserData("tris_AF", tris_p2);
    }

    /**
     * Remove any non-boundary triangles where the shortest-to-longest side length ratio
     * is below a threshold and adjust neighboring triangles to fill in the gap.
     *
     * @param tris          The list of all the triangles with pre-computed
     *                      characteristics "short_long_AF" and "shortest_side_AF".
     * @param boundaryNodes The node points that formed the original boundary of the mesh.
     * @param interiorNodes The node points that are interior to the mesh.
     * @param threshold     The shortest-to-longest side length ratio threshold for
     *                      removing triangles.
     */
    private static void removeShortEdges(TriangleList<Triangle> tris, 
            PointString<Point> boundaryNodes, GeomList<Point> interiorNodes, double threshold) {
        
        //  Loop over all the triangles.  If any have a shortest edge to longest edge
        //  ratio < 0.35, then collapse the shortest edge to it's start (which removes two
        //  triangles and adjust any triangles that connect to the end of that edge
        //  to instead point to the start.
        int size = tris.size();
        for (int i = size - 1; i >= 0; --i) {
            Triangle t = tris.get(i);
            double lratio = (double)t.getUserData("short_long_AF");
            if (lratio < threshold) {
                //  Find the shortest edge of this triangle.
                int sedge = (int)t.getUserData("shortest_side_AF");

                //  Do not remove an edge of the original boundary.
                if (!isBoundaryEdge(boundaryNodes, t, sedge)) {
                    //  Remove the triangle by collapsing it's shortest edge.
                    int n = collapseTriangleEdge(tris, interiorNodes, t, sedge);
                    i -= n;
                }
            }
        }
        
    }
    
    /**
     * Remove any non-boundary triangles that are not on the boundary where the area is
     * below a threshold and adjust neighboring triangles to fill in the gap.
     *
     * @param tris          The list of all the triangles with pre-computed
     *                      characteristics "short_long_AF" and "shortest_side_AF".
     * @param boundaryNodes The node points that formed the original boundary of the mesh.
     * @param interiorNodes The node points that are interior to the mesh.
     * @param threshold     The triangle area threshold for removing triangles.
     */
    private static void removeSmallAreas(TriangleList<Triangle> tris,
            PointString<Point> boundaryNodes, GeomList<Point> interiorNodes, Parameter<Area> threshold) {
        
        if (DEBUG) {
            Comparator<Triangle> comparator = new Comparator<Triangle>() {
                @Override
                public int compare(Triangle o1, Triangle o2) {
                    return -o1.getArea().compareTo(o2.getArea());
                }
            };
            tris.sort(comparator);
        }
        
        //  Loop over all the triangles.  If any have a shortest edge to longest edge
        //  ratio < 0.35, then collapse the shortest edge to it's start (which removes two
        //  triangles and adjust any triangles that connect to the end of that edge
        //  to instead point to the start.
        int size = tris.size();
        for (int i = size - 1; i >= 0; --i) {
            Triangle t = tris.get(i);
            Parameter<Area> area = t.getArea();
            if (area.isLessThan(threshold)) {
                //  Find the shortest edge of this triangle.
                int sedge = (int)t.getUserData("shortest_side_AF");
                
                //  Do not remove an edge of the original boundary.
                if (!isBoundaryEdge(boundaryNodes, t, sedge)) {
                    //  Remove the triangle by collapsing it's shortest edge.
                    int n = collapseTriangleEdge(tris, interiorNodes, t, sedge);
                    i -= n;
                }
            }
        }
        
    }
    
    /**
     * Return true if the specified edge of the specified triangle falls on the original
     * boundary of the mesh.
     *
     * @param boundary The points that make up the original boundary of the mesh.
     * @param tri      The triangle being checked to see if it contains a boundary edge.
     * @param edge     The edge to check to see if it is on the outer boundary of the
     *                 mesh.
     * @return <code>true</code> if the specified edge of the triangle is on the outside
     *         boundary of the mesh.
     */
    private static boolean isBoundaryEdge(PointString<Point> boundary, Triangle tri, int edge) {

        //  Get the start and end points on the specified edge.
        Point p0, p1;
        switch (edge) {
            case 0:
                p0 = tri.getP1();
                p1 = tri.getP2();
                break;
            case 1:
                p0 = tri.getP2();
                p1 = tri.getP3();
                break;
            default:
                p0 = tri.getP3();
                p1 = tri.getP1();
                break;
        }

        return boundary.contains(p0) && boundary.contains(p1);
    }

    /**
     * Remove the specified triangle by collapsing the specified edge while adjusting all
     * the adjacent triangles to fill in the gap. This will result in the deletion of the
     * triangle that shares the specified edge and the adjustment of any triangles that
     * connect to the end of that edge to instead connect to the start of that edge.
     *
     * @param tris The list of all the triangles in the mesh.
     * @param interiorNodes The node points that are interior to the mesh.
     * @param t1   The triangle being deleted.
     * @param edge The edge to be collapsed in order to delete the triangle.
     * @return The number of triangles deleted from the mesh. Will be either 0 (if t1
     *         could not be deleted) or 2.
     */
    private static int collapseTriangleEdge(TriangleList<Triangle> tris, GeomList<Point> interiorNodes, 
            Triangle t1, int edge) {

        //  Get the start and end points on the specified edge.
        Point p0, p1;
        switch (edge) {
            case 0:
                p0 = t1.getP1();
                p1 = t1.getP2();
                break;
            case 1:
                p0 = t1.getP2();
                p1 = t1.getP3();
                break;
            default:
                p0 = t1.getP3();
                p1 = t1.getP1();
                break;
        }

        //  Get all the triangles assocated with these points.
        GeomList<Triangle> tris_p0 = (GeomList)p0.getUserData("tris_AF");
        GeomList<Triangle> tris_p1 = (GeomList)p1.getUserData("tris_AF");

        //  Find the "other" triangle in common between these points besides "t1".
        Triangle t2 = null;
        outer:
        for (Triangle t_0 : tris_p0) {
            if (t_0 == t1)
                continue;
            for (Triangle t_1 : tris_p1) {
                if (t_1 == t1)
                    continue;
                if (t_1 == t_0) {
                    t2 = t_1;
                    break outer;
                }
            }
        }
        if (t2 == null) {
            System.out.println("Unexpected t2 == null in MeshUtil.removeShortEdges().");
            return 0;
        }

        //  Delete t1 and t2 from the triangle lists.
        tris_p0.remove(t1);
        tris_p0.remove(t2);
        tris_p1.remove(t1);
        tris_p1.remove(t2);
        tris.remove(t1);
        tris.remove(t2);
        
        //  Delete the point p1 from the list of interior nodes.
        interiorNodes.remove(p1);

        //  Replace all the other triangles that touch p1 with versions that are
        //  adjusted to touch p0 instead.
        for (Triangle t : tris_p1) {
            //  Remove this triangle from the triangles list (it is being replaced).
            tris.remove(t);

            //  Replace p1 with p0 in the list of vertices from this triangle.
            GeomList<Point> verts = (GeomList)t.getAll();
            int idx = verts.indexOf(p1);
            verts.remove(idx);
            verts.add(idx, p0);

            //  Create a new triangle with p1 replaced by p0.
            Triangle t_2 = Triangle.valueOf(verts);
            calcTriangleChar(t_2);
            
            //  Save off the new triangle in the triangle list.
            tris.add(t_2);
            tris_p0.add(t_2);
            
            //  Add the new triangle to the triangle lists for the other 2 points.
            for (int i=idx+1; i < 3; ++i) {
                Point pi = verts.get(i);
                GeomList<Triangle> tris_pi = (GeomList)pi.getUserData("tris_AF");
                tris_pi.remove(t1);
                tris_pi.remove(t2);
                tris_pi.remove(t);
                tris_pi.add(t_2);
            }
            for (int i=0; i < idx; ++i) {
                Point pi = verts.get(i);
                GeomList<Triangle> tris_pi = (GeomList)pi.getUserData("tris_AF");
                tris_pi.remove(t1);
                tris_pi.remove(t2);
                tris_pi.remove(t);
                tris_pi.add(t_2);
            }
            GeomList.recycle(verts);
        }

        return 2;
    }

    /**
     * Calculate the characteristics for a list of triangles. The computed values are
     * stored in each triangle's user data with the following keys:
     * <pre>
     *      aspect_ratio_AF = (double) Aspect ratio scaled relative to that of an equilateral triangle.
     *      short_long_AF = (double) Ratio of the shortest to longest sides of the triangle.
     *      shortest_side_AF = (int) Index of the shortest side of the triangle.
     * </pre>
     *
     * @param tris The list of triangles to have the characteristics calculated for.
     */
    private static void calcTriangleChar(TriangleList<Triangle> tris) {
        for (Triangle t : tris) {
            calcTriangleChar(t);
        }
    }

    /**
     * Calculate some geometric characteristics of the input triangle. The computed values
     * are stored in the triangle's user data with the following keys:
     * <pre>
     *      aspect_ratio_AF = (double) Aspect ratio scaled relative to that of an equilateral triangle.
     *      short_long_AF = (double) Ratio of the shortest to longest sides of the triangle.
     *      shortest_side_AF = (int) Index of the shortest side of the triangle.
     * </pre>
     *
     * @param tri The triangle to have the characteristics calculated for.
     */
    private static void calcTriangleChar(Triangle tri) {
        //  The diameter of the incircle of the triangle.
        //  Ref: https://en.wikipedia.org/wiki/Incircle_and_excircles_of_a_triangle#Relation_to_area_of_the_triangle
        Point p1 = tri.getP1();
        Point p2 = tri.getP2();
        Point p3 = tri.getP3();
        double a = p1.distanceValue(p2);
        double b = p2.distanceValue(p3);
        double c = p3.distanceValue(p1);
        double s = 0.5 * (a + b + c);               //  Semi-permieter of triangle.
        double d = 2*tri.getArea().getValue()/s;    //  In-circle diameter.

        //  The triangle "scaled aspect ratio".
        double lmax = Math.max(Math.max(a, b), c);
        double AR = lmax/d/IDEAL_AR;                //  Scaled relative to an equilateral triangle.
        tri.putUserData("aspect_ratio_AF", AR);

        //  The shortest side of the triangle.
        s = Math.min(Math.min(a, b), c);
        int shortest = 2;
        if (s == a)
            shortest = 0;
        else if (s == b)
            shortest = 1;
        tri.putUserData("shortest_side_AF", shortest);
        tri.putUserData("short_long_AF", s / lmax);
    }
    
    /**
     * Assign the input triangle to the list of associated triangles stored in the user
     * data for the input point. If the input point doesn't have a list of triangles, it
     * is created and the input triangle is added to it.
     *
     * @param p   The point to have the triangle added to the list of associated
     *            triangles.
     * @param tri The triangle associated with the point.
     */
    private static void assignTriangle2Node(Point p, Triangle tri) {
        GeomList<Triangle> tris = (GeomList)p.getUserData("tris_AF");
        if (tris == null) {
            tris = GeomList.newInstance();
            p.putUserData("tris_AF", tris);
        }
        tris.add(tri);
    }
    
    /**
     * Return a list of adjacent faces that are oriented correctly and on the "inside" of 
     * the target face.
     *
     * @param face   The face to return the adjacent faces for.
     * @param face_n The pre-calculated face normal for face.
     * @return A list of adjacent faces. May contain 1 or two faces.
     */
    private static GeomList<LineSeg> getAdjacentFaces(LineSeg face, geomss.geom.Vector<Dimensionless> face_n) {
        Point p0 = face.getStart();
        Point p1 = face.getEnd();

        GeomList<LineSeg> adj_faces = GeomList.newInstance();
        adj_faces.addAll((GeomList<LineSeg>)p0.getUserData("faces_AF"));
        adj_faces.remove(face);
        adj_faces.addAll((GeomList<LineSeg>)p1.getUserData("faces_AF"));
        adj_faces.remove(face);

        //  Remove any adjacent faces that are oriented the wrong way or are on the wrong
        //  side of the input face.
        int size = adj_faces.size();
        for (int i = size - 1; i >= 0; --i) {
            LineSeg adj = adj_faces.get(i);
            Point ps = adj.getStart();
            Point pe = adj.getEnd();
            if (ps.isApproxEqual(p0))
                //  Adjacent-face starts at the start of the input face.
                //  This adjacent-face is oriented the wrong way, remove it.
                adj_faces.remove(i);
            else if (ps.isApproxEqual(p1)) {
                //  Adjacent-face starts at the end of the face.
                //  The other end of the adjacent-face must be on the inward side of the input face.
                if (wrongSide(p0, face_n, pe))
                    adj_faces.remove(i);
            } else if (pe.isApproxEqual(p0)) {
                //  Adjacent-face ends at the start of the face.
                //  The other end of the adjacent-face must be on the inward side of the input face.
                if (wrongSide(p0, face_n, ps))
                    adj_faces.remove(i);
            } else {    // else if (pe.isApproxEqual(p1)) {
                //  Adjacent-face ends at the end of the input face.
                //  This adjacent-face is oriented the wrong way, remove it.
                adj_faces.remove(i);
            }
        }

        return adj_faces;
    }

    /**
     * Find and return the 1st edge/face in common between the two lists of faces.
     * 
     * @param faces1    The 1st list of faces.
     * @param faces2    The 2nd list of faces.
     * @return The first face found in common between the two input lists.
     */
    private static LineSeg commonFace(GeomList<LineSeg> faces1, GeomList<LineSeg> faces2) {
        LineSeg common = null;
        for (LineSeg f : faces1) {
            if (faces2.contains(f)) {
                common = f;
                break;
            }
        }
        return common;
    }
    
    /**
     * Return true if the input point is on the "wrong side" of the input face.
     * If the point falls on the input line segment, it is not on the wrong side.
     * 
     * @param p0 Any point on the face being tested against.
     * @param face_n The normal vector for the face the point is being tested against.
     * @param p The point being tested.
     * @return True if the point is on the wrong side of the face.
     */
    private static boolean wrongSide(Point p0, Vector<Dimensionless> face_n, Point p) {
        StackContext.enter();
        try {
            Vector<Length> v = p.minus(p0).toGeomVector();
            Parameter dotp = v.dot(face_n);
            return dotp.getValue() < -MathTools.EPS;
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Return true if the two specified line segments intersect.
     * 
     * @param seg1  The 1st line segment being tested for intersection.
     * @param s2p0  The start point of the 2nd line segment being tested for intersection.
     * @param s2p1  The end point of the 2nd line segment being tested for intersection.
     * @return true if the two line segments intersect.  Touching at the ends is not
     * considered an intersection.
     */
    private static boolean lineSegsIntersect(LineSeg seg1, Point s2p0, Point s2p1) {
        //  Ref: https://martin-thoma.com/how-to-check-if-two-line-segments-intersect/
        StackContext.enter();
        try {
            LineSeg seg2 = LineSeg.valueOf(s2p0, s2p1);
            
            //  Check for intersection.
            return GeomUtil.boundsIntersect(seg1, seg2) &&
                    lineSegmentCrossesLine2D(seg1, seg2) &&
                    lineSegmentCrossesLine2D(seg2, seg1);
            
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Check if line segment a crosses the line that is defined by line segment b. If the
     * ends touch, that does not count as "crossing".
     *
     * @param a first line segment interpreted as line
     * @param b second line segment interpreted as line
     * @return <code>true</code> if line segment a crosses line b in the interior,
     *         <code>false</code> otherwise.
     */
    private static boolean lineSegmentCrossesLine2D(LineSeg a, LineSeg b) {
        return !isPointOnLine(a, b.getStart()) && !isPointOnLine(a,b.getEnd()) && 
                isPointRightOfLine2D(a, b.getStart()) ^ isPointRightOfLine2D(a, b.getEnd());
    }

    /**
     * Checks if a point is to the right of a line. If the point is on the line, it is not
     * right of the line.
     *
     * @param a line segment interpreted as a line
     * @param b the point
     * @return <code>true</code> if the point is right of the line, <code>false</code>
     *         otherwise
     */
    private static boolean isPointRightOfLine2D(LineSeg a, Point b) {
        // Move the line segment and point, so that a.getStart() is on (0,0).
        Point ap0 = a.getStart();
        Point ap1 = a.getEnd();
        double x0 = ap0.getValue(Point.X);
        double y0 = ap0.getValue(Point.Y);
        double ax = ap1.getValue(Point.X) - x0;
        double ay = ap1.getValue(Point.Y) - y0;
        double bx = b.getValue(Point.X) - x0;
        double by = b.getValue(Point.Y) - y0;
        double r = cross2D(ax,ay, bx,by);
        boolean result = r < -MathTools.EPS;
        return result;
    }
    
    /**
     * Checks if a Point is on a line.
     *
     * @param a line (interpreted as line, although given as line segment).
     * @param b point tested to see if it is on the line.
     * @return <code>true</code> if point is on line, otherwise <code>false</code>
     */
    private static boolean isPointOnLine(LineSeg a, Point b) {
        // Move the line segment and point, so that a.getStart() is on (0,0).
        Point ap0 = a.getStart();
        Point ap1 = a.getEnd();
        double x0 = ap0.getValue(Point.X);
        double y0 = ap0.getValue(Point.Y);
        double ax = ap1.getValue(Point.X) - x0;
        double ay = ap1.getValue(Point.Y) - y0;
        double bx = b.getValue(Point.X) - x0;
        double by = b.getValue(Point.Y) - y0;
        double r = cross2D(ax,ay, bx,by);
        boolean result = MathTools.isApproxZero(r);
        return result;
    }
    
    /**
     * Return the cross product between two 2D vectors.
     *
     * @param ax The X-component of the 1st vector.
     * @param ay The Y-component of the 1st vector.
     * @param bx The X-component of the 2nd vector.
     * @param by The Y-component of the 2nd vector.
     * @return The cross product of the two 2D vectors.
     */
    private static double cross2D(double ax, double ay, double bx, double by) {
        return ax * by - bx * ay;
    }
    
    /**
     * Return true if a point is near a line segment to within the specified tolerance.
     * 
     * @param seg The line segment to see if a point is near.
     * @param p The point to test for being near the line segment.
     * @param tol The tolerance for being near the line segment.
     * @return <code>true</code> if the point is closer to the line segment than tol.
     */
    private static boolean isPointNearLineSeg(LineSeg seg, Point p, Parameter<Length> tol) {
        StackContext.enter();
        try {
            
            Parameter<Length> d = GeomUtil.pointLineSegDistance(p, seg.getStart(), seg.getEnd());
            return d.isLessThan(tol);
            
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Add any faces in list2 to list1 that are not already in list1.
     * 
     * @param list1 The list to add the faces to.
     * @param list2 The list to pull any unique faces from.
     */
    private static void addUniqueFaces(List<LineSeg> list1, List<LineSeg> list2) {
        for (LineSeg f : list2) {
            if (!list1.contains(f))
                list1.add(f);
        }
    }
    
    /**
     * Add any points in list2 to list1 that are not already in list1.
     * 
     * @param list1 The list to add the points to.
     * @param list2 The list to pull any unique points from.
     */
    private static void addUniquePoints(List<Point> list1, List<Point> list2) {
        for (Point f : list2) {
            if (!list1.contains(f))
                list1.add(f);
        }
    }
    
    /**
     * A Comparator that compares the length of a pair of line segments and identifies
     * the one with the shortest length.
     */
    private static class ShortestLengthComparator implements Comparator<LineSeg> {
        
        @Override
        public int compare(LineSeg o1, LineSeg o2) {
            Parameter len1 = o1.getArcLength(0);
            Parameter len2 = o2.getArcLength(0);
            return len1.compareTo(len2);
        }
        
    }
    
    /**
     * A Comparator that compares the apex angles of the triangle formed by the defined
     * face segment and a pair of points and identifies the one with the largest apex angle.
     */
    private static class ApexAngleComparator implements Comparator<Point> {
        private final LineSeg face;
        
        public ApexAngleComparator(LineSeg face) {
            this.face = face;
        }
        
        @Override
        public int compare(Point o1, Point o2) {
            Point p0 = face.getStart();
            Point p1 = face.getEnd();
            
            double cosphi1 = cosC(p0, p1, o1);
            double cosphi2 = cosC(p0, p1, o2);
            
            //  If cos(phi1) < cos(phi2) then phi1 > phi2.
            return Double.compare(cosphi1, cosphi2);
        }
    }
    
    /**
     * Returns the cosine of the interior angle in corner "C" formed by a triangle
     * consisting of points pA, pB and pC.
     *
     * @param pA The first point of a triangle.
     * @param pB The second point of a triangle.
     * @param pC The third point of a triangle where the interior angle is to be
     *           calculated.
     * @return The cosine of the angle in the corner of a triangle at point pC.
     */
    private static double cosC(Point pA, Point pB, Point pC) {
        double a = pB.distanceValue(pC);
        double b = pA.distanceValue(pC);
        double c2 = pB.distanceSqValue(pA);
        double cosC = (a*a + b*b - c2)/(2*a*b);
        return cosC;
    }
    
    /**
     * Returns the normal vector for a planar PointString. No check is made here for 
     * the point string being planar.
     * 
     * @param pnts The point string to return the normal vector for.
     * @return The normal vector for the planar PointString.
     */
    private static Vector<Dimensionless> getPointStringNormal(PointString<? extends GeomPoint> pnts) {

        StackContext.enter();
        try {
            //  Extract 3 points from the string of points and form a plane form them.
            GeomPoint p0 = pnts.get(0);
            int numPnts = pnts.size();
            int idx1 = numPnts / 3;
            if (idx1 == 0)
                ++idx1;
            int idx2 = 2 * numPnts / 3;
            if (idx2 == idx1)
                ++idx2;
            GeomPoint p1 = pnts.get(idx1);
            GeomPoint p2 = pnts.get(idx2);
            Vector<Length> v1 = p1.minus(p0).toGeomVector();
            Vector<Length> v2 = p2.minus(p0).toGeomVector();
            Vector n = v1.cross(v2);
            Vector<Dimensionless> nhat;
            if (n.magValue() > SQRT_EPS) {
                nhat = n.toUnitVector();
                nhat.setOrigin(Point.newInstance(p0.getPhyDimension()));

            } else {
                //  Try a different set of points on the string.
                idx1 = numPnts / 4;
                if (idx1 == 0)
                    ++idx1;
                idx2 = 3 * numPnts / 4;
                if (idx2 == idx1)
                    ++idx2;
                p1 = pnts.get(idx1);
                p2 = pnts.get(idx2);
                v1 = p1.minus(p0).toGeomVector();
                v2 = p2.minus(p0).toGeomVector();
                n = v1.cross(v2);
                if (n.magValue() > SQRT_EPS) {
                    nhat = n.toUnitVector();
                    nhat.setOrigin(Point.newInstance(p0.getPhyDimension()));
                } else {
                    //  Points from input curve likely form a straight line.
                    nhat = GeomUtil.calcYHat(p1.minus(p2).toGeomVector().toUnitVector());
                    nhat = v2.cross(nhat).toUnitVector();
                }
            }

            return StackContext.outerCopy(nhat);

        } finally {
            StackContext.exit();
        }
    }

}
