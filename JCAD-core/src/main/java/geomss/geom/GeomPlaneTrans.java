/**
 * GeomPlaneTrans -- A GeomTransform that has an GeomPlane for a child.
 *
 * Copyright (C) 2009-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} element that refers to a {@link GeomPlane} object and
 * masquerades as a GeomPlane object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 14, 2009
 * @version September 13, 2016
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class GeomPlaneTrans extends GeomPlane implements GeomTransform<GeomPlane> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private GeomPlane _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link GeomPlaneTrans} instance holding the specified
     * {@link GeomPlane} and {@link GTransform}.
     *
     * @param child     The plane that is the child of this transform element (may not be
     *                  <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     * @throws DimensionException if the input element is not 3D.
     */
    public static GeomPlaneTrans newInstance(GeomPlane child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "plane", child.getPhyDimension()));

        GeomPlaneTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;

        //  Listen for changes to the child object and pass them on.
        child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation represented by this transformation element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     *
     * @return The total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this transform element.
     */
    @Override
    public GeomPlane getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public Plane copyToReal() {
        GeomVector n = _child.getNormal();
        VectorTrans nt = n.getTransformed(_TM);
        n = nt.copyToReal();

        Point refPoint = _TM.transform(_child.getRefPoint()).copyToReal();
        Plane P = Plane.valueOf(n, refPoint);

        _child.copyState(P);

        return P;
    }

    /**
     * Return an immutable version of this plane.
     *
     * @return An immutable version of this plane.
     */
    @Override
    public Plane immutable() {
        return copyToReal();
    }

    /**
     * Recycles a <code>GeomPlaneTrans</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(GeomPlaneTrans instance) {
        requireNonNull(instance, MessageFormat.format(RESOURCES.getString("paramNullErr"), "instance"));
        FACTORY.recycle(instance);
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns 3.
     */
    @Override
    public int getPhyDimension() {
        return 3;
    }

    /**
     * Return the normal vector for the plane. The normal vector is a unit vector that is
     * perpendicular to the plane.
     *
     * @return The normal vector for the plane.
     */
    @Override
    public GeomVector<Dimensionless> getNormal() {
        return _child.getNormal().getTransformed(_TM);
    }

    /**
     * Return the constant term of the plane point (e.g.: "D" for a 3D plane:
     * <code>A*x + B*y + C*z = D</code>).
     *
     * @return The constant term of the plane point for this plane.
     */
    @Override
    public Parameter<Length> getConstant() {
        GeomPointTrans pt = getRefPoint();
        Vector<Length> v = Vector.valueOf(pt);
        GeomVector<Dimensionless> n = getNormal();

        Parameter<Length> c = (Parameter<Length>)n.dot(v);

        return c;
    }

    /**
     * Return the reference point for this plane. The reference point is an arbitrary
     * point that is contained in the plane and is used as a reference when drawing the
     * plane.
     *
     * @return The reference point for this plane.
     */
    @Override
    public GeomPointTrans getRefPoint() {
        return _child.getRefPoint().getTransformed(_TM);
    }

    /**
     * Return the equivalent of this plane converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the plane to return. MUST equal 3.
     * @return The equivalent of this plane converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public GeomPlaneTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Returns the unit in which the geometry in this element are stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this element but stated in the specified unit.
     * <p>
     * WARNING: If the unit changes, then the returned transform element DOES NOT refer
     * back to the original plane (the link with the original plane is broken).
     * </p>
     *
     * @param unit the length unit of the element to be returned. May not be null.
     * @return an equivalent to this element but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public GeomPlane to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        GeomPlaneTrans newPlane = GeomPlaneTrans.newInstance(_child.to(unit), _TM);
        return newPlane;
    }

    /**
     * Compares this GeomPlaneTrans against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        GeomPlaneTrans that = (GeomPlaneTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this object.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Returns a copy of this GeomPlaneTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public GeomPlaneTrans copy() {
        return copyOf(this);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<GeomPlaneTrans> XML = new XMLFormat<GeomPlaneTrans>(GeomPlaneTrans.class) {

        @Override
        public GeomPlaneTrans newInstance(Class<GeomPlaneTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, GeomPlaneTrans obj) throws XMLStreamException {
            GeomPlane.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            GeomPlane child = xml.getNext();
            obj._child = child;

            //  Listen for changes to the child object and pass them on.
            child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(GeomPlaneTrans obj, OutputElement xml) throws XMLStreamException {
            GeomPlane.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private GeomPlaneTrans() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<GeomPlaneTrans> FACTORY = new ObjectFactory<GeomPlaneTrans>() {
        @Override
        protected GeomPlaneTrans create() {
            return new GeomPlaneTrans();
        }

        @Override
        protected void cleanup(GeomPlaneTrans obj) {
            obj.reset();
            obj._TM = null;
            obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static GeomPlaneTrans copyOf(GeomPlaneTrans original) {
        GeomPlaneTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

}
