/**
 * GeomPointTrans -- A GeomTransform that has a GeomPoint for a child.
 *
 * Copyright (C) 2009-2018, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.*;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.lang.Immutable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A {@link GeomTransform} element that refers to a {@link GeomPoint} object and
 * masquerades as a GeomPoint object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 30, 2009
 * @version April 10, 2018
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class GeomPointTrans extends GeomPoint implements GeomTransform<GeomPoint> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private GeomPoint _child;

    //  The child point with the transformation applied to it (an optimization).
    private Point _pTrans;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link GeomPointTrans} instance holding the specified
     * {@link GeomPoint} and {@link GTransform}.
     *
     * @param child     The point that is the child of this transform element (may not be
     *                  <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     * @throws DimensionException if the input element is not 3D.
     */
    public static GeomPointTrans newInstance(GeomPoint child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "point", child.getPhyDimension()));

        GeomPointTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;
        obj._pTrans = transform.transform(child);
        child.copyState(obj._pTrans);
        child.copyState(obj);

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation represented by this transformation element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     *
     * @return The total transformation represented by an entire chain of GeomTransform
     *         objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        _pTrans = transform.transform(_child);
        _pTrans.setName(_child.getName());
        _pTrans.putAllUserData(_child.getAllUserData());
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this transform element.
     */
    @Override
    public GeomPoint getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public Point copyToReal() {
        return _pTrans.copy();
    }

    /**
     * Recycles a <code>GeomPointTrans</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(GeomPointTrans instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Return an immutable version of this point.
     *
     * @return An immutable version of this plane.
     */
    @Override
    public Point immutable() {
        return copyToReal();
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns 3.
     */
    @Override
    public int getPhyDimension() {
        return 3;
    }

    /**
     * Returns the value of the Parameter in this point as a <code>double</code>, stated
     * in this point's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; dimension())</code>
     */
    @Override
    public double getValue(int i) {
        return _pTrans.getValue(i);
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * the specified unit.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; dimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Length> unit) {
        return _pTrans.getValue(i, requireNonNull(unit));
    }

    /**
     * Returns the square of the Euclidean norm, magnitude, or length value of the vector
     * from the origin to this point (the dot product of the origin-to-this-point vector
     * and itself). This is slightly faster than calling <code>normValue</code> if the
     * squared value is all that is needed.
     *
     * @return <code>this.normSq().getValue()</code>.
     * @see #normValue() 
     */
    @Override
    public double normSqValue() {
        return _pTrans.normSqValue();
    }
    
    /**
     * Returns the sum of this point with the one specified. The unit of the output point
     * will be the units of this point.
     *
     * @param that the point to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point plus(GeomPoint that) {
        return _pTrans.plus(that);
    }

    /**
     * Adds the specified parameter to each component of this point. The unit of the
     * output point will be the units of this point.
     *
     * @param that the parameter to be added to each component of this point. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    @Override
    public Point plus(Parameter<Length> that) {
        return _pTrans.plus(that);
    }

    /**
     * Returns the difference between this point and the one specified. The unit of the
     * output point will be the units of this point.
     *
     * @param that the point to be subtracted from this point. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point minus(GeomPoint that) {
        return _pTrans.minus(that);
    }

    /**
     * Subtracts the specified parameter from each component of this point. The unit of
     * the output point will be the units of this point.
     *
     * @param that the parameter to be subtracted from each component of this point. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Point minus(Parameter<Length> that) {
        return _pTrans.minus(that);
    }

    /**
     * Returns the negation of this point (all the values of each dimension negated).
     *
     * @return <code>-this</code>
     */
    @Override
    public Point opposite() {
        return _pTrans.opposite();
    }

    /**
     * Returns the product of this point with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Point times(double k) {
        return _pTrans.times(k);
    }

    /**
     * Return <code>true</code> if this point contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     */
    @Override
    public boolean isValid() {
        return _pTrans.isValid();
    }

    /**
     * Returns a copy of this GeomPointTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public GeomPointTrans copy() {
        return copyOf(this);
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this point are stated in.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent of this GeomPointTrans object that has a child point in the
     * specified units.
     * <p>
     * WARNING: If the unit changes, then the returned transform element DOES NOT refer
     * back to the original point (the link with the original point is broken).
     * </p>
     *
     * @param unit the length unit of the point to be returned. May not be null.
     * @return an equivalent to this point but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public GeomPointTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        GeomPoint newP = _child.to(unit);
        GeomPointTrans result = GeomPointTrans.newInstance(newP, _TM);

        return result;
    }

    /**
     * Returns a Vector3D representation of this transformed point if possible.
     *
     * @return A Vector3D that is equivalent to this transformed point
     * @throws DimensionException if this point has any number of dimensions other than 3.
     */
    @Override
    public Vector3D<Length> toVector3D() {
        return _pTrans.toVector3D();
    }

    /**
     * Returns the values stored in this transformed point, stated in this point's
     * {@link #getUnit unit}, as a Float64Vector.
     *
     * @return A Float64Vector containing the values stored in this point in the current
     *         units.
     */
    @Override
    public Float64Vector toFloat64Vector() {
        return _pTrans.toFloat64Vector();
    }

    /**
     * Return the equivalent of this point converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the point to return. MUST equal 3.
     * @return The equivalent of this point converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public GeomPointTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Compares this GeomPointTrans against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        GeomPointTrans that = (GeomPointTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<GeomPointTrans> XML = new XMLFormat<GeomPointTrans>(GeomPointTrans.class) {

        @Override
        public GeomPointTrans newInstance(Class<GeomPointTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, GeomPointTrans obj) throws XMLStreamException {
            GeomPoint.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            GeomPoint child = xml.getNext();
            obj._child = child;

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);

            obj._pTrans = obj._TM.transform(obj._child);
            obj._pTrans.setName(obj._child.getName());
            obj._pTrans.putAllUserData(obj._child.getAllUserData());
        }

        @Override
        public void write(GeomPointTrans obj, OutputElement xml) throws XMLStreamException {
            GeomPoint.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private GeomPointTrans() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<GeomPointTrans> FACTORY = new ObjectFactory<GeomPointTrans>() {
        @Override
        protected GeomPointTrans create() {
            return new GeomPointTrans();
        }

        @Override
        protected void cleanup(GeomPointTrans obj) {
            obj.reset();
            obj._TM = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
            obj._pTrans = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static GeomPointTrans copyOf(GeomPointTrans original) {
        GeomPointTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        obj._pTrans = original._pTrans.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

    /**
     * Tests the methods in this class.
     *
     * @param args Command-line arguments (not used).
     */
    public static void main(String args[]) {
        System.out.println("Testing GeomPointTrans:  test = result [correct result]");

        Parameter<Angle> psi = Parameter.valueOf(60, javax.measure.unit.NonSI.DEGREE_ANGLE);
        Parameter<Angle> theta = Parameter.ZERO_ANGLE;
        Parameter<Angle> phi = Parameter.valueOf(20, javax.measure.unit.NonSI.DEGREE_ANGLE);
        DCMatrix dcm = DCMatrix.getEulerTM(psi, theta, phi);
        GTransform T1 = GTransform.valueOf(dcm);
        T1 = T1.applyScale(2.5);
        Point p1 = Point.valueOf(1, 1, 1, SI.METER);

        GeomPointTrans p1T = GeomPointTrans.newInstance(p1, T1);

        System.out.println("\npsi = " + psi + ", theta = " + theta + ", phi = " + phi);
        System.out.println("\np1  = " + p1);
        System.out.println("  T1  = \n" + T1);
        System.out.println("  p1T = " + p1T + " [-0.0439988715583743 m, 2.9121541062864 m, 3.20428191027894 m]");
        System.out.println("  p1T.norm() = " + p1T.norm() + " [4.33012701892219 m]");

        GTransform T2 = GTransform.newTranslation(Vector.valueOf(SI.METER, 2, -1, -3));
        GeomPointTrans p2T = GeomPointTrans.newInstance(p1T, T2);
        System.out.println("\ntranslate p1T by: 2, -1, -3 m");
        System.out.println("  p2T = " + p2T + " [1.95600112844163 m, 1.9121541062864 m, 0.20428191027894 m]");
        System.out.println("  p2T.norm() = " + p2T.norm() + " [2.74299195031995 m]");

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new GeomXMLBinding();

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(p1T, "GeomPointTrans", GeomPointTrans.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
