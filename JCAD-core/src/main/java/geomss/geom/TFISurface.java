/**
 * TFISurface -- A transfinite interpolation surface defined by four boundary curves.
 *
 * Copyright (C) 2013-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.BasicNurbsSurface;
import geomss.geom.nurbs.NurbsSurface;
import geomss.geom.nurbs.SurfaceFactory;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * Represents a bi-linearly blended transfinite interpolation (TFI) or Coons patch surface
 * defined from four boundary curves. The boundary curves must meet at each of the 4
 * corners of the patch. It is assumed that all the input curves are oriented in an
 * appropriate direction and order to create the desired TFI surface.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 17, 2013
 * @version October 29, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public class TFISurface extends AbstractSurface<TFISurface> {

    //  The four boundary curves
    private Curve s0crv, s1crv;
    private Curve t0crv, t1crv;

    /**
     * Reference to a change listener for this object's child curves.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Return a TFISurface made up of the specified boundary {@link Curve} objects. It is
     * assumed that the boundary curves are ordered properly and that the end points are
     * coincident. No check for this is made.
     *
     * @param s0 The curve that serves as the s=0 boundary of this surface. May not be
     *           null.
     * @param t0 The curve that serves as the t=0 boundary of this surface. May not be
     *           null.
     * @param s1 The curve that serves as the s=1 boundary of this surface. May not be
     *           null.
     * @param t1 The curve that serves as the t=1 boundary of this surface. May not be
     *           null.
     * @return The TFISurface with the specified boundary curves.
     */
    public static TFISurface newInstance(Curve s0, Curve t0, Curve s1, Curve t1) {
        return newInstance(null, s0, t0, s1, t1);
    }

    /**
     * Return a TFISurface made up of the specified boundary {@link Curve} objects. It is
     * assumed that the boundary curves are ordered properly and that the end points are
     * coincident. No check for this is made.
     *
     * @param name The name to be assigned to this surface (may be <code>null</code>).
     * @param s0   The curve that serves as the s=0 boundary of this surface. May not be
     *             null.
     * @param t0   The curve that serves as the t=0 boundary of this surface. May not be
     *             null.
     * @param s1   The curve that serves as the s=1 boundary of this surface. May not be
     *             null.
     * @param t1   The curve that serves as the t=1 boundary of this surface. May not be
     *             null.
     * @return The TFISurface with the specified boundary curves.
     */
    public static TFISurface newInstance(String name, Curve s0, Curve t0, Curve s1, Curve t1) {

        //  Make sure that each input curve is the same dimension.
        int numDims = GeomUtil.maxPhyDimension(s0, t0, s1, t1);
        s0 = s0.toDimension(numDims);
        s1 = s1.toDimension(numDims);
        t0 = t0.toDimension(numDims);
        t1 = t1.toDimension(numDims);

        //  Convert all the curves to the same units.
        Unit<Length> unit = s0.getUnit();
        s1 = s1.to(unit);
        t0 = t0.to(unit);
        t1 = t1.to(unit);

        TFISurface o = FACTORY.object();
        o.setName(name);
        o.s0crv = s0;
        o.s1crv = s1;
        o.t0crv = t0;
        o.t1crv = t1;

        if (!(s0 instanceof Immutable))
            s0.addChangeListener(o._childChangeListener);
        if (!(s1 instanceof Immutable))
            s1.addChangeListener(o._childChangeListener);
        if (!(t0 instanceof Immutable))
            t0.addChangeListener(o._childChangeListener);
        if (!(t1 instanceof Immutable))
            t1.addChangeListener(o._childChangeListener);

        return o;
    }

    /**
     * Returns the number of physical dimensions of the geometry element. This
     * implementation always returns the physical dimension of the underlying
     * {@link Curve} objects.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return s0crv.getPhyDimension();
    }

    /**
     * Return the equivalent of this surface converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the surface to return.
     * @return This surface converted to the new dimensions.
     */
    @Override
    public TFISurface toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        //  Convert the curves.
        Curve ns0crv = s0crv.toDimension(newDim);
        Curve ns1crv = s1crv.toDimension(newDim);
        Curve nt0crv = t0crv.toDimension(newDim);
        Curve nt1crv = t1crv.toDimension(newDim);

        TFISurface srf = TFISurface.newInstance(getName(), ns0crv, ns1crv, nt0crv, nt1crv);
        copyState(srf);

        return srf;
    }

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 4 for the four boundary curves.
     *
     * @return The number of child-elements that make up this geometry element.
     */
    @Override
    public int size() {
        return 4;
    }

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     *          inclusive).
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(double s, double t) {
        validateParameter(s, t);
        StackContext.enter();
        try {
            //  Reference:  http://en.wikipedia.org/wiki/Coons_surface
            //  Lt = b0(t)*p(s,0) + b1(t)*p(s,1)
            //  Ls = c0(s)*p(0,t) + c1(s)*p(1,t)
            //  B = p00*c0(s)*b0(t) + p01*c1(s)*b0(t) + p10*c0(s)*b1(t) + p11*b1(t)*c1(s)
            //  p = Lt + Ls - B

            double b0t = 1 - t;
            double b1t = t;
            double c0s = 1 - s;
            double c1s = s;
            Point ps0 = t0crv.getRealPoint(s);
            Point ps1 = t1crv.getRealPoint(s);
            Point p0t = s0crv.getRealPoint(t);
            Point p1t = s1crv.getRealPoint(t);

            Point Lt = ps0.times(b0t).plus(ps1.times(b1t));
            Point Ls = p0t.times(c0s).plus(p1t.times(c1s));
            Point p00 = t0crv.getRealPoint(0);
            Point p01 = t0crv.getRealPoint(1);
            Point p10 = t1crv.getRealPoint(0);
            Point p11 = t1crv.getRealPoint(1);
            Point B = p00.times(c0s * b0t).plus(p01.times(c1s * b0t)).plus(p10.times(c0s * b1t)).plus(p11.times(c1s * b1t));

            //  Now sum up the pieces.
            Point p = Lt.plus(Ls).minus(B);
            return StackContext.outerCopy(p);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the u-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of s-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade, boolean scaled) {
        validateParameter(s, t);
        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        //  Check for easy cases.
        if (parNearStart(t, TOL_ST))
            return t0crv.getSDerivatives(s, grade);
        else if (parNearEnd(t, TOL_ST))
            return t1crv.getSDerivatives(s, grade);

        /*
         P = Lt + Ls - B
         Lt = b0(t)*P(s,0) + b1(t)*P(s,1)
         Ls = c0(s)*P(0,t) + c1(s)*P(1,t)
         B = P(0,0)*c0(s)*b0(t) + P(0,1)*c1(s)*b0(t) + P(1,0)*c0(s)*b1(t) + P(1,1)*b1(t)*c1(s)
         So:
         dP/ds = d(Lt)/ds + d(Ls)/ds - d(B)/ds
         Ps = dP/ds,  Lts = d(Lt)/ds, Lss = d(Ls)/ds, and Bs = d(B)/ds
         Ps = Lts + Lss - Bs
         Lts = b0(t)*Ps(s,0) + b1(t)*Ps(s,1)
         Lss = P(0,t)*d(c0(s))/ds + P(1,t)*d(c1(s))/ds
         c0ss = d(c0(s))/ds  and  c1ss = d(c1(s))/ds
         Lss = P(0,t)*c0ss + P(1,t)*c1ss
         Bs = P(0,0)*b0(t)*c0ss + P(0,1)*b0(t)*c1ss + P(1,0)*b1(t)*c0ss + P(1,1)*b1(t)*c1ss
            
         For higher derivatives, c0ss and c1ss = 0.
         */
        FastTable<Vector<Length>> ders = FastTable.newInstance();

        //  Get the actual point on the surface.
        Point Pst = getRealPoint(s, t);
        ders.add(Pst.toGeomVector());

        StackContext.enter();
        try {
            double b0t = 1 - t;
            double b1t = t;
            double c0ss = -1;
            double c1ss = 1;

            //  Determine derivatives in the S-direction at the t=0 and t=1 boundaries.
            //  These are:  Ps(s,0) and Ps(s,1)
            List<Vector<Length>> Pss0 = t0crv.getSDerivatives(s, grade);
            List<Vector<Length>> Pss1 = t1crv.getSDerivatives(s, grade);

            //  Determine the points on the s=0 and s=1 boundaries.
            //  These are:  P(0,t) and P(1,t)
            Point P0t = s0crv.getRealPoint(t);
            Point P1t = s1crv.getRealPoint(t);

            //  Get the corner points.
            Point P00 = t0crv.getRealPoint(0);
            Point P01 = t0crv.getRealPoint(1);
            Point P10 = t1crv.getRealPoint(0);
            Point P11 = t1crv.getRealPoint(1);

            //  Combine the different elements to get the total interpolated derivatives.
            for (int g = 1; g <= grade; ++g) {
                Vector<Length> Pss0v = Pss0.get(g);
                Vector<Length> Pss1v = Pss1.get(g);
                Vector<Length> Lts = Pss0v.times(b0t).plus(Pss1v.times(b1t));
                Point Lss = P0t.times(c0ss).plus(P1t.times(c1ss));
                Point Bs = P00.times(c0ss * b0t);
                Bs = Bs.plus(P01.times(c1ss * b0t));
                Bs = Bs.plus(P10.times(c0ss * b1t));
                Bs = Bs.plus(P11.times(c1ss * b1t));
                Vector<Length> d = Lts.plus(Lss.minus(Bs).toGeomVector());
                ders.add(StackContext.outerCopy(d));

                //  Prepare for next grade (derivative).
                c0ss = 0;
                c1ss = 0;
            }

        } finally {
            StackContext.exit();
        }

        //  Set the origin for the derivative vectors.
        for (int i = 1; i <= grade; ++i) {
            ders.get(i).setOrigin(Pst);
        }

        return ders;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s      1st parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param t      2nd parametric dimension distance to calculate derivative for (0.0 to
     *               1.0 inclusive).
     * @param grade  The maximum grade to calculate the v-derivatives for (1=1st
     *               derivative, 2=2nd derivative, etc)
     * @param scaled Pass <code>true</code> for properly scaled derivatives or
     *               <code>false</code> if the magnitude of the derivative vector is not
     *               required -- this sometimes results in faster calculation times.
     * @return A list of t-derivatives up to the specified grade of the surface at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade, boolean scaled) {
        validateParameter(s, t);
        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));

        //  Check for easy cases.
        if (parNearStart(s, TOL_ST))
            return s0crv.getSDerivatives(t, grade);
        else if (parNearEnd(s, TOL_ST))
            return s1crv.getSDerivatives(t, grade);

        /*
         P = Lt + Ls - B
         Lt = b0(t)*P(s,0) + b1(t)*P(s,1)
         Ls = c0(s)*P(0,t) + c1(s)*P(1,t)
         B = P(0,0)*c0(s)*b0(t) + P(0,1)*c1(s)*b0(t) + P(1,0)*c0(s)*b1(t) + P(1,1)*b1(t)*c1(s)
         So:
         dP/dt = d(Lt)/dt + d(Ls)/dt - d(B)/dt
         Pt = dP/dt,  Ltt = d(Lt)/dt, Lst = d(Ls)/dt, and Bt = d(B)/dt
         Pt = Ltt + Lst - Bt
         Ltt = d(b0(t))/dt*P(s,0) + d(b1(t))/dt*P(s,1)
         b0tt = d(b0(t))/dt   and  b1tt = d(b1(t))/dt
         Ltt = b0tt*P(s,0) + b1tt*P(s,1)
         Lst = Pt(0,t)*c0(s) + Pt(1,t)*c1(s)
         Bt = P(0,0)*b0tt*c0(s) + P(0,1)*b0tt*c1(s) + P(1,0)*b1tt*c0(s) + P(1,1)*b1tt*c1(s)
            
         For higher derivatives, b0tt and b1tt = 0.
         */
        FastTable<Vector<Length>> ders = FastTable.newInstance();

        //  Get the actual point on the surface.
        Point Pst = getRealPoint(s, t);
        ders.add(Pst.toGeomVector());

        StackContext.enter();
        try {
            double b0tt = -1;
            double b1tt = 1;
            double c0s = 1 - s;
            double c1s = s;

            //  Determine derivatives in the T-direction at the s=0 and s=1 boundaries.
            //  These are:  Pt(0,t) and Pt(1,t)
            List<Vector<Length>> Pt0t = s0crv.getSDerivatives(t, grade);
            List<Vector<Length>> Pt1t = s1crv.getSDerivatives(t, grade);

            //  Determine the points on the t=0 and t=1 boundaries.
            //  These are:  P(s,0) and P(s,1)
            Point Ps0 = t0crv.getRealPoint(s);
            Point Ps1 = t1crv.getRealPoint(s);

            //  Get the corner points.
            Point P00 = t0crv.getRealPoint(0);
            Point P01 = t0crv.getRealPoint(1);
            Point P10 = t1crv.getRealPoint(0);
            Point P11 = t1crv.getRealPoint(1);

            //  Combine the different elements to get the total interpolated derivatives.
            for (int g = 1; g <= grade; ++g) {
                Vector<Length> Pt0tv = Pt0t.get(g);
                Vector<Length> Pt1tv = Pt1t.get(g);
                Vector<Length> Ltt = Pt0tv.times(c0s).plus(Pt1tv.times(c1s));
                Point Lst = Ps0.times(b0tt).plus(Ps1.times(b1tt));
                Point Bt = P00.times(c0s * b0tt);
                Bt = Bt.plus(P01.times(c1s * b0tt));
                Bt = Bt.plus(P10.times(c0s * b1tt));
                Bt = Bt.plus(P11.times(c1s * b1tt));
                Vector<Length> d = Ltt.plus(Lst.minus(Bt).toGeomVector());
                ders.add(StackContext.outerCopy(d));

                //  Prepare for next grade (derivative).
                b0tt = 0;
                b1tt = 0;
            }

        } finally {
            StackContext.exit();
        }

        //  Set the origin for the derivative vectors.
        for (int i = 1; i <= grade; ++i) {
            ders.get(i).setOrigin(Pst);
        }

        return ders;
    }

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param s 1st parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate twist vector for (0.0 to
     *          1.0 inclusive).
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    @Override
    public Vector<Length> getTwistVector(double s, double t) {
        validateParameter(s, t);

        /*
         P = Lt + Ls - B
         Lt = b0(t)*P(s,0) + b1(t)*P(s,1)
         Ls = c0(s)*P(0,t) + c1(s)*P(1,t)
         B = P(0,0)*c0(s)*b0(t) + P(0,1)*c1(s)*b0(t) + P(1,0)*c0(s)*b1(t) + P(1,1)*b1(t)*c1(s)
            
         So:
         dP/ds = d(Lt)/ds + d(Ls)/ds - d(B)/ds
         Ps = dP/ds,  Lts = d(Lt)/ds, Lss = d(Ls)/ds, and Bs = d(B)/ds
         Ps = Lts + Lss - Bs
         Lts = b0(t)*Ps(s,0) + b1(t)*Ps(s,1)
         Lss = P(0,t)*d(c0(s))/ds + P(1,t)*d(c1(s))/ds
         c0ss = d(c0(s))/ds  and  c1ss = d(c1(s))/ds
         Lss = P(0,t)*c0ss + P(1,t)*c1ss
         Bs = P(0,0)*b0(t)*c0ss + P(0,1)*b0(t)*c1ss + P(1,0)*b1(t)*c0ss + P(1,1)*b1(t)*c1ss
            
         So:
         d^2P/(ds*dt) = d(dP/ds)/dt = d(Ps)/dt = dPsdt = d(Lts)/dt + d(Lss)/dt - d(Bs)/dt = Ltst + Lsst - Bst
         d(Lts)/dt = Ltst = d(b0(t))/dt*Ps(s,0) + d(b1(t))/dt*Ps(s,1)
         b0tt = d(b0(t))/dt  and b1tt = d(b1(t))/dt
         Ltst = Ps(s,0)*b0tt + Ps(s,1)*b1tt
         d(Lss)/dt = Lsst = Pt(0,t)*c0ss + Pt(1,t)*c1ss
         d(Bs)/dt = Bst = P(0,0)*b0tt*c0ss + P(0,1)*b0tt*c1ss + P(1,0)*b1tt*c0ss + P(1,1)*b1tt*c1ss
            
         */
        StackContext.enter();
        try {
            Unit<Length> unit = getUnit();
            double b0tt = -1;
            double b1tt = 1;
            double c0ss = -1;
            double c1ss = 1;

            //  Determine derivatives in the S-direction at the t=0 and t=1 boundaries.
            //  These are:  Ps(s,0) and Ps(s,1)
            Vector<Length> Pss0 = t0crv.getSDerivative(s, 1);
            Vector<Length> Pss1 = t1crv.getSDerivative(s, 1);
            Point nullPnt = Point.newInstance(getPhyDimension(), unit);
            Pss0.setOrigin(nullPnt);
            Pss1.setOrigin(nullPnt);

            //  Determine derivatives in the T-direction at the s=0 and s=1 boundaries.
            //  These are:  Pt(0,t) and Pt(1,t)
            Vector<Length> Pt0t = s0crv.getSDerivative(t, 1);
            Vector<Length> Pt1t = s1crv.getSDerivative(t, 1);
            Pt0t.setOrigin(nullPnt);
            Pt1t.setOrigin(nullPnt);

            //  Get the corner points.
            Point P00 = t0crv.getRealPoint(0);
            Point P01 = t0crv.getRealPoint(1);
            Point P10 = t1crv.getRealPoint(0);
            Point P11 = t1crv.getRealPoint(1);

            //  Get the actual point on the surface.
            Point Pst = getRealPoint(s, t);

            //  Ltst = Ps(s,0)*b0tt + Ps(s,1)*b1tt
            Vector<Length> Ltst = Pss0.times(b0tt).plus(Pss1.times(b1tt));

            //  Lsst = Pt(0,t)*c0ss + Pt(1,t)*c1ss
            Vector<Length> Lsst = Pt0t.times(c0ss).plus(Pt1t.times(c1ss));

            //  Bst = P(0,0)*b0tt*c0ss + P(0,1)*b0tt*c1ss + P(1,0)*b1tt*c0ss + P(1,1)*b1tt*c1ss
            Point Bst = P00.times(b0tt * c0ss);
            Bst = Bst.plus(P01.times(b0tt * c1ss));
            Bst = Bst.plus(P10.times(b1tt * c0ss));
            Bst = Bst.plus(P11.times(b1tt * c1ss));

            //  dPsdt = Ltst + Lsst - Bst
            Vector<Length> dPsdt = Ltst.plus(Lsst).minus(Bst.toGeomVector());
            dPsdt.setOrigin(Pst);

            return StackContext.outerCopy(dPsdt);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new surface that is identical to this one, but with the S-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         S-parameterization reversed.
     * @see #reverseT
     */
    @Override
    public TFISurface reverseS() {
        Curve s0 = s0crv.reverse();
        Curve s1 = s1crv.reverse();
        Curve t0 = t1crv;
        Curve t1 = t0crv;

        TFISurface srf = TFISurface.newInstance(s0, s1, t0, t1);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return a new surface that is identical to this one, but with the T-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     *         T-parameterization reversed.
     * @see #reverseS
     */
    @Override
    public TFISurface reverseT() {
        Curve s0 = s1crv;
        Curve s1 = s0crv;
        Curve t0 = t0crv.reverse();
        Curve t1 = t1crv.reverse();

        TFISurface srf = TFISurface.newInstance(s0, s1, t0, t1);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return a new surface that is identical to this one but with the transpose of the
     * parameterization of this surface. The S and T directions will be swapped.
     *
     * @return A new surface that is identical to this one but with the transpose of the
     *         parameterization of this surface.
     * @see #reverseT
     * @see #reverseS
     */
    @Override
    public TFISurface transpose() {
        Curve s0 = t0crv;
        Curve s1 = t1crv;
        Curve t0 = s0crv;
        Curve t1 = s1crv;

        TFISurface srf = TFISurface.newInstance(s0, s1, t0, t1);
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Split this {@link SubrangeSurface} at the specified parametric S-position returning
     * a list containing two new surfaces (a lower surface with smaller S-parametric
     * positions than "s" and an upper surface with larger S-parametric positions).
     *
     * @param s The S-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<TFISurface> splitAtS(double s) {
        validateParameter(s, 0);
        if (parNearEnds(s, TOL_ST))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "surface"));

        //  Get the dividing curve from the NURBS representation.
        //  TODO:  Figure out how to do this direclty on a TFI.
        BasicNurbsSurface nsrf = createNurbsSurface();
        SubrangeCurve splitC = nsrf.getSCurve(s);

        //  Split the edge curves.
        GeomList<Curve> t0crvs = t0crv.splitAt(s);
        GeomList<Curve> t1crvs = t1crv.splitAt(s);

        //  Create the new surface pieces.
        TFISurface srfL = TFISurface.newInstance(s0crv, t0crvs.get(0), splitC, t1crvs.get(0));
        TFISurface srfU = TFISurface.newInstance(splitC, t0crvs.get(1), s1crv, t1crvs.get(1));

        //  Create the output list.
        GeomList<TFISurface> output = GeomList.valueOf(srfL, srfU);
        return output;
    }

    /**
     * Split this {@link SubrangeSurface} at the specified parametric T-position returning
     * a list containing two new surfaces (a lower surface with smaller T-parametric
     * positions than "t" and an upper surface with larger T-parametric positions).
     *
     * @param t The T-parametric position where this surface should be split (must not be
     *          0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     *         surface.
     */
    @Override
    public GeomList<TFISurface> splitAtT(double t) {
        validateParameter(0, t);
        if (parNearEnds(t, TOL_ST))
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("canNotSplitAtEnds"), "surface"));

        //  Get the dividing curve from the NURBS representation.
        //  TODO:  Figure out how to do this direclty on a TFI.
        BasicNurbsSurface nsrf = createNurbsSurface();
        SubrangeCurve splitC = nsrf.getTCurve(t);
        BasicNurbsSurface.recycle(nsrf);

        //  Split the edge curves.
        GeomList<Curve> s0crvs = s0crv.splitAt(t);
        GeomList<Curve> s1crvs = s1crv.splitAt(t);

        //  Create the new surface pieces.
        TFISurface srfL = TFISurface.newInstance(s0crvs.get(0), t0crv, s1crvs.get(0), splitC);
        TFISurface srfU = TFISurface.newInstance(s0crvs.get(1), splitC, s1crvs.get(1), t1crv);

        //  Create the output list.
        GeomList<TFISurface> output = GeomList.valueOf(srfL, srfU);
        return output;
    }

    private BasicNurbsSurface createNurbsSurface() {
        //  Determine tolerance from size of bounding box.
        Point range = getBoundsMax().minus(getBoundsMin());
        Parameter<Length> tol = range.norm().divide(1000);
        BasicNurbsSurface srf = SurfaceFactory.createTFISurface(s0crv, t0crv, s1crv, t1crv, tol);
        return srf;
    }

    /**
     * Return the T=0 Boundary for this surface as a curve.
     *
     * @return The T=0 Boundary for this surface as a curve.
     */
    @Override
    public Curve getT0Curve() {
        return t0crv;
    }

    /**
     * Return the T=1 Boundary for this surface as a curve.
     *
     * @return The T=1 Boundary for this surface as a curve.
     */
    @Override
    public Curve getT1Curve() {
        return t1crv;
    }

    /**
     * Return the S=0 Boundary for this surface as a curve.
     *
     * @return The S=0 Boundary for this surface as a curve.
     */
    @Override
    public Curve getS0Curve() {
        return s0crv;
    }

    /**
     * Return the S=1 Boundary for this surface as a curve.
     *
     * @return The S=1 Boundary for this surface as a curve.
     */
    @Override
    public Curve getS1Curve() {
        return s1crv;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public TFISurface getTransformed(GTransform transform) {
        Curve s0 = (Curve)s0crv.getTransformed(requireNonNull(transform));
        Curve s1 = (Curve)s1crv.getTransformed(transform);
        Curve t0 = (Curve)t0crv.getTransformed(transform);
        Curve t1 = (Curve)t1crv.getTransformed(transform);

        return TFISurface.newInstance(s0, t0, s1, t1);
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no geometry.
     */
    @Override
    public Point getBoundsMin() {

        Point minPoint = s0crv.getBoundsMin();
        minPoint = minPoint.min(s1crv.getBoundsMin());
        minPoint = minPoint.min(t0crv.getBoundsMin());
        minPoint = minPoint.min(t1crv.getBoundsMin());

        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {

        Point maxPoint = s0crv.getBoundsMax();
        maxPoint = maxPoint.max(s1crv.getBoundsMax());
        maxPoint = maxPoint.max(t0crv.getBoundsMax());
        maxPoint = maxPoint.max(t1crv.getBoundsMax());

        return maxPoint;
    }

    /**
     * Returns the unit in which this surface is stated.
     *
     * @return The unit in which this surface is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return s0crv.getUnit();
    }

    /**
     * Returns the equivalent to this surface but stated in the specified unit.
     *
     * @param unit the length unit of the surface to be returned. May not be null.
     * @return an equivalent to this surface but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public TFISurface to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        //  Convert the curves.
        TFISurface srf = FACTORY.object();
        copyState(srf);

        srf.s0crv = s0crv.to(unit);
        srf.s1crv = s1crv.to(unit);
        srf.t0crv = t0crv.to(unit);
        srf.t1crv = t1crv.to(unit);

        return srf;
    }

    /**
     * Return a NURBS surface representation of this surface to within the specified
     * tolerance. If the curves making up this surface are NURBS curves, then the
     * resulting NURBS surface is an exact representation of this surface (and tol is
     * ignored). However, if any of the member curves are not NURBS curves, then tol is
     * used to make a NURBS approximation of that curve (and this surface).
     *
     * @param tol The greatest possible difference between this surface and the NURBS
     *            representation returned. May not be null.
     * @return A NURBS surface that represents this surface to within the specified
     *         tolerance.
     */
    @Override
    public NurbsSurface toNurbs(Parameter<Length> tol) {

        //  Go off and create a skinned NURBS surface.
        BasicNurbsSurface srf = SurfaceFactory.createTFISurface(s0crv, t0crv, s1crv, t1crv, requireNonNull(tol));
        copyState(srf); //  Copy over the super-class state for this surface to the new one.

        return srf;
    }

    /**
     * Return <code>true</code> if this TFISurface contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the member
     * curves are not valid.
     *
     * @return true if this TFISurface contains valid and finite numerical components.
     */
    @Override
    public boolean isValid() {
        return s0crv.isValid() && s1crv.isValid() && t0crv.isValid() && t1crv.isValid();
    }

    /**
     * Return <code>true</code> if this surface is degenerate (i.e.: has area less than
     * the specified tolerance squared).
     *
     * @param tol The tolerance for determining if this surface is degenerate. May not be
     *            null.
     * @return true if this surface is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        requireNonNull(tol);
        //  A TFI is degenerate if all of it's bounding curves are degenerate.
        return s0crv.isDegenerate(tol) && s1crv.isDegenerate(tol)
                && t0crv.isDegenerate(tol) && t1crv.isDegenerate(tol);
    }

    /**
     * Compares the specified object with this <code>TIFSurface</code> for equality.
     * Returns true if and only if both surfaces are of the same type and both contain the
     * same boundary curves in the same order.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this surface is identical to that surface;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        TFISurface that = (TFISurface)obj;
        return this.s0crv.equals(that.s0crv)
                && this.s1crv.equals(that.s1crv)
                && this.t0crv.equals(that.t0crv)
                && this.t1crv.equals(that.t1crv)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>TFISurface</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(s0crv, s1crv, t0crv, t1crv);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public TFISurface copyToReal() {
        TFISurface newSrf = FACTORY.object();
        copyState(newSrf);

        newSrf.s0crv = s0crv.copyToReal();
        newSrf.s1crv = s1crv.copyToReal();
        newSrf.t0crv = t0crv.copyToReal();
        newSrf.t1crv = t1crv.copyToReal();

        return newSrf;
    }

    /**
     * Returns a copy of this <code>TFISurface</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public TFISurface copy() {
        return copyOf(this);
    }

    /**
     * Returns the text representation of this geometry element.
     *
     * @return The text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        String className = this.getClass().getName();
        tmp.append(className.substring(className.lastIndexOf(".") + 1));
        tmp.append(": {\n");
        tmp.append(s0crv.toText());
        tmp.append(",\n");
        tmp.append(s1crv.toText());
        tmp.append(",\n");
        tmp.append(t0crv.toText());
        tmp.append(",\n");
        tmp.append(t1crv.toText());
        tmp.append("\n");

        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<TFISurface> XML = new XMLFormat<TFISurface>(TFISurface.class) {

        @Override
        public TFISurface newInstance(Class<TFISurface> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            TFISurface obj = FACTORY.object();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, TFISurface obj) throws XMLStreamException {
            AbstractSurface.XML.read(xml, obj);     // Call parent read.

            obj.s0crv = xml.get("S0");
            if (isNull(obj.s0crv))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("crvMissingXML"), "S0"));
            obj.s1crv = xml.get("S1");
            if (isNull(obj.s1crv))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("crvMissingXML"), "S1"));
            obj.t0crv = xml.get("T0");
            if (isNull(obj.t0crv))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("crvMissingXML"), "T0"));
            obj.t1crv = xml.get("T1");
            if (isNull(obj.t1crv))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("crvMissingXML"), "T1"));

            if (!(obj.s0crv instanceof Immutable))
                obj.s0crv.addChangeListener(obj._childChangeListener);
            if (!(obj.s1crv instanceof Immutable))
                obj.s1crv.addChangeListener(obj._childChangeListener);
            if (!(obj.t0crv instanceof Immutable))
                obj.t0crv.addChangeListener(obj._childChangeListener);
            if (!(obj.t1crv instanceof Immutable))
                obj.t1crv.addChangeListener(obj._childChangeListener);

        }

        @Override
        public void write(TFISurface obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractSurface.XML.write(obj, xml);    // Call parent write.

            xml.add(obj.s0crv, "S0");
            xml.add(obj.s1crv, "S1");
            xml.add(obj.t0crv, "T0");
            xml.add(obj.t1crv, "T1");
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected TFISurface() { }

    private static final ObjectFactory<TFISurface> FACTORY = new ObjectFactory<TFISurface>() {
        @Override
        protected TFISurface create() {
            return new TFISurface();
        }

        @Override
        protected void cleanup(TFISurface obj) {
            obj.reset();
            if (!(obj.s0crv instanceof Immutable))
                obj.s0crv.removeChangeListener(obj._childChangeListener);
            if (!(obj.s1crv instanceof Immutable))
                obj.s1crv.removeChangeListener(obj._childChangeListener);
            if (!(obj.t0crv instanceof Immutable))
                obj.t0crv.removeChangeListener(obj._childChangeListener);
            if (!(obj.t1crv instanceof Immutable))
                obj.t1crv.removeChangeListener(obj._childChangeListener);
            obj.s0crv = null;
            obj.s1crv = null;
            obj.t0crv = null;
            obj.t1crv = null;
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(TFISurface instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static TFISurface copyOf(TFISurface original) {
        TFISurface obj = FACTORY.object();
        obj.s0crv = original.s0crv.copy();
        obj.s1crv = original.s1crv.copy();
        obj.t0crv = original.t0crv.copy();
        obj.t1crv = original.t1crv.copy();

        if (!(obj.s0crv instanceof Immutable))
            obj.s0crv.addChangeListener(obj._childChangeListener);
        if (!(obj.s1crv instanceof Immutable))
            obj.s1crv.addChangeListener(obj._childChangeListener);
        if (!(obj.t0crv instanceof Immutable))
            obj.t0crv.addChangeListener(obj._childChangeListener);
        if (!(obj.t1crv instanceof Immutable))
            obj.t1crv.addChangeListener(obj._childChangeListener);

        original.copyState(obj);
        return obj;
    }
}
