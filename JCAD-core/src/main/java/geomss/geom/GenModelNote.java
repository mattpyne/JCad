/*
 *   GenModelNote  -- Partial implementation of a note that is displayed at a fixed size in model space.
 *
 *   Copyright (C) 2014-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.DCMatrix;
import jahuwaldt.js.param.Parameter;
import java.awt.Canvas;
import java.awt.Font;
import java.awt.FontMetrics;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javolution.context.StackContext;
import javolution.text.Text;
import javolution.text.TextBuilder;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * Partial implementation of a textual note located at a point in nD space, and
 * represented at a fixed size and orientation in model space.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 10, 2014
 * @version November 24, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class GenModelNote extends AbstractNote<GenModelNote> implements Transformable<GenModelNote> {

    /**
     * Return an immutable version of this note.
     *
     * @return An immutable version of this note.
     */
    public abstract ModelNote immutable();

    /**
     * Return the vector indicating the horizontal axis direction for the text.
     *
     * @return The vector indicating the horizontal axis direction for the text.
     */
    public abstract GeomVector<Dimensionless> getXHat();

    /**
     * Return the vector indicating the vertical axis direction (or ascent direction) for
     * the text.
     *
     * @return The vector indicating the vertical axis direction (or ascent direction) for
     *         the text.
     */
    public abstract GeomVector<Dimensionless> getYHat();

    /**
     * Return the (at least 3D) vector indicating the normal axis direction for the text.
     * When looking down the normal axis, you are looking at the text face-on.
     *
     * @return The (at least 3D) vector indicating the normal axis direction for the text.
     */
    public Vector<Dimensionless> getNormal() {
        if (getPhyDimension() < 3)
            return Vector.valueOf(0, 0, 1);
        return getXHat().cross(getYHat()).toUnitVector();
    }

    /**
     * Return the height of the text box in model units.
     *
     * @return The height of the text box in model units.
     */
    public abstract Parameter<Length> getHeight();

    /**
     * Return the width of the text box in model units.
     *
     * @return The width of the text box in model units.
     */
    public Parameter<Length> getWidth() {
        Font font = getFont();
        font.deriveFont(font.getSize() * 4f);  //  Scale up the font a bit to make it sharper in the rendering.
        FontMetrics fm = new Canvas().getFontMetrics(font);
        int height = fm.getMaxAscent() + fm.getMaxDescent();
        int width = fm.stringWidth(getNote());

        // Need to make width/height powers of 2 because of Java3d texture
        // size restrictions
        int pow = 1;
        for (int i = 1; i < 32; ++i) {
            pow *= 2;
            if (width <= pow)
                break;
        }
        width = Math.max(width, pow);
        pow = 1;
        for (int i = 1; i < 32; ++i) {
            pow *= 2;
            if (height <= pow)
                break;
        }
        height = Math.max(height, pow);

        Parameter<Length> heightParam = getHeight();
        double modelHeight = heightParam.getValue();
        double scale = modelHeight / height;
        return Parameter.valueOf(width * scale, heightParam.getUnit());
    }

    /**
     * Return the descent of the text below the baseline (text location) in model units.
     * This is the offset, in the yhat direction, of the "lower-left" corner of the
     * bounding box from the text string baseline (which passes through the location
     * point).
     */
    private Parameter<Length> getDescent() {
        Font font = getFont();
        font.deriveFont(font.getSize() * 4f);  //  Scale up the font a bit to make it sharper in the rendering.
        FontMetrics fm = new Canvas().getFontMetrics(font);
        int ascent = fm.getMaxAscent();
        int descent = fm.getMaxDescent();
        int height = ascent + descent;

        // Need to make height a power of 2 because of Java3D texture
        // size restrictions
        int pow = 1;
        for (int i = 1; i < 32; ++i) {
            pow *= 2;
            if (height <= pow)
                break;
        }
        height = Math.max(height, pow);

        Parameter<Length> heightParam = getHeight();
        double modelHeight = heightParam.getValue();
        double scale = modelHeight / height;
        return Parameter.valueOf(descent * scale, heightParam.getUnit());
    }

    /**
     * Return a new note object identical to this one, but with the specified height in
     * model space.
     *
     * @param height The new height for the note in model space coordinates. May not be
     *               null.
     * @return A new note object identical to this one, but with the specified height in
     *         model space.
     */
    public abstract GenModelNote changeHeight(Parameter<Length> height);

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 1 as a GenModelNote is located at a single point in
     * model space.
     */
    @Override
    public int size() {
        return 1;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMin() {
        //  Compute opposite corners of the text box.

        StackContext.enter();
        try {
            //  Get the parameters of this note.
            Parameter<Length> height = getHeight();
            Parameter<Length> width = getWidth();
            Parameter<Length> descent = getDescent();
            GeomVector<Dimensionless> xhat = getXHat();
            GeomVector<Dimensionless> yhat = getYHat();

            //  Get the "lower-left" corner of the text box.
            Point p0 = getLocation().minus(Point.valueOf(yhat.times(descent)));

            //  Compute the "upper-right" corner of the text box.
            Point p1 = p0.plus(Point.valueOf(xhat.times(width).plus((GeomVector)yhat.times(height))));

            Point bounds = p0.min(p1);
            return StackContext.outerCopy(bounds);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMax() {
        //  Compute opposite corners of the text box.

        StackContext.enter();
        try {
            //  Get the parameters of this note.
            Parameter<Length> height = getHeight();
            Parameter<Length> width = getWidth();
            Parameter<Length> descent = getDescent();
            GeomVector<Dimensionless> xhat = getXHat();
            GeomVector<Dimensionless> yhat = getYHat();

            //  Get the "lower-left" corner of the text box.
            Point p0 = getLocation().minus(Point.valueOf(yhat.times(descent)));

            //  Compute the "upper-right" corner of the text box.
            Point p1 = p0.plus(Point.valueOf(xhat.times(width).plus((GeomVector)yhat.times(height))));

            Point bounds = p0.max(p1);
            return StackContext.outerCopy(bounds);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this geometry element.
     *
     * @param dim An index indicating the dimension to find the min/max point for
     *            (0=X,1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public Point getLimitPoint(int dim, boolean max, double tol) {
        //  Compute all 4 corners of the text box.

        StackContext.enter();
        try {
            //  Get the parameters of this note.
            Parameter<Length> height = getHeight();
            Parameter<Length> width = getWidth();
            Parameter<Length> descent = getDescent();
            GeomVector<Dimensionless> xhat = getXHat();
            GeomVector<Dimensionless> yhat = getYHat();

            //  Get the "lower-left" corner of the text box.
            Point p0 = getLocation().minus(Point.valueOf(yhat.times(descent)));

            //  Compute the "lower-right" corner of the text box.
            Vector projWidth = xhat.times(width);
            Point p1 = p0.plus(Point.valueOf(projWidth));

            //  Compute the "upper-right" corner of the text box.
            Vector projHeight = yhat.times(height);
            Point p2 = p0.plus(Point.valueOf(projWidth.plus(projHeight)));

            //  Compute the "upper-left" corner of the text box.
            Point p3 = p0.plus(Point.valueOf(projHeight));

            //  Compare the corner points to find the limiting point.
            Point limPnt = p0;
            Parameter<Length> lim = limPnt.get(dim);
            if ((max && p1.get(dim).isGreaterThan(lim)) || (!max && p1.get(dim).isLessThan(lim))) {
                limPnt = p1;
                lim = limPnt.get(dim);
            }
            if ((max && p2.get(dim).isGreaterThan(lim)) || (!max && p2.get(dim).isLessThan(lim))) {
                limPnt = p2;
                lim = limPnt.get(dim);
            }
            if ((max && p3.get(dim).isGreaterThan(lim)) || (!max && p3.get(dim).isLessThan(lim))) {
                limPnt = p3;
            }

            return StackContext.outerCopy(limPnt);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a direction cosine matrix containing the orientation of the note string.
     *
     * @return A direction cosine matrix containing the orientation of the note string.
     */
    public DCMatrix getOrientation() {
        Float64Vector col0 = getXHat().toFloat64Vector();
        Float64Vector col1 = getYHat().toFloat64Vector();
        Float64Vector col2 = getNormal().toFloat64Vector();
        DCMatrix M = DCMatrix.valueOf(col0, col1, col2).transpose();
        return M;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public ModelNoteTrans getTransformed(GTransform transform) {
        return ModelNoteTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the text representation of this geometry element that consists of the text
     * string, the orienting plane and location, and the text box height. For example:
     * <pre>
     *   {aNote = {"A text string.",{1,0,0},{0,1,0},{10 ft, -3 ft, 4.56 ft},0.2 ft}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {"A text string.",{0,0,1},{0,1,0},{10 ft, -3 ft, 4.56 ft},0.2 ft}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        tmp.append("\"");
        tmp.append(getNote());
        tmp.append("\",");
        tmp.append(getXHat().toFloat64Vector().toText());
        tmp.append(",");
        tmp.append(getYHat().toFloat64Vector().toText());
        tmp.append(",");
        tmp.append(getLocation().toText());
        tmp.append(",");
        tmp.append(getHeight().toText());
        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

}
