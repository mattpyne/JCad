/*
 *   GeomTriangle  -- Represents an abstract planar triangle in n-D space.
 *
 *   Copyright (C) 2015-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import static geomss.geom.AbstractGeomElement.RESOURCES;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import static java.util.Objects.nonNull;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.text.Text;
import javolution.text.TextBuilder;

/**
 * The interface and implementation in common to all triangles in n-dimensional space. A
 * triangle is represented by exactly three vertex points arranged in a counter-clockwise
 * direction (or winding) when viewed from the "outside" (the direction the normal vector
 * points).
 *
 * <p> Modified by: Joseph A. Huwaldt   </p>
 *
 * @author Joseph A. Huwaldt, Date: August 26, 2015
 * @version February 16, 2016
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class GeomTriangle extends AbstractGeomElement<GeomTriangle>
        implements PointGeometry<GeomTriangle>, Transformable<GeomTriangle> {

    /**
     * A flag indicating if this triangle is degenerate or not. A value of -1 means
     * degeneracy has not been assessed.  A value of 0 means the triangle is NOT
     * degenerate.  A value of 1 means that the triangle IS degenerate.
     */
    private int _degFlg = -1;
    
    
    /**
     * Return the first vertex in this triangle.
     *
     * @return The first vertex in this triangle.
     */
    public abstract GeomPoint getP1();

    /**
     * Return the second vertex in this triangle.
     *
     * @return The second vertex in this triangle.
     */
    public abstract GeomPoint getP2();

    /**
     * Return the third and last vertex in this triangle.
     *
     * @return The third and last vertex in this triangle.
     */
    public abstract GeomPoint getP3();

    /**
     * Return the surface unit normal vector for this triangle. If the triangle is
     * degenerate (zero area), then the normal vector will have zero length.
     *
     * @return The surface normal vector for this triangle.
     */
    public abstract GeomVector<Dimensionless> getNormal();

    /**
     * Return the surface area of one side of this triangle. The returned area is always
     * positive, but can be zero.
     *
     * @return The surface area of one side of this triangle.
     */
    public abstract Parameter<Area> getArea();

    /**
     * Return <code>true</code> if this triangle is degenerate (i.e.: has zero surface
     * area). A degenerate triangle is defined as a triangle that has any edge with length
     * less than the input tolerance.
     *
     * @param tol The tolerance for determining if this triangle is degenerate. A value of
     *            <code>null</code> would indicate that exactly zero area is required to
     *            be degenerate.
     * @return true if this triangle is degenerate.
     */
    public boolean isDegenerate(Parameter<Length> tol) {
        if (_degFlg < 0) {
            GeomPoint p1 = getP1();
            GeomPoint p2 = getP2();
            if (p1.isApproxEqual(p2, tol)) {
                _degFlg = 1;
                return true;
            }
            GeomPoint p3 = getP3();
            boolean output = p1.isApproxEqual(p3,tol) || p2.isApproxEqual(p3,tol);
            _degFlg = (output ? 1 : 0);
            return output;
        }
        return _degFlg > 0;
    }

    /**
     * Return a new triangle that is identical to this one, but with the order of the
     * points (and the surface normal direction) reversed.
     *
     * @return A new Triangle that is identical to this one, but with the order of the
     *         points reversed.
     */
    public abstract GeomTriangle reverse();

    /**
     * Return all three vertices of this triangle as an ordered list of points.
     *
     * @return A list containing the three vertices of this triangle in counter-clockwise
     *         order.
     */
    public PointString<? extends GeomPoint> getPoints() {
        GeomPoint p1 = getP1();
        GeomPoint p2 = getP2();
        GeomPoint p3 = getP3();
        PointString<? extends GeomPoint> str = PointString.valueOf(p1, p2, p3);
        return str;
    }

    /**
     * Return all three vertices of this triangle contained in the supplied array of
     * points. The input array must have at least 3 elements.
     *
     * @param points An existing array with at leat 3 elements that will be filled in with
     *               points from this triangle. May not be null.
     * @return The input array with the three vertices of this triangle, in counter-
     *         clockwise order, placed in the 1st 3 elements.
     */
    public GeomPoint[] getPoints(GeomPoint[] points) {
        points[0] = getP1();
        points[1] = getP2();
        points[2] = getP3();
        return points;
    }

    /**
     * Returns an new {@link GeomList} containing the ordered points in this triangle.
     *
     * @return A new GeomList containing the ordered points from this triangle.
     */
    public GeomList<GeomPoint> getAll() {
        GeomList<GeomPoint> list = GeomList.newInstance();
        list.add(getP1());
        list.add(getP2());
        list.add(getP3());
        return list;
    }

    /**
     * Returns the number of child-elements that make up this geometry element. This
 implementation always returns 3 as a GeomTriangle has three vertices.
     *
     * @return The number of points in this triangle (always returns 3).
     */
    @Override
    public int size() {
        return 3;
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of this geometry element.
     */
    @Override
    public int getPhyDimension() {
        return getP1().getPhyDimension();
    }

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 0 as a Triangle is not parametric.
     */
    @Override
    public int getParDimension() {
        return 0;
    }

    /**
     * Return the total number of points in this geometry element. This implementation
     * always returns 3 as a GeomTriangle has three vertices.
     *
     * @return Always returns 3.
     */
    @Override
    public int getNumberOfPoints() {
        return 3;
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this triangle.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance (in parameter space) to refine the min/max point
     *            position to.
     * @return The point found on this triangle that is the min or max in the specified
     *         coordinate direction.
     */
    @Override
    public GeomPoint getLimitPoint(int dim, boolean max, double tol) {
        //  Check the input dimension for consistancy.
        int thisDim = getPhyDimension();
        if (dim < 0 || dim >= thisDim)
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("inputDimOutOfRange"), "triangle"));

        //  Get the three points.
        PointString str = getPoints();

        //  Return the limit of the three points.
        return str.getLimitPoint(dim, max, tol);
    }

    /**
     * Return <code>true</code> if this GeomTriangle contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     *
     * @return true if this GeomTriangle contains valid and finite data.
     */
    @Override
    public boolean isValid() {
        return getP1().isValid() && getP2().isValid() && getP3().isValid();
    }

    /**
     * Returns the unit in which this GeomTriangle is stated.
     *
     * @return The unit in which this GeomTriangle is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return getP1().getUnit();
    }

    /**
     * Returns the text representation of this geometry element that consists of the name
     * followed by the three defining points. For example:
     * <pre>
     *   {aTri = {{0.0 m, 0.0 m},{1.0 m, 0.0 m},{0.5 m, 1.0 m}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {{0.0 m, 0.0 m},{1.0 m, 0.0 m},{0.5 m, 1.0 m}}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }

        tmp.append(getP1().toText());
        tmp.append(",");
        tmp.append(getP2().toText());
        tmp.append(",");
        tmp.append(getP3().toText());

        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Resets the internal state of this object to its default values. Subclasses that
     * override this method must call <code>super.reset();</code> to ensure that the state
     * is reset properly.
     */
    @Override
    public void reset() {
        super.reset();
        this._degFlg = -1;
    }
    
}
