/*
 *   GeomPoint  -- Holds the floating point coordinates of a point in nD space.
 *
 *   Copyright (C) 2002-2018, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.param.ParameterVector;
import jahuwaldt.js.param.Vector3D;
import jahuwaldt.util.XYPoint;
import java.text.MessageFormat;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.lang.MathLib;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A container that holds the coordinates of a point in n-dimensional space.
 * 
 * <p> * Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: December 11, 1999
 * @version February 5, 2018
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class GeomPoint extends AbstractGeomElement<GeomPoint>
        implements PointGeometry<GeomPoint>, Transformable<GeomPoint>, XYPoint {

    /**
     * Constant used to identify the X or 1st coordinate.
     */
    public static final int X = 0;

    /**
     * Constant used to identify the Y or 2nd coordinate.
     */
    public static final int Y = 1;

    /**
     * Constant used to identify the Z or 3rd coordinate.
     */
    public static final int Z = 2;

    /**
     * Constant used to identify the W or 4th coordinate.
     */
    public static final int W = 2;

    /**
     * Return an immutable version of this point.
     *
     * @return an immutable version of this point.
     */
    public abstract Point immutable();

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 0 as a GeomPoint is not made up of any other
     * elements.
     */
    @Override
    public int size() {
        return 0;
    }

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 0 as a GeomPoint is not parametric.
     */
    @Override
    public int getParDimension() {
        return 0;
    }

    /**
     * Returns the value of a Parameter from this point.
     *
     * @param i the dimension index.
     * @return the value of the parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException
     * <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public Parameter<Length> get(int i) {
        return Parameter.valueOf(getValue(i), getUnit());
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException
     * <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public abstract double getValue(int i);

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * the specified unit.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; dimension())</code>
     */
    public abstract double getValue(int i, Unit<Length> unit);

    /**
     * Return the X-coordinate of this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     * 
     * @return the value of the X-coordinate in the current units.
     */
    @Override
    public double getX() {
        return this.getValue(X);
    }
    
    /**
     * Return the Y-coordinate of this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     * 
     * @return the value of the Y-coordinate in the current units.
     */
    @Override
    public double getY() {
        return this.getValue(Y);
    }
    
    /**
     * Return the Z-coordinate of this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     * 
     * @return the value of the Z-coordinate in the current units.
     */
    public double getZ() {
        return this.getValue(Z);
    }
    
    /**
     * Returns the square of the Euclidean norm, magnitude, or length value of the vector
     * from the origin to this point (the dot product of the origin-to-this-point vector
     * and itself). This is slightly faster than calling <code>normValue</code> if the
     * squared value is all that is needed.
     *
     * @return <code>this.normSq().getValue()</code>.
     * @see #normValue() 
     */
    public abstract double normSqValue();

    /**
     * Returns the square of the Euclidean norm, magnitude, or length of the vector from
     * the origin to this point (the dot product of the origin-to-this-point vector and
     * itself). This is slightly faster than calling <code>normValue</code> if the
     * squared value is all that is needed.
     *
     * @return <code>this · this</code>.
     * @see #norm() 
     */
    public Parameter<Area> normSq() {
        return Parameter.valueOf(normSqValue(), getUnit().pow(2)).asType(Area.class);
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of the vector from the origin
     * to this point (square root of the dot product of the origin-to-this-point vector
     * and itself).
     *
     * @return <code>this.norm().doubleValue()</code>.
     * @see #normSqValue() 
     */
    public double normValue() {
        return MathLib.sqrt(normSqValue());
    }

    /**
     * Returns the Euclidian norm, magnitude, or length of the vector from the origin to
     * this point (square root of the dot product of the origin-to-this-point vector and
     * itself).
     *
     * @return <code>sqrt(this · this)</code>.
     * @see #normValue() 
     */
    public Parameter<Length> norm() {
        return Parameter.valueOf(normValue(), getUnit());
    }

    /**
     * Returns the sum of this point with the one specified. The unit of the output point
     * will be the units of this point.
     *
     * @param that the point to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    public abstract Point plus(GeomPoint that);

    /**
     * Adds the specified parameter to each component of this point. The unit of the
     * output point will be the units of this point.
     *
     * @param that the parameter to be added to each component of this point. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    public abstract Point plus(Parameter<Length> that);

    /**
     * Returns the difference between this point and the one specified. The unit of the
     * output point will be the units of this point.
     *
     * @param that the point to be subtracted from this point. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    public abstract Point minus(GeomPoint that);

    /**
     * Subtracts the specified parameter from each component of this point. The unit of
     * the output point will be the units of this point.
     *
     * @param that the parameter to be subtracted from each component of this point. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    public abstract Point minus(Parameter<Length> that);

    /**
     * Returns the negation of this point (all the values of each dimension negated).
     *
     * @return <code>-this</code>
     */
    public abstract Point opposite();

    /**
     * Returns the product of this point with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    public abstract Point times(double k);

    /**
     * Returns the product of this point with the specified dimensionless Parameter.
     *
     * @param k the dimensionless Parameter multiplier. May not be null.
     * @return <code>this · k</code>
     */
    public Point times(Parameter<Dimensionless> k) {
        return times(k.getValue(Dimensionless.UNIT));
    }

    /**
     * Returns this point with each element divided by the specified divisor.
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    public Point divide(double divisor) {
        return times(1.0 / divisor);
    }

    /**
     * Returns this point with each element divided by the specified dimensionless
     * Parameter.
     *
     * @param divisor the dimensionless Parameter divisor. May not be null.
     * @return <code>this / divisor</code>.
     */
    public Point divide(Parameter<Dimensionless> divisor) {
        return divide(divisor.getValue(Dimensionless.UNIT));
    }

    /**
     * Return the square of the Euclidian distance between this point and the one
     * specified. This is slightly faster than calling <code>distance</code> if the
     * squared value is all that you need.
     *
     * @param that The point to determine the distance squared from this point to. May not
     *             be null.
     * @return the distance squared from this point to the one specified.
     * @see #distance(geomss.geom.GeomPoint) 
     */
    public Parameter<Area> distanceSq(GeomPoint that) {
        return Parameter.valueOf(distanceSqValue(that), getUnit().pow(2)).asType(Area.class);
    }

    /**
     * Return the Euclidian distance squared between this point and the one specified as a
     * <code>double</code>. This is slightly faster than calling <code>distance</code> if
     * the squared value is all that you need.
     *
     * @param that The point to determine the distance squared from this point to. May not
     *             be null.
     * @return the distance squared from this point to the one specified.
     * @see #distanceValue(geomss.geom.GeomPoint) 
     */
    public double distanceSqValue(GeomPoint that) {
        StackContext.enter();
        try {
            GeomPoint dp = this.minus(requireNonNull(that));
            return dp.normSqValue();
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the Euclidian distance between this point and the one specified.
     *
     * @param that The point to determine the distance from this point to. May not be null.
     * @return the distance from this point to the one specified.
     * @see #distanceSq(geomss.geom.GeomPoint) 
     */
    public Parameter<Length> distance(GeomPoint that) {
        return Parameter.valueOf(distanceValue(that), getUnit());
    }

    /**
     * Return the Euclidian distance between this point and the one specified as a
     * <code>double</code>.
     *
     * @param that The point to determine the distance from this point to. May not be null.
     * @return the distance from this point to the one specified.
     * @see #distanceSqValue(geomss.geom.GeomPoint) 
     */
    public double distanceValue(GeomPoint that) {
        return MathLib.sqrt(distanceSqValue(that));
    }

    /**
     * Returns a point consisting of the minimum value in each dimension between this
     * point and the input point. This is used to find points that bound a geometry.
     *
     * @param that The point being compared with this one for minimum values in each
     *             dimension. May not be null.
     * @return A point consisting of the minimum value in each dimension between this
     *         point and the input point.
     */
    public Point min(GeomPoint that) {
        requireNonNull(that);
        StackContext.enter();
        try {
            Unit<Length> unit = getUnit();

            FastTable<Float64> valueList = FastTable.newInstance();
            int numDims = getPhyDimension();
            for (int i = 0; i < numDims; ++i) {
                double value = MathLib.min(this.getValue(i), that.getValue(i, unit));
                valueList.add(Float64.valueOf(value));
            }
            Float64Vector V = Float64Vector.valueOf(valueList);

            Point P = Point.valueOf(V, unit);
            return StackContext.outerCopy(P);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a point consisting of the maximum value in each dimension between this
     * point and the input point. This is used to find points that bound a geometry.
     *
     * @param that The point being compared with this one for maximum values in each
     *             dimension. May not be null.
     * @return A point consisting of the maximum value in each dimension between this
     *         point and the input point.
     */
    public Point max(GeomPoint that) {
        requireNonNull(that);
        StackContext.enter();
        try {
            Unit<Length> unit = getUnit();

            FastTable<Float64> valueList = FastTable.newInstance();
            int numDims = getPhyDimension();
            for (int i = 0; i < numDims; ++i) {
                double value = MathLib.max(this.getValue(i), that.getValue(i, unit));
                valueList.add(Float64.valueOf(value));
            }
            Float64Vector V = Float64Vector.valueOf(valueList);

            Point P = Point.valueOf(V, getUnit());
            return StackContext.outerCopy(P);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMin() {
        return this.immutable();
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMax() {
        return this.immutable();
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this geometry element. This implementation always returns
     * this point's coordinate.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public GeomPoint getLimitPoint(int dim, boolean max, double tol) {
        return this;
    }

    /**
     * Return the total number of points in this geometry element. This implementation
     * always returns 1.
     */
    @Override
    public int getNumberOfPoints() {
        return 1;
    }

    /**
     * Return <code>true</code> if this point contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     */
    @Override
    public boolean isValid() {
        int numDims = getPhyDimension();
        for (int i = 0; i < numDims; ++i) {
            double value = getValue(i);
            if (Double.isInfinite(value) || Double.isNaN(value))
                return false;
        }
        return true;
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     */
    @Override
    public abstract Point copyToReal();

    /**
     * Returns a copy of this {@link GeomPoint} instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public abstract GeomPoint copy();

    /**
     * Returns a Vector3D representation of this point if possible.
     *
     * @return A Vector3D that is equivalent to this point
     * @throws DimensionException if this point has any number of dimensions other than 3.
     */
    public Vector3D<Length> toVector3D() {
        if (getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "point", getPhyDimension()));
        return Vector3D.valueOf(this.toFloat64Vector(), this.getUnit());
    }

    /**
     * Returns a ParameterVector representation of this point.
     *
     * @return A ParameterVector that is equivalent to this point
     */
    public ParameterVector<Length> toParameterVector() {
        return ParameterVector.valueOf(this.toFloat64Vector(), this.getUnit());
    }

    /**
     * Returns a <code>GeomVector</code> representation of this point.
     *
     * @return A GeomVector that is equivalent to this point
     */
    public Vector<Length> toGeomVector() {
        return Vector.valueOf(this);
    }

    /**
     * Returns the values stored in this point, stated in the current
     * {@link #getUnit units}, as a Float64Vector.
     *
     * @return A Float64Vector containing the values stored in this point in the current
     *         units.
     */
    public abstract Float64Vector toFloat64Vector();

    /**
     * Returns the values stored in this point as a Java array, stated in the
     * current {@link #getUnit units}.
     *
     * @return A new array with the point values copied into it.
     */
    public double[] toArray() {
        return toArray(new double[this.getPhyDimension()]);
    }

    /**
     * Returns the values stored in this point, stated in the current
     * {@link #getUnit units} stored in the input Java array.
     *
     * @param array An existing array that has at least as many elements as the physical
     *              dimension of this point.
     * @return A reference to the input array after the point values have been copied over
     *         to it.
     * @see #getPhyDimension()
     */
    public double[] toArray(double[] array) {
        int numDims = this.getPhyDimension();
        for (int i = 0; i < numDims; ++i)
            array[i] = this.getValue(i);
        return array;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public GeomPointTrans getTransformed(GTransform transform) {
        return GeomPointTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the text representation of this geometry element that consists of the name
     * followed by the coordinate values. For example:
     * <pre>
     *   {aPoint = {10 ft, -3 ft, 4.56 ft}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {10 ft, -3 ft, 4.56 ft}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        final int dimension = this.getPhyDimension();
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        for (int i = 0; i < dimension; i++) {
            tmp.append(get(i));
            if (i != dimension - 1) {
                tmp.append(", ");
            }
        }
        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Compares this point against the specified point for approximate equality
     * (coordinate values approximately equal to this one to within the numerical roundoff
     * tolerance).
     *
     * @param obj The GeomPoint object to compare with.
     * @return <code>true</code> if this point is approximately identical to that point;
     *         <code>false</code> otherwise.
     */
    public boolean isApproxEqual(GeomPoint obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;

        int numDims = getPhyDimension();
        if (obj.getPhyDimension() != numDims)
            return false;

        for (int i = 0; i < numDims; ++i) {
            Parameter<Length> thisi = get(i);
            Parameter<Length> thati = obj.get(i);
            if (!thisi.isApproxEqual(thati))
                return false;
        }

        return true;
    }

    /**
     * Compares this point against the specified point for approximate equality
     * (coordinate values approximately equal to this one to within the numerical roundoff
     * tolerance).
     *
     * @param obj The GeomPoint object to compare with.
     * @param tol The amount use to define approximate equality. If <code>null</code> is
     *            passed, exact numerical equality will be required.
     * @return <code>true</code> if this point is approximately identical to that point;
     *         <code>false</code> otherwise.
     */
    public boolean isApproxEqual(GeomPoint obj, Parameter<Length> tol) {
        if (this == obj)
            return true;
        if (isNull(obj))
            return false;

        int numDims = getPhyDimension();
        if (obj.getPhyDimension() != numDims)
            return false;

        for (int i = 0; i < numDims; ++i) {
            Parameter<Length> thisi = get(i);
            Parameter<Length> thati = obj.get(i);
            if (!thisi.isApproxEqual(thati, tol))
                return false;
        }

        return true;
    }

}
