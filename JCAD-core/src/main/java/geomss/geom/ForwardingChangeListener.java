/*
 *   ForwardingChangeListener  -- A change listener that forwards the change on to an AbstractGeomElement.
 *
 *   Copyright (C) 2014-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * A ChangeListener that forwards the change event on to a target
 * AbstractGeomElement instance.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: January 2, 2014
 * @version August 28, 2015
 */
public class ForwardingChangeListener implements ChangeListener {

    private final AbstractGeomElement targetGeom;

    public ForwardingChangeListener(AbstractGeomElement target) {
        targetGeom = target;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        targetGeom.fireChangeEvent();
    }
}
