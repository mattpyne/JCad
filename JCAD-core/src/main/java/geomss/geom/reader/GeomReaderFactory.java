/**
 * GeomReaderFactory -- Factory that returns appropriate GeomReader objects.
 *
 * Copyright (C) 2000-2016, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Enumeration;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javolution.util.FastComparator;
import javolution.util.FastSet;
import javolution.util.FastTable;

/**
 * This class returns a specific {@link GeomReader} object that can read in the specified
 * file. This class implements a pluggable architecture. A new {@link GeomReader} class
 * can be added by simply creating a subclass of <code>GeomReader</code>, creating a
 * "GeomReader.properties" file that refers to it, and putting that properties file
 * somewhere in the Java search path. All "GeomReader.properties" files that are found are
 * merged together to create a global list of reader/handler mappings.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: April 14, 2000
 * @version September 9, 2016
 */
public final class GeomReaderFactory {

    /**
     * The resource bundle for this package.
    *
     */
    private static final ResourceBundle RESOURCES
            = ResourceBundle.getBundle("geomss.geom.reader.GeomReaderFactoryResources", java.util.Locale.getDefault());

    /**
     * All class loader resources with this name ("GeomReader.properties") are loaded as
     * .properties definitions and merged together to create a global list of reader
     * handler mappings.
     */
    private static final String MAPPING_RES_NAME = "GeomReader.properties";

    //  An array containing a reference to all the readers that have been found.
    private static final GeomReader[] _allReaders;

    //  Locate and load all the readers we can find.
    static {
        GeomReader[] temp = null;
        try {
            temp = loadResourceList(MAPPING_RES_NAME, getClassLoader());

        } catch (Exception e) {
            Logger.getLogger(GeomReaderFactory.class.getName()).log(Level.SEVERE,
                    MessageFormat.format(RESOURCES.getString("couldntLoadMappings"), MAPPING_RES_NAME), e);
        }

        _allReaders = temp;
    }

    // this class is not extendible
    private GeomReaderFactory() { }

    /**
     * Method that attempts to find an {@link GeomReader} object that might be able to
     * read the data in the specified file.
     *
     * @param theFile The file to find a reader for. May not be null.
     * @return A reader that is appropriate for the specified file, or <code>null</code>
     *         if the user cancels the multiple reader selection dialog.
     * @throws IOException if an appropriate reader for the file could not be found.
     */
    public static GeomReader getReader(File theFile) throws IOException {

        if (!theFile.exists())
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("missingFileErr"), theFile.getName()));

        //  Get the list of data readers that are available.
        GeomReader[] allReaders = getAllReaders();
        if (isNull(allReaders) || allReaders.length < 1)
            throw new IOException(RESOURCES.getString("noReadersErr"));

        GeomReader selectedReader;
        FastTable<GeomReader> list = FastTable.newInstance();

        try {
            try {

                //  Loop through all the available readers and see if any of them will work.
                for (GeomReader reader : allReaders) {

                    //  Can this reader read the specified input stream?
                    int canReadFile = reader.canReadData(theFile);

                    //  If the reader is certain it can read the data, use it.
                    if (canReadFile == GeomReader.YES) {
                        return reader;
                    }

                    //  Otherwise, build a list of maybes.
                    if (canReadFile == GeomReader.MAYBE)
                        list.add(reader);

                }

            } catch (Exception e) {
                e.printStackTrace();
                throw new IOException(RESOURCES.getString("detFileTypeErr"));
            }

            if (list.size() < 1) {
                throw new IOException(RESOURCES.getString("fileTypeErr"));
            }

            //  If there is only one reader in the list, try and use it.
            if (list.size() == 1)
                selectedReader = list.get(0);

            else {
                //  Ask the user to select which reader they want to try and use.
                GeomReader[] possibleValues = list.toArray(new GeomReader[list.size()]);
                selectedReader = (GeomReader)JOptionPane.showInputDialog(null,
                        MessageFormat.format(RESOURCES.getString("chooseFormatMsg"), theFile.getName()),
                        RESOURCES.getString("chooseFormatTitle"),
                        JOptionPane.INFORMATION_MESSAGE, null, possibleValues, possibleValues[0]);
            }

        } finally {
            //  Clean up before leaving.
            FastTable.recycle(list);
        }

        return selectedReader;
    }

    /**
     * Method that returns a list of all the {@link GeomReader} objects found by this
     * factory.
     *
     * @return An array of GeomReader objects (can be <code>null</code> if static
     *         initialization failed).
     */
    public static GeomReader[] getAllReaders() {
        return _allReaders;
    }

    /*
     * Loads a reader list that is a union of *all* resources named
     * 'resourceName' as seen by 'loader'. Null 'loader' is equivalent to the
     * application loader.
     */
    private static GeomReader[] loadResourceList(final String resourceName, ClassLoader loader) {
        if (isNull(loader))
            loader = ClassLoader.getSystemClassLoader();

        final FastSet<GeomReader> result = FastSet.newInstance();

        try {
            final Enumeration<URL> resources = loader.getResources(resourceName);

            if (nonNull(resources)) {
                // merge all mappings in 'resources':

                while (resources.hasMoreElements()) {
                    final URL url = resources.nextElement();
                    final Properties mapping;

                    try (InputStream urlIn = url.openStream()) {

                        mapping = new Properties();
                        mapping.load(urlIn);            // load in .properties format

                    } catch (IOException ioe) {
                        // ignore this resource and go to the next one
                        continue;
                    }

                    // load all readers specified in 'mapping':
                    for (Enumeration keys = mapping.propertyNames(); keys.hasMoreElements();) {
                        final String format = (String)keys.nextElement();
                        final String implClassName = mapping.getProperty(format);

                        result.add(loadResource(implClassName, loader));
                    }
                }
            }

        } catch (IOException ignore) {
            // ignore: an empty result will be returned
        }

        //  Convert result Set to an array.
        GeomReader[] resultArr = result.toArray(new GeomReader[result.size()]);

        //  Sort the array using the specified comparator.
        Arrays.sort(resultArr, FastComparator.DEFAULT);

        //  Clean up before leaving.
        FastSet.recycle(result);

        //  Output the sorted array.
        return resultArr;
    }

    /*
     * Loads and initializes a single resource for a given format name via a
     * given classloader. For simplicity, all errors are converted to
     * RuntimeExceptions.
     */
    private static GeomReader loadResource(final String className, final ClassLoader loader) {
        requireNonNull(className, "null input: className");
        requireNonNull(loader, "null input: loader");

        final Class cls;
        final Object reader;
        try {
            cls = Class.forName(className, true, loader);
            reader = cls.newInstance();

        } catch (Exception e) {
            throw new RuntimeException(MessageFormat.format(
                    RESOURCES.getString("couldntLoadClass"), className) + " " + e.getMessage());
        }

        if (!(reader instanceof GeomReader))
            throw new RuntimeException(
                    MessageFormat.format(RESOURCES.getString("notGeomReader"), cls.getName()));
        
        return (GeomReader)reader;
    }

    /**
     * This method decides on which class loader is to be used by all resource/class
     * loading in this class. At the very least you should use the current thread's
     * context loader. A better strategy would be to use techniques shown in
     * http://www.javaworld.com/javaworld/javaqa/2003-06/01-qa-0606-load.html
     */
    private static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

}   //  end of class

