/*
 *   GGPGeomReader  -- A class that can read a GGP formatted geometry file.
 *
 *   Copyright (C) 2000-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import jahuwaldt.js.util.TextTokenizer;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javolution.context.StackContext;
import javolution.text.Text;
import javolution.text.TypeFormat;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading vehicle geometry from a GGP formatted geometry file.
 * This is a geometry file format often used in conjunction with solution output from A502
 * (PANAIR) and A633 (TRANAIR). This class will only read in parameters from the GGP file
 * with the names "X", "Y", and "Z". All other parameters are ignored. This class also
 * assumes that all the parameters are contained in a single line and that the 1st format
 * is duplicated throughout the file.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 11, 2000
 * @version September 9, 2016
 */
public class GGPGeomReader extends AbstractGeomReader {

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("ggpDescription");
    
    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "ggp";
    
    // Define the comment characters that are possible.
    private static final String[] COMMENT_CHARACTERS = {"*", "$", "("};
    
    /**
     * A description of the format of the arrays in the GGP file.
     */
    private final FastTable<Integer> formatLst = FastTable.newInstance();

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file format is definitely recognized by this reader.
     *         GeomReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".ggp"))
            response = MAYBE;

        return response;
    }

    /**
     * Reads in a GGP formatted geometry file from the specified input file and returns a
     * {@link PointVehicle} object that contains the geometry from the GGP file.
     * <p>
     * A GGP file does not support multiple components, therefore, the vehicle returned
     * will always contain a single component (or none at all if there was no data in the
     * file).
     * </p>
     * <p>
     * WARNING: This file format is not unit aware. You must set the units to be used by
     * calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link PointVehicle} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         components in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public PointVehicle read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        PointVehicle vehicle = PointVehicle.newInstance(inputFile.getName());

        //  GGP files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the ASCII file.
        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile))) {
            reader.mark(8192);

            // Start by parsing the "format" line (must be 1st line if it exists).
            String aLine = readLine(reader);
            parseFormatString(aLine);
            reader.reset();

            // Read in the networks.
            PointComponent comp = readNetworks(reader);

            // Add the component to the vehicle.
            if (comp.size() != 0)
                vehicle.add(comp);

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return vehicle;
    }

    /**
     * This method always returns <code>false</code> as GGP files do not encode the units
     * that are being used. You must call <code>setFileUnits</code> to set the units being
     * used before reading from a file of this format.
     *
     * @return true if this reader (and its format) is unit aware.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

    /**
     * Parse the format line of the GGP file. The format line is always the 1st line and
     * must contain a FORTRAN style number format declaration such as (3G15.7) or
     * (F5.0,7F13.4). All that this reader cares about is how many columns there are and
     * how many characters are contained in each column. This method sets a class variable
     * "formatLst" such that each entry corresponds to a column and each entry records how
     * many characters are in each column.
     *
     * @param formatStr A string containing the format statement.
     * @throws IOException if unable to parse the format line.
     */
    private void parseFormatString(String formatStr) throws IOException {
        formatLst.clear();
        formatStr = formatStr.trim();

        if (formatStr.startsWith("(")) {
            try {

                // Search for commas that separate different formats
                int start = 1;
                int end = formatStr.indexOf(",", start);
                while (end != -1) {
                    String subStr = formatStr.substring(start, end);
                    parseSingleFormat(subStr);
                    start = end + 1;
                    end = formatStr.indexOf(",", start);
                }
                parseSingleFormat(formatStr.substring(start, formatStr.length() - 1));

            } catch (NumberFormatException e) {
                throw new IOException(RESOURCES.getString("ggpformatParseErr"));
            }

        } else {

            // Create the default format list if one isn't provided.
            for (int i = 0; i < 3; ++i) {
                formatLst.add(15);
            }
        }
    }

    /**
     * Parse a single FORTRAN style number format declaration such as "3G15.7" or "F5.0"
     * or "7F13.4". All that this reader cares about is how many columns there are and how
     * many characters are contained in each column. This method sets a class variable
     * "formatLst" such that each entry corresponds to a column and each entry records how
     * many characters are in each column.
     *
     * @param formatStr A string containing the format statement.
     * @throws NumberFormatException if unable to parse the format declaration.
     */
    private void parseSingleFormat(String formatStr) throws NumberFormatException {
        formatStr = formatStr.trim();

        // Convert string to a character array.
        char[] array = formatStr.toCharArray();

        // Extract the number of columns using this format.
        int pos1 = 0;
        int pos2 = 0;
        while (Character.isDigit(array[pos2])) {
            ++pos2;
        }

        int nCol;
        if (pos2 == pos1)
            nCol = 1;
        else
            nCol = Integer.parseInt(formatStr.substring(pos1, pos2));

        // Skip the format type (F,G, or E).
        pos1 = pos2 + 1;

        // Extract the number of characters per column.
        pos2 = pos1;
        while (Character.isDigit(array[pos2])) {
            ++pos2;
        }

        if (pos2 < pos1 + 1)
            throw new NumberFormatException();

        Integer nChar = Integer.valueOf(formatStr.substring(pos1, pos2));

        // Create list of formats, one for each column.
        for (int i = 0; i < nCol; ++i) {
            formatLst.add(nChar);
        }
    }

    /**
     * Read in all the networks in this geometry file.
     *
     */
    private PointComponent readNetworks(LineNumberReader reader) throws IOException {

        // Create a component object to contain the networks read in.
        PointComponent comp = PointComponent.newInstance();

        // Read until we get something that is not a comment.
        String aLine = readSkippingComments(reader);

        // Read in the network ID and optional name.
        String netID = parseNetID(aLine);

        // Parse the parameter name line.
        FastTable<Text> paramNames = parseParamNames(reader);

        // Determine which columns contain the X,Y,Z parameters.
        int[] columns = new int[3];
        columns[0] = findParam(paramNames, "X");
        columns[1] = findParam(paramNames, "Y");
        columns[2] = findParam(paramNames, "Z");
        if (columns[0] < 0 || columns[1] < 0 || columns[2] < 0) {
            throw new IOException(
                    MessageFormat.format(RESOURCES.getString("ggpMissingParameter"),
                            reader.getLineNumber()));
        }

        // Keep reading until we reach the end of the file.
        while (nonNull(netID)) {

            // Parse the optional network name.
            String netName = parseOptionalNetName(aLine);

            // Read in a single list of data points.
            PointString dataStr = readString(reader, columns);

            // Create a list of strings (lists of data points) and add the
            // 1st one to it.
            PointArray strList = PointArray.newInstance();
            strList.setName(netName);
            strList.add(dataStr);

            // Read in the remaining strings in this network.
            String oldNetID = netID;
            aLine = readSkippingComments(reader);
            netID = parseNetID(aLine);
            while (nonNull(netID) && netID.equals(oldNetID)) {

                // Read in the string.
                dataStr = readString(reader, columns);

                // Add it to the list of strings.
                strList.add(dataStr);

                aLine = readSkippingComments(reader);
                netID = parseNetID(aLine);
            }

            // Add the new network to the component.
            comp.add(strList);

        }

        return comp;
    }

    /**
     * Determines if this line is a comment line.
     *
     * @param aLine The line to be tested.
     * @return true if the line is a comment line, false if it is not. If aLine is null,
     *         false is returned.
     */
    private boolean isComment(String aLine) {
        boolean retVal = false;

        aLine = aLine.trim();
        int length = COMMENT_CHARACTERS.length;
        for (int i = 0; i < length; ++i) {
            if (aLine.startsWith(COMMENT_CHARACTERS[i]) && !aLine.startsWith("*EOD")
                    && !aLine.startsWith("*EOF")) {
                retVal = true;
                break;
            }
        }

        return retVal;
    }

    /**
     * Read in lines from the GGP file while skipping comment lines.
     *
     * @param reader The reader for the GGP file.
     * @return The 1st line that is not a comment line or null if the end of file is
     *         reached.
     */
    private String readSkippingComments(LineNumberReader reader) throws IOException {
        String aLine = readLine(reader);
        while (isComment(aLine)) {
            aLine = readLine(reader);
        }
        return aLine;
    }

    /**
     * Parse the network ID from the string name line. The network ID is a letter/number
     * combination that uniquely identifies the network that this string of points belongs
     * to. Immediately following the network ID is the string ID which this program
     * ignores.
     *
     * @param aLine The line containing the net/string ID.
     * @param The   network ID is returned.
     */
    private String parseNetID(String aLine) {

        aLine = aLine.trim();
        char[] array = aLine.toCharArray();
        int length = array.length;

        // Find end of letter part of ID.
        int pos = 0;
        while (pos < length) {
            if (Character.isDigit(array[pos]))
                break;
            ++pos;
        }

        // Find end of number part of ID.
        ++pos;
        while (pos < length) {
            if (!Character.isDigit(array[pos])) {
                ++pos;
                break;
            }
            ++pos;
        }

        // Extract the ID string.
        return aLine.substring(0, pos - 1);
    }

    /**
     * Parse the list of parameters.
     *
     * @param reader The reader for our GGP file.
     * @return A list of strings where each element is a parameter in this GGP file.
     */
    private FastTable<Text> parseParamNames(LineNumberReader reader) throws IOException {

        String aLine = readSkippingComments(reader);

        int nParams = formatLst.size();
        FastTable<Text> array = FastTable.newInstance();
        TextTokenizer tokenizer = TextTokenizer.valueOf(aLine, " ");

        // Extract one token (param name) for each element identified
        // in the format line.
        for (int i = 0; i < nParams; ++i) {
            if (!tokenizer.hasMoreTokens()) {
                aLine = readSkippingComments(reader);
                tokenizer.setText(aLine);
            }

            Text token = tokenizer.nextToken();
            array.add(token);
        }

        return array;
    }

    /**
     * Identify which parameter in the parameter string matches the target parameter.
     *
     * @param nameList The list of parameter names.
     * @param target   The target parameter that we are looking for.
     * @return The data column corresponding the the target parameter. If the parameter
     *         could not be found, -1 is returned.
     */
    private int findParam(List<Text> nameList, CharSequence target) {
        Text targetText = Text.valueOf(target);

        int length = nameList.size();
        int pos = 0;
        while (pos < length) {
            if (nameList.get(pos).equals(targetText))
                break;
            ++pos;
        }

        if (pos == length)
            pos = -1;

        return pos;
    }

    /**
     * Parse the optional network name from the string name line. The network name is
     * different from the network ID. The optional network name comes after the network ID
     * and string ID and is separated from them by at least one space.
     *
     * @param aLine The line containing the string & network name.
     * @return A string containing the optional network name or null if that name is not
     *         found.
     */
    private String parseOptionalNetName(String aLine) {
        aLine = aLine.trim();
        int pos = aLine.lastIndexOf(" ");

        String retVal = null;
        if (pos > 0)
            retVal = aLine.substring(pos);

        // Extract the ID string.
        return retVal;
    }

    /**
     * Read a string of points from the GGP file. A string of points consists of the
     * parameters read in, 1 point per line, until the *EOD record is encountered. This
     * version of this method does not require the string to be of any length. Data is
     * read in until the *EOD card is reached.
     *
     * @param reader  The reader for the GGP file.
     * @param columns An array containing the indices of the data columns (parameters) to
     *                be read in.
     * @return A list of PointString objects.
     *
     */
    private PointString readString(LineNumberReader reader, int[] columns)
            throws IOException {

        PointString string = PointString.newInstance();

        StackContext.enter();
        try {
            FastTable<Text> coordinates = FastTable.newInstance();
            int nParams = formatLst.size();

            // Read a line of data.
            String aLine = readSkippingComments(reader);

            // If *EOD, then the string is finished.
            String trimLine = aLine.trim();
            while (!trimLine.equals("*EOD") && !trimLine.equals("*EOF")) {
                TextTokenizer tokenizer = TextTokenizer.valueOf(trimLine, " ");
                if (tokenizer.countTokens() < nParams)
                    throw new IOException(
                            MessageFormat.format(RESOURCES.getString("ggpNumParameterErr"),
                                    reader.getLineNumber()));

                // Loop over the parameters in the file.
                int pos = 0;
                while (pos < nParams) {
                    Text token = tokenizer.nextToken();

                    // Check if this parameter is one of the columns requested.
                    for (int i = 0; i < columns.length; ++i) {
                        if (columns[i] == pos) {

                            // This one is requested, so store it.
                            coordinates.add(token);
                            break;
                        }
                    }
                    ++pos;
                }

                // Parse the stored coordinates.
                double xValue = TypeFormat.parseDouble(coordinates.get(0));
                double yValue = TypeFormat.parseDouble(coordinates.get(1));
                double zValue = TypeFormat.parseDouble(coordinates.get(2));
                coordinates.clear();

                //  Create and store a point.
                Point point = Point.valueOf(xValue, yValue, zValue, getFileUnits());
                string.add(StackContext.outerCopy(point));

                // Read the next line.
                aLine = readSkippingComments(reader);
                trimLine = aLine.trim();
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new IOException(
                    MessageFormat.format(RESOURCES.getString("parseErrMsg"),
                            DESCRIPTION, reader.getLineNumber()));

        } finally {
            StackContext.exit();
        }

        return string;
    }
}
