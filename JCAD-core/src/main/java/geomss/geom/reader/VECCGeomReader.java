/*
 *   VECCGeomReader  -- A class that can read and write a VECC MK5 formatted geometry file.
 *
 *   Copyright (C) 2009-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import static geomss.geom.reader.AbstractGeomReader.RESOURCES;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.text.Text;
import javolution.text.TypeFormat;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading and writing vehicle geometry from/to a VECC (Viscous
 * Effects on Complex Configurations) MK5 formatted geometry file. This class can also
 * read the related VECC GEO file.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 10, 2009
 * @version September 9, 2016
 */
public class VECCGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    private static final boolean DEBUG = false;

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("veccDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "mk5";

    //  Units strings in Text format.
    private static final Text IN_TEXT = Text.intern("in");
    private static final Text FT_TEXT = Text.intern("ft");
    private static final Text CM_TEXT = Text.intern("cm");
    private static final Text MM_TEXT = Text.intern("mm");
    private static final Text ME_TEXT = Text.intern("me");

    private boolean _isUnitAware = true;

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read paneled geometry from the specified
     * input file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file has the extension ".geo" or ".mk5".
     *         GeomReader.MAYBE if the file has the extension ".lib".
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".geo")) {
            response = YES;
            _isUnitAware = false;
        } else if (name.endsWith(".mk5")) {
            response = YES;
            _isUnitAware = true;
        } else if (name.endsWith(".lib")) {
            response = MAYBE;
            _isUnitAware = false;
        }

        return response;
    }

    /**
     * Returns true. This class can write PointVehicle data to a VECC Geometry file.
     *
     * @return this method always returns true.
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Reads in a VECC MK5 or GEO geometry file from the specified input file and returns
     * a {@link PointVehicle} object that contains the geometry from the file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link PointVehicle} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         components in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     */
    @Override
    public PointVehicle read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        // Create an empty vehicle with the provided name as the vehicle name.
        PointVehicle vehicle = PointVehicle.newInstance(inputFile.getName());

        //  VECC/GEO files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the ASCII file.
        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile))) {
            reader.mark(1024);

            // Is this a MK5 file or a plain GEO file?
            String aLine = readLine(reader);
            if (aLine.contains("new mk IV"))
                //  We have a mk5 file.
                read_habp(reader, vehicle);
            
            else {
                //  We just have a plain GEO file, so read in the panels into a single component.

                //  Read in the panels (and any sections that the panels may contain).
                reader.reset();
                PointComponent panelList = PointComponent.newInstance();
                read_panels(reader, panelList);

                if (panelList.size() > 0) {
                    //  Create the component that will go in the vehicle.
                    PointComponent component = PointComponent.newInstance();

                    //  Loop over all the panels in the panel list.
                    for (PointArray panel : panelList) {
                        //  Add the panel to the component.
                        component.add(panel);

                        //  Add any sections temporary stored in the panel's user data.
                        extractSections(panel, component);
                    }

                    //  Add the component to the vehicle.
                    vehicle.add(component);
                }
            }

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return vehicle;
    }

    /**
     * Writes out a VECC MK5 geometry file for the geometry contained in the supplied
     * {@link PointVehicle} object.
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The {@link PointVehicle} object to be written out. May not be
     *                   null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();

        //  Convert the input generic geometry list to a PointVehicle.
        PointVehicle vehicle = geomList2PointVehicle(requireNonNull(geometry));
        if (!vehicle.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  Determine the unit code for the input geometry (and user's locale).
        int unitCode = getUnitCode(vehicle);

        //  VECC/GEO files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Get a reference to the output stream writer.
        StackContext.enter();
        try (PrintWriter writer = new PrintWriter(outputFile, StandardCharsets.US_ASCII.name())) {

            //  Write the executive control card.
            writer.println("10                                                    new mk IV 0 4");

            //  Write the system control card.
            writer.println("10000000000000000000");

            //  Write the geometry control card.
            writer.print(" 800000");
            writer.print(padSpaces(String.valueOf(vehicle.size()), 2));
            String name = vehicle.getName();
            if (isNull(name) || name.equals(""))
                name = RESOURCES.getString("veccDefaultVehName");
            writer.print(padSpaces(name, 60));
            writer.println(unitCode);

            //  Combine all the arrays in all the components in this vehicle into one list of panels
            //  for writing out.
            PointComponent panelList = PointComponent.newInstance();
            for (PointComponent comp : vehicle) {
                if (comp.containsGeometry())
                    panelList.addAll(comp);
            }

            //  Write out the geometry.
            writeGeometry(writer, panelList);

            //  Write the component names.
            writeComponentNames(writer, vehicle, panelList);

            //  Write out the other misc. stuff.
            writer.println("  0 other run(s) follow");
            writer.println("  0 quadstream inputs");
            writer.println("  1 trim inputs");
            writer.println("  0");
            writer.println("  0");
            writer.println("  1 Cg locations follow");
            writer.println("  -100.0000     0.0000     0.0000");

        } finally {
            StackContext.exit();

            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

    }

    /**
     * Returns true if this reader is unit aware and false if it is not. VECC MK5 files
     * are unit aware and this method returns true by default. However, the related GEO
     * files do not encode the units that are being used (are not unit aware). You must
     * call <code>setFileUnits</code> to set the units being used before reading from a
     * file of GEO format. If "canReadData" is called with either a GEO or MK5 file before
     * calling this method, then this method will return the correct value.
     *
     * @return true if this reader is unit aware.
     * @see #setFileUnits(javax.measure.unit.Unit)
     * @see #canReadData(java.io.File)
     */
    @Override
    public boolean isUnitAware() {
        return _isUnitAware;
    }

    /**
     * Read in geometry stored in a MK5 file (which has some header info and a lot of
     * other stuff we are ignoring.
     */
    private void read_habp(LineNumberReader in, PointVehicle vehicle) throws IOException {
        try {
            //  Read system control card
            String aLine = readLine(in);
            Text text = Text.valueOf(aLine).trim();

            //  Read in the geometry from the mk5 file.
            for (int i = 0; i < 20; ++i) {
                int ipg = TypeFormat.parseInt(text.subtext(i, i + 1));
                if (ipg == 1) {
                    read_habp_geom(in, vehicle);
                    break;
                }
            }
            if (vehicle.size() == 0)
                return;

            //  Extract the temporary list of panels that have been read in.
            PointComponent panelList = vehicle.get(0);

            //  Find the list of components in the mk5 file.
            aLine = readLine(in);
            while (!aLine.startsWith("component names")) {
                aLine = in.readLine();
            }

            //  Parse out the number of components.
            int nc = TypeFormat.parseInt(aLine.substring(15, 19).trim());
            if (DEBUG)
                System.out.println("Number of Components: nc = " + nc + ", numPanels = " + panelList.size());

            if (nc > 0)
                vehicle.remove(0);  //  Remove the temporary list from the vehicle.

            //  Loop over each of the components.
            for (int i = 0; i < nc; ++i) {
                //  Read in the next line.
                aLine = readLine(in);
                text = Text.valueOf(aLine);

                //  Parse out the number of panels.
                int np = TypeFormat.parseInt(text.subtext(0, 2).trim());

                //  Parse out the name of the component.
                Text title = text.subtext(2).trim();

                //  Create a new component.
                PointComponent comp = PointComponent.newInstance(title.toString());

                if (DEBUG)
                    System.out.println("Component #" + (i + 1) + ":  title = " + title + ", np = " + np);

                //  Add each panel from the panelList as needed.
                aLine = readLine(in);
                text = Text.valueOf(aLine);
                for (int j = 0; j < np; ++j) {
                    int pos = j * 2;
                    Text numText = text.subtext(pos, pos + 2).trim();
                    int ipanel = TypeFormat.parseInt(numText);

                    //  Get the panel indicated.
                    PointArray panel = panelList.get(ipanel - 1);

                    //  Add the panel to the component.
                    comp.add(panel);

                    if (DEBUG)
                        System.out.println("    ipanel = " + ipanel + ", name = " + panel.getName());

                    //  Does the panel have any sections?  If so, store them
                    //  in the component as panels.
                    extractSections(panel, comp);
                }

                //  Add the component to the vehicle.
                if (comp.size() > 0)
                    vehicle.add(comp);
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("parseErrMsg"), DESCRIPTION, in.getLineNumber()));
        }
    }

    /**
     * Extracts any "sections" stored in the user data of the provided panel and adds them
     * to the specified component as panels.
     */
    private void extractSections(PointArray panel, PointComponent component) {
        //  Does the panel have any sections?
        FastTable<PointArray> sectionList = (FastTable<PointArray>)panel.getUserData("Sections");
        if (nonNull(sectionList)) {
            //  Add all the sections to the component as panels.
            for (PointArray section : sectionList) {
                if (DEBUG) {
                    System.out.println("    section name = " + section.getName());
                }
                component.add(section);
            }

            //  Remove the sections from the user data of the main panel.
            panel.removeUserData("Sections");
            FastTable.recycle(sectionList);
        }
    }

    /**
     * Read in geometry stored in a MK5 file (which has some header info and a lot of
     * other stuff we are ignoring.
     */
    private void read_habp_geom(LineNumberReader in, PointVehicle vehicle)
            throws IOException, NumberFormatException {
        //  Read geometry control card
        String aLine = readLine(in);
        Text text = Text.valueOf(aLine);

        //  Parse out the configuration name.
        //Text name = text.subtext(8, 8+61);
        //vehicle.setName(name);
        
        //  Parse out the units.
        int unitCode = TypeFormat.parseInt(text.subtext(9 + 60).trim());
        switch (unitCode) {
            case 0:
                setFileUnits(NonSI.INCH);
                break;
            case 1:
                setFileUnits(NonSI.FOOT);
                break;
            case 2:
                setFileUnits(SI.MILLIMETER);
                break;
            case 3:
                setFileUnits(SI.CENTIMETER);
                break;
            case 4:
                setFileUnits(SI.METER);
                break;
        }

        //  Now read in the panels into a temporary component which is a list of all the panels.
        PointComponent component = PointComponent.newInstance();
        read_panels(in, component);

        //  Store the temporary component in the vehicle.
        if (component.size() > 0)
            vehicle.add(component);
    }

    /**
     * Read in the panels stored in a GEO file (which may or may not have a header
     * identifying the panel name, etc).
     *
     * @param in        The reader used to read text from the file.
     * @param component The component to add this panel to.
     */
    private void read_panels(LineNumberReader in, PointComponent component) throws IOException {
        int last = 0;

        //  Loop over all the panels
        while (last == 0) {
            int isymm = 0;
            int itype = 3;
            Text name = Text.EMPTY;

            //  Read in the 1st line.
            in.mark(1024);
            String aLine = readLine(in);
            Text text = Text.valueOf(aLine);

            //  See if the first record is a panel header by attempting to 
            //  parse grid points from it.
            try {
                TypeFormat.parseDouble(text.subtext(0, 10).trim());
                TypeFormat.parseDouble(text.subtext(10, 20).trim());
                TypeFormat.parseDouble(text.subtext(20, 30).trim());
                TypeFormat.parseInt(text.subtext(30, 31));
                in.reset();
                //  If no error, there are not any header lines

            } catch (NumberFormatException e) {
                // Parse the panel header using defaults for any errors.

                try {
                    //  Read in the last flag.  If it is != 0, this is the last panel.
                    last = TypeFormat.parseInt(text.subtext(4, 5));

                    //  Read in the symmetry flag.
                    isymm = TypeFormat.parseInt(text.subtext(16, 17));

                    //  Read in the panel type.
                    itype = TypeFormat.parseInt(text.subtext(26, 27));

                    //  Read in the name.
                    name = text.subtext(29).trim();
                } catch (NumberFormatException err) { /* ignore */ }

                if (name.equals(Text.EMPTY))
                    name = text.subtext(0, 5);

                //  Read in the 2nd part of the header.
                aLine = readLine(in);
                parse_units(Text.valueOf(aLine));
            }

            if (DEBUG) {
                System.out.println("name = " + name + ", last = " + last + ", isymm = "
                        + isymm + ", itype = " + itype);
                System.out.println("units = " + getFileUnits());
            }

            //  Create a new PointArray object for this panel.
            PointArray panel = PointArray.newInstance(name.toString());
            panel.putUserData("Symmetry", (isymm == 0 ? 1 : 0));
            panel.putUserData("HABPType", itype);

            //  Read in all the sections.
            read_sections(in, panel);

            //  Add the panel to the component.
            component.add(panel);

        }   //  Next panel.
    }

    /**
     * Parses the units from the 2nd line of the GEO header (if there are any).
     */
    private void parse_units(Text aLine) {
        Text text = aLine.subtext(2, 4).toLowerCase();

        if (text.equals(IN_TEXT))
            setFileUnits(NonSI.INCH);
        else if (text.equals(FT_TEXT))
            setFileUnits(NonSI.FOOT);
        else if (text.equals(CM_TEXT))
            setFileUnits(SI.CENTIMETER);
        else if (text.equals(MM_TEXT))
            setFileUnits(SI.MILLIMETER);
        else if (text.equals(ME_TEXT))
            setFileUnits(SI.METER);
    }

    /**
     * Read in all the single sections for a single panel from a GEO file.
     *
     * @param in    The reader used to read text from the file.
     * @param panel The panel to add this section to.
     *
     */
    private void read_sections(LineNumberReader in, PointArray panel) throws IOException {
        PointArray mainPanel = panel;
        int sectionCount = 1;

        //  Create a new string.
        PointString section = PointString.newInstance();

        try {
            boolean first = true;
            boolean moreSections = true;
            readLoop:
            while (moreSections) {
                //  Read in a line.
                String aLine = readLine(in);
                Text text = Text.valueOf(aLine);

                //  Try to read two points from the record.
                for (int jj = 0; jj < 2; ++jj) {
                    int icol = jj * 31;
                    Text pointText = text.subtext(icol, icol + 31);
                    if (pointText.trim().equals(Text.EMPTY))
                        continue readLoop;

                    //  Parse out the coordinate values.
                    double xValue = TypeFormat.parseDouble(pointText.subtext(0, 10).trim());
                    double yValue = TypeFormat.parseDouble(pointText.subtext(10, 20).trim());
                    double zValue = TypeFormat.parseDouble(pointText.subtext(20, 30).trim());
                    int iflag = TypeFormat.parseInt(pointText.subtext(30, 31));

                    //  Create a point.
                    Point point = Point.valueOf(xValue, yValue, zValue, getFileUnits());

                    if (iflag == 1 || (!first && iflag == 2)) {
                        //  We are done with this section, start a new one.
                        panel.add(section);
                        section = PointString.newInstance();
                    }

                    if (!first && iflag == 2) {
                        //  Create a new panel, based on the last panel, for the new sections.
                        //  Store them in the main panel's user data for now.
                        PointArray newPanel = PointArray.newInstance(
                                mainPanel.getName() + Integer.toString(sectionCount++));
                        Object ud = mainPanel.getUserData("Symmetry");
                        if (nonNull(ud))
                            newPanel.putUserData("Symmetry", ud);
                        ud = mainPanel.getUserData("HABPType");
                        if (nonNull(ud))
                            newPanel.putUserData("HABPType", ud);
                        FastTable<PointArray> sectionList
                                = (FastTable<PointArray>)mainPanel.getUserData("Sections");
                        if (isNull(sectionList)) {
                            sectionList = FastTable.newInstance();
                            mainPanel.putUserData("Sections", sectionList);
                        }
                        sectionList.add(newPanel);
                        panel = newPanel;
                    }

                    //  Add this point to the section.
                    section.add(point);

                    if (iflag == 3) {
                        //  If this is the end of the panel we are finished.
                        panel.add(section);
                        moreSections = false;
                    }

                    first = false;
                }

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("parseErrMsg"), DESCRIPTION, in.getLineNumber()));
        }
    }

    /**
     * Add spaces to the right of a string of text until that text equals the specified
     * length.
     *
     */
    private String padSpaces(String input, int length) {
        int inputLength = input.length();
        if (inputLength < length) {
            StringBuilder buf = new StringBuilder(input);
            for (int i = inputLength; i < length; ++i) {
                buf.append(" ");
            }
            input = buf.toString();
        }
        return input;
    }

    /**
     * Returns the unit code for the input geometry. This method gets the unit for the
     * minimum bounds of the geometry, then makes ure it is a unit that the VECC format
     * recognizes (and changes it to one that it does recognize if necessary). Then the
     * unit is stored using "setFileUnits". All input geometry must be converted to this
     * unit for storage in the VECC file.
     *
     */
    private int getUnitCode(PointVehicle geometry) {

        GeomPoint point = geometry.getBoundsMin();
        Unit<Length> unit = point.getUnit();

        int unitCode;
        if (unit.equals(NonSI.INCH))
            unitCode = 0;
        else if (unit.equals(NonSI.FOOT))
            unitCode = 1;
        else if (unit.equals(SI.MILLIMETER))
            unitCode = 2;
        else if (unit.equals(SI.CENTIMETER))
            unitCode = 3;
        else if (unit.equals(SI.METER))
            unitCode = 4;
        else {
            //  Convert to a locale specific option.
            if (java.util.Locale.getDefault().equals(java.util.Locale.US)) {
                unit = NonSI.INCH;
                unitCode = 0;

            } else {
                unit = SI.METER;
                unitCode = 4;
            }
        }

        setFileUnits(unit);

        return unitCode;
    }

    /**
     * Writes out the HABP geometry to the file.
     *
     */
    private void writeGeometry(PrintWriter out, PointComponent panelList) throws IOException {

        //  Loop over all the panels.
        int size = panelList.size();
        for (int i = 0; i < size; ++i) {
            PointArray panel = panelList.get(i);
            if (!panel.containsGeometry())
                continue;       //  Skip any empty panels.

            //  Write out this panel.
            write_panel(out, panel, (i == size - 1), i + 1);
        }

    }

    /**
     * Writes out a single panel.
     */
    private void write_panel(PrintWriter out, PointArray<? extends GeomPoint> panel, boolean last, int count) throws IOException {

        //  Write the panel header
        
        //  Write out the name.
        String fullName = panel.getName();
        if (isNull(fullName)) {
            //  No name given, so use a panel count instead (must start with a letter though).
            fullName = "P" + String.valueOf(count);
            if (fullName.length() > 4)
                fullName = "P" + fullName.substring(fullName.length() - 3);
        }
        String name = fullName;
        if (name.length() > 4)
            name = name.substring(0, 4);
        else
            name = padSpaces(name, 4);
        out.print(name);

        //  Indicate if this is the last panel or not.
        out.print((last ? 1 : 0));
        out.print("01         ");

        //  Write out the symmetry flag (0 == symmetric, 1 = non-symmetric).
        Integer isymm = (Integer)panel.getUserData("Symmetry");
        if (isNull(isymm))
            out.print("0");
        else
            out.print((isymm == 0 ? 1 : 0));
        out.print("0        ");

        //  Write out the panel type.
        Integer itype = (Integer)panel.getUserData("HABPType");
        if (isNull(itype))
            out.print("3");
        else
            out.print(itype.intValue());
        out.print("  ");

        //  Write out the name again (but longer this time).
        if (fullName.length() > 16)
            fullName = fullName.substring(0, 16);
        else
            fullName = padSpaces(fullName, 16);
        out.println(fullName);

        out.println(" 1 000");

        //  Write out the array of data.
        int icol = 1;
        int iflag = 2;  //  flag indicating we are at the start of a panel.
        int nCuts = panel.size() - 1;
        int nPoints = panel.get(0).size() - 1;

        //  Loop over all the cuts (strings) in this panel.
        for (int l = 0; l <= nCuts; ++l) {
            PointString<?> cut = panel.get(l);

            //  Loop over all the points in this cut/string.
            for (int m = 0; m <= nPoints; ++m) {
                if (m == nPoints && l == nCuts)
                    iflag = 3;  //  flag indicating we are at the end of the panel.

                //  Get the point (in the right units).
                GeomPoint point = cut.get(m).to(getFileUnits());
                double x = point.getValue(0);
                double y = point.getValue(1);
                double z = point.getValue(2);

                //  Write it out.
                out.printf("%10.4f", x);
                out.printf("%10.4f", y);
                out.printf("%10.4f", z);

                //  Write the iflag.
                out.print(iflag);

                //  Deal with the columns.
                if (icol != 1) {
                    out.println("         3");
                    icol = 0;
                }
                ++icol;

                //  Change the iflag to indicate we are in the middle of a cut.
                iflag = 0;
            }
            //  Change the iflag to indicate we are starting a new cut.
            iflag = 1;
        }

        //  If there were an odd number of points, output the last line ending.
        if (icol != 1)
            out.println("                                        3");

    }

    /**
     * Write out the names of all the components.
     */
    private void writeComponentNames(PrintWriter out, PointVehicle geometry, PointComponent panelList) throws IOException {

        //  Collect only non-empty components.
        PointVehicle geomV = PointVehicle.newInstance(geometry.getName());
        for (PointComponent comp : geometry) {
            if (comp.containsGeometry())
                geomV.add(comp);
        }

        //  Output header.
        out.printf("component names %3d", geomV.size());
        out.println();

        for (PointComponent comp : geomV) {

            //  Get a 16 character title.
            String title = comp.getName();
            if (isNull(title))
                title = String.valueOf(comp.getID());
            if (title.length() > 16)
                title = title.substring(0, 16);
            else
                title = padSpaces(title, 16);

            //  Write out the number of panels in this component.
            int size = comp.size();
            out.printf("%2d", size);

            //  Write out the name of the panel.
            out.println(title);

            //  Write out the indices to each panel in this component.
            for (PointArray array : comp) {
                if (array.containsGeometry()) {
                    int index = panelList.indexOf(array);
                    out.printf("%2d", index + 1);
                }
            }
            out.println();
        }

    }
}
