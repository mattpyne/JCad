/*
 *   LaWGSGeomReader  -- A class that can read a LaWGS formatted geometry file.
 *
 *   Copyright (C) 2009-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import static geomss.geom.reader.AbstractGeomReader.RESOURCES;
import jahuwaldt.js.util.TextTokenizer;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.MessageFormat;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javolution.text.Text;
import javolution.text.TypeFormat;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading vehicle geometry from a LaWGS (WGS) formatted geometry
 * file. This is the Langley Wireframe Geometry Standard (LaWGS) defined in NASA-TM-85767.
 * This implementation ignores the local symmetry and local transformation information!
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 27, 2009
 * @version September 9, 2016
 */
public class LaWGSGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    private static final boolean DEBUG = false;

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("lawgsDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "wgs";

    //  Any of the delimiters that could be found in the file.
    private static final String DELIMITERS = " ,/";

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file format is definitely recognized by this reader.
     *         GeomReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".wgs"))
            response = MAYBE;

        return response;
    }

    /**
     * Reads in a LaWGS geometry file from the specified input file and returns a
     * {@link PointVehicle} object that contains the geometry from the LaWGS file.
     * <p>
     * WARNING: This file format is not unit aware. You must set the units
     * to be used by calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link PointVehicle} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         components in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public PointVehicle read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        // Create an empty vehicle.
        PointVehicle vehicle = PointVehicle.newInstance();

        //  LaWGS files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the ASCII file.
        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile))) {


            //  Skip any leading blank lines.
            String aLine = readLine(reader).trim();
            while (aLine.length() == 0)
                aLine = readLine(reader).trim();

            //  Extract the configuration identification.
            String idcomf = parseQuotedText(Text.valueOf(aLine), "'");
            if (isNull(idcomf))
                idcomf = inputFile.getName();

            // Set the name as the vehicle name.
            vehicle.setName(idcomf);

            //  There is only a single component, so create it.
            PointComponent comp = PointComponent.newInstance("Component");
            vehicle.add(comp);

            // Loop over all the arrays stored in the file.
            aLine = reader.readLine();
            while (nonNull(aLine)) {

                String arrName = parseQuotedText(Text.valueOf(aLine), "'");
                PointArray array = readArray(reader, arrName);
                comp.add(array);

                // Begin searching for the next component.
                aLine = reader.readLine();
            }

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return vehicle;
    }

    /**
     * This method always returns <code>false</code> as LaWGS files do not encode the
     * units that are being used. You must call <code>setFileUnits</code> to set the units
     * being used before reading or writing to a file of this format.
     *
     * @return this implementation always returns false
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

    /**
     * Reads a single array or network from an input stream (pointing to the appropriate
     * location in a LaWGS file).
     *
     * @param in        Reader for the LaWGS file we are reading (positioned so that the
     *                  next read will occur on the line following the line containing the
     *                  name of the array).
     * @param arrayName The name of the array.
     * @return The network read in from the file.
     * @throws java.io.IOException if there is a problem reading the array.
     */
    private PointArray readArray(LineNumberReader in, String arrayName) throws IOException {
        PointArray net = null;

        // Create the needed tables.
        FastTable<Point> pointList = FastTable.newInstance();
        FastTable<PointString<Point>> stringList = FastTable.newInstance();

        try {
            // Read in the object information line.
            String aLine = readLine(in);
            TextTokenizer tokenizer = TextTokenizer.valueOf(aLine, DELIMITERS);
            if (tokenizer.countTokens() != 14)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("parseErrMsg"), DESCRIPTION, in.getLineNumber()));

            //  Parse out the object ID number.
            Text token = tokenizer.nextToken();
            int nObj = TypeFormat.parseInt(token);

            //  Parse out the number of contour lines.
            token = tokenizer.nextToken();
            int nLine = TypeFormat.parseInt(token);

            //  Parse out the number of points in each contour line.
            token = tokenizer.nextToken();
            int nPnt = TypeFormat.parseInt(token);

            // Do a sanity check.
            if (nLine < 0 || nLine > 1000000 || nPnt < 0 || nPnt > 1000000)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("incRowsColsErr"), DESCRIPTION, in.getLineNumber()));

            //  Parse out the local symmetry code.
            token = tokenizer.nextToken();
            int iSymL = TypeFormat.parseInt(token);
            if (iSymL < 0 || iSymL > 3)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("lawgsUnknownLocalSymm"), in.getLineNumber()));

            //  Parse out the rotation angles.
            token = tokenizer.nextToken();
            double rx = TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            double ry = TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            double rz = TypeFormat.parseDouble(token);

            //  Parse out the translations.
            token = tokenizer.nextToken();
            double tx = TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            double ty = TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            double tz = TypeFormat.parseDouble(token);

            //  Parse out the scales in each axis.
            token = tokenizer.nextToken();
            double xScale = TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            double yScale = TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            double zScale = TypeFormat.parseDouble(token);

            //  Parse out the global symmetry code.
            token = tokenizer.nextToken();
            int iSymG = TypeFormat.parseInt(token);
            if (iSymG < 0 || iSymG > 3)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("lawgsUnknownGlobalSymm"), in.getLineNumber()));

            if (DEBUG) {
                System.out.println("arrayName = " + arrayName + ", nLine = " + nLine + ", nPnt = " + nPnt + ", iSymL = " + iSymL);
                System.out.println("  rx,ry,rz = " + rx + "," + ry + "," + rz);
                System.out.println("  tx,ty,tz = " + tx + "," + ty + "," + tz);
                System.out.println("  sx,sy,sz = " + xScale + "," + yScale + "," + zScale);
                System.out.println("  iSymG = " + iSymG);
            }

            // Loop over each line.
            for (int i = 0; i < nLine; ++i) {
                if (DEBUG)
                    System.out.println("i = " + i);
                aLine = readLine(in);
                tokenizer.setText(aLine);
                if (tokenizer.countTokens() != 3 && tokenizer.countTokens() != 6)
                    throw new IOException(MessageFormat.format(
                            RESOURCES.getString("incPointCount"), in.getLineNumber()));

                int pntsPerLine = tokenizer.countTokens() / 3;

                // Loop over each point.
                int count = 1;
                for (int j = 0; j < nPnt; ++j) {

                    // There may be a limited number of points per line.
                    if (count > pntsPerLine) {
                        count = 1;
                        aLine = readLine(in);
                        tokenizer.setText(aLine);
                        if (tokenizer.countTokens() != 3 && tokenizer.countTokens() != 6)
                            throw new IOException(MessageFormat.format(
                                    RESOURCES.getString("incPointCount"), in.getLineNumber()));
                    }

                    // Read in X coordinate.
                    token = tokenizer.nextToken();
                    double xValue = TypeFormat.parseDouble(token);

                    // Read in Y coordinate.
                    token = tokenizer.nextToken();
                    double yValue = TypeFormat.parseDouble(token);

                    // Read in Z coordinate.
                    token = tokenizer.nextToken();
                    double zValue = TypeFormat.parseDouble(token);

                    if (DEBUG)
                        System.out.println("col = " + j + ", x,y,z = " + xValue + ", " + yValue + ", " + zValue);

                    //  Create a Point object.
                    Point point = Point.valueOf(xValue, yValue, zValue, getFileUnits());
                    pointList.add(point);

                    ++count;
                }

                //  Create a PointString object from the points.
                PointString<Point> string = PointString.valueOf(null, pointList);
                stringList.add(string);

                //  Clear the point list for the next row.
                pointList.clear();
            }

            // Create a new network from the points just read in.
            net = PointArray.valueOf(arrayName, stringList);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new IOException(
                    MessageFormat.format(RESOURCES.getString("parseErrMsg"),
                            DESCRIPTION, in.getLineNumber()));

        } finally {
            //  Recycle the lists.
            FastTable.recycle(pointList);
            FastTable.recycle(stringList);
        }

        return net;
    }

    /**
     * Method that parses out quoted text from a string of text and returns whatever is
     * between the quotes.
     *
     * @param input     The text containing a quote.
     * @param quoteChar The character that delimits the quote.
     * @return All the text between the 1st and last occurrence of quoteChar in the input
     *         Text.
     */
    private static String parseQuotedText(Text input, CharSequence quoteChar) {
        int idx1 = input.indexOf(quoteChar, 0) + 1;
        int idx2 = input.lastIndexOf(quoteChar);
        Text output;
        if (idx1 < 0 || idx2 < 0)
            output = input;
        else
            output = input.subtext(idx1, idx2);

        if (isNull(output))
            return null;

        output = output.trim();
        if (Text.EMPTY.equals(output))
            return null;
        return output.toString();
    }

}
