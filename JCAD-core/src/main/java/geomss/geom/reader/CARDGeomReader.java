/**
 * CARDGeomReader -- A class that can read and write an APAS II CARD formatted geometry
 * file.
 *
 * Copyright (C) 2009-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import jahuwaldt.js.param.EulerAngles;
import jahuwaldt.js.param.Parameter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.text.Text;
import javolution.text.TypeFormat;
import javolution.util.FastTable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.number.Integer64;

/**
 * A {@link GeomReader} for reading and writing vehicle geometry from/to an APAS II
 * (Airplane Preliminary Analysis System) CARD formatted geometry file.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 27, 2009
 * @version September 9, 2016
 */
public class CARDGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    private static final boolean DEBUG = false;

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("cardDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "card";

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file has the extension ".geo" or ".mk5".
     *         GeomReader.MAYBE if the file has the extension ".lib".
     * @throws java.io.IOException if there is a problem reading from the specified file
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".card")) {
            response = MAYBE;
        }

        return response;
    }

    /**
     * Returns true. This class can write point geometry to an APAS CARD formatted file.
     *
     * @return true
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Reads in an APAS CARD formatted geometry file from the specified input file and
     * returns a {@link PointVehicle} object that contains the geometry from the file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link PointVehicle} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         components in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     */
    @Override
    public PointVehicle read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        // Create an empty vehicle with the provided name as the vehicle name.
        PointVehicle vehicle = PointVehicle.newInstance();

        //  CARD files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the ASCII file.
        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile))) {

            // Get the number of records and the units.
            String aLine = readLine(reader);
            Text text = Text.valueOf(aLine);
            //int nRecords = TypeFormat.parseInt(text.subtext(0, 5).trim());
            int unitCode = TypeFormat.parseInt(text.subtext(5).trim());
            selectUnits(unitCode);

            //  Get the title of the configuration.
            aLine = readLine(reader);
            vehicle.setName(aLine.trim());

            //  Get a list of component numbers from the next two lines.
            aLine = readLine(reader);
            String aLine2 = readLine(reader);
            text = Text.valueOf(aLine).concat(Text.valueOf(aLine2));
            int length = text.length();
            if (length <= 8)
                throw new IOException(RESOURCES.getString("cardMissingCompNum"));

            FastTable<Integer64> compNumbers = FastTable.newInstance();
            int pos = 0;
            int posp8 = 8;
            while (pos < length) {
                Text token = text.subtext(pos, posp8).trim();
                int num = (int)TypeFormat.parseDouble(token);
                compNumbers.add(Integer64.valueOf(num));
                pos = posp8;
                posp8 = pos + 8;
                if (posp8 > length)
                    posp8 = length;
            }

            int numComponents = compNumbers.size();
            if (numComponents == 0)
                throw new IOException(RESOURCES.getString("cardCompNumParseErr"));

            //  Read in each component in turn.
            for (int i = 0; i < numComponents; ++i) {
                PointComponent comp = readComponent(reader);
                vehicle.add(comp);
            }

            //  Cleanup before leaving.
            FastTable.recycle(compNumbers);

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return vehicle;
    }

    /**
     * Writes out an APAS CARD geometry file for the geometry contained in the supplied
     * {@link PointVehicle} object.
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The {@link PointVehicle} object to be written out. May not be
     *                   null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  Convert the input generic geometry list to a PointVehicle.
        PointVehicle vehicle = geomList2PointVehicle(geometry);

        if (!vehicle.containsGeometry())
            return;

        //  Determine the unit code for the input geometry (and user's locale).
        int unitCode = getUnitCode(vehicle);

        //  CARD files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        StackContext.enter();
        // Get a reference to the output stream writer.
        try (PrintWriter writer = new PrintWriter(outputFile, StandardCharsets.US_ASCII.name())) {

            //  Write out the number of records and the units.
            int size = vehicle.size();
            writer.printf("%5d", size + 1);
            writer.printf("%5d", unitCode);
            writer.println();

            //  Write out the name of the configuration (vehicle name).
            String name = vehicle.getName();
            if (isNull(name))
                name = "vehicle";
            writer.println(name);

            //  Write the component numbers out on the next two lines.
            int col = 0;
            for (int i = 0; i < size; ++i) {
                PointComponent comp = vehicle.get(i);
                Integer compNumber = (Integer)comp.getUserData("APASComponentNumber");
                if (isNull(compNumber)) {
                    compNumber = 10 + i;
                    comp.putUserData("APASComponentNumber", compNumber);
                }

                writer.printf("%5d.00", compNumber);
                ++col;

                if (col > 9 || i + 1 == size) {
                    writer.println();
                    col = 0;
                }
            }

            //  Write out each component to the file.
            for (PointComponent comp : vehicle) {
                writeComponent(writer, comp);
            }

        } finally {
            StackContext.exit();

            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

    }

    /**
     * Returns true if this reader is unit aware and false if it is not. APAS CARD files
     * are unit aware and this method returns true.
     *
     * @return this implementation always returns true.
     */
    @Override
    public boolean isUnitAware() {
        return true;
    }

    /**
     * Selects the appropriate units for the unit code provided.
     */
    private void selectUnits(int unitCode) throws IOException {
        switch (unitCode) {
            case 1:
                setFileUnits(SI.METER);
                break;
            case 2:
                setFileUnits(NonSI.INCH);
                break;
            case 3:
                setFileUnits(SI.CENTIMETER);
                break;
            default:
                throw new IOException(
                        MessageFormat.format(RESOURCES.getString("cardbadTypeCode"), unitCode));
        }
    }

    /**
     * Method that reads in a single component from the file.
     */
    private PointComponent readComponent(LineNumberReader in) throws IOException {
        Unit<Length> units = getFileUnits();

        //  Create an empty component.
        PointComponent component = PointComponent.newInstance();

        try {
            //  Parse the 1st line.
            String aLine = readLine(in);
            Text text = Text.valueOf(aLine);

            //  Get the component number.
            Text token = text.subtext(0, 8).trim();
            int compNum = (int)TypeFormat.parseDouble(token);
            component.putUserData("APASComponentNumber", compNum);

            //  Get the component type code.
            token = text.subtext(8, 10).trim();
            int typeCode = TypeFormat.parseInt(token);
            component.putUserData("APASType", typeCode);

            //  Get the component name.
            Text name = text.subtext(10, 26).trim();
            component.setName(name.toString());

            //  Get the number of cross sections in the component.
            token = text.subtext(26, 28).trim();
            int numXSections = TypeFormat.parseInt(token);

            //  Get the number of points per cross section.
            token = text.subtext(28, 30).trim();
            int numPointsPerXSect = TypeFormat.parseInt(token);

            //  Get the number of segments per cross section.
            token = text.subtext(30, 32).trim();
            int numSegmentsPerXSect = TypeFormat.parseInt(token);

            //  Get the symmetry code.
            token = text.subtext(32, 34).trim();
            int symCode = TypeFormat.parseInt(token);
            component.putUserData("APASSymmetry", symCode);

            //  Get the panel code.
            token = text.subtext(34, 36).trim();
            int panelCode = TypeFormat.parseInt(token);
            component.putUserData("APASPanelCode", panelCode);

            //  Get the number of points in each segment.
            FastTable<Integer64> numPointsPerSeg = FastTable.newInstance();
            int total = 0;
            int pos = 36;
            for (int i = 0; i < numSegmentsPerXSect; ++i) {
                int posp2 = pos + 2;
                token = text.subtext(pos, posp2).trim();
                int num = TypeFormat.parseInt(token);
                total += num;
                numPointsPerSeg.add(Integer64.valueOf(num));
                pos = posp2;
            }
            if (total != numPointsPerXSect)
                throw new IOException(
                        MessageFormat.format(RESOURCES.getString("cardSegPointCountErr"),
                                in.getLineNumber()));

            //  Get the wetted/unwetted segment flag.
            FastTable<Integer> wettedSegFlags = FastTable.newInstance();
            for (int i = 0; i < numSegmentsPerXSect; ++i) {
                int posp2 = pos + 2;
                token = text.subtext(pos, posp2).trim();
                int num = TypeFormat.parseInt(token);
                wettedSegFlags.add(num);
                pos = posp2;
            }

            if (DEBUG) {
                System.out.println("CompID = " + compNum + ", type = " + typeCode + ", name = " + name);
                System.out.println("numXSections = " + numXSections + ", numPointsPerXSect = " + numPointsPerXSect);
                System.out.println("numSegmentsPerXSect = " + numSegmentsPerXSect + ", symCode = " + symCode);
                System.out.println("numPointsPerSeg = " + numPointsPerSeg + ", panelCode = " + panelCode);
                System.out.println("wettedSegFlags = " + wettedSegFlags);
            }

            //  Read in the geometry position records.
            aLine = readLine(in);
            text = Text.valueOf(aLine);

            //  Get the geometry position offsets.
            pos = 0;
            int posp12 = 12;
            token = text.subtext(pos, posp12).trim();
            double dx = TypeFormat.parseDouble(token);

            pos = posp12;
            posp12 = pos + 12;
            token = text.subtext(pos, posp12).trim();
            double dy = TypeFormat.parseDouble(token);

            pos = posp12;
            posp12 = pos + 12;
            token = text.subtext(pos, posp12).trim();
            double dz = TypeFormat.parseDouble(token);

            //  Get the geometry rotation angles.
            pos = posp12;
            posp12 = pos + 12;
            token = text.subtext(pos, posp12).trim();
            double yaw = TypeFormat.parseDouble(token);

            pos = posp12;
            posp12 = pos + 12;
            token = text.subtext(pos, posp12).trim();
            double pitch = TypeFormat.parseDouble(token);

            pos = posp12;
            token = text.subtext(pos).trim();
            double roll = TypeFormat.parseDouble(token);

            //  Create a transform if necessary.
            GTransform TM = GTransform.IDENTITY;
            if (dx != 0 || dy != 0 || dz != 0 || pitch != 0 || roll != 0 || yaw != 0) {
                Parameter<Angle> yawP = Parameter.valueOf(yaw, NonSI.DEGREE_ANGLE);
                Parameter<Angle> pitchP = Parameter.valueOf(pitch, NonSI.DEGREE_ANGLE);
                Parameter<Angle> rollP = Parameter.valueOf(roll, NonSI.DEGREE_ANGLE);
                EulerAngles ea = EulerAngles.valueOf(yawP, pitchP, rollP, EulerAngles.YAW_PITCH_ROLL);
                TM = GTransform.valueOf(ea.toDCM(), 1, Vector.valueOf(units, dx, dy, dz));
            }
            if (DEBUG) {
                if (TM.equals(GTransform.IDENTITY))
                    System.out.println("TM = IDENTITY");
                else {
                    System.out.println("dx,dy,z = " + dx + "," + dy + "," + dz);
                    System.out.println("pitch,roll,yaw = " + pitch + "," + roll + "," + yaw);
                    System.out.println("TM = \n" + TM);
                }
            }

            //  Skip the min and max values.
            in.readLine();

            //  Skip the HABP parameter lines.
            if (typeCode < 5) {
                for (int i = 0; i < 4; ++i) {
                    in.readLine();
                }
            }

            //  Read in all the coordinate values for this component.
            int numValues = numXSections * numPointsPerXSect;
            FastTable<Float64> xValues = readAxisValues(in, numValues);
            FastTable<Float64> yValues = readAxisValues(in, numValues);
            FastTable<Float64> zValues = readAxisValues(in, numValues);

            //  Break up the data into arrays (each segment across all the cross sections is an array).
            pos = 0;
            for (int i = 0; i < numSegmentsPerXSect; ++i) {
                PointArray array = PointArray.newInstance();
                array.putUserData("APASWetted", wettedSegFlags.get(i));
                int numPoints = (int)numPointsPerSeg.get(i).longValue();

                for (int j = 0; j < numXSections; ++j) {
                    PointString str = PointString.newInstance();
                    int idx = pos + j * numPointsPerXSect;

                    for (int k = 0; k < numPoints; ++k) {
                        double x = xValues.get(idx).doubleValue();
                        double y = yValues.get(idx).doubleValue();
                        double z = zValues.get(idx).doubleValue();
                        ++idx;
                        str.add(Point.valueOf(x, y, z, units));
                    }
                    array.add(str);
                }
                //  Apply any transform to each array.
                if (!TM.equals(GTransform.IDENTITY))
                    array = array.getTransformed(TM);

                //  Save the array in the component.
                component.add(array);
                pos += numPoints;
            }

            //  Cleanup before leaving.
            FastTable.recycle(xValues);
            FastTable.recycle(yValues);
            FastTable.recycle(zValues);
            FastTable.recycle(wettedSegFlags);
            FastTable.recycle(numPointsPerSeg);

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            throw new IOException(
                    MessageFormat.format(RESOURCES.getString("missingDataOnLine"),
                            in.getLineNumber()));
        }

        return component;
    }

    /**
     * Read in a table of coordinate axis (X, Y, or Z) values from the file.
     */
    private FastTable<Float64> readAxisValues(LineNumberReader in, int numValues) throws IOException {

        FastTable<Float64> values = FastTable.newInstance();

        StackContext.enter();
        try {
            Text text = null;
            int posp12 = 12;
            int col = 6;
            for (int i = 0; i < numValues; ++i) {
                //  Deal with the 6 column width of the CARD format.
                if (col == 6) {
                    String aLine = readLine(in);
                    text = Text.valueOf(aLine);
                    posp12 = 0;
                    col = 0;
                }

                int pos = posp12;
                posp12 = pos + 12;
                @SuppressWarnings("null")
                Text token = text.subtext(pos, posp12).trim();
                double value = TypeFormat.parseDouble(token);
                values.add(StackContext.outerCopy(Float64.valueOf(value)));

                ++col;
            }

        } finally {
            StackContext.exit();
        }

        return values;
    }

    /**
     * Returns the unit code for the input geometry. This method gets the unit for the
     * minimum bounds of the geometry, then makes sure it is a unit that the APAS CARD
     * format recognizes (and changes it to one that it does recognize if necessary). Then
     * the unit is stored using "setFileUnits". All input geometry must be converted to
     * this unit for storage in the APAS CARD file.
     */
    private int getUnitCode(PointVehicle geometry) {

        GeomPoint point = geometry.getBoundsMin();
        Unit<Length> unit = point.getUnit();

        int unitCode;
        if (unit.equals(SI.METER))
            unitCode = 1;
        else if (unit.equals(NonSI.INCH))
            unitCode = 2;
        else if (unit.equals(SI.CENTIMETER))
            unitCode = 3;
        else {
            //  Convert to a locale specific option.
            if (Locale.getDefault().equals(Locale.US)) {
                unit = NonSI.INCH;
                unitCode = 2;

            } else {
                unit = SI.METER;
                unitCode = 1;
            }
        }

        setFileUnits(unit);

        return unitCode;
    }

    /**
     * Add spaces to the right of a string of text until that text equals the specified
     * length.
     */
    private String padSpaces(String input, int length) {
        int inputLength = input.length();
        if (inputLength < length) {
            StringBuilder buf = new StringBuilder(input);
            for (int i = inputLength; i < length; ++i) {
                buf.append(" ");
            }
            input = buf.toString();
        }
        return input;
    }

    /**
     * Writes out the geometry for a single component to an APAS CARD file.
     */
    private void writeComponent(PrintWriter out, PointComponent comp) throws IOException {
        Unit<Length> units = getFileUnits();

        //  Write the component number.
        Integer num = (Integer)comp.getUserData("APASComponentNumber");
        out.printf("%5d.00", num);

        //  Write out the APAS type code.
        num = (Integer)comp.getUserData("APASType");
        if (isNull(num))
            num = 1;
        out.printf("%2d", num);
        int typeCode = num;

        //  Write out the component name.
        String name = comp.getName();
        if (isNull(name))
            name = "comp" + num;
        if (name.length() > 16)
            name = name.substring(0, 16);
        out.print(padSpaces(name, 16));

        //  Extract some data about the geometry.
        int numSegmentsPerXSect = comp.size();
        int numXSections = comp.get(0).size();
        int numPointsPerXSect = 0;
        for (PointArray arr : comp) {
            int numPoints = arr.get(0).size();
            numPointsPerXSect += numPoints;
        }

        //  Write out the number of cross sections in the component.
        out.printf("%2d", numXSections);

        //  Write out the number of points per cross section.
        out.printf("%2d", numPointsPerXSect);

        //  Write out the number of segments per cross section.
        out.printf("%2d", numSegmentsPerXSect);

        //  Write out the symmetry code.
        num = (Integer)comp.getUserData("APASSymmetry");
        if (isNull(num))
            num = 2;
        out.printf("%2d", num);

        //  Write out the panel code.
        num = (Integer)comp.getUserData("APASPanelCode");
        if (isNull(num))
            num = 0;
        out.printf("%2d", num);

        //  Write out the number of points in each segment.
        for (PointArray arr : comp) {
            int numPoints = arr.get(0).size();
            out.printf("%2d", numPoints);
        }

        //  Write out the wetted/unwetted flag.
        for (PointArray arr : comp) {
            num = (Integer)arr.getUserData("APASWetted");
            if (isNull(num))
                num = 1;
            out.printf("%2d", num);
        }
        out.println();

        //  Write out the geometry position and rotation records.
        out.println(" 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00");

        //  Write out the min & max bounds for the component.
        GeomPoint boundsMin = comp.getBoundsMin().to(units);
        GeomPoint boundsMax = comp.getBoundsMax().to(units);
        out.printf("%12.5E", boundsMin.getValue(0));
        out.printf("%12.5E", boundsMax.getValue(0));
        out.printf("%12.5E", boundsMin.getValue(1));
        out.printf("%12.5E", boundsMax.getValue(1));
        out.printf("%12.5E", boundsMin.getValue(2));
        out.printf("%12.5E", boundsMax.getValue(2));
        out.println();

        //  Write out dummy HABP parameter lines.
        if (typeCode < 5) {
            out.println("    0    0    0    0  0.0000      0.0000      0.0000      0.0000    ");
            out.println("    0    0    0    0  0.0000      0.0000      0.0000      0.0000    ");
            out.println("    0    0    0    0  0.0000      0.0000      0.0000      0.0000    ");
            out.println("  0.0000      0.0000      0.0000       1");
        }

        //  Convert the component to the output units.
        comp = comp.to(units);

        //  Write out all the coordinate values for this component.
        writeAxisValues(out, Point.X, comp);
        writeAxisValues(out, Point.Y, comp);
        writeAxisValues(out, Point.Z, comp);
    }

    /**
     * Write out all the coordinate values for a component for the specified axis. These
     * are written out in cross sections (columns) through all the arrays.
     */
    private void writeAxisValues(PrintWriter out, int axis, PointComponent comp) {

        int numXSections = comp.get(0).size();
        int col = 0;
        for (int i = 0; i < numXSections; ++i) {
            for (PointArray<?> arr : comp) {
                PointString<?> str = arr.get(i);
                for (GeomPoint point : str) {
                    out.printf("%12.5E", point.getValue(axis));
                    ++col;
                    if (col == 6) {
                        col = 0;
                        out.println();
                    }
                }
            }
        }
        if (col != 0)
            out.println();
    }
}
