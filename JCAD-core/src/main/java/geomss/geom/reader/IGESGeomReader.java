/*
 *   IGESGeomReader -- A class that can read and write an IGES formatted geometry file.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.ControlPoint;
import geomss.geom.nurbs.CurveUtils;
import geomss.geom.nurbs.NurbsCurve;
import geomss.geom.reader.iges.*;
import jahuwaldt.js.param.Parameter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.util.FastMap;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading and writing geometry to an IGES
 * formatted point file. This implementation ignores many the IGES entity types at this
 * time.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: August 21, 2010
 * @version September 9, 2016
 */
public class IGESGeomReader extends AbstractGeomReader {

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("igesDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "igs";

    /**
     * A mapping of IGES Entity objects to geometry elements. This is used to prevent
     * multiple instances of the same element being written to an IGES point.
     */
    private FastMap<GeomElement, Entity> elementEntityMap;

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file format is definitely recognized by this reader.
     *         GeomReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        if (inputFile.isFile() && inputFile.exists()) {
            String name = inputFile.getName();
            name = name.toLowerCase().trim();
            if (name.endsWith(".igs") || name.endsWith(".iges"))
                response = YES;
        }

        return response;
    }

    /**
     * Reads in an IGES geometry file from the specified input file and returns a
     * {@link GeometryList} object that contains the geometry from the file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not
     *                  be null.
     * @return A {@link GeometryList} object containing the geometry read in from the
     *         file. If the file has no readable geometry in it, then this list will have
     *         no elements in it (will have a size() of 0).
     * @throws IOException If there is a problem reading the specified file.
     */
    @Override
    public GeometryList read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        //  Create an empty geometry list.
        GeometryList output = GeomList.newInstance();

        //  IGES files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the IGES ASCII file.
        try (RandomAccessFile in = new RandomAccessFile(inputFile, "r")) {

            //  Use the IGES library to do the actual reading of the file.
            Part part = new Part();
            part.read(in);

            //  Store any error/warning messages.
            _warnings.addAll(part.getErrors());

            //  Get a list of all the entities read in from the IGES file.
            List<Entity> entities = part.getEntities();

            //  Copy the geometry from the IGES Part to the output list.
            for (Entity entity : entities) {
                if (entity instanceof GeomSSEntity) {
                    //  We only care about entities that have GeomSS geometry in them.
                    GeomSSEntity geomEntity = (GeomSSEntity)entity;

                    //  Only include entities that are not subsets of other entities (like lists).
                    if (!geomEntity.isUsedInList()) {

                        //  Ignore blanked and subordinate entities.
                        //if (geomEntity.blankedStatus() < 1 && geomEntity.subordStatus() < 1) {
                        //  Add the geometry to our output list.
                        GeomElement geom = geomEntity.getGeomElement(GTransform.IDENTITY);
                        if (geom != null)
                            output.add(geom);
                        else {
                            _warnings.add("GeomEntity has no GeomElement: " + geomEntity);
                            System.out.println(_warnings.get(_warnings.size()-1));
                        }

                        /*
                         * } else { //  Add a note that this was skipped to the
                         * warning list. StringBuffer msg = new StringBuffer();
                         * msg.append(" ");
                         * msg.append(RESOURCES.getString("skippedWarning"));
                         * msg.append(" "); msg.append(geomEntity.getHeader());
                         * msg.append(", blanked = ");
                         * msg.append(geomEntity.blankedStatus()); msg.append(",
                         * subordStatus = ");
                         * msg.append(geomEntity.subordStatus());
                         * _warnings.add(msg.toString());
                         }
                         */
                    }
                }
            }

            //  If all we have is a GeomList, just output that.
            if (output.size() == 1 && output.get(0) instanceof GeomList)
                output = (GeomList)output.get(0);

            //  Store some of the IGES meta-data in the output list's user data.
            String str = part.getStartSection().toString();
            if (!str.equals(""))
                output.putUserData("IGESStartSection", str);
            GlobalSection global = part.getGlobalSection();
            output.putUserData("IGESGrain", global.getGrainParameter());
            str = global.getProductName();
            if (!str.equals("")) {
                output.putUserData("IGESProductName", str);
                output.setName(str);
            }
            str = global.getFileName();
            if (!str.equals(""))
                output.putUserData("IGESFileName", str);
            str = global.getPreprocesorVersion();
            if (!str.equals(""))
                output.putUserData("IGESPreprocesorVersion", str);
            str = global.getDateTime();
            if (!str.equals(""))
                output.putUserData("IGESDateTime", str);
            str = global.getModDateTime();
            if (!str.equals(""))
                output.putUserData("IGESModDateTime", str);
            str = global.getAuthor();
            if (!str.equals("") && !str.equals("NULL"))
                output.putUserData("IGESAuthor", str);
            str = global.getOrganization();
            if (!str.equals("") && !str.equals("NULL"))
                output.putUserData("IGESOrganization", str);

            //  Replace any generic lists with more specific ones if possible.
            output = refineLists(output);

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return output;
    }

    /**
     * Recursively replace any generic lists with more specific ones if possible.
     */
    private GeometryList refineLists(GeometryList list) {

        //  First look for any sub-lists.
        int size = list.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = (GeomElement)list.get(i);
            if (element instanceof GeometryList) {
                GeometryList geomList = (GeometryList)element;
                GeometryList newList = refineLists(geomList);
                if (newList != geomList)
                    list.set(i, newList);
            }
        }

        //  Is this a "PointString"?
        boolean isType = true;
        for (int i = 0; i < size; ++i) {
            GeomElement element = (GeomElement)list.get(i);
            if (!(element instanceof GeomPoint)) {
                isType = false;
                break;
            }
        }
        if (isType) {
            PointString newList = PointString.newInstance(list.getName());
            newList.putAllUserData(list.getAllUserData());
            newList.addAll(list);
            return newList;
        }

        //  Is this a "PointArray"?
        isType = true;
        for (int i = 0; i < size; ++i) {
            GeomElement element = (GeomElement)list.get(i);
            if (!(element instanceof PointString)) {
                isType = false;
                break;
            }
        }
        if (isType) {
            PointArray newList = PointArray.newInstance(list.getName());
            newList.putAllUserData(list.getAllUserData());
            newList.addAll(list);
            return newList;
        }

        //  Is this a "PointComponent"?
        isType = true;
        for (int i = 0; i < size; ++i) {
            GeomElement element = (GeomElement)list.get(i);
            if (!(element instanceof PointArray)) {
                isType = false;
                break;
            }
        }
        if (isType) {
            PointComponent newList = PointComponent.newInstance(list.getName());
            newList.putAllUserData(list.getAllUserData());
            newList.addAll(list);
            return newList;
        }

        //  Is this a "PointVehicle"?
        isType = true;
        for (int i = 0; i < size; ++i) {
            GeomElement element = (GeomElement)list.get(i);
            if (!(element instanceof PointComponent)) {
                isType = false;
                break;
            }
        }
        if (isType) {
            PointVehicle newList = PointVehicle.newInstance(list.getName());
            newList.putAllUserData(list.getAllUserData());
            newList.addAll(list);
            return newList;
        }

        return list;
    }

    /**
     * Returns <code>true</code>. This reader can write some entity types to an IGES file.
     *
     * @return true
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Writes out a geometry file for the geometry contained in the supplied
     * {@link GeometryList} object. If the input geometry is not 3D, it will be forced to
     * be 3D (by padding if there are too few or by truncating additional dimensions).
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The {@link GeometryList} object containing the geometry to be
     *                   written out. May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  IGES files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        //  Create a PrintWriter that writes to the specified ASCII file.
        StackContext.enter();
        try (PrintWriter writer = new PrintWriter(outputFile, StandardCharsets.US_ASCII.name())) {

            //  Create an IGES Part to contain the exchange geometry.
            Part part = new Part();
            GlobalSection global = part.getGlobalSection();

            //  Set the units being used for this geometry.
            Unit<Length> unit = geometry.getUnit();
            global.setUnit(unit);

            //  Look for IGES meta-data in the input list.
            Object udata = geometry.getUserData("IGESStartSection");
            if (nonNull(udata))
                part.getStartSection().setStartSection((String)udata);
            udata = geometry.getUserData("IGESGrain");
            if (nonNull(udata)) {
                Parameter<Length> grain = (Parameter)udata;
                global.setGrain(grain.getValue(unit));
            }
            udata = geometry.getUserData("IGESProductName");
            if (nonNull(udata))
                global.setProductName((String)udata);
            udata = geometry.getUserData("IGESModDateTime");
            if (nonNull(udata))
                global.setModDateTime((String)udata);
            udata = geometry.getUserData("IGESAuthor");
            if (nonNull(udata))
                global.setAuthor((String)udata);
            udata = geometry.getUserData("IGESOrganization");
            if (nonNull(udata))
                global.setOrganization((String)udata);

            //  Set the file name.
            global.setFileName(outputFile.getName());

            //  Recurse through list and create entities for as much of the input geometry as possible.
            elementEntityMap = new FastMap();
            List<Entity> entities = part.getEntities();
            Parameter<Length> grain = global.getGrainParameter().times(100);
            Parameter<Length> tol = Parameter.valueOf(0.001, SI.METER).to(unit);
            if (grain.isGreaterThan(tol))
                tol = grain;
            addGeomList(tol, part, 1, entities, geometry);

            //  Write out the exchange file.
            part.write(writer);

            //  Store any error/warning messages.
            _warnings.addAll(part.getErrors());

            elementEntityMap.clear();

        } finally {
            StackContext.exit();

            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

    }

    /**
     * Recursively add a list of geometry objects to the Part's entity list.
     *
     * @param tol        The tolerance to use when converting curves to NURBS.
     * @param part       The part that the entities are associated with.
     * @param DEnum      The line count from the start of the Directory Entry Section for
     *                   the next entry (odd number).
     * @param entityList The list of entities being aggregated.
     * @param geometry   The list of GeomElement objects to be added to the entity list.
     * @return The next DEnum after adding this list of geometry elements.
     */
    private int addGeomList(Parameter<Length> tol, Part part, int DEnum, List<Entity> entityList, GeometryList geometry) {
        boolean elementAdded = false;       //  Prevent adding empty lists.
        FastTable<Entity> childEntities = FastTable.newInstance();

        int size = geometry.size();
        for (int i = 0; i < size; ++i) {
            GeomElement element = (GeomElement)geometry.get(i);

            //  First make sure this element has not already been added to the point.
            Entity e = elementEntityMap.get(element);
            if (nonNull(e)) {
                //  Element is already a part of this point, but make a part of this list.
                elementAdded = true;
                childEntities.add(e);
                continue;
            }

            //  Deal with curves that are subranged onto a surface as a special case.
            if (element instanceof SubrangeCurve) {
                SubrangeCurve subCrv = (SubrangeCurve)element;
                int numEntities = childEntities.size();
                if (subCrv.getChild() instanceof Surface) {
                    DEnum = handleCurveOnSurface(subCrv, DEnum, part, entityList, tol, childEntities);
                }
                if (numEntities != childEntities.size())
                    elementAdded = true;
                else
                    _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                            element.getClass().getName(), element.getID()));
                continue;
            }

            //  Deal with a subrange surface as a special case.
            if (element instanceof SubrangeSurface) {
                SubrangeSurface subSrf = (SubrangeSurface)element;
                int numEntities = childEntities.size();
                DEnum = handleSubrangeSurface(subSrf, DEnum, part, entityList, tol, childEntities);
                if (numEntities != childEntities.size())
                    elementAdded = true;
                else
                    _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                            element.getClass().getName(), element.getID()));
                continue;
            }

            //  Try to create an Entity for this element.
            e = EntityFactory.create(part, DEnum, element);
            if (nonNull(e)) {
                entityList.add(e);
                childEntities.add(e);
                elementEntityMap.put(element, e);
                elementAdded = true;
                DEnum += 2;             //  Each Directory Entry takes 2 lines.

            } else if (element instanceof GeometryList) {
                //  It's a list, so recurse down into it.
                int oldDEnum = DEnum;
                DEnum = addGeomList(tol, part, DEnum, entityList, (GeometryList)element);
                if (oldDEnum != DEnum) {
                    elementAdded = true;
                    childEntities.add(entityList.get(entityList.size() - 1));
                }

            } else
                _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                        element.getClass().getName(), element.getID()));

        }   //  Next i

        //  Create an entity for the overall list.
        if (elementAdded) {
            Entity e = EntityFactory.createList(part, DEnum, childEntities, geometry.getName());
            if (nonNull(e)) {
                entityList.add(e);
                DEnum += 2;             //  Each Directory Entry takes 2 lines.
            }
        }

        FastTable.recycle(childEntities);

        return DEnum;
    }

    /**
     * Handles the special case of a subrange curve on a surface.
     *
     * @param subCrv        The subrange curve on a surface to be transferred.
     * @param DEnum         The current DE pointer value.
     * @param part          The part this curve is associated with.
     * @param entityList    The list of all the entities being transferred.
     * @param tol           The tolerance to use when converting curves to NURBS.
     * @param childEntities The entities that are a part of the current geometry list
     *                      being transferred.
     * @return The DEnum incremented to account for the new entities added to the point
     *         by this method.
     */
    private int handleCurveOnSurface(SubrangeCurve subCrv, int DEnum, Part part, List<Entity> entityList, Parameter<Length> tol, FastTable<Entity> childEntities) {

        //  Try to create an Entity for the surface that the curve is on.
        Surface Ssrf = (Surface)subCrv.getChild();

        //  First see if the Ssrf has been added to the point already.
        Entity Se = elementEntityMap.get(Ssrf);
        if (isNull(Se)) {
            //  Entity has not already been stored, so create a new one.
            Se = EntityFactory.create(part, DEnum, Ssrf);
            if (nonNull(Se)) {
                entityList.add(Se);
                DEnum += 2;
            }
        }
        if (isNull(Se)) {
            _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                    Ssrf.getClass().getName(), Ssrf.getID()));
            return DEnum;     //  Failed to create the point.
        }
        int Sptr = Se.getDENum();

        //  Store a mapping of the Ssrf surface to the Se entity in case it is used again.
        elementEntityMap.put(Ssrf, Se);

        //  Now add the (u,v) parametric curve.
        NurbsCurve Bcrv = subCrv.getParPosition().toNurbs(tol);
        //  Must change to point units or scaling will be incorrect.
        Bcrv = changeCurveUnits(Bcrv, part.getGlobalSection().getUnit());
        int Bptr = DEnum;
        Entity Be = EntityFactory.create(part, Bptr, Bcrv);
        if (isNull(Be)) {
            _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                    Bcrv.getClass().getName(), Bcrv.getID()));
            return DEnum;   //  Failed to create point.
        }
        entityList.add(Be);
        elementEntityMap.put(Bcrv, Be);
        DEnum += 2;

        //  Now add the real-space curve.
        Curve Ccrv = subCrv.toNurbs(tol);   //  Convert to NURBS.
        int Cptr = DEnum;
        Entity Ce = EntityFactory.create(part, Cptr, Ccrv);
        if (nonNull(Ce)) {
            entityList.add(Ce);
            DEnum += 2;

            //  Finally, add the curve on surface entity.
            Entity crvOnSrfe = new Entity142_CurveOnSurface(part, DEnum, subCrv.getName(), Sptr, Bptr, Cptr);
            entityList.add(crvOnSrfe);
            DEnum += 2;
            if (nonNull(childEntities))
                childEntities.add(crvOnSrfe);
            elementEntityMap.put(subCrv, crvOnSrfe);

        } else
            _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                    Ccrv.getClass().getName(), Ccrv.getID()));

        return DEnum;
    }

    /**
     * Handles the special case of a subrange or trimmed surface.
     *
     * @param subSrf        The subrange surface being transferred.
     * @param DEnum         The current DE pointer value.
     * @param part          The part this curve is associated with.
     * @param entityList    The list of all the entities being transferred.
     * @param tol           The tolerance to use when converting curves to NURBS.
     * @param childEntities The entities that are a part of the current geometry list
     *                      being transferred.
     * @return The DEnum incremented to account for the new entities added to the point
     *         by this method.
     */
    private int handleSubrangeSurface(SubrangeSurface subSrf, int DEnum, Part part, List<Entity> entityList, Parameter<Length> tol, FastTable<Entity> childEntities) {
        //  Get the surface being trimmed.
        Surface Ssrf = (Surface)subSrf.getChild();

        //  First see if the Ssrf has been added to the point already.
        Entity Se = elementEntityMap.get(Ssrf);
        if (isNull(Se)) {
            //  Entity has not already been stored, so create a new one.
            Se = EntityFactory.create(part, DEnum, Ssrf);
            if (nonNull(Se)) {
                entityList.add(Se);
                DEnum += 2;
            }
        }
        if (isNull(Se)) {
            _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                    Ssrf.getClass().getName(), Ssrf.getID()));
            return DEnum;     //  Failed to create the point.
        }
        int Sptr = Se.getDENum();

        //  Get the parametric position surface (assumed to be a TFI surface -- what if it isn't?).
        Surface parSrf = subSrf.getParPosition();

        //  Get the 4 boundary parametric space curves oriented in a counter-clockwise direction.
        NurbsCurve[] parBoundaryCrvs = new NurbsCurve[4];
        parBoundaryCrvs[0] = parSrf.getS0Curve().toNurbs(tol);
        parBoundaryCrvs[1] = parSrf.getT1Curve().toNurbs(tol);
        parBoundaryCrvs[2] = parSrf.getS1Curve().reverse().toNurbs(tol);
        parBoundaryCrvs[3] = parSrf.getT0Curve().reverse().toNurbs(tol);

        //  Add each of the parametric boundary curves to the point.
        Unit<Length> unit = part.getGlobalSection().getUnit();
        FastTable<Integer> segDEPtrs = FastTable.newInstance();
        for (int i = 0; i < 4; ++i) {
            NurbsCurve crvi = parBoundaryCrvs[i];

            //  Must change to point units or scaling will be incorrect.
            crvi = changeCurveUnits(crvi, unit);

            Entity segE = EntityFactory.create(part, DEnum, crvi);
            if (isNull(segE)) {
                _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                        crvi.getClass().getName(), crvi.getID()));
                return DEnum;   //  Failed to create point.
            }
            entityList.add(segE);
            segDEPtrs.add(DEnum);
            DEnum += 2;
        }

        //  Add a composite curve made up of the parametric segments to the point.
        Entity Be = new Entity102_CompositeCurve(part, DEnum, null, segDEPtrs);
        entityList.add(Be);
        int Bptr = DEnum;
        DEnum += 2;
        FastTable.recycle(segDEPtrs);

        //  Create a real-space composite boundary curve to adhear to the contract for Entity 142.
        NurbsCurve[] boundaryCrvs = new NurbsCurve[4];
        boundaryCrvs[0] = subSrf.getS0Curve().toNurbs(tol);
        boundaryCrvs[1] = subSrf.getT1Curve().toNurbs(tol);
        boundaryCrvs[2] = subSrf.getS1Curve().reverse().toNurbs(tol);
        boundaryCrvs[3] = subSrf.getT0Curve().reverse().toNurbs(tol);
        NurbsCurve Ccrv = CurveUtils.connectCurves(boundaryCrvs);

        //  Add the composite boundary curve to the point.
        Entity Ce = EntityFactory.create(part, DEnum, Ccrv);
        if (isNull(Ce)) {
            _warnings.add(MessageFormat.format(RESOURCES.getString("igesSkipUnsupported"),
                    Ccrv.getClass().getName(), Ccrv.getID()));
            return DEnum;   //  Failed to create point.
        }
        entityList.add(Ce);
        int Cptr = DEnum;
        DEnum += 2;

        //  Add a curve on surface entity to the point using the composite curve.
        Entity Oe = new Entity142_CurveOnSurface(part, DEnum, null, Sptr, Bptr, Cptr);
        entityList.add(Oe);
        int PTO = DEnum;
        DEnum += 2;

        //  Finally, add the trimmed surface to the point.
        Entity trimSrfe = new Entity144_TrimmedSurface(part, DEnum, subSrf, Sptr, PTO);
        entityList.add(trimSrfe);
        DEnum += 2;
        if (nonNull(childEntities))
            childEntities.add(trimSrfe);
        elementEntityMap.put(subSrf, trimSrfe);

        return DEnum;
    }

    /**
     * Return a new curve with the same numerical values as the 1st curve, but with the
     * units changed to those specified.
     *
     * @param crv  The curve to have the units changed.
     * @param unit The new unit for the output curve.
     * @return A curve with the same numerical values as the "crv", but with the units
     *         changed to "unit".
     */
    private static NurbsCurve changeCurveUnits(NurbsCurve crv, Unit<Length> unit) {
        StackContext.enter();
        try {

            List<ControlPoint> oldCPList = crv.getControlPoints();
            FastTable<ControlPoint> newCPList = FastTable.newInstance();
            for (ControlPoint cp : oldCPList) {
                Point p = cp.getPoint();
                double w = cp.getWeight();
                Point pnew = Point.valueOf(p.getValue(0), p.getValue(1), 0, unit);
                newCPList.add(ControlPoint.valueOf(pnew, w));
            }
            crv = BasicNurbsCurve.newInstance(newCPList, crv.getKnotVector());
            return StackContext.outerCopy((BasicNurbsCurve)crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * This method always returns <code>true</code> as IGES files do encode the units that
     * are being used though the list of available units is limited.
     *
     * @return this implementation always returns true
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return true;
    }
}
