/*
 *   XGSSGeomReader -- A class that can read and write an XGSS formatted geometry file.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.GeomElement;
import geomss.geom.GeomList;
import geomss.geom.GeomXMLBinding;
import geomss.geom.GeometryList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipException;
import javolution.context.StackContext;
import javolution.util.FastMap;
import javolution.xml.XMLObjectReader;
import javolution.xml.XMLObjectWriter;
import javolution.xml.XMLReferenceResolver;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomReader} for reading geometry from an XGSS formatted, GZIP compressed, XML
 * geometry file. XGSS is the native file format for GeomSS.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 20, 2013
 * @version September 9, 2016
 */
public class XGSSGeomReader extends AbstractGeomReader {

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("xgssDescription");

    /**
     * The preferred file extension for files of this reader's type.
     */
    public static final String EXTENSION = "xgss";

    /**
     * The XML file tag used for all GeomElement objects.
     */
    private static final String GEOMETRYTAG = "Geometry";

    /**
     * The XML file tag used for all non-geometry related workspace items.
     */
    private static final String WORKSPACETAG = "Workspace";

    /**
     * The user data key used to store the variable name for geometry stored in the
     * workspace.
     */
    private static final String VARNAMEKEY = "GeomSSVarName";

    /**
     * The XML binding to use in reading/writing an XGSS formatted file.
     */
    private static final GeomXMLBinding BINDING = new GeomXMLBinding();

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * This method always returns <code>true</code> as XGSS files do encode the units that
     * are being used for each geometry element.
     *
     * @return This method always returns true.
     */
    @Override
    public boolean isUnitAware() {
        return true;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file format is definitely recognized by this reader.
     *         GeomReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        if (inputFile.isFile() && inputFile.exists()) {
            String name = inputFile.getName();
            name = name.toLowerCase().trim();
            if (name.endsWith(".xgss"))
                response = YES;
        }

        return response;
    }

    /**
     * Reads in an XGSS geometry file from the specified input file and returns a
     * {@link GeometryList} object that contains the geometry from the file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link GeometryList} object containing the geometry read in from the
     *         file. If the file has no readable geometry in it, then this list will have
     *         no elements in it (will have a size() of 0).
     * @throws IOException If there is a problem reading the specified file.
     */
    @Override
    public GeometryList read(File inputFile) throws IOException {
        requireNonNull(inputFile);

        //  XGSS XML files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        try (GZIPInputStream is = new GZIPInputStream(new FileInputStream(inputFile))) {
            
            //  Create an XML object reader.
            XMLObjectReader reader = XMLObjectReader.newInstance(is);

            //  Set the GeomSS XML binding to get consistant formatting.
            reader.setBinding(BINDING);

            //  Use a reference resolver.
            reader.setReferenceResolver(new XMLReferenceResolver());

            //  Read in the XML file and return a top level Geometry List.
            GeometryList<?, GeomElement> geom = reader.read(GEOMETRYTAG);

            //  For this version of "read" strip out any workspace variable name references
            //  in the geometry.
            for (GeomElement elem : geom) {
                elem.removeUserData(VARNAMEKEY);
            }

            return geom;

        } catch (ZipException e) {
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("xgssWrongFormat"), inputFile.getName()), e);

        } catch (XMLStreamException e) {
            throw new IOException(e);
            
        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }
    }

    /**
     * Returns <code>true</code>. This reader can write all geometry element types to an
     * XGSS file.
     *
     * @return This method always returns true.
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Writes out a geometry file for the geometry contained in the supplied
     * {@link GeometryList} object.
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The {@link GeometryList} object containing the geometry to be
     *                   written out. May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  XGSS XML files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        StackContext.enter();
        try (GZIPOutputStream os = new GZIPOutputStream(new FileOutputStream(outputFile))) {
            //  Create an XML object writer.
            XMLObjectWriter writer = XMLObjectWriter.newInstance(os);

            // Set the GeomSS XML binding to get consistant formatting.  Use a tabs for spacing.
            writer.setIndentation("\t");
            writer.setBinding(BINDING);

            //  Use a reference resolver to prevent duplicate objects in the file.
            writer.setReferenceResolver(new XMLReferenceResolver());

            //  Write out the top level geometry list to the file.
            writer.write(geometry, GEOMETRYTAG);

            writer.close();

        } catch (XMLStreamException e) {
            throw new IOException(e);

        } finally {
            StackContext.exit();
            
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }
    }

    /**
     * Writes out an XGSS geometry file for the and non-geometry workspace variables
     * contained in the supplied maps.
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   A map identifying geometry elements by their associated variable
     *                   names. May not be null.
     * @param vars       A map identifying non-geometry related workspace variables by
     *                   their variable names. May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    public void write(File outputFile, Map<String, GeomElement> geometry, Map<String, Object> vars) throws IOException {
        requireNonNull(outputFile);
        requireNonNull(vars);
        _warnings.clear();
        if (geometry.isEmpty()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
        }

        StackContext.enter();
        try (GZIPOutputStream os = new GZIPOutputStream(new FileOutputStream(outputFile))) {
            //  Convert the geometry map into a list of geometry with the variable names stored
            //  in the user data.
            GeomList geom = GeomList.newInstance();
            for (String varName : geometry.keySet()) {
                GeomElement elem = geometry.get(varName);
                elem.putUserData(VARNAMEKEY, varName);
                geom.add(elem);
            }

            //  Create an XML object writer.
            XMLObjectWriter writer = XMLObjectWriter.newInstance(os);

            // Set the GeomSS XML binding to get consistant formatting.  Use a tabs for spacing.
            writer.setIndentation("\t");
            writer.setBinding(BINDING);

            //  Use a reference resolver to prevent duplicate objects in the file.
            writer.setReferenceResolver(new XMLReferenceResolver());

            //  Write out the top level geometry list to the file.
            writer.write(geom, GEOMETRYTAG);

            //  Write the Map of workspace variables to the file.
            if (!vars.isEmpty())
                writer.write(vars, WORKSPACETAG);

            writer.close();

        } catch (XMLStreamException e) {
            throw new IOException(e);

        } finally {
            //  Remove the variable names added to the user data above.
            for (String varName : geometry.keySet()) {
                GeomElement elem = geometry.get(varName);
                elem.removeUserData(VARNAMEKEY);
            }
            StackContext.exit();
        }
    }

    /**
     * Reads in an XGSS geometry + workspace file from the specified input file and
     * returns a Map object that contains the geometry and workspace variables from the
     * file with the variable names as the keys.
     *
     * @param inputFile The input file containing the geometry + workspace to be read in.
     * @return A Map object containing the geometry and workspace variables read in from
     *         the file with the variable names as the keys. If the file has no readable
     *         geometry in it, then this map will have no contents.
     * @throws IOException If there is a problem reading the specified file.
     */
    public Map<String, Object> readWorkspace(File inputFile) throws IOException {

        try (GZIPInputStream is = new GZIPInputStream(new FileInputStream(inputFile))) {
            //  Create an XML object reader.
            XMLObjectReader reader = XMLObjectReader.newInstance(is);

            //  Set the GeomSS XML binding to get consistant formatting.
            reader.setBinding(BINDING);

            //  Use a reference resolver.
            reader.setReferenceResolver(new XMLReferenceResolver());

            //  Read in the geometry and return a top level Geometry List.
            GeometryList<?, GeomElement> geom = reader.read(GEOMETRYTAG);

            //  Read in the optional non-geometry workspace variables.
            Map<String, Object> vars = null;
            try {
                vars = reader.read(WORKSPACETAG);
            } catch (XMLStreamException e) { /* ignore (means workspace is not present) */ }

            //  Close the reader.
            reader.close();

            //  For this version of "read" add named elements to the workspace by their
            //  variable name.
            FastMap<String, Object> workspace = FastMap.newInstance();
            for (GeomElement elem : geom) {
                String varName = (String)elem.getUserData(VARNAMEKEY);
                if (nonNull(varName))
                    workspace.put(varName, elem);
            }

            //  Add any non-geometry variables to the output workspace.
            if (nonNull(vars) && !vars.isEmpty())
                workspace.putAll(vars);

            //  If there were no named variable names defined, put in the entire geometry
            //  list with the name "geom".
            if (workspace.isEmpty())
                workspace.put("geom", geom);

            return (Map<String, Object>)workspace;

        } catch (ZipException e) {
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("xgssWrongFormat"), inputFile.getName()), e);

        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

}
