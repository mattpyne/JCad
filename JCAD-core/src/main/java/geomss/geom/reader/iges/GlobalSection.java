/*
*   GlobalSection  -- Encapsulates the IGES Global Section.
*
*   Copyright (C) 2010-2016, Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2.1 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
*
*   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
*/
package geomss.geom.reader.iges;

import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

/**
 * The GlobalSection class encapsulates the IGES Global Section.  This holds
 * the "global variables" for the file.  This class handles initialization,
 * reading, access, and dumping of the values.
 *
 *  <p>  Modified by:  Joseph A. Huwaldt </p>
 *
 * @author JDN, Version 1.0
 * @version April 7, 2016
 */
public class GlobalSection {
    
    private char   Delim = ',';         // Parameter delimiter character
    private char   Term = ';';          // Record delimiter character
    private String PName = "";          // Product identification from sending system
    private String FName = "";          // File name
    private String SysID = Constants.RESOURCES.getString("systemID");       // System ID
    private String TranVrsn = Constants.RESOURCES.getString("tranVersion"); // Preprocessor version
    private int    IntSize = 32;        // Number of binary bits for integer representation
    private int    FloatExp = 38;       // Max power of ten for float
    private int    FloatMant = 6;       // Number of significant digits for float
    private int    DoubleExp = 308;     // Max power of ten for double
    private int    DoubleMant = 15;     // Number of significant digits for double
    private String DestName = "";       // Product identification for the receiving system
    private double ModelScale = 1;      // Model space scale
    private int    UnitFlg = 1;         // Unit flag (unit code)
    private String UnitType = "IN";     // Units name
    private int    LineWeights = 1;     // Max number of line weight gradations
    private double LineWidth = 0.01;    // Width of max line weight in units
    private String DateTime = "";       // Date and time of file generation
    private double Grain = 0.01;        // Min granularity of model in units
    private double MaxValue = 0;        // Max coordinate value in units
    private String Author = "";         // Name of author
    private String Organization = "";   // Author's organization
    private int    SpecVrsn = 11;       // Version of specification for this file
    private int    DraftStd = 0;        // Drafting standard of this file
    private String ModDate = "";        // Date and time of last modification to the model.
    private String AppProtocol = "";    // Application protocol, application subset, MIL-spec, etc.
    
    private int    start = 0;           // Current string index for parsing
    private String GlobalText = "";     // Global data as a string

    /**
     * Default constructor.
     */
    public GlobalSection() {
    }

    /**
     * Return char parameter from input string. The input string must be in Hollerith form
     * (1H-), as per the IGES specification, and delimited by the global delimiter
     * character.
     *
     * @param s input string from IGES file.
     * @return single character
     */
    private char getChar(String s) {
        String sResult = "";

        while (s.charAt(start) == ' ')
            start++;

        //  Deal with hitting the terminator right away.
        int c = s.charAt(start);
        if (c == Delim || c == Term) {
            if (c == Delim)
                ++start;        //  Advance only if it is the delimiter that is hit.
            return '\0';
        }

        if ((s.charAt(start) != Delim) && (s.charAt(start) != Term) && Character.isDigit(s.charAt(start))) {
            // Should be in form ##Hchars
            int sStart = s.indexOf('H', start);
            String str = s.substring(start, sStart).trim();
            int iLen = Integer.parseInt(str);
            int newstart = sStart + 2 + iLen;

            if (newstart < 0)
                sResult = "";
            else {
                sResult = s.substring(sStart + 1, newstart - 1);
                start = newstart - 1;
            }

        } else {
            while ((s.charAt(start) != Delim) && (s.charAt(start) != Term)) {
                sResult = sResult + s.charAt(start);

                if (s.charAt(start + 1) == Term)
                    break;
                else
                    start++;
            }

            // Not a proper string, so nullify it
            sResult = "";
        }

        if (s.charAt(start) != Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getChar - \"" + sResult + "\"");
        }

        if (sResult.length() == 0)
            return '\0';
        return sResult.charAt(0);
    }

    /**
     * Return string parameter from input string. The input may either be in Hollerith
     * form (nH...), as per the IGES specification delimited by the global delimiter
     * character or whatever characters are found between the delimiter character are
     * returned. If the String equals "NULL", then an empty String is returned.
     *
     * @param s input string from IGES file.
     * @return string stripped of Hollerith prefix, or empty string if the input string is
     *         invalid.
     */
    private String getString(String s) {

        while (s.charAt(start) == ' ')
            start++;

        //  Deal with hitting the terminator right away.
        int c = s.charAt(start);
        if (c == Delim || c == Term) {
            if (c == Delim)
                ++start;        //  Advance only if it is the delimiter that is hit.
            return "";
        }

        // Pull out everything between the delimiters.
        int newstart = start + 1;
        c = s.charAt(newstart);
        while (c != Delim && c != Term) {
            ++newstart;
            c = s.charAt(newstart);
        }
        String sResult = s.substring(start, newstart);

        //  Now convert from Hollerith form (if necessary).
        try {
            // Should be in form ##Hchars
            int sStart = s.indexOf('H', start);

            int iLen = Integer.parseInt((s.substring(start, sStart)).trim());
            int end = sStart + 2 + iLen;

            if (end < 0)
                sResult = "";
            else {
                sResult = s.substring(sStart + 1, end - 1);
                if (end - 1 > newstart)
                    newstart = end - 1;
            }

        } catch (Exception e) {
            //  Do nothing. Keep the entire string.
        }

        start = newstart;
        if (s.charAt(start) != Term)
            start++;

        if ("NULL".equals(sResult))
            sResult = "";

        if (Constants.DEBUG)
            System.out.println("getString - \"" + sResult + "\"");

        return sResult;
    }

    /**
     * Return real parameter from input string. The input string can be of single
     * (1.0e+010) or double (1.0d+010) precision, and delimited by the global delimiter
     * character. The field is blank, then Double.NaN is returned.
     *
     * @param s input string from IGES file.
     * @return real number
     */
    private double getReal(String s) {
        String sResult = "";

        while (s.charAt(start) == ' ')
            start++;

        //  Deal with hitting the terminator right away.
        int c = s.charAt(start);
        if (c == Delim || c == Term) {
            if (c == Delim)
                ++start;        //  Advance only if it is the delimiter that is hit.
            return Double.NaN;
        }

        while ((s.charAt(start) != Delim) && (s.charAt(start) != Term)) {
            sResult = sResult + s.charAt(start);

            if (s.charAt(start + 1) == Term)
                break;
            else
                start++;
        }

        if (s.charAt(start) != Term)
            start++;

        sResult = sResult.trim();

        if (Constants.DEBUG)
            System.out.println("getReal - \"" + sResult + "\"");

        return Constants.toDouble(sResult);
    }

    /**
     * Return integer parameter from input string. The input string can be represent
     * either a standard integer, or a float value (e.g. 1.000e+010), and delimited by the
     * global delimiter character. If the field is blank, a 0 is returned.
     *
     * @param s input string from IGES file.
     * @return integer number
     */
    private int getInt(String s) {
        String sResult = "";

        while (s.charAt(start) == ' ')
            start++;

        //  Deal with hitting the terminator right away.
        int c = s.charAt(start);
        if (c == Delim || c == Term) {
            if (c == Delim)
                ++start;        //  Advance only if it is the delimiter that is hit.
            return 0;
        }

        while ((s.charAt(start) != Delim) && (s.charAt(start) != Term)) {
            sResult = sResult + s.charAt(start);

            if (s.charAt(start + 1) == Term)
                break;
            else
                start++;
        }

        if (s.charAt(start) != Term)
            start++;

        sResult = sResult.trim();

        if (Constants.DEBUG) {
            System.out.println("getInt - \"" + sResult + "\"");
        }

        return Constants.toInt(sResult);
    }

    /**
     * Return parameter delimiter character. This indicates which character is used to
     * separate parameter values in the Global and Parameter Data sections. The default
     * value is “comma.”
     *
     * @return parameter delimiter character
     */
    public char getDelim() {
        return Delim;
    }

    /**
     * Return parameter terminator character. This indicates which character denotes the
     * end of parameters in the Global Section and in each Parameter Data Section entry.
     * The default value is “semicolon.”
     *
     * @return parameter terminator character
     */
    public char getTerm() {
        return Term;
    }

    /**
     * Return the IGES product identification from the sending system for this Part. This
     * contains the name or identifier which is used by the sender reference this product.
     * 
     * @return The IGES product identification of the sending system.
     */
    public String getProductName() {
        return PName;
    }

    /**
     * Set the IGES product identification from the sending system for this Part. This
     * contains the name or identifier which is used by the sender reference this product.
     *
     * @param productName The name the sender uses to reference this product (Part).
     */
    public void setProductName(String productName) {
        PName = productName;
    }

    /**
     * Return the IGES file name record which contains the name of the exchange file.
     * 
     * @return The IGES file name
     */
    public String getFileName() {
        return FName;
    }

    /**
     * Set the file name of the exchange file.
     * 
     * @param fileName The file name for the exchange file.
     */
    public void setFileName(String fileName) {
        FName = fileName;
    }

    /**
     * Return the Native System ID which identifies the native system software which
     * created the native format file used to generate this exchange file.
     * 
     * @return The Native System ID
     */
    public String getSystemID() {
        return SysID;
    }

    /**
     * Set the Native System ID which identifies the native system software which created
     * the native format file used to generate this exchange file.
     * 
     * @param sysID The Native System ID to set.
     */
    public void setSystemID(String sysID) {
        SysID = sysID;
    }

    /**
     * Return the IGES file preprocessor version. This uniquely identifies the version or
     * release date of the preprocessor which created this file.
     * @return The IGES file preprocessor version
     */
    public String getPreprocesorVersion() {
        return TranVrsn;
    }

    /**
     * Set the preprocessor version. This uniquely identifies the version or release date
     * of the preprocessor which created this file.
     * 
     * @param version The IGES file preprocessor version to set.
     */
    public void setPreprocessorVersion(String version) {
        TranVrsn = version;
    }

    /**
     * Return the model length unit's used in the IGES file.
     *
     * @return The length units used in this point.
     */
    public Unit<Length> getUnit() {
        Unit unit = NonSI.INCH;
        switch (UnitFlg) {
            case 2:
                unit = SI.MILLIMETER;
                break;
            case 3:
                //  The unit is identified by symbol, not by code.
                switch (UnitType) {
                    case "MM":
                        unit = SI.MILLIMETER;
                        break;
                    case "FT":
                        unit = NonSI.FOOT;
                        break;
                    case "MI":
                        unit = NonSI.MILE;
                        break;
                    case "M":
                        unit = SI.METER;
                        break;
                    case "KM":
                        unit = SI.KILOMETER;
                        break;
                    case "MIL":
                        unit = NonSI.INCH.times(0.001);
                        break;
                    case "UM":
                        unit = SI.METER.times(1.e-6);
                        break;
                    case "CM":
                        unit = SI.CENTIMETER;
                        break;
                    case "UIN":
                        unit = NonSI.INCH.times(1.e-6);
                        break;
                }
                break;
            case 4:
                unit = NonSI.FOOT;
                break;
            case 5:
                unit = NonSI.MILE;
                break;
            case 6:
                unit = SI.METER;
                break;
            case 7:
                unit = SI.KILOMETER;
                break;
            case 8:
                unit = NonSI.INCH.times(0.001);
                break;
            case 9:
                unit = SI.METER.times(1.e-6);
                break;
            case 10:
                unit = SI.CENTIMETER;
                break;
            case 11:
                unit = NonSI.INCH.times(1.e-6);
                break;
        }
        return unit;
    }

    /**
     * Set the model length unit's used in the IGES file.
     * 
     * @param unit The model length unit to save the data in.
     */
    public void setUnit(Unit<Length> unit) {
        //  Turn the input unit into a unit flag and type value.
        if (SI.MILLIMETER.equals(unit)) {
            UnitFlg = 2;
            UnitType = "MM";

        } else if (NonSI.FOOT.equals(unit)) {
            UnitFlg = 4;
            UnitType = "FT";

        } else if (NonSI.MILE.equals(unit)) {
            UnitFlg = 5;
            UnitType = "MI";

        } else if (SI.METER.equals(unit)) {
            UnitFlg = 6;
            UnitType = "M";

        } else if (SI.KILOMETER.equals(unit)) {
            UnitFlg = 7;
            UnitType = "KM";

        } else if (NonSI.INCH.times(0.001).equals(unit)) {
            UnitFlg = 8;
            UnitType = "MIL";

        } else if (SI.METER.times(1.e-6).equals(unit)) {
            UnitFlg = 9;
            UnitType = "UM";

        } else if (SI.CENTIMETER.equals(unit)) {
            UnitFlg = 10;
            UnitType = "CM";

        } else if (NonSI.INCH.times(1.e-6).equals(unit)) {
            UnitFlg = 11;
            UnitType = "UIN";

        } else {
            //  Default to inches.
            UnitFlg = 1;
            UnitType = "IN";
        }
    }

    /**
     * Return number of line weights.
     *
     * @return number of line weights
     */
    public int getLineWeights() {
        return LineWeights;
    }

    /**
     * Return maximum line width.
     *
     * @return maximum line width
     */
    public double getLineWidth() {
        return LineWidth;
    }

    /**
     * Return the date and time of IGES file generation.
     * 
     * @return The date and time of IGES file generation
     */
    public String getDateTime() {
        return DateTime;
    }

    /**
     * Return Minimum User-Intended Resolution Granularity value. This specifies the
     * smallest distance between coordinates, in model-space units, that the receiving
     * system shall consider as discernible.
     *
     * @return the granularity value
     */
    public double getGrain() {
        return Grain;
    }

    /**
     * Return Minimum User-Intended Resolution Granularity value. This specifies the
     * smallest distance between coordinates, in model-space units, that the receiving
     * system shall consider as discernible.
     *
     * @return the granularity value as a Parameter object.
     */
    public Parameter<Length> getGrainParameter() {
        return Parameter.valueOf(Grain, getUnit());
    }

    /**
     * Set the Minimum User-Intended Resolution or Granularity value. This specifies the
     * smallest distance between coordinates, in model-space units, that the receiving
     * system shall consider as discernible.
     *
     * @param value The granularity value
     */
    public void setGrain(double value) {
        Grain = value;
    }

    /**
     * Return the date and time of the last modification of the model in this exchange
     * file.
     * @return The date and time of the last modification
     */
    public String getModDateTime() {
        return ModDate;
    }

    /**
    *  Set the date and time of the last modification of the model in this exchange file.
    * 
     * @param modDate The date and time of the last modification as a String.
    */
    public void setModDateTime(String modDate) {
        ModDate = modDate;
    }
    
    /**
    * Return the name of the person who created this exchange file.
    *
    * @return the author identification
    */
    public String getAuthor() {
        return Author;
    }
    
    /**
    *  Set the name of the person who created this exchange file.
    * 
     * @param author The name of the author of the file.
    */
    public void setAuthor(String author) {
        Author = author;
    }
    
    /**
    * Return the name of the organization or group with whom the author is associated.
    *
    * @return the author's organization identification
    */
    public String getOrganization() {
        return Organization;
    }
    
    /**
    *  Set the name of the organization or group with whom the author is associated.
    * 
     * @param organization The name of the organization the author is associated with.
    */
    public void setOrganization(String organization) {
        Organization = organization;
    }
    
    /**
    *  Return the version of the IGES Specification to which the data in this file complies.
    * 
     * @return The version of the IGES Specification to which the file complies.
    */
    public int getSpecVersion() {
        return SpecVrsn;
    }
    
    
    /**
     * Read the Global Section from the input file. This method can handle missing or
     * incorrect fields.
     *
     * @param in input file
     * @throws IOException if there is any problem reading the IGES file.
     */
    public void read(RandomAccessFile in) throws IOException {
        boolean ok = false;

        StringBuilder buffer = new StringBuilder();
        while (true) {
            long curloc = in.getFilePointer();
            String line = Constants.myReadLine(in);
            if (line.charAt(Constants.SECTID_COL) == 'G') {
                //  Deal with non-standard blank spaces between PDID_COL and SECTID_COL.
                int end = Constants.SECTID_COL;
                String rhs = line.substring(Constants.PDID_COL,Constants.SECTID_COL).trim();
                if (rhs.length() < 1)
                    end = Constants.PDID_COL;
                buffer.append( line.substring(0,end) );
            } else {
                in.seek(curloc);
                ok = true;
                break;
            }
        }
        String s = buffer.toString();
        
        if (ok) {
            Delim        = getChar(s); if (Delim == '\0') Delim = ',';
            Term         = getChar(s); if (Term  == '\0') Term  = ';';
            PName        = getString(s);
            FName        = getString(s);
            SysID        = getString(s);
            TranVrsn     = getString(s);
            IntSize      = getInt(s);
            FloatExp     = getInt(s);
            FloatMant    = getInt(s);
            DoubleExp    = getInt(s);
            DoubleMant   = getInt(s);
            DestName     = getString(s);
            ModelScale   = getReal(s);
            UnitFlg      = getInt(s);
            UnitType     = getString(s);
            LineWeights  = getInt(s);
            LineWidth    = getReal(s);
            DateTime     = getString(s);
            Grain        = getReal(s);
            MaxValue     = getReal(s);
            if (Double.isNaN(MaxValue))
                MaxValue = 0;
            Author       = getString(s);
            Organization = getString(s);
            SpecVrsn     = getInt(s);
            DraftStd     = getInt(s);
            ModDate      = getString(s);
            AppProtocol  = getString(s);
        }

        StringBuilder outStr = new StringBuilder();
        outStr.append("Delim        = \""); outStr.append(Delim);       outStr.append("\"\n");
        outStr.append("Term         = \""); outStr.append(Term);        outStr.append("\"\n");
        outStr.append("PName        = \""); outStr.append(PName);       outStr.append("\"\n");
        outStr.append("FName        = \""); outStr.append(FName);       outStr.append("\"\n");
        outStr.append("SysID        = \""); outStr.append(SysID);       outStr.append("\"\n");
        outStr.append("TranVrsn     = \""); outStr.append(TranVrsn);    outStr.append("\"\n");
        outStr.append("IntSize      = ");   outStr.append(IntSize);     outStr.append("\n");
        outStr.append("FloatExp     = ");   outStr.append(FloatExp);    outStr.append("\n");
        outStr.append("FloatMant    = ");   outStr.append(FloatMant);   outStr.append("\n");
        outStr.append("DoubleExp    = ");   outStr.append(DoubleExp);   outStr.append("\n");
        outStr.append("DoubleMant   = ");   outStr.append(DoubleMant);  outStr.append("\n");
        outStr.append("DestName     = \""); outStr.append(DestName);    outStr.append("\"\n");
        outStr.append("ModelScale   = ");   outStr.append(ModelScale);  outStr.append("\n");
        outStr.append("UnitFlg      = ");   outStr.append(UnitFlg);     outStr.append("\n");
        outStr.append("UnitType     = \""); outStr.append(UnitType);    outStr.append("\"\n");
        outStr.append("LineWeights  = ");   outStr.append(LineWeights); outStr.append("\n");
        outStr.append("LineWidth    = ");   outStr.append(LineWidth);   outStr.append("\n");
        outStr.append("DateTime     = \""); outStr.append(DateTime);    outStr.append("\"\n");
        outStr.append("Grain        = ");   outStr.append(Grain);       outStr.append("\n");
        outStr.append("MaxValue     = ");   outStr.append(MaxValue);    outStr.append("\n");
        outStr.append("Author       = \""); outStr.append(Author);      outStr.append("\"\n");
        outStr.append("Organization = \""); outStr.append(Organization);outStr.append("\"\n");
        outStr.append("SpecVrsn     = ");   outStr.append(SpecVrsn);    outStr.append("\n");
        outStr.append("DraftStd     = ");   outStr.append(DraftStd);    outStr.append("\n");
        outStr.append("ModDate      = \""); outStr.append(ModDate);     outStr.append("\n");
        outStr.append("AppProtocol  = \""); outStr.append(AppProtocol); outStr.append("\"\n");

        GlobalText = outStr.toString();
    }

    /**
     * Write the Global Section to the specified writer.
     *
     * @param writer The PrintWriter to write the Global Section to.
     * @return The number of lines written out to the global section.
     * @throws IOException if there is any problem writing the section.
     */
    public int write(PrintWriter writer) throws IOException {
       
        //  Construct the current date/time for output.
        SimpleDateFormat dformat = new SimpleDateFormat("yyyyMMdd.HHmmss");
        DateTime = dformat.format(new Date());
        
        //  Construct a long string with all the section's parameters in it.
        StringBuilder buffer = new StringBuilder();
        buffer.append(makeHollerith(Delim));        buffer.append(Delim);
        buffer.append(makeHollerith(Term));         buffer.append(Delim);
        buffer.append(makeHollerith(PName));        buffer.append(Delim);
        buffer.append(makeHollerith(FName));        buffer.append(Delim);
        buffer.append(makeHollerith(SysID));        buffer.append(Delim);
        buffer.append(makeHollerith(TranVrsn));     buffer.append(Delim);
        buffer.append(IntSize);                     buffer.append(Delim);
        buffer.append(FloatExp);                    buffer.append(Delim);
        buffer.append(FloatMant);                   buffer.append(Delim);
        buffer.append(DoubleExp);                   buffer.append(Delim);
        buffer.append(DoubleMant);                  buffer.append(Delim);
        buffer.append(makeHollerith(DestName));     buffer.append(Delim);
        buffer.append(ModelScale);                  buffer.append(Delim);
        buffer.append(UnitFlg);                     buffer.append(Delim);
        buffer.append(makeHollerith(UnitType));     buffer.append(Delim);
        buffer.append(LineWeights);                 buffer.append(Delim);
        buffer.append(LineWidth);                   buffer.append(Delim);
        buffer.append(makeHollerith(DateTime));     buffer.append(Delim);
        buffer.append(Grain);                       buffer.append(Delim);
        buffer.append(MaxValue);                    buffer.append(Delim);
        buffer.append(makeHollerith(Author));       buffer.append(Delim);
        buffer.append(makeHollerith(Organization)); buffer.append(Delim);
        buffer.append(SpecVrsn);                    buffer.append(Delim);
        buffer.append(DraftStd);                    buffer.append(Delim);
        buffer.append(makeHollerith(ModDate));      buffer.append(Delim);
        buffer.append(makeHollerith(AppProtocol));  buffer.append(Term);

        //  Now write out the string of data to form the global section.
        int index = Constants.writeSection(writer, 1, "", 'G', buffer);

        return index - 1;
    }

    /**
     * Take the input and turn it into a Hollerith String.
     */
    private static String makeHollerith(String input) {
        if (input.length() == 0)
            return input;
        StringBuilder buffer = new StringBuilder();
        buffer.append(input.length());
        buffer.append("H");
        buffer.append(input);
        return buffer.toString();
    }

    /**
     * Take the input and turn it into a Hollerith String.
     */
    private static String makeHollerith(char input) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(1);
        buffer.append("H");
        buffer.append(input);
        return buffer.toString();
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        return GlobalText;
    }
}
