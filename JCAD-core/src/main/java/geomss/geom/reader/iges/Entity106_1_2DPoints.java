/*
*   Entity106_1_2DPoints  -- Entity that represents a set of of 2D points.
*
*   Copyright (C) 2010-2013, Joseph A. Huwaldt
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2.1 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
*
*   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
**/
package geomss.geom.reader.iges;


/**
* <p><b><i>COPIOUS DATA ENTITY - DATA POINTS</i></b> - This entity defines
* a set of 2D coordinate points which may be
* isolated or used as a component of a Subfigure Entity.  Form 1 indicates
* that all the points share a constant depth (Zt) value (are 2D).</p>
*
* <p>
* This entity, when read from an IGES file, is converted to a
* list of points (a PointString object).  This entity type can not
* be written out to an IGES file.
*</p>
* 
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
* @author Joseph A. Huwaldt     Date:  March 10, 2013
* @version March 14, 2013
* @see Entity106_CopiousData
*/
public class Entity106_1_2DPoints extends Entity106_CopiousData {

    /**
    * Default constructor.
    *
    * @param p part to which this entity is contained
    * @param de Directory Entry for this entity
    */
    public Entity106_1_2DPoints(Part p, DirEntry de) {
        super(p,de);

        if (Constants.DEBUG) {
            System.out.println("Entity106_1 constructor called");
        }
    }

    /**
    *  Returns a short String describing this Entity object's type.
    **/
    @Override
    public String getTypeString() {
        return "Entity106_1 - Copious Data - 2D Points";
    }
    
}
