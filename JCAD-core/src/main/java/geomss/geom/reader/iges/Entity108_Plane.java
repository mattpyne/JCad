/*
 *   Entity108_Plane  -- Entity that represents a plane in 3D space.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import javax.measure.quantity.Dimensionless;

/**
 * <b><i>PLANE ENTITY</i></b> - This entity represents a plane.
 * 
 * <p>
 * When reading in this entity, all plane parameters are stored in the user data with the
 * prefix "IGES_108_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: September 6, 2010
 * @version September 13, 2016
 */
public abstract class Entity108_Plane extends GeomSSEntity {

    protected double A, B, C, D;        //  Plane coefficients.
    protected int ptr;                  //  Pointer boundary entity or zero.
    protected double xt, yt, zt;        //  Coordinates of location point for display symbol.
    protected double symbolSize = 0.01; //  Size parameter for the display symbol.

    protected GeomPlane plane;          //  The GeomSS plane.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity108_Plane(Part p, DirEntry de) {
        super(p, de);
    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     * - The Label Display Pointer shall be 0.
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // DE Label Display shall be 0
        if (DE.getLblDsp() != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("labelDisplay"), DE.getLblDsp());
            addErrorMessage(getWarningString(msg));
        }
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        //  Read in the plane coefficients.
        A = getReal(s);
        B = getReal(s);
        C = getReal(s);
        D = getReal(s);

        //  Get the pointer to a bounding curve.
        ptr = getInt(s);

        //  Get the location for the display symbol (in the plane).
        xt = getReal(s);
        yt = getReal(s);
        zt = getReal(s);

        //  Get the size of the display symbol.
        symbolSize = getReal(s);

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() {
        //  Create the GeomSS plane.  The "D" value is redundant.  Using xt,yt,zt to create
        //  the plane as this contains more information than D alone.
        Vector<Dimensionless> nhat = Vector.valueOf(A, B, C).toUnitVector();
        Point refPoint = Point.valueOf(xt, yt, zt, Constants.unit);
        plane = Plane.valueOf(nhat, refPoint);
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_108_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_108_A", A);
        element.putUserData("IGES_108_B", B);
        element.putUserData("IGES_108_C", C);
        element.putUserData("IGES_108_D", D);
        element.putUserData("IGES_108_X", xt);
        element.putUserData("IGES_108_Y", yt);
        element.putUserData("IGES_108_Z", zt);
        element.putUserData("IGES_108_SIZE", symbolSize);
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return plane;
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        outStr.append("A = ");      outStr.append(A);   outStr.append("\n");
        outStr.append("B = ");      outStr.append(B);   outStr.append("\n");
        outStr.append("C = ");      outStr.append(C);   outStr.append("\n");
        outStr.append("D = ");      outStr.append(D);   outStr.append("\n");
        outStr.append("ptr = ");    outStr.append(ptr); outStr.append("\n");
        outStr.append("x = ");      outStr.append(xt);  outStr.append("\n");
        outStr.append("y = ");      outStr.append(yt);  outStr.append("\n");
        outStr.append("z = ");      outStr.append(zt);  outStr.append("\n");
        outStr.append("size = ");   outStr.append(symbolSize);  outStr.append("\n");

        return outStr.toString();
    }

}
