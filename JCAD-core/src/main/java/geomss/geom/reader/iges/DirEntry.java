/*
 *   DirEntry  -- Represents an IGES directory entry.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

/**
 * The DirEntry class represents the IGES Directory Entry. This structure contains general
 * attributes for an entity, such as type, form, color, level, etc. The DirEntry is read
 * from the file, and then a new Entity is instantiated and read depending on the type and
 * form in the DirEntry.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class DirEntry {

    private int Type;               // Identifies the entity type
    private int PDNum;              // First line of parameter data record for the entity
    private int Font = 0;           // Line font pattern or negated pointer to Entity304
    private int Level = 0;          // Number of the level for this entity, or negated pointer to Entity406_1
    private int View = 0;           // Pointer to Entity410 or Entity402_3/4 or 0
    private int Matrix = 0;         // Pointer to Entity124 or 0
    private int LblDsp = 0;         // Pointer to Entity402_5 or 0
    private String Status = "00000000"; // Status values: aabbccdd where
    //                                      aa = Blank Status
    //                                      bb = Subordinate Entity Switch
    //                                      cc = Entity Use Flag
    //                                      dd = Hierarchy
    // See IGES spec for values
    private int DENum;              // Line count from start of Directory Entry Section (odd)
    private int Weight = 0;         // System display thickness 0->GlobalSection.LineWeights
    private int Color = 0;          // Color number or negated pointer to Entity314
    private int PDCnt;              // Number of lines in parameter data record for this entity
    private int Form;               // Form number
    private String Label = "";      // Up to eight alphanumeric characters (right justified)
    private int Subscr = 0;         // 1 to 8 digit unsigned number associated with the label

    /**
     * Default constructor.
     */
    public DirEntry() { }

    /**
     * Create a DirEntry from the specified information. Make sure and call "setPDNumber"
     * before writing an IGES file with this directory entry.
     *
     * @param type   The entity type code.
     * @param form   The entity type form code or 0 for the default.
     * @param DEnum  The line count from the start of the Directory Entry Section for this
     *               entry (odd number).
     * @param matrix An Entity124 DE number or 0.
     * @param label  A label for this parameter or <code>null</code> or "" for none.
     */
    public DirEntry(int type, int form, int DEnum, int matrix, String label) {
        Type = type;
        Form = form;
        DENum = DEnum;
        Matrix = matrix;

        if (label == null)
            label = "";
        else if (label.length() > 8)
            label = label.substring(0, 8);
        Label = label;
    }

    /**
     * Copy constructor.
     *
     * @param de input Directory Entry
     */
    public DirEntry(DirEntry de) {
        Type = de.Type;
        PDNum = de.PDNum;
        Font = de.Font;
        Level = de.Level;
        View = de.View;
        Matrix = de.Matrix;
        LblDsp = de.LblDsp;
        Status = de.Status;
        DENum = de.DENum;
        Weight = de.Weight;
        Color = de.Color;
        PDCnt = de.PDCnt;
        Form = de.Form;
        Label = de.Label;
        Subscr = de.Subscr;
    }

    /**
     * Read the Directory Entry from a file. This method can handle blank or non-existent
     * fields.
     *
     * @param in input file
     * @return true if the read was successful, false if the Directory Entry was
     *         incomplete
     * @throws IOException if there is any problem reading the directory entry file.
     */
    public boolean read(RandomAccessFile in) throws IOException {

        long curloc = in.getFilePointer();
        String line1 = Constants.myReadLine(in);

        if (line1.charAt(72) != 'D') {
            in.seek(curloc);
            return false;
        }

        String line2 = Constants.myReadLine(in);

        for (int i = 0; i < 10; i++) {
            String field = (line1.substring(i * 8, (i + 1) * 8)).trim();

			switch(i) {
				case 0: Type   = Constants.toInt(field);                       break;
				case 1: PDNum  = Constants.toInt(field);                       break;
				case 2:                                                        break; /* Structure */
				case 3: Font   = Constants.toInt(field);                       break;
				case 4: Level  = Constants.toInt(field);                       break;
				case 5: View   = Constants.toInt(field);                       break;
				case 6: Matrix = Constants.toInt(field);                       break;
				case 7: LblDsp = Constants.toInt(field);                       break;
				case 8: Status = field;                                        break;
				case 9: DENum  = Constants.toInt((field.substring(1)).trim()); break;
            }
        }

        for (int i = 0; i < 10; i++) {
            String field = (line2.substring(i * 8, (i + 1) * 8)).trim();

			switch(i) {
				case 0:                                  break; /* Type */
				case 1: Weight = Constants.toInt(field); break;
				case 2: Color  = Constants.toInt(field); break;
				case 3: PDCnt  = Constants.toInt(field); break;
				case 4: Form   = Constants.toInt(field); break;
				case 5:                                  break; /* Reserved */
				case 6:                                  break; /* Reserved */
				case 7: Label  = field;                  break;
				case 8: Subscr = Constants.toInt(field); break;
				case 9:                                  break; /* DENUM + 1 */
            }
        }

        return true;
    }

    /**
     * Write this directory entry out to the exchanged file. Make sure that this directory
     * entry has the correct parameter data index number (PDNum) and line count (PDCnt)
     * before calling this method. This typically means that the parameter data for all
     * the entities must be written first to a temporary buffer so that the PDNum & PDCnt
     * values can be determined for each, then the directory entries are written and
     * finally the buffered parameter data is written.
     *
     * @param writer The PrintWriter to write the directory entry to.
     * @throws IOException if there is any problem writing the entry.
     */
    public void write(PrintWriter writer) throws IOException {

        //  Write out the 1st line.
        writer.printf(Constants.US, "%8d", Type);
        writer.printf(Constants.US, "%8d", PDNum);
        writer.printf(Constants.US, "%8d", 0);       //  Structure
        writer.printf(Constants.US, "%8d", Font);
        writer.printf(Constants.US, "%8d", Level);
        writer.printf(Constants.US, "%8d", View);
        writer.printf(Constants.US, "%8d", Matrix);
        writer.printf(Constants.US, "%8d", LblDsp);
        writer.print(Status);
        writer.print("D");
        writer.println(Constants.makeSequenceNumber(DENum));

        //  Write out the 2nd line.
        writer.printf(Constants.US, "%8d", Type);
        writer.printf(Constants.US, "%8d", Weight);
        writer.printf(Constants.US, "%8d", Color);
        writer.printf(Constants.US, "%8d", PDCnt);
        writer.printf(Constants.US, "%8d", Form);
        writer.print("        ");       //  Reserved
        writer.print("        ");       //  Reserved
        writer.print(Constants.padLeft(Label, ' ', 8));
        writer.printf(Constants.US, "%8d", Subscr);
        writer.print("D");
        writer.println(Constants.makeSequenceNumber(DENum + 1));

    }

    /**
     * Return entity type.
     *
     * @return entity type
     */
    public int getType() {
        return Type;
    }

    /**
     * Return entity form.
     *
     * @return entity form
     */
    public int getForm() {
        return Form;
    }

    /**
     * Return Parameter Data line count for this entity.
     *
     * @return Parameter Data line count for this entity
     */
    public int getPDCnt() {
        return PDCnt;
    }

    /**
     * Return Directory Entry number for this entity.
     *
     * @return DE number
     */
    public int getDENum() {
        return DENum;
    }

    /**
     * Return Parameter Data number for this entity.
     *
     * @return PD number
     */
    public int getPDNum() {
        return PDNum;
    }

    /**
     * Return color of this entity.
     *
     * @return color of this entity
     */
    public int getColor() {
        return Color;
    }

    /**
     * Return line font of this entity.
     *
     * @return line font of this entity
     */
    public int getFont() {
        return Font;
    }

    /**
     * Return line weight of this entity.
     *
     * @return line weight of this entity
     */
    public int getWeight() {
        return Weight;
    }

    /**
     * Return level of this entity.
     *
     * @return level of this entity
     */
    public int getLevel() {
        return Level;
    }

    /**
     * Return view pointer of this entity.
     *
     * @return view pointer of this entity
     */
    public int getView() {
        return View;
    }

    /**
     * Return matrix pointer of this entity.
     *
     * @return matrix pointer of this entity
     */
    public int getMatrix() {
        return Matrix;
    }

    /**
     * Return status field of this entity. If the field is less than eight characters, the
     * string will be padded on the left with zeros.
     *
     * @return status field of this entity
     */
    public String getStatus() {
        String outStr = Status;
        int i;

        if (Status.length() < 8) {
            for (i = 0; i < (8 - Status.length()); i++)
                outStr = "0" + outStr;
        }

        return outStr;
    }

    /**
     * Return label display pointer of this entity.
     *
     * @return label display pointer of this entity
     */
    public int getLblDsp() {
        return LblDsp;
    }

    /**
     * Return the label for this entity. Will return an empty String if there is no label.
     *
     * @return The label for this entity.
     */
    public String getLabel() {
        return Label;
    }

    /**
     * Set the first line of parameter data record for the entity as well as number of
     * lines in parameter data record for this entity.
     *
     * @param PDnum The first line of parameter data record for the entity.
     * @param PDcnt The number of lines in the parameter data for this entity.
     */
    public void setPDNumber(int PDnum, int PDcnt) {
        PDNum = PDnum;
        PDCnt = PDcnt;
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder("Directory Entry:\n");

		outStr.append("Type =        ");	outStr.append(Type  );	outStr.append("\n");
		outStr.append("PDNum =       ");	outStr.append(PDNum );	outStr.append("\n");
		outStr.append("Font =        ");	outStr.append(Font  );	outStr.append("\n");
		outStr.append("Level =       ");	outStr.append(Level );	outStr.append("\n");
		outStr.append("View =        ");	outStr.append(View  );	outStr.append("\n");
		outStr.append("Matrix =      ");	outStr.append(Matrix);	outStr.append("\n");
		outStr.append("LblDsp =      ");	outStr.append(LblDsp);	outStr.append("\n");
		outStr.append("Status =      \"");	outStr.append(Status);	outStr.append("\"\n");
		outStr.append("DENum =       ");	outStr.append(DENum );	outStr.append("\n");
		outStr.append("Weight =      ");	outStr.append(Weight);	outStr.append("\n");
		outStr.append("Color =       ");	outStr.append(Color );	outStr.append("\n");
		outStr.append("PDCnt =       ");	outStr.append(PDCnt );	outStr.append("\n");
		outStr.append("Form =        ");	outStr.append(Form  );	outStr.append("\n");
		outStr.append("Label =       \"");	outStr.append(Label );	outStr.append("\"\n");
		outStr.append("Subscr =      ");	outStr.append(Subscr);

        return outStr.toString();
    }
}
