/*
 *   Entity404_Drawing  -- This Entity defines a drawing space coordinate system.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * <b><i>DRAWING ENTITY</i></b> - This entity defines drawing space coordinate system. A
 * View Entity may also be used in conjunction with this entity to position entities in
 * the defined drawing space. The Drawing Entity specifies a drawing as a collection of
 * annotation entities i.e., any entity with its Entity Use Flag set to 01) defined in
 * drawing space, and views (i.e., projections of model space data in view space).
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Entity404_Drawing extends Entity {

    protected int n;                // Number of views
    protected List<View> views;     // List of views
    protected int m;                // Number of annotation entities
    protected List<Integer> dptr;   // List of annotation entities

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity404_Drawing(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity404 constructor called");
        }

        views = new ArrayList();
        dptr = new ArrayList();
    }

    /**
     * Checks to see if the entity should be drawn. The following restrictions are
     * imposed:
     *
     * - Number of view pointers shall be 1 - Number of annotation entities shall be 0 -
     * Number of associativity pointers shall be 0 - Number of property pointers shall be
     * 0, 1, 2, or 3
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // Number of view pointers shall be 1
        if (n != 1) {
            String msg = MessageFormat.format(RESOURCES.getString("numViewPointers"), n);
            addErrorMessage(getWarningString(msg));
        }

        // Number of annotation entities shall be 0
        if (m != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("numAnnEntities"), m);
            addErrorMessage(getWarningString(msg));
        }

        // Number of associativity pointers shall be 0
        int num_assoc = associativities.size();
        if (num_assoc != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("numAssocPointers"), num_assoc);
            addErrorMessage(getWarningString(msg));
        }

        // Number of property pointers shall be 0
        int num_props = properties.size();
        if ((num_props < 0) || (num_props > 3)) {
            String msg = MessageFormat.format(RESOURCES.getString("numPropPointers"), num_props);
            addErrorMessage(getWarningString(msg));
        }

    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity404.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        n = getInt(s);

        for (int i = 0; i < n; i++) {
            views.add(new View(getView(s, getDirectoryEntry().getForm())));
        }

        m = getInt(s);

        for (int i = 0; i < m; i++) {
            dptr.add(getInt(s));
        }

        super.read_additional();
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity404 - Drawing";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        outStr.append("n = ");  outStr.append(n);   outStr.append("\n");

        if (n > 0) {
            for (int i = 0; i < n; i++) {
                outStr.append("vptr(");     outStr.append(i);   outStr.append(") =\n");
                outStr.append(views.get(i).toString());         outStr.append("\n");
            }
            outStr.append("\n");
        }

        if (m > 0) {
            for (int i = 0; i < m; i++) {
                outStr.append("dptr(");     outStr.append(i);   outStr.append(") =\n");
                outStr.append(dptr.get(i).intValue());          outStr.append("\n");
            }
            outStr.append("\n");
        }

        return outStr.toString();
    }

}
