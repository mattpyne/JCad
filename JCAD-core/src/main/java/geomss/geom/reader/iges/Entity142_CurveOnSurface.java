/*
 *   Entity142_CurveOnSurface  -- An entity representing a Curve On a Parametric Surface.
 *
 *   Copyright (Ccrv) 2013-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.ControlPoint;
import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.List;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.util.FastTable;

/**
 * <b><i>CURVE ON A PARAMETRIC SURFACE ENTITY</i></b> - This entity represents a curve
 * associated with a surface and identifies the curve as lying on the surface.
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a SubrangeCurve curve. This
 * entity type can be written out to an IGES file. When reading in this entity, the IGES
 * parameters are stored in the user data with the prefix "IGES_142_" followed by the
 * parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 13, 2013
 * @version April 10, 2016
 */
public class Entity142_CurveOnSurface extends GeomSSEntity {

    private int Crtn = 0;   //  Indicates the way the curve on the surface has been created:
                            //      0 = Unspecified, 1 = Projection of a givencurve on the surface,
                            //      2 = Intersection of two surfaces,
                            //      3 = Isoparametric curve, i.e., either a u- parametric or a v- parametric curve.
    private int Sptr;       //  Pointer to the DE of the surface on which the curve lies
    private int Bptr;       //  Pointer to the DE of the entity that contains the definition of
                            //      the curve Bcrv in the parametric space (u, v) of the surface Ssrf
    private int Cptr;       //  Pointer to the DE of the curve Ccrv
    private int Pref = 1;   //  Indicates preferred representation in the sending system:
                            //      0 = Unspecified, 1 = S o B is preferred, 2 = C is preferred,
                            //      3 = C and S o B are equally preferred

    private GeomList<SubrangeCurve> curves; //  The list of GeomSS curves this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity142_CurveOnSurface(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity142 constructor called");
        }

    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part to which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param name  The name to assign to the IGES entity or <code>nulL</code> for none.
     * @param Sptr  Pointer to the DE of the surface on which the curve lies.
     * @param Bptr  Pointer to the DE of the entity that contains the definition of the
     *              curve Bcrv in the parametric space (u, v) of the surface Ssrf
     * @param Cptr  Pointer to the DE of the curve Ccrv
     */
    public Entity142_CurveOnSurface(Part part, int DEnum, String name, int Sptr, int Bptr, int Cptr) {
        super(part, new DirEntry(142, 0, DEnum, 0, name));

        this.Sptr = Sptr;
        this.Bptr = Bptr;
        this.Cptr = Cptr;

    }

    /**
     * Checks to see if the entity is correct.
     */
    @Override
    public void check() { }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity142.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        Crtn = getInt(s);   //  Indicates the way the curve on the surface has been created
        Sptr = getInt(s);   //  Pointer to the DE of the surface on which the curve lies
        Bptr = getInt(s);   //  Pointer to the DE of the entity that contains the definition of the curve Bcrv in the parametric space (u, v) of the surface Ssrf
        Cptr = getInt(s);   //  Pointer to the DE of the curve Ccrv
        Pref = getInt(s);   //  Indicates preferred representation in the sending system

        super.read_additional();
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_142_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_142_CRTN", Crtn);
        element.putUserData("IGES_142_PREF", Pref);
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        Part part = getPart();
        Parameter<Length> tol = Parameter.valueOf(Constants.Grain, Constants.unit);
        curves = GeomList.newInstance();

        //  Get the surface the curve is on.
        Surface Ssrf;
        Entity entity = part.getEntity(Sptr);
        if (entity instanceof GeomSSEntity) {
            GeomSSEntity geomEntity = (GeomSSEntity)entity;
            geomEntity.setUsedInList(true); //  Indicate that entity is used by this association.
            Ssrf = (Surface)geomEntity.getGeomElement(GTransform.IDENTITY);
            if (Ssrf == null)
                return;
        } else
            return;

        //  Extract the original parametric bounds of the surface.
        double sStart = (Double)Ssrf.getUserData("IGES_U0");
        double tStart = (Double)Ssrf.getUserData("IGES_V0");
        double sEnd = (Double)Ssrf.getUserData("IGES_U1");
        double tEnd = (Double)Ssrf.getUserData("IGES_V1");

        //  Get the parametric space definition curve.
        GeomList<NurbsCurve> Bcrvs = GeomList.newInstance();
        NurbsCurve Bcc;
        entity = part.getEntity(Bptr);
        if (entity instanceof GeomSSEntity) {
            GeomSSEntity geomEntity = (GeomSSEntity)entity;
            geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
            Curve crv = (Curve)geomEntity.getGeomElement(GTransform.IDENTITY);
            if (crv == null)
                return;

            //  Convert the B curve to NURBS
            Bcc = crv.toNurbs(tol);

            //  Change B units to METER to be consistant with the requirements of SubrangeCurve.
            Bcc = changeCurveUnits(Bcc, SI.METER);

            //  Scale the B curve to the correct surface parametric range.
            Bcc = scaleParCurve(sStart, sEnd, tStart, tEnd, Bcc);

            //  Convert B from 3D to 2D by dropping the Z dimension.
            Bcc = Bcc.toDimension(2);

            //  Store the original B curve.
            Bcc.putUserData("IGES_142_B", crv);

            if (entity instanceof Entity102_CompositeCurve) {
                //  We have a composite B curve.  We want the segments.

                //  Extract the segments of the composite curve.
                GeomList<GeomElement> segs = (GeomList)crv.getUserData("IGES_102_CCSegs");
                curves.putUserData("IGES_142_BSegs", segs);

                //  Convert the segments to NurbsCurves
                GeomList<NurbsCurve> crvSegs = convert2NurbsSegs(segs, tol);

                //  Convert all the segments to properly scaled subrange parameter curves.
                for (NurbsCurve seg : crvSegs) {
                    //  Change Bcrv units to METER to be consistant with the requirements of SubrangeCurve.
                    NurbsCurve nseg = changeCurveUnits(seg, SI.METER);

                    //  Scale the B curve to the correct surface parametric range.
                    nseg = scaleParCurve(sStart, sEnd, tStart, tEnd, nseg);

                    //  Convert B from 3D to 2D by dropping the Z dimension.
                    nseg = nseg.toDimension(2);

                    nseg.putUserData("IGES_142_BSeg", seg);

                    //  Save the segment in the Bcrvs list.
                    Bcrvs.add(nseg);
                }

            } else {
                //  Just a single curve segment.
                Bcrvs.add(Bcc);
                Bcc = null;
            }

        } else
            return;

        //  Get the model space curve coincident with the curve on the surface.
        Curve Ccrv = null;
        entity = part.getEntity(Cptr);
        if (entity instanceof GeomSSEntity) {
            GeomSSEntity geomEntity = (GeomSSEntity)entity;
            geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
            Ccrv = (Curve)geomEntity.getGeomElement(GTransform.IDENTITY);
        }

        //  Create the curves.
        for (NurbsCurve B : Bcrvs) {
            SubrangeCurve curve = SubrangeCurve.newInstance(Ssrf, B);
            if (Ccrv != null)
                curve.putUserData("IGES_142_C", Ccrv);
            curves.add(curve);
        }
        if (Ccrv != null)
            curves.putUserData("IGES_142_C", Ccrv);
        if (Bcc != null)
            //  This will only be added if B was originally a composite curve.
            curves.putUserData("IGES_142_Bcc", Bcc);
    }

    /**
     * Converts all the non-degenerate Curve elements in the specified geometry list into
     * NurbsCurves.
     *
     * @param crvs The list of geometry elements to be converted.
     * @param tol  The tolerance for the conversion to NURBS curves.
     * @return A list of non-degenerate NURBS curve versions of each element in "crvs".
     */
    private static GeomList<NurbsCurve> convert2NurbsSegs(GeomList<GeomElement> crvs, Parameter<Length> tol) {
        GeomList<NurbsCurve> nurbsSegs = GeomList.newInstance();
        for (GeomElement elem : crvs) {
            if (elem instanceof Curve) {
                NurbsCurve nurbs = ((Curve)elem).toNurbs(tol);
                nurbsSegs.add(nurbs);
            } else if (elem instanceof GeomPoint) {
                NurbsCurve nurbs = CurveFactory.createPoint(1, (GeomPoint)elem);
                nurbsSegs.add(nurbs);
            }
        }
        return nurbsSegs;
    }

    /**
     * Return a new curve with the same numerical values as the 1st curve, but with the
     * units changed to those specified.
     *
     * @param crv  The curve to have the units changed.
     * @param unit The new unit for the output curve.
     * @return A curve with the same numerical values as the "crv", but with the units
     *         changed to "unit".
     */
    private static NurbsCurve changeCurveUnits(NurbsCurve crv, Unit<Length> unit) {
        StackContext.enter();
        try {

            List<ControlPoint> oldCPList = crv.getControlPoints();
            FastTable<ControlPoint> newCPList = FastTable.newInstance();
            for (ControlPoint cp : oldCPList) {
                Point p = cp.getPoint();
                double w = cp.getWeight();
                Point pnew = Point.valueOf(p.getValue(0), p.getValue(1), 0, unit);
                newCPList.add(ControlPoint.valueOf(pnew, w));
            }
            crv = BasicNurbsCurve.newInstance(newCPList, crv.getKnotVector());
            return StackContext.outerCopy((BasicNurbsCurve)crv);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Scales an input parametric curve who's values range from sStart,tStart to sEnd,tEnd
     * to a new curve who's values range from 0,0 to 1,1.
     *
     * @param sStart The parametric S starting position on the parent surface.
     * @param sEnd   The parametric T starting position on the parent surface.
     * @param tStart The parametric S ending position on the parent surface.
     * @param tEnd   The parametric T ending position on the parent surface.
     * @param crv    The curve, in the range, sStart,tStart to sEnd,tEnd to be scaled.
     * @return A new curve that is scaled off the input curve that is in the range 0,0 to
     *         1,1.
     */
    private static NurbsCurve scaleParCurve(double sStart, double sEnd, double tStart, double tEnd, NurbsCurve crv) {
        double mS = 1. / (sEnd - sStart);
        double mT = 1. / (tEnd - tStart);
        GTransform Tt = GTransform.newTranslation(-sStart, -tStart, 0); //  Translate sStart,tStart to 0,0.
        GTransform Ts = GTransform.valueOf(mS, mT, 1, 1);               //  Scale S & T to the right range.
        GTransform T = Ts.times(Tt);
        crv = crv.getTransformed(T).copyToReal();

        //  Are there small round-off errors causing the parameters to be out of bounds?
        final double tol = 1e-4;
        double min = crv.getLimitPoint(0, false, tol/10).getValue(0);
        double max = crv.getLimitPoint(0, true, tol/10).getValue(0);
        if (min < -tol || max > 1 + tol)
            throw new IllegalArgumentException("Parametric curve out of range in Entity142: u0 = " + min + ", u1 = " + max);
        sStart = 0;
        sEnd = 1;
        if (min < 0)
            sStart = min;
        if (max > 1)
            sEnd = max;
        mS = 1. / (sEnd - sStart);

        min = crv.getLimitPoint(1, false, tol/10).getValue(1);
        max = crv.getLimitPoint(1, true, tol/10).getValue(1);
        if (min < -tol || max > 1 + tol)
            throw new IllegalArgumentException("Parametric curve out of range in Entity142: v0 = " + min + ", v1 = " + max);
        tStart = 0;
        tEnd = 1;
        if (min < 0)
            tStart = min;
        if (max > 1)
            tEnd = max;
        mT = 1. / (tEnd - tStart);
        if (mS != 1 || mT != 1) {
            Tt = GTransform.newTranslation(-sStart, -tStart, 0);
            Ts = GTransform.valueOf(mS, mT, 1, 1);
            T = Ts.times(Tt);
            crv = crv.getTransformed(T).copyToReal();
        }

        return crv;
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        if (curves.size() == 1)
            return curves.get(0);
        return curves;
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entity object's parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {
        //  Build up the parameter data string.
        StringBuilder buffer = new StringBuilder();
        buffer.append(142);     buffer.append(Constants.Delim);
        buffer.append(Crtn);    buffer.append(Constants.Delim);
        buffer.append(Sptr);    buffer.append(Constants.Delim);
        buffer.append(Bptr);    buffer.append(Constants.Delim);
        buffer.append(Cptr);    buffer.append(Constants.Delim);
        buffer.append(Pref);    buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity142 - Curve on Parametric Surface";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        outStr.append("Crtn  = ");  outStr.append(Crtn);    outStr.append("\n");
        outStr.append("Sptr  = ");  outStr.append(Sptr);    outStr.append("\n");
        outStr.append("Bptr  = ");  outStr.append(Sptr);    outStr.append("\n");
        outStr.append("Cptr  = ");  outStr.append(Sptr);    outStr.append("\n");
        outStr.append("Pref  = ");  outStr.append(Sptr);    outStr.append("\n");

        return outStr.toString();
    }
}
