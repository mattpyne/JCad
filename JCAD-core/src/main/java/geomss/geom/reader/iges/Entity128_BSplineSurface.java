/*
 *   Entity128_BSplineSurface  -- An entity representing a Rational B-Spline Surface.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomElement;
import geomss.geom.Point;
import geomss.geom.Transformable;
import geomss.geom.nurbs.*;
import jahuwaldt.tools.math.MathTools;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.util.List;

/**
 * <p>
 * <b><i>RATIONAL B-SPLINE SURFACE ENTITY</i></b> - This entity represents a Rational
 * B-Spline Surface. This represents a parametric point obtained by dividing two
 * summations involving weights (which are real numbers), the control points, and B-Spline
 * basis functions.</p>
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a NURBS surface. This entity
 * type <b>can</b> be written out to an IGES file. When reading in this entity, all IGES
 * parameters are stored in the user data with the prefix "IGES_128_" followed by the
 * parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: August 23, 2010
 * @version April 7, 2016
 */
public class Entity128_BSplineSurface extends GeomSSEntity {

    private int prop1 = 0;  //  0=closed in S-direction, 1=not closed
    private int prop2 = 0;  //  0=closed in T-direction, 1=not closed
    private int prop3 = 0;  //  0=rational, 1=polynomial
    private int prop4 = 0;  //  0=non-periodic (clamped) in S-direction, 1=periodic (un-clamped).
    private int prop5 = 0;  //  0=non-periodic (clamped) in T-direction, 1=periodic (un-clamped).
    private double[] sKnots = null; //  Knot sequence in S-direction
    private double[] tKnots = null; //  Knot sequence in T-direction
    private double uSstart = 0;//   Starting parameter value in S-direction.
    private double uSend = 1;   //  Ending parameter value in S-direction.
    private double uTstart = 0;//   Starting parameter value in T-direction.
    private double uTend = 1;   //  Ending parameter value in T-direction.

    private NurbsSurface surface;   //  The GeomSS surface this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity128_BSplineSurface(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity128 constructor called");
        }

    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part in which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS geometry to return an Entity for.
     */
    public Entity128_BSplineSurface(Part part, int DEnum, NurbsSurface geom) {
        super(part, new DirEntry(128, 0, DEnum, 0, geom.getName()));
        surface = geom;

        //  Is the surface closed in S-direction?
        prop1 = 1;
        int numCols = surface.getNumberOfColumns();
        for (int i = 0; i < numCols; ++i) {
            NurbsCurve curve = surface.getSCurve(i);
            if (!curve.getRealPoint(0).distance(curve.getRealPoint(1)).isApproxZero()) {
                prop1 = 0;
                break;
            }
        }

        //  Is the surface closed in T-direction?
        prop2 = 1;
        int numRows = surface.getNumberOfRows();
        for (int i = 0; i < numRows; ++i) {
            NurbsCurve curve = surface.getTCurve(i);
            if (!curve.getRealPoint(0).distance(curve.getRealPoint(1)).isApproxZero()) {
                prop2 = 0;
                break;
            }
        }

        //  Is the surface rational (prop3 = 0) or polynomial (prop3 = 1)?
        ControlPointNet cpNet = surface.getControlPoints();
        double w = cpNet.get(0, 0).getWeight();
        prop3 = 1;
        for (List<ControlPoint> column : cpNet) {
            for (ControlPoint cp : column) {
                if (Math.abs(cp.getWeight() - w) > MathTools.EPS) {
                    prop3 = 0;
                    break;
                }
            }
        }

        //  Store the knot vectors of this surface.
        int K1 = cpNet.getNumberOfRows() - 1;
        int K2 = cpNet.getNumberOfColumns() - 1;
        KnotVector kv = surface.getSKnotVector();
        int M1 = kv.getDegree();
        int n = kv.length();
        sKnots = new double[n];
        for (int i = 0; i < n; ++i)
            sKnots[i] = kv.getValue(i);

        kv = surface.getTKnotVector();
        int M2 = kv.getDegree();
        n = kv.length();
        tKnots = new double[n];
        for (int i = 0; i < n; ++i)
            tKnots[i] = kv.getValue(i);

    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     *
     * - The Label Display Pointer shall be 0
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // DE LblDsp shall be 0
        if (DE.getLblDsp() != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("labelDisplay"), DE.getLblDsp());
            addErrorMessage(getWarningString(msg));
        }

    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG)
            System.out.println("Entity128.read() called for " + getHeader());

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG)
            System.out.println("PD String = \"" + s + "\"");

        int K1 = getInt(s);     //  Number of control points.
        int K2 = getInt(s);     //  Number of control points.
        int M1 = getInt(s);     //  Degree of basis functions.
        int M2 = getInt(s);     //  Degree of basis functions.
        prop1 = getInt(s);
        prop2 = getInt(s);
        prop3 = getInt(s);
        prop4 = getInt(s);
        prop5 = getInt(s);

        int n = K1 + 1 + M1 + 1;        //  Number of knotsU.
        sKnots = new double[n];
        for (int i = 0; i < n; ++i) {
            double u = getReal(s);
            sKnots[i] = u;
        }

        n = K2 + 1 + M2 + 1;        //  Number of knotsU.
        tKnots = new double[n];
        for (int i = 0; i < n; ++i) {
            double u = getReal(s);
            tKnots[i] = u;
        }

        double[][] weights = new double[K2 + 1][K1 + 1];
        for (int i = 0; i <= K2; ++i) {
            for (int j = 0; j <= K1; ++j) {
                double w = getReal(s);
                weights[i][j] = w;
            }
        }

        Point[][] cps = new Point[K2 + 1][K1 + 1];
        for (int i = 0; i <= K2; ++i) {
            for (int j = 0; j <= K1; ++j) {
                cps[i][j] = getPoint3(s);
            }
        }

        uSstart = getReal(s);
        uSend = getReal(s);
        uTstart = getReal(s);
        uTend = getReal(s);

        //  Make sure all the knotsU read in are in-range.
        n = sKnots.length;
        for (int i = 0; i < n; ++i) {
            double kv = sKnots[i];
            if (kv < uSstart)
                sKnots[i] = uSstart;
            else if (kv > uSend)
                sKnots[i] = uSend;
        }
        n = tKnots.length;
        for (int i = 0; i < n; ++i) {
            double kv = tKnots[i];
            if (kv < uTstart)
                tKnots[i] = uTstart;
            else if (kv > uTend)
                tKnots[i] = uTend;
        }

        //  Scale the knotsU into the range 0-1.
        double[] scaledSKnots = sKnots;
        if (Math.abs(uSstart) > MathTools.EPS || Math.abs(uSend - 1.0) > MathTools.EPS) {
            n = sKnots.length;
            scaledSKnots = new double[n];
            double m = 1. / (uSend - uSstart);
            double b = -m * uSstart;
            for (int i = 0; i < n; ++i) {
                double kv = sKnots[i];
                kv = scaleKnot(m, b, kv);
                scaledSKnots[i] = kv;
            }
        }
        double[] scaledTKnots = tKnots;
        if (Math.abs(uTstart) > MathTools.EPS || Math.abs(uTend - 1.0) > MathTools.EPS) {
            n = tKnots.length;
            scaledTKnots = new double[n];
            double m = 1. / (uTend - uTstart);
            double b = -m * uTstart;
            for (int i = 0; i < n; ++i) {
                double kv = tKnots[i];
                kv = scaleKnot(m, b, kv);
                scaledTKnots[i] = kv;
            }
        }

        //  Create the knot vector.
        KnotVector kvS = KnotVector.newInstance(M1, scaledSKnots);
        KnotVector kvT = KnotVector.newInstance(M2, scaledTKnots);

        //  Create the control point network.
        ControlPoint[][] controlPoints = new ControlPoint[K2 + 1][K1 + 1];
        for (int i = 0; i <= K2; ++i) {
            for (int j = 0; j <= K1; ++j) {
                double weight = weights[i][j];
                Point pnt = cps[i][j];
                controlPoints[i][j] = ControlPoint.valueOf(pnt, weight);
            }
        }
        ControlPointNet cpNet = ControlPointNet.valueOf(controlPoints);

        //  Create the surface.
        surface = BasicNurbsSurface.newInstance(cpNet, kvS, kvT);

        super.read_additional();
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_128_". The range of original U,V parameter
     * values is also avaliable as "IGES_U0", "IGES_U1", etc.
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_128_PROP1", prop1);
        element.putUserData("IGES_128_PROP2", prop2);
        element.putUserData("IGES_128_PROP3", prop3);
        element.putUserData("IGES_128_PROP4", prop4);
        element.putUserData("IGES_128_PROP5", prop5);
        element.putUserData("IGES_U0", uSstart);
        element.putUserData("IGES_U1", uSend);
        element.putUserData("IGES_V0", uTstart);
        element.putUserData("IGES_V1", uTend);
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        //  Everything was done in "read()".
    }

    /**
     * Scale the knot value in the the required range from 0.0 to 1.0.
     */
    private double scaleKnot(double m, double b, double value) {
        //  Scale the knot value.
        double kv = m * value + b;

        //  Watch for roundoff.
        if (kv < 0.0)
            kv = 0.0;
        else if (kv > 1.0)
            kv = 1.0;

        return kv;
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return surface;
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        NurbsSurface srf = surface.to(Constants.unit);
        ControlPointNet cpNet = srf.getControlPoints();
        int K1 = cpNet.getNumberOfRows() - 1;
        int K2 = cpNet.getNumberOfColumns() - 1;
        KnotVector sKV = srf.getSKnotVector();
        KnotVector tKV = srf.getTKnotVector();
        int M1 = sKV.getDegree();
        int M2 = tKV.getDegree();

        StringBuilder buffer = new StringBuilder();
        buffer.append(128);                 buffer.append(Constants.Delim);
        buffer.append(K1);                  buffer.append(Constants.Delim);
        buffer.append(K2);                  buffer.append(Constants.Delim);
        buffer.append(M1);                  buffer.append(Constants.Delim);
        buffer.append(M2);                  buffer.append(Constants.Delim);
        buffer.append(prop1);               buffer.append(Constants.Delim);
        buffer.append(prop2);               buffer.append(Constants.Delim);
        buffer.append(prop3);               buffer.append(Constants.Delim);
        buffer.append(prop4);               buffer.append(Constants.Delim);
        buffer.append(prop5);               buffer.append(Constants.Delim);

        int n = sKV.length();
        for (int i = 0; i < n; ++i) {
            double u = sKV.getValue(i);
            buffer.append(u);
            buffer.append(Constants.Delim);
        }

        n = tKV.length();
        for (int i = 0; i < n; ++i) {
            double u = tKV.getValue(i);
            buffer.append(u);
            buffer.append(Constants.Delim);
        }

        for (int i = 0; i <= K2; ++i) {
            for (int j = 0; j <= K1; ++j) {
                double w = cpNet.get(j, i).getWeight();
                buffer.append(w);
                buffer.append(Constants.Delim);
            }
        }

        for (int i = 0; i <= K2; ++i) {
            for (int j = 0; j <= K1; ++j) {
                Point cp = cpNet.get(j, i).getPoint();
                appendPoint3(buffer, cp);
            }
        }

        buffer.append(uSstart);             buffer.append(Constants.Delim);
        buffer.append(uSend);               buffer.append(Constants.Delim);
        buffer.append(uTstart);             buffer.append(Constants.Delim);
        buffer.append(uTend);               buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity128 - Rational B-Spline Surface";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        if (surface == null)
            return super.toString();
        
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        ControlPointNet cpNet = surface.getControlPoints();
        int K1 = cpNet.getNumberOfRows()-1;
        int K2 = cpNet.getNumberOfColumns()-1;
        int M1 = surface.getSDegree();
        int M2 = surface.getTDegree();
        
        outStr.append("K1    = ");  outStr.append(K1);      outStr.append("\n");
        outStr.append("K2    = ");  outStr.append(K2);      outStr.append("\n");
        outStr.append("M1    = ");  outStr.append(M1);      outStr.append("\n");
        outStr.append("M2    = ");  outStr.append(M2);      outStr.append("\n");
        outStr.append("prop1 = ");  outStr.append(prop1);   outStr.append("\n");
        outStr.append("prop2 = ");  outStr.append(prop2);   outStr.append("\n");
        outStr.append("prop3 = ");  outStr.append(prop3);   outStr.append("\n");
        outStr.append("prop4 = ");  outStr.append(prop4);   outStr.append("\n");
        outStr.append("prop5 = ");  outStr.append(prop5);   outStr.append("\n");
        
        int n = sKnots.length;
        for (int i = 0; i < n; i++) {
            outStr.append("S(");            outStr.append(i);   outStr.append("):  ");
            outStr.append(sKnots[i]);       outStr.append("\n");
        }
        outStr.append("\n");
        
        n = tKnots.length;
        for (int i = 0; i < n; i++) {
            outStr.append("T(");            outStr.append(i);   outStr.append("):  ");
            outStr.append(tKnots[i]);   outStr.append("\n");
        }
        outStr.append("\n");
        
        for (int i=0; i <= K2; i++) {
            for (int j=0; j <= K1; j++) {
                double weight = cpNet.get(j,i).getWeight();
                outStr.append("W(");            outStr.append(j);   outStr.append(",");
                outStr.append(i);               outStr.append("):  ");
                outStr.append(weight);          outStr.append("\n");
            }
        }

        for (int i=0; i <= K2; i++) {
            for (int j=0; j <= K1; j++) {
                Point pnt = cpNet.get(j,i).getPoint();
                outStr.append("X(");            outStr.append(j);   outStr.append(",");
                outStr.append(i);               outStr.append("):  ");
                outStr.append(pnt.get(0));      outStr.append("\n");
                outStr.append("Y(");            outStr.append(j);   outStr.append(",");
                outStr.append(i);               outStr.append("):  ");
                outStr.append(pnt.get(1));      outStr.append("\n");
                outStr.append("Z(");            outStr.append(j);   outStr.append(",");
                outStr.append(i);               outStr.append("):  ");
                outStr.append(pnt.get(2));      outStr.append("\n");
            }
        }
        
        outStr.append("uSstart = ");    outStr.append(uSstart); outStr.append("\n");
        outStr.append("uSend   = ");    outStr.append(uSend);   outStr.append("\n");
        outStr.append("uTstart = ");    outStr.append(uTstart); outStr.append("\n");
        outStr.append("uTend   = ");    outStr.append(uTend);   outStr.append("\n");
        
        return outStr.toString();
    }

}
