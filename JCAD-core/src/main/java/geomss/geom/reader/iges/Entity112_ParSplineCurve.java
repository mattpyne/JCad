/*
 *   Entity112_ParSplineCurve  -- An entity representing a Parametric Spline Curve Entity.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomElement;
import geomss.geom.Transformable;
import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.ControlPoint;
import geomss.geom.nurbs.KnotVector;
import geomss.geom.nurbs.NurbsCurve;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.measure.unit.Unit;
import org.jscience.mathematics.vector.Float64Matrix;

/**
 * <b><i>PARAMETRIC SPLINE CURVE ENTITY</i></b> - This entity represents a Parametric
 * Spline Curve that may be isolated or used as a component of a Composite Curve Entity or
 * a Subfigure Entity. The parametric spline curve is a sequence of parametric polynomial
 * segments.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a NURBS curve. This entity
 * type can not be written out to an IGES file. The spline parameters are stored in the
 * user data with the prefix "IGES_112_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 9, 2013
 * @version September 13, 2016
 */
public class Entity112_ParSplineCurve extends GeomSSEntity {

    private int ctype = 3;          //  1=linear, 2=quadratic, 3=cubic, 4=Wilson-Fowler, 5=Modified Wilson-Fowler, 6=B-spline
    private int H = 1;              //  Degree of continuity with respect to arc length
    private int NDIM = 3;           //  Number of dimemsions (2=planar, 3=non-planar)
    private int N = 1;              //  Number of segments
    private double[] T = null;      //  Break points of piecewise polynomial
    private double[][] A = null;    //  Polynomial in each direction for each segment
    private double[][] B = null;
    private double[][] C = null;
    private double[][] D = null;
    private double TP[][] = null;

    private static final double[][] Bmat
            =   {{1.,   0.,         0.,     0.},
                {1.,    1. / 3.,    0.,     0.},
                {1.,    2. / 3.,    1. / 3., 0.},
                {1.,    1.,         1.,     1.}};
    private static final Float64Matrix Bm = Float64Matrix.valueOf(Bmat);

    private NurbsCurve curve;   //  The GeomSS curve this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity112_ParSplineCurve(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity112 constructor called");
        }

    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     *
     * - The Label Display Pointer shall be 0
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // DE LblDsp shall be 0
        if (DE.getLblDsp() != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("labelDisplay"), DE.getLblDsp());
            addErrorMessage(getWarningString(msg));
        }

    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity112.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        ctype = getInt(s);          //  Spline Type before conversion to Type 112
        H = getInt(s);              //  Degree of continuity with respect to arc length
        NDIM = getInt(s);           //  Number of dimensions
        N = getInt(s);              //  Number of segments

        //  Read in breakpoints of piecewise polynomial
        T = new double[N + 1];        //  Allocate storage for the breakpoints
        for (int i = 0; i <= N; ++i) {
            double u = getReal(s);
            T[i] = u;
        }

        //  Allocate memory for the polynomial segment coefficients.
        A = new double[N][NDIM];
        B = new double[N][NDIM];
        C = new double[N][NDIM];
        D = new double[N][NDIM];

        //  Loop over each segment.
        for (int i = 0; i < N; ++i) {
            //  Read in the coefficients in each coordinate direction.
            for (int j = 0; j < NDIM; ++j) {
                A[i][j] = getReal(s);
                B[i][j] = getReal(s);
                C[i][j] = getReal(s);
                D[i][j] = getReal(s);
            }
        }

        //  Read in the remaining evaluation points.
        TP = new double[NDIM][4];
        for (int i = 0; i < NDIM; ++i) {
            for (int j = 0; j < 4; ++j) {
                TP[i][j] = getReal(s);
            }
        }

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {

        //  Convert from polynomial coefficient form to B-Spline control points.
        //      Reference:  Handbook of Grid Generation, Chapter 30, Section 30.3.0.
        
        //  Bezier control polygon: bmat[0..3] = Bm*Cmat
        final int degree = 3;
        final int order = degree + 1;
        List<ControlPoint> cps = new ArrayList();
        double[][] Cmat = new double[order][NDIM];
        boolean firstPass = true;
        for (int i = 0; i < N; ++i) {

            //  Calculate the reparameterization factor.
            double h = T[i + 1] - T[i];
            double h2 = h * h;
            double h3 = h2 * h;

            //  Build up the polynomial coefficient matrix.
            for (int j = 0; j < NDIM; ++j) {
                Cmat[0][j] = A[i][j];
                Cmat[1][j] = B[i][j] * h;
                Cmat[2][j] = C[i][j] * h2;
                Cmat[3][j] = D[i][j] * h3;
            }

            //  bmat = Bmat*Cmat
            Float64Matrix Cm = Float64Matrix.valueOf(Cmat);
            Float64Matrix bm = Bm.times(Cm);

            //  Convert bm into a list of control points.
            List<ControlPoint> cpList = cpMatrix2ControlPoints(bm, Constants.unit);
            if (firstPass) {
                //  Only add the 1st control point on the 1st segment.
                firstPass = false;
                cps.add(cpList.get(0));
            }
            for (int j = 1; j < order; ++j) {
                cps.add(cpList.get(j));
            }
        }

        //  Create a knot list and put a degreeU+1 touple knot at the start.
        List<Double> knots = new ArrayList();
        for (int i = 0; i <= degree; i++) {
            knots.add(ZERO);
        }

        //  Add degreeU+1 touple knotsU where each Bezier segment is joined.
        double ds = 1. / N;
        double sv = 0;
        for (int j = 1; j < N; ++j) {
            sv += ds;
            for (int i = 0; i < degree; i++) {
                knots.add(sv);
            }
        }

        //  Add a degreeU+1 touble set of knotsU at the end.
        for (int i = 0; i <= degree; i++) {
            knots.add(ONE);
        }

        //  Create the knot vector.
        KnotVector kv = KnotVector.newInstance(degree, knots);

        //  Create the curve.
        curve = BasicNurbsCurve.newInstance(cps, kv);

    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_112_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_112_CTYPE", ctype);
        element.putUserData("IGES_112_H", H);
        element.putUserData("IGES_112_N", N);
        element.putUserData("IGES_112_T", T);
        element.putUserData("IGES_112_A", A);
        element.putUserData("IGES_112_B", B);
        element.putUserData("IGES_112_C", C);
        element.putUserData("IGES_112_D", D);
        element.putUserData("IGES_112_TP", TP);
        element.putUserData("IGES_U0", T[0]);
        element.putUserData("IGES_U1", T[N]);
    }

    /**
     * Return a list of control points from the input matrix of control point coordinates
     * (points in rows, dimensions in columns).
     *
     */
    private static List<ControlPoint> cpMatrix2ControlPoints(Float64Matrix Pmat, Unit units) {

        //  Create a list of control points from the matrix of control point positions.
        List<ControlPoint> cpList = new ArrayList();
        int numPoints = Pmat.getNumberOfRows();
        for (int i = 0; i < numPoints; i++) {
            ControlPoint pnt = ControlPoint.valueOf(Pmat.getRow(i), 1., units);
            cpList.add(pnt);
        }

        return cpList;
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return curve;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity112 - Parametric Spline Curve";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        return outStr.toString();
    }

}
