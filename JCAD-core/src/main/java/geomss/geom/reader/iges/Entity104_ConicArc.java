/*
 *   Entity104_ConicArc  -- Entity representing a general conic section other than a circle.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.*;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import static javolution.lang.MathLib.*;
import javolution.util.FastTable;

/**
 * <b><i>CONIC ARC ENTITY</i></b> - This entity defines an ellipse, a parabola, a
 * hyperbola, or a portion of one of these conic curves which may be isolated or used as a
 * component of a Composite Curve or a sub-figure. The definition space coordinate system
 * is always chosen so that the conic arc lies in a plane either coincident with or
 * parallel to XT, YT plane. Within such a plane a conic is defined by the six
 * coefficients in the following point: A*XT^2 + B*XT*YT + C*YT^2 + D*XT + E*YT + F = 0
 *
 * <p>
 * These entities (forms 0-3), when read from an IGES file, are converted to NURBS curves
 * of degreeU 2 (IGES type 126, Form 2, Degree 2). This entity type can not be written out
 * to an IGES file. All conic arc parameters are stored in the user data with the prefix
 * "IGES_106_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public abstract class Entity104_ConicArc extends GeomSSEntity {

    private double Acoef;       // General Conic Coefficients
    private double Bcoef;
    private double Ccoef;
    private double Dcoef;
    private double Ecoef;
    private double Fcoef;
    private double zt;          // ZT Coordinate of plane of definition
    private double x1;          // Start point abcissa
    private double y1;          // Start point ordinate
    private double x2;          // Terminate point abcissa
    private double y2;          // Terminate point ordinate

    private double Q1, Q2;      // Used to determine conic type.

    private NurbsCurve curve;   //  THe GeomSS curve representing this conic section.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity104_ConicArc(Part p, DirEntry de) {
        super(p, de);
    }

    /**
     * Checks to see if the entity should be drawn. No restrictions are imposed.
     */
    @Override
    public void check() {
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        Acoef = getReal(s);
        Bcoef = getReal(s);
        Ccoef = getReal(s);
        Dcoef = getReal(s);
        Ecoef = getReal(s);
        Fcoef = getReal(s);
        zt = getReal(s);
        if (Math.abs(zt) < Constants.Grain)
            zt = 0;
        x1 = getReal(s);
        if (Math.abs(x1) < Constants.Grain)
            x1 = 0;
        y1 = getReal(s);
        if (Math.abs(y1) < Constants.Grain)
            y1 = 0;
        x2 = getReal(s);
        if (Math.abs(x2) < Constants.Grain)
            x2 = 0;
        y2 = getReal(s);
        if (Math.abs(y2) < Constants.Grain)
            y2 = 0;

        if (abs(Bcoef) > Constants.Grain || Acoef > Ccoef)
            throw new IOException(
                    MessageFormat.format(RESOURCES.getString("rotatedConicsNotSupported"),
                            getHeader()));

        super.read_additional();

        //  Calculate some needed parameters.
        double A = Acoef, B = Bcoef, C = Ccoef, D = Dcoef, E = Ecoef, F = Fcoef;
        Q1 = A * C * F + 0.25 * (B * D * E - A * E * E - B * B * F - D * D * C);
        Q2 = A * C - 0.25 * B * B;

    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {

        //  Determine the curve type (do not trust the form ID).
        if (abs(Q2) < Constants.Grain && abs(Q1) >= Constants.Grain) {
            //  Parabola
            curve = makeParabolicCurve();

        } else if (Q2 < 0 && abs(Q1) >= Constants.Grain) {
            //  Hyperbola
            curve = makeHyperbolicCurve();

        } else if (Q2 > 0 && Q1 * (Acoef + Ccoef) < 0) {
            //  Ellipse
            curve = makeEllipseCurve();

        } else {
            System.out.println(toString());
            System.out.println("Q1 = " + Q1 + ", Q2 = " + Q2);

            throw new IOException(MessageFormat.format(RESOURCES.getString("unknownConicType"), getHeader()));
        }

    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_104_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_104_A", Acoef);
        element.putUserData("IGES_104_B", Bcoef);
        element.putUserData("IGES_104_C", Ccoef);
        element.putUserData("IGES_104_D", Dcoef);
        element.putUserData("IGES_104_E", Ecoef);
        element.putUserData("IGES_104_F", Fcoef);
        element.putUserData("IGES_104_ZT", zt);
        element.putUserData("IGES_104_X1", x1);
        element.putUserData("IGES_104_Y1", y1);
        element.putUserData("IGES_104_X2", x2);
        element.putUserData("IGES_104_Y2", y2);
    }

    /**
     * Return a new elliptical arc segment based on the general conic point
     * coefficients and end points.
     */
    private BasicNurbsCurve makeEllipseCurve() {
        //  Determine semi-major and semi-minor axis lengths.
        Parameter<Length> sma = Parameter.valueOf(sqrt(-Fcoef / Acoef), Constants.unit);
        Parameter<Length> smb = Parameter.valueOf(sqrt(-Fcoef / Ccoef), Constants.unit);

        //  Determine the origin of the ellipse.
        double xo = -2 * Ccoef * Dcoef / 4 / Q1;
        double yo = -2 * Acoef * Ecoef / 4 / Q1;
        Point origin = Point.valueOf(xo, yo, zt, Constants.unit);

        //  Determine the start and ending angles.
        double p1mOx = x1 - xo;
        double p1mOy = y1 - yo;
        double p2mOx = x2 - xo;
        double p2mOy = y2 - yo;
        double ths = atan2(p1mOy, p1mOx);
        double the = atan2(p2mOy, p2mOx);
        Parameter<Angle> thetaStart = Parameter.valueOf(ths, SI.RADIAN);
        Parameter<Angle> thetaEnd = Parameter.valueOf(the, SI.RADIAN);

        //  Define X & Y directions.
        Vector<Dimensionless> xhat = Vector.valueOf(1, 0, 0).toUnitVector();
        Vector<Dimensionless> yhat = Vector.valueOf(0, 1, 0).toUnitVector();

        //  Create the elliptical arc.
        return CurveFactory.createEllipticalArc(origin, sma, smb, xhat, yhat, thetaStart, thetaEnd);
    }

    /**
     * Return a new hyperbolic arc segment based on the general conic point
     * coefficients and end points.
     */
    private BasicNurbsCurve makeHyperbolicCurve() {
        Parameter<Length> tol = Parameter.valueOf(Constants.Grain, Constants.unit);

        //  Create end points.
        Point p0 = Point.valueOf(x1, y1, zt, Constants.unit);
        Point p2 = Point.valueOf(x2, y2, zt, Constants.unit);

        //  Create tangent vectors at each end.
        Vector t0 = derivative(x1, y1);
        Vector t2 = derivative(x2, y2);

        //  Find the intersection of the two tangent vector lines.
        MutablePoint p1 = MutablePoint.newInstance(3, Constants.unit);
        GeomUtil.lineLineIntersect(p0, t0, p2, t2, tol, null, p1, null);

        //  Find the bisector of the line between the end points.
        GeomPoint pM = p0.plus(p2).divide(2);

        //  Find the shoulder point on the curve.
        Point S;
        double xm = pM.getValue(0);
        double xp1 = p1.getValue(0);
        double A = Acoef, C = Ccoef, F = Fcoef;
        if (abs(xm - xp1) < Constants.Grain) {
            //  Line from m to p1 is vertical.
            double ys = sqrt(-C * (A * xm * xm + F)) / C;
            S = Point.valueOf(xm, ys, zt, Constants.unit);

        } else {
            double yp1 = p1.getValue(1);
            double ym = pM.getValue(1);
            double m = (ym - yp1) / (xm - xp1);
            double b = yp1 - m * xp1;
            double den = 2 * (A + C * m * m);

            double xs = -2 * C * m * b + 2 * sqrt(-A * F - A * C * b * b - C * F * m * m);
            xs /= den;
            double ys = m * xs + b;

            S = Point.valueOf(xs, ys, zt, Constants.unit);
        }

        //  Determine the weight of the middle control point.
        double w1 = pM.distance(S).divide(S.distance(p1)).getValue();

        //  Create the control points.
        FastTable<ControlPoint> cps = FastTable.newInstance();
        cps.add(ControlPoint.valueOf(p0, 1));
        cps.add(ControlPoint.valueOf(p1.immutable(), w1));
        cps.add(ControlPoint.valueOf(p2, 1));
        MutablePoint.recycle(p1);

        // Create the knot vector.
        KnotVector kv = KnotVector.newInstance(2, 0., 0., 0., 1., 1., 1.);

        //  Create the curve.
        BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kv);

        //  Clean up before leaving.
        FastTable.recycle(cps);

        return crv;
    }

    /**
     * Return a new parabolic arc segment based on the general conic point coefficients
     * and end points.
     */
    private BasicNurbsCurve makeParabolicCurve() {
        //  Create end points.
        Point p1 = Point.valueOf(x1, y1, zt, Constants.unit);
        Point p2 = Point.valueOf(x2, y2, zt, Constants.unit);

        //  Create tangent vectors at each end.
        Vector t1 = derivative(x1, y1);
        Vector t2 = derivative(x2, y2);

        //  Create the parabolic arc.
        return CurveFactory.createParabolicArc(p1, t1, p2, t2);
    }

    /**
     * Return the tangent vector to the general conic curve at the specified x,y location.
     *
     * @param x The abscissa for the point on the curve to return the tangent vector for.
     * @param y The ordinate for the point on the curve to return the tangent vector for.
     * @return The tangent vector for the conic curve at x,y.
     */
    private Vector<Dimensionless> derivative(double x, double y) {
        double num = 2 * Acoef * x + Bcoef * y + Dcoef;
        double den = -2 * Ccoef * y - Bcoef * x - Ecoef;

        Vector<Dimensionless> T;
        if (abs(den) < Constants.Grain) {
            //  Infinite slope (vertical line).
            T = Vector.valueOf(0, num, 0).toUnitVector();

        } else {
            double dydx = num / den;
            T = Vector.valueOf(1, dydx, 0).toUnitVector();
        }

        return T;
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return curve;
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        outStr.append("A  = "); outStr.append(Acoef);   outStr.append("\n");
        outStr.append("B  = "); outStr.append(Bcoef);   outStr.append("\n");
        outStr.append("C  = "); outStr.append(Ccoef);   outStr.append("\n");
        outStr.append("D  = "); outStr.append(Dcoef);   outStr.append("\n");
        outStr.append("E  = "); outStr.append(Ecoef);   outStr.append("\n");
        outStr.append("F  = "); outStr.append(Fcoef);   outStr.append("\n");
        outStr.append("zt = "); outStr.append(zt);  outStr.append("\n");
        outStr.append("x1 = "); outStr.append(x1);  outStr.append("\n");
        outStr.append("y1 = "); outStr.append(y1);  outStr.append("\n");
        outStr.append("x2 = "); outStr.append(x2);  outStr.append("\n");
        outStr.append("y2 = "); outStr.append(y2);

        return outStr.toString();
    }

}
