/*
 *   Entity126_BSplineCurve  -- An entity representing a Rational B-Spline Curve.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomElement;
import geomss.geom.Point;
import geomss.geom.Transformable;
import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.ControlPoint;
import geomss.geom.nurbs.KnotVector;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.measure.quantity.Length;

/**
 * <p>
 * <b><i>RATIONAL B-SPLINE CURVE ENTITY</i></b> - This entity represents a Rational
 * B-Spline Curve that may be isolated or used as a component of a Composite Curve Entity
 * or a Subfigure Entity. This represents a parametric point obtained by dividing two
 * summations involving weights (which are real numbers), the control points, and B-Spline
 * basis functions.</p>
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a NURBS curve. This entity
 * type <b>can</b> be written out to an IGES file. When reading in this entity, all plane
 * parameters are stored in the user data with the prefix "IGES_126_" followed by the
 * parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: August 23, 2010
 * @version April 7, 2016
 */
public class Entity126_BSplineCurve extends GeomSSEntity {

    private int prop1 = 0;  //  0=non-planar, 1=planar
    private int prop2 = 0;  //  0=open, 1=closed
    private int prop3 = 0;  //  0=rational, 1=polynomial
    private int prop4 = 0;  //  0=non-periodic (clamped), 1=periodic (un-clamped).
    private double[] knots = null;  //  Knot sequence
    private double ustart = 0;  //  Starting parameter value.
    private double uend = 1;    //  Ending parameter value.
    private double xnorm = 0;   //  Unit normal (if curve is planar; flag1=1)
    private double ynorm = 0;
    private double znorm = 1;

    private NurbsCurve curve;   //  The GeomSS curve this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity126_BSplineCurve(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity126 constructor called");
        }

    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part to which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS GeomPoint geometry to return an Entity for.
     */
    public Entity126_BSplineCurve(Part part, int DEnum, NurbsCurve geom) {
        super(part, new DirEntry(126, 0, DEnum, 0, geom.getName()));
        curve = geom;

        //  Store off the Knot vector also.
        KnotVector kv = curve.getKnotVector();
        int n = kv.length();
        knots = new double[n];
        for (int i = 0; i < n; ++i)
            knots[i] = kv.getValue(i);

        //  Is the curve planar?
        Parameter<Length> tol = Parameter.valueOf(Constants.Grain, Constants.unit);
        if (curve.isPlanar(tol))
            prop1 = 1;

        //  Is the curve closed?
        if (curve.getRealPoint(0).distance(curve.getRealPoint(1)).isLessThan(tol))
            prop2 = 1;

        //  Is the curve rational (prop3 = 0) or polynomial (prop3 = 1)?
        List<ControlPoint> cps = curve.getControlPoints();
        n = cps.size();
        double w = cps.get(0).getWeight();
        prop3 = 1;
        for (int i = 1; i < n; ++i) {
            ControlPoint cp = cps.get(i);
            if (Math.abs(cp.getWeight() - w) > MathTools.EPS) {
                prop3 = 0;
                break;
            }
        }
    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     *
     * - The Label Display Pointer shall be 0
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // DE LblDsp shall be 0
        if (DE.getLblDsp() != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("labelDisplay"), DE.getLblDsp());
            addErrorMessage(getWarningString(msg));
        }

    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity126.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        int K = getInt(s);          //  Number of control points.
        int M = getInt(s);          //  Degree of basis functions.
        prop1 = getInt(s);
        prop2 = getInt(s);
        prop3 = getInt(s);
        prop4 = getInt(s);

        int n = K + 1 + M + 1;      //  Number of knotsU.
        knots = new double[n];
        for (int i = 0; i < n; ++i) {
            double u = getReal(s);
            knots[i] = u;
        }

        List<Double> weights = new ArrayList(); //  List of weights for each control point.
        for (int i = 0; i <= K; ++i) {
            double w = getReal(s);
            weights.add(w);
        }

        List<Point> cps = new ArrayList();      //  List of control points.
        for (int i = 0; i <= K; ++i) {
            cps.add(getPoint3(s));
        }

        ustart = getReal(s);
        uend = getReal(s);

        xnorm = getReal(s);
        if (Math.abs(xnorm) < Constants.Grain)
            xnorm = 0;
        ynorm = getReal(s);
        if (Math.abs(ynorm) < Constants.Grain)
            ynorm = 0;
        znorm = getReal(s);
        if (Math.abs(znorm) < Constants.Grain)
            znorm = 0;

        //  Make sure all the knotsU read in are in-range.
        for (int i = 0; i < n; ++i) {
            double kv = knots[i];
            if (kv < ustart)
                knots[i] = ustart;
            else if (kv > uend)
                knots[i] = uend;
        }

        //  Scale the knotsU into the range 0-1.
        double[] scaledKnots = knots;
        if (Math.abs(ustart) > MathTools.EPS || Math.abs(uend - 1.0) > MathTools.EPS) {
            scaledKnots = new double[n];
            double m = 1. / (uend - ustart);
            double b = -m * ustart;
            for (int i = 0; i < n; ++i) {
                double kv = knots[i];
                kv = scaleKnot(m, b, kv);
                scaledKnots[i] = kv;
            }
        }

        //  Create the knot vector.
        KnotVector kv = KnotVector.newInstance(M, scaledKnots);

        //  Create the control points.
        List<ControlPoint> cpList = new ArrayList();
        n = cps.size();
        for (int i = 0; i < n; ++i) {
            double weight = weights.get(i);
            Point pnt = cps.get(i);
            cpList.add(ControlPoint.valueOf(pnt, weight));
        }

        //  Create the curve.
        curve = BasicNurbsCurve.newInstance(cpList, kv);

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        //  Everything done in "read()".
    }

    /**
     * Scale the knot value in the the required range from 0.0 to 1.0.
     */
    private double scaleKnot(double m, double b, double value) {
        //  Scale the knot value.
        double kv = m * value + b;

        //  Watch for roundoff.
        if (kv < 0.0)
            kv = 0.0;
        else if (kv > 1.0)
            kv = 1.0;

        return kv;
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_126_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_126_PROP1", prop1);
        element.putUserData("IGES_126_PROP2", prop2);
        element.putUserData("IGES_126_PROP3", prop3);
        element.putUserData("IGES_126_PROP4", prop4);
        element.putUserData("IGES_U0", ustart);
        element.putUserData("IGES_U1", uend);
        element.putUserData("IGES_126_XNORM", xnorm);
        element.putUserData("IGES_126_YNORM", ynorm);
        element.putUserData("IGES_126_ZNORM", znorm);
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return curve;
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entity's parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        NurbsCurve curve2 = curve.to(Constants.unit);
        List<ControlPoint> cps = curve2.getControlPoints();
        int K = cps.size() - 1;
        KnotVector kv = curve2.getKnotVector();
        int M = kv.getDegree();

        StringBuilder buffer = new StringBuilder();
        buffer.append(126);                 buffer.append(Constants.Delim);
        buffer.append(K);                   buffer.append(Constants.Delim);
        buffer.append(M);                   buffer.append(Constants.Delim);
        buffer.append(prop1);               buffer.append(Constants.Delim);
        buffer.append(prop2);               buffer.append(Constants.Delim);
        buffer.append(prop3);               buffer.append(Constants.Delim);
        buffer.append(prop4);               buffer.append(Constants.Delim);

        int n = kv.length();
        for (int i = 0; i < n; ++i) {
            double u = kv.getValue(i);
            buffer.append(u);   buffer.append(Constants.Delim);
        }

        for (int i = 0; i <= K; ++i) {
            double w = cps.get(i).getWeight();
            buffer.append(w);   buffer.append(Constants.Delim);
        }

        for (int i = 0; i <= K; ++i) {
            Point cp = cps.get(i).getPoint();
            appendPoint3(buffer, cp);
        }

        buffer.append(ustart);  buffer.append(Constants.Delim);
        buffer.append(uend);    buffer.append(Constants.Delim);

        if (Math.abs(xnorm) < Constants.Grain)
            xnorm = 0;
        buffer.append(xnorm);   buffer.append(Constants.Delim);
        if (Math.abs(ynorm) < Constants.Grain)
            ynorm = 0;
        buffer.append(ynorm);   buffer.append(Constants.Delim);
        if (Math.abs(znorm) < Constants.Grain)
            znorm = 0;
        buffer.append(znorm);   buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity126 - Rational B-Spline Curve";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        if (curve == null)
            return super.toString();
        
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");
        
        //  Get the data we need.
        List<ControlPoint> cps = curve.getControlPoints();
        int K = cps.size()-1;
        KnotVector kv = curve.getKnotVector();
        int M = kv.getDegree();
        
        outStr.append("K     = ");  outStr.append(K);       outStr.append("\n");
        outStr.append("M     = ");  outStr.append(M    );   outStr.append("\n");
        outStr.append("prop1 = ");  outStr.append(prop1);   outStr.append("\n");
        outStr.append("prop2 = ");  outStr.append(prop2);   outStr.append("\n");
        outStr.append("prop3 = ");  outStr.append(prop3);   outStr.append("\n");
        outStr.append("prop4 = ");  outStr.append(prop4);   outStr.append("\n");
        
        int n = knots.length;
        for (int i = 0; i < n; i++) {
            outStr.append("T(");            outStr.append(i);   outStr.append("):  ");
            outStr.append(knots[i]);        outStr.append("\n");
        }
        outStr.append("\n");
        
        
        for (int i=0; i <= K; i++) {
            double weight = cps.get(i).getWeight();
            outStr.append("W(");            outStr.append(i);   outStr.append("):  ");
            outStr.append(weight);          outStr.append("\n");
        }

        for (int i = 0; i <= K; i++) {
            Point cp = cps.get(i).getPoint();
            outStr.append("X(");        outStr.append(i);   outStr.append("):  ");
            outStr.append(cp.get(0));   outStr.append("\n");
            outStr.append("Y(");        outStr.append(i);   outStr.append("):  ");
            outStr.append(cp.get(1));   outStr.append("\n");
            outStr.append("Z(");        outStr.append(i);   outStr.append("):  ");
            outStr.append(cp.get(2));   outStr.append("\n");
        }
        
        outStr.append("ustart = "); outStr.append(ustart);  outStr.append("\n");
        outStr.append("uend   = "); outStr.append(uend);    outStr.append("\n");
        
        if (prop1 == 1) {
            outStr.append("xnorm = ");  outStr.append(xnorm);   outStr.append("\n");
            outStr.append("ynorm = ");  outStr.append(ynorm);   outStr.append("\n");
            outStr.append("znorm = ");  outStr.append(znorm);   outStr.append("\n");
        }
        
        return outStr.toString();
    }

}
