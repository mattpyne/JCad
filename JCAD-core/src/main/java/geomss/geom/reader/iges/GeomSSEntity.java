/*
 *   GeomSSEntity  -- Interface and implementation in common to all Entitys that contain GeomSS geometry.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.GTransform;
import geomss.geom.GeomElement;
import geomss.geom.Transformable;
import java.io.IOException;

/**
 * The interface an implementation in common to all Entity objects that contain GeomSS
 * geometry.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt
 * @version September 13, 2016
 */
public abstract class GeomSSEntity extends Entity {

    protected static final Double ZERO = (double)0;
    protected static final Double ONE = (double)1;

    private boolean isUsedInList = false;

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public GeomSSEntity(Part p, DirEntry de) {
        super(p, de);
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    abstract void createGeometry() throws IOException;

    /**
     * Return a reference to the Transformable GeomElement element contained in this IGES
     * Entity.
     *
     * @return A reference to the Transformable GeomElement element contained in this IGES
     *         Entity.
     */
    protected abstract Transformable getGeomElement();

    /**
     * Return a reference to the GeomElement element contained in this IGES Entity.
     *
     * @param m the current transformation matrix to apply to the entity.
     * @return A reference to the GeomElement element contained in this IGES Entity
     */
    public GeomElement getGeomElement(GTransform m) {
        //  Get the geometry element from the sub-class.
        Transformable transElement = getGeomElement();
        GeomElement element = transElement;
        if (element == null)
            return null;

        //  Apply any rotation matrix.
        m = getMatrix(m);
        if (!m.equals(GTransform.IDENTITY)) {
            //  Apply transformation.
            element = transElement.getTransformed(m);
        }

        // Attach some IGES meta-data.
        applyMetaData(element);

        return element;
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation applies
     * the entity type, form, and header string as: IGESType, IGESForm and IGESHeader
     * respectively.
     *
     * @param element The element to set the meta-data on.
     */
    protected void applyMetaData(GeomElement element) {

        DirEntry DE = getDirectoryEntry();
        String label = DE.getLabel();
        if (!label.equals(""))
            element.setName(label);
        element.putUserData("IGESType", DE.getType());
        element.putUserData("IGESForm", DE.getForm());
        element.putUserData("IGESHeader", getHeader());

    }

    /**
     * Returns true if this Entity is being used in another GeomSSEntity, otherwise
     * returns false.
     *
     * @return true if this Entity is being used in another GeomSSEntity
     */
    public boolean isUsedInList() {
        return isUsedInList;
    }

    /**
     * Set if this GeomSSEntity is being used by another entity or not.
     */
    void setUsedInList(boolean used) {
        isUsedInList = used;
    }

}
