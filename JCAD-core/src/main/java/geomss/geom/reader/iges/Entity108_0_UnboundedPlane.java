/*
 *   Entity108_0_UnboundedPlane  -- Entity that represents an unbounded plane.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomPlane;
import geomss.geom.GeomPoint;
import geomss.geom.GeomVector;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <b><i>PLANE ENTITY - UNBOUNDED</i></b> - This entity represents an unbounded plane.
 * 
 * <p>
 * This entity type <b>can</b> be written out to an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: September 6, 2010
 * @version September 13, 2016
 * @see Entity108_Plane
 */
public class Entity108_0_UnboundedPlane extends Entity108_Plane {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity108_0_UnboundedPlane(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity108_0 constructor called");
        }
    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part in which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS PointString geometry to return an Entity for.
     */
    public Entity108_0_UnboundedPlane(Part part, int DEnum, GeomPlane geom) {
        super(part, new DirEntry(108, 0, DEnum, 0, geom.getName()));
        plane = geom;

        //  Extract the data we are writing out.
        GeomVector n = geom.getNormal();
        A = n.getValue(0);
        B = n.getValue(1);
        C = n.getValue(2);
        D = geom.getConstant().getValue(Constants.unit);

        ptr = 0;

        GeomPoint refPnt = geom.getRefPoint().to(Constants.unit);
        xt = refPnt.getValue(0);
        yt = refPnt.getValue(1);
        zt = refPnt.getValue(2);

    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true if the Entity can be written to an exchange file.
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        StringBuilder buffer = new StringBuilder();
		buffer.append(108);					buffer.append(Constants.Delim);
		buffer.append(A);					buffer.append(Constants.Delim);
		buffer.append(B);					buffer.append(Constants.Delim);
		buffer.append(C);					buffer.append(Constants.Delim);
		buffer.append(D);					buffer.append(Constants.Delim);
		buffer.append(ptr);					buffer.append(Constants.Delim);
		buffer.append(xt);					buffer.append(Constants.Delim);
		buffer.append(yt);					buffer.append(Constants.Delim);
		buffer.append(zt);					buffer.append(Constants.Delim);
		buffer.append(symbolSize);			buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity108_0 - Unbounded Plane";
    }

}
