/*
 *   Entity124_TransformationMatrix  -- This class encapsulates a transformation elment that allows translation & rotation.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.GTransform;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import javax.measure.converter.UnitConverter;
import javax.measure.unit.SI;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * <b><i>TRANSFORMATION MATRIX ENTITY</i></b> - This allows translation and rotation to be
 * applied to other entities. This is used to define alternate coordinate systems for
 * definition and viewing. The transformation matrix entity transforms three-row column
 * vectors by means of matrix multiplication and then a vector addition. This entity can
 * be considered as an "operator" entity in that it starts with the input vector, operates
 * on it as described above, and produces the output vector.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a GTransform object. This
 * entity type can not be written out to an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Entity124_TransformationMatrix extends Entity {

    private GTransform mat = GTransform.IDENTITY;

    /**
     * Default constructor. Needs to initialize the internal matrix representation.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity124_TransformationMatrix(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity124 constructor called");
        }

    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     *
     * - The Transformation Matrix Pointer shall be 0
     */
    @Override
    public void check() {
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws IOException if there is any problem reading the entity from the IGES file.
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        if (Constants.DEBUG) {
            System.out.println("Entity124.read() called");
        }

        if (getDirectoryEntry().getForm() != 0)
            throw new IOException(MessageFormat.format(RESOURCES.getString("illegalForm"),
                    getDirectoryEntry().getForm(), getTypeString()));

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        //  Get a unit converter between the model units and meters.
        UnitConverter u2meters = Constants.unit.getConverterTo(SI.METER);

        //  Start reading in the matrix.
        double r1 = getReal(s), r2 = getReal(s), r3 = getReal(s);
        double t = u2meters.convert(getReal(s));        //  Converted to meters.
        Float64Vector row0 = Float64Vector.valueOf(r1, r2, r3, t);

        r1 = getReal(s);
        r2 = getReal(s);
        r3 = getReal(s);
        t = u2meters.convert(getReal(s));
        Float64Vector row1 = Float64Vector.valueOf(r1, r2, r3, t);

        r1 = getReal(s);
        r2 = getReal(s);
        r3 = getReal(s);
        t = u2meters.convert(getReal(s));
        Float64Vector row2 = Float64Vector.valueOf(r1, r2, r3, t);
        Float64Vector row3 = Float64Vector.valueOf(0, 0, 0, 1);

        mat = GTransform.valueOf(row0, row1, row2, row3);

        super.read_additional();
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity124 - Transformation Matrix";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");
        outStr.append(mat.toString());
        return outStr.toString();
    }

    /**
     * Returns the internal matrix.
     *
     * @return GTransform set to the values of the transform matrix.
     */
    public GTransform getMat() {
        return getMatrix(mat);
    }
}
