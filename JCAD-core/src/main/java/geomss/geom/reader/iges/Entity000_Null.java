/*
 *   Entity000_Null  -- A Null Entity that may be created as a result of editing a file.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * <b><i>NULL ENTITY</i></b> - An entity that may be created as a result of editing a file
 * to delete an "undesired" entity.
 *
 * <p>
 * This entity type is ignored when reading in an IGES file and can not be written out to
 * an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Entity000_Null extends Entity {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity000_Null(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity000 constructor called");
        }
    }

    /**
     * Checks to see if the entity is correct. This implementation does nothing.
     */
    @Override
    public void check() { }

    /**
     * Read the Parameter Data from the String read in by the superclass. Nothing is
     * parsed for the Null entity.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        if (Constants.DEBUG) {
            System.out.println("Entity000.read() called");
        }

        super.read(in);

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + getPDString() + "\"");
        }
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity000 - Null";
    }

}
