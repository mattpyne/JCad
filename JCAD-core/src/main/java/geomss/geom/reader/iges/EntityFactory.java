/*
 *   EntityFactory  -- A factory for creating Entity instances.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.NurbsCurve;
import geomss.geom.nurbs.NurbsSurface;
import java.util.List;

/**
 * A factory class that provides methods for creating Entity instances.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: April 27, 2013
 * @version September 13, 2016
 */
public class EntityFactory {

    /**
     * Return an Entity that corresponds to the specified Directory Entry.
     *
     * @param part     The part that the entity is associated with.
     * @param drawings A list that any Entity 404 Drawing objects will be added to.
     * @param views    A list that any Entity 410 Views will be added to.
     * @param de       The Directory Entry for the part entity (which defines the type for
     *                 the Entity).
     * @return The newly created entity that corresponds to the specified Directory Entry.
     */
    public static Entity create(Part part, List<Entity> drawings, List<Entity> views, DirEntry de) {
        Entity e;

        switch (de.getType()) {
            case   0: e = new Entity000_Null(part,de); break;                       //  Null entity
            case 100: e = new Entity100_CircularArc(part,de); break;                //  Circle or circular arc
            case 102: e = new Entity102_CompositeCurve(part,de); break;             //  Composite curve
            case 104: switch(de.getForm()) {
                    case 1:  e = new Entity104_1_EllipticalArc(part,de);  break;    //  Elliptical arc
                    case 2:  e = new Entity104_2_HyperbolicArc(part,de);  break;    //  Hyperbolic arc
                    case 3:  e = new Entity104_3_ParabolicArc(part,de);  break;     //  Parabolic arc
                    default: e = new Entity104_0_GenConicArc(part,de);  break;      //  General conic arc
                }
                break;
            case 106: switch(de.getForm()) {
                    case 1:  e = new Entity106_1_2DPoints(part,de); break;          //  Copious Data: 2D points
                    case 2:  e = new Entity106_2_3DPoints(part,de); break;          //  Copious Data: 3D points
                    case 3:  e = new Entity106_3_3DVectors(part,de); break;         //  Copious Data: 3D vectors
                    case 11: e = new Entity106_11_2DLinearString(part,de); break;   //  Copious Data: 2D consecutive points
                    case 12: e = new Entity106_12_3DLinearString(part,de); break;   //  Copious Data: 3D consecutive points
                    case 13: e = new Entity106_13_3DVectorString(part,de); break;   //  Copious Data: 3D consecutive vectors
                    case 63: e = new Entity106_63_ClosedPlanarLinearCurve(part,de); break;  //  Copious Data: Closed planar curve string
                    default: e = new Entity106_XX_Unsupported(part,de); break;      //  Copious Data: Unsupported
                }
                break;
            case 108: switch(de.getForm()) {
                    case 0:  e = new Entity108_0_UnboundedPlane(part,de); break;    //  Unbounded plane
                    default: e = new Entity108_XX_Unsupported(part,de); break;      //  Plane: Unsupported
                }
                break;
            case 110: e = new Entity110_Line(part,de); break;                       //  Line segement
            case 116: e = new Entity116_Point(part,de); break;                      //  Point
            case 112: e = new Entity112_ParSplineCurve(part,de); break;             //  Parametric cubic spline curve
            case 118: switch(de.getForm()) {
                case 0:  e = new Entity118_0_RuledSurface(part,de); break;          //  Ruled Surface with arc-length spacing
                case 1:  e = new Entity118_1_RuledSurface(part,de); break;          //  Ruled Surface with parametric spacing
                default: e = new Entity118_XX_Unsupported(part,de); break;
            } break;
            case 120: e = new Entity120_SurfaceOfRevolution(part,de); break;        //  Surface of Revolution
            case 124: e = new Entity124_TransformationMatrix(part,de); break;       //  Transformation matrix
            case 126: e = new Entity126_BSplineCurve(part,de); break;               //  Rational B-Spline curve
            case 128: e = new Entity128_BSplineSurface(part,de); break;             //  Rational B-Spline surface
            case 142: e = new Entity142_CurveOnSurface(part,de); break;             //  Curve on Parametric Surface
            case 144: e = new Entity144_TrimmedSurface(part,de); break;             //  Trimmedn Parametric Surface
/*          case 132: e = new Entity132(part,de); break;                            //  Connect point
            case 212: switch(de.getForm()) {
                    case  0: e = new Entity212_00(part,de); break;                  //  Simple note
                    default: e = new Entity212_XX(part,de); break;                  //  Unsupported note
                }
                break;
            case 230: e = new Entity230(part,de); break;                            //  Sectioned area
            case 232: e = new Entity232(part,de); break;                            //  Annotation of inclusion of multimedia
*/          case 308: e = new Entity308_Subfigure(part,de); break;                  //  Subfigure definition
/*          case 320: e = new Entity320(part,de); break;                            //  Network subfigure definition
*/          case 402: switch(de.getForm()) {
                    case 15: e = new Entity402_15_OrderedGroupNoBackPointers(part,de); break;   //  Ordered group.
                    default: e = new Entity402_XX_Unsupported(part,de); break;      //  Unsupported associativity.
                }
                break;
            case 404: e = new Entity404_Drawing(part,de); drawings.add(e); break;   //  Drawing
/*          case 406: switch(de.getForm()) {
                    case  15: e = new Entity406_15(part,de);  break;                //  Name property
                    case  16: e = new Entity406_16(part,de);  break;                //  Drawing size property
                    case  17: e = new Entity406_17(part,de);  break;                //  Drawing units property
                    case  18: e = new Entity406_18(part,de);  break;                //  Intercharacter spacing property
                    case  38: e = new Entity406_38(part,de);  break;                //  URL anchor property
                    default:  e = new Entity406_XX(part,de);  break;                //  Unsupported property
                }
                break;
*/          case 408: e = new Entity408_SingularSubfigure(part,de); break;          //  Singular subfigure
            case 410: e = new Entity410_View(part,de); views.add(e); break;         //  View
/*          case 412: e = new Entity412(part,de); break;                            //  Rectangular array
            case 414: e = new Entity414(part,de); break;                            //  Circular array
            case 420: e = new Entity420(part,de); break;                            //  Network subfigure
*/          default:  e = new EntityXXX_Unsupported(part,de); break;                //  Unsupported entity
        }

        return e;
    }

    /**
     * Return an entity that corresponds to the specified GeomSS geometry object.
     *
     * @param part  The part that the entity is associated with.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS geometry to return an Entity for.
     * @return An Entity that corresponds to the specified geometry object or
     *         <code>null</code> if no match could be found.
     */
    public static GeomSSEntity create(Part part, int DEnum, GeomElement geom) {
        GeomSSEntity entity = null;
        int dim = geom.getPhyDimension();

        if (dim > 3) {
            part.getErrors().add(Constants.RESOURCES.getString("noMoreThan3D"));
            return entity;
        }

        if (geom instanceof GeomPoint) {
            entity = new Entity116_Point(part, DEnum, (GeomPoint)geom.toDimension(3));

        } else if (geom instanceof PointString) {
            if (dim == 2)
                entity = new Entity106_11_2DLinearString(part, DEnum, (PointString)geom);
            else
                entity = new Entity106_12_3DLinearString(part, DEnum, (PointString)geom.toDimension(3));

        } else if (geom instanceof GeomPlane) {
            entity = new Entity108_0_UnboundedPlane(part, DEnum, (GeomPlane)geom);

        } else if (geom instanceof NurbsCurve) {
            entity = new Entity126_BSplineCurve(part, DEnum, (NurbsCurve)geom.toDimension(3));

        } else if (geom instanceof LineSegment) {
            entity = new Entity110_Line(part, DEnum, (LineSegment)geom.toDimension(3));

        } else if (geom instanceof NurbsSurface) {
            entity = new Entity128_BSplineSurface(part, DEnum, (NurbsSurface)geom.toDimension(3));

        } else if (geom instanceof GeomList) {
            GeomList<GeomElement> glst = (GeomList)geom;
            //  Does it contain only vectors?
            if (isGeomVectorList(glst))
                entity = new Entity106_13_3DVectorString(part, DEnum, glst.toDimension(3));
        }

        return entity;
    }

    /**
     * Return true if the input list contains only instances of GeomVector.
     */
    private static boolean isGeomVectorList(GeomList<GeomElement> lst) {
        boolean isType = true;
        for (GeomElement elem : lst) {
            if (!(elem instanceof GeomVector)) {
                isType = false;
                break;
            }
        }
        return isType;
    }

    /**
     * Return an entity that corresponds to the a Type 402-15 Associativity (List).
     *
     * @param part     The part that the entity is associated with.
     * @param DEnum    The line count from the start of the Directory Entry Section for
     *                 this entry (odd number).
     * @param children The entities that are contained in this list.
     * @param name     The name to apply to the entity or null for no name.
     * @return An Entity that corresponds to the specified geometry object or
     *         <code>null</code> if no match could be found.
     */
    public static GeomSSEntity createList(Part part, int DEnum, List<Entity> children, String name) {
        if (children == null || children.isEmpty())
            return null;
        return new Entity402_15_OrderedGroupNoBackPointers(part, DEnum, children, name);
    }

}
