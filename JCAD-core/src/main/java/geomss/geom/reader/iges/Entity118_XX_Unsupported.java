/*
 *   Entity118_XX_Unsupported  -- Unsupported Ruled Surface entity.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

/**
 * <b><i>RULED SURFACE ENTITY</i></b> - This entity represents a ruled or 2-curve linearly
 * lofted surface. This form is currently unsupported.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 11, 2013
 * @version September 13, 2016
 */
public class Entity118_XX_Unsupported extends Entity {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity118_XX_Unsupported(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity118_XX constructor called");
        }
    }

    /**
     * Checks to see if the entity has any errors or warnings.
     */
    @Override
    public void check() {
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity128_XX - Ruled Surface - Unsupported";
    }

}
