/*
 *   Part  -- This class encapsulates the entire IGES file.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

/**
 * The Part class encapsulates the entire IGES file.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, CHP 7/31/98; 406-592 to 406-38 per IGES 6.0, Version 1.0
 * @version September 13, 2016
 */
public class Part {

    private final StartSection startSection = new StartSection();
    private final GlobalSection globalSection = new GlobalSection();
    private final List<Entity> entities = new ArrayList();
    private final List<String> errors = new ArrayList();
    private final List<Entity> drawings = new ArrayList();
    private final List<Entity> views = new ArrayList();

    /**
     * Directory of base file. Used for images and URL's.
     */
    public String dir = "";

    private static final int SORT_BY_DENUM = 1;
    private static final int SORT_BY_PDNUM = 2;

    /**
     * Default constructor. This instantiates all of the sub-sections in the IGES file.
     */
    public Part() {
    }

    /**
     * Read in Part from input IGES file. Reads in Directory Entries first, then Parameter
     * Data, and then the list of entities is checked for correctness. To allow Parameter
     * Data entries to not be in the same order as the Directory Entries, after the DE's
     * are read in, the entity list is sorted by PD numbers. Then the Parameter Data is
     * read in, and then the entity list is sorted by DE numbers again.
     *
     * @param in input file
     * @throws IOException if there is any problem reading the IGES file.
     */
    public void read(RandomAccessFile in) throws IOException {

        //  Go to the start of the IGES file.
        in.seek(0);

        //  Read in the header material.
        startSection.read(in);
        globalSection.read(in);
        Constants.initGlobals(globalSection);

        DirEntry de = new DirEntry();

        // Read in the directory entries.
        //  Allocate appropriate typed storage for all the entities (create the Entity objects).
        boolean flag;
        do {
            flag = de.read(in);

            if (flag) {
                Entity e = EntityFactory.create(this, drawings, views, de);
                entities.add(e);
            }

        } while (flag);

        // Sort by PD numbers so that they are read in correctly no matter how the PD's are ordered
        sort(0, entities.size() - 1, SORT_BY_PDNUM);

        // Read in all the entities
        for (Entity entity : entities) {
            entity.read(in);
            entity.check();
        }

        // Sort to DE number order
        sort(0, entities.size() - 1, SORT_BY_DENUM);

        //  Create the GeomSS geometry elements (if any).
        for (Entity entity : entities) {
            if (entity instanceof GeomSSEntity) {
                try {
                    ((GeomSSEntity)entity).createGeometry();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("IGES Read Error:  " + e.getMessage());
                    entity.addErrorMessage(e.getMessage());
                }
            }
        }

        // Now output any errors
        for (Entity entity : entities) {
            List<String> msgs = entity.getErrors();
            errors.addAll(msgs);
        }

    }

    /**
     * Write this Part to an IGES file.
     *
     * @param writer The PrintWriter to write the Part to.
     * @throws IOException if there is any problem writing the section.
     */
    public void write(PrintWriter writer) throws IOException {

        //  Initialize the global constants.
        Constants.initGlobals(globalSection);

        //  Start by writing out the Start Section.
        int nStart = startSection.write(writer);

        //  Write out the Global Section.
        int nGlobal = globalSection.write(writer);

        //  Write all the entity parameter data into a temporary buffer.
        //  This is needed to track the PDNum and PDCnt for each entity's directory entry.
        StringWriter str = new StringWriter();
        int PDnum;
        try (PrintWriter strWriter = new PrintWriter(str)) {
            PDnum = 1;
            for (Entity entity : entities) {
                if (entity.canWrite()) {
                    PDnum = entity.write(strWriter, PDnum);
                }
            }
        }
        int nPD = PDnum - 1;

        //  Write out the directory entries for each entity to be written out
        //  (now that we know the PDNum & PDCnt for each entry).
        int nDE = 0;
        for (Entity entity : entities) {
            if (entity.canWrite()) {
                DirEntry de = entity.getDirectoryEntry();
                de.write(writer);
                nDE += 2;
            }
        }

        //  Write out the parameter data buffered above.
        writer.write(str.toString());

        //  Finally, write out the terminate section.
        writer.print("S");      writer.print(Constants.makeSequenceNumber(nStart));
        writer.print("G");      writer.print(Constants.makeSequenceNumber(nGlobal));
        writer.print("D");      writer.print(Constants.makeSequenceNumber(nDE));
        writer.print("P");      writer.print(Constants.makeSequenceNumber(nPD));
        writer.print("                                        ");
        writer.print("T");      writer.print(Constants.makeSequenceNumber(1));

    }

    /**
     * Get the list of error strings built up after checking the entities.
     *
     * @return list of error strings
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * Return the Start Section object.
     *
     * @return Start Section
     */
    public StartSection getStartSection() {
        return startSection;
    }

    /**
     * Return the Global Section object.
     *
     * @return Global Section
     */
    public GlobalSection getGlobalSection() {
        return globalSection;
    }

    /**
     * Return the entity count.
     *
     * @return number of entities in part
     */
    public int getEntityCount() {
        return entities.size();
    }

    /**
     * Return list of entities.
     *
     * @return list of all entities in part
     */
    public List<Entity> getEntities() {
        return entities;
    }

    /**
     * Return entity with specified DE number
     *
     * @param de DE number of entity to be returned
     * @return entity with specified DE number
     */
    public Entity getEntity(int de) {
        return entities.get((de - 1) / 2);
    }

    /**
     * Return a specific entity's header for the Entity List window.
     *
     * @param i index of entity in list (not DE number)
     * @return String to be placed in Entity List window
     */
    public String getEntityHeader(int i) {
        return entities.get(i).getHeader();
    }

    /**
     * Return a specific entity as a String. This is the Directory Entry and Parameter
     * Data for the requested index.
     *
     * @param i index of entity in list (not DE number)
     * @return string with entity's information
     */
    public String toString(int i) {
        return entities.get(i).toString();
    }

    /**
     * Return info on the whole part as a String. This includes the number of entities.
     *
     * @return string containing info on whole part
     */
    @Override
    public String toString() {
        String outStr = "Part:\n";

        outStr = outStr + "# Entities = " + entities.size();

        return outStr;
    }

    /**
     * Sort the entities. This uses the QuickSort algorithm.
     *
     * @param lo0  low index of range to sort
     * @param hi0  high index of range to sort
     * @param flag either SORT_BY_DENUM or SORT_BY_PDNUM
     */
    private void sort(int lo0, int hi0, int flag) {
        int lo = lo0;
        int hi = hi0;
        int mid = lo0;

        if (hi0 > lo0) {
            if (flag == SORT_BY_DENUM)
                mid = entities.get((lo0 + hi0) / 2).getDENum();
            else if (flag == SORT_BY_PDNUM)
                mid = entities.get((lo0 + hi0) / 2).getPDNum();

            while (lo <= hi) {
                if (flag == SORT_BY_DENUM) {
                    while ((lo < hi0) && entities.get(lo).getDENum() < mid)
                        ++lo;

                    while ((hi > lo0) && entities.get(hi).getDENum() > mid)
                        --hi;

                } else if (flag == SORT_BY_PDNUM) {
                    while ((lo < hi0) && entities.get(lo).getPDNum() < mid)
                        ++lo;

                    while ((hi > lo0) && entities.get(hi).getPDNum() > mid)
                        --hi;
                }

                if (lo <= hi) {
                    swap(lo, hi);
                    ++lo;
                    --hi;
                }
            }

            if (lo0 < hi)
                sort(lo0, hi, flag);

            if (lo < hi0)
                sort(lo, hi0, flag);
        }
    }

    /**
     * Swap routine. Used in QuickSort algorithm.
     *
     * @param i index of 1st value to swap
     * @param j index of 2nd value to swap
     */
    private void swap(int i, int j) {
        Entity T = entities.get(i);
        entities.set(i, entities.get(j));
        entities.set(j, T);
    }
}
