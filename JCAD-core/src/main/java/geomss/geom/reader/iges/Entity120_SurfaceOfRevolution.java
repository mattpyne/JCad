/*
 *   Entity120_SurfaceOfRevolution  -- An entity representing a Surface of Revolution Entity.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.NurbsCurve;
import geomss.geom.nurbs.NurbsSurface;
import geomss.geom.nurbs.SurfaceFactory;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;

/**
 * <b><i>SURFACE OF REVOLUTION ENTITY</i></b> - This entity represents a surface of
 * revolution. A surface of revolution is defined by an axis of rotation (which shall be a
 * Line Entity), a generatrix, and start and terminate rotation angles. The surface is
 * created by rotating the generatrix about the axis of rotation through the start and
 * terminating angles.
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a NURBS curve. This entity
 * type can not be written out to an IGES file. The surface parameters are stored in the
 * user data with the prefix "IGES_120_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 9, 2013
 * @version September 13, 2016
 */
public class Entity120_SurfaceOfRevolution extends GeomSSEntity {

    private int Lde;            //  Pointer to the DE of the Line Entity (axis of revolution)
    private int Cde;            //  Pointer to the DE of the generatrix entity
    private double SA;          //  Start angle in radians
    private double TA;          //  Terminate angle in radians

    private NurbsSurface srf;   //  The GeomSS surface this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity120_SurfaceOfRevolution(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity120 constructor called");
        }

    }

    /**
     * Checks to see if the entity is correct.
     */
    @Override
    public void check() {
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity120.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        Lde = getInt(s);            //  Pointer to the DE of the Line Entity (axis of revolution)
        Cde = getInt(s);            //  Pointer to the DE of the generatrix entity
        SA = getReal(s);            //  Start angle in radians
        TA = getReal(s);            //  Terminate angle in radians

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        Part part = getPart();
        Parameter<Length> tol = Parameter.valueOf(Constants.Grain, Constants.unit);

        //  Create the surface of revolution from the specified axis, curve, and angles.
        
        //  Get the axis of rotation.
        GeomVector<Dimensionless> axis;
        Entity entity = part.getEntity(Lde);
        if (entity instanceof GeomSSEntity) {
            //  Found a GeomSS geometry Entity.
            GeomSSEntity geomEntity = (GeomSSEntity)entity;
            geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
            LineSegment L = (LineSegment)geomEntity.getGeomElement(GTransform.IDENTITY);
            axis = L.getUnitVector();
            axis.setOrigin(L.getStart().immutable());
        } else
            return;

        //  Get the generatrix curve
        NurbsCurve C;
        entity = part.getEntity(Cde);
        if (entity instanceof GeomSSEntity) {
            //  Found a GeomSS geometry Entity.
            GeomSSEntity geomEntity = (GeomSSEntity)entity;
            geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
            Curve crv = (Curve)geomEntity.getGeomElement(GTransform.IDENTITY);
            C = crv.toNurbs(tol);
        } else
            return;

        //  Create the angle objects required.
        Parameter<Angle> thetaS = Parameter.valueOf(SA, SI.RADIAN);
        Parameter<Angle> thetaE = Parameter.valueOf(TA, SI.RADIAN);

        //  Create the surface of revolution.
        srf = SurfaceFactory.createRevolvedSurface(axis, C, thetaS, thetaE);

        //  Apply meta-data.
        srf.putUserData("IGES_120_L", axis);
        srf.putUserData("IGES_120_C", C);
        srf.putUserData("IGES_120_SA", thetaS);
        srf.putUserData("IGES_120_TA", thetaE);

        Double u0 = (Double)C.getUserData("IGES_U0");
        if (u0 == null)
            u0 = 0.0;
        Double u1 = (Double)C.getUserData("IGES_U1");
        if (u1 == null)
            u1 = 1.0;
        srf.putUserData("IGES_U0", u0);
        srf.putUserData("IGES_U1", u1);
        srf.putUserData("IGES_V0", SA);
        srf.putUserData("IGES_V1", TA);

    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return srf;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity120 - Surface of Revolution";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        return outStr.toString();
    }

}
