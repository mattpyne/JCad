/*
 *   Entity102_CompositeCurve  -- Entity that represents a composite curve made up of other curve segments.
 *
 *   Copyright (C) 2011-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.CurveUtils;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import javax.measure.quantity.Length;

/**
 * <b><i>COMPOSITE CURVE ENTITY</i></b> - This entity defines an associativity
 * relationship between an ordered list of curve segments. A composite curve is defined as
 * an ordered list of entities consisting of a point, connect point and parameterized
 * curve entities.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a single NurbsCurve with the
 * original segments stored in the list "IGES_102_CCSegs" in the user data of the curve.
 * This entity type can be written out to an IGES file.</p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: November 16, 2011
 * @version December 24, 2013
 */
public class Entity102_CompositeCurve extends GeomSSEntity {

    protected List<Integer> pointers = new ArrayList();     //  List of entity DE numbers.

    private NurbsCurve curve = null;                        //  The NURBS curve that will represent the composit curve.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity102_CompositeCurve(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity102 constructor called");
        }
    }

    /**
     * Create this entity from the specified list of pointers to the curve segment DEs.
     *
     * @param part      The Part to which this entity is contained.
     * @param DEnum     The line count from the start of the Directory Entry Section for
     *                  this entry (odd number).
     * @param name      The GeomSS name for this entity or <code>null</code> for none.
     * @param segDEPtrs A list of pointers to the DE each segment of the curve in order
     *                  from start to end.
     */
    public Entity102_CompositeCurve(Part part, int DEnum, String name, List<Integer> segDEPtrs) {
        super(part, new DirEntry(102, 0, DEnum, 0, name));

        pointers.addAll(segDEPtrs);
    }

    /**
     * Checks to see if the entity is correct. No restrictions are imposed.
     */
    @Override
    public void check() { }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        int n = getInt(s);
        for (int i = 0; i < n; ++i)
            pointers.add(getInt(s));

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() {
        Part part = getPart();

        //  Loop over all the entities in this association.
        GeomList<GeomElement> geom = GeomList.newInstance();
        int n = pointers.size();
        for (int i = 0; i < n; ++i) {
            int deNum = pointers.get(i);
            Entity entity = part.getEntity(deNum);

            if (entity instanceof GeomSSEntity) {
                //  Found a GeomSS geometry Entity.
                GeomSSEntity geomEntity = (GeomSSEntity)entity;
                geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
                GeomElement element = geomEntity.getGeomElement(GTransform.IDENTITY);
                geom.add(element);
            }
        }

        //  Convert all the non-degenerate elements into NURBS curves.
        Parameter<Length> tol = Parameter.valueOf(Constants.Grain, Constants.unit);
        GeomList<NurbsCurve> crvSegs = convert2NurbsSegs(geom, tol);

        //  Concatentate all the curve segments together.
        curve = CurveUtils.connectCurves(crvSegs);
        GeomList.recycle(crvSegs);

        //  TODO:  Remove unnecessary knotsU to return the minimal curve that matches the original.
        curve.putUserData("IGES_102_CCSegs", geom);

    }

    /**
     * Converts all the non-degenerate Curve elements in the specified geometry list into
     * NurbsCurves.
     *
     * @param crvs The list of geometry elements to be converted.
     * @param tol  The tolerance for the conversion to NURBS curves.
     * @return A list of non-degenerate NURBS curve versions of each element in "crvs".
     */
    private GeomList<NurbsCurve> convert2NurbsSegs(GeomList<GeomElement> crvs, Parameter<Length> tol) {
        GeomList<NurbsCurve> nurbsSegs = GeomList.newInstance();
        for (GeomElement elem : crvs) {
            if (elem instanceof Curve) {
                NurbsCurve nurbs = ((Curve)elem).toNurbs(tol);
                if (!nurbs.isDegenerate(tol))
                    nurbsSegs.add(nurbs);
            }
        }
        return nurbsSegs;
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return curve;
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entity object's parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {
        int numSegs = pointers.size();

        //  Build up the parameter data string.
        StringBuilder buffer = new StringBuilder();
        buffer.append(102);             buffer.append(Constants.Delim);
        buffer.append(numSegs);         buffer.append(Constants.Delim);
        buffer.append(pointers.get(0));
        for (int i = 1; i < numSegs; ++i) {
            buffer.append(Constants.Delim);
            buffer.append(pointers.get(i));
        }
        buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        int n = pointers.size();
        outStr.append("n  = "); outStr.append(n);   outStr.append("\n");

        for (int i = 0; i < n; i++) {
            outStr.append("curve(");
            outStr.append(i);
            outStr.append(") = ");
            outStr.append(pointers.get(i));
            outStr.append("\n");
        }

        return outStr.toString();
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity102 - Composite Curve";
    }

}
