/*
 *   Entity116_Point  -- A Point entity.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomPoint;
import geomss.geom.Transformable;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.MessageFormat;

/**
 * <b><i>POINT ENTITY</i></b> - This entity defines a point which may be isolated or used
 * as a component of a Composite Curve Entity or a subfigure. A point is defined by its
 * coordinates in definition space.
 * 
 * <p>
 * This entity type <b>can</b> be written out to an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Entity116_Point extends GeomSSEntity {

    private GeomPoint p;    // Point coordinates
    private int ptr = 0;    // Display symbol

    /**
     * Default constructor.
     *
     * @param prt part to which this entity is contained
     * @param de  Directory Entry for this entity
     */
    public Entity116_Point(Part prt, DirEntry de) {
        super(prt, de);

        if (Constants.DEBUG) {
            System.out.println("Entity116 constructor called");
        }

    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part in which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS GeomPoint geometry to return an Entity for.
     */
    public Entity116_Point(Part part, int DEnum, GeomPoint geom) {
        super(part, new DirEntry(116, 0, DEnum, 0, geom.getName()));
        p = geom;
    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     *
     * - The Label Display Pointer shall be 0
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // DE Label Display Pointer shall be 0
        if (DE.getLblDsp() != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("labelDisplay"), DE.getLblDsp());
            addErrorMessage(getWarningString(msg));
        }

    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        if (Constants.DEBUG) {
            System.out.println("Entity116.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        p = getPoint3(s);
        ptr = getInt(s);

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        //  Already done in read().
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return p;
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        GeomPoint point = p.to(Constants.unit);
        StringBuilder buffer = new StringBuilder();
        buffer.append(116);                 buffer.append(Constants.Delim);
        appendPoint3(buffer, point);
        buffer.append(ptr);                 buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity116 - Point";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        outStr.append("p = ");
        outStr.append(p.toString());
        outStr.append("\n");

        outStr.append("ptr = ");
        outStr.append(ptr);

        return outStr.toString();
    }

}
