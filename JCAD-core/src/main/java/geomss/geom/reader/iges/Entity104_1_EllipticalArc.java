/*
 *   Entity104_1_EllipticalArc  -- Entity representing an ellipse or elliptical arc.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

/**
 * <b><i>CONIC ARC ENTITY - ELLIPSE</i></b> - This entity defines an ellipse or elliptical
 * arc.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a NURBS curve of degreeU 2
 * (IGES type 126, Form 2, Degree 2). This entity type can not be written out to an IGES
 * file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 * @see Entity104_ConicArc
 */
public class Entity104_1_EllipticalArc extends Entity104_ConicArc {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity104_1_EllipticalArc(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity104_1 constructor called");
        }
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity104_1 - Conic Arc - Ellipse";
    }

}
