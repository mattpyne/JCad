/*
 *   Constants  -- Global variables and methods used for reading an IGES file.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

/**
 * The Constants class is a holding area for any "global" variables that are needed for
 * multiple classes. Also included are several utility methods that may be used any place
 * that needs them. It is hoped that there will be no hard-coded values anywhere.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Constants {

    /**
     * Sets debug mode.
     */
    public static final boolean DEBUG = false;

    /**
     * The String resources used by this package.
     */
    public static final ResourceBundle RESOURCES
            = ResourceBundle.getBundle("geomss.geom.reader.iges.IGESResources", java.util.Locale.getDefault());

    /**
     * The IGES ASCII file format must always be written using the US locale.
     */
    public static final Locale US = Locale.US;

    /**
     * Column in IGES file with section identifier ('S', 'G', etc)
     */
    public static final int SECTID_COL = 72;

    /**
     * Column in IGES file Parameter Data delimiting free-form data with DENum of the
     * entity. According to the IGES specification, this must be a space (' ').
     */
    public static final int PDID_COL = 64;

    /**
     * String type identifier. Used when reading in parameters from file.
     */
    public static final int TYPE_STRING = 0;

    /**
     * Integer type identifier. Used when reading in parameters from file.
     */
    public static final int TYPE_INT = 1;

    /**
     * Real type identifier. Used when reading in parameters from file.
     */
    public static final int TYPE_FLOAT = 2;

    /**
     * Character type identifier. Used when reading in parameters from file.
     */
    public static final int TYPE_CHAR = 3;

    /**
     * Globalized terminate character. Done this way because Part.getGlobal().getTerm()
     * was too long to type over and over.
     */
    public static char Term;

    /**
     * Globalized delimiter character. Done this way because Part.getGlobal().getDelim()
     * was too long to type over and over.
     */
    public static char Delim;

    /**
     * Globalized grain value. Done this way because Part.getGlobal().getGrain() was too
     * long to type over and over.
     */
    public static double Grain;

    /**
     * Globalized length units for the file. Done this way because
     * Part.getGlobal().getUnit() was too long to type over and over.
     */
    public static Unit<Length> unit = SI.METER;

    /**
     * X-Axis
     */
    public static final int X_AXIS = 0;

    /**
     * Y-Axis
     */
    public static final int Y_AXIS = 1;

    /**
     * Z-Axis
     */
    public static final int Z_AXIS = 2;

    /**
     * Standard IGES Colors (Index 0, "undefined" is set to black).
     */
    public static final Color IGESColor[] = new Color[9];

    /**
     * Initialize IGES color array. Sets up array with standard IGES colors. Index 0,
     * "undefined", will be displayed as black.
     */
    static {
        IGESColor[0] = Color.BLACK;
        IGESColor[1] = Color.BLACK;
        IGESColor[2] = Color.RED;
        IGESColor[3] = Color.GREEN;
        IGESColor[4] = Color.BLUE;
        IGESColor[5] = Color.YELLOW;
        IGESColor[6] = Color.MAGENTA;
        IGESColor[7] = Color.CYAN;
        IGESColor[8] = Color.WHITE;
    }

    /**
     * Set up globals from GlobalSection
     *
     * @param g Global Section object
     */
    public static void initGlobals(GlobalSection g) {
        Term = g.getTerm();
        Delim = g.getDelim();
        Grain = g.getGrain();
        unit = g.getUnit();
    }

    /**
     * Reads in a line of text, 80 characters, ignoring any EOL chars
     *
     * @param in input file
     * @return the single text line of input
     * @exception IOException if end of file is reached, or other generic file I/O error.
     */
    public static String myReadLine(RandomAccessFile in) throws IOException {
        char[] line = new char[80];
        int i;

        do {
            for (i = 0; i < 80; i++) {
                char ch = (char)in.readByte();

                if ((ch == '\r') || (ch == '\n'))
                    break;

                line[i] = ch;
            }
        } while (i == 0);

        return new String(line);
    }

    /**
     * Converts the string to an integer. This method can handle "integers" that are
     * technically real, such as 1.000D+001. A zero length string results in the value
     * 0 being returned.
     *
     * @param s the string to be parsed. Assumed to be trimmed of whitespace.
     * @return resulting integer
     */
    public static int toInt(String s) {
        if (s.length() == 0)
            return 0;

        if ((s.indexOf('E') < 0) && (s.indexOf('e') < 0) && (s.indexOf('D') < 0) && (s.indexOf('d') < 0)
                && s.indexOf('.') < 0) {
            return Integer.parseInt(s);

        } else {
            s = s.replace('d', 'e');
            s = s.replace('D', 'e');
            return (Double.valueOf(s)).intValue();
        }
    }

    /**
     * Converts the string to an double. This method can handle IGES floats of single
     * ("1.00e+010") or double ("1.00d+010") precision.  A zero length string results
     * in the value 0.0 being returned.
     *
     * @param s the string to be parsed. Assumed to be trimmed of whitespace.
     * @return resulting double value
     */
    public static double toDouble(String s) {
        if (s.length() == 0)
            return 0.0;

        s = s.replace('d', 'e');
        s = s.replace('D', 'e');

        return Double.valueOf(s);
    }

    /**
     * 2D distance. Distance between two points in a plane.
     *
     * @param x1 abscissa of first point
     * @param y1 ordinate of first point
     * @param x2 abscissa of 2nd point
     * @param y2 ordinate of second point
     * @return 2D distance between the points
     */
    public static double dist(double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        return Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * 3D distance. Distance between two points in space.
     *
     * @param x1 X-coordinate of
     * @param y1 Y-coordinate of
     * @param z1 Z-coordinate of first point
     * @param x2 X-coordinate of second point
     * @param y2 Y-coordinate of second point
     * @param z2 Z-coordinate of second point
     * @return 3D distance between the points
     */
    public static double dist(double x1, double y1, double z1, double x2, double y2, double z2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double dz = z2 - z1;
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    /**
     * Add the specified pad character to the left of the input String until it is the
     * specified length.
     *
     * @param string The string to be made the specified length.
     * @param pad    The character to insert into the beginning of the string to reach the
     *               specified length.
     * @param length The desired length of the padded string.
     * @return The input string padded with the specified pad character on the left until
     *         the length matches the specified length.
     */
    public static String padLeft(String string, char pad, int length) {
        StringBuilder buffer = new StringBuilder(string);
        while (buffer.length() < length) {
            buffer.insert(0, pad);
        }
        return buffer.toString();
    }

    /**
     * Add the specified pad character to the right of the input String until it is the
     * specified length.
     *
     * @param string The string to be made the specified length.
     * @param pad    The character to append onto the string to reach the specified
     *               length.
     * @param length The desired length of the padded string.
     * @return The input string padded with the specified pad character on the right until
     *         the length matches the specified length.
     */
    public static String padRight(String string, char pad, int length) {
        if (string.length() < length) {
            StringBuilder buffer = new StringBuilder(string);
            for (int i = buffer.length(); i < length; ++i) {
                buffer.append(pad);
            }
            string = buffer.toString();
        }
        return string;
    }

    /**
     * Create a sequence number using the specified integer and padding it with zeros on
     * the left until it is 7 characters long.
     *
     * @param number The number to turn into a sequence number.
     * @return A properly formatted sequence number made from the input integer.
     */
    public static String makeSequenceNumber(int number) {
        return padLeft(String.valueOf(number), '0', 7);
    }

    /**
     * Write out a single section of an IGES file. The section is provided as a single
     * String that contains no line-breaks or control characters. The output will be
     * limited to 72 characters plus a section code and sequence index.
     *
     * @param writer      The PrintWriter to write the section to.
     * @param startIndex  The starting sequence index for this section.
     * @param deNumber    The 7 character DE number code (made with "makeSequenceNumber")
     *                    for this part or "".
     * @param sectionCode The code used to identify this section's type ('S', 'G', 'P',
     *                    etc").
     * @param data        A String containing the data for the section.
     * @return The index for the next sequence following this section.
     * @throws java.io.IOException
     */
    public static int writeSection(PrintWriter writer, int startIndex, String deNumber,
            char sectionCode, StringBuilder data) throws IOException {
        if (DEBUG)
            System.out.println("length = " + data.length() + ", data = " + data);

        int deNumberLength = deNumber.length();
        int strLen = 71 - deNumberLength;
        boolean notDone;
        do {
            notDone = data.length() > strLen;
            if (notDone) {
                String tmp = data.substring(0, strLen);
                int idx = tmp.lastIndexOf(',') + 1;
                tmp = data.substring(0, idx);
                writer.print(padRight(tmp, ' ', strLen));
                data = data.delete(0, idx);

            } else {
                writer.print(padRight(data.toString(), ' ', strLen));
            }

            writer.print(' ');
            writer.print(deNumber);
            writer.print(sectionCode);
            writer.println(makeSequenceNumber(startIndex++));

        } while (notDone);

        return startIndex;
    }

}
