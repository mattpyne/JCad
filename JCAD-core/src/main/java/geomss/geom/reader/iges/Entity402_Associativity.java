/*
 *   Entity402_Associativity  -- Entity that represents an associativity relationship between entities.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * <b><i>ASSOCIATIVITY ENTITY</i></b> - This entity defines an associativity relationship
 * between other entities.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a list of geometry objects.
 * This entity type can not be written out to an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, AED, Version 1.0
 * @version September 13, 2016
 */
public abstract class Entity402_Associativity extends GeomSSEntity {

    protected List<Integer> pointers = new ArrayList(); //  List of entity DE numbers.

    protected GeometryList geom = GeomList.newInstance();   //  List of GeomSS geometry objects.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity402_Associativity(Part p, DirEntry de) {
        super(p, de);
    }

    /**
     * Checks to see if the entity is correct. No restrictions are imposed.
     */
    @Override
    public void check() {
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        int n = getInt(s);
        for (int i = 0; i < n; ++i)
            pointers.add(getInt(s));

        super.read_additional();
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() {
        Part part = getPart();

        //  Loop over all the entities in this association.
        int n = pointers.size();
        for (int i = 0; i < n; ++i) {
            int deNum = pointers.get(i);
            Entity entity = part.getEntity(deNum);

            if (entity instanceof GeomSSEntity) {
                //  Found a GeomSS geometry Entity.
                GeomSSEntity geomEntity = (GeomSSEntity)entity;
                geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
                GeomElement element = geomEntity.getGeomElement(GTransform.IDENTITY);
                geom.add(element);
            }
        }
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return geom;
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        int n = pointers.size();
        outStr.append("n  = ");
        outStr.append(n);
        outStr.append("\n");

        for (int i = 0; i < n; i++) {
            outStr.append("entity(");
            outStr.append(i);
            outStr.append(") = ");
            outStr.append(pointers.get(i));
            outStr.append("\n");
        }

        return outStr.toString();
    }

}
