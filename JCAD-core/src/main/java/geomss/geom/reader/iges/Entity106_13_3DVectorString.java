/*
 *   Entity106_13_3DVectorString  -- Entity that represents a string of of 3D points with associated vectors.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomList;
import geomss.geom.GeomPoint;
import geomss.geom.GeomVector;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <b><i>COPIOUS DATA ENTITY - LINEAR STRING WITH VECTORS</i></b> - This entity defines an
 * ordered set of 3D coordinate points with associated 3D vectors. Form 13 indicates that
 * all the points are 3D, ordered, and have associated 3D vectors.
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a list of vectors
 * (GeomList&lt;GeomVector&gt;). Each vector has the origin set to the appropriate point.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 14, 2013
 * @version September 13, 2016
 * @see Entity106_CopiousData
 */
public class Entity106_13_3DVectorString extends Entity106_CopiousData {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity106_13_3DVectorString(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity106_13 constructor called");
        }
    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part in which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS list of GeomVector geometry to return an Entity for.
     */
    public Entity106_13_3DVectorString(Part part, int DEnum, GeomList geom) {
        super(part, new DirEntry(106, 13, DEnum, 0, geom.getName()));
        ip = 2;
        points.addAll(geom);
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        GeomList<GeomVector> points2 = (GeomList<GeomVector>)points.to(Constants.unit);
        StringBuilder buffer = new StringBuilder();
        buffer.append(106);                 buffer.append(Constants.Delim);
        buffer.append(ip);                  buffer.append(Constants.Delim);
        int n = points2.size();
        buffer.append(n);                   buffer.append(Constants.Delim);
        for (GeomVector v : points2) {
            GeomPoint p = v.getOrigin();
            appendPoint3(buffer, p);
            double i = v.getValue(0);
            double j = v.getValue(1);
            double k = v.getValue(2);
            buffer.append(i);               buffer.append(Constants.Delim);
            buffer.append(j);               buffer.append(Constants.Delim);
            buffer.append(k);               buffer.append(Constants.Delim);
        }
        buffer.setLength(buffer.length() - 1);  //  Replace Delim with Term for last entry.
        buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity106_13 - Copious Data - String of 3D Vectors";
    }

}
