/*
 *   View  -- This class represents an IGES view and its transformation.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.Point;

/**
 * The View class represents an IGES view and its transformation. This is mainly to keep
 * each view's information organized in one List instead of three or four Lists.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class View {

    /**
     * View DE number
     */
    public int viewde;

    /**
     * Origin 2D point
     */
    public Point origin;

    /**
     * Rotation angle
     */
    public double angle;

    /**
     * Default constructor.
     */
    public View() {
        origin = Point.newInstance(2);
    }

    /**
     * Copy constructor.
     *
     * @param v The view to copy.
     */
    public View(View v) {
        viewde = v.viewde;
        origin = Point.valueOf(v.origin);
        angle = v.angle;
    }

    /**
     * Initialization constructor
     *
     * @param de view DE number
     * @param or origin point
     * @param an angle
     */
    public View(int de, Point or, double an) {
        viewde = de;
        origin = or;
        angle = an;
    }

    /**
     * Set the values for the view.
     *
     * @param de view DE number
     * @param or origin point
     * @param an angle
     */
    public void set(int de, Point or, double an) {
        viewde = de;
        origin = or;
        angle = an;
    }

    /**
     * Dump to string.
     *
     * @return string value
     */
    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
		buffer.append("  viewde = ");   buffer.append(viewde);              buffer.append("\n");
		buffer.append("  origin = ");   buffer.append(origin.toString());   buffer.append("\n");
		buffer.append("   angle = ");   buffer.append(angle);

        return buffer.toString();
    }
}
