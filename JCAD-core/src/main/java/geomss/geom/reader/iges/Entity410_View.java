/*
 *   Entity410_View  -- This class represents the position of the model in a drawing entity.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;

/**
 * <b><i>VIEW ENTITY</i></b> - This entity specifies the position of the model entities
 * when referenced from a Drawing Entity.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Entity410_View extends Entity {

    private int vno;        // View number
    private double scale;   // Scale factor (default = 1.0)
    private int xvminp;     // DE number to left side of view volume (XVMIN plane) or zero
    private int yvmaxp;     // DE number to top of view volume (YVMAX plane) or zero
    private int xvmaxp;     // DE number to right side of view volume (XVMAX plane) or zero
    private int yvminp;     // DE number to bottom of view volume (YVMIN plane) or zero
    private int zvminp;     // DE number to back of view volume (ZVMIN plane) or zero
    private int zvmaxp;     // DE number to front of view volume (ZVMAX plane) or zero

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity410_View(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity410 constructor called");
        }
    }

    /**
     * Checks to see if the entity should be drawn. The following restrictions are
     * imposed:
     *
     * - The Transformation Matrix shall be 0 - The view volume pointers shall all be 0
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // DE Matrix shall be 0
        if (DE.getMatrix() != 0) {
            String msg = MessageFormat.format(RESOURCES.getString("matrixNotZero"), DE.getMatrix());
            addErrorMessage(getWarningString(msg));
        }

        // View volume pointers shall all be 0
        if ((xvminp != 0) || (yvminp != 0) || (zvminp != 0)
                || (xvmaxp != 0) || (yvmaxp != 0) || (zvmaxp != 0)) {
            String msg = RESOURCES.getString("viewVolPointersNotZero");
            addErrorMessage(getWarningString(msg));
        }
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        if (Constants.DEBUG) {
            System.out.println("Entity410.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        vno = getInt(s);
        scale = getReal(s, 1.0);
        xvminp = getInt(s);
        yvmaxp = getInt(s);
        xvmaxp = getInt(s);
        yvminp = getInt(s);
        zvminp = getInt(s);
        zvmaxp = getInt(s);

        super.read_additional();
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity410 - View";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        outStr.append("vno    = ");     outStr.append(vno);     outStr.append("\n");
        outStr.append("scale  = ");     outStr.append(scale);   outStr.append("\n");
        outStr.append("xvminp = ");     outStr.append(xvminp);  outStr.append("\n");
        outStr.append("yvmaxp = ");     outStr.append(yvmaxp);  outStr.append("\n");
        outStr.append("xvmaxp = ");     outStr.append(xvmaxp);  outStr.append("\n");
        outStr.append("yvminp = ");     outStr.append(yvminp);  outStr.append("\n");
        outStr.append("zvminp = ");     outStr.append(zvminp);  outStr.append("\n");
        outStr.append("zvmaxp = ");     outStr.append(zvmaxp);

        return outStr.toString();
    }

}
