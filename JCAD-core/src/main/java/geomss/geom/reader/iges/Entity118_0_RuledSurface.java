/*
 *   Entity118_0_RuledSurface  -- An entity representing an Arc-Length Space Ruled Surface Entity.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.Curve;
import geomss.geom.GTransform;
import geomss.geom.GeomElement;
import geomss.geom.LoftedSurface;
import geomss.geom.nurbs.CurveUtils;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import javax.measure.quantity.Length;

/**
 * <b><i>RULED SURFACE ENTITY</i></b> - This entity represents a ruled or 2-curve linearly
 * lofted surface. A ruled surface is formed by moving a line connecting points of equal
 * relative arc length (Form 0) on two parametric curves from a start point to a terminate
 * point on the curves.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a LoftedSurface surface. This
 * entity type can not be written out to an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 11, 2013
 * @version September 13, 2016
 */
public class Entity118_0_RuledSurface extends Entity118_RuledSurface {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity118_0_RuledSurface(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity118_0 constructor called");
        }

    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        Part part = getPart();
        Parameter<Length> tol = Parameter.valueOf(Constants.Grain, Constants.unit);

        //  Get the member curves.
        Entity crv1_entity = part.getEntity(DE1);
        Entity crv2_entity = part.getEntity(DE2);
        if (crv1_entity instanceof GeomSSEntity && crv2_entity instanceof GeomSSEntity) {
            GeomSSEntity gcrv1_entity = (GeomSSEntity)crv1_entity;
            gcrv1_entity.setUsedInList(true);   //  Indicate that the entity is used by this association.
            GeomSSEntity gcrv2_entity = (GeomSSEntity)crv2_entity;
            gcrv2_entity.setUsedInList(true);   //  Indicate that the entity is used by this association.

            GeomElement elem1 = gcrv1_entity.getGeomElement(GTransform.IDENTITY);
            GeomElement elem2 = gcrv2_entity.getGeomElement(GTransform.IDENTITY);
            if (elem1 != null && elem2 != null && elem1 instanceof Curve && elem2 instanceof Curve) {
                //  Convert member curves to NURBS
                NurbsCurve crv1 = ((Curve)elem1).toNurbs(tol);
                NurbsCurve crv2 = ((Curve)elem2).toNurbs(tol);

                //  Deal with the direction flag.
                if (DIRFLG == 1) {
                    crv2 = crv2.reverse();
                }

                //  Convert the input curves from parametric spacing to arc-length spacing.
                NurbsCurve C1 = CurveUtils.arcLengthParameterize(crv1, tol);
                NurbsCurve C2 = CurveUtils.arcLengthParameterize(crv2, tol);

                //  Create the ruled/lofted surface.
                srf = LoftedSurface.valueOf(1, C1, C2);
            }
        }

    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity118_0 - Ruled Surface";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        return outStr.toString();
    }

}
