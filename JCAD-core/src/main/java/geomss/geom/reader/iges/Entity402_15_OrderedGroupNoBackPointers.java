/*
 *   Entity402_15_OrderedGroupNoBackPointers  -- Entity that represents an ordered group without back pointers.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * <b><i>ASSOCIATIVITY ENTITY - ORDERED GROUP WITHOUT BACK POINTERS</i></b> - This entity
 * allows an ordered group of entities without back pointers to be maintained as a single,
 * logical entity.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 * @see Entity402_Associativity
 */
public class Entity402_15_OrderedGroupNoBackPointers extends Entity402_Associativity {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity402_15_OrderedGroupNoBackPointers(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity402_15 constructor called");
        }
    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part     The Part in which this entity is contained.
     * @param DEnum    The line count from the start of the Directory Entry Section for
     *                 this entry (odd number).
     * @param children A list of the entities contained in this list.
     * @param name     The name to apply to the entity or null for no name.
     */
    public Entity402_15_OrderedGroupNoBackPointers(Part part, int DEnum, List<Entity> children, String name) {
        super(part, new DirEntry(402, 15, DEnum, 0, name));
        for (Entity entity : children) {
            pointers.add(entity.getDENum());
        }
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        int n = pointers.size();
        int nm1 = n - 1;
        StringBuilder buffer = new StringBuilder();
        buffer.append(402);                 buffer.append(Constants.Delim);
        buffer.append(n);                   buffer.append(Constants.Delim);
        for (int i = 0; i < n; ++i) {
            buffer.append(pointers.get(i));
            if (i != nm1)
                buffer.append(Constants.Delim);
            else
                buffer.append(Constants.Term);
        }

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity402_15 - Ordered Group Without Back Pointers";
    }

}
