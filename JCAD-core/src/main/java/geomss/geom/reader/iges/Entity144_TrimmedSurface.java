/*
 *   Entity144_TrimmedSurface  -- An entity representing a Trimmed Parametric Surface.
 *
 *   Copyright (Ccrv) 2013-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.CurveUtils;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;

/**
 * <b><i>TRIMMED PARAMETRIC SURFACE ENTITY</i></b> - This entity represents a surface that
 * is a subset of another surface bounded on the outside by a boundary curve and on the
 * inside (holes) with a series of boundary curves.
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a SubrangeSurface surface if
 * possible (if the number of boundary curve segments is exactly 4 or can be automatically
 * converted into 4). This entity type can be written out to an IGES file. When reading in
 * this entity, the IGES parameters are stored in the user data with the prefix
 * "IGES_144_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: April 10, 2013
 * @version April 12, 2016
 */
public class Entity144_TrimmedSurface extends GeomSSEntity {

    private static final double epsEP = 0.86603;    //  End-point parallel angle tolerance : cos(30 deg)
    private int PTS;    //  Pointer to the DE of the surface entity that is to be trimmed
    private int N1 = 1; //  0 = the outer boundary is the boundary of D, 1 = otherwise
    private int N2 = 0; //  This number indicates the number of simple closed curves which
                        //  constitute the inner boundary of the trimmed surface.
                        //  In case no inner boundary is introduced, this is set equal to zero.
    private int PTO;    //  Pointer to the DE of the Curve on a Parametric Surface Entity
                        //  that constitutes the outer boundary of the trimmed surface or zero.

    //  This implementation ignores all interior boundaries.
    
    private Surface surface;    //  The subrange surface this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity144_TrimmedSurface(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity144 constructor called");
        }

    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part to which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS GeomPoint geometry to return an Entity for.
     * @param PTS   Pointer to the DE of the surface entity that is to be trimmed.
     * @param PTO   Pointer to the DE of the Curve on a Parametric Surface Entity that
     *              constitutes the outer boundary of the trimmed surface or zero.
     */
    public Entity144_TrimmedSurface(Part part, int DEnum, SubrangeSurface geom, int PTS, int PTO) {
        super(part, new DirEntry(144, 0, DEnum, 0, geom.getName()));
        this.PTS = PTS;
        this.PTO = PTO;
        this.surface = geom;
    }

    /**
     * Checks to see if the entity is correct.
     */
    @Override
    public void check() {
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity144.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        PTS = getInt(s);    //  Pointer to the DE of the surface entity that is to be trimmed
        N1 = getInt(s);     //  0 = the outer boundary is the boundary of D, 1 = otherwise
        N2 = getInt(s);     //  The number of interior boundary curves if any.
        PTO = getInt(s);    //  Pointer to the DE of the Curve on a Parametric Surface Entity that constitutes the outer boundary of the trimmed surface or zero.

        //  Read in, but ignore, all the inner boundary curve segment pointers.
        for (int i = 0; i < N2; ++i) {
            int PTI = getInt(s);
        }

        super.read_additional();
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_144_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_144_N1", N1);
        element.putUserData("IGES_144_N2", N2);
    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {
        Part part = getPart();
        Parameter<Length> tol = Parameter.valueOf(1e-4, SI.METER);  //  Parametric tolerance.

        //  Get the surface to be trimmed.
        Surface Ssrf;
        Entity entity = part.getEntity(PTS);
        GeomSSEntity srfEntity;
        if (entity instanceof GeomSSEntity) {
            srfEntity = (GeomSSEntity)entity;
            Ssrf = (Surface)srfEntity.getGeomElement(GTransform.IDENTITY);
            if (Ssrf == null)
                return;
        } else
            return;

        //  If the outer boundary of Ssrf is the boundary, we are done.
        if (N1 == 0) {
            surface = SubrangeSurface.newInstance(Ssrf, 0, 0, 1, 1);
            srfEntity.setUsedInList(true);  //  Indicate that the entity is used by this association.
            return;
        }

        //  Get the boundary curve.
        entity = part.getEntity(PTO);
        if (entity instanceof Entity142_CurveOnSurface) {
            GeomSSEntity geomEntity = (GeomSSEntity)entity;
            GeomElement tmp = geomEntity.getGeomElement(GTransform.IDENTITY);
            if (tmp == null || !(tmp instanceof GeomList))
                return;
            GeomList<SubrangeCurve> Dcc = (GeomList)tmp;

            //  We can only handle 4 sided trimmed surfaces in SubrangeSurface.
            //  So, first try to deal with common situations where there are not
            //  exactly 4 boundary curves.
            if (Dcc.size() < 3)
                //  Don't know what to deal with a 0, 1 or 2 segment boundary.
                return;

            else if (Dcc.size() == 3) {
                //  Check to see if one edge is collapsed.  If all 3 curves connect
                //  end to end, then add a degenerate curve to the list to make 4 sides.
                GeomPoint p0 = Dcc.getFirst().getRealPoint(1);
                GeomPoint pim1e = p0;
                for (int i = 1; i < 3; ++i) {
                    SubrangeCurve crv = Dcc.get(i);
                    if (crv.getRealPoint(0).distance(pim1e).isGreaterThan(tol))
                        //  Curves do not connect end-to-end (as they should).
                        return;
                    pim1e = crv.getRealPoint(1);
                }
                if (pim1e.distance(p0).isGreaterThan(tol))
                    //  Last curve end point doesn't connect with 1st curve start point.
                    return;

                //  All the end points are within tolerance of each other, so add a degenerate curve
                //  between the last and the 1st points.
                GeomPoint ppnt = Dcc.getLast().getParPosition().getRealPoint(1);
                Curve pcrv = CurveFactory.createPoint(1, ppnt);
                SubrangeCurve crv = SubrangeCurve.newInstance(Ssrf, pcrv);
                Dcc.add(crv);

            } else if (Dcc.size() > 4) {
                //  Maybe some of the segments can be joined to get down to 4 segments?
                GeomList<SubrangeCurve> Dcc2 = combineSegments(Dcc, Ssrf);
                if (Dcc2.size() != 4) {
                    GeomList.recycle(Dcc2);
                    return;
                }

                //  Swap out the old list of segments for the new one.
                Dcc = Dcc2;
            }

            //  Pull out the boundary curves for the subrange surface.
            Curve s0 = Dcc.get(0).getParPosition();
            Curve t1 = Dcc.get(1).getParPosition();
            Curve s1 = Dcc.get(2).getParPosition().reverse();
            Curve t0 = Dcc.get(3).getParPosition().reverse();

            //  Create the subrange surface.
            surface = SubrangeSurface.newInstance(Ssrf, s0, t0, s1, t1, 1e-4);

            geomEntity.setUsedInList(true); //  Indicate that the entity is used by this association.
            srfEntity.setUsedInList(true);  //  Indicate that the entity is used by this association.
        }

    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return surface;
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entity object's parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {
        //  Build up the parameter data string.
        StringBuilder buffer = new StringBuilder();
        buffer.append(144);     buffer.append(Constants.Delim);
        buffer.append(PTS);     buffer.append(Constants.Delim);
        buffer.append(N1);      buffer.append(Constants.Delim);
        buffer.append(N2);      buffer.append(Constants.Delim);
        buffer.append(PTO);     buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity144 - Trimmed Parametric Surface";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        return outStr.toString();
    }

    /**
     * Method that tries to combine the segments of a composite curve in such a way that
     * they make up a 4 sided boundary for a SubrangeSurface entity.
     */
    private static GeomList<SubrangeCurve> combineSegments(GeomList<SubrangeCurve> Dcc, Surface Ssrf) throws IllegalArgumentException {
        Parameter<Length> ptol = Parameter.valueOf(1e-6, SI.METER);

        GeomList<SubrangeCurve> Dcc2 = GeomList.newInstance();
        SubrangeCurve lastCrv = Dcc.getFirst();
        NurbsCurve lastPcrv = lastCrv.getParPosition().toNurbs(ptol);

        int size = Dcc.size();
        for (int i = 1; i < size; ++i) {
            SubrangeCurve crv = Dcc.get(i);
            NurbsCurve pcrv = crv.getParPosition().toNurbs(ptol);

            //  Check to see if the end-start vectors in parametric space for each curve
            //  are approximately parallel.
            GeomVector<Dimensionless> Tim1 = lastPcrv.getSDerivative(1, 1).toUnitVector();
            GeomVector<Dimensionless> Ti = pcrv.getSDerivative(0, 1).toUnitVector();
            double cosa = Ti.dot(Tim1).getValue();
            if (cosa > epsEP) {
                //  End vectors are parallel.

                //  Join the two segments together
                pcrv = CurveUtils.connectCurves(lastPcrv, pcrv);
                crv = SubrangeCurve.newInstance(Ssrf, pcrv);

            } else
                //  End vectors are not parallel.
                Dcc2.add(lastCrv);

            //  Pepare for next loop.
            lastCrv = crv;
            lastPcrv = pcrv;
        }
        if (Dcc2.size() == 0)
            return Dcc2;

        //  See if the last curve connects with the 1st one.
        SubrangeCurve crv = Dcc2.get(0);
        NurbsCurve pcrv = crv.getParPosition().toNurbs(ptol);
        GeomVector<Dimensionless> Tim1 = lastPcrv.getSDerivative(1, 1).toUnitVector();
        GeomVector<Dimensionless> Ti = pcrv.getSDerivative(0, 1).toUnitVector();
        double cosa = Ti.dot(Tim1).getValue();
        if (cosa > epsEP) {
            pcrv = CurveUtils.connectCurves(lastPcrv, pcrv);
            crv = SubrangeCurve.newInstance(Ssrf, pcrv);
            Dcc2.remove(0);
            lastCrv = crv;
        }
        Dcc2.add(lastCrv);

        return Dcc2;
    }

}
