/*
 *   StartSection  -- Encapsulates the IGES Start Section.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.regex.*;

/**
 * The StartSection class encapsulates the IGES Start Section. There are methods to
 * initialize, read, and dump the object into a String.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class StartSection {

    private String StartText = "";

    /**
     * Default constructor.
     */
    public StartSection() {
    }

    /**
     * Initialization constructor.
     *
     * @param s input string
     */
    public StartSection(String s) {
        StartText = s;
    }

    /**
     * Read Start Section from input file. This method keeps track of how many lines are
     * read in.
     *
     * @param in input file
     * @throws IOException if there is any problem reading the start section.
     */
    public void read(RandomAccessFile in) throws IOException {
        StringBuilder outStr = new StringBuilder();

        while (true) {
            long curloc = in.getFilePointer();
            String line = Constants.myReadLine(in);
            if (line.charAt(72) == 'S') {
                outStr.append(line.substring(0, Constants.SECTID_COL));
                outStr.append("\n");
            } else {
                in.seek(curloc);
                break;
            }
        }

        StartText = outStr.toString().trim();
    }

    /**
     * Write the Start Section to the specified writer.
     *
     * @param writer The PrintWriter to write the Start Section to.
     * @return The number of lines written out to the start section.
     * @throws IOException if there is any problem writing the start section.
     */
    public int write(PrintWriter writer) throws IOException {
        String[] lines = StartText.split("\n");

        // This pattern matches control characters
        Pattern pattern = Pattern.compile("\\{cntrl\\}");

        int indx = 1;
        int numLines = lines.length;
        for (int i = 0; i < numLines; ++i) {
            String line = lines[i].trim();
            line = removeControlChars(pattern, line);
            indx = Constants.writeSection(writer, indx, "", 'S', new StringBuilder(line));
        }

        return indx - 1;
    }

    /**
     * Remove any ASCII control characters from the specified String.
     */
    private String removeControlChars(Pattern cntrlPattern, String string) {
        if (string.length() == 0)
            return string;

        Matcher m = cntrlPattern.matcher(string);

        //Replaces control characters with an empty string.
        String result = m.replaceAll("");

        return result;
    }

    /**
     * Set Start Section string.
     *
     * @param s input string
     */
    public void setStartSection(String s) {
        StartText = s;
    }

    /**
     * Dump to a String.
     *
     * @return result string
     */
    @Override
    public String toString() {
        return StartText;
    }

}
