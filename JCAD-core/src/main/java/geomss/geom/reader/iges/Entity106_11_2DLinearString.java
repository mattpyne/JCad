/*
 *   Entity106_11_2DLinearString  -- Entity that represents a consecutive series (string) of 2D points.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomList;
import geomss.geom.GeomPoint;
import geomss.geom.PointString;
import geomss.geom.Transformable;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <b><i>COPIOUS DATA ENTITY - LINEAR STRING</i></b> - This entity defines a series of
 * linear segments along the consecutive points of the path which may be isolated or used
 * as a component of a Subfigure Entity. The segments may cross, or be coincident with,
 * each other. Paths may close; i.e., the first path point may be coincident with the
 * last. Form 11 indicates that all segments share a constant depth (zt) value (are 2D).
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a list of points (a
 * PointString object). This entity type <b>can</b> be written out to an IGES file.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwladt, Date: March 10, 2013
 * @version September 13, 2016
 * @see Entity106_CopiousData
 */
public class Entity106_11_2DLinearString extends Entity106_CopiousData {

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity106_11_2DLinearString(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity106_11 constructor called");
        }
    }

    /**
     * Create this entity from the specified GeomSS geometry element.
     *
     * @param part  The Part in which this entity is contained.
     * @param DEnum The line count from the start of the Directory Entry Section for this
     *              entry (odd number).
     * @param geom  The GeomSS PointString geometry to return an Entity for.
     */
    public Entity106_11_2DLinearString(Part part, int DEnum, PointString geom) {
        super(part, new DirEntry(106, 11, DEnum, 0, geom.getName()));
        ip = 1;
        points.addAll(geom);
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file.
     *
     * @return true
     */
    @Override
    public boolean canWrite() {
        return true;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDnum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException
     */
    @Override
    public int write(PrintWriter writer, int PDnum) throws IOException {

        //  Build up the parameter data string.
        GeomList<GeomPoint> points2 = (GeomList<GeomPoint>)points.to(Constants.unit);
        StringBuilder buffer = new StringBuilder();
        buffer.append(106);                 buffer.append(Constants.Delim);
        buffer.append(ip);                  buffer.append(Constants.Delim);
        int n = points2.size();
        buffer.append(n);                   buffer.append(Constants.Delim);
        for (GeomPoint p : points2) {
            buffer.append(0);               buffer.append(Constants.Delim);
            appendPoint2(buffer, p);
        }
        buffer.setLength(buffer.length() - 1);  //  Replace Delim with Term for last entry.
        buffer.append(Constants.Term);

        //  Write it out.
        int oldPDnum = PDnum;
        PDnum = Constants.writeSection(writer, PDnum, Constants.makeSequenceNumber(getDENum()),
                'P', buffer);

        //  Store the PD line number and line count in the directory entry.
        getDirectoryEntry().setPDNumber(oldPDnum, PDnum - oldPDnum);

        return PDnum;
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        PointString<GeomPoint> str = PointString.newInstance();
        int size = points.size();
        for (int i = 0; i < size; ++i) {
            GeomPoint p = (GeomPoint)points.get(i);
            str.add(p);
        }
        return str;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity106_11 - Copious Data - 2D Linear String";
    }

}
