/*
 *   Entity  -- Superclass for all IGES entity types.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.ResourceBundle;
import geomss.geom.GeomPoint;
import geomss.geom.Point;
import geomss.geom.GTransform;

/**
 * The Entity class is meant to be a superclass for the individual entity type/ form
 * classes. This class also contains some utility functions to make reading of the entity
 * easier. When the entity is read in, it will be assumed that the first read call will
 * return the first Parameter Data line for that entity.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author JDN, AED, Version 1.0
 * @version September 13, 2016
 */
public abstract class Entity {

    /**
     * The String resources used by this package.
     */
    protected static final ResourceBundle RESOURCES = Constants.RESOURCES;

    /**
     * List of DE's of associativities
     */
    protected List<Integer> associativities = new ArrayList();

    /**
     * List of DE's of properties
     */
    protected List<Integer> properties = new ArrayList();

    /**
     * Directory Entry for this entity
     */
    private final DirEntry DE;

    /**
     * Index of start of current parameter in input PD string
     */
    private int start = 0;

    /**
     * Parameter Data string to be read in and parsed
     */
    private String PDstring = "";

    /**
     * Error report for this entity
     */
    private final List<String> errors = new ArrayList();

    /**
     * The active part to which this entity belongs
     */
    private final Part part;

    // Constants that only Entity and its subclasses need to know
    protected static final int ENTSTAT_VISIBLE = 0;
    protected static final int ENTSTAT_BLANKED = 1;

    protected static final int ENTSTAT_INDEPENDENT = 0;
    protected static final int ENTSTAT_PHYSDEPENDENT = 1;
    protected static final int ENTSTAT_LOGDEPENDENT = 2;
    protected static final int ENTSTAT_PHYSLOGDEPENDENT = 3;

    protected static final int ENTSTAT_GEOMETRY = 0;
    protected static final int ENTSTAT_ANNOTATION = 1;
    protected static final int ENTSTAT_DEFINITION = 2;
    protected static final int ENTSTAT_OTHER = 3;
    protected static final int ENTSTAT_LOGPOSITION = 4;
    protected static final int ENTSTAT_2DPARAMETRIC = 5;
    protected static final int ENTSTAT_CONSTRUCTION = 6;

    protected static final int ENTSTAT_UNDEFINED = -1;

    /**
     * Default constructor for the Entity superclass. Sets a pointer back to the main Part
     * and sets anything else to their defaults.
     *
     * @param p  part in which this entity is contained
     * @param de Directory Entry for this entity
     * @see DirEntry#read
     */
    public Entity(Part p, DirEntry de) {
        part = p;
        DE = new DirEntry(de);
    }

    /**
     * Reads the entity from the input file "in". It is assumed that the first
     * myReadLine() call will return the first Parameter Data line for this entity. The PD
     * data will be read into StringBuffer "PDstring", which will then be parsed by the
     * Entity???.read() method.
     *
     * @param in input file, which is of class RandomAccessFile
     * @throws IOException if there is any problem reading the entity from the IGES file.
     */
    public void read(RandomAccessFile in) throws IOException {
        StringBuilder buffer = new StringBuilder();

        if (Constants.DEBUG) {
            System.out.println("Entity.read() called\n");
        }

        for (int i = 0; i < DE.getPDCnt(); i++) {
            long curloc = in.getFilePointer();
            String line = Constants.myReadLine(in);
            if (line.charAt(Constants.SECTID_COL) == 'P')
                buffer.append(line.substring(0, Constants.PDID_COL));
            else {
                in.seek(curloc);
                break;
            }
        }

        PDstring = buffer.toString();
        int PDType = getInt(PDstring); // Read in the type from the PD Data string, check against what is in DE.
        if (PDType != getType()) {
            StringBuilder msg = new StringBuilder(RESOURCES.getString("warning"));
            msg.append(getTypeString());
            msg.append(", DE ");
            msg.append(getDENum());
            msg.append(":  ");
            msg.append(MessageFormat.format(RESOURCES.getString("entityTypeMissmatch"), PDType, getType()));
            addErrorMessage(msg.toString());
        }
    }

    /**
     * Reads in the "additional pointers" after the regular Parameter Data. These are
     * pointers to associativities and properties.
     */
    public void read_additional() {
        String s = PDstring;

        int num_assoc = getInt(s);
        for (int i = 0; i < num_assoc; i++)
            associativities.add(getInt(s));

        int num_props = getInt(s);
        for (int i = 0; i < num_props; i++)
            properties.add(getInt(s));
    }

    /**
     * Returns <code>true</code> if the Entity can be written to an exchange file. The
     * default implementation returns <code>false</code> indicating that this Entity can
     * not be written to a file.
     *
     * @return true if the Entity can be written to an exchange file.
     */
    public boolean canWrite() {
        return false;
    }

    /**
     * Write this entities parameter data to the specified PrintWriter. The default
     * implementation always throws an exception.
     *
     * @param writer The PrintWriter to write the parameter data for this entity to.
     * @param PDNum  The starting Parameter Data row index number.
     * @return The Parameter Data row index number for the next row.
     * @throws java.io.IOException This implementation always throws this exception.
     */
    public int write(PrintWriter writer, int PDNum) throws IOException {
        throw new IOException(
                MessageFormat.format(RESOURCES.getString("canNotWrite"), getTypeString()));
    }

    /**
     * Checks to see if the entity has any errors or warnings.
     */
    public abstract void check();

    /**
     * Return the Parameter Data string for this entity.
     *
     * @return The Parameter Data string for this entity.
     */
    protected String getPDString() {
        return PDstring;
    }

    /**
     * Return a reference to the directory entry for this entity.
     *
     * @return A reference to the directory entry for this entity.
     */
    protected DirEntry getDirectoryEntry() {
        return DE;
    }

    /**
     * Return a reference to the Part for this entity.
     *
     * @return A reference to the Part for this entity.
     */
    protected Part getPart() {
        return part;
    }

    /**
     * Add a new entry to the list of error/warning messages for this entity.
     *
     * @param msg The message to be added to the list of error/warning messages.
     */
    protected void addErrorMessage(String msg) {
        errors.add(msg);
    }

    /**
     * Get a list of error and warning messages issued by this Entity. If there are no
     * messages, an empty list is returned.
     *
     * @return List of error strings
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    public abstract String getTypeString();

    /**
     * Returns a warning message String that is specific to this Entity type, DE # and the
     * specified message. Using this method allows consistency in warning messages across
     * Entity types.
     *
     * @param msg The specific warning message to be appended to the information about
     *            this entity.
     * @return A String representing the input message appended onto information about
     *         this specific Entity.
     */
    protected String getWarningString(String msg) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(RESOURCES.getString("warning"));
        buffer.append(getTypeString());
        buffer.append(", DE ");
        buffer.append(DE.getDENum());
        buffer.append(":  ");
        buffer.append(msg);
        return buffer.toString();
    }

    /**
     * Dumps a simple header. Just the DE Number and type.
     *
     * @return String containing the resulting text.
     */
    public String getHeader() {
        StringBuilder outStr = new StringBuilder("DE");
        outStr.append(getDirectoryEntry().getDENum());
        outStr.append(" - ");
        outStr.append(getTypeString());
        return outStr.toString();
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(getTypeString());
        outStr.append("\n");

        outStr.append(DE.toString());
        outStr.append("\n");

        int num_assoc = associativities.size();
        if (num_assoc > 0) {
            outStr.append("Associativities:\n");
            for (int i = 0; i < num_assoc; i++) {
                outStr.append("  assoc(");
                outStr.append(i);
                outStr.append(") = ");
                outStr.append(associativities.get(i).intValue());
                outStr.append("\n");
            }
            outStr.append("\n");
        }

        int num_props = properties.size();
        if (num_props > 0) {
            outStr.append("Properties:\n");
            for (int i = 0; i < num_props; i++) {
                outStr.append("  prop(");
                outStr.append(i);
                outStr.append(") = ");
                outStr.append(properties.get(i).intValue());
                outStr.append("\n");
            }
            outStr.append("\n");
        }

        return outStr.toString();
    }

    /**
     * Return blank status. Returns 0 if the entity is visible, 1 if it is blanked.
     *
     * @return blank status
     */
    public int blankedStatus() {
        String sub = DE.getStatus().substring(0, 2);

        return Constants.toInt(sub);
    }

    /**
     * Return subordinate status. Returns 0 if the entity is independent, 1 if it is
     * physically dependent to its parent, 2 if it is logically dependent to its parent,
     * and 3 if it is both physically and logically dependent to its parent.
     *
     * @return subordinate status
     */
    public int subordStatus() {
        String sub = DE.getStatus().substring(2, 4);

        return Constants.toInt(sub);
    }

    /**
     * Return usage status. Returns 0 if the entity is geometry, 1 if it is annotation, 2
     * if it is definition, 3 if it is something else, 4 if it is logical/positional, 5 if
     * it is 2D parametric, and 6 if it is construction geometry.
     *
     * @return entity usage status
     */
    public int useStatus() {
        String sub = DE.getStatus().substring(4, 6);

        return Constants.toInt(sub);
    }

    /**
     * Return hierarchy status. Returns 0 if the entity has global top down hierarchy, 1
     * if global defer, or 2 if a hierarchy property is to be used.
     *
     * @return hierarchy status
     */
    public int hierStatus() {
        String sub = DE.getStatus().substring(6, 8);

        return Constants.toInt(sub);
    }

    /**
     * Get matrix representing entity's matrix appended to supplied matrix.
     *
     * @param m matrix to which to append this entities matrix.
     * @return combined matrix: m * m(entity)
     */
    public GTransform getMatrix(GTransform m) {
        int id = DE.getMatrix();
        if (id == 0)
            return m;

        // This handles nested matrices
        GTransform me = ((Entity124_TransformationMatrix)part.getEntity(id)).getMat();
        GTransform m1 = m.times(me);

        return m1;
    }

    /**
     * Return Directory Entry number.
     *
     * @return DE number for this entity
     */
    public int getDENum() {
        return DE.getDENum();
    }

    /**
     * Return Entity type.
     *
     * @return type
     */
    public int getType() {
        return DE.getType();
    }

    /**
     * Return Parameter Data number.
     *
     * @return PD number for this entity
     */
    public int getPDNum() {
        return DE.getPDNum();
    }

    /**
     * Return View.
     *
     * @return View for this entity
     */
    public int getView() {
        return DE.getView();
    }

    /**
     * Return char parameter from input string. The character string must be in Hollerith
     * form (1H-), as per the IGES specification, and delimited by the global delimiter
     * character.
     *
     * @param s input string
     * @return single character
     */
    protected char getChar(String s) {
        String sResult = "";

        while (s.charAt(start) == ' ') {
            start++;
        }

        if ((s.charAt(start) != Constants.Delim) && (s.charAt(start) != Constants.Term)) {
            // Should be in form ##Hchars
            int sStart = s.indexOf('H', start);
            int iLen = Integer.parseInt((s.substring(start, sStart)).trim());
            int newstart = sStart + 2 + iLen;

            if (newstart < 0) {
                sResult = "";
            } else {
                sResult = s.substring(sStart + 1, newstart - 1);
                start = newstart - 1;
            }
        }

        if (s.charAt(start) != Constants.Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getChar - \"" + sResult + "\"");
        }

        if (sResult.length() == 0)
            return '\0';
        return sResult.charAt(0);
    }

    /**
     * Return string parameter from input string. The string parameter must be in
     * Hollerith form (nH...), as per the IGES specification, and delimited by the global
     * delimiter character.
     *
     * @param s input string
     * @return string stripped of Hollerith prefix, or empty string if the input string is
     *         invalid
     */
    protected String getString(String s) {
        String sResult = "";

        while (s.charAt(start) == ' ') {
            start++;
        }

        if ((s.charAt(start) != Constants.Delim) && (s.charAt(start) != Constants.Term)) {
            // Should be in form ##Hchars
            int sStart = s.indexOf('H', start);
            int iLen = Integer.parseInt((s.substring(start, sStart)).trim());
            int newstart = sStart + 2 + iLen;

            if (newstart < 0) {
                sResult = "";
            } else {
                sResult = s.substring(sStart + 1, newstart - 1);
                start = newstart - 1;
            }
        }

        if (s.charAt(start) != Constants.Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getString - \"" + sResult + "\"");
        }

        return sResult;
    }

    /**
     * Return real parameter from input string. The real parameter can be of single
     * (1.0e+010) or double (1.0d+010) precision, and delimited by the global delimiter
     * character.
     *
     * @param s input string
     * @return real number
     */
    protected double getReal(String s) {
        StringBuffer sResult = new StringBuffer();

        while (s.charAt(start) == ' ') {
            start++;
        }

        while ((s.charAt(start) != Constants.Delim) && (s.charAt(start) != Constants.Term)) {
            sResult.append(s.charAt(start));

            if (s.charAt(start + 1) == Constants.Term)
                break;
            else
                start++;
        }

        if (s.charAt(start) != Constants.Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getReal - \"" + sResult + "\"");
        }

        return Constants.toDouble(sResult.toString());
    }

    /**
     * Return real parameter from input string, with default value. The real parameter can
     * be of single (1.0e+010) or double (1.0d+010) precision, and delimited by the global
     * delimiter character. If the parameter is invalid or blank, the input default value
     * is used.
     *
     * @param s   input string
     * @param def The default value to return if the string is invalid or blank.
     * @return real number
     */
    protected double getReal(String s, double def) {
        StringBuffer sResult = new StringBuffer();

        while (s.charAt(start) == ' ') {
            start++;
        }

        while ((s.charAt(start) != Constants.Delim) && (s.charAt(start) != Constants.Term)) {
            sResult.append(s.charAt(start));

            if (s.charAt(start + 1) == Constants.Term)
                break;
            else
                start++;
        }

        if (s.charAt(start) != Constants.Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getReal - \"" + sResult + "\"");
        }

        String str = sResult.toString().trim();
        if (str.length() == 0)
            return def;
        return Constants.toDouble(str);
    }

    /**
     * Return integer parameter from input string. The integer parameter can be
     * represented either by the standard integer, or as a float value (e.g. 1.000e+010),
     * and delimited by the global delimiter character.
     *
     * @param s input string
     * @return integer number
     */
    protected int getInt(String s) {
        StringBuffer sResult = new StringBuffer();

        while (s.charAt(start) == ' ') {
            start++;
        }

        while ((s.charAt(start) != Constants.Delim) && (s.charAt(start) != Constants.Term)) {
            sResult.append(s.charAt(start));

            if (s.charAt(start + 1) == Constants.Term)
                break;
            else
                start++;
        }

        if (s.charAt(start) != Constants.Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getInt - \"" + sResult + "\"");
        }

        return Constants.toInt(sResult.toString());
    }

    /**
     * Return integer parameter from input string, with default value. The integer
     * parameter can be represented either by the standard integer, or as a float value
     * (e.g. 1.000e+010), and delimited by the global delimiter character. If the
     * parameter is invalid or blank, the input default value is used.
     *
     * @param s   input string
     * @param def The default value for the integer.
     * @return integer number
     */
    protected int getInt(String s, int def) {
        StringBuffer sResult = new StringBuffer();

        while (s.charAt(start) == ' ') {
            start++;
        }

        while ((s.charAt(start) != Constants.Delim) && (s.charAt(start) != Constants.Term)) {
            sResult.append(s.charAt(start));

            if (s.charAt(start + 1) == Constants.Term)
                break;
            else
                start++;
        }

        if (s.charAt(start) != Constants.Term)
            start++;

        if (Constants.DEBUG) {
            System.out.println("getInt - \"" + sResult + "\"");
        }

        String str = sResult.toString().trim();
        if (str.length() == 0)
            return def;
        return Constants.toInt(str);
    }

    /**
     * Return 3D Point from string.
     *
     * @param s input string
     * @return 3D Point object
     */
    protected Point getPoint3(String s) {

        double x = getReal(s);
        if (Math.abs(x) < Constants.Grain)
            x = 0;
        double y = getReal(s);
        if (Math.abs(y) < Constants.Grain)
            y = 0;
        double z = getReal(s);
        if (Math.abs(z) < Constants.Grain)
            z = 0;

        return Point.valueOf(x, y, z, Constants.unit);
    }

    /**
     * Return 2D Point from string.
     *
     * @param s input string
     * @return 3D Point object with the Z coordinate set to 0.
     */
    protected Point getPoint2(String s) {

        double x = getReal(s);
        if (Math.abs(x) < Constants.Grain)
            x = 0;
        double y = getReal(s);
        if (Math.abs(y) < Constants.Grain)
            y = 0;

        return Point.valueOf(x, y, 0, Constants.unit);
    }

    /**
     * Append a 2D Point to the specified StringBuffer with each coordinate separated by
     * the delimiter character.
     *
     * @param buffer The buffer to write the point to.
     * @param point  The 2D point to write out.
     * @return A reference to the input buffer.
     */
    protected StringBuilder appendPoint2(StringBuilder buffer, GeomPoint point) {
        double x = point.getValue(0);
        if (Math.abs(x) < Constants.Grain)
            x = 0;
        double y = point.getValue(1);
        if (Math.abs(y) < Constants.Grain)
            y = 0;

        buffer.append(x);   buffer.append(Constants.Delim);
        buffer.append(y);   buffer.append(Constants.Delim);

        return buffer;
    }

    /**
     * Append a 3D Point to the specified StringBuffer with each coordinate separated by
     * the delimiter character.
     *
     * @param buffer The buffer to write the point to.
     * @param point  The point to write out.
     * @return A reference to the input buffer.
     */
    protected StringBuilder appendPoint3(StringBuilder buffer, GeomPoint point) {
        double x = point.getValue(0);
        if (Math.abs(x) < Constants.Grain)
            x = 0;
        double y = point.getValue(1);
        if (Math.abs(y) < Constants.Grain)
            y = 0;
        double z = point.getValue(2);
        if (Math.abs(z) < Constants.Grain)
            z = 0;
        buffer.append(x);   buffer.append(Constants.Delim);
        buffer.append(y);   buffer.append(Constants.Delim);
        buffer.append(z);   buffer.append(Constants.Delim);
        return buffer;
    }

    /**
     * Return View object from string. The View is used in the type 404 entity (Drawing).
     *
     * @param s input string
     * @param f drawing form
     * @return View object
     */
    protected View getView(String s, int f) {
        View v = new View();

        v.viewde = getInt(s);
        v.origin = getPoint2(s);
        v.angle = (f == 1) ? getReal(s) : 0.0;

        return v;
    }

}
