/*
 *   Entity118_RuledSurface  -- An entity representing a Ruled Surface Entity.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   part library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   part library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with part program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader.iges;

import geomss.geom.GeomElement;
import geomss.geom.LoftedSurface;
import geomss.geom.Transformable;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * <b><i>RULED SURFACE ENTITY</i></b> - This entity represents a ruled or 2-curve linearly
 * lofted surface. A ruled surface is formed by moving a line connecting points of equal
 * relative arc length (Form 0) or equal relative parametric value (Form 1) on two
 * parametric curves from a start point to a terminate point on the curves.
 * 
 * <p>
 * This entity, when read from an IGES file, is converted to a LoftedSurface surface. This
 * entity type can not be written out to an IGES file. The surface parameters are stored
 * in the user data with the prefix "IGES_118_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: March 11, 2013
 * @version September 13, 2016
 */
public abstract class Entity118_RuledSurface extends GeomSSEntity {

    protected int DE1;              //  Pointer to the DE of the first curve entity
    protected int DE2;              //  Pointer to the DE of the second curve entity
    protected int DIRFLG = 0;       //  Direction_stp flag (0=Join first to first, last to last; 1=Join first to last, last to first)
    protected int DEVFLG = 0;       //  Developable surface flag (1= Developable, 0=Possibly not)

    protected LoftedSurface srf;    //  The GeomSS surface this entity represents.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity118_RuledSurface(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity118_RuledSurface constructor called");
        }

    }

    /**
     * Checks to see if the entity is correct.
     */
    @Override
    public void check() {
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {

        if (Constants.DEBUG) {
            System.out.println("Entity118_RuledSurface.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        DE1 = getInt(s);            //  Pointer to the DE of the first curve entity
        DE2 = getInt(s);            //  Pointer to the DE of the second curve entity
        DIRFLG = getInt(s);         //  Direction_stp flag (0=Join first to first, last to last; 1=Join first to last, last to first)
        DEVFLG = getInt(s);         //  Developable surface flag (1= Developable, 0=Possibly not)

        super.read_additional();
    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_118_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_118_DIRFLG", DIRFLG);
        element.putUserData("IGES_118_DEVFLG", DEVFLG);
        element.putUserData("IGES_U0", ZERO);
        element.putUserData("IGES_U1", ONE);
        element.putUserData("IGES_V0", ZERO);
        element.putUserData("IGES_V1", ONE);
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return srf;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity118 - Ruled Surface";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

        return outStr.toString();
    }

}
