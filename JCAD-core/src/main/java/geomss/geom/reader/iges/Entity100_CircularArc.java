/*
 *   Entity100_CircularArc  -- Entity that defines a circle or circular arc.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import geomss.geom.*;
import geomss.geom.nurbs.CurveFactory;
import jahuwaldt.js.param.Parameter;
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import static javolution.lang.MathLib.*;

/**
 * <b><i>CIRCULAR ARC ENTITY</i></b> - This entity defines a circle or a portion of a
 * circle which may be isolated or used as a component of a Composite Curve Entity or a
 * subfigure. The definition space coordinate system is always chosen so that the circular
 * arc remains in a plane either coincident with or parallel to the XT, YT plane.
 *
 * <p>
 * This entity, when read from an IGES file, is converted to a NURBS curve of degreeU 2
 * (IGES type 126, Form 2, Degree 2). This entity type can not be written out to an IGES
 * file. All circular arc parameters are stored in the user data with the prefix
 * "IGES_100_" followed by the parameter name.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author JDN, Version 1.0
 * @version September 13, 2016
 */
public class Entity100_CircularArc extends GeomSSEntity {

    private double zt; // Parallel ZT displacement of arc from XT, YT plane
    private double x1; // Arc center abcissa
    private double y1; // Arc center ordinate
    private double x2; // Start point abcissa
    private double y2; // Start point ordinate
    private double x3; // Terminate point abcissa
    private double y3; // Terminate point ordinate

    private double r;       // Radius
    private double angle1;  // Start angle
    private double angle2;  // Terminate angle
    private boolean circle; // Full circle?

    private Curve curve;    //  The GeomSS curve represented by this element.

    /**
     * Default constructor.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public Entity100_CircularArc(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("Entity100 constructor called");
        }
    }

    /**
     * Checks to see if the entity is correct. The following restrictions are imposed:
     *
     * - The length must be greater than 0.0
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // Radius must be greater than 0.0
        double rad1 = Constants.dist(x1, y1, x2, y2);
        double rad2 = Constants.dist(x1, y1, x3, y3);
        if ((abs(rad1) < Constants.Grain) || (abs(rad2) < Constants.Grain)) {
            String msg = RESOURCES.getString("zeroRadiusArc");
            addErrorMessage(getWarningString(msg));
        }

        // Length must be greater than 0.0
        if (abs(angle2 - angle1) < Constants.Grain) {
            String msg = RESOURCES.getString("zeroLengthArc");
            addErrorMessage(getWarningString(msg));
        }
    }

    /**
     * Read the Parameter Data from the String read in by the superclass.
     *
     * @param in input file
     * @throws java.io.IOException
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        if (Constants.DEBUG) {
            System.out.println("Entity100.read() called");
        }

        super.read(in);
        String s = getPDString();

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + s + "\"");
        }

        zt = getReal(s);
        if (abs(zt) < Constants.Grain)
            zt = 0;
        x1 = getReal(s);
        if (abs(x1) < Constants.Grain)
            x1 = 0;
        y1 = getReal(s);
        if (abs(y1) < Constants.Grain)
            y1 = 0;
        x2 = getReal(s);
        if (abs(x2) < Constants.Grain)
            x2 = 0;
        y2 = getReal(s);
        if (abs(y2) < Constants.Grain)
            y2 = 0;
        x3 = getReal(s);
        if (abs(x3) < Constants.Grain)
            x3 = 0;
        y3 = getReal(s);
        if (abs(y3) < Constants.Grain)
            y3 = 0;

        super.read_additional();

        double dx = x2 - x1;
        double dy = y2 - y1;
        r = sqrt(dx * dx + dy * dy);

        angle1 = atan2(dy, dx);
        angle2 = atan2(y3 - y1, x3 - x1);

        circle = (abs(angle2 - angle1) < Constants.Grain);

        while (angle1 < 0.0) {
            angle1 += TWO_PI;
        }

        while (angle2 <= angle1) {
            angle2 += TWO_PI;
        }

    }

    /**
     * The GeomSS geometry element is created from the IGES parameters when this method is
     * called.
     */
    @Override
    void createGeometry() throws IOException {

        //  Define some inputs required.
        Point center = Point.valueOf(x1, y1, zt, Constants.unit);
        Parameter<Length> radius = Parameter.valueOf(r, Constants.unit);
        Vector<Dimensionless> normal = Vector.valueOf(0, 0, 1).toUnitVector();

        if (circle) {
            //  We have a full circle.
            curve = CurveFactory.createCircle(center, radius, normal);

        } else {
            //  We have an arc.
            Parameter<Angle> thetaS = Parameter.valueOf(angle1, SI.RADIAN);
            Parameter<Angle> thetaE = Parameter.valueOf(angle2, SI.RADIAN);
            curve = CurveFactory.createCircularArc(center, radius, normal, thetaS, thetaE);
        }

    }

    /**
     * Method used to apply IGES meta-data to GeomSS elements. This implementation stores
     * in the user data, in addition to the header info, all the parameter information for
     * this entity type prefixed by "IGES_100_".
     */
    @Override
    protected void applyMetaData(GeomElement element) {
        super.applyMetaData(element);
        element.putUserData("IGES_100_ZT", zt);
        element.putUserData("IGES_100_X1", x1);
        element.putUserData("IGES_100_Y1", y1);
        element.putUserData("IGES_100_X2", x2);
        element.putUserData("IGES_100_Y2", y2);
        element.putUserData("IGES_100_X3", x3);
        element.putUserData("IGES_100_Y3", y3);
    }

    /**
     * Return a reference to the Transformable GeomElement contained in this IGES Entity.
     *
     * @return A reference to the Transformable GeomElement contained in this IGES Entity.
     */
    @Override
    protected Transformable getGeomElement() {
        return curve;
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "Entity100 - Circular Arc";
    }

    /**
     * Dump to String.
     *
     * @return String containing the resulting text.
     */
    @Override
    public String toString() {
        StringBuilder outStr = new StringBuilder(super.toString());
        outStr.append("\n");

		outStr.append("zt = ");	outStr.append(zt);	outStr.append("\n");
		outStr.append("x1 = ");	outStr.append(x1);	outStr.append("\n");
		outStr.append("y1 = ");	outStr.append(y1);	outStr.append("\n");
		outStr.append("x2 = ");	outStr.append(x2);	outStr.append("\n");
		outStr.append("y2 = ");	outStr.append(y2);	outStr.append("\n");
		outStr.append("x3 = ");	outStr.append(x3);	outStr.append("\n");
		outStr.append("y3 = ");	outStr.append(y3);

        return outStr.toString();
    }

}
