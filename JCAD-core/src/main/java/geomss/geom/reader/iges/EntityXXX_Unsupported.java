/*
 *   EntityXXX_Unsupported  -- This class encapsulates an unknown IGES entity.
 *
 *   Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 *
 *   Based on, but heavily modified from, IGESView ( http://ts.nist.gov/Standards/IGES/igesTools.cfm )
 */
package geomss.geom.reader.iges;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;

/**
 * <b><i>UNSUPPORTED ENTITY</i></b> - This entity is both a generic starting point for
 * entity class creation and a placeholder for unsupported entities in the IGES file. To
 * extend this class, add whatever member variables are required for the entity type. Make
 * them private.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author JDN, Version 1.0
 * @version April 10, 2016
 */
public class EntityXXX_Unsupported extends Entity {

    /**
     * The default constructor should be good as is, unless you declare members that need
     * initializing, such as Lists.
     *
     * @param p  part to which this entity is contained
     * @param de Directory Entry for this entity
     */
    public EntityXXX_Unsupported(Part p, DirEntry de) {
        super(p, de);

        if (Constants.DEBUG) {
            System.out.println("EntityXXX constructor called");
        }
    }

    /**
     * The method check() should go through the given restrictions for the entity and put
     * together an error string to be displayed in the error window. If the error should
     * prevent the entity from being drawn, set toBeDrawn to false (it is true by
     * default).
     */
    @Override
    public void check() {
        DirEntry DE = getDirectoryEntry();

        // Set message to unsupported
        StringBuilder msg = new StringBuilder(RESOURCES.getString("error"));
        msg.append(":  DE ");
        msg.append(DE.getDENum());
        msg.append(":  ");
        msg.append(MessageFormat.format(RESOURCES.getString("unsupportedEntity"), DE.getType()));
        addErrorMessage(msg.toString());

    }

    /**
     * The read() method basically parses the String read in by Entity.read(). There are
     * several methods to use to grab specific data types: getInt(), getReal(),
     * getString(), and getChar(). See other entity classes for examples of usage. The
     * GlobalSection class also has some examples.
     *
     * @param in input file
     * @throws IOException if there is any problem reading the entity from the IGES file.
     */
    @Override
    public void read(RandomAccessFile in) throws IOException {
        if (Constants.DEBUG) {
            System.out.println("EntityXXX.read() called");
        }

        super.read(in);

        if (Constants.DEBUG) {
            System.out.println("PD String = \"" + getPDString() + "\"");
        }
    }

    /**
     * Returns a short String describing this Entity object's type.
     *
     * @return A short String describing this Entity object's type.
     */
    @Override
    public String getTypeString() {
        return "EntityXXX - Unsupported";
    }

}
