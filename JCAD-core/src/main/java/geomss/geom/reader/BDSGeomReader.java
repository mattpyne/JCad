/**
 * BDSGeomReader -- A class that can write out an SAIC bdStudio BDS formatted geometry
 * file.
 *
 * Copyright (C) 2010-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import static java.util.Objects.requireNonNull;
import javolution.context.StackContext;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for writing vehicle point geometry out to an SAIC bdStudio BDS
 * mesh geometry file. The input geometry is assumed to be made up of quadrilateral panels
 * (with one panel side possibly collapsed).
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 3, 2010
 * @version September 9, 2016
 */
public class BDSGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    //private static final boolean DEBUG = false;
    
    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("bdsDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "bds";

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A description of this format type.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for this format type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Returns true. This class can write point geometry to a BDS formatted file.
     *
     * @return Always returns true.
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Writes out a bdStudio mesh (BDS) geometry file for the geometry contained in the
     * supplied {@link PointVehicle} object.
     * <p>
     * WARNING: This format is not unit aware. The geometry will be written out in
     * whatever its current units are! Make sure to convert to the desired units for the
     * file before calling this method.
     * </p>
     *
     * @param outputFile The output file to which the geometry is to be written.
     * @param geometry   The {@link PointVehicle} object to be written out. May not be null.
     * @throws IOException if there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  Convert the input generic geometry list to a PointVehicle.
        PointVehicle vehicle = geomList2PointVehicle(geometry);

        if (!vehicle.containsGeometry())
            return;

        //  Convert all the geometry to the same units.
        vehicle = vehicle.to(vehicle.getUnit());
        
        //  Convert the geometry into BDMeshGroups.
        List<BDSMeshGroup> meshGroups = convertGeometry(vehicle);

        //  BDS files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        StackContext.enter();
        // Get a reference to the output stream writer.
        try (PrintWriter writer = new PrintWriter(outputFile, StandardCharsets.US_ASCII.name())) {

            //  Write the header data.
            writer.println("bdStudio");
            writer.println("GROUPS " + meshGroups.size());

            //  Output the mesh group data.
            for (BDSMeshGroup meshGroup : meshGroups) {
                meshGroup.save(writer);
            }

            //  Write the materials section header.
            writer.println("MATERIALS 0");

            //  Write the textures section header.
            writer.println("TEXTURES 0");

            //  Write the textures section header.
            writer.println("SOLID 0");

        } finally {
            StackContext.exit();

            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

    }

    /**
     * This method always returns <code>false</code> as BDS files do not encode the units
     * that are being used. You must call <code>setFileUnits</code> to set the units being
     * used before reading a file of this format.
     *
     * @return Always returns false.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

    /**
     * Convert the geometry from the native format to a list of bdStudio mesh groups.
     */
    private List<BDSMeshGroup> convertGeometry(PointVehicle geometry) {
        List<BDSMeshGroup> meshGroups = FastTable.newInstance();

        //  Loop over the components in this geometry.
        for (PointComponent comp : geometry) {

            //  Loop over the arrays in this component.
            for (PointArray array : comp) {

                //  Create a mesh group for this array.
                BDSMeshGroup meshGroup = new BDSMeshGroup(array);
                meshGroups.add(meshGroup);
            }
        }

        return meshGroups;
    }

    /**
     * A bdStudio mesh group object.
     */
    private class BDSMeshGroup {

        private final String name;
        private final BDSVertex[] vertexes;
        private final List<int[]> faces = FastTable.newInstance();  //  Face vertex indicies.

        public BDSMeshGroup(PointArray<? extends GeomPoint> array) {
            // Store the name of the mesh.
            name = array.getName();

            //  Turn the array of points in a list of faces and verticies.
            //  The array is made up of quadralateral panels, so convert
            //  those into pairs of triangles.
            
            int numRows = array.size();
            int numPanelRows = numRows - 1;
            int numCols = array.get(0).size();
            int numPanelCols = numCols - 1;
            int vertexCount = numRows * numCols;

            //  Load the PointArray objects into the required vertex list.
            vertexes = new BDSVertex[vertexCount];
            int vIdx = 0;
            for (int i = 0; i < numRows; ++i) {
                PointString<?> string = array.get(i);
                for (int j = 0; j < numCols; ++j) {
                    GeomPoint point = string.get(j);
                    point = point.toDimension(3);   //  Convert the geometry to 3D.
                    float[] normal = calcNormal(array, point, i, j);
                    BDSVertex vertex = new BDSVertex(point, normal);
                    vertexes[vIdx] = vertex;
                    ++vIdx;
                }
            }

            //  Set up the triangle indices (two per quad panel).
            vIdx = 0;
            for (int i = 0; i < numPanelRows; ++i) {
                for (int j = 0; j < numPanelCols; ++j) {
                    int[] aFace = new int[3];
                    aFace[0] = vIdx;
                    aFace[1] = vIdx + numCols;
                    aFace[2] = vIdx + numCols + 1;
                    faces.add(aFace);

                    aFace = new int[3];
                    aFace[0] = vIdx;
                    aFace[1] = vIdx + numCols + 1;
                    aFace[2] = vIdx + 1;
                    faces.add(aFace);
                    ++vIdx;
                }
                ++vIdx;
            }
        }

        public void save(PrintWriter out) {

            int numVerts = vertexes.length;
            int numFaces = faces.size();

            //  Write the header information for the mesh group.
            out.println("MATERIAL 0");
            out.println("TEXTURE 0");
            out.println("SOLID 0");
            out.print("GEOM ");
            out.print(numVerts);
            out.print(" ");
            out.print(numFaces);
            out.print(" ; ");
            out.println(name);

            //  Output the vertex data.
            for (BDSVertex vertex : vertexes) {
                vertex.save(out);
            }

            //  Output the face data.
            for (int[] face : faces) {
                out.print(face[0]);
                out.print(" ");
                out.print(face[1]);
                out.print(" ");
                out.println(face[2]);
            }
        }

        //  Calculate the normal vector for a point by averaging the normals
        //  of all the surrounding faces.
        private float[] calcNormal(PointArray<? extends GeomPoint> array, GeomPoint pij, int i, int j) {
            int numRows = array.size() - 1;
            int numCols = array.get(0).size() - 1;
            FastTable<GeomVector> neighbors = FastTable.newInstance();
            GeomPoint p1 = pij;
            if (i > 0) {
                if (j < numCols) {
                    GeomPoint p2 = array.get(i).get(j + 1).toDimension(3);
                    GeomPoint p3 = array.get(i - 1).get(j).toDimension(3);
                    GeomVector v1 = p2.minus(p1).toGeomVector();
                    GeomVector v2 = p3.minus(p1).toGeomVector();
                    if (!v1.mag().isApproxZero() && !v2.mag().isApproxZero()) {
                        GeomVector v1xv2 = v1.cross(v2).toUnitVector();
                        if (!v1xv2.get(0).isNaN())
                            neighbors.add(v1xv2);
                    }
                }
                if (j > 0) {
                    GeomPoint p2 = array.get(i - 1).get(j).toDimension(3);
                    GeomPoint p3 = array.get(i - 1).get(j - 1).toDimension(3);
                    GeomPoint p4 = array.get(i).get(j - 1).toDimension(3);
                    GeomVector v1 = p2.minus(p1).toGeomVector();
                    GeomVector v2 = p3.minus(p1).toGeomVector();
                    if (!v1.mag().isApproxZero() && !v2.mag().isApproxZero()) {
                        GeomVector v1xv2 = v1.cross(v2).toUnitVector();
                        if (!v1xv2.get(0).isNaN())
                            neighbors.add(v1xv2);
                    }
                    v1 = v2;
                    v2 = p4.minus(p1).toGeomVector();
                    if (!v1.mag().isApproxZero() && !v2.mag().isApproxZero()) {
                        GeomVector v1xv2 = v1.cross(v2).toUnitVector();
                        if (!v1xv2.get(0).isNaN())
                            neighbors.add(v1xv2);
                    }
                }
            }
            if (i < numRows) {
                if (j < numCols) {
                    GeomPoint p2 = array.get(i + 1).get(j).toDimension(3);
                    GeomPoint p3 = array.get(i + 1).get(j + 1).toDimension(3);
                    GeomPoint p4 = array.get(i).get(j + 1).toDimension(3);
                    GeomVector v1 = p2.minus(p1).toGeomVector();
                    GeomVector v2 = p3.minus(p1).toGeomVector();
                    if (!v1.mag().isApproxZero() && !v2.mag().isApproxZero()) {
                        GeomVector v1xv2 = v1.cross(v2).toUnitVector();
                        if (!v1xv2.get(0).isNaN())
                            neighbors.add(v1xv2);
                    }
                    v1 = v2;
                    v2 = p4.minus(p1).toGeomVector();
                    if (!v1.mag().isApproxZero() && !v2.mag().isApproxZero()) {
                        GeomVector v1xv2 = v1.cross(v2).toUnitVector();
                        if (!v1xv2.get(0).isNaN())
                            neighbors.add(v1xv2);
                    }
                }
                if (j > 0) {
                    GeomPoint p2 = array.get(i).get(j - 1).toDimension(3);
                    GeomPoint p3 = array.get(i + 1).get(j).toDimension(3);
                    GeomVector v1 = p2.minus(p1).toGeomVector();
                    GeomVector v2 = p3.minus(p1).toGeomVector();
                    if (!v1.mag().isApproxZero() && !v2.mag().isApproxZero()) {
                        GeomVector v1xv2 = v1.cross(v2).toUnitVector();
                        if (!v1xv2.get(0).isNaN())
                            neighbors.add(v1xv2);
                    }
                }
            }

            //  Average together the normal vectors from all the neighbors.
            Vector sum = Vector.valueOf(0, 0, 0);
            int size = neighbors.size();
            if (size > 0) {
                for (int idx = 0; idx < size; ++idx) {
                    sum = sum.plus(neighbors.get(idx));
                }
                sum = sum.divide(size);
            } else
                sum = Vector.valueOf(1, 0, 0);

            float[] normal = new float[3];
            normal[0] = (float)sum.getValue(0);
            normal[1] = (float)sum.getValue(1);
            normal[2] = (float)sum.getValue(2);

            FastTable.recycle(neighbors);

            return normal;
        }
    }

    /**
     * A vertex in a BDS mesh.
     */
    private class BDSVertex {

        private final float[] pos = new float[3];
        private final float[] norm;

        public BDSVertex(GeomPoint point, float[] normal) {
            norm = normal;

            pos[0] = (float)point.getValue(0);
            pos[1] = (float)point.getValue(1);
            pos[2] = (float)point.getValue(2);
        }

        public void save(PrintWriter out) {
            // Location data.
            //  Swap Y & Z since bdStudio's CS is different than geomss's.
            out.print(pos[0]);
            out.print(" ");
            out.print(pos[2]);
            out.print(" ");
            out.print(pos[1]);
            out.print(" ");

            //  Normal data.
            out.print(norm[0]);
            out.print(" ");
            out.print(norm[2]);
            out.print(" ");
            out.println(norm[1]);
        }
    }

}
