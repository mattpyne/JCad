/*
 *   POIGeomReader  -- A class that can read a POI formatted geometry file.
 *
 *   Copyright (C) 2000-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import static geomss.geom.reader.AbstractGeomReader.RESOURCES;
import jahuwaldt.js.util.TextTokenizer;
import java.io.*;
import java.text.MessageFormat;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javolution.text.Text;
import javolution.text.TypeFormat;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading vehicle geometry from a POINTS (POI) formatted
 * geometry file. This is the geometry file format used by A502 (PANAIR) and A633
 * (TRANAIR) for input geometry.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 14, 2000
 * @version September 9, 2016
 */
public class POIGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    private static final boolean DEBUG = false;

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("poiDescription");

    // Some error messages.
    private static final String kIncRowsCols = RESOURCES.getString("incRowsColsErr").
            replace("<TYPE/>", DESCRIPTION);

    // The number of characters per number stored in the POI file.
    private static final int FIELDSIZE = 10;

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "poi";

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred extension for this file type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read paneled geometry from the specified
     * input file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file format is definitely recognized by this reader.
     *         GeomReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".poi"))
            response = MAYBE;

        //  POI files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        //  Create an input stream from the file.
        try (FileInputStream input = new FileInputStream(inputFile)) {

            //  Create a buffer to hold the data read in from the file.
            byte[] buffer = new byte[10240];

            //  Read in up to 10k worth of data.
            int byteCount = input.read(buffer, 0, 10240);
            if (byteCount < 0)
                return NO;

            //  Turn the buffer into a (potentially 10k long) string.
            String str = new String(buffer, 0, byteCount);

            //  Convert the string into a line number reader.
            LineNumberReader lnr = new LineNumberReader(new StringReader(str));

            //  Search for a line that starts with "$POI".
            String line = lnr.readLine();
            while (nonNull(line)) {
                if (line.startsWith("$POI")) {
                    response = YES;
                    break;
                }
                line = lnr.readLine();
            }

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return response;
    }

    /**
     * Reads in a POINTS (POI) geometry file from the specified input file and returns a
     * {@link PointVehicle} object that contains the geometry from the POI file.
     * <p>
     * Each component will have an Integer stored in the user data under the key
     * "A502A633TypeCode" that contains the A502-A633 array type code for all the networks
     * contained in that component.
     * </p>
     * <p>
     * WARNING: This file format is not unit aware. You must set the units
     * to be used by calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link PointVehicle} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         components in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public PointVehicle read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        // Create an empty vehicle with the provided name as the vehicle name.
        PointVehicle vehicle = PointVehicle.newInstance(inputFile.getName());

        //  POI files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the ASCII file.
        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile))) {

            // Loop over all the components stored in the file.
            String aLine = reader.readLine();
            while (nonNull(aLine)) {

                // Skip ahead to the next "$POI" line.
                while (nonNull(aLine) && !aLine.startsWith("$POI"))
                    aLine = reader.readLine();
                if (isNull(aLine))
                    break;

                // A $POI line was found, read in the component.
                PointComponent comp = readComponent(reader);
                vehicle.add(comp);

                // Begin searching for the next component.
                aLine = reader.readLine();
            }

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return vehicle;
    }

    /**
     * This method always returns <code>false</code> as POI files do not encode the units
     * that are being used. You must call <code>setFileUnits</code> to set the units being
     * used before reading from a file of this format.
     *
     * @return This implementation always returns false.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

    /**
     * Reads a single component made up of multiple networks of the same type from an
     * input stream pointing to a POI file. This method assumes that the stream starts
     * immediately after the $POI line in a POI file.
     *
     * @param in Reader for the POI file we are reading (positioned so that the next read
     *           will occur on the line following the $POI line).
     * @return The component read in from the file.
     */
    private PointComponent readComponent(LineNumberReader in) throws IOException {
        PointComponent comp = null;

        try {
            // Read in the number of arrays.
            String aLine = readLine(in);
            String subStr = aLine.substring(0, FIELDSIZE - 1).trim();
            int numArrs = Integer.parseInt(subStr);

            // Read in the component type.
            aLine = readLine(in);
            subStr = aLine.substring(0, FIELDSIZE - 1).trim();
            Integer typeCode = new Integer(subStr);
            String typeStr = "Type " + typeCode;

            // Loop over each array and read it in, adding it to a new component.
            comp = PointComponent.newInstance(typeStr);
            comp.putUserData("A502A633TypeCode", typeCode);
            for (int i = 0; i < numArrs; ++i) {
                PointArray net = readNetwork(in);
                comp.add(net);
            }

        } catch (NumberFormatException e) {
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("parseErrMsg"), DESCRIPTION, in.getLineNumber()));
        }

        return comp;
    }

    /**
     * Reads a single array or network from an input reader (pointing to a POI file). This
     * method assumes that the stream starts immediately after the line containing the
     * component type in a POI file (two lines after the $POI line).
     *
     * @param in Reader for the POI file we are reading (positioned so that the next read
     *           will occur on the line following the line identifying component type).
     * @return The network read in from the file.
     * @throws java.io.IOException if there is a problem reading from the input reader.
     */
    private PointArray readNetwork(LineNumberReader in) throws IOException {
        PointArray net = null;

        // Create the needed tables.
        FastTable<Point> pointList = FastTable.newInstance();
        FastTable<PointString<Point>> stringList = FastTable.newInstance();

        try {
            // Read in the number of rows and columns.
            String aLine = readLine(in);
            TextTokenizer tokenizer = TextTokenizer.valueOf(aLine, " ");
            if (tokenizer.countTokens() != 3)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("parseErrMsg"), DESCRIPTION, in.getLineNumber()));

            Text token = tokenizer.nextToken();
            int numCols = (int)TypeFormat.parseDouble(token);

            token = tokenizer.nextToken();
            int numRows = (int)TypeFormat.parseDouble(token);

            // Do a sanity check.
            if (numRows < 0 || numRows > 1000000 || numCols < 0 || numCols > 1000000)
                throw new IOException(kIncRowsCols + in.getLineNumber() + ".");

            // Read in the name of this network.
            Text name = tokenizer.nextToken();

            if (DEBUG)
                System.out.println("name = " + name + ", numRows = " + numRows + ", numCols = " + numCols);

            // Loop over each row.
            for (int i = 0; i < numRows; ++i) {
                if (DEBUG)
                    System.out.println("row = " + i);
                aLine = readLine(in);
                tokenizer.setText(aLine);
                if (tokenizer.countTokens() != 3 && tokenizer.countTokens() != 6)
                    throw new IOException(MessageFormat.format(
                            RESOURCES.getString("incPointCount"), in.getLineNumber()));

                // Loop over each column.
                int count = 0;
                for (int j = 0; j < numCols; ++j) {

                    // There are two points per line.
                    if (count > 1) {
                        count = 0;
                        aLine = readLine(in);
                        tokenizer.setText(aLine);
                        if (tokenizer.countTokens() != 3 && tokenizer.countTokens() != 6)
                            throw new IOException(MessageFormat.format(
                                    RESOURCES.getString("incPointCount"), in.getLineNumber()));
                    }

                    // Read in X coordinate.
                    token = tokenizer.nextToken();
                    double xValue = TypeFormat.parseDouble(token);

                    // Read in Y coordinate.
                    token = tokenizer.nextToken();
                    double yValue = TypeFormat.parseDouble(token);

                    // Read in Z coordinate.
                    token = tokenizer.nextToken();
                    double zValue = TypeFormat.parseDouble(token);

                    if (DEBUG)
                        System.out.println("col = " + j + ", x,y,z = " + xValue + ", " + yValue + ", " + zValue);

                    //  Create a Point object.
                    Point point = Point.valueOf(xValue, yValue, zValue, getFileUnits());
                    pointList.add(point);

                    ++count;
                }

                //  Create a PointString object from the points.
                PointString<Point> string = PointString.valueOf(null, pointList);
                stringList.add(string);

                //  Clear the point list for the next row.
                pointList.clear();
            }

            // Create a new network from the points just read in.
            net = PointArray.valueOf(null, stringList);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("parseErrMsg"), DESCRIPTION, in.getLineNumber()));

        } finally {
            //  Recycle the lists.
            FastTable.recycle(pointList);
            FastTable.recycle(stringList);
        }

        return net;
    }

}
