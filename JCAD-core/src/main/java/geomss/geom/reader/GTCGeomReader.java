/*
 *   GTCGeomReader  -- A class that can read GridTool formatted restart file.
 *
 *   Copyright (C) 2013-2016, Joseph A. Huwaldt
 *   All rights reserved.
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import geomss.geom.nurbs.*;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.js.util.TextTokenizer;
import jahuwaldt.tools.math.MathTools;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.text.Text;
import javolution.text.TypeFormat;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading geometry from a GridTool restart file. GridTool is a
 * NASA developed grid/geometry program that is a part of the NASA TetrUSS CFD system.
 * More information can be found here: <a href="http://geolab.larc.nasa.gov/GridTool/">
 * http://geolab.larc.nasa.gov/GridTool/</a>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: December 26, 2013
 * @version September 9, 2016
 */
public class GTCGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    //private static final boolean DEBUG = true;
    
    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("gtcDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "rst";

    //  10 kilobytes
    private static final int TENK = 10240;
    
    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * This method always returns <code>false</code> as GTC restart files do not encode
     * the units that are being used. You must call <code>setFileUnits</code> to set the
     * units being used before reading from a file of this format, otherwise
     * default system units will be assumed.
     *
     * @return this implementation always returns false
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     * GeomReader.YES if the file format is definitely recognized by this reader.
     * GeomReader.MAYBE if the file format might be readable by this reader, but that
     * can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {
        requireNonNull(inputFile);
        
        //  GTC files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        int response = NO;
        //  Create an input stream from the file.
        try (FileInputStream input = new FileInputStream(inputFile)) {

            //  Create a buffer to hold the data read in from the file.
            byte[] buffer = new byte[TENK];

            //  Read in up to 10k worth of data.
            int byteCount = input.read(buffer, 0, TENK);
            if (byteCount < 0)
                return NO;

            //  Turn the buffer into a (potentially 10k long) string.
            String str = new String(buffer, 0, byteCount);

            //  Convert the string into a line number reader.
            LineNumberReader lnr = new LineNumberReader(new StringReader(str));

            //  The 5th line should start with "Version X.X" where X.X is >= 4.1
            lnr.readLine();
            lnr.readLine();
            lnr.readLine();
            lnr.readLine();
            String aLine = lnr.readLine();
            TextTokenizer tokenizer = TextTokenizer.valueOf(aLine, " ");
            Text token = tokenizer.nextToken();
            if (token.equals(Text.valueOf("Version"))) {
                token = tokenizer.nextToken();
                String[] parts = token.toString().split("\\.");
                if (parts.length > 2)
                    return NO;
                double major = TypeFormat.parseInt(parts[0]);
                double minor = TypeFormat.parseInt(parts[1]);
                double ver = major + minor / 10;
                if (ver == 4.1)
                    return YES;
            }

        } catch (Throwable e) {
            //  Any problem reading is a failure.
            response = NO;

        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return response;
    }

    /**
     * <p>
     * Reads in a GridTool restart file from the specified input file and returns a
     * {@link GeometryList} object that contains the geometry from the file. The output
     * list contains 4 sub-lists: SURFACES, CURVES, PATCHES and SOURCES. Empty lists are
     * included if any of these entities is not included in the file.
     * </p>
     *
     * The top-level list has the following data stored in the user data using the
     * following keys:
     * <UL>
     * <LI> GTC_Version -- The GTC version code. Must be >= 1.4 to be read by this reader.
     * <LI> GTC_Tol -- The gobal geometry tolerance used.
     * <LI> GTC_Name -- The GTC model name.
     * </UL>
     *
     * Each sub-list is defined as follows:
     * <DL>
     * <DT> SURFACES
     * <DD> All the NURBS surfaces and NURBS curves in the geometry. Each surface & curve
     * has stored in its user data the minimum and maximum parameter ranges for the
     * original geometry using the GTC_U0, GTC_U1, GTC_V0, and GTC_V1 keys. All NURBS
     * surfaces & curves in GeomSS have parameter ranges from 0 to 1, but other geometry
     * systems allow different parameter ranges. In addition, the IGES type code for the
     * geometry is stored with the GTC_Type key and the family name is stored with the
     * GTC_Family key.
     * <DT> CURVES
     * <DD> This list contains PointString objects (which are lists of points) for each
     * "curve". Each point stored in each point string is either free (in X,Y,Z) or
     * subranged onto a NURBS surface or curve.
     * <DT> PATCHES
     * <DD> Each patch in the PATCHES list contains two lists, one containing either
     * nothing or the NURBS surface associated with the patch and one containing a list of
     * edge curves (PointStrings) for the patch. Each patch has user data stored with it
     * using the following keys:
     * <UL>
     * <LI> GTC_Type -- The patch type code
     * <LI> GTC_BCType -- The boundary condition type code
     * <LI> GTC_Family -- The family name
     * <LI> GTC_ValidPatch -- A patch validity flag
     * <LI> GTC_D3MNumber -- The d3m number for the patch
     * <LI> GTC_EdgeSideIndexes -- A list of side indexes for each edge (which side does
     * each edge belong to)
     * <LI> GTC_EdgeReversedFlags -- A list of flags indicating if each edge is reversed or not.
     * </UL>
     * <DT> SOURCES
     * <DD> The sources for the "background grid" are stored in this list as PointString
     * objects. Each PointString has two points for the ends of the source (though the 2nd
     * one is sometimes not required, it is always there). As with curves, the points can
     * either be in free space (X,Y,Z) or subranged onto a NURBS curve or surface. The
     * global source list has the following user data keys associated with it:
     * <UL>
     * <LI> GTC_Delta1, GTC_Rate1, & GTC_Rate2 -- Global boundary layer growth parameters.
     * <LI> GTC_NLayers -- The number of layers in the boundary layer grid
     * <LI> GTC_NIterations -- The number of refinement iterations for the boundary layer grid.
     * <LI> GTC_Stretching -- Global flags indicating if there is cell stretching in this grid
     * <LI> GTC_ViscousLayers -- Global flag indicating if viscous layers should be grown or not.
     * </UL>
     * Each source in the SOURCES list has the following user data keys associated with it:
     * <UL>
     * <LI> GTC_SourceType -- The type code for the source.
     * <LI> GTC_Family -- The family name
     * <LI> GTC_StretchingDir -- The stretching direction code.
     * <LI> GTC_an, GTC_bn, and GTC_Alpha -- Source strength parameters
     * </UL>
     * Finally, each point in each source has the following keys stored in it's user data:
     * <UL>
     * <LI> GTC_s, GTC_S -- Source strength information
     * <LI> GTC_ro, GTC_ri -- Outer and inner radii for volume sources if required by the source type.
     * <LI> GTC_Delta1, GTC_Rate1, GTC_Rate2 -- Values to override the global boundary layer
     * grown parameters.
     * </UL>
     * </DL>
     * <p>
     * WARNING: This file format is not unit aware. You must set the units
     * to be used by calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not be 
     *                  null.
     * @return A {@link GeometryList} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         components in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    @SuppressWarnings("null")
    public GeometryList read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        //  Create an empty geometry list.
        GeometryList output = GeomList.newInstance();
        GeomList surfaces = GeomList.newInstance();
        surfaces.setName("SURFACES");
        output.add(surfaces);
        GeomList<PointString> curves = GeomList.newInstance();
        curves.setName("CURVES");
        output.add(curves);
        GeomList patches = GeomList.newInstance();
        patches.setName("PATCHES");
        output.add(patches);
        GeomList sources = GeomList.newInstance();
        sources.setName("SOURCES");
        output.add(sources);

        Unit<Length> unit = getFileUnits();
        TextTokenizer tokenizer = TextTokenizer.newInstance();
        tokenizer.setDelimiters(" ");

        //  GTC files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        // Create a reader to access the ASCII file.
        try (LineNumberReader reader = new LineNumberReader(new FileReader(inputFile))) {
            try {
                //  The 1st 4 lines are comments.
                for (int i = 0; i < 4; ++i)
                    readLine(reader);

                //  Get the GTC version information.
                String aLine = readLine(reader);
                tokenizer.setText(aLine);
                tokenizer.nextToken();
                String version = tokenizer.nextToken().toString();
                output.putUserData("GTC_Version", version);

                //  Read in the global tolerance parameter.
                Text token = tokenizer.nextToken();
                double tol = TypeFormat.parseDouble(token);
                output.putUserData("GTC_Tol", Parameter.valueOf(tol, unit));

                //  Read in the name of the model.
                String name = readLine(reader).trim();
                output.setName(name);
                output.putUserData("GTC_Name", name);

                //  Skip the General Display Constants.
                for (int i = 0; i < 4; ++i)
                    readLine(reader);

                //  Read in all the surface definitions and put them in the "surfaces" list.
                readSurfaces(reader, tokenizer, unit, surfaces);

                //  Read in all the "curve" definitions (which are actually strings of points).
                readCurves(reader, tokenizer, unit, surfaces, curves);

                //  Read in the patch defintitions into the "patches" list.
                readPatches(reader, tokenizer, surfaces, curves, patches);

                //  Read in the source definitions into the "sources" list.
                readSources(reader, tokenizer, sources, unit, surfaces);

            } catch (NumberFormatException e) {
                throw new IOException(MessageFormat.format(RESOURCES.getString("parseErrMsg"),
                        DESCRIPTION, reader.getLineNumber()));
            }
        } finally {
            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

        return output;
    }

    /**
     * Read in surface (and curve) definitions from the file./
     *
     * @param reader    The reader for reading lines of data from the file.
     * @param tokenizer The tokenizer to use to tokenize each line.
     * @param unit      The unit used for interpreting the data.
     * @param surfaces  The list of surfaces to have all the new ones added to.
     * @throws IOException If there is any problem reading from the file.
     */
    private static void readSurfaces(LineNumberReader reader, TextTokenizer tokenizer,
            Unit<Length> unit, GeomList surfaces) throws IllegalArgumentException, IOException {

        //  The next line should contain surface definitions.
        String aLine = readLine(reader).trim();
        if (!"SURFACES".equals(aLine) && !"GTSURFACES".equals(aLine))
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("missingTypeData"), "SURFACES"));

        //  Read in the number of surfaces.
        aLine = readLine(reader);
        int nsurf = TypeFormat.parseInt(aLine);

        //  Skip 2 lines.
        for (int i = 0; i < 2; ++i)
            readLine(reader);

        //  Loop over all the surfaces.
        for (int sidx = 0; sidx < nsurf; ++sidx) {
            //  Read in the code indicating if this is a trimmed surface.
            aLine = readLine(reader);
            tokenizer.setText(aLine);
            tokenizer.nextToken();
            tokenizer.nextToken();
            Text token = tokenizer.nextToken();
            int itrim = TypeFormat.parseInt(token);
            if (itrim == 1) {
                System.out.println("itrim == 1, sidx = " + sidx);
                System.out.println("Processing stopped");
                break;
            }

            //  Read in the number of control points and degreeU of the surface.
            aLine = readLine(reader);
            tokenizer.setText(aLine);
            token = tokenizer.nextToken();
            int k1 = TypeFormat.parseInt(token);
            token = tokenizer.nextToken();
            int k2 = TypeFormat.parseInt(token);
            tokenizer.nextToken();
            tokenizer.nextToken();
            token = tokenizer.nextToken();
            int m1 = TypeFormat.parseInt(token);
            token = tokenizer.nextToken();
            int m2 = TypeFormat.parseInt(token);

            //  Read in the family name & IGES type.
            aLine = readLine(reader);
            tokenizer.setText(aLine);
            tokenizer.nextToken();
            String family = tokenizer.nextToken().toString();
            String IGEStype = tokenizer.nextToken().toString();

            //  Skip a line.
            readLine(reader);

            //  Read in the S-direction knot vector.
            int n = k1 + 1 + m1 + 1;        //  Number of knotsU.
            double sStart = Double.MAX_VALUE;
            double sEnd = -Double.MAX_VALUE;
            double[] sKnots = new double[n];
            for (int i = 0; i < n; ++i) {
                aLine = readLine(reader).trim();
                double u = TypeFormat.parseDouble(aLine);
                sKnots[i] = u;
                if (u < sStart)
                    sStart = u;
                if (u > sEnd)
                    sEnd = u;
            }

            //  Scale the knotsU into the range 0-1.
            double[] scaledSKnots = sKnots;
            if (Math.abs(sStart) > MathTools.EPS || Math.abs(sEnd - 1.0) > MathTools.EPS) {
                scaledSKnots = new double[n];
                double m = 1. / (sEnd - sStart);
                double b = -m * sStart;
                for (int i = 0; i < n; ++i) {
                    double kv = sKnots[i];
                    kv = scaleParam(m, b, kv);
                    scaledSKnots[i] = kv;
                }
            }

            //  Read in the T-direction knot vector.
            n = k2 + 1 + m2 + 1;          //  Number of knotsU.
            double[] tKnots = new double[n];
            double tStart = Double.MAX_VALUE;
            double tEnd = -Double.MAX_VALUE;
            for (int i = 0; i < n; ++i) {
                aLine = readLine(reader).trim();
                double v = TypeFormat.parseDouble(aLine);
                tKnots[i] = v;
                if (v < tStart)
                    tStart = v;
                if (v > tEnd)
                    tEnd = v;
            }

            //  Scale the knotsU into the range 0-1.
            double[] scaledTKnots = tKnots;
            if (Math.abs(tStart) > MathTools.EPS || Math.abs(tEnd - 1.0) > MathTools.EPS) {
                scaledTKnots = new double[n];
                double m = 1. / (tEnd - tStart);
                double b = -m * tStart;
                for (int i = 0; i < n; ++i) {
                    double kv = tKnots[i];
                    kv = scaleParam(m, b, kv);
                    scaledTKnots[i] = kv;
                }
            }

            //  Create the knot vectors.
            KnotVector kvS = KnotVector.newInstance(m1, scaledSKnots);
            KnotVector kvT = KnotVector.newInstance(m2, scaledTKnots);

            //  Read in the control point coordinates.
            double[][] cpsX = new double[k1 + 1][k2 + 1];
            double[][] cpsY = new double[k1 + 1][k2 + 1];
            double[][] cpsZ = new double[k1 + 1][k2 + 1];
            double[][] cpsW = new double[k1 + 1][k2 + 1];
            readArray(reader, tokenizer, k1, k2, cpsX);
            readArray(reader, tokenizer, k1, k2, cpsY);
            readArray(reader, tokenizer, k1, k2, cpsZ);
            readArray(reader, tokenizer, k1, k2, cpsW);

            //  Skip 18 lines.
            for (int i = 0; i < 18; ++i)
                readLine(reader);

            if (IGEStype.equals("-126")) {
                //  Collapsed surface. It's actually a curve.
                FastTable<ControlPoint> cps = FastTable.newInstance();
                for (int i = 0; i <= k1; ++i) {
                    double weight = cpsW[i][0];
                    double x = cpsX[i][0];
                    double y = cpsY[i][0];
                    double z = cpsZ[i][0];
                    Point pnt = Point.valueOf(x, y, z, unit);
                    cps.add(ControlPoint.valueOf(pnt, weight));
                }

                //  Create the curve.
                BasicNurbsCurve crv = BasicNurbsCurve.newInstance(cps, kvS);
                crv.putUserData("GTC_U0", sStart);
                crv.putUserData("GTC_U1", sEnd);
                crv.putUserData("GTC_Family", family);
                crv.putUserData("GTC_Type", IGEStype);
                crv.setName(family);
                surfaces.add(crv);

            } else {
                //  A normal surface.

                //  Create the control point network.
                ControlPoint[][] controlPoints = new ControlPoint[k2 + 1][k1 + 1];
                for (int i = 0; i <= k1; ++i) {
                    for (int j = 0; j <= k2; ++j) {
                        double weight = cpsW[i][j];
                        double x = cpsX[i][j];
                        double y = cpsY[i][j];
                        double z = cpsZ[i][j];
                        Point pnt = Point.valueOf(x, y, z, unit);
                        controlPoints[j][i] = ControlPoint.valueOf(pnt, weight);
                    }
                }
                ControlPointNet cpNet = ControlPointNet.valueOf(controlPoints);

                //  Create the surface.
                BasicNurbsSurface surface = BasicNurbsSurface.newInstance(cpNet, kvS, kvT);
                surface.putUserData("GTC_U0", sStart);
                surface.putUserData("GTC_U1", sEnd);
                surface.putUserData("GTC_V0", tStart);
                surface.putUserData("GTC_V1", tEnd);
                surface.putUserData("GTC_Family", family);
                surface.putUserData("GTC_Type", IGEStype);
                surface.setName(family);
                surfaces.add(surface);
            }

        }
    }   //  end readSurfaces()

    /**
     * Read a single array of data from the file.
     */
    private static void readArray(LineNumberReader reader, TextTokenizer tokenizer,
            int n1, int n2, double[][] arr) throws IOException, NumberFormatException {
        String aLine = readLine(reader);
        tokenizer.setText(aLine);
        for (int i = 0; i <= n1; ++i) {
            for (int j = 0; j <= n2; ++j) {
                if (!tokenizer.hasMoreTokens()) {
                    aLine = readLine(reader);
                    tokenizer.setText(aLine);
                }
                Text token = tokenizer.nextToken();
                double v = TypeFormat.parseDouble(token);
                arr[i][j] = v;
            }
        }
    }

    /**
     * Scale the parameter value in the the required range from 0.0 to 1.0.
     */
    private static double scaleParam(double m, double b, double value) {
        //  Scale the parameter value.
        double kv = m * value + b;

        //  Watch for roundoff.
        if (kv < 0.0)
            kv = 0.0;
        else if (kv > 1.0)
            kv = 1.0;

        return kv;
    }

    /**
     * Read in the list of curves from the data file. "curves" are actually lists of
     * points in GTC terminology.
     *
     * @param reader    The reader for reading lines of data from the file.
     * @param tokenizer The tokenizer to use to tokenize each line.
     * @param unit      The unit used for interpreting the data.
     * @param surfaces  The list of already existing surfaces (some curve points are
     *                  subranges onto surfaces).
     * @param curves    The list to be filled in with PointString objects for each
     *                  "curve".
     * @throws IOException If there is any problem reading from the file.
     */
    private static void readCurves(LineNumberReader reader, TextTokenizer tokenizer, Unit<Length> unit,
            GeomList surfaces, GeomList<PointString> curves) throws NumberFormatException, IOException {

        //  The next line should contain "curve" definitions.
        String aLine = readLine(reader).trim();
        if (!"CURVES".equals(aLine) && !"GTCURVES".equals(aLine))
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("missingTypeData"), "CURVES"));

        //  Read in the number of curves.
        aLine = readLine(reader);
        int ncrvs = TypeFormat.parseInt(aLine);

        //  Skip 2 lines.
        for (int i = 0; i < 2; ++i)
            readLine(reader);

        //  Loop over all the "curves" (which are actually stings of points).
        for (int cidx = 0; cidx < ncrvs; ++cidx) {
            //  Read in the number of points in this curve.
            aLine = readLine(reader);
            tokenizer.setText(aLine);
            tokenizer.nextToken();
            tokenizer.nextToken();
            Text token = tokenizer.nextToken();
            int npts = TypeFormat.parseInt(token);

            //  Get the name of this "curve".
            tokenizer.nextToken();
            tokenizer.nextToken();
            String crvName = tokenizer.nextToken().toString();

            //  Skip a line.
            readLine(reader);

            //  Read in all the points for this curve.
            PointString str = PointString.newInstance();
            str.setName(crvName);
            for (int i = 0; i < npts; ++i) {
                aLine = readLine(reader);
                tokenizer.setText(aLine);
                token = tokenizer.nextToken();
                double x = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double y = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double z = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double u = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double v = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                int j = TypeFormat.parseInt(token);

                if (j == 0) {
                    //  Not attached to a "surface".
                    Point p = Point.valueOf(x, y, z, unit);
                    str.add(p);

                } else {
                    SubrangePoint p;
                    --j;    //  Convert index from unit offset to 0 offset.
                    if (surfaces.get(j) instanceof BasicNurbsSurface) {
                        BasicNurbsSurface Ssrf = (BasicNurbsSurface)surfaces.get(j);

                        //  Extract the original parametric bounds of the surface.
                        double sStart = (Double)Ssrf.getUserData("GTC_U0");
                        double tStart = (Double)Ssrf.getUserData("GTC_V0");
                        double sEnd = (Double)Ssrf.getUserData("GTC_U1");
                        double tEnd = (Double)Ssrf.getUserData("GTC_V1");

                        //  Scale the parameters into range.
                        double mS = 1. / (sEnd - sStart);
                        double bS = -mS * sStart;
                        double mT = 1. / (tEnd - tStart);
                        double bT = -mT * tStart;
                        u = scaleParam(mS, bS, u);
                        v = scaleParam(mT, bT, v);

                        //  Create the point.
                        p = Ssrf.getPoint(u, v);

                    } else {
                        BasicNurbsCurve Bcrv = (BasicNurbsCurve)surfaces.get(j);

                        //  Extract the original parametric bounds of the surface.
                        double sStart = (Double)Bcrv.getUserData("GTC_U0");
                        double sEnd = (Double)Bcrv.getUserData("GTC_U1");

                        //  Scale the parameter into range.
                        double mS = 1. / (sEnd - sStart);
                        double bS = -mS * sStart;
                        u = scaleParam(mS, bS, u);

                        //  Create the point.
                        p = Bcrv.getPoint(u);
                    }
                    str.add(p);
                }
            }
            curves.add(str);
        }

    }   //  end readCurves()

    /**
     * Read in the patch definitions from the file.
     *
     * @param reader    The reader for reading lines of data from the file.
     * @param tokenizer The tokenizer to use to tokenize each line.
     * @param unit      The unit used for interpreting the data.
     * @param surfaces  The list of already existing surfaces (some patches lie on top of
     *                  surfaces).
     * @param curves    The list of already existing curve definitions (patch boundaries
     *                  are made up of curves).
     * @param patches   The list of patches to be filled in by this method.
     * @throws IOException If there is any problem reading from the file.
     */
    private static void readPatches(LineNumberReader reader, TextTokenizer tokenizer, GeomList surfaces,
            GeomList<PointString> curves, GeomList patches) throws IOException {

        //  The next line should contain "patch" definitions.
        String aLine = readLine(reader).trim();
        if (!"PATCHES".equals(aLine) && !"GTPATCHES".equals(aLine))
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("missingTypeData"), "PATCHES"));

        //  Read in the number of patches.
        aLine = readLine(reader);
        int npatches = TypeFormat.parseInt(aLine);

        //  Skip 3 lines.
        for (int i = 0; i < 3; ++i)
            readLine(reader);

        //  Loop over all the patches.
        for (int pidx = 0; pidx < npatches; ++pidx) {
            GeomList patch = GeomList.newInstance();

            aLine = readLine(reader);
            tokenizer.setText(aLine);
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();

            //  Read in the number of edges for the patch.
            Text token = tokenizer.nextToken();
            int nedges = TypeFormat.parseInt(token);

            //  Read in the patch type code.
            token = tokenizer.nextToken();
            int pType = TypeFormat.parseInt(token);
            patch.putUserData("GTC_PatchType", pType);

            //  Read in the boundary condition type code.
            token = tokenizer.nextToken();
            int BCType = TypeFormat.parseInt(token);
            patch.putUserData("GTC_BCType", BCType);

            //  Read in the family name.
            String family = tokenizer.nextToken().toString();
            patch.putUserData("GTC_Family", family);

            //  Read in the next line.
            aLine = readLine(reader);
            tokenizer.setText(aLine);

            //  Read in the d3m number
            token = tokenizer.nextToken();
            int d3m = TypeFormat.parseInt(token);
            patch.putUserData("GTC_D3MNumber", d3m);
            patch.putUserData("GTC_ValidPatch", (d3m == -1 ? Boolean.FALSE : Boolean.TRUE));

            //  Read in the number of surfaces (should be either 0 or 1).
            token = tokenizer.nextToken();
            int nsrf = TypeFormat.parseInt(token);
            if (nsrf < 0 || nsrf > 1)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("patchSrfNumErr"), (pidx + 1), nsrf));

            GeomList srfLst = GeomList.newInstance();
            patch.add(srfLst);
            if (nsrf > 0) {
                //  Read in the surface related information.
                aLine = readLine(reader);
                tokenizer.setText(aLine);

                //  Read in the surface ID number
                token = tokenizer.nextToken();
                int srfID = TypeFormat.parseInt(token);
                srfLst.add(surfaces.get(srfID - 1));

                // Skip the next two lines.
                readLine(reader);
                readLine(reader);
            }

            //  Create a list of edges.
            GeomList<PointString> edgeLst = GeomList.newInstance();
            patch.add(edgeLst);

            //  Create a list of side index numbers.
            FastTable<Integer> sides = FastTable.newInstance();
            patch.putUserData("GTC_EdgeSideIndexes", sides);

            //  Create a list of flags indicating if the edge curve is reversed or not.
            FastTable<Boolean> reversed = FastTable.newInstance();
            patch.putUserData("GTC_EdgeReversedFlags", reversed);

            //  Loop over the edgeLst.
            int side = 0;
            for (int eidx = 0; eidx < nedges; ++eidx) {
                //  Read in the edge.
                aLine = readLine(reader);
                tokenizer.setText(aLine);

                //  Skip the edge ID number (sequential).
                tokenizer.nextToken();

                //  Is the edge reversed?
                token = tokenizer.nextToken();
                int rev = TypeFormat.parseInt(token);
                if (rev == -1)
                    reversed.add(Boolean.TRUE);
                else
                    reversed.add(Boolean.FALSE);

                //  Read in the curve ID number.
                token = tokenizer.nextToken();
                int crvID = TypeFormat.parseInt(token);
                edgeLst.add(curves.get(crvID - 1));

                //  Read in the side flag.
                token = tokenizer.nextToken();
                int sideFlg = TypeFormat.parseInt(token);
                if (sideFlg == 1 || eidx == 0)
                    ++side;
                sides.add(side);
            }

            //  Save this patch in the output list.
            patches.add(patch);

        }   //  Next pidx

    }   //  end readPatches()

    /**
     * Read in the list of sources (or "background grid") from the file.
     *
     * @param reader    The reader for reading lines of data from the file.
     * @param tokenizer The tokenizer to use to tokenize each line.
     * @param unit      The unit used for interpreting the data.
     * @param surfaces  The list of already existing surfaces (some curve source are
     *                  subranges onto surfaces).
     * @param sources   The list to be filled in with information for each source.
     * @throws IOException If there is a problem reading from the file.
     */
    private static void readSources(LineNumberReader reader, TextTokenizer tokenizer, GeomList sources,
            Unit<Length> unit, GeomList surfaces) throws IOException, NumberFormatException {

        //  The next line should contain "source" definitions.
        String aLine = readLine(reader).trim();
        if (!"Background Grid".equals(aLine))
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("missingTypeData"), "Background Grid"));

        //  Read in the number of sources.
        aLine = readLine(reader);
        int nsources = TypeFormat.parseInt(aLine);

        //  Read in the global parameters.
        aLine = readLine(reader);
        tokenizer.setText(aLine);

        //  Read in the delta1 parameter.
        Text token = tokenizer.nextToken();
        double delta1 = TypeFormat.parseDouble(token);
        sources.putUserData("GTC_Delta1", delta1);

        //  Read in the rate1 parameter.
        token = tokenizer.nextToken();
        double rate1 = TypeFormat.parseDouble(token);
        sources.putUserData("GTC_Rate1", rate1);

        //  Read in the rate2 parameter.
        token = tokenizer.nextToken();
        double rate2 = TypeFormat.parseDouble(token);
        sources.putUserData("GTC_Rate2", rate2);

        //  Read in the number of layers parameter.
        token = tokenizer.nextToken();
        int nLayer = TypeFormat.parseInt(token);
        sources.putUserData("GTC_NLayers", nLayer);

        aLine = readLine(reader);
        tokenizer.setText(aLine);

        //  Read in the ni parameter.
        token = tokenizer.nextToken();
        int ni = TypeFormat.parseInt(token);
        sources.putUserData("GTC_NIterations", ni);

        //  Skip one.
        tokenizer.nextToken();

        //  Read in the stretching flag.
        token = tokenizer.nextToken();
        int stretching = TypeFormat.parseInt(token);
        sources.putUserData("GTC_Stretching", (stretching == 1 ? Boolean.TRUE : Boolean.FALSE));

        //  Read in the grow viscous layers flag.
        token = tokenizer.nextToken();
        int viscous = TypeFormat.parseInt(token);
        sources.putUserData("GTC_ViscousLayers", (viscous == 1 ? Boolean.TRUE : Boolean.FALSE));

        //  Loop over all the sources.
        for (int sidx = 0; sidx < nsources; ++sidx) {
            //  A "source" consists of two points.  The 2nd one is sometimes unnecessary, but always present.
            PointString<GeomPoint> source = PointString.newInstance();
            sources.add(source);

            aLine = readLine(reader);
            tokenizer.setText(aLine);
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();
            tokenizer.nextToken();

            //  Read in the source type.
            token = tokenizer.nextToken();
            int type = TypeFormat.parseInt(token);
            source.putUserData("GTC_SourceType", type);

            //  Read in the source family.
            String family = tokenizer.nextToken().toString();
            source.putUserData("GTC_Family", family);

            //  Read in the stretching direction.
            aLine = readLine(reader);
            tokenizer.setText(aLine);
            token = tokenizer.nextToken();
            int stetchingDir = TypeFormat.parseInt(token);
            source.putUserData("GTC_StretchingDir", stetchingDir);

            //  Read in an.
            aLine = readLine(reader);
            tokenizer.setText(aLine);
            token = tokenizer.nextToken();
            double an = TypeFormat.parseDouble(token);
            source.putUserData("GTC_an", an);

            //  Read in bn.
            token = tokenizer.nextToken();
            double bn = TypeFormat.parseDouble(token);
            source.putUserData("GTC_bn", bn);

            //  Read in alpha.
            token = tokenizer.nextToken();
            double alpha = TypeFormat.parseDouble(token);
            source.putUserData("GTC_Alpha", alpha);

            //  Read in the two points.
            for (int i = 0; i < 2; ++i) {
                //  Read in s.
                aLine = readLine(reader);
                tokenizer.setText(aLine);
                token = tokenizer.nextToken();
                double smalls = TypeFormat.parseDouble(token);

                //  Read in S.
                token = tokenizer.nextToken();
                double capS = TypeFormat.parseDouble(token);

                //  Read in ro.
                token = tokenizer.nextToken();
                double ro = TypeFormat.parseDouble(token);

                //  Read in ri.
                token = tokenizer.nextToken();
                double ri = TypeFormat.parseDouble(token);

                //  Skip next line.
                readLine(reader);

                //  Read in delta1.
                aLine = readLine(reader);
                tokenizer.setText(aLine);
                token = tokenizer.nextToken();
                delta1 = TypeFormat.parseDouble(token);

                //  Read in rate1.
                token = tokenizer.nextToken();
                rate1 = TypeFormat.parseDouble(token);

                //  Read in rate2.
                token = tokenizer.nextToken();
                rate2 = TypeFormat.parseDouble(token);

                //  Read in the point coordinates.
                aLine = readLine(reader);
                tokenizer.setText(aLine);
                token = tokenizer.nextToken();
                double x = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double y = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double z = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double u = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                double v = TypeFormat.parseDouble(token);
                token = tokenizer.nextToken();
                int j = TypeFormat.parseInt(token);

                if (j == 0) {
                    //  Not attached to a "surface".
                    Point p = Point.valueOf(x, y, z, unit);
                    source.add(p);

                } else {
                    SubrangePoint p;
                    --j;    //  Convert index from unit offset to 0 offset.
                    if (surfaces.get(j) instanceof BasicNurbsSurface) {
                        BasicNurbsSurface Ssrf = (BasicNurbsSurface)surfaces.get(j);

                        //  Extract the original parametric bounds of the surface.
                        double sStart = (Double)Ssrf.getUserData("GTC_U0");
                        double tStart = (Double)Ssrf.getUserData("GTC_V0");
                        double sEnd = (Double)Ssrf.getUserData("GTC_U1");
                        double tEnd = (Double)Ssrf.getUserData("GTC_V1");

                        //  Scale the parameters into range.
                        double mS = 1. / (sEnd - sStart);
                        double bS = -mS * sStart;
                        double mT = 1. / (tEnd - tStart);
                        double bT = -mT * tStart;
                        u = scaleParam(mS, bS, u);
                        v = scaleParam(mT, bT, v);

                        //  Create the point.
                        p = Ssrf.getPoint(u, v);

                    } else {
                        BasicNurbsCurve Bcrv = (BasicNurbsCurve)surfaces.get(j);

                        //  Extract the original parametric bounds of the surface.
                        double sStart = (Double)Bcrv.getUserData("GTC_U0");
                        double sEnd = (Double)Bcrv.getUserData("GTC_U1");

                        //  Scale the parameter into range.
                        double mS = 1. / (sEnd - sStart);
                        double bS = -mS * sStart;
                        u = scaleParam(mS, bS, u);

                        //  Create the point.
                        p = Bcrv.getPoint(u);
                    }
                    source.add(p);
                }

                //  Store the meta-data for this point.
                GeomPoint p = source.get(i);
                p.putUserData("GTC_s", smalls);
                p.putUserData("GTC_S", capS);
                p.putUserData("GTC_ro", ro);
                p.putUserData("GTC_ri", ri);
                p.putUserData("GTC_Delta1", delta1);
                p.putUserData("GTC_Rate1", rate1);
                p.putUserData("GTC_Rate2", rate2);

            }   //  Next i

            //  Skip the next 8*4 lines.
            for (int i = 0; i < 8 * 4; ++i) {
                readLine(reader);
            }

        }   //  Next sidx

    }   //  end readSources()

    /**
     * Returns <code>true</code>. This reader can write some entity types to a GTC file.
     *
     * @return true
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Writes out a geometry file for the geometry contained in the supplied
     * {@link GeometryList} object. If the input geometry is not 3D, it will be forced to
     * be 3D (by padding if there are too few or by truncating additional dimensions).
     * <p>
     * WARNING: This format is not unit aware. The geometry will be written out in
     * whatever its current units are! Make sure to convert to the desired units for the
     * file before calling this method.
     * </p>
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The {@link GeometryList} object containing the geometry to be
     *                   written out. May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  Check for input data formatting.
        int size = geometry.size();
        if (size > 4)
            throw new IOException(MessageFormat.format(RESOURCES.getString("inputGeomErr1"), size));
        for (int i = 0; i < size; ++i) {
            Object elem = geometry.get(i);
            if (!(elem instanceof GeomList))
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("inputGeomErr2"), elem.getClass(), i + 1));
        }

        //  Pull out the big pieces.
        GeomList surfaces = (GeomList)geometry.get(0);
        GeomList<PointString<GeomPoint>> curves;
        if (size > 1)
            curves = (GeomList<PointString<GeomPoint>>)geometry.get(1);
        else
            curves = GeomList.newInstance();
        GeomList<GeomList> patches;
        if (size > 2)
            patches = (GeomList<GeomList>)geometry.get(2);
        else
            patches = GeomList.newInstance();
        GeomList<PointString<GeomPoint>> sources;
        if (size == 4)
            sources = (GeomList<PointString<GeomPoint>>)geometry.get(3);
        else
            sources = GeomList.newInstance();

        //  The surfaces list must contain only NURBS surfaces and curves.
        size = surfaces.size();
        for (int i = 0; i < size; ++i) {
            Object elem = surfaces.get(i);
            if (!(elem instanceof NurbsSurface) && !(elem instanceof NurbsCurve))
                throw new IOException(MessageFormat.format(RESOURCES.getString("elementTypeErr"),
                        "SURFACES", "NURBS surfaces & curves", elem.getClass(), i + 1));
        }

        //  The curves list must contain only PointString objects.
        size = curves.size();
        for (int i = 0; i < size; ++i) {
            Object elem = curves.get(i);
            if (!(elem instanceof PointString))
                throw new IOException(MessageFormat.format(RESOURCES.getString("elementTypeErr"),
                        "CURVES", "PointString objects", elem.getClass(), i + 1));
        }

        //  The patches list must contain lists that contain exactly two geometry lists;
        //  1st one with at most 1 surface, 2nd with a list of PointStrings.
        size = patches.size();
        for (int i = 0; i < size; ++i) {
            Object elem = patches.get(i);
            if (!(elem instanceof GeomList))
                throw new IOException(MessageFormat.format(RESOURCES.getString("elementTypeErr"),
                        "PATCHES", "GeomList objects", elem.getClass(), i + 1));

            GeomList<GeomList> patch = (GeomList<GeomList>)elem;
            if (patch.size() != 2)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("patchInputErr1"), patch.size(), i + 1));

            for (int j = 0; j < 2; ++j) {
                elem = patch.get(j);
                if (!(elem instanceof GeomList))
                    throw new IOException(MessageFormat.format(
                            RESOURCES.getString("patchInputErr2"), elem.getClass(), j + 1, i + 1));
            }

            if (patch.get(0).size() > 1)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("patchMultSrfErr"), patch.get(0).size(), i + 1));
            GeomList srfLst = patch.get(0);
            if (srfLst.size() > 0 && !(srfLst.get(0) instanceof NurbsSurface))
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("patchSubListTypeErr"), srfLst.get(0).getClass()));
            GeomList edges = patch.get(1);
            if (edges.size() < 3)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("patchEdgeCountErr"), edges.size(), i + 1));
            for (int j = 0; j < edges.size(); ++j) {
                elem = edges.get(j);
                if (!(elem instanceof PointString))
                    throw new IOException(MessageFormat.format(
                            RESOURCES.getString("patchEdgeTypeErr"), elem.getClass(), j + 1, i + 1));
            }
        }

        //  The sources list must contain only PointString objects each of which contains exactly 2 points.
        size = sources.size();
        for (int i = 0; i < size; ++i) {
            Object elem = sources.get(i);
            if (!(elem instanceof PointString))
                throw new IOException(MessageFormat.format(RESOURCES.getString("elementTypeErr"),
                        "SOURCES", "PointString objects", elem.getClass(), i + 1));
            PointString source = sources.get(i);
            if (source.size() != 2)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("sourceSizeErr"), source.size(), i + 1));
        }

        //  GTC files are required to be in ASCII with U.S. style number formatting.
        //  Get the default locale and save it.  Then change the default locale to US.
        Locale defLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        //  Create a PrintWriter that writes to the specified file.
        StackContext.enter();
        try (PrintWriter writer = new PrintWriter(outputFile, StandardCharsets.US_ASCII.name())) {

            //  Write out the header information.
            Unit<Length> units = geometry.getUnit();
            setFileUnits(units);
            writeHeader(writer, outputFile.getAbsolutePath(), geometry);

            //  Write out the NURBS surfaces & curves.
            writeSurfaces(writer, surfaces, units);

            //  Write out the CURVE data.
            writeCurves(writer, curves, units, surfaces);

            //  Write out the PATCHES data.
            writePatches(writer, patches, surfaces, curves);

            //  Write out the SOURCES data.
            writer.println("Background Grid");
            int nSources = sources.size();
            writer.println(0);
            writer.println("0.005 0.15 0.02 100");
            writer.println("20 0 0 0");

            //  Write out some mystery numbers.
            writer.println("1");
            writer.println("0");
            writer.println("0");
            writer.println("0");
            writer.println("1");
            writer.println("0");
            writer.println("0");
            writer.println("0");
            writer.println("1");
            writer.println("1 1 20");
            writer.println("1 -1 0 0");

            //  Write out the default SPAR parameters.
            writer.println("SPAR");
            writer.println("0");
            writer.println("15");
            writer.println("0.1");
            writer.println("0.1");
            writer.println("0.1");
            writer.println("0");
            writer.println("0");

        } finally {
            StackContext.exit();

            //  Restore the default locale.
            Locale.setDefault(defLocale);
        }

    }

    /**
     * Write out the header for the file.
     *
     * @param writer   The PrintWriter we are writing to the file with.
     * @param fileName The path to the file being written out.
     * @param geometry The geometry being written out.
     */
    private void writeHeader(PrintWriter writer, String fileName, GeometryList geometry) {
        //  Write out the date & time this file was written.
        Date theDate = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy", Locale.US);
        writer.print("The restart file was written on ");
        writer.println(fmt.format(theDate));

        //  Write out the file name.
        writer.print("File name is ");
        writer.println(fileName);

        //  Write out GTC information.
        writer.println("GTC 2010 - Build 20091209");

        //  Write out general constants.
        writer.println("General Constants");

        //  Write out the version.
        String ver = (String)geometry.getUserData("GTC_Version");
        if (isNull(ver))
            ver = "4.1";
        writer.print("Version ");   writer.print(ver);

        //  Write out the global tolerance value.
        Parameter<Length> tol = (Parameter<Length>)geometry.getUserData("GTC_Tol");
        if (isNull(tol))
            tol = Parameter.valueOf(1e-6, getFileUnits());
        writer.print(" ");  writer.print(tol.getValue());
        writer.println(" -1");

        //  Write out the model name.
        String name = (String)geometry.getUserData("GTC_Name");
        if (isNull(name))
            name = geometry.getName();
        if (isNull(name))
            name = "MyProject";
        writer.println(name);

        //  Write out general display constants.
        writer.println("General Display Constants");
        writer.println("0 0");
        writer.println("99 99 0 0");
        writer.println("99 99 0 0");

    }

    /**
     * Write out the list of surfaces to a GTC restart file.
     *
     * @param writer   The PrintWriter we are writing to the file with.
     * @param surfaces The list of surfaces to be written out.
     * @param units    The units for the geometry in the file.
     */
    private static void writeSurfaces(PrintWriter writer, GeomList surfaces, Unit<Length> units) {

        //  Write out the SURFACE data.
        writer.println("SURFACES");
        int nSrfs = surfaces.size();
        writer.println(nSrfs);
        writer.println("99 99 99 99");
        writer.println("99 99 99 99");

        //  Loop over all the NURBS surfaces & curves.
        Parameter<Length> srfTol = Parameter.valueOf(1e-4, units);
        for (int sidx = 0; sidx < nSrfs; ++sidx) {
            Object elem = surfaces.get(sidx);

            boolean isCrv = false;
            NurbsSurface srf;
            if (elem instanceof NurbsCurve) {
                // Convert curves to collapsed surfaces.
                NurbsCurve crv = ((NurbsCurve)elem).to(units);
                GeomList<NurbsCurve> crvs = GeomList.newInstance();
                crvs.add(crv);
                crvs.add(crv);
                srf = SurfaceFactory.createSkinnedSurface(crvs, 1, srfTol);
                srf.putAllUserData(crv.getAllUserData());
                srf.setName(crv.getName());
                isCrv = true;
            } else
                srf = ((NurbsSurface)elem).to(units);

            //  Write out the surface number line.
            writer.print("Surface_Number ");
            writer.print(sidx + 1);
            writer.println(" 0");

            //  Write out the number of control points and degreeU of the surface in each direction.
            int k1 = srf.getNumberOfRows();
            int k2 = srf.getNumberOfColumns();
            int m1 = srf.getSDegree();
            int m2 = srf.getTDegree();
            int n1 = k1 + m1;
            int n2 = k2 + m2;
            writer.print(k1 - 1);   writer.print(" ");
            writer.print(k2 - 1);   writer.print(" ");
            writer.print(n1);       writer.print(" ");
            writer.print(n2);       writer.print(" ");
            writer.print(m1);       writer.print(" ");
            writer.println(m2);

            //  Write out a mystery number.
            writer.print("1 ");

            //  Write out the family name.
            String family = (String)srf.getUserData("GTC_Family");
            if (isNull(family)) {
                family = srf.getName();
                if (isNull(family))
                    family = (isCrv ? "L0E126" : "L0E128");
            }
            writer.print(family);   writer.print(" ");

            //  Write out the surface IGES type code.
            Object IGESType = srf.getUserData("GTC_Type");
            if (isNull(IGESType))
                IGESType = (isCrv ? -126 : 128);
            writer.print(IGESType);
            writer.println(" 99");

            //  Write a line of mystery numbers.
            writer.println("0 0 0 0");

            //  Write out the knot vector in the S direction.
            KnotVector kS = srf.getSKnotVector();
            int nKnots = kS.length();
            for (int k = 0; k < nKnots; ++k) {
                double value = kS.getValue(k);
                writer.printf("%15.9g", value);
                writer.println();
            }

            //  Write out the knot vector in the T direction.
            KnotVector kT = srf.getTKnotVector();
            nKnots = kT.length();
            for (int k = 0; k < nKnots; ++k) {
                double value = kT.getValue(k);
                writer.printf("%15.9g", value);
                writer.println();
            }

            //  Write out the control points.
            ControlPointNet cpNet = srf.getControlPoints();

            //  Write out the X-coordinates.
            int count = 0;
            for (int i = 0; i < k1; ++i) {
                for (int j = 0; j < k2; ++j) {
                    ControlPoint cp = cpNet.get(i, j);
                    writer.printf("%15.9g ", cp.getPoint().getValue(0));
                    ++count;
                    if (count > 4) {
                        count = 0;
                        writer.println();
                    }
                }
            }
            if (count != 0)
                writer.println();

            //  Write out the Y-coordinates.
            count = 0;
            for (int i = 0; i < k1; ++i) {
                for (int j = 0; j < k2; ++j) {
                    ControlPoint cp = cpNet.get(i, j);
                    writer.printf("%15.9g ", cp.getPoint().getValue(1));
                    ++count;
                    if (count > 4) {
                        count = 0;
                        writer.println();
                    }
                }
            }
            if (count != 0)
                writer.println();

            //  Write out the Z-coordinates.
            count = 0;
            for (int i = 0; i < k1; ++i) {
                for (int j = 0; j < k2; ++j) {
                    ControlPoint cp = cpNet.get(i, j);
                    writer.printf("%15.9g ", cp.getPoint().getValue(2));
                    ++count;
                    if (count > 4) {
                        count = 0;
                        writer.println();
                    }
                }
            }
            if (count != 0)
                writer.println();

            //  Write out k1*k2 "1" values (I don't know what they are for).
            int size = k1 * k2;
            count = 0;
            for (int i = 0; i < size; ++i) {
                writer.print("              1 ");
                ++count;
                if (count > 4) {
                    count = 0;
                    writer.println();
                }
            }
            if (count != 0)
                writer.println();

            //  Write out 8 mystery number lines.
            writer.println("              0               1               0               1");
            writer.println("              0               1               0               1");
            writer.println("         0.0001          0.0001             0.5             0.5");
            writer.println("            0.1           1e-05               0               0");
            writer.println("0 1 0 1");
            writer.println("0 0 0 0");
            writer.println("0 0 0 0");
            writer.println("0 0 0 0");

            //  Write out the default display path.
            writer.print("0 ");
            if (isCrv) {
                if (k1 < 2)
                    k1 = 3;
                k2 = 2;
            } else {
                if (k1 < 2)
                    k1 = 3;
                if (k2 < 2)
                    k2 = 3;
            }
            writer.print(k1);   writer.print(" 0 ");
            writer.println(k2);

            //  Write out the current display path.
            writer.print("0 ");
            writer.print(k1);   writer.print(" 1 ");
            writer.println(k1);
            writer.print("0 ");
            writer.print(k2);   writer.print(" 1 ");
            writer.println(k2);

            //  Write some mystery numbers.
            writer.println("0 0 0 0");
            writer.println("1 2 1000 0");

            //  Write out display code and color (0 = don't draw, 1 = boundary, 2 = wireframe).
            //  Don't understand how colors are encoded, but 4287365120 == 0,0,140 in RGB.
            writer.println("2 4287365120 99 99");
            for (int i = 0; i < 4; ++i) {
                writer.println("99 99 99 99");
            }

        }   //  Next sidx

    }   //  end writeSurfaces()

    /**
     *
     * @param writer   The PrintWriter we are writing to the file with.
     * @param curves   The list of curve entities to write out (actually a list of
     *                 PointStrings).
     * @param units    The units for the geometry in the file.
     * @param surfaces The list of surfaces that the points in the curves may be subranged
     *                 onto.
     * @throws IOException If there is any problem writing to the file.
     */
    private static void writeCurves(PrintWriter writer, GeomList<PointString<GeomPoint>> curves,
            Unit<Length> units, GeomList surfaces) throws IOException {

        //  Write out the CURVE data.
        writer.println("CURVES");
        int nCrvs = curves.size();
        writer.println(nCrvs);
        writer.println("99 99 99 99");
        writer.println("99 99 99 99");

        //  Loop over all the curves.
        for (int cidx = 0; cidx < nCrvs; ++cidx) {
            PointString<GeomPoint> crv = curves.get(cidx).to(units);
            int npts = crv.size();

            //  Write out the curve index
            writer.print("Curve_Number ");
            writer.print(cidx + 1); writer.print(" ");

            //  Write out the number of points in the curve.
            writer.print(npts);     writer.print(" ");

            //  Write out a pair of mystery numbers.
            writer.print("0 2271780 ");

            //  Write out the family name for the curve.
            String family = (String)crv.getUserData("GTC_Family");
            if (isNull(family)) {
                family = crv.getName();
                if (isNull(family))
                    family = "Addams";
            }
            writer.println(family);

            //  Write some mystery numbers.
            writer.println("99 99 99 99");

            //  Loop over all the points in this curve.
            for (int pidx = 0; pidx < npts; ++pidx) {
                GeomPoint p = crv.get(pidx);

                //  Write out the coordinates of the point.
                writer.printf("%15g %15g %15g ", p.getValue(0), p.getValue(1), p.getValue(2));

                //  Write out the parametric coordinates (if any).
                if (p instanceof SubrangePoint) {
                    SubrangePoint sp = (SubrangePoint)p;
                    GeomPoint parPos = sp.getParPosition();

                    //  Get the index of the child object.
                    ParametricGeometry child = sp.getChild();
                    int sidx = surfaces.indexOf(child);
                    if (sidx < 0) {
                        writer.println("             -1              -1 0");
                    } else {
                        writer.printf("%15g %15g ", parPos.getValue(0),
                                (parPos.getPhyDimension() > 1 ? parPos.getValue(1) : 0));
                        writer.println(sidx + 1);
                    }

                } else
                    writer.println("             -1              -1 0");
            }
        }   //  Next cidx

    }   //  end writeCurves()

    /**
     * Write out patch data to the GTC file.
     *
     * @param writer   The PrintWriter we are writing to the file with.
     * @param patches  The list of patches to be written out.
     * @param surfaces The list of surfaces that a patch may be associated with.
     * @param curves   The list of curves (PointStrings) that the edges of this patch are
     *                 contained in.
     * @throws IOException
     */
    private static void writePatches(PrintWriter writer, GeomList<GeomList> patches, GeomList surfaces,
            GeomList<PointString<GeomPoint>> curves) throws IOException {

        //  Write out the PATCHES data.
        writer.println("PATCHES");
        int nPatches = patches.size();
        writer.println(nPatches);
        writer.print(nPatches);
        writer.println(" 0 0 2 2 0 0");
        writer.println("99 99 99 99");
        writer.println("99 99 99 99");

        //  Loop over all the patches.
        int d3mNum = 1;
        for (int pidx = 0; pidx < nPatches; ++pidx) {
            GeomList<GeomList> patch = patches.get(pidx);

            //  Write out the patch number.
            writer.print("Patch_Number ");
            writer.print(pidx + 1);

            //  Write out some mystery numbers.
            writer.print(" 0 16748573 ");

            //  Write out the number of edges.
            GeomList<PointString> edgeLst = patch.get(1);
            int nedges = edgeLst.size();
            writer.print(nedges);   writer.print(" ");

            //  Write out the patch type.
            Integer type = (Integer)patch.getUserData("GTC_PatchType");
            if (isNull(type))
                type = -1;
            writer.print(type);     writer.print(" ");

            //  Write out the boundary condition type.
            Integer BCType = (Integer)patch.getUserData("GTC_BCType");
            if (isNull(BCType))
                BCType = 5;
            writer.print(BCType);   writer.print(" ");

            //  Write out the family name.
            String family = (String)patch.getUserData("GTC_Family");
            if (isNull(family)) {
                family = patch.getName();
                if (isNull(family))
                    family = "Addams";
            }
            writer.println(family);

            //  Write out the D3M number.
            Boolean isValid = (Boolean)patch.getUserData("GTC_ValidPatch");
            if (isNull(isValid))
                isValid = Boolean.FALSE;
            if (isValid) {
                writer.print(d3mNum);   writer.print(" ");
                ++d3mNum;
            } else
                writer.print("-1 ");

            //  Write out the number of surfaces associated with this patch (0 or 1).
            int nsrfs = patch.get(0).size();
            if (nsrfs > 1)
                throw new IOException(MessageFormat.format(
                        RESOURCES.getString("sourceSizeErr"), nsrfs, pidx + 1));
            writer.print(nsrfs);
            writer.println(" 99 99");

            if (nsrfs > 0) {
                //  Write out the surface information.
                ParametricGeometry srf = (ParametricGeometry)patch.get(0).get(0);
                int sidx = surfaces.indexOf(srf);
                if (sidx < 0)
                    throw new IOException(MessageFormat.format(
                            RESOURCES.getString("missingPatchSrf"), pidx + 1));
                writer.print(sidx + 1); writer.print(" ");

                //  Write out the display path.
                boolean isCrv = false;
                int k1, k2;
                if (srf instanceof NurbsCurve) {
                    isCrv = true;
                    NurbsCurve ncrv = (NurbsCurve)srf;
                    k1 = ncrv.getControlPoints().size();
                    k2 = 2;
                } else {
                    NurbsSurface nsrf = (NurbsSurface)srf;
                    k1 = nsrf.getNumberOfRows();
                    k2 = nsrf.getNumberOfColumns();
                }
                if (isCrv) {
                    if (k1 < 2)
                        k1 = 3;
                    k2 = 2;
                } else {
                    if (k1 < 2)
                        k1 = 3;
                    if (k2 < 2)
                        k2 = 3;
                }
                writer.print(k1);   writer.print(" ");
                writer.print(k2);   writer.print(" ");

                //  Write out two mystery numbers.
                writer.println("1 1000");

                //  Write out the min/max U,V for the surface.
                writer.println("     0 1 0 1");

                //  Write out the patch project parameters.
                writer.println("     0.0001 0.0001 0.100000 1e-05");
            }

            //  Get the side indexes and reversed edge flags.
            List<Integer> sideLst = (List<Integer>)patch.getUserData("GTC_EdgeSideIndexes");
            List<Boolean> reversedLst = (List<Boolean>)patch.getUserData("GTC_EdgeReversedFlags");

            //  Loop over the edges.
            int prevSide = 0;
            for (int eidx = 0; eidx < nedges; ++eidx) {
                PointString edge = edgeLst.get(eidx);
                int ip1 = eidx + 1;

                //  Write the edge index.
                writer.print("           ");
                writer.print(ip1);

                //  Write out the edge reversed flag.
                String reversed = " 1 ";
                if (nonNull(reversedLst) && reversedLst.get(eidx))
                    reversed = " -1 ";
                writer.print(reversed);

                //  Write out the edge curve number
                int cidx = curves.indexOf(edge);
                if (cidx < 0)
                    throw new IOException(MessageFormat.format(
                            RESOURCES.getString("missingPatchEdge"), ip1, pidx + 1));
                writer.print(cidx + 1);

                //  Write out the side indicator.
                int side = ip1;
                if (nonNull(sideLst))
                    side = sideLst.get(eidx);
                if (side != prevSide || eidx == 0)
                    writer.print(" 1 ");
                else
                    writer.print(" -1 ");
                prevSide = side;
                writer.println("99");
            }
        }   //  Next pidx

    }   //  end writePatches()

}
