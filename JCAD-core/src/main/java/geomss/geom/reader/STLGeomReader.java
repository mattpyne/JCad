/**
 * STLGeomReader -- A class that can read and write a binary STL formatted geometry file.
 *
 * Copyright (C) 2015-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import jahuwaldt.io.FileUtils;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;

/**
 * A {@link GeomReader} for reading and writing geometry from/to a binary STL
 * (STereoLithography) formatted geometry file.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: September 4, 2015
 * @version September 9, 2016
 */
public class STLGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    //private static final boolean DEBUG = false;

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("stlDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "stl";

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file has the extension ".geo" or ".mk5".
     *         GeomReader.MAYBE if the file has the extension ".lib".
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        int response = NO;
        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".stl")) {
            response = MAYBE;
        }

        return response;
    }

    /**
     * Returns true. This class can write triangle and quad-paneled point geometry data to
     * a Binary STL formatted file.
     *
     * @return this method always returns true
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Reads in a binary STL formatted geometry file from the specified input file and
     * returns a {@link TriangleList} object that contains the triangle geometry from the
     * file.
     * <p>
     * WARNING: This file format is not unit aware. You must set the units
     * to be used by calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link TriangleList} object containing the geometry read in from the
     *         file. If the file has no geometry in it, then this list will have no
     *         triangles in it (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit)
     */
    @Override
    public TriangleList read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        //  Read all the bytes from the input file.
        ByteBuffer buf = FileUtils.file2ByteBuffer(inputFile);

        //  An STL file is always little-endian
        buf = buf.order(ByteOrder.LITTLE_ENDIAN);

        //  Read in the the triangle count.
        buf.position(80);
        int numTris = buf.getInt();

        //  Read in all the triangles and add them to the output list.
        Unit<Length> units = this.getFileUnits();
        Point[] verts = new Point[3];
        TriangleList triList = TriangleList.newInstance();
        for (int i = 0; i < numTris; ++i) {
            //  Skip the redundant normal vector
            buf.getFloat();
            buf.getFloat();
            buf.getFloat();

            //  Read in the vertex points.
            for (int j = 0; j < 3; ++j) {
                float x = buf.getFloat();
                float y = buf.getFloat();
                float z = buf.getFloat();
                Point pnt = Point.valueOf(units, x, y, z);
                verts[j] = pnt;
            }

            //  Skip the unused attribute byte count.
            buf.get();
            buf.get();

            //  Create and store the triangle.
            Triangle tri = Triangle.valueOf(verts);
            triList.add(tri);
        }

        return triList;
    }

    /**
     * Writes out an binary STL formatted geometry file for the geometry contained in the
     * supplied geometry list. If the list contains triangles or lists of triangles, they
     * are all written out to the file directly. If the list contains array based paneled
     * geometry such as a PointVehicle or PointComponent, then all the arrays in these
     * objects are converted into triangles and those triangles are written out to the
     * file. If no triangle or point array based geometry can be found in the geometry
     * list, then nothing will happen (the output file will not be created).
     * <p>
     * The 80 byte header of the STL file is not used and is filled with zeros.
     * </p>
     * <p>
     * WARNING: This format is not unit aware. The geometry will be written out in
     * whatever its current units are! Make sure to convert to the desired units for the
     * file before calling this method.
     * </p>
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The list of triangle or point array geometry to be written out.
     *                   May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        //  Convert the input geometry into a single list of triangles.
        TriangleList<? extends GeomTriangle> tris = TriangleList.newInstance();
        convertGeometry(geometry, tris);
        int numTris = tris.size();
        if (numTris == 0)
            return;

        //  Remove any degenerate triangles.
        tris = tris.removeDegenerate(null);
        numTris = tris.size();

        //  Convert all the triangles to the same units.
        Unit<Length> units = geometry.getUnit();

        //  Allocate a little-endian byte buffer for the data to be output (STL is always little-endian).
        int numBytes = 80 + 4 + numTris * (12 * 32 + 16) / 8;
        ByteBuffer buf = ByteBuffer.allocate(numBytes).order(ByteOrder.LITTLE_ENDIAN);

        //  Write in the 80 byte header (blank).
        for (int i = 0; i < 80; ++i)
            buf.put((byte)0);

        //  Write out the triangle count.
        buf.putInt(numTris);

        //  Loop over all the triangles in the list.
        GeomPoint[] verts = new GeomPoint[3];
        for (GeomTriangle tri : tris) {
            GeomVector<Dimensionless> n = tri.getNormal();
            tri.getPoints(verts);

            //  Write normal vector.
            buf.putFloat((float)n.getValue(Point.X));
            buf.putFloat((float)n.getValue(Point.Y));
            buf.putFloat((float)n.getValue(Point.Z));

            //  Write out vertices.
            for (int j = 0; j < 3; ++j) {
                GeomPoint p = verts[j];
                float x = (float)p.getValue(Point.X, units);
                float y = (float)p.getValue(Point.Y, units);
                float z = (float)p.getValue(Point.Z, units);
                buf.putFloat(x);
                buf.putFloat(y);
                buf.putFloat(z);
            }

            //  Write out the unused attribute byte count.
            buf.putShort((short)0);
        }

        //  Write the byte buffer out to the file.
        FileUtils.byteBuffer2File(buf, outputFile);

    }

    /**
     * Convert the input list of geometry into triangles by recursively processing lists
     * until either PointArray or TriangleList or GeomTriangle objects are encountered.
     *
     * @param geom   The list of geometry to be processed.
     * @param output The list that the triangles are all added to.
     */
    private static void convertGeometry(GeometryList<?, GeomElement> geom, TriangleList output) {
        //  Is the input list a list of triangles?
        if (geom instanceof TriangleList) {
            output.addAll((TriangleList)geom);
            return;
        }

        //  Is the input list a PointArray?
        if (geom instanceof PointArray) {
            PointArray arr = (PointArray)geom;
            output.addAll(arr.triangulate());
            return;
        }

        //  Loop over the items in the list.
        for (GeomElement elem : geom) {
            if (elem instanceof GeomTriangle)
                output.add((GeomTriangle)elem);
            else if (elem instanceof GeometryList) {
                //  Recurse down into the sub-list.
                convertGeometry((GeometryList)elem, output);
            }
        }
    }

    /**
     * This method always returns <code>false</code> as STL files do not encode the units
     * that are being used. You must call <code>setFileUnits</code> to set the units being
     * used before reading from a file of this format.
     *
     * @return This implementation always returns false.
     * @see #setFileUnits(javax.measure.unit.Unit)
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

}
