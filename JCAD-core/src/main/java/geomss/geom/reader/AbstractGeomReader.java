/*
 *   AbstractGeomReader -- A partial implementation of the GeomReader interface.
 *
 *   Copyright (C) 2010-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.GeomUtil;
import geomss.geom.GeometryList;
import geomss.geom.PointVehicle;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.MessageFormat;
import java.util.List;
import static java.util.Objects.isNull;
import java.util.ResourceBundle;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.util.FastTable;

/**
 * A partial implementation of the {@link GeomReader} interface. Some methods have default
 * implementations that sub-classes should override as needed.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: August 26, 2010
 * @version November 28, 2015
 */
public abstract class AbstractGeomReader implements GeomReader {

    /**
     * The resource bundle for this package.
     */
    protected static final ResourceBundle RESOURCES
            = ResourceBundle.getBundle("geomss.geom.reader.GeomReaderResources",
                    java.util.Locale.getDefault());

    /**
     * A list of any warning messages from the reader.
     */
    protected List<String> _warnings = FastTable.newInstance();

    // The units used for the geometry in the specified file.
    private Unit<Length> _units = GeomUtil.getDefaultUnit();

    /**
     * Read a line from the given reader and throw an exception of EOF is encountered.
     *
     * @param reader The LineNumberReader to read from.
     * @return The line read from the reader.
     * @throws IOException if EOF was encountered.
     */
    protected static final String readLine(LineNumberReader reader) throws IOException {
        String data = reader.readLine();
         if (isNull(data))
            throw new IOException(
                    MessageFormat.format(RESOURCES.getString("EOFMsg"), reader.getLineNumber()));
        return data;
    }
    
    /**
     * Compares this object with the specified object for order based on the
     * <code>toString().compareTo(o.toString())</code> method. Returns a negative integer,
     * zero, or a positive integer as this object is less than, equal to, or greater than
     * the specified object.
     */
    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file. This implementation always returns NO.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.YES if this class can definitely read the file, GeomReader.MAYBE
     *         if the file could possibly be read and GeomReader.NO if the file can
     *         definitely not be read by this class.
     * @throws java.io.IOException Could be thrown by sub-classes and indicates that the
     * file can not be read by this reader.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {
        return NO;
    }

    /**
     * Returns <code>true</code> if this class can write at least some data in the format
     * supported by this class. Returns <code>false</code> if it can not.
     * <p>
     * This default implementation always returns false.
     * </p>
     *
     * @return true if this class can write at least some data in the format supported by
     *         this class.
     */
    @Override
    public boolean canWriteData() {
        return false;
    }

    /**
     * Reads in a geometry file from the supplied input file and returns a
     * {@link GeometryList} object that contains the geometry from the file.
     * <p>
     * WARNING! This method must be implemented by sub-classes and  this implementation
     * will throw an exception if you try and use it.
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return A {@link GeometryList} object containing the geometry read in from the
     *         file.
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public GeometryList read(File inputFile) throws IOException {
        throw new IOException(
                MessageFormat.format(RESOURCES.getString("unsupportedReadMsg"), toString()));
    }

    /**
     * Return a list of any warning messages that the reader/writer may have issued.
     *
     * @return A list of any warning messages that the reader/writer may have issued.
     */
    @Override
    public List<String> getWarnings() {
        FastTable<String> output = FastTable.newInstance();
        output.addAll(_warnings);
        return output;
    }

    /**
     * Writes out a geometry file for the geometry contained in the supplied
     * {@link GeometryList} object.
     * <p>
     * WARNING! This method must be implemented by sub-classes and  this implementation
     * will throw an exception if you try and use it.
     * </p>
     *
     * @param outputFile The output File to which the geometry is to be written.
     * @param geometry   The {@link GeometryList} object containing the geometry to be
     *                   written out.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        throw new IOException(
                MessageFormat.format(RESOURCES.getString("unsupportedWriteMsg"), toString()));
    }

    /**
     * Sets the units used for the geometry as stored in a non-unit aware geometry file
     * being read in. The geometry is returned in these units for file types which are not
     * unit aware. If the file format specifies units, the input from this method will be
     * ignored. If this method is not called before reading a non-unit aware geometry
     * file, then the units of the returned geometry will be the system default units.
     * When writing out geometry, this is ignored.
     *
     * @param units The units used for the geometry in the file being read in. If null is
     *              passed, the units will default to the default system units.
     * @see #isUnitAware() 
     */
    @Override
    public void setFileUnits(Unit<Length> units) {
        if (units == null)
            units = GeomUtil.getDefaultUnit();
        _units = units;
    }

    /**
     * Returns the units used for the geometry when reading a file format that is
     * not unit aware. This is ignored for readers that are unit aware.
     *
     * @return The units used for the geometry when reading a file format that is
     *         not unit aware.
     */
    protected Unit<Length> getFileUnits() {
        return _units;
    }

    /**
     * Convert a generic GeometryList into a PointVehicle. If the input GeometryList is a
     * PointVehicle, it is simply returned. If the input GeometryList contains anything
     * other than PointComponent objects, an exception is thrown. If the input geomList is
     * <code>null</code>, then <code>null</code> is returned. Otherwise a new PointVehicle
     * is created containing the PointComponent objects in the input list. It's name is
     * also set to the name of the input list.
     *
     * @param geomList The GeometryList to convert into a PointVehicle list if possible.
     * @return The input GeometryList converted into a PointVehicle list.
     * @throws java.io.IOException If the input GeometryList could not be converted into a
     * PointVehicle list.
     */
    protected static PointVehicle geomList2PointVehicle(GeometryList geomList) throws IOException {
        if (geomList == null)
            return null;

        PointVehicle vehicle;
        if (geomList instanceof PointVehicle)
            vehicle = (PointVehicle)geomList;
        else {
            try {
                vehicle = PointVehicle.newInstance();
                vehicle.addAll(geomList);
                vehicle.setName(geomList.getName());
            } catch (Exception e) {
                throw new IOException(RESOURCES.getString("inputNotListOfComponents"));
            }
        }

        return vehicle;
    }
}
