/**
 * TRIGeomReader -- A class that can read and write an ASCII *.tri formatted geometry file.
 *
 * Copyright (C) 2015-2016, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.*;
import jahuwaldt.js.param.Parameter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.StackContext;
import javolution.text.TypeFormat;
import javolution.util.FastTable;

/**
 * A {@link GeomReader} for reading and writing geometry from/to an Cart3D ASCII
 * configuration TRI (TRIangulation) formatted geometry file.
 * <p>
 * Reference:
 * <a href="http://docs.desktop.aero/docs/cart3d/index.php/Surface_Geometry#Cart3D_Triangulation_Format">
 * http://docs.desktop.aero/docs/cart3d/index.php/Surface_Geometry</a>
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: September 4, 2015
 * @version September 9, 2016
 */
public class TRIGeomReader extends AbstractGeomReader {

    //  Debug output flag.
    //private static final boolean DEBUG = false;

    //  A brief description of the data read by this reaader.
    private static final String DESCRIPTION = RESOURCES.getString("triDescription");

    //  The preferred file extension for files of this reader's type.
    public static final String EXTENSION = "tri";
    
    //  Used to split strings on white space.
    private static final String WHITESPACE = "\\s+";

    /**
     * Returns a string representation of the object. This will return a brief description
     * of the format read by this reader.
     *
     * @return A brief description of the format read by this reader.
     */
    @Override
    public String toString() {
        return DESCRIPTION;
    }

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file has the extension ".geo" or ".mk5".
     *         GeomReader.MAYBE if the file has the extension ".lib".
     * @throws java.io.IOException If there is a problem reading from the specified file.
     */
    @Override
    public int canReadData(File inputFile) throws IOException {

        String name = inputFile.getName();
        name = name.toLowerCase().trim();
        if (name.endsWith(".tri")) {
            //  Try reading the number of vertices and triangles from the file.
            try (BufferedReader reader = Files.newBufferedReader(inputFile.toPath())) {
                
                String aLine = reader.readLine();
                if (isNull(aLine))
                    return NO;
                String[] tokens = aLine.trim().split(WHITESPACE);
                if (tokens.length > 2)
                    return NO;
                int numVerts = TypeFormat.parseInt(tokens[0]);
                int numTris = TypeFormat.parseInt(tokens[1]);
                if (numVerts < 3 || numTris < 1)
                    return NO;
            
                return YES;
                
            } catch (NumberFormatException e) {
                return NO;
            }
        }

        return NO;
    }

    /**
     * Returns true. This class can write triangle and quad-paneled point geometry data to
     * an ASCII *.tri formatted file.
     *
     * @return this method always returns true
     */
    @Override
    public boolean canWriteData() {
        return true;
    }

    /**
     * Reads in a Cart3D ASCII Configuration *.tri formatted geometry file from the
     * specified input file and returns a {@link GeomList} object that contains a set of
     * {@link TriangleList} objects (1 for each "configuration" in the *.tri file or a
     * single one if the *.tri file is a component file only).
     * <p>
     * WARNING: This file format is not unit aware. You must set the units
     * to be used by calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not be
     *                  null.
     * @return A {@link GeomList} object containing the geometry read in from the file. If
     *         the file has no geometry in it, then this list will have no triangles in it
     *         (will have a size() of zero).
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public GeomList<TriangleList> read(File inputFile) throws IOException {
        requireNonNull(inputFile);
        _warnings.clear();

        //  Create an initially empty geometry list for output.
        GeomList<TriangleList> output = GeomList.newInstance();

        //  Read in all the lines of the file.
        List<String> lines = Files.readAllLines(inputFile.toPath(), StandardCharsets.US_ASCII);

        Unit<Length> units = this.getFileUnits();
        int line = 0;
        try {
            //  Read in the number of verticies and triangles in the file.
            String[] tokens = lines.get(line++).trim().split(WHITESPACE);
            int numVerts = TypeFormat.parseInt(tokens[0]);
            int numTris = TypeFormat.parseInt(tokens[1]);

            //  Read in all the vertices.
            FastTable<Point> vertices = FastTable.newInstance();
            for (int i = 0; i < numVerts; ++i) {
                tokens = lines.get(line++).trim().split(WHITESPACE);
                double x = TypeFormat.parseDouble(tokens[0]);
                double y = TypeFormat.parseDouble(tokens[1]);
                double z = TypeFormat.parseDouble(tokens[2]);
                Point p = Point.valueOf(x, y, z, units);
                vertices.add(p);
            }

            //  Read in the array of indexes.
            int numIndexes = numTris * 3;
            int[] indexes = new int[numIndexes];
            int pos = 0;
            for (int i = 0; i < numTris; ++i) {
                tokens = lines.get(line++).trim().split(WHITESPACE);
                for (int j = 0; j < 3; ++j) {
                    int idx = TypeFormat.parseInt(tokens[j]) - 1;   //  Convert to 0 offset indexes.
                    indexes[pos++] = idx;
                }
            }

            //  Define the configuration ID list and initially set them all to 1.
            int[] configIDs = new int[numTris];
            Arrays.fill(configIDs, 1);

            //  Read in the configuration codes.
            //  Could be 1 per line or all on one line space delimited.
            int maxConfigID = 1;
            pos = 0;
            while (line < lines.size() && pos < numTris) {
                String aLine = lines.get(line++).trim();
                if (aLine.contains(" ") || aLine.contains("\t")) {
                    //  Multiple IDs on one line.
                    tokens = aLine.split(WHITESPACE);
                    for (int i = 0; i < tokens.length; ++i) {
                        int id = TypeFormat.parseInt(tokens[i]);
                        configIDs[pos++] = id;
                        if (id > maxConfigID)
                            maxConfigID = id;
                    }
                } else {
                    //  A single ID per line.
                    int id = TypeFormat.parseInt(aLine);
                    configIDs[pos++] = id;
                    if (id > maxConfigID)
                        maxConfigID = id;
                }
            }

            //  Break up the output into TriangleList objects based on the configuration IDs.
            if (maxConfigID == 1) {
                //  Only one configuration.
                TriangleList<Triangle> tris = TriangleList.valueOf(vertices, indexes);
                output.add(tris);

            } else {
                //  Multiple configurations in the file.

                //  Loop over each configuration ID.
                for (int cID = 1; cID <= maxConfigID; ++cID) {
                    //  Count how many triangles have this configuration ID.
                    int num = 0;
                    for (int i = numTris - 1; i >= 0; --i) {
                        if (configIDs[i] == cID)
                            ++num;
                    }

                    //  Create an array of indexes for this configuration.
                    int[] configIndexes = new int[num * 3];

                    //  Find all the triangles with this set of configuration indexes.
                    pos = 0;
                    for (int i = numTris - 1; i >= 0; --i) {
                        if (configIDs[i] == cID) {
                            int idx = i * 3;
                            configIndexes[pos++] = indexes[idx++];
                            configIndexes[pos++] = indexes[idx++];
                            configIndexes[pos++] = indexes[idx];
                        }
                    }

                    //  Create a list of triangles using this array of indexes.
                    TriangleList<Triangle> tris = TriangleList.valueOf(vertices, configIndexes);
                    tris.setName("Config #" + cID);
                    output.add(tris);
                }

            }

        } catch (NumberFormatException e) {
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("parseErrMsg"), DESCRIPTION, line + 1));
        }

        return output;
    }

    /**
     * Writes out a Cart3D ASCII Configuration *.tri formatted geometry file for the
     * geometry contained in the supplied geometry list. If the list contains only
     * triangles, they are written out as a "Component TRI file". If the list contains
     * lists of triangles, each are written out as separate configurations in a
     * "Configuration TRI file". If the list contains a PointVehicle, then each
     * PointComponent is triangulated and written out as a separate configuration in a
     * "Configuration TRI file". If no triangle or point array based geometry can be found
     * in the geometry list, then nothing will happen (the output file will not be
     * created).
     * <p>
     * The user data key "TRI_TOL" can optionally be set to a Parameter containing the
     * tolerance to use when determining if vertices are coincident. If not supplied, a
     * default tolerance will be used that essentially requires identical vertex
     * coordinates in order to be coincident.
     * </p>
     * <p>
     * WARNING: This format is not unit aware. The geometry will be written out in
     * whatever its current units are! Make sure to convert to the desired units for the
     * file before calling this method.
     * </p>
     *
     * @param outputFile The output file to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The list of triangle or point array geometry to be written out.
     *                   May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    @Override
    public void write(File outputFile, GeometryList geometry) throws IOException {
        requireNonNull(outputFile);
        _warnings.clear();
        if (!geometry.containsGeometry()) {
            _warnings.add(RESOURCES.getString("noGeometryWarning"));
            return;
        }

        StackContext.enter();
        try {
            //  Convert the input into a list of TriangleList objects.
            GeomList<TriangleList> configs = convertGeometry(geometry);
            int numConfigs = configs.size();
            if (numConfigs == 0) {
                _warnings.add(RESOURCES.getString("noGeometryWarning"));
                return;
            }

            //  Convert all the triangles to the same units.
            Unit<Length> units = geometry.getUnit();
            
            //  Create a single triangle list with all the triangles in it.
            TriangleList triLst = TriangleList.newInstance();
            for (int i = 0; i < numConfigs; ++i)
                triLst.addAll(configs.get(i));

            //  Convert the Triangle objects into a list of unique vertices and indexes.
            Parameter<Length> tol = (Parameter<Length>)geometry.getUserData("TRI_TOL");
            if (isNull(tol))
                tol = Parameter.valueOf(Parameter.SQRT_EPS, triLst.getUnit());
            TriangleVertData vd = triLst.toVertData(tol);
            int numVerts = vd.vertices.size();
            int numTris = vd.numTris;

            //  Get a writer to the file.
            Locale US = Locale.US;
            try (PrintWriter writer = new PrintWriter(outputFile, StandardCharsets.US_ASCII.name())) {

                //  Write out the number of vertices and triangles.
                writer.printf(US, "%12d%12d", numVerts, numTris);
                writer.println();

                //  Lookp over all the vertices and write them out.
                for (int i = 0; i < numVerts; ++i) {
                    GeomPoint p = vd.vertices.get(i);
                    double x = p.getValue(Point.X, units);
                    double y = p.getValue(Point.Y, units);
                    double z = p.getValue(Point.Z, units);
                    writer.printf(US, "%18.8E%18.8E%18.8E", x, y, z);
                    writer.println();
                }

                //  Loop over all the triangle vertex indexes and write them out 3 at a time.
                int numIndexes = numTris * 3;
                for (int i = 0; i < numIndexes;) {
                    int idx1 = vd.tris[i++] + 1;    //  Convert to unit offset indexes.
                    int idx2 = vd.tris[i++] + 1;
                    int idx3 = vd.tris[i++] + 1;
                    writer.printf(US, "%12d%12d%12d", idx1, idx2, idx3);
                    writer.println();
                }

                //  Write out the configuration ID tags.
                if (numConfigs > 1) {
                    for (int cID = 1; cID <= numConfigs; ++cID) {
                        int num = configs.get(cID - 1).size();
                        for (int i = 0; i < num; ++i) {
                            writer.printf(US, "%8d", cID);
                            writer.println();
                        }
                    }
                }
            }
        } finally {
            StackContext.exit();
        }

    }

    /**
     * Convert the input list of geometry into a list of TriangleList objects if possible.
     *
     * @param geom   The list of geometry to be processed.
     * @param output The list that the triangles are all added to.
     */
    private static GeomList<TriangleList> convertGeometry(GeometryList geometry) {
        //  Sort out what the top-level list is.
        
        GeomList<TriangleList> geom = GeomList.newInstance();
        if (geometry instanceof TriangleList) {
            geom.add((TriangleList)geometry);
            
        } else if (geometry instanceof PointVehicle) {
            //  A PointVehicle is output as a series of TriangleList objects.
            PointVehicle veh = (PointVehicle)geometry;
            
            //  Convert each PointComponent into a separate list of triangles.
            for (PointComponent comp : veh) {
                TriangleList tris = TriangleList.newInstance();
                
                //  Combine all the arrays in this component into one triangle list.
                for (PointArray arr : comp)
                    tris.addAll(arr.triangulate());
                
                //  Add the triangle list to the output list.
                geom.add(tris);
                
            }
            
        } else if (geometry instanceof PointComponent) {
            //  A PointComponent is output as a series of TriangleList objects.
            PointComponent comp = (PointComponent)geometry;
            
            //  Combine all the arrays in this component into one triangle list.
            TriangleList tris = TriangleList.newInstance();
            for (PointArray arr : comp)
                tris.addAll(arr.triangulate());
            
            //  Add the triangle list to the output list.
            geom.add(tris);
            
        } else if (geometry instanceof PointArray) {
            //  A PointComponent is output as a series of TriangleList objects.
            PointArray arr = (PointArray)geometry;
            TriangleList tris = arr.triangulate();
            geom.add(tris);
            
        } else if (geometry instanceof GeomList) {
            int size = geometry.size();
            if (geometry.get(0) instanceof TriangleList) {
                //  All the elements must be TriangleList.
                for (int i=0; i < size; ++i) {
                    if (!(geometry.get(i) instanceof TriangleList))
                        return geom;
                }
                geom.addAll(geometry);
                
            } else if (geometry.get(0) instanceof GeomTriangle) {
                //  All the elements must be triangles.
                for (int i=0; i < size; ++i) {
                    if (!(geometry.get(i) instanceof GeomTriangle))
                        return geom;
                }
                TriangleList tris = TriangleList.newInstance();
                tris.addAll(geometry);
                geom.add(tris);
                
            }
            
        }
        
        return geom;
    }

    /**
     * This method always returns <code>false</code> as TRI files do not encode the units
     * that are being used. You must call <code>setFileUnits</code> to set the units being
     * used before reading from a file of this format.
     *
     * @return This implementation always returns false.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    @Override
    public boolean isUnitAware() {
        return false;
    }

}
