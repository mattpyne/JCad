/*
 *   GeomReader  -- Interface for classes that read and write geometry files.
 *
 *   Copyright (C) 2000-2016, by Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.reader;

import geomss.geom.GeometryList;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;

/**
 * Interface for classes that read and write geometry files of various formats. Sub-classes
 * actually do the translation to and from the data file's format.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: April 14, 2000
 * @version September 9, 2016
 */
public interface GeomReader extends Comparable {

    /**
     * Constant indicating that a reader certainly <I>can not</I> read a specified file.
     */
    public static final int NO = 0;

    /**
     * Constant indicating that a reader certainly <I>can</I> read a specified file.
     */
    public static final int YES = 1;

    /**
     * Constant indicating that a reader <I>might</I> be able to read a specified file,
     * but can't determine for sure.
     */
    public static final int MAYBE = -1;

    /**
     * Reads in a geometry file from the supplied input file and returns a
     * {@link GeometryList} object that contains the geometry from the file.
     * <p>
     * WARNING: If the file being read in is not unit aware, then you must set the units
     * to be used by calling "setFileUnits()" before calling this method!
     * </p>
     *
     * @param inputFile The input file containing the geometry to be read in. May not 
     *                  be null.
     * @return A {@link GeometryList} object containing the geometry read in from the
     *         file.
     * @throws IOException If there is a problem reading the specified file.
     * @see #setFileUnits(javax.measure.unit.Unit)
     */
    public GeometryList read(File inputFile) throws IOException;

    /**
     * Writes out a geometry file for the geometry contained in the supplied
     * {@link GeometryList} object.
     * <p>
     * WARNING: If the format being written is not unit aware,
     * then the values are written out in whatever their current units are!
     * Make sure to convert to the desired units for the file before calling this method.
     * </p>
     *
     * @param outputFile The output File to which the geometry is to be written. May not
     *                   be null.
     * @param geometry   The {@link GeometryList} object containing the geometry to be
     *                   written out. May not be null.
     * @throws IOException If there is a problem writing to the specified file.
     */
    public void write(File outputFile, GeometryList geometry) throws IOException;

    /**
     * Method that determines if this reader can read geometry from the specified input
     * file.
     *
     * @param inputFile The input file containing the geometry to be read in.
     * @return GeomReader.NO if the file format is not recognized by this reader.
     *         GeomReader.YES if the file format is definitely recognized by this reader.
     *         GeomReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     * @throws java.io.IOException If there is a problem reading from the specified
     * file.
     */
    public int canReadData(File inputFile) throws IOException;

    /**
     * Returns true if this class can write at least some data in the format supported by
     * this class. Returns false if it can not.
     *
     * @return true if this class can write at least some data in the format supported by
     *         this class.
     */
    public boolean canWriteData();

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * GeomReader's type.
     *
     * @return The preferred file extension for files of this readers type.
     */
    public String getExtension();

    /**
     * Sets the units used for the geometry as stored in a non-unit aware geometry file
     * being read in. The geometry is returned in these units for file types which are not
     * unit aware. If the file format specifies units, the input from this method will be
     * ignored. If this method is not called before reading a non-unit aware geometry
     * file, then the units of the returned geometry will be the system default units.
     * When writing out geometry, this is ignored.
     *
     * @param units The units used for the geometry in the file being read in. If null is
     *              passed, the units will default to the default system units.
     * @see #isUnitAware()
     */
    public void setFileUnits(Unit<Length> units);

    /**
     * Returns <code>true</code> if this reader is unit aware (the format it reads from or
     * writes to stores the units being used) and returns <code>false</code> otherwise.
     *
     * @return true if this reader (and its format) is unit aware.
     * @see #setFileUnits(javax.measure.unit.Unit) 
     */
    public boolean isUnitAware();

    /**
     * Return a list of any warning messages that the reader/writer may have issued.
     *
     * @return A list of any warning messages that the reader/writer may have issued.
     */
    public List<String> getWarnings();

}
