/*
 *   Curve  -- Interface all curve type objects.
 *
 *   Copyright (C) 2009-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.util.List;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;

/**
 * Defines the interface for {@link GeomElement} objects that are continuous curves.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 15, 2009
 * @version August 31, 2015
 *
 * @param <T> The type of this curve.
 */
public interface Curve<T extends Curve> extends GeomElement<T>, ParametricGeometry<T>, Transformable<T> {

    /**
     * Returns the equivalent to this curve but stated in the specified unit.
     *
     * @param unit the length unit of the curve to be returned.
     * @return an equivalent to this curve but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public T to(Unit<Length> unit) throws ConversionException;

    /**
     * Return a copy of this curve converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the curve to return.
     * @return This curve converted to the new dimensions.
     */
    @Override
    public abstract T toDimension(int newDim);

    /**
     * Returns a copy of this curve instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this curve.
     */
    @Override
    public T copy();

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     */
    @Override
    public T copyToReal();

    /**
     * Return a new curve that is identical to this one, but with the parameterization
     * reversed.
     * 
     * @return A new curve that is identical to this one, but with the parameterization
     * reversed.
     */
    public T reverse();

    /**
     * Return a NURBS curve representation of this curve to within the specified
     * tolerance.
     *
     * @param tol The greatest possible difference between this curve and the NURBS
     *            representation returned.
     * @return A NURBS curve that represents this curve to within the specified tolerance.
     */
    public NurbsCurve toNurbs(Parameter<Length> tol);

    /**
     * Split this curve at the specified parametric position returning a list containing
     * two curves (a lower curve with smaller parametric positions than "s" and an upper
     * curve with larger parametric positions).
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!).
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    public GeomList<T> splitAt(double s);

    /**
     * Split this curve at the specified parametric position returning a list containing
     * two curves (a lower curve with smaller parametric positions than "s" and an upper
     * curve with larger parametric positions).
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!). Must be a 1-dimensional point with a value in the range 0 to 1.0,
     *          non-inclusive. Units are ignored.
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    public GeomList<T> splitAt(GeomPoint s);

    /**
     * Return a subrange point on the curve for the given parametric distance along the
     * curve.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return The subrange point or <code>null</code> if the parameter is not in a valid
     *         range.
     */
    public SubrangePoint getPoint(double s);

    /**
     * Return a subrange point on the curve for the given parametric distance along the
     * curve.
     *
     * @param s parametric distance to calculate a point for. Must be a 1-dimensional
     *          point with a value in the range 0 to 1.0. Units are ignored.
     * @return The subrange point
     */
    @Override
    public SubrangePoint getPoint(GeomPoint s);

    /**
     * Calculate a point on the curve for the given parametric distance along the curve.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return The calculated point
     */
    public Point getRealPoint(double s);

    /**
     * Calculate a point on the curve for the given parametric distance along the curve.
     *
     * @param s parametric distance to calculate a point for. Must be a 1-dimensional
     *          point with a value in the range 0 to 1.0. Units are ignored.
     * @return calculated point
     */
    @Override
    public Point getRealPoint(GeomPoint s);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric position(s) on a parametric object for the given parametric
     * position on the object, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     parametric position to calculate the derivatives for. Must match the
     *              parametric dimension of this parametric surface and have each value in
     *              the range 0 to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of lists of derivatives (one list for each parametric dimension).
     *         Each list contains derivatives up to the specified grade at the specified
     *         parametric position. Example: for a surface list element 0 is the array of
     *         derivatives in the 1st parametric dimension (s), then 2nd list element is
     *         the array of derivatives in the 2nd parametric dimension (t).
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public List<List<Vector<Length>>> getDerivatives(GeomPoint s, int grade);

    /**
     * Calculate a derivative with respect to parametric distance of the given grade on
     * the curve for the given parametric distance along the curve,
     * <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s)/ds</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s)/d^2s</code>; etc.
     * </p>
     *
     * Note: Cartesian space derivatives can be calculated as follows:
     * <pre>
     *      derivative = curve.getDerivative(s, 1);
     *      dy/dx = (dy/ds)/(dx/ds) = derivative.getValue(1)/derivative.getValue(0),
     *      dy/dz = (dy/ds)/(dz/ds) = derivative.getValue(1)/derivative.getValue(2),
     *      etc
     * </pre>
     *
     * @param s     Parametric distance to calculate a derivative for (0.0 to 1.0
     *              inclusive).
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *              derivative, etc)
     * @return The specified derivative of the curve at the specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    public Vector<Length> getSDerivative(double s, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the curve for the given parametric distance along
     * the curve, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     parametric distance to calculate the derivatives for. Must be a
     *              1-dimensional point with a value in the range 0 to 1.0. Units are
     *              ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is < 0.
     */
    public List<Vector<Length>> getSDerivatives(GeomPoint s, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the curve for the given parametric distance along
     * the curve, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     Parametric distance to calculate the derivatives for (0.0 to 1.0
     *              inclusive).
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is < 0.
     */
    public List<Vector<Length>> getSDerivatives(double s, int grade);

    /**
     * Return the tangent vector for the given parametric distance along the curve. This
     * vector contains the normalized 1st derivative of the curve with respect to
     * parametric distance in each component direction: dx/ds/|dp(s)/ds|,
     * dy/ds/|dp(s)/ds|, dz/ds/|dp(s)/ds|, etc.
     * <p>
     * Note: Cartesian space derivatives can be calculated as follows:
     * <pre>
     *      tangent = curve.getTangent(s);
     *      dy/dx = (dy/ds)/(dx/ds) = tangent.getValue(1)/tangent.getValue(0),
     *      dy/dz = (dy/ds)/(dz/ds) = tangent.getValue(1)/tangent.getValue(2),
     *      etc
     * </pre>
     * </p>
     *
     * @param s Parametric distance to calculate a tangent vector for (0.0 to 1.0
     *          inclusive).
     * @return The tangent vector of the curve at the specified parametric position.
     */
    public Vector<Dimensionless> getTangent(double s);

    /**
     * Return the tangent vector for the given parametric distance along the curve. This
     * vector contains the normalized 1st derivative of the curve with respect to
     * parametric distance in each component direction: dx/ds/|dp(s)/ds|,
     * dy/ds/|dp(s)/ds|, dz/ds/|dp(s)/ds|, etc.
     * <p>
     * Note: Cartesian space derivatives can be calculated as follows:
     * <pre>
     *      tangent = curve.getTangent(s);
     *      dy/dx = (dy/ds)/(dx/ds) = tangent.getValue(1)/tangent.getValue(0),
     *      dy/dz = (dy/ds)/(dz/ds) = tangent.getValue(1)/tangent.getValue(2),
     *      etc
     * </pre>
     * </p>
     *
     * @param s Parametric distance to calculate a tangent vector for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The tangent vector of the curve at the specified parametric position.
     */
    public Vector<Dimensionless> getTangent(GeomPoint s);

    /**
     * Return the principal normal vector for the given parametric distance,
     * <code>s</code>, along the curve. This vector is normal to the curve, lies in the
     * normal plane (is perpendicular to the tangent vector), and points toward the center
     * of curvature at the specified point.
     *
     * @param s Parametric distance to calculate the principal normal vector for (0.0 to
     *          1.0 inclusive).
     * @return The principal normal vector of the curve at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment -- this
     * means there are an infinite number of possible principal normal vectors).
     */
    public Vector<Dimensionless> getPrincipalNormal(double s);

    /**
     * Return the principal normal vector for the given parametric distance,
     * <code>s</code>, along the curve. This vector is normal to the curve, lies in the
     * normal plane (is perpendicular to the tangent vector), and points toward the center
     * of curvature at the specified point.
     *
     * @param s Parametric distance to calculate the principal normal vector for. Must be
     *          a 1-dimensional point with a value in the range 0 to 1.0. Units are
     *          ignored.
     * @return The principal normal vector of the curve at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment -- this
     * means there are an infinite number of possible principal normal vectors).
     */
    public Vector<Dimensionless> getPrincipalNormal(GeomPoint s);

    /**
     * Return the binormal vector for the given parametric distance along the curve. This
     * vector is normal to the curve at <code>s</code>, lies in the normal plane (is
     * perpendicular to the tangent vector), and is perpendicular to the principal normal
     * vector. Together, the tangent vector, principal normal vector, and binormal vector
     * form a useful orthogonal frame.
     *
     * @param s Parametric distance to calculate the binormal vector for (0.0 to 1.0
     *          inclusive).
     * @return The binormal vector of the curve at the specified parametric position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment).
     * @throws DimensionException if the curve does not have at least 3 physical
     * dimensions.
     */
    public Vector<Dimensionless> getBinormal(double s) throws DimensionException;

    /**
     * Return the binormal vector for the given parametric distance along the curve. This
     * vector is normal to the curve at <code>s</code>, lies in the normal plane (is
     * perpendicular to the tangent vector), and is perpendicular to the principal normal
     * vector. Together, the tangent vector, principal normal vector, and binormal vector
     * form a useful orthogonal frame.
     *
     * @param s Parametric distance to calculate the binormal vector for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The binormal vector of the curve at the specified parametric position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment).
     * @throws DimensionException if the curve does not have at least 3 physical
     * dimensions.
     */
    public Vector<Dimensionless> getBinormal(GeomPoint s) throws DimensionException;

    /**
     * Return the curvature (kappa = 1/rho; where rho = the radius of curvature) of the
     * curve at the parametric position <code>s</code>. The curvature vector (vector from
     * p(s) to the center of curvature) can be constructed from:  <code>k = rhoi*ni</code>
     * or <code>k = curve.getPrincipalNormal(s).divide(curve.getCurvature(s));</code>
     *
     * @param s Parametric distance to calculate the curvature for (0.0 to 1.0 inclusive).
     * @return The curvature of the curve at the specified parametric position in units of
     *         1/Length.
     */
    public Parameter getCurvature(double s);

    /**
     * Return the curvature (kappa = 1/rho; where rho = the radius of curvature) of the
     * curve at the parametric position <code>s</code>. The curvature vector (vector from
     * p(s) to the center of curvature) can be constructed from:  <code>k = rhoi*ni</code>
     * or <code>k = curve.getPrincipalNormal(s).divide(curve.getCurvature(s));</code>
     *
     * @param s Parametric distance to calculate the curvature for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The curvature of the curve at the specified parametric position in units of
     *         1/Length.
     */
    public Parameter getCurvature(GeomPoint s);

    /**
     * Return the variation of curvature or rate of change of curvature (VOC or
     * dKappa(s)/ds) at the parametric position <code>s</code>.
     *
     * @param s Parametric distance to calculate the variation of curvature for (0.0 to
     *          1.0 inclusive).
     * @return The variation of curvature of the curve at the specified parametric
     *         position in units of 1/Length.
     */
    public Parameter getVariationOfCurvature(double s);

    /**
     * Return the variation of curvature or rate of change of curvature (VOC or
     * dKappa(s)/ds) at the parametric position <code>s</code>.
     *
     * @param s Parametric distance to calculate the variation of curvature for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The variation of curvature of the curve at the specified parametric
     *         position in units of 1/Length.
     */
    public Parameter getVariationOfCurvature(GeomPoint s);

    /**
     * Return the torsion of the curve at the parametric position <code>s</code>. The
     * torsion is a measure of the rotation or twist about the tangent vector.
     *
     * @param s Parametric distance to calculate the torsion for (0.0 to 1.0 inclusive).
     * @return The torsion of the curve at the specified parametric position in units of
     *         1/Length.
     */
    public Parameter getTorsion(double s);

    /**
     * Return the torsion of the curve at the parametric position <code>s</code>. The
     * torsion is a measure of the rotation or twist about the tangent vector.
     *
     * @param s Parametric distance to calculate the torsion for. Must be a 1-dimensional
     *          point with a value in the range 0 to 1.0. Units are ignored.
     * @return The torsion of the curve at the specified parametric position in units of
     *         1/Length.
     */
    public Parameter getTorsion(GeomPoint s);

    /**
     * Return the total arc length of this curve.
     *
     * @param eps The desired fractional accuracy on the arc-length.
     * @return The total arc length of the curve.
     */
    public Parameter<Length> getArcLength(double eps);

    /**
     * Return the arc length of a segment of this curve.
     *
     * @param s1  The starting parametric distance along the curve to begin the arc-length
     *            calculation from.
     * @param s2  The ending parametric distance along the curve to end the arc-length
     *            calculation at.
     * @param eps The desired fractional accuracy on the arc-length.
     * @return The arc length of the specified segment of the curve.
     */
    public Parameter<Length> getArcLength(double s1, double s2, double eps);

    /**
     * Return the arc length of a segment of this curve.
     *
     * @param s1  The starting parametric distance along the curve to begin the arc-length
     *            calculation from. Must be a 1-dimensional point with a value in the
     *            range 0 to 1.0. Units are ignored.
     * @param s2  The ending parametric distance along the curve to end the arc-length
     *            calculation at. Must be a 1-dimensional point with a value in the range
     *            0 to 1.0. Units are ignored.
     * @param eps The desired fractional accuracy on the arc-length.
     * @return The arc length of the specified segment of the curve.
     */
    public Parameter<Length> getArcLength(GeomPoint s1, GeomPoint s2, double eps);

    /**
     * Return a subrange point at the position on the curve with the specified arc-length.
     *
     * @param arcLength The target arc length to find in meters.
     * @param tol       Fractional tolerance (in parameter space) to refine the point
     *                  position to.
     * @return A subrange point on the curve at the specified arc-length position.
     */
    public SubrangePoint getPointAtArcLength(Parameter<Length> arcLength, double tol);

    /**
     * A point is found along this curve that when connected by a straight line to the
     * given point in space, the resulting line is tangent to this curve. This method
     * should only be used if this curve is a planar curve that is C1 (slope) continuous,
     * and convex with respect to the point. The given point should be in the plane of the
     * curve. If it is not, it will be projected into the plane of the curve.
     *
     * @param point The point that the tangent on the curve is to be found relative to.
     * @param near  The parametric distance on the curve (0-1) that serves as an initial
     *              guess at the location of the tangent point.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The point on this curve that is tangent to the curve and the supplied
     *         point.
     */
    public SubrangePoint getTangencyPoint(GeomPoint point, double near, double tol);

    /**
     * Return a string of points that are gridded onto the curve using the specified
     * spacing and gridding rule.
     *
     * @param gridRule Specifies whether the subdivision spacing is applied with respect
     *                 to arc-length in real space or parameter space.
     * @param spacing  A list of values used to define the subdivision spacing.
     *                 <code>gridRule</code> specifies whether the subdivision spacing is
     *                 applied with respect to arc-length in real space or parameter
     *                 space. If the spacing is arc-length, then the values are
     *                 interpreted as fractions of the arc length of the curve from 0 to
     *                 1.
     * @return A string of subrange points gridded onto the curve at the specified
     *         parametric positions.
     */
    public PointString<SubrangePoint> extractGrid(GridRule gridRule, List<Double> spacing);

    /**
     * Return a string of points that are gridded onto the curve with the number of points
     * and spacing chosen to result in straight line segments between the points that are
     * within the specified tolerance of this curve.
     *
     * @param tol The maximum distance that a straight line between gridded points may
     *            deviate from this curve.
     * @return A string of subrange points gridded onto the curve.
     */
    public PointString<SubrangePoint> gridToTolerance(Parameter<Length> tol);

    /**
     * Return the intersection between a plane and this curve.
     *
     * @param plane The plane to intersect with this curve.
     * @param tol   Fractional tolerance (in parameter space) to refine the point
     *              positions to.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this curve with the specified plane. If no intersection is
     *         found, an empty PointString is returned.
     */
    public PointString<SubrangePoint> intersect(GeomPlane plane, double tol);

    /**
     * Return the intersection between an infinite line and this curve.
     *
     * @param L0   A point on the line (origin of the line).
     * @param Ldir The direction vector for the line (does not have to be a unit vector).
     * @param tol  Tolerance (in physical space) to refine the point positions to.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this curve with the specified line. If no intersection is
     *         found, an empty PointString is returned.
     */
    public PointString<SubrangePoint> intersect(GeomPoint L0, GeomVector Ldir, Parameter<Length> tol);

    /**
     * Return the intersection between a line segment and this curve.
     *
     * @param line The line segment to intersect.
     * @param tol  Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this curve and the input line segment respectively,
     *         made by the intersection of this curve with the specified line segment. If
     *         no intersections are found a list of two empty PointStrings are returned.
     */
    public GeomList<PointString<SubrangePoint>> intersect(LineSegment line, Parameter<Length> tol);

    /**
     * Return the intersection between another curve and this curve.
     *
     * @param curve The curve to intersect with this curve.
     * @param tol   Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this curve and the input curve respectively, made by
     *         the intersection of this curve with the specified curve. If no
     *         intersections are found a list of two empty PointStrings are returned.
     */
    public GeomList<PointString<SubrangePoint>> intersect(Curve curve, Parameter<Length> tol);

    /**
     * Return the intersections between a surface and this curve.
     *
     * @param surface The surface to intersect with this curve.
     * @param tol     Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this curve and the input surface respectively, made by
     *         the intersection of this curve with the specified surface. If no
     *         intersections are found a list of two empty PointStrings are returned.
     */
    public GeomList<PointString<SubrangePoint>> intersect(Surface surface, Parameter<Length> tol);

    /**
     * Returns the closest point on this curve to the specified point.
     *
     * @param point The point to find the closest point on this curve to.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The {@link SubrangePoint} on this curve that is closest to the specified
     *         point.
     */
    @Override
    public SubrangePoint getClosest(GeomPoint point, double tol);

    /**
     * Returns the closest point on this curve to the specified point near the specified
     * parametric position. This may not return the global closest point!
     *
     * @param point The point to find the closest point on this curve to.
     * @param near  The parametric position where the search for the closest point should
     *              be started.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The point on this curve that is closest to the specified point near the
     *         specified parametric position.
     */
    public SubrangePoint getClosest(GeomPoint point, double near, double tol);

    /**
     * Returns the closest points on this curve to the specified list of points.
     *
     * @param points The list of points to find the closest points on this curve to.
     * @param tol    Fractional tolerance (in parameter space) to refine the point
     *               positions to.
     * @return The list of {@link SubrangePoint} objects on this curve that are closest to
     *         the specified points.
     */
    public PointString<SubrangePoint> getClosest(List<? extends GeomPoint> points, double tol);

    /**
     * Returns the farthest point on this curve from the specified point.
     *
     * @param point The point to find the farthest point on this curve from.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The {@link SubrangePoint} on this curve that is farthest from the specified
     *         point.
     */
    @Override
    public SubrangePoint getFarthest(GeomPoint point, double tol);

    /**
     * Returns the farthest point on this curve from the specified point near the
     * specified parametric position. This may not return the global farthest point!
     *
     * @param point The point to find the farthest point on this curve from.
     * @param near  The parametric position where the search for the farthest point should
     *              be started.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The {@link SubrangePoint} on this curve that is farthest from the specified
     *         point.
     */
    public SubrangePoint getFarthest(GeomPoint point, double near, double tol);

    /**
     * Returns the farthest points on this curve to the specified list of points.
     *
     * @param points The list of points to find the farthest points on this curve to.
     * @param tol    Fractional tolerance (in parameter space) to refine the point
     *               positions to.
     * @return The list of {@link SubrangePoint} objects on this curve that are farthest
     *         from the specified points.
     */
    public PointString<SubrangePoint> getFarthest(List<? extends GeomPoint> points, double tol);

    /**
     * Returns the closest points (giving the minimum distance) between this curve and the
     * specified curve. If the specified curve intersects this curve, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If the specified curve is
     * parallel to this curve (all points are equidistant away), then any point between
     * the two curves could be returned as the "closest".
     *
     * @param curve The curve to find the closest points between this curve and that one.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return A list containing two SubrangePoint objects that represent the closest
     *         point on this curve (index 0) and the closest point on the specified curve
     *         (index 1).
     */
    public PointString<SubrangePoint> getClosest(Curve curve, double tol);

    /**
     * Returns the closest points (giving the minimum distance) between this curve and the
     * specified surface. If this curve intersects the specified surface, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If this curve is parallel to
     * the specified surface (all closest points are equidistant away), then any point on
     * this curve and the closest point on the target surface will be returned as
     * "closest".
     *
     * @param surface The surface to find the closest points between this curve and that
     *                surface.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                position to.
     * @return A list containing two SubrangePoint objects that represent the closest
     *         point on this curve (index 0) and the closest point on the specified
     *         surface (index 1).
     */
    public PointString<SubrangePoint> getClosest(Surface surface, double tol);

    /**
     * Returns the closest points (giving the minimum distance) between this curve and the
     * specified plane. If this curve intersects the specified plane, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If this curve is parallel to
     * the specified plane (all closest points are equidistant away), then any point on
     * this curve and the closest point on the target plane will be returned as "closest".
     *
     * @param plane The plane to find the closest points between this curve and that
     *              plane.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The closest point found on this curve to the specified plane.
     */
    public SubrangePoint getClosest(GeomPlane plane, double tol);

    /**
     * Return the signed area of the region enclosed or subtended by a planar curve
     * relative to the specified reference point. This method assumes that the curve is
     * planar and will give incorrect results if the curve is not planar. Also, the input
     * reference point is assumed to lie in the plane of the curve, it if is not, this
     * will give incorrect results. If the curve is closed, then the reference point is
     * arbitrary (as long as it is in the plane of the curve). If the curve is open, then
     * the area returned is that subtended by the curve with straight lines from each end
     * of the curve to the input point.
     *
     * @param refPnt The reference point used for computing the enclosed or subtended area
     *               of the curve.
     * @param eps    The desired fractional accuracy of the area calculated.
     * @return The area enclosed or subtended by this curve (assuming that this curve is
     *         planar and that the reference point lies in the plane of this curve).
     */
    public Parameter<Area> getEnclosedArea(GeomPoint refPnt, double eps);

    /**
     * Return <code>true</code> if this curve is degenerate (i.e.: has length less than
     * the specified tolerance).
     *
     * @param tol The tolerance for determining if this curve is degenerate. May not be
     *            null.
     * @return true if this curve is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol);

    /**
     * Return <code>true</code> if this curve is planar or <code>false</code> if it is
     * not.
     *
     * @param tol The geometric tolerance to use in determining if the curve is planar.
     * @return true if this curve is planar.
     */
    public boolean isPlanar(Parameter<Length> tol);

    /**
     * Returns <code>true</code> if this curve is a line to within the specified
     * tolerance.
     *
     * @param tol The tolerance for determining if this curve is a line.
     * @return true if this curve is a line.
     */
    public boolean isLine(Parameter<Length> tol);

    /**
     * Returns <code>true</code> if this curve is a circular arc to within the specified
     * tolerance.
     *
     * @param tol The tolerance for determining if this curve is a circular arc.
     * @return true if this curve is a circular arc.
     */
    public boolean isCircular(Parameter<Length> tol);

}
