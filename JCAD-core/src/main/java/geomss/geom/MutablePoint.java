/**
 * MutablePoint -- Holds the changeable floating point coordinates of a point in nD space.
 *
 * Copyright (C) 2013-2017, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Coordinate3D;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A mutable container that holds changeable coordinates of a point in n-dimensional
 * space.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: December 11, 1999
 * @version October 14, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class MutablePoint extends GeomPoint {

    //  Use an immutable Point as a backing for the MutablePoint.
    private Point _point;

    /**
     * Returns a {@link MutablePoint} instance of the specified dimension with zero meters
     * for each coordinate value.
     *
     * @param dim the physical dimension of the point to create.
     * @return the point having the specified dimension and zero meters for values.
     */
    public static MutablePoint newInstance(int dim) {
        MutablePoint V = FACTORY.object();
        V._point = Point.newInstance(dim);
        return V;
    }

    /**
     * Returns a {@link Point} instance of the specified dimension and units with zero for
     * each coordinate value.
     *
     * @param dim  the physical dimension of the point to create.
     * @param unit The unit for the point to create. May not be null.
     * @return the point having the specified dimension & units and zero for values.
     */
    public static MutablePoint newInstance(int dim, Unit<Length> unit) {
        MutablePoint V = FACTORY.object();
        V._point = Point.newInstance(dim, requireNonNull(unit));
        return V;
    }

    /**
     * Returns a {@link MutablePoint} instance holding the specified <code>double</code>
     * value or values stated in meters.
     *
     * @param x the coordinate values stated in meters. May not be null.
     * @return the point having the specified value.
     */
    public static MutablePoint valueOf(double... x) {
        return valueOf(SI.METER, x);
    }

    /**
     * Returns a 1D {@link MutablePoint} instance holding the specified
     * <code>double</code> values stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified value.
     */
    public static MutablePoint valueOf(double x, Unit<Length> unit) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(x, requireNonNull(unit));
        return V;
    }

    /**
     * Returns a 2D {@link MutablePoint} instance holding the specified
     * <code>double</code> value stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static MutablePoint valueOf(double x, double y, Unit<Length> unit) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(x, y, requireNonNull(unit));
        return V;
    }

    /**
     * Returns a 3D {@link MutablePoint} instance holding the specified
     * <code>double</code> values stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param z    the z value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static MutablePoint valueOf(double x, double y, double z, Unit<Length> unit) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(x, y, z, requireNonNull(unit));
        return V;
    }

    /**
     * Returns a {@link MutablePoint} instance holding the specified <code>double</code>
     * values stated in the specified units.
     *
     * @param unit   the length unit in which the coordinates are stated. May not be null.
     * @param values the list of values stated in the specified unit. May not be null.
     * @return the point having the specified values.
     */
    public static MutablePoint valueOf(Unit<Length> unit, double... values) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(requireNonNull(unit), requireNonNull(values));
        return V;
    }

    /**
     * Returns a {@link MutablePoint} instance holding the specified
     * <code>Parameter</code> values. All the values are converted to the same units as
     * the 1st value.
     *
     * @param values The list of values to be stored. May not be null.
     * @return the point having the specified values in the units of the 1st value.
     */
    public static MutablePoint valueOf(Parameter<Length>... values) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(requireNonNull(values));
        return V;
    }

    /**
     * Returns a {@link MutablePoint} instance holding the specified
     * <code>Parameter</code> values. All the values are converted to the same units as
     * the 1st value.
     *
     * @param values The list of values to be stored. May not be null.
     * @return the point having the specified values in the units of the 1st value.
     */
    public static MutablePoint valueOf(List<Parameter<Length>> values) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(requireNonNull(values));
        return V;
    }

    /**
     * Returns a {@link MutablePoint} instance holding the specified
     * <code>@link jahuwaldt.js.param.Coordinate3D Coordinate3D</code> values.
     *
     * @param coord The {@link jahuwaldt.js.param.Coordinate3D Coordinate3D} to be stored.
     *              May not be null.
     * @return the point having the specified values.
     */
    public static MutablePoint valueOf(Coordinate3D<Length> coord) {
        MutablePoint P = FACTORY.object();
        P._point = Point.valueOf(requireNonNull(coord));
        return P;
    }

    /**
     * Returns a {@link MutablePoint} instance containing the specified vector of Float64
     * values stated in the specified units.
     *
     * @param vector the vector of Float64 values stated in the specified unit. May not be null.
     * @param unit   the unit in which the values are stated. May not be null.
     * @return the point having the specified values.
     */
    public static MutablePoint valueOf(org.jscience.mathematics.vector.Vector<Float64> vector, Unit<Length> unit) {
        MutablePoint V = FACTORY.object();
        V._point = Point.valueOf(requireNonNull(vector), requireNonNull(unit));
        return V;
    }

    /**
     * Returns a {@link MutablePoint} instance containing the specified vector of
     * Parameter values with length units. All the values are converted to the same units
     * as the 1st value.
     *
     * @param <Q>    The Quantity (unit type) of this point.
     * @param vector the vector of Parameter values stated in length or dimensionless
     *               units. If in dimensionless units, the values are interpreted as being
     *               in meters. May not be null.
     * @return the point having the specified values.
     * @throws ConversionException if the input vector is not in length (or Dimensionless)
     * units.
     */
    public static <Q extends Quantity> MutablePoint valueOf(org.jscience.mathematics.vector.Vector<Parameter<Q>> vector) {
        MutablePoint P = FACTORY.object();
        P._point = Point.valueOf(requireNonNull(vector));
        return P;
    }

    /**
     * Returns a {@link MutablePoint} instance that represents the direction elements of
     * the specified GeomVector given in length or dimensionless units. If the user wishes
     * for the point to represent the end or tip of the vehicle they should use the
     * following:
     * <code>MutablePoint p = MutablePoint.valueOf(vector).plus(vector.getOrigin());</code>
     *
     * @param vector the GeomVector stated in length or dimensionless units. If in
     *               dimensionless units, the values are interpreted as being in meters.
     *               May not be null.
     * @return the point having the specified values of the vector end point or tip.
     * @throws ConversionException if the input vector is not in length (or Dimensionless)
     * units.
     */
    public static MutablePoint valueOf(GeomVector<?> vector) {
        MutablePoint P = FACTORY.object();
        P._point = Point.valueOf(requireNonNull(vector));
        return P;
    }

    /**
     * Returns a {@link MutablePoint} instance containing the specified GeomPoint's data.
     *
     * @param point the GeomPoint to be copied into a new Point. May not be null.
     * @return the point having the specified values.
     */
    public static MutablePoint valueOf(GeomPoint point) {
        MutablePoint P = FACTORY.object();
        P._point = point.immutable();
        return P;
    }

    /**
     * Recycles a <code>MutablePoint</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(MutablePoint instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _point.getPhyDimension();
    }

    /**
     * Set the value of elements of this point to the elements of the specified point. If
     * the input GeomPoint is of type "Point", then the data stored in this MutablePoint
     * is set to the data stored in "p" without creating any new object instances (copy by
     * value).
     *
     * @param p The new point to make this point equal to. May not be null.
     * @throws DimensionException <code>(p.getPhyDimension() != getPhyDimension())</code>
     */
    public void set(GeomPoint p) {
        if (p.getPhyDimension() != getPhyDimension())
            throw new DimensionException(RESOURCES.getString("consistantPointDim"));
        _point = p.immutable();
        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Set the value of elements of this point to the elements of the specified vector of
     * Parameter objects. The origin of the vector is ignored.
     *
     * @param vector The new vector to make this point equal to in length or dimensionless
     *               units. If in dimensionless units, the values are interpreted as being
     *               in the same units as this point. May not be null.
     * @throws DimensionException <code>(vector.getDimension() != getPhyDimension())</code>
     * @throws ConversionException if the input vector is not in length (or Dimensionless)
     * units.
     */
    public void set(GeomVector<?> vector) {
        if (vector.getPhyDimension() != getPhyDimension())
            throw new DimensionException(RESOURCES.getString("consistantPointDim"));
        _point = Point.valueOf(vector);
    }

    /**
     * Set the value of a point dimension to the specified Parameter.
     *
     * @param i     the dimension index.
     * @param value The new value of the parameter to set at <code>i</code>. May not be
     *              null.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public void set(int i, Parameter<Length> value) {
        requireNonNull(value);
        
        //  Get a list of values in this point.
        FastTable<Parameter<Length>> values = FastTable.newInstance();
        int size = _point.getPhyDimension();
        for (int j = 0; j < size; ++j) {
            values.add(_point.get(j));
        }

        //  Change the dimension indicated.
        values.set(i, value);
        _point = Point.valueOf(values);
        FastTable.recycle(values);

        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Set the value of a point dimension to the specified double in the current point
     * units.
     *
     * @param i     the dimension index.
     * @param value The new value of the parameter to set at <code>i</code> in the current
     *              units of this point.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public void setValue(int i, double value) {
        //  Create a Parameter object.
        Parameter<Length> param = Parameter.valueOf(value, getUnit());

        //  Set the specified dimension to the new parameter object.
        set(i, param);
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i) {
        return _point.getValue(i);
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * the specified unit.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Length> unit) {
        return _point.getValue(i, unit);
    }

    /**
     * Returns the square of the Euclidean norm, magnitude, or length value of the vector
     * from the origin to this point (the dot product of the origin-to-this-point vector
     * and itself). This is slightly faster than calling <code>normValue</code> if the
     * squared value is all that is needed.
     *
     * @return <code>this.normSq().getValue()</code>.
     * @see #normValue() 
     */
    @Override
    public double normSqValue() {
        return _point.normSqValue();
    }

    /**
     * Returns the sum of this point with the one specified. The unit of the output point
     * will be the units of this point.
     *
     * @param that the point to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point plus(GeomPoint that) {
        return _point.plus(that);
    }

    /**
     * Adds the specified parameter to each component of this point. The unit of the
     * output point will be the units of this point.
     *
     * @param that the parameter to be added to each component of this point. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    @Override
    public Point plus(Parameter<Length> that) {
        return _point.plus(that);
    }

    /**
     * Returns the difference between this point and the one specified. The unit of the
     * output point will be the units of this point.
     *
     * @param that the point to be subtracted from this point. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point minus(GeomPoint that) {
        return _point.minus(that);
    }

    /**
     * Subtracts the specified parameter from each component of this point. The unit of
     * the output point will be the units of this point.
     *
     * @param that the parameter to be subtracted from each component of this point. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Point minus(Parameter<Length> that) {
        return _point.minus(that);
    }

    /**
     * Returns the negation of this point (all the values of each dimension negated).
     *
     * @return <code>-this</code>
     */
    @Override
    public Point opposite() {
        return _point.opposite();
    }

    /**
     * Returns the product of this point with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Point times(double k) {
        return _point.times(k);
    }

    /**
     * Returns a copy of this MutablePoint instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public MutablePoint copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public Point copyToReal() {
        return Point.valueOf(_point);
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this point are stated in.
     *
     * @return The unit in which the {@link #getValue values} in this point are stated in.
     */
    @Override
    public final Unit<Length> getUnit() {
        return _point.getUnit();
    }

    /**
     * Return an immutable version of this point.
     *
     * @return An immutable version of this point.
     */
    @Override
    public Point immutable() {
        return _point;
    }

    /**
     * Returns the equivalent to this point but stated in the specified unit.
     *
     * @param unit the length unit of the point to be returned. May not be null.
     * @return an equivalent of this point but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public MutablePoint to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        MutablePoint P = FACTORY.object();
        P._point = _point.to(unit);
        return P;
    }

    /**
     * Return the equivalent of this point converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the point to return.
     * @return A copy of this point converted to the new dimensions.
     */
    @Override
    public MutablePoint toDimension(int newDim) {
        if (newDim < 1)
            throw new IllegalArgumentException(RESOURCES.getString("pointDimGT0"));
        int thisDim = this.getPhyDimension();
        if (newDim == thisDim)
            return this;
        MutablePoint P = FACTORY.object();
        P._point = _point.toDimension(newDim);
        return P;
    }

    /**
     * Returns the values stored in this point, stated in this point's
     * {@link #getUnit unit}, as a Float64Vector.
     *
     * @return A Float64Vector containing the coordinate values for this point.
     */
    @Override
    public final Float64Vector toFloat64Vector() {
        return _point.toFloat64Vector();
    }

    /**
     * Compares this Point against the specified object for strict equality (same values
     * and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        MutablePoint that = (MutablePoint)obj;
        return this._point.equals(that._point)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_point);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<MutablePoint> XML = new XMLFormat<MutablePoint>(MutablePoint.class) {

        @Override
        public MutablePoint newInstance(Class<MutablePoint> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, MutablePoint obj) throws XMLStreamException {
            Unit unit = Unit.valueOf(xml.getAttribute("unit"));
            if (!Length.UNIT.isCompatible(unit))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("incompatibleUnits"),
                                "MutablePoint", "length"));

            GeomPoint.XML.read(xml, obj);     // Call parent read.

            FastTable<Float64> valueList = FastTable.newInstance();
            while (xml.hasNext()) {
                Float64 value = xml.getNext();
                valueList.add(value);
            }
            obj._point = Point.valueOf(Float64Vector.valueOf(valueList), unit);

        }

        @Override
        public void write(MutablePoint obj, OutputElement xml) throws XMLStreamException {
            xml.setAttribute("unit", obj.getUnit().toString());

            GeomPoint.XML.write(obj, xml);    // Call parent write.

            int size = obj._point.getPhyDimension();
            for (int i = 0; i < size; ++i)
                xml.add(Float64.valueOf(obj._point.getValue(i)));

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private MutablePoint() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<MutablePoint> FACTORY = new ObjectFactory<MutablePoint>() {
        @Override
        protected MutablePoint create() {
            return new MutablePoint();
        }

        @Override
        protected void cleanup(MutablePoint obj) {
            obj.reset();
        }
    };

    @SuppressWarnings("unchecked")
    private static MutablePoint copyOf(MutablePoint original) {
        MutablePoint P = FACTORY.object();
        P._point = original._point.copy();
        original.copyState(P);
        return P;
    }

}
