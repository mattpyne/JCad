/*
 *   DimensionException  -- Exception thrown when dimensions do not match expectations.
 *
 *   Copyright (C) 2009-2015, by Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

/**
 * Exception thrown when geometry element dimensions do not match expectations.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: April 28, 2009
 * @version November 25, 2015
 */
@SuppressWarnings("serial")
public class DimensionException extends RuntimeException {

    /**
     * Constructs a dimension exception with no detail message.
     */
    public DimensionException() {
    }

    /**
     * Constructs a dimension exception with the specified message.
     *
     * @param message the error message.
     */
    public DimensionException(String message) {
        super(message);
    }

}
