/*
 *   GeomElement  -- Interface in common to all geometry elements.
 *
 *   Copyright (C) 2002-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.util.Map;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.text.Text;
import javolution.xml.XMLSerializable;

/**
 * Defines the interface in common to all geometry elements.
 *
 * <p> Modified by: Joseph A. Huwaldt</p>
 *
 * @author Joseph A. Huwaldt, Date: March 31, 2000
 * @version August 31, 2015
 *
 * @param <T> The type of this geometry element.
 */
public interface GeomElement<T extends GeomElement> extends Cloneable, Comparable, XMLSerializable {

    /**
     * Return the unique ID number of this geometry element.
     *
     * @return The unique ID of this geometry element.
     */
    public long getID();

    /**
     * Return the name of this geometry element. If the name is not defined, then
     * <code>null</code> is returned.
     *
     * @return The name of this geometry element.
     */
    public String getName();

    /**
     * Change the name of this geometry element to the specified name (may be
     * <code>null</code> to clear the name and make it undefined).
     *
     * @param name The name of this geometry element.
     */
    public void setName(String name);

    /**
     * Returns the number of child-elements that make up this geometry element.
     *
     * @return The number of child-elements that make up this geometry element.
     */
    public int size();

    /**
     * Returns the text representation of this geometry element.
     *
     * @return The text representation of this geometry element.
     */
    public Text toText();

    /**
     * Returns the number of physical dimensions of this geometry element.
     *
     * @return The number of physical dimensions of this geometry element.
     */
    public int getPhyDimension();

    /**
     * Return a copy of this element converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the element to return.
     * @return A copy of this geometry element converted to the new dimensions.
     */
    public T toDimension(int newDim);

    /**
     * Returns the number of parametric dimensions of this geometry element.
     *
     * @return The number of parametric dimensions of this geometry element.
     */
    public int getParDimension();

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    public Point getBoundsMin();

    /**
     * Return the coordinate point representing the maximum bounding box corner of this
     * geometry element (e.g.: max X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    public Point getBoundsMax();

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this geometry element. This is more accurate than
     * <code>getBoundsMax</code> & <code>getBoundsMin</code>, but also typically takes
     * longer to compute.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    public GeomPoint getLimitPoint(int dim, boolean max, double tol);

    /**
     * Return <code>true</code> if this GeomElement contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the
     * evaluations of this element are NaN or Inf.
     *
     * @return true if this geometry element contains valid and finite data.
     */
    public boolean isValid();

    /**
     * Returns the unit in which the geometry in this element are stated.
     *
     * @return The unit in which the geometry in this element are stated.
     */
    public Unit<Length> getUnit();

    /**
     * Returns the equivalent to this element but stated in the specified unit.
     *
     * @param unit the length unit of the element to be returned.
     * @return an equivalent to this element but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    public T to(Unit<Length> unit) throws ConversionException;

    /**
     * Returns a copy of this GeomElement instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    public T copy();

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations removed (applied).
     */
    public T copyToReal();

    /**
     * Return any user defined object associated with this geometry element and the
     * specified key. If there is no user data with the specified key, then
     * <code>null</code> is returned.
     *
     * @param key the key whose associated value is to be returned or <code>null</code> if
     *            the specified key could not be found.
     * @return Any user defined data associated with this geometry and the specified key.
     * @throws NullPointerException if <code>key</code> is <code>null</code>.
     */
    public Object getUserData(Object key);

    /**
     * Returns a new Map containing all the user objects associated with this geometry
     * element.
     *
     * @return A new Map containing all the user objects associated with this geometry
     *         element.
     */
    public Map getAllUserData();

    /**
     * Set the user defined object associated with this geometry element and the specified
     * key. This can be used to store any type of information with a geometry element that
     * could be useful.
     *
     * @param key   the key with which the specified value is to be associated.
     * @param value the value to be associated with the specified key.
     * @throws NullPointerException if the <code>key</code> is <code>null</code>.
     */
    public void putUserData(Object key, Object value);

    /**
     * Add all the user defined data in the supplied Map to this objects map of user
     * defined data.
     *
     * @param data The Map of user defined data to be added to this object's map of user
     *             defined data.
     */
    public void putAllUserData(Map data);

    /**
     * Removes the entry for the specified user object key if present.
     *
     * @param key the key whose mapping is to be removed from the map.
     * @throws NullPointerException if the <code>key</code> is <code>null</code>.
     */
    public void removeUserData(Object key);

    /**
     * Add a listener that is notified of changes to the state of this element.
     *
     * @param listener The listener to be notified of changes to the state of this
     *                 element.
     */
    public void addChangeListener(ChangeListener listener);

    /**
     * Remove a listener from receiving notifications of changes to the state of this
     * element.
     *
     * @param listener The listener to be removed from those being notified of changes to
     *                 the state of this element.
     */
    public void removeChangeListener(ChangeListener listener);

}
