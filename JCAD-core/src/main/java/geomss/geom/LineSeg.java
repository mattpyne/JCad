/*
*   LineSeg  -- A concrete line segment in nD space.
*
*   Copyright (C) 2012-2015, Joseph A. Huwaldt
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2.1 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
*/
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.util.List;
import java.util.Objects;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;


/**
*  A concrete line segment in n-dimensional space.
*
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt    Date:  January 14, 2012
*  @version November 27, 2015
*/
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class LineSeg extends LineSegment implements ValueType {

    
    /**
    *  The ends of the line segment.
    */
    private Point _p0, _p1;
    
    /**
     *  The length of the line segment.
     */
    private Parameter<Length> _length;
    
    /**
     *  Unit direction vector for the line segment.
     */
    private Vector<Dimensionless> _u;
    
    /**
     * Dimensional vector for the line segment.
     */
    private Vector<Length> _Ds;
    
    
    
    /**
    * Returns a <code>LineSeg</code> instance with the specified end points.
    * The units of the curve will be the units of the start point.
    *
    * @param p0  The start (beginning) of the line segment. May not be null.
    * @param p1  The end of the line segment. May not be null.
    * @return A <code>LineSeg</code> instance that represents a line between the input points.
    */
    public static LineSeg valueOf(Point p0, Point p1) {
        
        //  Check the dimensionality.
        int numDims = Math.max(p0.getPhyDimension(), p1.getPhyDimension());
        
        LineSeg L = newInstance();
        L._p0 = p0.immutable().toDimension(numDims);
        L._p1 = p1.immutable().to(p0.getUnit()).toDimension(numDims);
        
        computeLineData(L);
        
        return L;
    }
    
    /**
     * Returns a <code>LineSeg</code> instance with the specified start point and
     * direction/length vector. The units of the curve will be the units of the start
     * point.
     *
     * @param p0   The start (beginning) of the line segment. May not be null.
     * @param Ldir The vector defining the direction and length of the line segment. May
     *             not be null.
     * @return A line segment from the input point in the direction and length of the
     *         input vector.
     */
    public static LineSeg valueOf(Point p0, GeomVector<Length> Ldir) {
        
        //  Check the dimensionality.
        int numDims = Math.max(p0.getPhyDimension(), Ldir.getPhyDimension());
        
        LineSeg L = newInstance();
        L._p0 = p0.immutable().toDimension(numDims);
        L._p1 = L._p0.plus(Point.valueOf(Ldir));
        
        computeLineData(L);
        
        return L;
    }
    
    /**
     * Returns a <code>LineSeg</code> that represents a straight line between the end
     * points of the input curve. WARNING: No check is made that the input curve is truly
     * a straight line. The end points of the input curve are simply used to define a
     * straight line segment.
     *
     * @param lineCrv A curve from which the end points will be used to define a line
     *                segment. May not be null.
     * @return The line segment between the end points of the input curve.
     * @see Curve#isLine
     */
    public static LineSeg valueOf(Curve lineCrv) {
        
        LineSeg L = newInstance();
        L._p0 = lineCrv.getRealPoint(0);
        L._p1 = lineCrv.getRealPoint(1);
        
        computeLineData(L);
        
        return L;
    }
    
    /**
     *  Compute values defined by the end points of the line segment.
     *  It is assumed that the maximum dimension and units have
     *  been defined for p0 and p1 before calling this method.
     */
    private static void computeLineData(LineSeg L) {
        Point p0 = L._p0;
        Point p1 = L._p1;
        
        L._length = p0.distance(p1);
        L._Ds = p1.minus(p0).toGeomVector();
        L._u = L._Ds.toUnitVector();
    }
    
    /**
     * Recycles a <code>LineSeg</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to recycle immediately.
     */
    public static void recycle(LineSeg instance) {
        FACTORY.recycle(instance);
    }
    

    /**
     * Return the starting point of the line segment.
     *
     * @return The starting point for this line segment.
     */
    @Override
    public Point getStart() {
        return _p0;
    }
    
    /**
     * Return the end point of the line segment.
     *
     * @return The end point of this line segment.
     */
    @Override
    public Point getEnd() {
        return _p1;
    }
    
    /**
     *  Get a unit direction vector for the line segment.
     * 
     * @return A unit vector indicating the direction of this line segment.
     */
    @Override
    public Vector<Dimensionless> getUnitVector() {
        return _u.copy();
    }
    
    /**
     *  Return the dimensional derivative vector for this line segment.
     *  The length of this vector is the length of the line segment,
     *  the origin is at the start point and the end of the vector is the line end.
     * 
     * @return The dimensional derivative vector for this line segment.
     */
    @Override
    public GeomVector<Length> getDerivativeVector() {
        Vector<Length> D = _Ds.copy();
        D.setOrigin(_p0);
        return D;
    }
    
    /**
    * Calculate a point on the curve for the given parametric distance
    * along the curve.
    *
    * @param s  parametric distance to calculate a point for (0.0 to 1.0 inclusive).
    * @return the calculated point
    */
    @Override
    public Point getRealPoint(double s) {
        validateParameter(s);
        
        if (parNearStart(s, TOL_S))
            return _p0;
        else if (parNearEnd(s, TOL_S))
            return _p1;
        
        Point p = _p0.plus(Point.valueOf(_Ds.times(s)));
        
        return p;
    }
    
    /**
    * Calculate all the derivatives from <code>0</code> to <code>grade</code> with respect
    * to parametric distance on the curve for the given parametric distance along the curve,
    * <code>d^{grade}p(s)/d^{grade}s</code>.
    * <p>
    * Example:<br>
    * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
    * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
    * </p>
    *
    * @param s     Parametric distance to calculate derivatives for (0.0 to 1.0 inclusive).
    * @param grade The maximum grade to calculate the derivatives for (1=1st derivative, 2=2nd derivative, etc)
    * @return A list of derivatives up to the specified grade of the curve at the specified parametric position.
    * @throws IllegalArgumentException if the grade is < 0.
    */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, int grade) {
        validateParameter(s);
        if (grade < 0)
            throw new IllegalArgumentException(RESOURCES.getString("gradeLTZeroErr"));
        
        //  Create a list to hold the output.
        List<Vector<Length>> output = FastTable.newInstance();
        
        //  Calculate the point.
        Point p = getRealPoint(s);
        Vector<Length> Pv = p.toGeomVector();
        output.add(Pv);
        
        //  Calculate each requested derivative.
        int der = 1;
        for (int i=1; i <= grade; ++i, ++der) {
            
            if (i == 1) {
                //  Copy the derivative vector.
                Vector<Length> Dv = _Ds.copy();
                Dv.setOrigin(p);
                output.add(Dv);
                
            } else {
                //  High derivatives are all zero.
                Vector<Length> Dv = Vector.newInstance(getPhyDimension(), getUnit());
                Dv.setOrigin(p);
                output.add(Dv);
            }
        }
        
        return output;
    }
    
    /**
    *  Return the total arc length of this curve.
    *
    *  @param eps The desired fractional accuracy on the arc-length.  For this
    *             curve type, this parameter is ignored and the exact arc
    *             length is always returned.
    *  @return The total arc length of the curve.
    */
    @Override
    public Parameter<Length> getArcLength(double eps) {
        return _length;
    }

    /**
     * Return a new curve that is identical to this one, but with the
     * parameterization reversed.
     *
     * @return A new curve that is identical to this one, but with the
     * parameterization reversed.
     */
    @Override
    public LineSegment reverse() {
        
        //  Create the reversed curve.
        LineSeg crv = LineSeg.valueOf(_p1, _p0);
        
        return copyState(crv);  //  Copy over the super-class state for this object to the new one.
    }
    
    /**
    *  Return the coordinate point representing the
    *  minimum bounding box corner of this geometry element (e.g.: min X, min Y, min Z).
    *
    *  @return The minimum bounding box coordinate for this geometry element.
    */
    @Override
    public Point getBoundsMin() {
        return _p0.min(_p1);
    }

    /**
    *  Return the coordinate point representing the
    *  maximum bounding box corner (e.g.: max X, max Y, max Z).
    *
    *  @return The maximum bounding box coordinate for this geometry element.
    */
    @Override
    public Point getBoundsMax() {
        return _p0.max(_p1);
    }

    /**
    *  Return a copy of this curve converted to the specified number
    *  of physical dimensions.  If the number of dimensions is greater than
    *  this element, then zeros are added to the additional dimensions.
    *  If the number of dimensions is less than this element, then
    *  the extra dimensions are simply dropped (truncated).  If
    *  the new dimensions are the same as the dimension of this element,
    *  then this element is simply returned.
    *
    *  @param newDim  The dimension of the curve to return.
    *  @return This curve converted to the new dimensions.
    */
    @Override
    public LineSeg toDimension(int newDim) {
        if (getPhyDimension() == newDim)    return this;
        
        LineSeg crv = LineSeg.valueOf(_p0.toDimension(newDim), _p1.toDimension(newDim));
        
        return copyState(crv);  //  Copy over the super-class state for this object to the new one.
    }
    
    /**
    * Returns the equivalent to this element but stated in the 
    * specified unit. 
    *
    * @param  unit the length unit of the element to be returned. May not be null.
    * @return an equivalent to this element but stated in the specified unit.
    * @throws ConversionException if the the input unit is not a length unit.
    */
    @Override
    public LineSeg to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        LineSeg crv = LineSeg.valueOf(_p0.to(unit), _p1.to(unit));
        return copyState(crv);  //  Copy over the super-class state for this object to the new one.
    }
    
    /**
    * Returns a copy of this LineSeg instance  
    * {@link javolution.context.AllocatorContext allocated} 
    * by the calling thread (possibly on the stack).
    *   
    * @return an identical and independent copy of this line segment.
    */
    @Override
    public LineSeg copy() {
        return copyOf(this);
    }
    
    /**
     * Return a copy of this object with any transformations or subranges
     * removed (applied).
     *
     * @return A copy of this object with any transformations or subranges
     *      removed (applied).
     */
    @Override
    public LineSeg copyToReal() {
        return copy();
    }
    
    /**
    * Compares this LineSeg against the specified object for strict 
    * equality.
    *
    * @param  obj the object to compare with.
    * @return <code>true</code> if this LineSeg is identical to that
    *       LineSeg; <code>false</code> otherwise.
    */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        
        LineSeg that = (LineSeg)obj;
        return this._p0.equals(that._p0)
                && this._p1.equals(that._p1)
                && super.equals(obj);
    }

    /**
    * Returns the hash code for this parameter.
    * 
    * @return the hash code value.
    */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_p0, _p1);
    }

    /**
    * Holds the default XML representation for this object.
    */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<LineSeg> XML = new XMLFormat<LineSeg>(LineSeg.class) {

        @Override
        public LineSeg newInstance(Class<LineSeg> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, LineSeg obj) throws XMLStreamException {
            LineSegment.XML.read(xml, obj);     // Call parent read.
            
            obj._p0 = xml.getNext();
            obj._p1 = xml.getNext();
            
            computeLineData(obj);
        }

        @Override
        public void write(LineSeg obj, OutputElement xml) throws XMLStreamException {
            LineSegment.XML.write(obj, xml);    // Call parent write.
            
            xml.add(obj._p0);
            xml.add(obj._p1);
            
        }
    };
    
    
    ///////////////////////
    // Factory creation. //
    ///////////////////////

    private LineSeg() {}
    
    private static final ObjectFactory<LineSeg> FACTORY = new ObjectFactory<LineSeg>() {
        @Override
        protected LineSeg create() {
            return new LineSeg();
        }
        @Override
        protected void cleanup(LineSeg obj) {
            obj.reset();
            obj._p0 = null;
            obj._p1 = null;
        }
    };

    private static LineSeg copyOf(LineSeg original) {
        LineSeg o = FACTORY.object();
        o._p0 = original._p0.copy();
        o._p1 = original._p1.copy();
        computeLineData(o);
        original.copyState(o);
        return o;
    }

    private static LineSeg newInstance() {
        return FACTORY.object();
    }
    
}


