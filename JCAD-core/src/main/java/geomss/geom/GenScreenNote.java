/*
 *   GenScreenNote  -- Partial implementation of a note is displayed at a fixed size on the screen.
 *
 *   Copyright (C) 2014-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javolution.text.Text;
import javolution.text.TextBuilder;

/**
 * Partial implementation of a textual note located at a point in nD space, but is
 * displayed at a fixed size on the screen and oriented such that it is always face-on to
 * the user.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 5, 2014
 * @version November 24, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class GenScreenNote extends AbstractNote<GenScreenNote> implements Transformable<GenScreenNote> {

    /**
     * Return an immutable version of this note.
     *
     * @return An immutable version of this note.
     */
    public abstract Note immutable();

    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 1 as a GenScreenNote is located at a single point in
     * model space.
     */
    @Override
    public int size() {
        return 1;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner (e.g.: min
     * X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMin() {
        return getLocation();
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMax() {
        return getLocation();
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this geometry element. This implementation always returns
     * this note location coordinate.
     *
     * @param dim An index indicating the dimension to find the min/max point for
     *            (0=X,1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance to refine the min/max point position to if
     *            necessary.
     * @return The point found on this element that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public Point getLimitPoint(int dim, boolean max, double tol) {
        return getLocation();
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public NoteTrans getTransformed(GTransform transform) {
        return NoteTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the text representation of this geometry element that consists of the text
     * string followed by the coordinate position. For example:
     * <pre>
     *   {aNote = {"A text string.",{10 ft, -3 ft, 4.56 ft}}}
     * </pre>
     * If there is no name, then the output looks like this:
     * <pre>
     *   {"A text string.",{10 ft, -3 ft, 4.56 ft}}
     * </pre>
     *
     * @return the text representation of this geometry element.
     */
    @Override
    public Text toText() {
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        String nameStr = getName();
        boolean hasName = nonNull(nameStr);
        if (hasName) {
            tmp.append(nameStr);
            tmp.append(" = {");
        }
        tmp.append("\"");
        tmp.append(getNote());
        tmp.append("\",");
        tmp.append(getLocation().toText());
        if (hasName)
            tmp.append('}');
        tmp.append('}');
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

}
