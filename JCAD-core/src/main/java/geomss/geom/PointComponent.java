/**
 * PointComponent -- A list of PointArray objects.
 *
 * Copyright (C) 2009-2016, by Joseph A. Huwaldt, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A list that holds only {@link PointArray} objects.
 * <p>
 * WARNING: This list allows geometry to be stored in different units. If consistent units
 * are required, then the user must specifically convert the list items.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 1, 2009
 * @version March 31, 2016
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class PointComponent extends AbstractPointGeomList<PointComponent, PointArray> {

    private FastTable<PointArray> _list;

    /**
     * Return the list underlying this geometry list.
     *
     * @return The list underlying this geometry list.
     */
    @Override
    protected FastTable<PointArray> getList() {
        return _list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>PointComponent</code> instance
     * (on the stack when executing in a <code>StackContext</code>), that can store a list
     * of {@link PointArray} objects.
     *
     * @return A new, empty PointComponent.
     */
    public static PointComponent newInstance() {
        PointComponent list = FACTORY.object();
        list._list = FastTable.newInstance();
        return list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>PointComponent</code> instance
     * (on the stack when executing in a <code>StackContext</code>) with the specified
     * name, that can store a list of {@link PointArray} objects.
     *
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @return A new, empty PointComponent.
     */
    public static PointComponent newInstance(String name) {
        PointComponent list = PointComponent.newInstance();
        list.setName(name);
        return list;
    }

    /**
     * Return a PointComponent made up of the {@link PointArray} objects in the specified
     * collection.
     *
     * @param name     The name to be assigned to this list (may be <code>null</code>).
     * @param elements A collection of PointArray elements. May not be null.
     * @return A new PointComponent containing the elements in the specified collection.
     */
    public static PointComponent valueOf(String name, Collection<? extends PointArray> elements) {
        for (Object element : elements) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof PointArray))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "PointComponent", "PointArray"));
        }

        PointComponent list = PointComponent.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a PointComponent made up of the {@link PointArray} objects in the specified
     * array.
     *
     * @param name     The name to be assigned to this list (may be <code>null</code>).
     * @param elements A list of PointArray elements. May not be null.
     * @return A new PointComponent containing the elements in the specified array.
     */
    public static PointComponent valueOf(String name, PointArray... elements) {
        requireNonNull(elements);
        PointComponent list = PointComponent.newInstance(name);
        list.addAll(Arrays.asList(elements));

        return list;
    }

    /**
     * Return a PointComponent made up of the {@link PointArray} objects in the specified
     * array.
     *
     * @param elements A list of PointArray elements. May not be null.
     * @return A new PointComponent containing the elements in the specified array.
     */
    public static PointComponent valueOf(PointArray... elements) {
        return PointComponent.valueOf(null, elements);
    }

    /**
     * Returns the range of elements in this list from the specified start and ending
     * indexes.
     *
     * @param first index of the first element to return.
     * @param last  index of the last element to return.
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range (index &lt; 0
     * || index &ge; size())
     */
    @Override
    public PointComponent getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        PointComponent list = PointComponent.newInstance();
        for (int i = first; i <= last; ++i)
            list.add(get(i));
        return list;
    }

    /**
     * Returns an new {@link PointString} with the elements in this list in reverse order.
     *
     * @return A new PointComponent with the elements in this list in reverse order.
     */
    @Override
    public PointComponent reverse() {
        PointComponent list = PointComponent.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = size - 1; i >= 0; --i) {
            list.add(get(i));
        }
        return list;
    }

    /**
     * Return the equivalent of this list converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this list is simply
     * returned.
     *
     * @param newDim The dimension of the element to return.
     * @return The equivalent of this list converted to the new dimensions.
     */
    @Override
    public PointComponent toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;
        PointComponent newList = PointComponent.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointArray element = this.get(i);
            newList.add(element.toDimension(newDim));
        }
        return newList;
    }

    /**
     * Returns the equivalent to this list but with <I>all</I> the elements stated in the
     * specified unit.
     *
     * @param unit the length unit of the list to be returned. May not be null.
     * @return an equivalent to this list but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public PointComponent to(Unit<Length> unit) {
        requireNonNull(unit);
        PointComponent list = PointComponent.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointArray e = this.get(i);
            list.add(e.to(unit));
        }
        return list;
    }

    /**
     * Returns a copy of this <code>PointComponent</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public PointComponent copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this list with any sub-element transformations or subranges
     *         removed.
     */
    @Override
    public PointComponent copyToReal() {
        PointComponent newList = PointComponent.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointArray element = this.get(i);
            newList.add(element.copyToReal());
        }
        return newList;
    }

    /**
     * Return the total number of quadrilateral panels in this component.
     *
     * @return the total number of panels in this component.
     * @throws IndexOutOfBoundsException if the strings in any array in this component
     * have different lengths.
     */
    public int getNumberOfPanels() throws IndexOutOfBoundsException {
        int sum = 0;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointArray e = this.get(i);
            sum += e.getNumberOfPanels();
        }
        return sum;
    }

    /**
     * Returns transformed version of this element. The returned object implements
     * {@link GeomTransform} and contains transformed versions of the contents of this
     * list as children.
     *
     * @param transform The transform to apply to this geometry element. May not be null.
     * @return A transformed version of this geometry element.
     * @throws DimensionException if this element is not 3D.
     */
    @Override
    public PointComponent getTransformed(GTransform transform) {
        requireNonNull(transform);
        PointComponent list = PointComponent.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointArray element = this.get(i);
            list.add(element.getTransformed(transform));
        }
        return list;
    }

    /**
     * Replaces the {@link PointArray} at the specified position in this list with the
     * specified element. Null elements are ignored. The input element must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param index   The index of the element to replace (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The element to be stored at the specified position. May not be null.
     * @return The element previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if <code>index > size()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public PointArray set(int index, PointArray element) {
        return super.set(index, requireNonNull(element));
    }

    /**
     * Inserts the specified {@link PointArray} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored. The input
     * value must have the same physical dimensions as the other items in this list, or
     * an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the list is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input value dimensions are different from
     * this list's dimensions.
     */
    @Override
    public void add(int index, PointArray value) {
        super.add(index, requireNonNull(value));
    }

    /**
     * Inserts all of the {@link PointArray} objects in the specified collection into this
     * list at the specified position. Shifts the element currently at that position (if
     * any) and any subsequent elements to the right (increases their indices). The new
     * elements will appear in this list in the order that they are returned by the
     * specified collection's iterator. The behavior of this operation is unspecified if
     * the specified collection is modified while the operation is in progress. Note that
     * this will occur if the specified collection is this list, and it's nonempty.  The
     * input elements must have the same physical dimensions as the other items in this
     * list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified collection.
     * @param c     Elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends PointArray> c) {
        int thisSize = this.size();
        for (Object element : c) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof PointArray))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "PointComponent", "PointArray"));
            if (thisSize != 0) {
                if (((GeomElement)element).getPhyDimension() != this.getPhyDimension())
                    throw new DimensionException(RESOURCES.getString("dimensionErr"));
            }
        }
        return super.addAll(index, c);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<PointComponent> XML = new XMLFormat<PointComponent>(PointComponent.class) {

        @Override
        public PointComponent newInstance(Class<PointComponent> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            PointComponent obj = PointComponent.newInstance();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, PointComponent obj) throws XMLStreamException {
            AbstractPointGeomList.XML.read(xml, obj);     // Call parent read.
        }

        @Override
        public void write(PointComponent obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractPointGeomList.XML.write(obj, xml);    // Call parent write.
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<PointComponent> FACTORY = new ObjectFactory<PointComponent>() {
        @Override
        protected PointComponent create() {
            return new PointComponent();
        }

        @Override
        protected void cleanup(PointComponent obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(PointComponent instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected PointComponent() {
    }

    private static PointComponent copyOf(PointComponent original) {
        PointComponent o = PointComponent.newInstance();
        original.copyState(o);
        int size = original.size();
        for (int i = 0; i < size; ++i) {
            PointArray element = original.get(i);
            o.add(element.copy());
        }
        return o;
    }
}
