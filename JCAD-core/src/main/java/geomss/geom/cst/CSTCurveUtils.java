/**
 * CSTCurveUtils -- A collection of methods for working with CSTCurve objects.
 *
 * Copyright (C) 2015-2016, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.cst;

import geomss.geom.*;
import geomss.geom.nurbs.BasicNurbsCurve;
import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.KnotVector;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.BasisFunction;
import jahuwaldt.tools.math.MathTools;
import jahuwaldt.tools.math.ModelData;
import jahuwaldt.tools.math.RootException;
import java.text.MessageFormat;
import java.util.Arrays;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javolution.context.ArrayFactory;
import javolution.context.StackContext;

/**
 * A collection of methods for working with Class-Shape-Transform or {@link CSTCurve}
 * curves.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: October 9, 2015
 * @version January 7, 2016
 */
public final class CSTCurveUtils {
    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = CSTCurve.RESOURCES;
    
    /**
     * Method that returns a {@link BasicCSTCurve} that approximates the input list of
     * points on either the upper or lower surface of a blunt nosed airfoil. This is
     * useful to approximate a specific airfoil or blunt LE body shape. This method offers
     * control over the leading edge radius and the trailing edge slope. The input points
     * must be ordered from leading edge to trailing edge and must be co-planar. In
     * addition, the input xhat (chord- wise direction vector) must be parallel to or
     * contained in the plane containing the points. The origin of the resulting CSTCurve
     * will be taken as the first point in the input list of points and the trailing edge
     * offset will be taken as the distance from the last point in the input list to the
     * line formed by passing xhat through the origin point. Finally, the points must
     * represent a shape that can be modeled by a blunt-nosed CST curve with the given
     * class function or the fit will be very poor.
     *
     * @param order  The order for the Bezier curve used for the shape function. This
     *               controls the accuracy that the input points are approximated.
     * @param xhat   The chord-wise direction to assume for the input points. This vector
     *               must lie parallel to the plane containing the points being
     *               approximated. May not be null.
     * @param rLE    The LE radius to use for the airfoil. May not be null.
     * @param tanTE  The trailing edge tangent vector on the airfoil curve. May not be null.
     * @param points The list of airfoil points ordered from leading edge to trailing edge
     *               to be approximated by a CSTCurve. May not be null.
     * @param tol    The tolerance to use when determining if the input points are
     *               co-planar. May not be null.
     * @return A new BasicCSTCurve that approximates the input list of points. The user
     *         data with the key "CHISQ" contains the chi-squared measure of the fit. A
     *         value near zero indicates a perfect fit.
     * @throws RootException if a fit to the points could not be made.
     */
    public static BasicCSTCurve approxAirfoilPoints(int order, GeomVector<Dimensionless> xhat,
            Parameter<Length> rLE, GeomVector<Dimensionless> tanTE, PointString<?> points,
            Parameter<Length> tol) throws RootException {
        requireNonNull(rLE);
        requireNonNull(tanTE);
        
        StackContext.enter();
        try {
            //  Is the input curve planar?
            BasicNurbsCurve ncrv = CurveFactory.fitPoints(2, requireNonNull(points));
            if (!ncrv.isPlanar(requireNonNull(tol)))
                throw new IllegalArgumentException(RESOURCES.getString("approxPntsNotCoplanarErr"));

            //  Find the largest dimension among the inputs.
            int numDims = points.getPhyDimension();
            if (numDims < xhat.getPhyDimension())
                numDims = xhat.getPhyDimension();
            if (numDims < 3)
                numDims = 3;

            //  Convert to common dimensions.
            points = points.toDimension(numDims);
            xhat = xhat.toDimension(numDims);
            ncrv = ncrv.toDimension(numDims);

            //  Since curve is planar, any binormal will work as the plane normal vector.
            Vector<Dimensionless> nhat = ncrv.getBinormal(0.5);
            Point pMid = nhat.getOrigin();

            //  Determine the yhat direction for the curve.
            Vector<Dimensionless> yhat = xhat.cross(nhat).toUnitVector();

            //  The origin is the first point in the input list.
            Point origin = points.get(0).immutable();

            //  Determine the distance from the end of the points curve to the chord-line.
            Point pTE = GeomUtil.pointLineClosest(points.get(-1), origin, xhat);
            Parameter<Length> chord = pTE.distance(origin);
            Parameter<Length> offsetTE = GeomUtil.pointPlaneDistance(points.get(-1), pTE, yhat);
            double offsetTE_c = offsetTE.divide(chord).asType(Dimensionless.class).getValue(Dimensionless.UNIT);
            if (MathTools.isApproxZero(offsetTE_c))
                offsetTE_c = 0;

            //  Check to make sure that xhat is in the plane of the curve.
            double ftol = tol.divide(chord).asType(Dimensionless.class).getValue(Dimensionless.UNIT);
            IntersectType itype = GeomUtil.linePlaneIntersect(origin, xhat, Plane.valueOf(nhat, pMid), null, ftol);
            if (!itype.equals(IntersectType.COINCIDENT))
                throw new IllegalArgumentException(RESOURCES.getString("xhatNotCoincidentWithPntsPlane"));

            //  Create the class function we need.
            CSTClassFunction Cf = CSTClassFunction.BLUNTNOSED_AIRFOIL;

            //  Create the shape function values we are trying to approximate.
            int size = points.size();
            double[] Sf = new double[size];
            double[] x_cArr = new double[size];
            int sizem1 = size - 1;
            for (int i = 1; i < sizem1; ++i) {
                Vector<Length> pv = points.get(i).toGeomVector();
                double x_c = xhat.times(pv).divide(chord).asType(Dimensionless.class).getValue(Dimensionless.UNIT);
                double y_c = yhat.times(pv).divide(chord).asType(Dimensionless.class).getValue(Dimensionless.UNIT);
                x_cArr[i] = x_c;
                Sf[i] = (y_c - offsetTE_c * x_c) / Cf.getValue(x_c);
            }

            //  Use the input leading edge radius to compute Sf[0] = sqrt(2*rLE/c).
            x_cArr[0] = 0;
            double rLE_c = rLE.divide(chord).asType(Dimensionless.class).getValue(Dimensionless.UNIT);
            Sf[0] = Math.sqrt(2*rLE_c);

            //  Use the input trailing edge tangent vector to compute Sf[1] = tan(beta) + offsetTE/c.
            x_cArr[sizem1] = 1;
            Parameter tanTEx = xhat.times(tanTE);
            Parameter tanTEy = yhat.times(tanTE).opposite();
            double tanBeta = tanTEy.divide(tanTEx).getValue();
            Sf[sizem1] = tanBeta + offsetTE_c;

            //  Fit a set of basis function coefficients to the Sf data in a least-square error sense.
            double[] coefs = new double[order];
            double[] sig = new double[size];
            Arrays.fill(sig, 1.0);

            //  Force the curve to match the input LE radius and TE slope more accurately than other points.
            sig[0] = 0.5;
            sig[sizem1] = 0.5;

            double chisq = ModelData.fit(x_cArr, Sf, sig, new BezierFitBasisFunction(order), coefs);

            //  Create the Shape Function from the basis function fit coefficients.
            CSTShapeFunction Sfunc = CSTShapeFunction.newInstance(order, coefs);

            //  Create the CST curve for the airfoil.
            BasicCSTCurve crv = BasicCSTCurve.newInstance(Cf, Sfunc, offsetTE, origin, chord, xhat, yhat);
            crv.putUserData("CHISQ", chisq);

            return StackContext.outerCopy(crv);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * BasisFunction used to fit a Bezier curve to a set of data where the parameters of
     * the function are the coefficients of the Bernstein polynomial of the Bezier curve.
     */
    private static class BezierFitBasisFunction implements BasisFunction {
        private final KnotVector kv;

        public BezierFitBasisFunction(int order) {
            kv = KnotVector.bezierKnotVector(order-1);
        }

        @Override
        public int getMinNumCoef() {
            return kv.getDegree()+1;
        }

        @Override
        public void func(double x, double[] p) {
            double[] Nik = kv.basisFunctions(x);
            int size = p.length;
            for (int i=0; i < size; ++i)
                p[i] = Nik[i];
            ArrayFactory.DOUBLES_FACTORY.recycle(Nik);
        }
    }

    /**
     * Return a {@link BasicCSTCurve} that represents the thickness distribution for an
     * airfoil formed by the input upper and lower airfoil curves. The input curves must
     * be of the same order.
     *
     * @param upper The {@link CSTCurve} that represents the upper surface of the airfoil.
     *              May not be null.
     * @param lower The {@link CSTCurve} that represents the lower surface of the airfoil.
     *              May not be null.
     * @return A curve that represents the thickness distribution for the airfoil. The
     *         returned curve will have the same units, dimensions, origin, chord and
     *         trailing edge thickness as the input upper surface curve.
     */
    public static BasicCSTCurve getThicknessDist(CSTCurve upper, CSTCurve lower) {
        // Reference: Kulfan, B.M., Bussoletti, J.E., "'Fundamental' Parametric Geometry
        //          Representations for Aircraft Component Shapes", AIAA-2006-6948, pg. 32.

        CSTShapeFunction Supper = upper.getSFunction();
        CSTShapeFunction Slower = lower.getSFunction();
        int order = Supper.getOrder();
        if (order != Slower.getOrder())
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("crvsDifferentOrder"), "upper", "lower airfoil"));

        //  Get the Bernstein Polynomial coefficients for each curve.
        double[] Aupper = Supper.getCoefficients();
        double[] Alower = Slower.getCoefficients();

        //  Thickness distribution is formed form the average of the
        //  upper and lower surface coefficients.
        double[] Ath = Aupper;
        for (int i=0; i < order; ++i) {
            Ath[i] = (Aupper[i] + Alower[i])/2;
        }

        //  Create the Shape Function from the thickness profile coefficients.
        CSTShapeFunction Sfunc = CSTShapeFunction.newInstance(order, Ath);

        //  Create the CST curve for the airfoil.
        BasicCSTCurve crv = BasicCSTCurve.newInstance(upper.getCFunction(), Sfunc, upper.getOffsetTE(), 
                upper.getOrigin(), upper.getChord(), upper.getXhat(), upper.getYhat());

        return crv;
    }

    /**
     * Return a {@link BasicCSTCurve} that represents the camber distribution for an
     * airfoil formed by the input upper and lower airfoil curves. The input curves must
     * be of the same order.
     *
     * @param upper The {@link CSTCurve} that represents the upper surface of the airfoil.
     *              May not be null.
     * @param lower The {@link CSTCurve} that represents the lower surface of the airfoil.
     *              May not be null.
     * @return A curve that represents the camber distribution for the airfoil. The
     *         returned curve will have the same units, dimensions, origin, chord and
     *         trailing edge thickness as the input upper surface curve.
     */
    public static BasicCSTCurve getCamberDist(CSTCurve upper, CSTCurve lower) {
        // Reference: Kulfan, B.M., Bussoletti, J.E., "'Fundamental' Parametric Geometry
        //          Representations for Aircraft Component Shapes", AIAA-2006-6948, pg. 32.

        CSTShapeFunction Supper = upper.getSFunction();
        CSTShapeFunction Slower = lower.getSFunction();
        int order = Supper.getOrder();
        if (order != Slower.getOrder())
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("crvsDifferentOrder"), "upper", "lower airfoil"));

        //  Get the Bernstein Polynomial coefficients for each curve.
        double[] Aupper = Supper.getCoefficients();
        double[] Alower = Slower.getCoefficients();

        //  Camber distribution is formed form the difference of the
        //  upper and lower surface coefficients.
        double[] Acamb = Aupper;
        for (int i=0; i < order; ++i) {
            Acamb[i] = (Aupper[i] - Alower[i])/2;
        }

        //  Create the Shape Function from the thickness profile coefficients.
        CSTShapeFunction Sfunc = CSTShapeFunction.newInstance(order, Acamb);

        //  Create the CST curve for the airfoil.
        BasicCSTCurve crv = BasicCSTCurve.newInstance(upper.getCFunction(), Sfunc, upper.getOffsetTE(), 
                upper.getOrigin(), upper.getChord(), upper.getXhat(), upper.getYhat());

        return crv;
    }
}
