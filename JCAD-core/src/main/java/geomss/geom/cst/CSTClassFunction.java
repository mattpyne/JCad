/**
 * CSTClassFunction -- A generic CST Class Function implementation.
 *
 * Copyright (C) 2014-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.cst;

import jahuwaldt.tools.math.MathTools;
import static java.lang.Math.*;
import javolution.context.ImmortalContext;
import javolution.context.ObjectFactory;
import javolution.lang.ValueType;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.apache.commons.math3.analysis.differentiation.DerivativeStructure;

/**
 * A generic Class-Shape-Transform (CST) class function implementation. The "class" of the
 * function is determined by the input exponents: C(s) = (s)^N1*(1 - s)^N2.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt Date: March 14, 2014
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class CSTClassFunction implements Cloneable, XMLSerializable, ValueType {
    // Reference: Kulfan, B.M., Bussoletti, J.E., "'Fundamental' Parametric Geometry
    //          Representations for Aircraft Component Shapes", AIAA-2006-6948.

    //  The exponents for the class function.
    private double N1, N2;

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private CSTClassFunction() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<CSTClassFunction> FACTORY = new ObjectFactory<CSTClassFunction>() {
        @Override
        protected CSTClassFunction create() {
            return new CSTClassFunction();
        }
    };

    @SuppressWarnings("unchecked")
    private static CSTClassFunction copyOf(CSTClassFunction original) {
        CSTClassFunction obj = FACTORY.object();
        obj.N1 = original.N1;
        obj.N2 = original.N2;
        return obj;
    }

    /**
     * Construct a new CST Class Function using the specified exponents.
     *
     * @param N1 The 1st exponent of the class function (leading edge exponent).
     * @param N2 The 2nd exponent of the class function (trailing edge exponent).
     * @return 
     */
    public static CSTClassFunction newInstance(double N1, double N2) {
        CSTClassFunction obj = FACTORY.object();
        obj.N1 = N1;
        obj.N2 = N2;
        return obj;
    }

    /**
     * A class function for a blunt-nosed airfoil (subsonic airfoil).
     */
    public static final CSTClassFunction BLUNTNOSED_AIRFOIL;

    /**
     * A class function for a sharp-nosed airfoil (supersonic airfoil).
     */
    public static final CSTClassFunction SHARPNOSED_AIRFOIL;

    /**
     * A class function for an elliptical shape/airfoil.
     */
    public static final CSTClassFunction ELLIPTICAL;

    /**
     * A class function for a Sears-Haack minimum wave-drag body profile.
     */
    public static final CSTClassFunction SEARS_HAACK;

    /**
     * A class function for a cone or wedge shape.
     */
    public static final CSTClassFunction CONE_WEDGE;

    /**
     * A class function for a rectangular shape.
     */
    public static final CSTClassFunction RECTANGULAR;

    static {
        ImmortalContext.enter();
        try {
            BLUNTNOSED_AIRFOIL = CSTClassFunction.newInstance(0.5, 1.0);
            SHARPNOSED_AIRFOIL = CSTClassFunction.newInstance(1.0, 1.0);
            ELLIPTICAL = CSTClassFunction.newInstance(0.5,0.5);
            SEARS_HAACK = CSTClassFunction.newInstance(0.75, 0.75);
            CONE_WEDGE = CSTClassFunction.newInstance(1.0, 0.0);
            RECTANGULAR = CSTClassFunction.newInstance(0.0, 0.0);
        } finally {
            ImmortalContext.exit();
        }
    }
    
    /**
     * Return the first exponent of the class function, N1.
     * 
     * @return The first exponent of the class function.
     */
    public double getN1() {
        return N1;
    }
    
    /**
     * Return the second exponent of the class function, N2.
     * 
     * @return The second exponent of the class function.
     */
    public double getN2() {
        return N2;
    }
    
    /**
     * Return the value of this class function at the specified parametric location.
     *
     * @param s The parametric distance to calculate this class functions value for (0 to
     *          1 inclusive).
     * @return The value of this class function at the specified parametric location.
     */
    public double getValue(double s) {
        double t1;
        if (MathTools.isApproxEqual(N1, 1.0))       //  N1 == 1.0
            t1 = s;
        else if (MathTools.isApproxEqual(N1, 0.5))  //  N1 == 0.5
            t1 = sqrt(s);
        else
            t1 = pow(s, N1);
        double t2 = 1 - s;
        if (MathTools.isApproxEqual(N2, 0.5))       //  N2 == 0.5
            t2 = sqrt(t2);
        else if (!MathTools.isApproxEqual(N2, 1.0)) //  N2 != 1.0
            t2 = pow(t2, N2);
        return t1 * t2;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the class function for the given parametric
     * distance along the class function, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     Parametric distance to calculate derivatives for (0.0 to 1.0
     *              inclusive).
     * @param grade The maximum grade to calculate the derivatives for (0 = value only,
     *              1=value + 1st derivative, 2=value + 1st derivative + 2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the class function at
     *         the specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    public double[] getDerivatives(double s, int grade) {
        //  This works around a problem in DerivativeStructure at s=0 and s=1.
        if (s < MathTools.EPS)
            s = MathTools.EPS;
        else if (s > 1 - MathTools.EPS)
            s = 1 - MathTools.EPS;
        
        //  Construct the class function point.
        DerivativeStructure x = new DerivativeStructure(1, grade, 0, s);    //  f(x) = x
        DerivativeStructure xN1 = x.pow(N1);                                //  x^N1
        DerivativeStructure OmxN2 = x.negate().add(1).pow(N2);              //  (1 - x)^N2
        DerivativeStructure Cf = xN1.multiply(OmxN2);                       //  Cf(x) = (x)^N1*(1-x)^N2

        //  Return the derivatives.
        double[] ders = Cf.getAllDerivatives();

        return ders;
    }

    /**
     * Compares this CSTClassFunction against the specified object for strict equality
     * (same values).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this object is identical to that object;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        CSTClassFunction that = (CSTClassFunction)obj;
        return this.N1 == that.N1 && this.N2 == that.N2;
    }

    /**
     * Returns the hash code for this object.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = hash * 31 + makeVarCode(N1);
        hash = hash * 31 + makeVarCode(N2);
        return hash;
    }

    private static int makeVarCode(double value) {
        long bits = Double.doubleToLongBits(value);
        int var_code = (int)(bits ^ (bits >>> 32));
        return var_code;
    }
    
    /**
     * Returns a copy of this CSTClassFunction instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public CSTClassFunction copy() {
        return copyOf(this);
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<CSTClassFunction> XML = new XMLFormat<CSTClassFunction>(CSTClassFunction.class) {

        @Override
        public CSTClassFunction newInstance(Class<CSTClassFunction> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, CSTClassFunction obj) throws XMLStreamException {
            double N1 = xml.getAttribute("N1", 1.0);
            double N2 = xml.getAttribute("N2", 1.0);
            obj.N1 = N1;
            obj.N2 = N2;
        }

        @Override
        public void write(CSTClassFunction obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            xml.setAttribute("N1", obj.N1);
            xml.setAttribute("N2", obj.N2);
        }
    };
    
}
