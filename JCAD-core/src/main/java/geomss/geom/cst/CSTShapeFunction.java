/**
 * CSTShapeFunction -- A generic CST Shape Function implementation.
 *
 * Copyright (C) 2014-2016, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.cst;

import geomss.geom.Point;
import geomss.geom.Vector;
import geomss.geom.nurbs.*;
import java.util.List;
import java.util.ResourceBundle;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;

/**
 * A generic Class-Shape-Transform (CST) shape function implementation.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt Date: March 14, 2014
 * @version January 8, 2016
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class CSTShapeFunction implements Cloneable, XMLSerializable, ValueType {
    // Reference: Kulfan, B.M., Bussoletti, J.E., "'Fundamental' Parametric Geometry
    //          Representations for Aircraft Component Shapes", AIAA-2006-6948.
    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = CSTCurve.RESOURCES;

    private BasicNurbsCurve Scrv;

    /**
     * Construct a new shape function of the specified order from the specified list of
     * Bernstein Polynomial coefficients.
     *
     * @param order  The order of the shape function to return.
     * @param Acoefs The coefficients of the Bernstein Polynomial used to construct the
     *               shape function. If more than "order" coefficients are provided then
     *               the additional coefficients are ignored. May not be null.
     * @return A new shape function of the specified order from the specified list of
     *         Bernstein Polynomial coefficients.
     */
    public static CSTShapeFunction newInstance(int order, double... Acoefs) {
        if (Acoefs.length < order)
            throw new IllegalArgumentException(RESOURCES.getString("orderNumCoefsErr"));

        //  Create a Bezier knot vector of the correct degreeU.
        int degree = order - 1;
        KnotVector kv = KnotVector.bezierKnotVector(degree);

        //  Turn the input coefficients into a list of 1D, unweighted, control points.
        FastTable<ControlPoint> cpLst = FastTable.newInstance();
        for (int i = 0; i < order; ++i) {
            double Ai = Acoefs[i];
            Point p = Point.valueOf(Ai, SI.METER);
            ControlPoint cp = ControlPoint.valueOf(p, 1);
            cpLst.add(cp);
        }

        //  Create the defining Bezier curve.
        BasicNurbsCurve Scrv = BasicNurbsCurve.newInstance(cpLst, kv);
        FastTable.recycle(cpLst);

        //  Construct a new CSTShapeFunction and fill in the inputs.
        CSTShapeFunction obj = FACTORY.object();
        obj.Scrv = Scrv;

        return obj;
    }

    /**
     * Construct a new shape function from the specified 1D basis function Bezier curve.
     *
     * @param bfCrv The 1D basis function Bezier curve used to define this shape function.
     *              May not be null.
     * @return A new shape function based on the specified 1D basis function Bezier curve.
     */
    public static CSTShapeFunction valueOf(NurbsCurve bfCrv) {
        if (bfCrv.getPhyDimension() > 1)
            throw new IllegalArgumentException(RESOURCES.getString("oneDCurve"));
        
        FastTable<Double> pts = CurveUtils.getBezierStartParameters(bfCrv);
        if (pts.size() > 2)
            throw new IllegalArgumentException(RESOURCES.getString("singleBezierSegmentErr"));
        FastTable.recycle(pts);
        
        CSTShapeFunction obj = FACTORY.object();
        obj.Scrv = bfCrv.immutable().to(SI.METER);
        
        return obj;
    }

    /**
     * Return the order of this shape function.
     *
     * @return order of shape function (degreeU + 1)
     * @see #getCoefficients() 
     */
    public int getOrder() {
        return Scrv.getDegree() + 1;
    }

    /**
     * Return the basis function for this shape function. The returned NURBS curve will
     * always represent a 1D Bezier curve.
     * @return The basis function for this shape function.
     * @see #getCoefficients() 
     */
    public BasicNurbsCurve getBasisFunction() {
        return Scrv;
    }
    
    /**
     * Return the value of this shape function at the specified parametric location.
     *
     * @param s The parametric distance along the curve to calculate the value of this
     *          shape function at.
     * @return The value of this shape function at the specified parametric location.
     */
    public double getValue(double s) {
        double S = Scrv.getRealPoint(s).getValue(Point.X);
        return S;
    }

    /**
     * Return a 2D point that represents the value of this shape function at the specified
     * parametric position: (s,getValue(s)).
     *
     * @param s The parametric distance along the curve to calculate the value of this
     *          shape function at.
     * @return A 2D point that represents the value of this shape function at the
     *         specified parametric position: P(s) = getValue(s).
     */
    public Point get2DPoint(double s) {
        Point p = Point.valueOf(s, getValue(s));
        return p;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the shape function for the given parametric
     * distance along the shape function, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     Parametric distance to calculate derivatives for (0.0 to 1.0
     *              inclusive).
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return An array of derivatives up to the specified grade of the shape function at
     *         the specified parametric position. The returned array was allocated using
     *         javolution.context.ArrayFactory.DOUBLES_FACTORY, could be longer than the
     *         number of derivatives requested, and could be recycled by the user when no
     *         longer needed.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    public double[] getDerivatives(double s, int grade) {
        double[] output = ArrayFactory.DOUBLES_FACTORY.array(grade + 1);
        
        StackContext.enter();
        try {
            List<Vector<Length>> ders = Scrv.getSDerivatives(s, grade);
            for (int i = grade; i >= 0; --i)
                output[i] = ders.get(i).getValue(Point.X);
        } finally {
            StackContext.exit();
        }
        
        return output;
    }

    /**
     * Return the array of Bernstein Polynomial coefficients for this shape function.
     * The array will have a length of the order of the polynomial.
     * 
     * @return The array of Bernstein Polynomial coefficients for this shape function.
     * @see #getOrder() 
     * @see #getBasisFunction() 
     */
    public double[] getCoefficients() {
        List<ControlPoint> cps = Scrv.getControlPoints();
        int order = cps.size();
        double[] Acoef = new double[order];
        
        for (int i=0; i < order; ++i) {
            ControlPoint cp = cps.get(i);
            Point p = cp.getPoint();
            Acoef[i] = p.getValue(Point.X);
        }
        
        FastTable.recycle((FastTable)cps);
        
        return Acoef;
    }
    
    /**
     * Return <code>true</code> if this shape function contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the values are
     * NaN or Inf.
     *
     * @return true if this shape function contains valid and finite numerical values.
     */
    public boolean isValid() {
        return Scrv.isValid();
    }

    /**
     * Compares this CSTShapeFunction against the specified object for strict equality
     * (same values).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this object is identical to that object;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        CSTShapeFunction that = (CSTShapeFunction)obj;
        return this.Scrv.equals(that.Scrv);
    }

    /**
     * Returns the hash code for this object.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        hash = hash * 31 + Scrv.hashCode();

        return hash;
    }

    /**
     * Returns a copy of this CSTShapeFunction instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public CSTShapeFunction copy() {
        return copyOf(this);
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<CSTShapeFunction> XML = new XMLFormat<CSTShapeFunction>(CSTShapeFunction.class) {
        @Override
        public CSTShapeFunction newInstance(Class<CSTShapeFunction> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, CSTShapeFunction obj) throws XMLStreamException {
            BasicNurbsCurve Scrv = xml.getNext();
            obj.Scrv = Scrv;
        }

        @Override
        public void write(CSTShapeFunction obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            xml.add(obj.Scrv);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private CSTShapeFunction() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<CSTShapeFunction> FACTORY = new ObjectFactory<CSTShapeFunction>() {
        @Override
        protected CSTShapeFunction create() {
            return new CSTShapeFunction();
        }

        @Override
        protected void cleanup(CSTShapeFunction obj) {
            obj.Scrv = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static CSTShapeFunction copyOf(CSTShapeFunction original) {
        CSTShapeFunction obj = FACTORY.object();
        obj.Scrv = original.Scrv.copy();
        return obj;
    }
}
