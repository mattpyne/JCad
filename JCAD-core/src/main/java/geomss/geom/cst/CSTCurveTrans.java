/**
 * CSTCurveTrans -- A GeomTransform that has a CSTCurve for a child.
 *
 * Copyright (C) 2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.cst;

import geomss.geom.*;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.Immutable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} object that refers to a {@link CSTCurve} object and masquerades
 * as a CSTCurve object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: October 7, 2015
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class CSTCurveTrans extends CSTCurve implements GeomTransform<CSTCurve> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private CSTCurve _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link CSTCurveTrans} instance holding the specified {@link CSTCurve}
     * and {@link GTransform}.
     *
     * @param child     The CSTCurve that is the child of this transform element (may not
     *                  be <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     */
    public static CSTCurveTrans newInstance(CSTCurve child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "CST curve", child.getPhyDimension()));

        CSTCurveTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation represented by this element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     *
     * @return The total transformation represented by the entire chain of transform
     *         objects.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this element.
     */
    @Override
    public CSTCurve getChild() {
        return _child;
    }

    /**
     * Return the class function associated with this CST curve.
     *
     * @return The class function associated with this CST curve.
     */
    @Override
    public CSTClassFunction getCFunction() {
        return _child.getCFunction();
    }

    /**
     * Return the shape function associated with this CST curve.
     *
     * @return The shape function associated with this CST curve.
     */
    @Override
    public CSTShapeFunction getSFunction() {
        return _child.getSFunction();
    }

    /**
     * Return the offset of the trailing edge of the curve from the chord line.
     *
     * @return The offset of the trailing edge of the curve from the chord line.
     */
    @Override
    public Parameter<Length> getOffsetTE() {
        //  It is possible that the transform scales the offset length.
        StackContext.enter();
        try {
            Parameter<Length> dy = _child.getOffsetTE();
            GeomVector<Length> vec = _child.getYhat().times(dy).to(dy.getUnit());
            vec = vec.getTransformed(_TM);
            return StackContext.outerCopy(vec.mag());
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the chord length of the curve. This defines the scale or size of the curve.
     *
     * @return The chord length of the curve.
     */
    @Override
    public Parameter<Length> getChord() {
        //  It is possible that the transform scales the chord length.
        StackContext.enter();
        try {
            Parameter<Length> chord = _child.getChord();
            GeomVector<Length> vec = _child.getXhat().times(chord).to(chord.getUnit());
            vec = vec.getTransformed(_TM);
            return StackContext.outerCopy(vec.mag());
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the origin of the leading edge of this curve. This defines the s=0 location
     * of the curve in space.
     *
     * @return The origin of the leading edge of this curve.
     */
    @Override
    public Point getOrigin() {
        return _TM.transform(_child.getOrigin());
    }

    /**
     * Return the chord-wise direction of this curve (the curve X-direction).
     *
     * @return The chord-wise direction of this curve.
     */
    @Override
    public Vector<Dimensionless> getXhat() {
        return _child.getXhat().getTransformed(_TM).immutable();
    }

    /**
     * Return the in-plane direction of this curve perpendicular to the chord-wise
     * direction (the curve Y-direction).
     *
     * @return The in-plane direction of this curve perpendicular to Xhat.
     */
    @Override
    public Vector<Dimensionless> getYhat() {
        return _child.getYhat().getTransformed(_TM).immutable();
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of this object with any transformations or subranges removed
     *         (applied).
     */
    @Override
    public BasicCSTCurve copyToReal() {
        StackContext.enter();
        try {
            //  Get the transformed piece-parts.
            CSTClassFunction Cf = getCFunction();
            CSTShapeFunction Sf = getSFunction();
            Parameter<Length> DZTE = getOffsetTE();
            Parameter<Length> chord = getChord();
            Point origin = getOrigin();
            Vector<Dimensionless> xhat = getXhat();
            Vector<Dimensionless> yhat = getYhat();

            //  Create and return a new CST curve.
            BasicCSTCurve crv = BasicCSTCurve.newInstance(Cf, Sf, DZTE, origin, chord, xhat, yhat);
            return StackContext.outerCopy(crv);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return an immutable version of this CST curve.
     *
     * @return an immutable version of this curve.
     */
    @Override
    public BasicCSTCurve immutable() {
        return copyToReal();
    }

    /**
     * Return <code>true</code> if this curve contains valid and finite numerical
     * components. A value of <code>false</code> will be returned if any of the coordinate
     * values are NaN or Inf.
     *
     * @return true if this element contains valid and finite numerical values.
     */
    @Override
    public boolean isValid() {
        return _child.isValid();
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Calculate a point on the curve for the given parametric distance along the curve,
     * <code>p(s)</code>.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return the calculated point
     */
    @Override
    public Point getRealPoint(double s) {
        return _TM.transform(_child.getRealPoint(s));
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the curve for the given parametric distance along
     * the curve, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     Parametric distance to calculate derivatives for (0.0 to 1.0
     *              inclusive).
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(double s, int grade) {
        BasicCSTCurve trans = copyToReal();
        List<Vector<Length>> ders = trans.getSDerivatives(s, grade);
        BasicCSTCurve.recycle(trans);
        return ders;
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner of this
     * geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        BasicCSTCurve trans = copyToReal();
        Point minPoint = trans.getBoundsMin();
        BasicCSTCurve.recycle(trans);
        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner (e.g.: max
     * X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        BasicCSTCurve trans = copyToReal();
        Point maxPoint = trans.getBoundsMax();
        BasicCSTCurve.recycle(trans);
        return maxPoint;
    }

    /**
     * Return a new curve that is identical to this one, but with the parameterization
     * reversed. This does not change the origin or axis directions, but only the curve
     * parameterization; what was s=0 will become s=1, etc
     *
     * @return A new curve identical to this one, but with the parameterization reversed.
     */
    @Override
    public CSTCurveTrans reverse() {
        CSTCurveTrans crv = CSTCurveTrans.newInstance(_child.reverse(), _TM);
        return copyState(crv);
    }

    /**
     * Split this curve at the specified parametric position returning a list containing
     * two curves (a lower curve with smaller parametric positions than "s" and an upper
     * curve with larger parametric positions). The origin, chord length, and trailing
     * edge offset from chord-line will be identical for the the returned segments and
     * this curve.  The only change is in the parameterization.
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!).
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    @Override
    public GeomList<CSTCurve> splitAt(double s) {

        //  Split the parametric position curve.
        GeomList<CSTCurve> pCrvs = _child.splitAt(s);

        //  Create the output list.
        GeomList<CSTCurve> output = GeomList.newInstance();

        //  Create the lower curve.
        output.add(CSTCurveTrans.newInstance(pCrvs.getFirst(), _TM));
        output.add(CSTCurveTrans.newInstance(pCrvs.getLast(), _TM));

        GeomList.recycle(pCrvs);

        return output;
    }
    
    /**
     * Returns the unit in which the geometry of this curve is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this curve but stated in the specified unit.
     *
     * @param unit the length unit of the curve to be returned. May not be null.
     * @return an equivalent to this curve but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public CSTCurveTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        return CSTCurveTrans.newInstance(_child.to(unit), _TM);
    }

    /**
     * Return the equivalent of this curve converted to the specified number of physical
     * dimensions. This implementation will throw an exception if the specified dimension
     * is anything other than 3.
     *
     * @param newDim The dimension of the curve to return. MUST equal 3.
     * @return The equivalent of this curve converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other than 3.
     */
    @Override
    public CSTCurveTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Return a NURBS curve representation of this curve to within the specified
     * tolerance.
     *
     * @param tol The greatest possible difference between this curve and the NURBS
     *            representation returned.
     * @return A NURBS curve that represents this curve to within the specified tolerance.
     */
    @Override
    public NurbsCurve toNurbs(Parameter<Length> tol) {
        BasicCSTCurve crv = copyToReal();
        NurbsCurve ncrv = crv.toNurbs(tol);
        BasicCSTCurve.recycle(crv);
        return ncrv;
    }

    /**
     * Returns a copy of this CSTCurveTrans instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public CSTCurveTrans copy() {
        return copyOf(this);
    }

    /**
     * Compares this NurbsCurveTrans against the specified object for strict equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this transform is identical to that transform;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        CSTCurveTrans that = (CSTCurveTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Recycles a <code>CSTCurveTrans</code> instance immediately (on the stack when
     * executing in a StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(CSTCurveTrans instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<CSTCurveTrans> XML = new XMLFormat<CSTCurveTrans>(CSTCurveTrans.class) {

        @Override
        public CSTCurveTrans newInstance(Class<CSTCurveTrans> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(XMLFormat.InputElement xml, CSTCurveTrans obj) throws XMLStreamException {
            CSTCurve.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            CSTCurve child = xml.getNext();
            obj._child = child;

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(CSTCurveTrans obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            CSTCurve.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<CSTCurveTrans> FACTORY = new ObjectFactory<CSTCurveTrans>() {
        @Override
        protected CSTCurveTrans create() {
            return new CSTCurveTrans();
        }

        @Override
        protected void cleanup(CSTCurveTrans obj) {
            obj.reset();
            obj._TM = null;
            if (Objects.nonNull(obj._childChangeListener))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    @SuppressWarnings("unchecked")
    private static CSTCurveTrans copyOf(CSTCurveTrans original) {
        CSTCurveTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

    /**
     * Do not allow the default constructor to be used.
     */
    private CSTCurveTrans() { }

}
