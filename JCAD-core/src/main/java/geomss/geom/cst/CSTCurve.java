/**
 * CSTCurve -- Implementation and interface in common to all CST curves.
 *
 * Copyright (C) 2014-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom.cst;

import geomss.geom.*;
import jahuwaldt.js.param.Parameter;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javolution.util.FastMap;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * The interface and implementation in common to all Class-Shape-Transform (CST) planar
 * curves. This curve type is useful for defining smooth aircraft component shapes with
 * a minimum number of defining parameters.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: October 7, 2015
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class CSTCurve extends AbstractCurve<CSTCurve> {
    
    /**
     * Return the class function associated with this CST curve.
     * @return The class function associated with this CST curve.
     */
    public abstract CSTClassFunction getCFunction();
    
    /**
     * Return the shape function associated with this CST curve.
     * @return The shape function associated with this CST curve.
     */
    public abstract CSTShapeFunction getSFunction();
    
    /**
     * Return the offset of the trailing edge of the curve from the chord line.
     * @return The offset of the trailing edge of the curve from the chord line.
     */
    public abstract Parameter<Length> getOffsetTE();
    
    /**
     * Return the chord length of the curve. This defines the scale or size of the curve.
     * @return The chord length of the curve.
     */
    public abstract Parameter<Length> getChord();
    
    /**
     * Return the origin of the leading edge of this curve. This defines the s=0 location
     * of the curve in space.
     * @return The origin of the leading edge of this curve.
     */
    public abstract Point getOrigin();
    
    /**
     * Return the chord-wise direction of this curve (the curve X-direction).
     * @return The chord-wise direction of this curve.
     */
    public abstract Vector<Dimensionless> getXhat();
    
    /**
     * Return the in-plane direction of this curve perpendicular to the chord-wise
     * direction (the curve Y-direction).
     * @return The in-plane direction of this curve perpendicular to Xhat.
     */
    public abstract Vector<Dimensionless> getYhat();
    
    /**
     * Return an immutable version of this CST curve.
     *
     * @return an immutable version of this curve.
     */
    public abstract BasicCSTCurve immutable();
    
    
    /**
     * Returns the number of child-elements that make up this geometry element. This
     * implementation always returns 0.
     *
     * @return Always returns 0.
     */
    @Override
    public int size() {
        return 0;
    }

    /**
     * Return <code>true</code> if this curve is degenerate (i.e.: has length less than
     * the specified tolerance).
     *
     * @param tol The tolerance for determining if this curve is degenerate. May not be
     *            null.
     * @return true if this curve is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        return !getChord().isLargerThan(requireNonNull(tol));
    }
    
    /**
     * Return <code>true</code> if this curve is planar or <code>false</code> if it is
     * not. BasicCSTCurve objects are always planar, so this always returns true.
     *
     * @param tol The geometric tolerance to use in determining if the curve is planar.
     *            Ignored by this implementation.
     * @return Always returns true.
     */
    @Override
    public boolean isPlanar(Parameter<Length> tol) {
        return true;
    }
    
    /**
     * Returns transformed version of this element. The returned object implements
     * {@link geomss.geom.GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the specified
     *         transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public CSTCurveTrans getTransformed(GTransform transform) {
        return CSTCurveTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<CSTCurve> XML = new XMLFormat<CSTCurve>(CSTCurve.class) {

        @Override
        public void read(XMLFormat.InputElement xml, CSTCurve obj) throws XMLStreamException {
            String name = xml.getAttribute("name", (String)null);
            if (nonNull(name))
                obj.setName(name);
            FastMap userData = xml.get("UserData", FastMap.class);
            if (nonNull(userData)) {
                obj.putAllUserData(userData);
            }
        }

        @Override
        public void write(CSTCurve obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            String name = obj.getName();
            if (nonNull(name))
                xml.setAttribute("name", name);
            FastMap userData = (FastMap)obj.getAllUserData();
            if (!userData.isEmpty())
                xml.add(userData, "UserData", FastMap.class);
        }
    };
    
}
