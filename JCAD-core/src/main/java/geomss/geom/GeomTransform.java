/*
 *   GeomTransform  -- Interface in common to all geometry transformation elements.
 *
 *   Copyright (C) 2009-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

/**
 * Represents a transformation element that applies a {@link GTransform transform} to a
 * child {@link Transformable} object. Changes made to the child object will generally be
 * reflected in the output of the transform object.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: April 30, 2009
 * @version August 31, 2015
 *
 * @param <T> The sub-type of this GeomTransform object.
 */
public interface GeomTransform<T extends Transformable> extends Transformable<T> {

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation represented by this transformation element.
     */
    public GTransform getTransform();

    /**
     * Returns the total transformation represented by an entire chain of GeomTransform
     * objects below this one.
     *
     * @return The total transformation represented by an entire chain of GeomTransform
     *         objects below this one.
     */
    public GTransform getTotalTransform();

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not be
     *                  <code>null</code>).
     */
    public void setTransform(GTransform transform);

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this transform element.
     */
    public T getChild();

    /**
     * Return a copy of the child object transformed by this transformation.
     */
    @Override
    public T copyToReal();

}
