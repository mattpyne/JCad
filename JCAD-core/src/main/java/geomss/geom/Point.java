/**
 * Point -- Holds the floating point coordinates of a point in nD space.
 *
 * Copyright (C) 2002-2017, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Coordinate3D;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A container that holds the coordinates of a point in n-dimensional space.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: December 11, 1999
 * @version October 14, 2017
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class Point extends GeomPoint implements ValueType {

    /**
     * The coordinate values are stored in this array.
     */
    private transient double[] _data;

    /**
     * The number of dimensions in the data.
     */
    private int _numDims;

    /**
     * Holds the unit of the coordinates.
     */
    private Unit<Length> _unit;

    /**
     * Returns a {@link Point} instance of the specified dimension with zero meters for
     * each coordinate value.
     *
     * @param dim the physical dimension of the point to create.
     * @return the point having the specified dimension and zero meters for values.
     */
    public static Point newInstance(int dim) {
        if (dim < 1)
            throw new IllegalArgumentException(RESOURCES.getString("pointDimGT0"));
        Point V = Point.newInstancePvt(dim, SI.METER);

        for (int i = 0; i < dim; ++i)
            V._data[i] = 0;

        return V;
    }

    /**
     * Returns a {@link Point} instance of the specified dimension and units with zero for
     * each coordinate value.
     *
     * @param dim  the physical dimension of the point to create.
     * @param unit The unit for the point to create. May not be null.
     * @return the point having the specified dimension & units and zero for values.
     */
    public static Point newInstance(int dim, Unit<Length> unit) {
        if (dim < 1)
            throw new IllegalArgumentException(RESOURCES.getString("pointDimGT0"));
        Point V = Point.newInstancePvt(dim, requireNonNull(unit));

        for (int i = 0; i < dim; ++i)
            V._data[i] = 0;

        return V;
    }

    /**
     * Returns a {@link Point} instance holding the specified <code>double</code> value or
     * values stated in meters.
     *
     * @param x the coordinate values stated in meters. May not be null.
     * @return the point having the specified value.
     */
    public static Point valueOf(double... x) {
        return valueOf(SI.METER, x);
    }

    /**
     * Returns a 1D {@link Point} instance holding the specified <code>double</code>
     * values stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified value.
     */
    public static Point valueOf(double x, Unit<Length> unit) {
        Point V = Point.newInstancePvt(1, unit);

        V._data[0] = x;

        return V;
    }

    /**
     * Returns a 2D {@link Point} instance holding the specified <code>double</code> value
     * stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(double x, double y, Unit<Length> unit) {
        Point V = Point.newInstancePvt(2, unit);

        V._data[0] = x;
        V._data[1] = y;

        return V;
    }

    /**
     * Returns a 3D {@link Point} instance holding the specified <code>double</code>
     * values stated in the specified units.
     *
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param z    the z value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(double x, double y, double z, Unit<Length> unit) {
        Point V = Point.newInstancePvt(3, unit);

        V._data[0] = x;
        V._data[1] = y;
        V._data[2] = z;

        return V;
    }

    /**
     * Returns a {@link Point} instance holding the specified <code>double</code> values
     * stated in the specified units.
     *
     * @param unit   the length unit in which the coordinates are stated. May not be null.
     * @param values the list of values stated in the specified unit. May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(Unit<Length> unit, double... values) {
        int dim = values.length;
        Point V = Point.newInstancePvt(dim, unit);

        for (int i = 0; i < dim; ++i)
            V._data[i] = values[i];

        return V;
    }

    /**
     * Returns a {@link Point} instance holding the specified <code>Parameter</code>
     * values. All the values are converted to the same units as the 1st value.
     *
     * @param values The list of values to be stored.  May not be null.
     * @return the point having the specified values in the units of the 1st value.
     */
    public static Point valueOf(Parameter<Length>... values) {
        Unit<Length> unit = values[0].getUnit();
        int size = values.length;

        Point V = Point.newInstancePvt(size, unit);

        for (int i = 0; i < size; ++i) {
            Parameter<Length> param = values[i];
            V._data[i] = param.getValue(unit);
        }

        return V;
    }

    /**
     * Returns a {@link Point} instance holding the specified <code>Parameter</code>
     * values. All the values are converted to the same units as the 1st value.
     *
     * @param values The list of values to be stored. May not be null and must have at
     *               least one element..
     * @return the point having the specified values in the units of the 1st value.
     */
    public static Point valueOf(List<Parameter<Length>> values) {
        Unit<Length> unit = values.get(0).getUnit();
        int size = values.size();

        Point V = Point.newInstancePvt(size, unit);

        for (int i = 0; i < size; ++i) {
            Parameter<Length> param = values.get(i);
            V._data[i] = param.getValue(unit);
        }

        return V;
    }

    /**
     * Returns a {@link Point} instance holding the specified
     * <code>@link jahuwaldt.js.param.Coordinate3D Coordinate3D</code> values.
     *
     * @param coord The {@link jahuwaldt.js.param.Coordinate3D Coordinate3D} to be stored.
     *              May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(Coordinate3D<Length> coord) {
        int dim = coord.getDimension();

        Point P = Point.newInstancePvt(dim, coord.getUnit());
        for (int i = 0; i < dim; ++i)
            P._data[i] = coord.getValue(i);

        return P;
    }

    /**
     * Returns a {@link Point} instance containing the specified vector of Float64 values
     * stated in the specified units.
     *
     * @param vector the vector of Float64 values stated in the specified unit. May not be
     *               null.
     * @param unit   the unit in which the values are stated. May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(org.jscience.mathematics.vector.Vector<Float64> vector, Unit<Length> unit) {
        int dim = vector.getDimension();

        Point V = Point.newInstancePvt(dim, unit);
        for (int i = 0; i < dim; ++i)
            V._data[i] = vector.get(i).doubleValue();

        return V;
    }

    /**
     * Returns a {@link Point} instance containing the specified vector of Parameter
     * values with length units. All the values are converted to the same units as the 1st
     * value.
     *
     * @param <Q>    The Quantity (type of unit) for this Point data. Must be "Length" or
     *               "Dimensionless".
     * @param vector The vector of Parameter values stated in length or dimensionless
     *               units. If in dimensionless units, the values are interpreted as being
     *               in meters. May not be null.
     * @return the point having the specified values.
     * @throws ConversionException if the input vector is not in Length (or Dimensionless)
     * units.
     */
    public static <Q extends Quantity> Point valueOf(org.jscience.mathematics.vector.Vector<Parameter<Q>> vector) {

        Unit unit = vector.get(0).getUnit();
        if (unit != Dimensionless.UNIT && !Length.UNIT.isCompatible(unit))
            throw new ConversionException(
                    MessageFormat.format(RESOURCES.getString("incompatibleUnits"),
                            "input vector", "length"));

        int size = vector.getDimension();
        Point P = Point.newInstancePvt(size, (unit == Dimensionless.UNIT ? SI.METER : unit));
        for (int i = 0; i < size; ++i) {
            Parameter param = vector.get(i);
            P._data[i] = param.getValue(unit);
        }

        return P;
    }

    /**
     * Returns a {@link Point} instance that represents the direction elements of the
     * specified GeomVector given in length or dimensionless units. If the user wishes for
     * the point to represent the end or tip of the vehicle they should use the following:
     * <code>Point p = Point.valueOf(vector).plus(vector.getOrigin());</code>
     *
     * @param vector the GeomVector stated in length or dimensionless units. If in
     *               dimensionless units, the values are interpreted as being in meters.
     *               May not be null.
     * @return the point having the elements of the vector.
     * @throws ConversionException if the input vector is not in length (or Dimensionless)
     * units.
     */
    public static Point valueOf(GeomVector<?> vector) {

        Unit unit = vector.getUnit();
        if (unit != Dimensionless.UNIT && !Length.UNIT.isCompatible(unit))
            throw new ConversionException(
                    MessageFormat.format(RESOURCES.getString("incompatibleUnits"),
                            "input vector", "length"));

        int size = vector.getPhyDimension();
        Point P = Point.newInstancePvt(size, (unit == Dimensionless.UNIT ? SI.METER : unit));
        for (int i = 0; i < size; ++i)
            P._data[i] = vector.getValue(i);

        return P;
    }

    /**
     * Returns a {@link Point} instance containing the specified GeomPoint's data.
     *
     * @param point the GeomPoint to be copied into a new Point. May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(GeomPoint point) {
        if (point instanceof Point)
            return (Point)point;

        int dim = point.getPhyDimension();

        Point P = Point.newInstancePvt(dim, point.getUnit());
        for (int i = 0; i < dim; ++i)
            P._data[i] = point.getValue(i);

        return P;
    }

    /**
     * Returns a {@link Point} instance containing the specified Point's data.
     *
     * @param point the Point to be copied into a new Point. May not be null.
     * @return the point having the specified values.
     */
    public static Point valueOf(Point point) {
        return copyOf(point);
    }

    /**
     * Return an immutable version of this point. This implementation simply returns this
     * Point instance.
     *
     * @return A reference to this Point (which is immutable).
     */
    @Override
    public Point immutable() {
        return this;
    }

    /**
     * Return a mutable copy of this point.
     *
     * @return A mutable copy of this Point object.
     */
    public MutablePoint mutable() {
        return MutablePoint.valueOf(this);
    }

    /**
     * Returns the number of physical dimensions of the geometry element.
     *
     * @return The number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _numDims;
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * this point's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; dimension())</code>
     */
    @Override
    public double getValue(int i) {
        return _data[i];
    }

    /**
     * Returns the value of a coordinate in this point as a <code>double</code>, stated in
     * the specified unit.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; dimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Length> unit) {
        double value = _data[i];
        if (_unit == unit || unit.equals(_unit))
            return value;
        UnitConverter cvtr = Parameter.converterOf(_unit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            return value;
        }
        return cvtr.convert(value);
    }

    /**
     * Returns the square of the Euclidean norm, magnitude, or length value of the vector
     * from the origin to this point (the dot product of the origin-to-this-point vector
     * and itself). This is slightly faster than calling <code>normValue</code> if the
     * squared value is all that is needed.
     *
     * @return <code>this.normSq().getValue()</code>.
     * @see #normValue() 
     */
    @Override
    public double normSqValue() {
        double[] data = _data;
        double s2 = data[0];
        s2 *= s2;
        for (int i = 1; i < _numDims; ++i) {
            double vi = data[i];
            s2 += vi * vi;
        }
        return s2;
    }

    /**
     * Returns the sum of this point with the one specified. The unit of the output point
     * will be the units of this point.
     *
     * @param that the point to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point plus(GeomPoint that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit<Length> unit = _unit;

        Point point = Point.newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = thisData[i] + that.getValue(i, unit);

        return point;
    }

    /**
     * Adds the specified parameter to each component of this point. The unit of the
     * output point will be the units of this point.
     *
     * @param that the parameter to be added to each component of this point. May not be
     *             null.
     * @return <code>this + that</code>.
     */
    @Override
    public Point plus(Parameter<Length> that) {
        int numDims = _numDims;
        Unit<Length> unit = _unit;

        //  Convert input parameter to the units of this point.
        double thatValue = that.getValue(unit);

        Point point = Point.newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = thisData[i] + thatValue;

        return point;
    }

    /**
     * Returns the difference between this point and the one specified. The unit of the
     * output point will be the units of this point.
     *
     * @param that the point to be subtracted from this point. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if point dimensions are different.
     */
    @Override
    public Point minus(GeomPoint that) {
        int numDims = _numDims;
        if (that.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        Unit<Length> unit = _unit;

        Point point = Point.newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = thisData[i] - that.getValue(i, unit);

        return point;
    }

    /**
     * Subtracts the specified parameter from each component of this point. The unit of
     * the output point will be the units of this point.
     *
     * @param that the parameter to be subtracted from each component of this point. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Point minus(Parameter<Length> that) {
        int numDims = _numDims;
        Unit<Length> unit = _unit;

        //  Convert input parameter to the units of this point.
        double thatValue = that.getValue(unit);

        Point point = Point.newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = thisData[i] - thatValue;

        return point;
    }

    /**
     * Returns the negation of this point (all the values of each dimension negated).
     *
     * @return <code>-this</code>
     */
    @Override
    public Point opposite() {
        int numDims = _numDims;

        Point point = Point.newInstancePvt(numDims, _unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = -thisData[i];

        return point;
    }

    /**
     * Returns the product of this point with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Point times(double k) {
        int numDims = _numDims;

        Point point = Point.newInstancePvt(numDims, _unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = thisData[i] * k;

        return point;
    }

    /**
     * Returns a copy of this Point instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public Point copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public Point copyToReal() {
        return copy();
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this point are stated in.
     *
     * @return The unit in which this Point is stated.
     */
    @Override
    public final Unit<Length> getUnit() {
        return _unit;
    }

    /**
     * Returns the equivalent to this point but stated in the specified unit.
     *
     * @param unit the length unit of the point to be returned. May not be null.
     * @return an equivalent of this point but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public Point to(Unit<Length> unit) throws ConversionException {
        if (_unit == unit || unit.equals(_unit))
            return this;

        UnitConverter cvtr = Parameter.converterOf(_unit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            return this;
        }

        int numDims = _numDims;
        Point point = Point.newInstancePvt(numDims, unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = cvtr.convert(thisData[i]);

        return point;
    }

    /**
     * Return the equivalent of this point converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the point to return.
     * @return A copy of this point converted to the new dimensions.
     */
    @Override
    public Point toDimension(int newDim) {
        if (newDim < 1)
            throw new IllegalArgumentException(RESOURCES.getString("pointDimGT0"));
        int thisDim = this.getPhyDimension();
        if (newDim == thisDim)
            return this;

        int numDims = newDim;
        if (newDim > thisDim)
            numDims = thisDim;

        Point point = Point.newInstancePvt(newDim, _unit);
        double[] thisData = this._data;
        for (int i = 0; i < numDims; ++i)
            point._data[i] = thisData[i];
        for (int i = numDims; i < newDim; ++i)
            point._data[i] = 0;

        return point;
    }

    /**
     * Returns the values stored in this point, stated in this point's
     * {@link #getUnit unit}, as a Float64Vector.
     *
     * @return A Float64Vector containing the values stored in this Point in the current
     *         units.
     */
    @Override
    public final Float64Vector toFloat64Vector() {
        FastTable<Float64> table = FastTable.newInstance();
        for (int i = 0; i < _numDims; ++i)
            table.add(Float64.valueOf(_data[i]));

        Float64Vector f64 = Float64Vector.valueOf(table);

        FastTable.recycle(table);
        return f64;
    }

    /**
     * Compares this Point against the specified object for strict equality (same values
     * and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this point is identical to that point;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        final Point that = (Point)obj;
        if (this._numDims != that._numDims)
            return false;
        if (!this._unit.equals(that._unit))
            return false;
        for (int i = 0; i < _numDims; ++i)
            if (this._data[i] != that._data[i])
                return false;

        return super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _unit.hashCode();
        hash = hash * 31 + var_code;

        for (int i = 0; i < _numDims; ++i)
            hash = hash * 31 + makeVarCode(_data[i]);

        return hash;
    }

    private static int makeVarCode(double value) {
        long bits = Double.doubleToLongBits(value);
        int var_code = (int)(bits ^ (bits >>> 32));
        return var_code;
    }

    /**
     * During serialization, this will write out the point data as a simple series of
     * <code>double</code> values. This method is ONLY called by the Java Serialization
     * mechanism and should not otherwise be used.
     */
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {

        // Call the default write object method.
        out.defaultWriteObject();

        //  Write out the number of elements.
        int size = _numDims;
        out.writeInt(size);

        //  Write out the coordinate values.
        for (int i = 0; i < size; ++i)
            out.writeDouble(_data[i]);

    }

    /**
     * During de-serialization, this will handle the reconstruction of the point data.
     * This method is ONLY called by the Java Serialization mechanism and should not
     * otherwise be used.
     */
    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {

        // Call the default read object method.
        in.defaultReadObject();

        //  Read in the number of elements.
        int size = in.readInt();

        //  Read in the coordinate values.
        _data = ArrayFactory.DOUBLES_FACTORY.array(size);
        for (int i = 0; i < size; ++i) {
            double value = in.readDouble();
            _data[i] = value;
        }

    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<Point> XML = new XMLFormat<Point>(Point.class) {

        @Override
        public Point newInstance(Class<Point> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, Point obj) throws XMLStreamException {
            Unit unit = Unit.valueOf(xml.getAttribute("unit"));
            if (!Length.UNIT.isCompatible(unit))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("incompatibleUnits"),
                                "Point", "length"));
            obj._unit = unit;

            GeomPoint.XML.read(xml, obj);     // Call parent read.

            FastTable<Float64> valueList = FastTable.newInstance();
            while (xml.hasNext()) {
                Float64 value = xml.getNext();
                valueList.add(value);
            }
            int numDims = valueList.size();
            obj._numDims = numDims;
            obj._data = ArrayFactory.DOUBLES_FACTORY.array(numDims);
            for (int i = 0; i < numDims; ++i)
                obj._data[i] = valueList.get(i).doubleValue();

        }

        @Override
        public void write(Point obj, OutputElement xml) throws XMLStreamException {
            xml.setAttribute("unit", obj._unit.toString());

            GeomPoint.XML.write(obj, xml);    // Call parent write.

            int size = obj._numDims;
            for (int i = 0; i < size; ++i)
                xml.add(Float64.valueOf(obj._data[i]));

        }
    };

    /**
     * Allocate a recyclable array that can contain Point objects using factory methods.
     * <p>
     * WARNING: The array returned may <I>not</I> be zeroed. Any object references in the
     * returned array must be assumed to be invalid. Also, the returned array may be
     * larger than the requested size! The array returned by this method can be recycled
     * by recycleArray().
     * </p>
     *
     * @param size The minimum number of elements for the returned array to contain.
     * @return An array that can contain Point objects allocated using factory methods.
     * @see #recycleArray
     */
    public static Point[] allocateArray(int size) {
        return POINTARRAY_FACTORY.array(size);
    }

    /**
     * Recycle an array of Point objects that was created by Point.allocateArray().
     *
     * @param arr The array to be recycled. The array must have been created by this the
     *            allocateArray() method!
     * @see #allocateArray
     */
    public static void recycleArray(Point[] arr) {
        POINTARRAY_FACTORY.recycle(arr);
    }
    
    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private static ArrayFactory<Point[]> POINTARRAY_FACTORY = new ArrayFactory<Point[]>() {
        @Override
        protected Point[] create(int size) {
            return new Point[size];
        }
    };

    private Point() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<Point> FACTORY = new ObjectFactory<Point>() {
        @Override
        protected Point create() {
            return new Point();
        }

        @Override
        protected void cleanup(Point obj) {
            obj.reset();
        }
    };

    @SuppressWarnings("unchecked")
    private static Point newInstancePvt(int numDims, Unit<Length> unit) {
        Point measure = FACTORY.object();
        measure._unit = requireNonNull(unit);
        measure._numDims = numDims;
        measure._data = ArrayFactory.DOUBLES_FACTORY.array(numDims);
        return measure;
    }

    @SuppressWarnings("unchecked")
    private static Point copyOf(Point original) {
        Point measure = newInstancePvt(original._numDims, original._unit);
        System.arraycopy(original._data, 0, measure._data, 0, original._numDims);
        original.copyState(measure);
        return measure;
    }

    /**
     * Tests the methods in this class.
     *
     * @param args Command-line arguments (not used).
     */
    public static void main(String args[]) {
        System.out.println("Testing Point:  test = result [correct result]");

        Point p1 = Point.valueOf(1, 2, 3, NonSI.FOOT);
        System.out.println("p1 = " + p1);
        System.out.println("  converted to m       = " + p1.to(SI.METER) + " [{0.3048 m, 0.6096 m, 0.9144 m}]");
        System.out.println("  p1.norm()            = " + p1.norm() + " [3.74165738677394 ft]");
        Point p2 = Point.valueOf(1, 1, 1, SI.METER);
        System.out.println("p2 = " + p2);

        Point p3 = Point.valueOf(1, 0.5, 1, SI.METER);
        System.out.println("p3 = " + p3);
        System.out.println("  p1.min(p3) = " + p1.min(p3) + " [1.0 ft, 1.64041994750656 ft, 3.0 ft]");
        System.out.println("  p1.max(p3) = " + p1.max(p3) + " [3.28083989501312 ft, 2.0 ft, 3.28083989501312 ft]");

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new GeomXMLBinding();

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(p1, "Point", Point.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
