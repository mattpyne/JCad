/**
 * AbstractCurve -- Represents the implementation in common to all curves in nD space.
 *
 * Copyright (C) 2009-2017, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.*;
import static jahuwaldt.tools.math.MathTools.isApproxEqual;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.MathLib;
import javolution.util.FastList;
import javolution.util.FastTable;

/**
 * The interface and implementation in common to all curves.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 15, 2009
 * @version October 20, 2017
 *
 * @param <T> The sub-type of this AbstractCurve.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public abstract class AbstractCurve<T extends AbstractCurve> extends AbstractGeomElement<T> implements Curve<T> {

    /*
     *  Reference 1:  Michael E. Mortenson, "Geometric Modeling, Third Edition", ISBN:9780831132989, 2008.
     */
    
    /**
     * Generic/default tolerance for use in root finders, etc.
     */
    public static final double GTOL = 1e-6;

    /**
     * Tolerance allowed on parametric spacing being == 0 or == 1.
     */
    protected static final double TOL_S = Parameter.EPS;

    /**
     * Returns the number of parametric dimensions of the geometry element. This
     * implementation always returns 1.
     */
    @Override
    public int getParDimension() {
        return 1;
    }

    /**
     * Checks the input parameter for validity (in the range 0 to 1 for example) and
     * throws an exception if it is not valid.
     *
     * @param s parametric distance (0.0 to 1.0 inclusive).
     * @throws IllegalArgumentException if there is any problem with the parameter value.
     */
    protected void validateParameter(double s) {
        if (s < -TOL_S || s > 1 + TOL_S)
            throw new IllegalArgumentException(
                    MessageFormat.format(RESOURCES.getString("crvInvalidSValue"), s));
    }

    /**
     * Checks the input parameter for validity (in the range 0 to 1 for example) and
     * throws an exception if it is not valid.
     *
     * @param s parametric distance. Must be a 1-dimensional point with a value in the
     *          range 0 to 1.0. Units are ignored.
     * @throws IllegalArgumentException if there is any problem with the parameter value.
     */
    protected void validateParameter(GeomPoint s) {
        if (isNull(s) || s.getPhyDimension() != 1)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("invalidParamDim"), "Curve", 1));
        validateParameter(s.getValue(0));
    }

    /**
     * If the input parameter is within tol of 0 or 1, or outside the bounds of 0 or 1,
     * then the output value will be set to exactly 0 or 1. Otherwise the input value will
     * be returned.
     *
     * @param s The parameter to check for approximate end values.
     * @return The input value or 0 and 1 if the input value is within tolerance of the
     *         ends.
     */
    protected static final double roundParNearEnds(double s) {
        return (s < TOL_S ? 0. : (s > 1. - TOL_S ? 1. : s));
    }
    
    /**
     * Return true if the input parameter is within tolerance of or less than the start of the
     * parameter range (s &le; tol).
     *
     * @param s   The parameter to check.
     * @param tol The tolerance to use for the check.
     * @return true if the parameter is within tolerance of zero.
     */
    protected static final boolean parNearStart(double s, double tol) {
        return s <= tol;
    }

    /**
     * Return true if the input parameter is within tolerance of or greater than the end
     * of the parameter range (s &ge; 1.0 - tol).
     *
     * @param s   The parameter to check.
     * @param tol The tolerance to use for the check.
     * @return true if the parameter is within tolerance of 1.0.
     */
    protected static final boolean parNearEnd(double s, double tol) {
        return s >= 1.0 - tol;
    }

    /**
     * Return true if the input parameter is within tolerance of or outside of the bounds
     * of either the end of the parameter range (s &le; tol || s &ge; 1.0 - tol).
     *
     * @param s   The parameter to check.
     * @param tol The tolerance to use for the check.
     * @return true if the parameter is within tolerance of 0.0 or 1.0.
     */
    protected static final boolean parNearEnds(double s, double tol) {
        return s <= tol || s >= 1.0 - tol;
    }

    /**
     * Split this curve at the specified parametric position returning a list containing
     * two curves (a lower curve with smaller parametric positions than "s" and an upper
     * curve with larger parametric positions).
     *
     * @param s The parametric position where this curve should be split (must not be 0 or
     *          1!). Must be a 1-dimensional point with a value in the range 0 to 1.0,
     *          non-inclusive. Units are ignored.
     * @return A list containing two curves: 0 == the lower curve, 1 == the upper curve.
     */
    @Override
    public GeomList<T> splitAt(GeomPoint s) {
        return splitAt(s.getValue(0));
    }

    /**
     * Return a subrange point on the curve for the given parametric distance along the
     * curve.
     *
     * @param s parametric distance to calculate a point for (0.0 to 1.0 inclusive).
     * @return The subrange point or <code>null</code> if the parameter is not in a valid
     *         range.
     */
    @Override
    public SubrangePoint getPoint(double s) {
        validateParameter(s);
        s = roundParNearEnds(s);
        return SubrangePoint.newInstance(this, Point.valueOf(s));
    }

    /**
     * Return a subrange point on the curve for the given parametric distance along the
     * curve.
     *
     * @param s parametric distance to calculate a point for. Must be a 1-dimensional
     *          point with a value in the range 0 to 1.0. Units are ignored.
     * @return The subrange point
     */
    @Override
    public SubrangePoint getPoint(GeomPoint s) {
        validateParameter(s);
        return SubrangePoint.newInstance(this, s);
    }

    /**
     * Calculate a point on the curve for the given parametric distance along the curve.
     *
     * @param s parametric distance to calculate a point for. Must be a 1-dimensional
     *          point with a value in the range 0 to 1.0. Units are ignored.
     * @return calculated point
     */
    @Override
    public Point getRealPoint(GeomPoint s) {
        validateParameter(s);
        return getRealPoint(s.getValue(0));
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric position(s) on a parametric object for the given parametric
     * position on the object, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     parametric position to calculate the derivatives for. Must match the
     *              parametric dimension of this parametric surface and have each value in
     *              the range 0 to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of lists of derivatives (one list for each parametric dimension).
     *         Each list contains derivatives up to the specified grade at the specified
     *         parametric position. Example: for a surface list element 0 is the array of
     *         derivatives in the 1st parametric dimension (s), then 2nd list element is
     *         the array of derivatives in the 2nd parametric dimension (t).
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public List<List<Vector<Length>>> getDerivatives(GeomPoint s, int grade) {
        validateParameter(s);

        List<List<Vector<Length>>> list = FastTable.newInstance();
        list.add(getSDerivatives(s.getValue(0), grade));

        return list;
    }

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric distance on the curve for the given parametric distance along
     * the curve, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param s     parametric distance to calculate the derivatives for. Must be a
     *              1-dimensional point with a value in the range 0 to 1.0. Units are
     *              ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     *              2=2nd derivative, etc)
     * @return A list of derivatives up to the specified grade of the curve at the
     *         specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public List<Vector<Length>> getSDerivatives(GeomPoint s, int grade) {
        validateParameter(s);
        return getSDerivatives(s.getValue(0), grade);
    }

    /**
     * Calculate a derivative with respect to parametric distance of the given grade on
     * the curve for the given parametric distance along the curve,
     * <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s)/ds</code>; <br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s)/d^2s</code>; etc.
     * </p>
     * <p>
     * Note: Cartesian space derivatives can be calculated as follows:
     * <pre>
     *      derivative = curve.getSDerivative(s, 1);
     *      dy/dx = (dy/ds)/(dx/ds) = derivative.getValue(1)/derivative.getValue(0),
     *      dy/dz = (dy/ds)/(dz/ds) = derivative.getValue(1)/derivative.getValue(2),
     *      etc
     * </pre>
     * </p>
     *
     * @param s     Parametric distance to calculate a derivative for (0.0 to 1.0
     *              inclusive).
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     *              derivative, etc)
     * @return The specified derivative of the curve at the specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0.
     */
    @Override
    public Vector<Length> getSDerivative(double s, int grade) {
        validateParameter(s);
        StackContext.enter();
        try {

            List<Vector<Length>> ders = getSDerivatives(s, grade);
            Vector<Length> derivative = ders.get(grade);
            return StackContext.outerCopy(derivative);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the tangent vector for the given parametric distance along the curve. This
     * vector contains the normalized 1st derivative of the curve with respect to
     * parametric distance in each component direction: dx/ds/|dp(s)/ds|,
     * dy/ds/|dp(s)/ds|, dz/ds/|dp(s)/ds|, etc.
     * <p>
     * Note: Cartesian space derivatives can be calculated as follows:
     * <pre>
     *      tangent = curve.getTangent(s);
     *      dy/dx = (dy/ds)/(dx/ds) = tangent.getValue(1)/tangent.getValue(0),
     *      dy/dz = (dy/ds)/(dz/ds) = tangent.getValue(1)/tangent.getValue(2),
     *      etc
     * </pre>
     * </p>
     *
     * @param s Parametric distance to calculate a tangent vector for (0.0 to 1.0
     *          inclusive).
     * @return The tangent vector of the curve at the specified parametric position.
     */
    @Override
    public Vector<Dimensionless> getTangent(double s) {
        validateParameter(s);
        StackContext.enter();
        try {

            //  Ref. 1, Eqn. 12.1.
            List<Vector<Length>> ders = getSDerivatives(s, 1);
            Vector<Dimensionless> derivative = ders.get(1).toUnitVector();

            return StackContext.outerCopy(derivative);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the tangent vector for the given parametric distance along the curve. This
     * vector contains the normalized 1st derivative of the curve with respect to
     * parametric distance in each component direction: dx/ds/|dp(s)/ds|,
     * dy/ds/|dp(s)/ds|, dz/ds/|dp(s)/ds|, etc.
     * <p>
     * Note: Cartesian space derivatives can be calculated as follows:
     * <pre>
     *      tangent = curve.getTangent(s);
     *      dy/dx = (dy/ds)/(dx/ds) = tangent.getValue(1)/tangent.getValue(0),
     *      dy/dz = (dy/ds)/(dz/ds) = tangent.getValue(1)/tangent.getValue(2),
     *      etc
     * </pre>
     * </p>
     *
     * @param s Parametric distance to calculate a tangent vector for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The tangent vector of the curve at the specified parametric position.
     */
    @Override
    public Vector<Dimensionless> getTangent(GeomPoint s) {
        validateParameter(s);
        return getTangent(s.getValue(0));
    }

    /**
     * Return the principal normal vector for the given parametric distance,
     * <code>s</code>, along the curve. This vector is normal to the curve, lies in the
     * normal plane (is perpendicular to the tangent vector), and points toward the center
     * of curvature at the specified point.
     *
     * @param s Parametric distance to calculate the principal normal vector for (0.0 to
     *          1.0 inclusive).
     * @return The principal normal vector of the curve at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment -- this
     * means there are an infinite number of possible principal normal vectors).
     */
    @Override
    public Vector<Dimensionless> getPrincipalNormal(double s) {
        validateParameter(s);

        StackContext.enter();
        try {
            //  Ref. 1, Eqn. 12.15.
            List<Vector<Length>> ders = getSDerivatives(s, 2);
            Vector<Length> piu = ders.get(1);
            Vector<Length> piuu = ders.get(2);

            if (piuu.mag().isApproxZero())
                throw new IllegalArgumentException(RESOURCES.getString("undefinedPrincipleNormal"));

            //  ni = k/|k|;  k = piuu - piuu DOT piu / |piu|^2 * piu;
            Parameter piuMag = piu.mag();
            Parameter piuuDotpiu = piuu.dot(piu);
            Vector k = piu.times(piuuDotpiu.divide(piuMag).divide(piuMag));
            k = piuu.minus(k);

            return StackContext.outerCopy(k.toUnitVector());

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the principal normal vector for the given parametric distance,
     * <code>s</code>, along the curve. This vector is normal to the curve, lies in the
     * normal plane (is perpendicular to the tangent vector), and points toward the center
     * of curvature at the specified point.
     *
     * @param s Parametric distance to calculate the principal normal vector for. Must be
     *          a 1-dimensional point with a value in the range 0 to 1.0. Units are
     *          ignored.
     * @return The principal normal vector of the curve at the specified parametric
     *         position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment -- this
     * means there are an infinite number of possible principal normal vectors).
     */
    @Override
    public Vector<Dimensionless> getPrincipalNormal(GeomPoint s) {
        validateParameter(s);
        return getPrincipalNormal(s.getValue(0));
    }

    /**
     * Return the binormal vector for the given parametric distance along the curve. This
     * vector is normal to the curve at <code>s</code>, lies in the normal plane (is
     * perpendicular to the tangent vector), and is perpendicular to the principal normal
     * vector. Together, the tangent vector, principal normal vector, and binormal vector
     * form a useful orthogonal frame.
     *
     * @param s Parametric distance to calculate the binormal vector for (0.0 to 1.0
     *          inclusive).
     * @return The binormal vector of the curve at the specified parametric position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment).
     * @throws DimensionException if the curve does not have at least 3 physical
     * dimensions.
     */
    @Override
    public Vector<Dimensionless> getBinormal(double s) throws DimensionException {
        validateParameter(s);
        if (getPhyDimension() < 3)
            throw new DimensionException(RESOURCES.getString("undefinedBinormal"));

        StackContext.enter();
        try {
            
            //  Ref. 1, Eqn. 12.18.
            //  bi = ti X ni;
            Vector ni = getPrincipalNormal(s);
            Vector ti = getTangent(s);
            Vector bi = ti.cross(ni);
            return StackContext.outerCopy(bi);
            
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the binormal vector for the given parametric distance along the curve. This
     * vector is normal to the curve at <code>s</code>, lies in the normal plane (is
     * perpendicular to the tangent vector), and is perpendicular to the principal normal
     * vector. Together, the tangent vector, principal normal vector, and binormal vector
     * form a useful orthogonal frame.
     *
     * @param s Parametric distance to calculate the binormal vector for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The binormal vector of the curve at the specified parametric position.
     * @throws IllegalArgumentException if the curve does not have a 2nd derivative with
     * respect to <code>s</code> (if the curve is locally a straight line segment).
     * @throws DimensionException if the curve does not have at least 3 physical
     * dimensions.
     */
    @Override
    public Vector<Dimensionless> getBinormal(GeomPoint s) throws DimensionException {
        validateParameter(s);
        return getBinormal(s.getValue(0));
    }

    /**
     * Return the curvature (kappa = 1/rho; where rho = the radius of curvature) of the
     * curve at the parametric position <code>s</code>. The curvature vector (vector from
     * p(s) to the center of curvature) can be constructed from:  <code>k = rhoi*ni</code>
     * or <code>k = curve.getPrincipalNormal(s).divide(curve.getCurvature(s));</code>
     *
     * @param s Parametric distance to calculate the curvature for (0.0 to 1.0 inclusive).
     * @return The curvature of the curve at the specified parametric position in units of
     *         1/Length.
     */
    @Override
    public Parameter getCurvature(double s) {
        validateParameter(s);
        int dim = getPhyDimension();
        if (dim < 2)
            return Parameter.valueOf(0, getUnit().inverse());

        StackContext.enter();
        try {
            //  Ref. 1, Eqn. 12.24.
            List<Vector<Length>> ders = getSDerivatives(s, 2);
            Vector<Length> piu = ders.get(1);
            Vector<Length> piuu = ders.get(2);

            Parameter piuMag = piu.mag();
            if (piuu.mag().isApproxZero() || piuMag.isApproxZero())
                return StackContext.outerCopy(Parameter.valueOf(0, getUnit().inverse()));

            Parameter kappa;

            if (dim > 2) {
                //  kappa = |piu X piuu| / |piu|^3
                Vector piuXpiuu = piu.cross(piuu);
                kappa = piuXpiuu.mag().divide(piuMag.pow(3));

            } else {
                Parameter xu = piu.get(0);
                Parameter yu = piu.get(1);
                Parameter xuu = piuu.get(0);
                Parameter yuu = piuu.get(1);

                //  kappa = |xu*yuu - xuu*yu|/(xu^2 + yu^2)^(3/2) = |xu*yuu - xuu*yu|/|piu|^3
                Parameter num = xu.times(yuu).minus(xuu.times(yu)).abs();
                kappa = num.divide(piuMag.pow(3));
            }

            kappa = kappa.to(getUnit().inverse());
            return StackContext.outerCopy(kappa);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the curvature (kappa = 1/rho; where rho = the radius of curvature) of the
     * curve at the parametric position <code>s</code>. The curvature vector (vector from
     * p(s) to the center of curvature) can be constructed from:  <code>k = rhoi*ni</code>
     * or <code>k = curve.getPrincipalNormal(s).divide(curve.getCurvature(s));</code>
     *
     * @param s Parametric distance to calculate the curvature for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The curvature of the curve at the specified parametric position in units of
     *         1/Length.
     */
    @Override
    public Parameter getCurvature(GeomPoint s) {
        validateParameter(s);
        return getCurvature(s.getValue(0));
    }

    /**
     * Return the variation of curvature or rate of change of curvature (VOC or
     * dKappa(s)/ds) at the parametric position <code>s</code>.
     *
     * @param s Parametric distance to calculate the variation of curvature for (0.0 to
     *          1.0 inclusive).
     * @return The variation of curvature of the curve at the specified parametric
     *         position in units of 1/Length.
     */
    @Override
    public Parameter getVariationOfCurvature(double s) {
        validateParameter(s);
        int dim = getPhyDimension();
        if (dim < 2)
            return Parameter.valueOf(0, getUnit().inverse());

        //  Ref: Moreton, H.P., "Minimum Curvature Variation Curves, Networks,
        //  and Surfaces for Fair Free-Form Shape Design", Dissertation,
        //  University of California Berkely, 1992, pg. 79.
        
        StackContext.enter();
        try {
            //  Get the needed derivatives.
            List<Vector<Length>> ders = getSDerivatives(s, 3);
            Vector<Length> piu = ders.get(1);
            Vector<Length> piuu = ders.get(2);
            Vector<Length> piuuu = ders.get(3);

            Parameter piuMag = piu.mag();
            if (piuMag.isApproxZero())
                return StackContext.outerCopy(Parameter.valueOf(0, getUnit().inverse()));

            Parameter dKds;
            if (dim > 2) {
                //  u = piu x piuu
                Vector u = piu.cross(piuu);

                //  kappa = (piu X piuu) / |piu|^3
                Vector kappa = u.divide(piuMag.pow(3));

                //  du = piu x piuuu
                Vector du = piu.cross(piuuu);

                //  v = |piu|^3 = (piu dot piu)^(3/2)
                Parameter v = piuMag.pow(3);

                //  dv = 3*|piu|*(piu dot piuu)
                Parameter dv = piu.dot(piuu).times(piuMag).times(3);

                //  dKv/ds = (v*du - u*dv)/v^2
                Vector num = du.times(v).minus(u.times(dv));
                Vector dKvds = num.divide(v.pow(2));

                //  dK/ds = (kappa/|kappa|) dot dKv/ds
                dKds = dKvds.dot(kappa.toUnitVector());

            } else {
                Parameter xu = piu.get(0);
                Parameter yu = piu.get(1);
                Parameter xuu = piuu.get(0);
                Parameter yuu = piuu.get(1);
                Parameter xuuu = piuuu.get(0);
                Parameter yuuu = piuuu.get(1);

                //  u = xu*yuu - xuu*yu
                Parameter u = xu.times(yuu).minus(xuu.times(yu));

                //  du = xu*yuuu - xuuu*yu
                Parameter du = xu.times(yuuu).minus(xuuu.times(yu));

                //  v = |piu|^3 = (piu dot piu)^(3/2)
                Parameter v = piuMag.pow(3);

                //  dv = 3*|piu|*(piu dot piuu)
                Parameter dv = xu.times(xuu).plus(yu.times(yuu)).times(piuMag).times(3);

                //  dK/ds = (v*du - u*dv)/v^2
                dKds = v.times(du).minus(u.times(dv)).divide(v.pow(2));
            }

            Parameter voc = dKds.to(getUnit().inverse());
            return StackContext.outerCopy(voc);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the variation of curvature or rate of change of curvature (VOC or
     * dKappa(s)/ds) at the parametric position <code>s</code>.
     *
     * @param s Parametric distance to calculate the variation of curvature for. Must be a
     *          1-dimensional point with a value in the range 0 to 1.0. Units are ignored.
     * @return The variation of curvature of the curve at the specified parametric
     *         position in units of 1/Length.
     */
    @Override
    public Parameter getVariationOfCurvature(GeomPoint s) {
        validateParameter(s);
        return getVariationOfCurvature(s.getValue(0));
    }

    /**
     * Return the torsion of the curve at the parametric position <code>s</code>. The
     * torsion is a measure of the rotation or twist about the tangent vector.
     *
     * @param s Parametric distance to calculate the torsion for (0.0 to 1.0 inclusive).
     * @return The torsion of the curve at the specified parametric position in units of
     *         1/Length.
     */
    @Override
    public Parameter getTorsion(double s) {
        validateParameter(s);
        int dim = getPhyDimension();
        if (dim < 3)
            return Parameter.valueOf(0, getUnit().inverse());

        StackContext.enter();
        try {
            //  Ref. 1, Eqn. 12.30.
            List<Vector<Length>> ders = getSDerivatives(s, 3);
            Vector<Length> piu = ders.get(1);
            Vector<Length> piuu = ders.get(2);
            Vector<Length> piuuu = ders.get(3);

            if (piuuu.mag().isApproxZero() || piuu.mag().isApproxZero()
                    || piu.mag().isApproxZero())
                return StackContext.outerCopy(Parameter.valueOf(0, getUnit().inverse()));

            //  tau = piu DOT (piuu X piuuu) / |piu X piuu|^2
            Parameter piuXpiuu = piu.cross(piuu).mag();
            Parameter tau = piu.dot(piuu.cross(piuuu)).divide(piuXpiuu.times(piuXpiuu));

            return StackContext.outerCopy(tau);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the torsion of the curve at the parametric position <code>s</code>. The
     * torsion is a measure of the rotation or twist about the tangent vector.
     *
     * @param s Parametric distance to calculate the torsion for. Must be a 1-dimensional
     *          point with a value in the range 0 to 1.0. Units are ignored.
     * @return The torsion of the curve at the specified parametric position in units of
     *         1/Length.
     */
    @Override
    public Parameter getTorsion(GeomPoint s) {
        validateParameter(s);
        return getTorsion(s.getValue(0));
    }

    /**
     * Return the total arc length of this curve.
     *
     * @param eps The desired fractional accuracy on the arc-length.
     * @return The total arc length of the curve.
     */
    @Override
    public Parameter<Length> getArcLength(double eps) {
        return getArcLength(0, 1, eps);
    }

    /**
     * Return the arc length of a segment of this curve.
     *
     * @param s1  The starting parametric distance along the curve to begin the arc-length
     *            calculation from.
     * @param s2  The ending parametric distance along the curve to end the arc-length
     *            calculation at.
     * @param eps The desired fractional accuracy on the arc-length.
     * @return The arc length of the specified segment of the curve.
     */
    @Override
    public Parameter<Length> getArcLength(double s1, double s2, double eps) {
        validateParameter(s1);
        validateParameter(s2);

        //  If s1 and s2 are equal, then we know the answer already.
        if (isApproxEqual(s1, s2, TOL_S))
            return Parameter.ZERO_LENGTH.to(getUnit());

        //  Make sure that s2 is > s1.
        if (s1 > s2) {
            double temp = s1;
            s1 = s2;
            s2 = temp;
        }

        ArcLengthEvaluatable arcLengthEvaluator = ArcLengthEvaluatable.newInstance(this);
        double length = 0;
        try {

            //  Integrate to find the arc length:   L = int_s1^s2{ sqrt(ps(s) DOT ps(s)) ds}
            length = Quadrature.adaptLobatto(arcLengthEvaluator, s1, s2, eps);

        } catch (IntegratorException | RootException e) {
            //  Fall back on Gaussian Quadrature.
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    "Trying Gaussian Quadrature in getArcLength()...");
            try {
                length = Quadrature.gaussLegendre_Wx1N20(arcLengthEvaluator, s1, s2);

            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.SEVERE,
                        "Could not find arc-length.", err);
            }

        } finally {
            ArcLengthEvaluatable.recycle(arcLengthEvaluator);
        }

        Parameter<Length> L = Parameter.valueOf(length, getUnit());

        return L;
    }

    /**
     * Return the arc length of a segment of this curve.
     *
     * @param s1  The starting parametric distance along the curve to begin the arc-length
     *            calculation from. Must be a 1-dimensional point with a value in the
     *            range 0 to 1.0. Units are ignored.
     * @param s2  The ending parametric distance along the curve to end the arc-length
     *            calculation at. Must be a 1-dimensional point with a value in the range
     *            0 to 1.0. Units are ignored.
     * @param eps The desired fractional accuracy on the arc-length.
     * @return The arc length of the specified segment of the curve.
     */
    @Override
    public Parameter<Length> getArcLength(GeomPoint s1, GeomPoint s2, double eps) {
        return getArcLength(s1.getValue(0), s2.getValue(0), eps);
    }

    /**
     * Return a subrange point at the position on the curve with the specified arc-length.
     * If the requested arc-length is &le; 0, the start of the curve is returned. If the
     * requested arc length is &gt; the total arc length of the curve, then the end point
     * is returned.
     *
     * @param targetArcLength The target arc length to find in meters.
     * @param tol             Fractional tolerance (in parameter space) to refine the
     *                        point position to.
     * @return A subrange point on the curve at the specified arc-length position.
     */
    @Override
    public SubrangePoint getPointAtArcLength(Parameter<Length> targetArcLength, double tol) {
        //  Deal with an obvious case first.
        if (targetArcLength.isApproxZero())
            return SubrangePoint.newInstance(this, Point.valueOf(0));

        double arcLengthValue = targetArcLength.getValue(SI.METER);

        //  Calculate the full arc-length of the curve (to GTOL tolerance).
        double fullArcLength = getArcLength(GTOL).getValue(SI.METER);

        //  Is the requested arc length > than the actual arc length?
        if (arcLengthValue > fullArcLength * (1 + GTOL))
            return SubrangePoint.newInstance(this, Point.valueOf(1));

        //  Calculate the arc-length tolerance to use based on the requested parameter
        //  space tolerance and the full arc-length.
        double eps = tol * fullArcLength / 100.;

        return pointAtArcLength(0, arcLengthValue, tol, eps);
    }

    /**
     * A point is found along this curve that when connected by a straight line to the
     * given point in space, the resulting line is tangent to this curve. This method
     * should only be used if this curve is a planar curve that is C1 (slope) continuous,
     * and convex with respect to the point. The given point should be in the plane of the
     * curve. If it is not, it will be projected into the plane of the curve.
     *
     * @param point The point that the tangent on the curve is to be found relative to.
     * @param near  The parametric distance on the curve (0-1) that serves as an initial
     *              guess at the location of the tangent point.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The point on this curve that is tangent to the curve and the supplied
     *         point.
     */
    @Override
    public SubrangePoint getTangencyPoint(GeomPoint point, double near, double tol) {
        validateParameter(near);
        requireNonNull(point, MessageFormat.format(RESOURCES.getString("paramNullErr"), "point"));

        double minP = 0;
        StackContext.enter();
        try {
            int cDim = this.getPhyDimension();
            if (point.getPhyDimension() < cDim) {
                point = point.toDimension(cDim);
            }
            if (point.getPhyDimension() != cDim) {
                throw new DimensionException(RESOURCES.getString("crvPointDimensionErr"));
            }

            if (cDim > 2) {
                //  Project the input point onto the plane of the curve.
                Point p0 = this.getRealPoint(0);        //  Any point in plane (on curve).
                Vector nhat = this.getBinormal(.5);     //  Plane normal vector.

                //  Find the closest point on the plane.
                Point projPoint = GeomUtil.pointPlaneClosest(point, p0, nhat);
                point = projPoint;
            }

            //  Create a function that evaluates: 1 - |t(s) DOT ((q - p(s))/|q - p(s)|)|.
            //  When this function is minimized, the s value may be a tangent point.
            TangentPointEvaluatable tangentEvaluator = TangentPointEvaluatable.newInstance(this, point);
            try {
                //  Find the minimum of the evaluator function.
                minP = Minimization.find(tangentEvaluator, 0, near, 1, tol, null);

            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.SEVERE,
                        "Could not find tangency point.", err);
            }
            
        } finally {
            StackContext.exit();
        }

        SubrangePoint p = SubrangePoint.newInstance(this, Point.valueOf(minP));
        return p;
    }

    /**
     * Return a string of points that are gridded onto the curve using the specified
     * spacing and gridding rule.
     *
     * @param gridRule Specifies whether the subdivision spacing is applied with respect
     *                 to arc-length in real space or parameter space.
     * @param spacing  A list of values used to define the subdivision spacing.
     *                 <code>gridRule</code> specifies whether the subdivision spacing is
     *                 applied with respect to arc-length in real space or parameter
     *                 space. If the spacing is arc-length, then the values are
     *                 interpreted as fractions of the arc length of the curve from 0 to
     *                 1.
     * @return A string of subrange points gridded onto the curve at the specified
     *         parametric positions.
     */
    @Override
    public PointString<SubrangePoint> extractGrid(GridRule gridRule, List<Double> spacing) {
        requireNonNull(gridRule);
        requireNonNull(spacing);
        PointString<SubrangePoint> str = PointString.newInstance();

        switch (gridRule) {
            case PAR:
                //  Parametric spacing.
                for (double s : spacing) {
                    if (s > 1)
                        s = 1;
                    else if (s < 0)
                        s = 0;
                    str.add(this.getPoint(s));
                }
                break;

            case ARC:
                //  Arc-length spacing.
                double arcLength = this.getArcLength(GTOL).getValue(SI.METER);
                double tol = GTOL / arcLength * 100.;   //  Tolerance for root finder in parameter space.

                double sOld = 0;
                for (double s : spacing) {
                    double targetArcLength = s * arcLength;

                    SubrangePoint p;
                    if (targetArcLength >= arcLength)
                        p = getPoint(1);
                    else if (targetArcLength <= 0)
                        p = getPoint(0);

                    else {
                        if (s < sOld)
                            sOld = 0;
                        p = pointAtArcLength(sOld, targetArcLength, tol, GTOL);
                    }
                    str.add(p);

                    sOld = p.getParPosition().getValue(0);
                }
                break;

            default:
                throw new IllegalArgumentException(
                        MessageFormat.format(RESOURCES.getString("crvUnsupportedGridRule"),
                                gridRule.toString()));

        }

        return str;
    }

    /**
     * Return a subrange point at the position on the curve with the specified arc length.
     *
     * @param x1              The minimum parametric position that could have the
     *                        specified arc length.
     * @param targetArcLength The target arc length to find in meters.
     * @param tol             Fractional tolerance (in parameter space) to refine the
     *                        point position to.
     * @param eps             The desired fractional accuracy on the arc-length.
     */
    private SubrangePoint pointAtArcLength(double x1, double targetArcLength, double tol, double eps) {

        double pos = 1;
        StackContext.enter();
        try {
            //  Create a function that is used to iteratively find the target arc length.
            ArcPosEvaluatable arcPosEvaluator = ArcPosEvaluatable.newInstance(this, targetArcLength, eps);

            try {
                //  Find the arc length position using the evaluator function.
                arcPosEvaluator.firstPass = true;
                pos = Roots.findRoot1D(arcPosEvaluator, x1, 1, tol);

            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                        "Could not find point at arc-length.", err);
                pos = 1;
            }
        } finally {
            StackContext.exit();
        }
        
        SubrangePoint p = SubrangePoint.newInstance(this, Point.valueOf(pos));
        return p;
    }

    private static final int MAX_POINTS = 10000;

    /**
     * Return a string of points that are gridded onto the curve with the number of points
     * and spacing chosen to result in straight line segments between the points that have
     * mid-points that are all within the specified tolerance of this curve.
     *
     * @param tol The maximum distance that a straight line between gridded points may
     *            deviate from this curve. May not be null.
     * @return A string of subrange points gridded onto the curve.
     */
    @Override
    public PointString<SubrangePoint> gridToTolerance(Parameter<Length> tol) {
        if (isDegenerate(requireNonNull(tol))) {
            //  If this curve is a single point, just return that point twice.
            PointString<SubrangePoint> str = PointString.newInstance();
            str.add(getPoint(0));
            str.add(getPoint(1));
            return str;
        }

        //  Start with the end-points.
        FastList<SubrangePoint> pntList = FastList.newInstance();
        pntList.add(getPoint(0));
        pntList.add(getPoint(1));

        //  Refine the initial points using subdivision.
        Parameter<Area> tol2 = tol.times(tol);
        refineCurvePoints(pntList, tol2);
        
        //  Check guessed grid-to-tolerance points and add any that fall out of tolerance.
        FastList<SubrangePoint> guessPnts = guessGrid2TolPoints(tol2);
        checkGuessedGrid2TolPoints(pntList, guessPnts, tol2);
        FastList.recycle(guessPnts);

        //  Refine the points after any guessed points were added using subdivision.
        refineCurvePoints(pntList, tol2);
        
        if (pntList.size() >= MAX_POINTS)
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    MessageFormat.format(RESOURCES.getString("maxPointsWarningMsg"),
                    "curve", this.getID()));

        //  Convert from a FastList to a PointString.
        PointString<SubrangePoint> str = PointString.valueOf(null, pntList);
        FastList.recycle(pntList);
        
        return str;
    }

    /**
     * Use subdivision to refine the points gridded onto this curve until the
     * mid-points of all the segments meet the specified tolerance from the
     * curve. Points are added to the list wherever the segment mid-point
     * distances are greater than the tolerance.
     *
     * @param pntList The starting list of points to refine.
     * @param tol2 The square of the maximum distance that a straight line between
     *            gridded points may deviate from this curve. May not be null.
     */
    private void refineCurvePoints(FastList<SubrangePoint> pntList, Parameter<Area> tol2) {
        //  Start at the head of the list.
        FastList.Node<SubrangePoint> current = pntList.head().getNext();
        
        //  Loop until we reach the end of the list or exceed the max. number of points.
        FastList.Node tail = pntList.tail().getPrevious();
        while (!current.equals(tail) && pntList.size() < MAX_POINTS) {
            
            //  Subdivide the current segment until the mid-point is less than tol away from the curve.
            boolean distGTtol = true;
            while (distGTtol) {
                
                //  Get the current point.
                SubrangePoint pc = current.getValue();
                double sc = pc.getParPosition().getValue(0);
                
                //  Get the next point.
                FastList.Node<SubrangePoint> next = current.getNext();
                SubrangePoint pn = next.getValue();
                double sn = pn.getParPosition().getValue(0);
                
                //  Compute the average point half-way on a line from pc to pn.
                Point pa = GeomUtil.averagePoints(pc, pn);
                
                //  Find the parametric middle point on the curve.
                double sm = (sc + sn) / 2;
                SubrangePoint pm = getPoint(sm);
                
                //  Compute the distance between the middle point and the average point
                //  and compare with the tolerance.
                distGTtol = pa.distanceSq(pm).isGreaterThan(tol2);
                if (distGTtol)
                    //  Insert the new point into the list.
                    pntList.addBefore(next, pm);
                else
                    SubrangePoint.recycle(pm);
                
            }   //  end while()
            
            //  Move on to the next point.
            current = current.getNext();

        }   //  end while()
    }

    /**
     * A Comparator that compares the parametric S-position of a pair of
     * subrange points.
     */
    private static class SComparator implements Comparator<SubrangePoint> {
        
            @Override
            public int compare(SubrangePoint o1, SubrangePoint o2) {
                double s1 = o1.getParPosition().getValue(0);
                double s2 = o2.getParPosition().getValue(0);
                return Double.compare(s1, s2);
            }
    }
    
    private static final SComparator S_CMPRTR = new SComparator();
    
    /**
     * Check guessed grid-to-tolerance points and add them to pntList if they
     * are greater than the tolerance distance away from straight line segments
     * between the points already in pntList.
     * 
     * @param pntList The existing list of gridded points on this curve. Any
     * points from guessPnts that are more than tol away from straight line
     * segments between these points are added to this list.
     * @param guessPnts The existing list of segment parameter derived guessed points.
     * @param tol2 The square of the maximum distance that a straight line between
     *            gridded points may deviate from this curve. May not be null.
     */
    private void checkGuessedGrid2TolPoints(FastList<SubrangePoint> pntList, 
            FastList<SubrangePoint> guessPnts, Parameter<Area> tol2) {
        
        int nSegs = guessPnts.size() - 1;
        for (int i=1; i < nSegs; ++i) {
            SubrangePoint ps = guessPnts.get(i);
            double s = ps.getParPosition().getValue(0);
            
            //  Find where this point should fall in the gridded list of points.
            int idx = Collections.binarySearch(pntList, ps, S_CMPRTR);
            if (idx < 0) {
                //  ps is not already in the pntList, so see if it is in tolerance or not.
                idx = -idx - 1;
                
                //  Calculate a point, pa, along the line between p(i) and p(i-1)
                //  at the position of "s".
                SubrangePoint pi = pntList.get(idx);
                SubrangePoint pim1 = pntList.get(idx-1);
                Point pa = interpSPoint(pim1,pi,s);
                
                //  Compute the distance between the point at "s" and the line seg point
                //  and compare with the tolerance.
                boolean distGTtol = pa.distanceSq(ps).isGreaterThan(tol2);
                if (distGTtol) {
                    //  Insert the new point into the list.
                    pntList.add(idx, ps);
                    if (pntList.size() == MAX_POINTS)
                        break;
                } else
                    SubrangePoint.recycle(ps);
            } else
                SubrangePoint.recycle(ps);
        }
    }

    /**
     * Interpolate a point at "s" between the points p1 and p2 (that must have
     * parametric positions surrounding "s").
     *
     * @param p1 Point with a parametric position less than "s".
     * @param p2 Point with a parametric position greater than "s".
     * @param s The parametric position at which a point is to be interpolated
     * between p1 and p2.
     * @return The interpolated point.
     */
    protected static Point interpSPoint(SubrangePoint p1, SubrangePoint p2, double s) {
        StackContext.enter();
        try {
            double s1 = p1.getParPosition().getValue(0);
            double s2 = p2.getParPosition().getValue(0);
            Point Ds = p2.minus(p1);
            Point pout = p1.plus(Ds.times((s - s1) / (s2 - s1)));
            return StackContext.outerCopy(pout);
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Guess an initial set of points to use when gridding to tolerance. The initial
     * points must include the start and end of the curve as well as any discontinuities
     * in the curve. Other than that, they should be distributed as best as possible to
     * indicate areas of higher curvature, but should be generated as efficiently as
     * possible. The guessed points MUST be monotonically increasing from 0 to 1.
     * <p>
     * The default implementation places points at the positions returned by
     * "getSegmentParameters()". Sub-classes should override this method if they can
     * provide a more efficient/robust method to locate points where there may be
     * curvature discontinuities or areas of high curvature.
     * </p>
     *
     * @param tol2 The square of the maximum distance that a straight line
     *             between gridded points may deviate from this curve.
     * @return A FastList of subrange points to use as a starting point for the grid to
     *         tolerance algorithms.
     */
    protected FastList<SubrangePoint> guessGrid2TolPoints(Parameter<Area> tol2) {

        //  Extract the boundaries of curve segments and place grid points at those.
        FastList<SubrangePoint> pntList = FastList.newInstance();

        //  Create a list of segment starting parametric positions ( and the last end position, 1).
        FastTable<Double> bParams = getSegmentParameters();
        if (bParams.size() < 3) {
            //  We only have 0.0 & 1.0.  Insert 0.5 to ensure there is always at least one intermediate point.
            bParams.add(1, 0.5);
        }

        //  Loop over the segment starting positions.
        int nSegs = bParams.size();
        for (int i = 0; i < nSegs; ++i) {
            double s0 = bParams.get(i);
            pntList.add(getPoint(s0));          //  Add the start-point of each segment.
        }

        //  Cleanup before leaving.
        FastTable.recycle(bParams);

        return pntList;
    }

    /**
     * Return the intersection between a plane and this curve.
     *
     * @param plane The plane to intersect with this curve.
     * @param tol   Fractional tolerance (in parameter space) to refine the point
     *              positions to.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this curve with the specified plane. If no intersection is
     *         found, an empty PointString is returned.
     */
    @Override
    public PointString<SubrangePoint> intersect(GeomPlane plane, double tol) {

        //  Algorithm:  Finds places on the curve where the signed distance from a curve
        //  point to the plane is zero:  n dot (p(s) - pp) = 0.
        //  See:  http://mathworld.wolfram.com/Point-PlaneDistance.html
        
        //  Create a function that is used to iteratively find the intersections.
        int numDims = getPhyDimension();
        if (numDims < plane.getPhyDimension())
            numDims = plane.getPhyDimension();
        PlaneIntersectEvaluatable intEvaluator
                = PlaneIntersectEvaluatable.newInstance(this.toDimension(numDims), plane.toDimension(numDims));

        PointString<SubrangePoint> output = PointString.newInstance();
        try {

            //  Guess brackets surrounding the intersection points (if any).
            List<BracketRoot1D> brackets = guessIntersect(plane, intEvaluator);

            //  Loop over all the brackets found.
            for (BracketRoot1D bracket : brackets) {
                //  Find the intersections using the evaluator function.
                double pos = Roots.findRoot1D(intEvaluator, bracket, tol);
                SubrangePoint p = SubrangePoint.newInstance(this, Point.valueOf(pos));

                //  Add the point to the list of intersections being collected.
                output.add(p);
            }

            //  Remove any near duplicates.
            int numPnts = output.size();
            for (int i = 0; i < numPnts; ++i) {
                SubrangePoint pi = output.get(i);
                for (int j = i + 1; j < numPnts; ++j) {
                    SubrangePoint pj = output.get(j);
                    double d = pj.getParPosition().distanceValue(pi.getParPosition());
                    if (d <= tol) {
                        SubrangePoint p = output.remove(j);
                        --numPnts;
                        --j;
                    }
                }
            }

        } catch (RootException err) {
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.SEVERE,
                    "Could not intersect this curve witih a plane.", err);

        } finally {
            PlaneIntersectEvaluatable.recycle(intEvaluator);
        }

        return output;
    }

    /**
     * Return a list of brackets that surround possible plane intersection points. Each of
     * the brackets will be refined using a root solver in order to find the actual
     * intersection points.
     * <p>
     * The default implementation sub-divides the curve into 17 equal-sized segments and
     * looks for a single bracket inside each segment. Sub-classes should override this
     * method if they can provide a more efficient/robust method to locate plane
     * intersection points.
     * </p>
     *
     * @param plane The plane to find the intersection points on this curve to/from.
     * @param eval  A function that calculates the <code>n dot (p(s) - pp)</code> on the
     *              curve.
     * @return A list of brackets that each surround a potential intersection point.
     *         Returns an empty list if there are no intersections found.
     */
    protected List<BracketRoot1D> guessIntersect(GeomPlane plane, Evaluatable1D eval) {

        //  Find multiple intersection points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {

            brackets = BracketRoot1D.findBrackets(requireNonNull(eval), 0, 1, getNumPointsPerSegment() * 2 + 1);

        } catch (RootException err) {
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    "guessIntersect(plane, eval) failed.", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Return the intersection between an infinite line and this curve.
     *
     * @param L0   A point on the line (origin of the line).
     * @param Ldir The direction vector for the line (does not have to be a unit vector).
     * @param tol  Tolerance (in physical space) to refine the point positions to.
     * @return A PointString containing zero or more subrange points made by the
     *         intersection of this curve with the specified line. If no intersection is
     *         found, an empty PointString is returned.
     */
    @Override
    public PointString<SubrangePoint> intersect(GeomPoint L0, GeomVector Ldir, Parameter<Length> tol) {
        requireNonNull(tol);
        
        //  First check to see if the line even comes near the curve.
        if (!GeomUtil.lineBoundsIntersect(L0, Ldir, this, null))
            return PointString.newInstance();

        //  Make sure the line and curve have the same dimensions.
        int numDims = getPhyDimension();
        if (L0.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "line", "curve"));
        else if (L0.getPhyDimension() < numDims)
            L0 = L0.toDimension(numDims);
        if (Ldir.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "line", "curve"));
        else if (Ldir.getPhyDimension() < numDims)
            Ldir = Ldir.toDimension(numDims);

        //  Create a line-curve intersector object.
        LineCurveIntersect intersector = LineCurveIntersect.newInstance(this, tol, L0, Ldir);

        //  Intersect the line and curve (intersections are added to "output").
        PointString<SubrangePoint> output = intersector.intersect();

        //  Clean up.
        LineCurveIntersect.recycle(intersector);

        return output;
    }

    /**
     * Return the intersection between a line segment and this curve.
     *
     * @param line The line segment to intersect.
     * @param tol  Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this curve and the input line segment respectively,
     *         made by the intersection of this curve with the specified line segment. If
     *         no intersections are found a list of two empty PointStrings are returned.
     */
    @Override
    public GeomList<PointString<SubrangePoint>> intersect(LineSegment line, Parameter<Length> tol) {
        requireNonNull(line);
        requireNonNull(tol);
        
        //  First check to see if the line segment even comes near the curve.
        GeomList<PointString<SubrangePoint>> output = GeomList.newInstance();
        output.add(PointString.newInstance());
        output.add(PointString.newInstance());
        if (!GeomUtil.lineSegBoundsIntersect(line.getStart(), line.getEnd(), this))
            return output;

        //  Check for a degenerate curve case.
        if (isDegenerate(tol)) {
            //  Just see if the start point on this curve intersects that curve (is on that curve).
            SubrangePoint p1 = getPoint(0);
            Point p1r = p1.copyToReal();

            //  Find the closest point on the surface.
            SubrangePoint p2 = line.getClosest(p1r, GTOL);

            if (p1r.distance(p2).isLessThan(tol)) {
                //  Add the points to the lists of intersections being collected.
                output.get(0).add(p1);
                output.get(1).add(p2);
            }

            return output;
        }

        //  Make sure the line and curve have the same dimensions.
        int numDims = getPhyDimension();
        if (line.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "line", "curve"));
        else if (line.getPhyDimension() < numDims)
            line = line.toDimension(numDims);

        //  Create a line segment-curve intersector object.
        LineSegCurveIntersect intersector = LineSegCurveIntersect.newInstance(this, tol, line, output);

        //  Intersect the line segment and curve (intersections are added to "output").
        output = intersector.intersect();

        //  Clean up.
        LineSegCurveIntersect.recycle(intersector);

        return output;
    }

    /**
     * Return the intersection between another curve and this curve.
     *
     * @param curve The curve to intersect with this curve.
     * @param tol   Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this curve and the input curve respectively, made by
     *         the intersection of this curve with the specified curve. If no
     *         intersections are found a list of two empty PointStrings are returned.
     */
    @Override
    public GeomList<PointString<SubrangePoint>> intersect(Curve curve, Parameter<Length> tol) {
        requireNonNull(curve);
        requireNonNull(tol);
        
        //  First check to see if the line segment even comes near the curve.
        GeomList<PointString<SubrangePoint>> output = GeomList.newInstance();
        output.add(PointString.newInstance());
        output.add(PointString.newInstance());
        if (!GeomUtil.boundsIntersect(curve, this))
            return output;

        //  Check for a degenerate curve case.
        if (isDegenerate(tol)) {
            //  Just see if the start point on this curve intersects that curve (is on that curve).
            SubrangePoint p1 = getPoint(0);
            Point p1r = p1.copyToReal();

            //  Find the closest point on the surface.
            SubrangePoint p2 = curve.getClosest(p1r, GTOL);

            if (p1r.distance(p2).isLessThan(tol)) {
                //  Add the points to the lists of intersections being collected.
                output.get(0).add(p1);
                output.get(1).add(p2);
            }

            return output;
        }

        //  Create a curve-curve intersector object.
        CurveCurveIntersect intersector = CurveCurveIntersect.newInstance(this, tol, curve, output);

        //  Intersect that curve and this curve (intersections are added to "output").
        output = intersector.intersect();

        //  Clean up.
        CurveCurveIntersect.recycle(intersector);

        return output;
    }

    /**
     * Return the intersections between a surface and this curve.
     *
     * @param surface The surface to intersect with this curve.
     * @param tol     Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     *         subrange points, on this curve and the input surface respectively, made by
     *         the intersection of this curve with the specified surface. If no
     *         intersections are found a list of two empty PointStrings are returned.
     */
    @Override
    public GeomList<PointString<SubrangePoint>> intersect(Surface surface, Parameter<Length> tol) {
        requireNonNull(surface);
        requireNonNull(tol);
        
        PointString<SubrangePoint> thisOutput = PointString.newInstance();
        PointString<SubrangePoint> thatOutput = PointString.newInstance();
        GeomList<PointString<SubrangePoint>> output = GeomList.newInstance();
        output.add(thisOutput);
        output.add(thatOutput);

        //  First check to see if the curve even comes near the surface.
        if (!GeomUtil.boundsIntersect(this, surface))
            return output;

        //  Make sure the line and curve have the same dimensions.
        Surface origSrf = surface;
        int numDims = getPhyDimension();
        if (surface.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "surface", "curve"));
        else if (surface.getPhyDimension() < numDims)
            surface = surface.toDimension(numDims);

        //  Check for a degenerate curve case.
        if (isDegenerate(tol)) {
            //  Just see if the start point on the curve intersects the surface (is on the surface).
            SubrangePoint p1 = getPoint(0);
            Point p1r = p1.copyToReal();

            //  Find the closest point on the surface.
            SubrangePoint p2 = origSrf.getClosest(p1r, GTOL);

            if (p1r.distance(p2).isLessThan(tol)) {
                //  Add the points to the lists of intersections being collected.
                thisOutput.add(p1);
                thatOutput.add(p2);
            }

            return output;
        }

        //  Convert the inputs to the units of this surface.
        Unit<Length> unit = getUnit();
        surface = surface.to(unit);
        tol = tol.to(unit);

        //  Determine the parameter tolerance to use.
        double arcLength = this.getArcLength(GTOL * 100).getValue();
        double ptol = tol.getValue() / arcLength;   //  Tolerance for root finder in parameter space.
        if (ptol < GTOL)
            ptol = GTOL;

        //  Find potential intersection points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.  Then, a 1D root finder
        //  is used to find the bracketed roots.
        CurveSrfIntersectFun eval = CurveSrfIntersectFun.newInstance(this, surface, GTOL * 1000);
        try {

            //  Guess brackets surrounding the intersection points (if any).
            List<BracketRoot1D> brackets = guessIntersect(surface, eval);

            //  Increase the tolerance for actual root finding.
            eval.tol = GTOL;

            //  Loop over all the brackets found.
            for (BracketRoot1D bracket : brackets) {

                //  Find the intersections using the evaluator function.
                eval.firstPass = true;
                double pos = Roots.findRoot1D(eval, bracket, ptol);
                SubrangePoint p1 = SubrangePoint.newInstance(this, Point.valueOf(pos));
                Point p1r = p1.copyToReal();

                //  Find the closest point on the surface.
                SubrangePoint p2 = origSrf.getClosest(p1r, eval._nearS, eval._nearT, GTOL);

                if (p1r.distance(p2).isLessThan(tol)) {
                    //  Add the points to the lists of intersections being collected.
                    thisOutput.add(p1);
                    thatOutput.add(p2);
                }
            }

        } catch (RootException e) {
            //e.printStackTrace();

        } finally {
            CurveSrfIntersectFun.recycle(eval);
        }

        return output;
    }

    /**
     * Return a list of brackets that surround possible surface intersection points. Each
     * of the brackets will be refined using a root solver in order to find the actual
     * intersection points.
     * <p>
     * The default implementation sub-divides the curve into 17 equal-sized segments and
     * looks for a single bracket inside each segment. Sub-classes should override this
     * method if they can provide a more efficient/robust method to locate surface
     * intersection points.
     * </p>
     *
     * @param surface The surface to find the intersection points on this curve to/from.
     * @param eval    A function that calculates the <code>n dot (p(s) - pp)</code> on the
     *                curve.
     * @return A list of brackets that each surround a potential intersection point.
     *         Returns an empty list if there are no intersections found.
     */
    protected List<BracketRoot1D> guessIntersect(Surface surface, Evaluatable1D eval) {

        //  Find multiple intersection points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {

            brackets = BracketRoot1D.findBrackets(requireNonNull(eval), 0, 1, getNumPointsPerSegment() * 2 + 1);

        } catch (RootException err) {
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    "guessIntersect(surface, eval) failed.", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Returns the closest point on this curve to the specified point.
     *
     * @param point The point to find the closest point on this curve to.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The point on this curve that is closest to the specified point.
     */
    @Override
    public SubrangePoint getClosest(GeomPoint point, double tol) {
        return getClosestFarthest(point, tol, true);
    }

    /**
     * Returns the closest point on this curve to the specified point near the specified
     * parametric position. This may not return the global closest point!
     *
     * @param point The point to find the closest point on this curve to.
     * @param near  The parametric position where the search for the closest point should
     *              be started.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The point on this curve that is closest to the specified point near the
     *         specified parametric position.
     */
    @Override
    public SubrangePoint getClosest(GeomPoint point, double near, double tol) {
        return getClosestFarthestNear(point, near, tol, true);
    }

    /**
     * Returns the closest points on this curve to the specified list of points.
     *
     * @param points The list of points to find the closest points on this curve to.
     * @param tol    Fractional tolerance (in parameter space) to refine the point
     *               positions to.
     * @return The list of {@link SubrangePoint} objects on this curve that are closest to
     *         the specified points.
     */
    @Override
    public PointString<SubrangePoint> getClosest(List<? extends GeomPoint> points, double tol) {
        return getClosestFarthest(points, tol, true);
    }

    /**
     * Returns the farthest point on this curve from the specified point.
     *
     * @param point The point to find the farthest point on this curve from.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The {@link SubrangePoint} on this curve that is farthest from the specified
     *         point.
     */
    @Override
    public SubrangePoint getFarthest(GeomPoint point, double tol) {
        return getClosestFarthest(point, tol, false);
    }

    /**
     * Returns the farthest point on this curve from the specified point near the
     * specified parametric position. This may not return the global farthest point!
     *
     * @param point The point to find the farthest point on this curve from.
     * @param near  The parametric position where the search for the farthest point should
     *              be started.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The {@link SubrangePoint} on this curve that is farthest from the specified
     *         point.
     */
    @Override
    public SubrangePoint getFarthest(GeomPoint point, double near, double tol) {
        return getClosestFarthestNear(point, near, tol, false);
    }

    /**
     * Returns the farthest points on this curve to the specified list of points.
     *
     * @param points The list of points to find the farthest points on this curve to.
     * @param tol    Fractional tolerance (in parameter space) to refine the point
     *               positions to.
     * @return The list of {@link SubrangePoint} objects on this curve that are farthest
     *         from the specified points.
     */
    @Override
    public PointString<SubrangePoint> getFarthest(List<? extends GeomPoint> points, double tol) {
        return getClosestFarthest(points, tol, false);
    }

    /**
     * Returns the closest/farthest points on this curve to the specified point.
     *
     * @param point   The point to find the closest/farthest point on this curve from.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                position to.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The {@link SubrangePoint} on this curve that is closest/farthest from the
     *         specified point.
     */
    protected SubrangePoint getClosestFarthest(GeomPoint point, double tol, boolean closest) {

        double sout = 0;
        StackContext.enter();
        try {
            //  Make sure the point and curve have the same dimensions.
            int numDims = getPhyDimension();
            if (point.getPhyDimension() > numDims) {
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "point", "curve"));
            } else if (point.getPhyDimension() < numDims) {
                point = point.toDimension(numDims);
            }

            //  Create a function that calculates the dot product of (p-q) and ps.  When this
            //  is zero, a point on the curve that forms a vector perpendicular to the curve has been found.
            PerpPointEvaluatable perpPointEval = PerpPointEvaluatable.newInstance(this, point);

            //  Find brackets surrounding possible closest/farthest points.
            List<BracketRoot1D> brackets = guessClosestFarthest(point, closest, perpPointEval);

            //  Find the closest/farthest point.
            SubrangePoint output = getClosestFarthest(point, tol, closest, perpPointEval, brackets);
            sout = output.getParPosition().getValue(0);

        } finally {
            StackContext.exit();
        }

        return getPoint(sout);
    }

    /**
     * Returns the closest/farthest points on this curve to the specified list of points.
     *
     * @param point   The point to find the closest/farthest point on this curve from.
     * @param near    The parametric position where the search for the farthest point
     *                should be started.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                position to.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The {@link SubrangePoint} on this curve that is closest/farthest from the
     *         specified point.
     */
    private SubrangePoint getClosestFarthestNear(GeomPoint point, double near, double tol, boolean closest) {
        validateParameter(near);
        
        double sout = 0;
        StackContext.enter();
        try {
            //  Make sure the point and curve have the same dimensions.
            int numDims = getPhyDimension();
            if (point.getPhyDimension() > numDims) {
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "point", "curve"));
            } else if (point.getPhyDimension() < numDims) {
                point = point.toDimension(numDims);
            }

            //  Create a function that calculates the dot product of (p-q) and ps.  When this
            //  is zero, a point on the curve that forms a vector perpendicular to the curve has been found.
            PerpPointEvaluatable perpPointEval = PerpPointEvaluatable.newInstance(this, point);

            //  Find the closet/farthest point near the specified parametric position.
            try {
                List<BracketRoot1D> brackets = bracketNear(perpPointEval, near);
                SubrangePoint output = getClosestFarthest(point, tol, closest, perpPointEval, brackets);
                sout = output.getParPosition().getValue(0);
                
            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                        "Failed to find closest/farthest point near existing point.", err);
                sout = 0;
            }
            
        } finally {
            StackContext.exit();
        }
        
        return getPoint(sout);
    }

    /**
     * Returns the closest/farthest points on this curve to the specified list of points.
     *
     * @param points  The list of points to find the closest/farthest points on this curve
     *                to.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                positions to.
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @return The list of {@link SubrangePoint} objects on this curve that are
     *         closest/farthest to the specified points.
     */
    @SuppressWarnings("null")
    private PointString<SubrangePoint> getClosestFarthest(List<? extends GeomPoint> points, double tol, boolean closest) {
        PointString<SubrangePoint> output = PointString.newInstance();

        //  Loop over all the target points in the supplied list and find the closest curve point for each one.
        SubrangePoint p_old = null;
        int numDims = getPhyDimension();
        int size = points.size();
        for (int i = 0; i < size; ++i) {
            //  Get the target point.
            GeomPoint q = points.get(i);

            //  Make sure the point and curve have the same dimensions.
            if (q.getPhyDimension() > numDims)
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "points", "curve"));
            else if (q.getPhyDimension() < numDims)
                q = q.toDimension(numDims);

            //  Create a function that calculates the dot product of (p(s)-q) and ps(s).  When this
            //  is zero, a point on the curve that forms a vector with the target perpendicular to the curve has been found.
            PerpPointEvaluatable perpPointEval = PerpPointEvaluatable.newInstance(this, q);

            //  Find multiple closest/farthest points (roots) by sub-dividing curve into pieces and
            //  bracketing any roots that occur in any of those segments.
            List<BracketRoot1D> brackets;
            try {
                if (i == 0)
                    //  Look across the entire curve.
                    brackets = guessClosestFarthest(q, closest, perpPointEval);
                else
                    //  Look for brackets near the previous point.
                    brackets = bracketNear(perpPointEval, p_old.getParPosition().getValue(0));
            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                        "Could not find closest/farthest point.", err);
                return output;
            }

            //  Now use a root finder to actually find the closest/farthest point on the curve.
            SubrangePoint p = getClosestFarthest(q, tol, closest, perpPointEval, brackets);
            output.add(p);
            p_old = p;

            //  Recycle temporary objects.
            PerpPointEvaluatable.recycle(perpPointEval);
        }

        return output;
    }

    /**
     * Return a list of brackets that surround possible closest/farthest points. Each of
     * the brackets will be refined using a root solver in order to find the actual
     * closest or farthest points.
     * <p>
     * The default implementation sub-divides the curve into 21 equal-sized segments and
     * looks for a single bracket inside each segment. Sub-classes should override this
     * method if they can provide a more efficient/robust method to locate
     * closest/farthest points.
     * </p>
     *
     * @param point   The point to find the closest/farthest point on this curve to/from
     *                (guaranteed to be the same physical dimension before this method is
     *                called).
     * @param closest Set to <code>true</code> to return the closest point or
     *                <code>false</code> to return the farthest point.
     * @param eval    A function that calculates the dot product of (p(s)-q) and ps(s) on
     *                the curve.
     * @return A list of brackets that each surround a potential closest/farthest point.
     */
    protected List<BracketRoot1D> guessClosestFarthest(GeomPoint point, boolean closest,
            Evaluatable1D eval) {

        //  Find multiple closest/farthest points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {

            brackets = BracketRoot1D.findBrackets(requireNonNull(eval), 0, 1, 21);

        } catch (RootException err) {
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    "Failed to guess closest/farthest point.", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Look for and bracket roots at parametric positions near the specified parametric
     * position value.
     *
     * @param eval A function that calculates the dot product of (p(s)-q) and ps(s) on the
     *             curve.
     * @param near The parametric position on the curve to begin search for roots near.
     * @return A list of brackets of roots to the perpPointEval function each of which
     *         could be the closest/farthest.
     * @throws jahuwaldt.tools.math.RootException if a bracket could not be found.
     */
    protected List<BracketRoot1D> bracketNear(Evaluatable1D eval, double near) throws RootException {
        requireNonNull(eval);
        List<BracketRoot1D> brackets = null;

        //  Look near the previous parametric position by expanding the upper and lower limits until
        //  at least one bracket is found.
        double old_sm = near;
        double old_sp = near - 0.01;
        if (old_sp < 0)
            old_sp = 0;
        do {
            double sm = old_sm - 0.1;
            if (sm < 0)
                sm = 0;
            double sp = old_sp + 0.1;
            if (sp > 1)
                sp = 1;
            //  Look on the high side of near.
            brackets = BracketRoot1D.findBrackets(eval, old_sp, sp, 5);
            //  Look on the low side of near.
            brackets.addAll(BracketRoot1D.findBrackets(eval, sm, old_sm, 5));
            old_sm = sm;
            old_sp = sp;
        } while (brackets.size() < 1 && !(old_sp == 1 && old_sm == 0));

        return brackets;
    }

    /**
     * Returns the closest point on this curve, p, to the specified point, q.
     *
     * @param point         The point, q, to find the closest point on this curve to
     *                      (guaranteed to be the same physical dimension before this
     *                      method is called).
     * @param tol           Fractional tolerance (in parameter space) to refine the point
     *                      position to.
     * @param closest       Set to <code>true</code> to return the closest point or
     *                      <code>false</code> to return the farthest point.
     * @param perpPointEval A function that calculates the dot product of (p(s)-q) and
     *                      ps(s) on the curve.
     * @param brackets      A list of brackets of roots to the perpPointEval function each
     *                      of which could be the closest/farthest.
     * @return The {@link SubrangePoint} on this curve that is closest to the specified
     *         point.
     */
    private SubrangePoint getClosestFarthest(GeomPoint point, double tol, boolean closest, PerpPointEvaluatable perpPointEval, List<BracketRoot1D> brackets) {
        
        double sout = 0;
        StackContext.enter();
        try {
            //  Create a list of potential closest points and add the ends of the curve to it.
            FastTable<SubrangePoint> potentials = FastTable.newInstance();
            potentials.add(SubrangePoint.newInstance(this, 0));
            potentials.add(SubrangePoint.newInstance(this, 1));

            try {
                //  Loop over all the brackets found.
                for (BracketRoot1D bracket : brackets) {
                    //  Find the perpendicular point on the curve using the evaluator function.
                    perpPointEval.firstPass = true;
                    double s = Roots.findRoot1D(perpPointEval, bracket, tol);

                    //  Add the point to the list of potential closest/farthest points being collected.
                    potentials.add(SubrangePoint.newInstance(this, s));
                }

            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                        "Failed to find perpendicular point along curve.", err);

            } finally {
                if (potentials.isEmpty()) {
                    sout = 0;

                } else {
                    //  Loop over the potentials and find the one that is the closest/farthest.
                    SubrangePoint output = potentials.get(0);
                    double d2Opt = point.distanceSqValue(output);
                    int size = potentials.size();
                    for (int i = 1; i < size; ++i) {
                        SubrangePoint p = potentials.get(i);
                        double dist2 = point.distanceSqValue(p);
                        if (closest) {
                            if (dist2 < d2Opt) {
                                d2Opt = dist2;
                                output = p;
                            }
                        } else {
                            if (dist2 > d2Opt) {
                                d2Opt = dist2;
                                output = p;
                            }
                        }
                    }
                    sout = output.getParPosition().getValue(0);
                }
            }
        } finally {
            StackContext.exit();
        }
       
        return getPoint(sout);
    }

    /**
     * Returns the closest points (giving the minimum distance) between this curve and the
     * specified curve. If the specified curve intersects this curve, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If the specified curve is
     * parallel to this curve (all points are equidistant away), then any point between
     * the two curves could be returned as the "closest".
     *
     * @param curve The curve to find the closest points between this curve and that one.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return A list containing two SubrangePoint objects that represent the closest
     *         point on this curve (index 0) and the closest point on the specified curve
     *         (index 1).
     */
    @Override
    public PointString<SubrangePoint> getClosest(Curve curve, double tol) {

        //  Make sure that curve and this curve have the same dimensions.
        int numDims = getPhyDimension();
        if (curve.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "input curve", "curve"));
        else if (curve.getPhyDimension() < numDims)
            curve = curve.toDimension(numDims);

        //  Create a function that calculates the dot product of (p-q) and ps where "q" is the closest
        //  point on the target curve.  When this is zero, a point on the curve that forms a
        //  vector perpendicular to the curve has been found.
        PerpPointParGeomEvaluatable perpPointEval = PerpPointParGeomEvaluatable.newInstance(this, curve, tol);

        //  Find brackets surrounding possible closest/farthest points.
        List<BracketRoot1D> brackets = guessClosest(curve, perpPointEval);

        //  Find the closest point.
        PointString<SubrangePoint> output = getClosest(curve, tol, perpPointEval, brackets);

        //  Recycle temporary objects.
        PerpPointParGeomEvaluatable.recycle(perpPointEval);

        return output;
    }

    /**
     * Return a list of brackets that surround possible points on this curve that are
     * closest to the target parametric geometry object. Each of the brackets will be
     * refined using a root solver in order to find the actual closest points.
      * <p>
     * The default implementation sub-divides the curve into 21 equal-sized segments and
     * looks for a single bracket inside each segment. Sub-classes should override this
     * method if they can provide a more efficient/robust method to locate closest
     * points.
     * </p>
     *
     * @param parGeom The target parametric geometry to find the closest point on this
     *                curve to (guaranteed to be the same physical dimension before this
     *                method is called).
     * @param eval    A function that calculates the dot product of (p(s)-q) and ps(s) on
     *                the curve where "q" is the closest point on the target parametric
     *                geometry object.
     * @return A list of brackets that each surround a potential closest point on this
     *         curve.
     */
    protected List<BracketRoot1D> guessClosest(ParametricGeometry parGeom, PerpPointParGeomEvaluatable eval) {

        //  Find multiple closest/farthest points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {

            brackets = BracketRoot1D.findBrackets(requireNonNull(eval), 0, 1, 21);

        } catch (RootException err) {
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    "Failed to bracket closest points.", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Returns a list containing the closest point on this curve, p, to the closest point
     * on the target parametric geometry, q.
     *
     * @param parGeom       The parametric geometry to find the closest point on this
     *                      curve to (guaranteed to be the same physical dimension before
     *                      this method is called).
     * @param tol           Fractional tolerance (in parameter space) to refine the point
     *                      position to.
     * @param perpPointEval A function that calculates the dot product of (p(s)-q) and
     *                      ps(s) on the curve where "q" is the closest point on the
     *                      target parametric geometry object.
     * @param brackets      A list of brackets of roots to the perpPointEval function each
     *                      of which could be the closest/farthest.
     * @return A list containing SubrangePoint objects representing the closest points on
     *         this curve (index 0) and the target parametric geometry (index 1).
     */
    private PointString<SubrangePoint> getClosest(ParametricGeometry parGeom, double tol, PerpPointParGeomEvaluatable perpPointEval,
            List<BracketRoot1D> brackets) {
        GeomPoint thisParPos, thatParPos;

        StackContext.enter();
        try {
            SubrangePoint thisOutput, thatOutput;
            
            //  Create a list of potential closest points and add the ends of this curve to it.
            FastTable<SubrangePoint> potentials = FastTable.newInstance();
            potentials.add(SubrangePoint.newInstance(this, 0));
            potentials.add(SubrangePoint.newInstance(this, 1));

            try {
                //  Loop over all the brackets found.
                for (BracketRoot1D bracket : brackets) {
                    //  Find the perpendicular point on this curve using the evaluator function.
                    double s = Roots.findRoot1D(perpPointEval, bracket, tol);

                    //  Add the point to the list of potential closest/farthest points being collected.
                    potentials.add(SubrangePoint.newInstance(this, s));
                }

            } catch (RootException err) {
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                        "Failed to find perpendicular points on curve.", err);

            } finally {
                if (potentials.isEmpty()) {
                    thisOutput = getPoint(0);
                    thatOutput = parGeom.getClosest(thisOutput, tol);

                } else {
                    //  Loop over the potentials and find the one that is the closest.
                    thisOutput = potentials.get(0);
                    thatOutput = parGeom.getClosest(thisOutput, tol);
                    double dOpt = thatOutput.distanceValue(thisOutput);
                    int size = potentials.size();
                    for (int i = 1; i < size; ++i) {
                        SubrangePoint p = potentials.get(i);
                        SubrangePoint q = parGeom.getClosest(p, tol);
                        double dist = q.distanceValue(p);
                        if (dist < dOpt) {
                            dOpt = dist;
                            thisOutput = p;
                            thatOutput = q;
                        }
                    }
                }
            }
            
            //  Copy out the parametric positions of the closest points.
            thisParPos = StackContext.outerCopy(thisOutput.getParPosition().immutable());
            thatParPos = StackContext.outerCopy(thatOutput.getParPosition().immutable());
            
        } finally {
            StackContext.exit();
        }

        //  Turn parametric positions back into actual subrange points.
        PointString<SubrangePoint> output = PointString.newInstance();
        output.add(SubrangePoint.newInstance(this, thisParPos));
        output.add(SubrangePoint.newInstance(parGeom, thatParPos));

        return output;
    }

    /**
     * Returns the closest points (giving the minimum distance) between this curve and the
     * specified surface. If this curve intersects the specified surface, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If this curve is parallel to
     * the specified surface (all closest points are equidistant away), then any point on
     * this curve and the closest point on the target surface will be returned as
     * "closest".
     *
     * @param surface The surface to find the closest points between this curve and that
     *                surface.
     * @param tol     Fractional tolerance (in parameter space) to refine the point
     *                position to.
     * @return A list containing two SubrangePoint objects that represent the closest
     *         point on this curve (index 0) and the closest point on the specified
     *         surface (index 1).
     */
    @Override
    public PointString<SubrangePoint> getClosest(Surface surface, double tol) {

        //  Make sure that surface and this curve have the same dimensions.
        int numDims = getPhyDimension();
        if (surface.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "surface", "curve"));
        else if (surface.getPhyDimension() < numDims)
            surface = surface.toDimension(numDims);

        //  Create a function that calculates the dot product of (p-q) and ps where "q" is the closest
        //  point on the target surface.  When this is zero, a point on the curve that forms a
        //  vector perpendicular to the curve has been found.
        PerpPointParGeomEvaluatable perpPointEval = PerpPointParGeomEvaluatable.newInstance(this, surface, tol);

        //  Find brackets surrounding possible closest/farthest points.
        List<BracketRoot1D> brackets = guessClosest(surface, perpPointEval);

        //  Find the closest point.
        PointString<SubrangePoint> output = getClosest(surface, tol, perpPointEval, brackets);

        //  Recycle temporary objects.
        PerpPointParGeomEvaluatable.recycle(perpPointEval);

        return output;
    }

    /**
     * Returns the closest points (giving the minimum distance) between this curve and the
     * specified plane. If this curve intersects the specified plane, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If this curve is parallel to
     * the specified plane (all closest points are equidistant away), then any point on
     * this curve and the closest point on the target plane will be returned as "closest".
     *
     * @param plane The plane to find the closest points between this curve and that
     *              plane.
     * @param tol   Fractional tolerance (in parameter space) to refine the point position
     *              to.
     * @return The closest point found on this curve to the specified plane.
     */
    @Override
    public SubrangePoint getClosest(GeomPlane plane, double tol) {

        //  Make sure that the input plane and this curve have the same dimensions.
        int numDims = getPhyDimension();
        if (plane.getPhyDimension() > numDims)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("sameOrFewerDimErr"), "plane", "curve"));
        else if (plane.getPhyDimension() < numDims)
            plane = plane.toDimension(numDims);

        //  Create a function that calculates the dot product of (p-q) and ps where "q" is the closest
        //  point on the target plane.  When this is zero, a point on the curve that forms a
        //  vector perpendicular to the curve has been found.
        PerpPointPlaneEvaluatable perpPointEval = PerpPointPlaneEvaluatable.newInstance(this, plane);

        //  Find brackets surrounding possible closest/farthest points.
        List<BracketRoot1D> brackets = guessClosest(perpPointEval);

        //  Find the closest point.
        SubrangePoint output = getClosest(plane, tol, perpPointEval, brackets);

        //  Recycle temporary objects.
        PerpPointPlaneEvaluatable.recycle(perpPointEval);

        return output;
    }

    /**
     * Return a list of brackets that surround possible points on this curve that are
     * closest to the target geometry object. Each of the brackets will be refined using a
     * root solver in order to find the actual closest points.
     * <p>
     * The default implementation sub-divides the curve into 21 equal-sized segments and
     * looks for a single bracket inside each segment. Sub-classes should override this
     * method if they can provide a more efficient/robust method to locate closest
     * points.
     * </p>
     *
     * @param eval A function that calculates the dot product of (p(s)-q) and ps(s) on the
     *             curve where "q" is the closest point on the target geometry object.
     * @return A list of brackets that each surround a potential closest point on this
     *         curve.
     */
    protected List<BracketRoot1D> guessClosest(PerpPointPlaneEvaluatable eval) {

        //  Find multiple closest/farthest points (roots) by sub-dividing curve into pieces and
        //  bracketing any roots that occur in any of those segments.
        List<BracketRoot1D> brackets;
        try {
            brackets = BracketRoot1D.findBrackets(requireNonNull(eval), 0, 1, 21);

        } catch (RootException err) {
            Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                    "Failed to bracket closest points", err);
            return FastTable.newInstance();
        }

        return brackets;
    }

    /**
     * Returns a list containing the closest point on this curve, p, to the closest point
     * on the target plane, q.
     *
     * @param plane         The plane to find the closest point on this curve to
     *                      (guaranteed to be the same physical dimension before this
     *                      method is called).
     * @param tol           Fractional tolerance (in parameter space) to refine the point
     *                      position to.
     * @param perpPointEval A function that calculates the dot product of (p(s)-q) and
     *                      ps(s) on the curve where "q" is the closest point on the
     *                      target plane object.
     * @param brackets      A list of brackets of roots to the perpPointEval function each
     *                      of which could be the closest/farthest.
     * @return The closest point found on this curve to the specified plane.
     */
    private SubrangePoint getClosest(GeomPlane plane, double tol, PerpPointPlaneEvaluatable perpPointEval,
            List<BracketRoot1D> brackets) {
        double sOutput;
        
        StackContext.enter();
        try {
            SubrangePoint thisOutput;

            //  Create a list of potential closest points and add the ends of this curve to it.
            FastTable<SubrangePoint> potentials = FastTable.newInstance();
            potentials.add(SubrangePoint.newInstance(this, 0));
            potentials.add(SubrangePoint.newInstance(this, 1));

            //  Loop over all the brackets found.
            for (BracketRoot1D bracket : brackets) {
                try {
                    //  Find the perpendicular point on this curve using the evaluator function.
                    double s = Roots.findRoot1D(perpPointEval, bracket, tol);

                    //  Add the point to the list of potential closest/farthest points being collected.
                    potentials.add(SubrangePoint.newInstance(this, s));

                } catch (RootException e) {
                    //  Just ignore.
                    //e.printStackTrace();
                }
            }

            if (potentials.isEmpty()) {
                thisOutput = getPoint(0);

            } else {
                //  Loop over the potentials and find the one that is the closest.
                thisOutput = potentials.get(0);
                GeomPoint q = plane.getClosest(thisOutput);
                double dOpt = q.distanceValue(thisOutput);
                int size = potentials.size();
                for (int i = 1; i < size; ++i) {
                    SubrangePoint p = potentials.get(i);
                    q = plane.getClosest(p);
                    double dist = q.distanceValue(p);
                    if (dist < dOpt) {
                        dOpt = dist;
                        thisOutput = p;
                    }
                }
            }

            //  Get the parametric position on this curve of the closest point.
            sOutput = thisOutput.getParPosition().getValue(0);

        } finally {
            StackContext.exit();
        }
        
        return getPoint(sOutput);
    }

    /**
     * Returns the most extreme point, either minimum or maximum, in the specified
     * coordinate direction on this curve. This is more accurate than
     * <code>getBoundsMax</code> & <code>getBoundsMin</code>, but also typically takes
     * longer to compute.
     *
     * @param dim An index indicating the dimension to find the min/max point for (0=X,
     *            1=Y, 2=Z, etc).
     * @param max Set to <code>true</code> to return the maximum value, <code>false</code>
     *            to return the minimum.
     * @param tol Fractional tolerance (in parameter space) to refine the min/max point
     *            position to.
     * @return The point found on this curve that is the min or max in the specified
     *         coordinate direction.
     * @see #getBoundsMin
     * @see #getBoundsMax
     */
    @Override
    public SubrangePoint getLimitPoint(int dim, boolean max, double tol) {
        //  Check the input dimension for consistancy.
        int thisDim = getPhyDimension();
        if (dim < 0 || dim >= thisDim)
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("inputDimOutOfRange"), "curve"));

        double sout = 0;
        StackContext.enter();
        try {
            //  Get the bounds of the curve.
            Point boundsMax = getBoundsMax();
            Point boundsMin = getBoundsMin();
            Parameter<Length> range = boundsMax.get(dim).minus(boundsMin.get(dim));

            if (thisDim > 2) {
                //  We have at least 3 dimensions.

                //  Create a vector pointing in the indicated direction.
                MutablePoint p = MutablePoint.newInstance(thisDim);
                p.set(dim, Parameter.valueOf(1, SI.METER));
                Vector<Dimensionless> uv = Vector.valueOf(p).toUnitVector();

                //  Bias the bounds off 1% in the specified dimension
                if (max) {
                    p.set(boundsMax);
                    p.set(dim, p.get(dim).plus(range.times(0.01)));
                } else {
                    p.set(boundsMin);
                    p.set(dim, p.get(dim).minus(range.times(0.01)));
                }

                //  Create a plane 1% of the diagonal off the bounds.
                Plane plane = Plane.valueOf(uv, p.immutable());

                //  Return the closest point to the specified plane.
                SubrangePoint pnt = getClosest(plane, tol);
                sout = pnt.getParPosition().getValue(0);

            } else if (thisDim == 2) {
                //  We have a 2D curve.

                //  Create two points biased off the bounds in the specified direction.
                MutablePoint p0, p1;
                if (max) {
                    p0 = MutablePoint.valueOf(boundsMax);
                    p0.set(dim, p0.get(dim).plus(range.times(0.01)));
                    p1 = MutablePoint.valueOf(boundsMin);
                    p1.set(dim, p0.get(dim));
                } else {
                    p0 = MutablePoint.valueOf(boundsMin);
                    p0.set(dim, p0.get(dim).minus(range.times(0.01)));
                    p1 = MutablePoint.valueOf(boundsMax);
                    p1.set(dim, p0.get(dim));
                }

                //  Create a line on the desired side of the curve.
                LineSeg seg = LineSeg.valueOf(p0.immutable(), p1.immutable());

                //  Find the closest point between this curve the line segment.
                PointString<SubrangePoint> str = getClosest(seg, tol);
                SubrangePoint p = str.getFirst();
                sout = p.getParPosition().getValue(0);

            } else {
                //  We have a 1 dimensional curve.

                //  Bias the bounds off 1% in the specified dimension
                Point pp;
                if (max) {
                    pp = boundsMax.plus(range.times(0.01));
                } else {
                    pp = boundsMin.minus(range.times(0.01));
                }

                //  Return the closest point to the specified point.
                sout = getClosest(pp, tol).getParPosition().getValue(0);
            }
        } finally {
            StackContext.exit();
        }

        return getPoint(sout);
    }

    /**
     * Return the signed area of the region enclosed or subtended by a planar curve
     * relative to the specified reference point. This method assumes that the curve is
     * planar and will give incorrect results if the curve is not planar. Also, the input
     * reference point is assumed to lie in the plane of the curve, it if is not, this
     * will give incorrect results. If the curve is closed, then the reference point is
     * arbitrary (as long as it is in the plane of the curve). If the curve is open, then
     * the area returned is that subtended by the curve with straight lines from each end
     * of the curve to the input point.
     *
     * @param refPnt The reference point used for computing the enclosed or subtended area
     *               of the curve.
     * @param eps    The desired fractional accuracy of the area calculated.
     * @return The area enclosed or subtended by this curve (assuming that this curve is
     *         planar and that the reference point lies in the plane of this curve).
     */
    @Override
    public Parameter<Area> getEnclosedArea(GeomPoint refPnt, double eps) {
        Unit<Area> areaUnit = getUnit().pow(2).asType(Area.class);
        
        //  If we only have 1 dimension, then there can be no area.
        int numDims = getPhyDimension();
        if (numDims == 1)
            return Parameter.ZERO_AREA.to(areaUnit);

        double area = 0;
        StackContext.enter();
        try {
            //  Make sure the point and curve have the same dimensions.
            if (refPnt.getPhyDimension() > numDims) {
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "refPnt", "curve"));
            } else if (refPnt.getPhyDimension() < numDims) {
                refPnt = refPnt.toDimension(numDims);
            }

            //  Convert the reference point to the same units as this curve.
            refPnt = refPnt.to(getUnit());

            EnclAreaEvaluatable areaEvaluator = EnclAreaEvaluatable.newInstance(this, refPnt);
            try {
                //  Integrate to find the subtended area:   A = int_0^1{ p0p(t) x p'(t) ds}
                area = Quadrature.adaptLobatto(areaEvaluator, 0, 1, eps) * 0.5;

            } catch (RootException | IntegratorException e) {
                //  Fall back on Gaussian Quadrature.
                Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                        "Trying Gaussian Quadrature in getEnclosedArea()...");
                try {
                    area = Quadrature.gaussLegendre_Wx1N40(areaEvaluator, 0, 1) * 0.5;

                } catch (RootException err) {
                    Logger.getLogger(AbstractCurve.class.getName()).log(Level.WARNING,
                            "Could not calculate enclosed area; using 0.", err);
                    area = 0;
                }
            }
        } finally {
            StackContext.exit();
        }

        return Parameter.valueOf(area, areaUnit);
    }

    /**
     * Return <code>true</code> if this curve is degenerate (i.e.: has length less than
     * the specified tolerance).
     *
     * @param tol The tolerance for determining if this curve is degenerate. May not be
     *            null.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol) {
        requireNonNull(tol);
        Parameter<Length> cLength = getArcLength(GTOL * 100);
        return cLength.compareTo(tol) <= 0;
    }

    /**
     * Return <code>true</code> if this curve is planar or <code>false</code> if it is
     * not.
     *
     * @param tol The geometric tolerance to use in determining if the curve is planar.
     */
    @Override
    public boolean isPlanar(Parameter<Length> tol) {
        //  If the curve is less than 3D, then it must be planar.
        int numDims = getPhyDimension();
        if (numDims <= 2)
            return true;

        //  Is this curve degenerate?
        if (isDegenerate(tol))
            return true;

        //  Is the curve linear?
        if (isLine(tol))
            return true;

        StackContext.enter();
        try {
            //  Create a list of segment starting par. positions (and the last end position, 1).
            FastTable<Double> segParams = getSegmentParameters();

            //  Form a plane from three points on the curve.
            Point p0 = getRealPoint(0);
            GeomVector<Dimensionless> nhat = getPlaneNormal(p0);

            // Number of points extracted on each segment to determine type.
            int nPnts = getNumPointsPerSegment();
            int nPntsm1 = nPnts - 1;

            //  Loop over all the segments.
            int numSegs = segParams.size() - 1;
            for (int seg = 0; seg < numSegs; ++seg) {

                //  Sample at 2*(p+1) points on each segment.
                double sSeg = segParams.get(seg);
                double sNext = segParams.get(seg + 1);
                double sRange = sNext - sSeg;
                for (int i = 0; i < nPntsm1; ++i) {
                    double s = sSeg + (i * sRange) / nPntsm1;
                    Point pi = getRealPoint(s);

                    //  Find deviation from a plane for each point by projecting a vector from p0 to pi
                    //  onto the normal for the arbitrarily chosen points on the curve.
                    //  If the project is anything other than zero, the points are not
                    //  in the same plane.
                    Vector<Length> p1pi = pi.minus(p0).toGeomVector();
                    if (!p1pi.mag().isApproxZero()) {
                        Parameter proj = p1pi.dot(nhat);
                        if (proj.isLargerThan(tol))
                            return false;
                    }
                }
            }

            //  Must be planar.
            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a normal vector for the curve from an input point and 2 arbitrarily chosen
     * points on a > 2D curve. If the input point is in the plane of the curve, then this
     * will return the normal vector for the plane of the curve.
     */
    private Vector<Dimensionless> getPlaneNormal(GeomPoint p0) {
        StackContext.enter();
        try {
            Vector p0v = p0.toGeomVector();

            Point p1 = getRealPoint(0.38196601125);
            Point p2 = getRealPoint(0.61803398875);
            Vector<Length> p1v = p1.toGeomVector();
            Vector<Length> p2v = p2.toGeomVector();
            Vector<Length> v1 = p1v.minus(p0v);
            Vector<Length> v2 = p2v.minus(p0v);
            Vector n = v1.cross(v2);
            Vector<Dimensionless> nhat;
            if (n.magValue() > GTOL) {
                nhat = n.toUnitVector();
                nhat.setOrigin(Point.newInstance(getPhyDimension()));
            } else {
                //  Try a different set of points on the curve.
                p1 = getRealPoint(0.25);
                p2 = getRealPoint(0.5);
                p1v = p1.toGeomVector();
                p2v = p2.toGeomVector();
                v1 = p1v.minus(p0v);
                v2 = p2v.minus(p0v);
                n = v1.cross(v2);
                if (n.magValue() > GTOL) {
                    nhat = n.toUnitVector();
                    nhat.setOrigin(Point.newInstance(getPhyDimension()));
                } else {
                    //  Points from input curve form a straight line.
                    nhat = GeomUtil.calcYHat(p1v.minus(p2v).toUnitVector());
                    nhat = v2.cross(nhat).toUnitVector();
                }
            }

            return StackContext.outerCopy(nhat);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns <code>true</code> if this curve is a line to within the specified
     * tolerance.
     *
     * @param tol The tolerance for determining if this curve is a line.
     */
    @Override
    public boolean isLine(Parameter<Length> tol) {
        //  Reference:  Piegl, L.A., Tiller, W., "Computing Offsets of NURBS Curves and Surfaces",
        //              Computer Aided Design 31, 1999, pgs. 147-156.
        requireNonNull(tol);
        
        StackContext.enter();
        try {
            //  Extract the curve end points.
            Point p0 = getRealPoint(0), p1 = getRealPoint(1);

            //  If the end points are coincident, then it can't be a line.
            Vector<Length> tv = p1.minus(p0).toGeomVector();
            if (!tv.mag().isGreaterThan(tol))
                return false;

            //  Get a unit vector for the potential line.
            Vector<Dimensionless> uv = tv.toUnitVector();

            //  Create a list of segment starting par. positions (and the last end position, 1).
            FastTable<Double> bParams = getSegmentParameters();

            // Number of points extracted on each segment to determine type.
            int nPnts = getNumPointsPerSegment();
            int nPntsm1 = nPnts - 1;

            //  Loop over all the segments.
            Parameter c2 = tv.dot(tv);
            int numSegs = bParams.size() - 1;
            for (int seg = 0; seg < numSegs; ++seg) {

                //  Sample at nPnts points on each segment.
                double sSeg = bParams.get(seg);
                double sNext = bParams.get(seg + 1);
                double sRange = sNext - sSeg;
                for (int i = 0; i < nPntsm1; ++i) {
                    double s = sSeg + (i * sRange) / nPntsm1;
                    Point pi = getRealPoint(s);

                    //  Check distance of this point from infinite line p0-uv.
                    if (GeomUtil.pointLineDistance(pi, p0, uv).isGreaterThan(tol))
                        return false;

                    //  See if the point falls in the segment p0 to p1.
                    Vector<Length> w = pi.minus(p0).toGeomVector();
                    Parameter c1 = w.dot(tv);
                    if (c1.getValue() < 0)
                        return false;
                    if (c2.isLessThan(c1))
                        return false;
                }

            }

            //  Must be a straight line.
            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns <code>true</code> if this curve is a circular arc to within the specified
     * tolerance.
     *
     * @param tol The tolerance for determining if this curve is a circular arc.
     */
    @Override
    public boolean isCircular(Parameter<Length> tol) {
        //  Reference:  Piegl, L.A., Tiller, W., "Computing Offsets of NURBS Curves and Surfaces",
        //              Computer Aided Design 31, 1999, pgs. 147-156.

        //  Can't be degenerate.
        if (isDegenerate(tol))
            return false;

        //  Must be planar.
        if (!isPlanar(tol))
            return false;

        StackContext.enter();
        try {
            //  Create a list of segment starting par. positions (and the last end position, 1).
            FastTable<Double> bParams = getSegmentParameters();

            //  Get the curvature and principal normal at the start of the curve.
            Parameter k0 = getCurvature(0);
            if (k0.isApproxZero())
                return false;
            Vector n0 = getPrincipalNormal(0);

            //  Compute the center of the circle.
            Point pc0 = Point.valueOf(n0.divide(k0)).plus(n0.getOrigin());

            // Number of points extracted on each segment to determine type.
            int nPnts = getNumPointsPerSegment();
            int nPntsm1 = nPnts - 1;

            //  Loop over all the segments.
            int numSegs = bParams.size() - 1;
            for (int seg = 0; seg < numSegs; ++seg) {

                //  Sample at nPnts points on each segment.
                double sSeg = bParams.get(seg);
                double sNext = bParams.get(seg + 1);
                double sRange = sNext - sSeg;
                for (int i = 0; i < nPntsm1; ++i) {
                    double s = sSeg + (i * sRange) / nPntsm1;

                    //  Get the local curvature and principal normal vector.
                    Parameter k = getCurvature(s);
                    if (k.isApproxZero())
                        return false;
                    Vector n = getPrincipalNormal(s);

                    //  Compute the local center of curvature.
                    Point pc = Point.valueOf(n.divide(k)).plus(n.getOrigin());
                    Parameter<Length> dist = pc0.distance(pc);
                    if (dist.isGreaterThan(tol))
                        return false;
                }
            }

            //  The curve must be circular.
            return true;

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a list containing parameters at the start of logical segments of this curve.
     * The first element in this list must always be 0.0 and the last element 1.0. These
     * should be good places to break the curve up into pieces for analysis, but otherwise
     * are arbitrary. Subclasses should override this method to provide better segment
     * starting parameters.
     *
     * The default implementation always splits the curve into two pieces and returns the
     * following parameters [0, 0.50, 1].
     *
     * @return A list containing parameters at the start of logical segments of this
     *         curve.
     */
    protected FastTable<Double> getSegmentParameters() {
        FastTable<Double> knots = FastTable.newInstance();
        knots.add(0.0);
        knots.add(0.5);
        knots.add(1.0);
        return knots;
    }

    /**
     * Return the number of points per segment that should be used when analyzing curve
     * segments by certain methods. Subclasses should override this method to provide a
     * more appropriate number of points per segment.
     *
     * The default implementation always returns 8.
     *
     * @return The number of points per segment that should be used when analyzing curve
     *         segments by certain methods.
     */
    protected int getNumPointsPerSegment() {
        return 8;   //  2*(3 + 1)
    }

    /**
     * Converts the input parametric position on a curve segment with the specified range
     * of parametric positions into a parametric position on the parent curve.
     *
     * @param s  The parametric position on the curve segment to be converted.
     * @param s0 The starting parametric position of the curve segment on the parent
     *           curve.
     * @param s1 The ending parametric position of the curve segment on the parent curve.
     * @return The input segment parametric position converted to the parent curve.
     */
    protected static double segmentPos2Parent(double s, double s0, double s1) {
        double s_out = s0 + (s1 - s0) * s;
        if (s_out > 1) {            //  Deal with roundoff issues.
            s_out = 1;
        } else if (s_out < 0) {
            s_out = 0;
        }
        return s_out;
    }

    /**
     * An Evaulatable1D that returns the magnitude of the tangent vector at the specified
     * parametric location. This is used to find the arc length of a curve. This is
     * <code>f(x)</code> in:
     * <code>L = int_0^1 {f(x)dx} = int_0^1 { sqrt(ps(s) DOT ps(s)) ds }</code>.
     */
    private static class ArcLengthEvaluatable extends AbstractEvaluatable1D {

        private Curve _curve;

        public static ArcLengthEvaluatable newInstance(Curve curve) {
            if (curve instanceof GeomTransform)
                curve = curve.copyToReal();
            ArcLengthEvaluatable o = FACTORY.object();
            o._curve = curve;
            return o;
        }

        @Override
        public double function(double s) throws RootException {
            StackContext.enter();
            try {
                Vector<Length> ps = _curve.getSDerivative(s, 1);        //  The dimensional tangent vector.
                double psv = ps.normValue();                            //  sqrt(ps DOT ps) = |ps|
                return psv;
            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(ArcLengthEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private ArcLengthEvaluatable() { }

        private static final ObjectFactory<ArcLengthEvaluatable> FACTORY = new ObjectFactory<ArcLengthEvaluatable>() {
            @Override
            protected ArcLengthEvaluatable create() {
                return new ArcLengthEvaluatable();
            }
        };

    }

    /**
     * An Evaulatable1D that is used to find a point on a curve that has a tangent vector
     * that points to the input point.
     */
    private static class TangentPointEvaluatable extends AbstractEvaluatable1D {

        private Curve _curve;
        private Point _q;

        public static TangentPointEvaluatable newInstance(Curve curve, GeomPoint q) {
            if (curve instanceof GeomTransform)
                curve = curve.copyToReal();
            TangentPointEvaluatable o = FACTORY.object();
            o._curve = curve;
            o._q = q.immutable();
            return o;
        }

        /**
         * Calculates: 1 - ((q - p(s))/|q - p(s)|) DOT t(s) where t(s) is the tangent
         * vector on the curve at s, p(s) is the point on the curve at s and q is the
         * point we are finding a tangent on the curve to.
         */
        @Override
        public double function(double s) throws RootException {
            StackContext.enter();
            try {

                Vector<Dimensionless> t = _curve.getTangent(s);
                Point p = _curve.getRealPoint(s);
                Vector pq = _q.minus(p).toGeomVector();
                double tDOTpq = 0;
                if (!pq.norm().isApproxZero()) {
                    pq = pq.toUnitVector();
                    tDOTpq = t.dot(pq).getValue();
                }
                return 1.0 - MathLib.abs(tDOTpq);

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(TangentPointEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private TangentPointEvaluatable() { }

        private static final ObjectFactory<TangentPointEvaluatable> FACTORY = new ObjectFactory<TangentPointEvaluatable>() {
            @Override
            protected TangentPointEvaluatable create() {
                return new TangentPointEvaluatable();
            }
        };
    }

    /**
     * An Evaulatable1D that is used to find a point on a curve that has the specified arc
     * length.
     */
    private static class ArcPosEvaluatable extends AbstractEvaluatable1D {

        private Curve _curve;
        private double _eps;
        public boolean firstPass;

        /**
         * Return a new ArcPosEvaluatable function.
         *
         * @param curve        The curve to find the arc-length along.
         * @param targetLength The target length, in meters, to search for.
         * @param eps          The desired fractional accuracy on the arc-length.
         */
        public static ArcPosEvaluatable newInstance(Curve curve, double targetLength, double eps) {
            if (curve instanceof GeomTransform)
                curve = curve.copyToReal();

            ArcPosEvaluatable o = FACTORY.object();
            o._curve = curve.to(SI.METER);  //  Convert everything to meters.
            o._eps = eps;
            o.setInitialValue(targetLength);
            o.firstPass = false;

            return o;
        }

        /**
         * Calculates: arcLength(s) - targetArcLength. When this is zero, then the arc
         * length position has been found.
         */
        @Override
        public double function(double s) throws RootException {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }
            StackContext.enter();
            try {

                double arcLength = _curve.getArcLength(0, s, _eps).getValue();
                return arcLength - getInitialValue();

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(ArcPosEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private ArcPosEvaluatable() { }

        private static final ObjectFactory<ArcPosEvaluatable> FACTORY = new ObjectFactory<ArcPosEvaluatable>() {
            @Override
            protected ArcPosEvaluatable create() {
                return new ArcPosEvaluatable();
            }
        };
    }

    /**
     * An Evaulatable1D that is used to find the intersection of a curve and a plane.
     */
    private static class PlaneIntersectEvaluatable implements Evaluatable1D {

        private Curve _curve;
        private GeomPlane _plane;
        public boolean firstPass;

        public static PlaneIntersectEvaluatable newInstance(Curve curve, GeomPlane plane) {
            if (curve instanceof GeomTransform)
                curve = curve.copyToReal();

            //  Create the object.
            PlaneIntersectEvaluatable o = FACTORY.object();
            o._curve = curve;
            o._plane = plane;
            o.firstPass = false;

            return o;
        }

        /**
         * Calculates: <code>n dot (p(s) - pp)</code> which is the signed distance from
         * the point to the plane. When this is zero, then an intersection has been found.
         */
        @Override
        public double function(double s) throws RootException {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                return 0;
            }

            StackContext.enter();
            try {

                Point ps = _curve.getRealPoint(s);
                GeomPoint pp = _plane.getRefPoint();
                Vector<Length> psmpp = ps.minus(pp).toGeomVector();
                GeomVector n = _plane.getNormal();

                Parameter d = n.dot(psmpp);

                return d.getValue();

            } finally {
                StackContext.exit();
            }
        }

        /**
         * Calculates: <code>d(n dot (p(s) - pp))/ds</code> which is the slope of the
         * signed distance from the point to the plane wrt to s.
         */
        @Override
        public double derivative(double s) throws RootException {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }

            StackContext.enter();
            try {

                Vector dpds = _curve.getSDerivative(s, 1);
                GeomVector n = _plane.getNormal();

                Parameter d = n.dot(dpds);

                return d.getValue();

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(PlaneIntersectEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private PlaneIntersectEvaluatable() { }

        private static final ObjectFactory<PlaneIntersectEvaluatable> FACTORY = new ObjectFactory<PlaneIntersectEvaluatable>() {
            @Override
            protected PlaneIntersectEvaluatable create() {
                return new PlaneIntersectEvaluatable();
            }
        };
    }

    /**
     * An Evaulatable1D that is used to find a parametric position on a curve that forms a
     * vector with the target point that is perpendicular to the curves tangent vector at
     * that position. Such a perpendicular vector indicates a point that is locally either
     * the closest or furthest away from the target point. This is then used with a 1D
     * root finder to isolate all the perpendicular points on the curve.
     */
    protected static class PerpPointEvaluatable implements Evaluatable1D {

        private Curve _curve;
        private Vector<Length> _point;
        public boolean firstPass;
        private Vector<Length> _pmq;
        private List<Vector<Length>> _ders;

        public static PerpPointEvaluatable newInstance(Curve curve, GeomPoint point) {
            if (curve instanceof GeomTransform)
                curve = curve.copyToReal();

            PerpPointEvaluatable o = FACTORY.object();
            o._curve = curve;
            o._point = point.toGeomVector();
            o.firstPass = false;

            return o;
        }

        /**
         * Calculates (p(s) - q) DOT ps(s) (dot product between the vector formed by the
         * target point and a point on the curve at "s" and the tangent vector at "s".
         * Where this is zero, a perpendicular point has been found. A perpendicular point
         * is either the closest or furthest away from the target point in a local region.
         *
         * @param s The parametric position on the curve being evaluated.
         */
        @Override
        public double function(double s) {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                return 0;
            }

            //  Get derivatives up to 2nd order.  These are also used in derivative().
            _ders = _curve.getSDerivatives(s, 2);

            //  Calculate: p(s) - q.  This is also used in derivative().
            Vector<Length> p = _ders.get(0);
            _pmq = p.minus(_point);
            if (_pmq.mag().isApproxZero())
                return 0;

            Vector ps = _ders.get(1);
            Parameter cosa = _pmq.dot(ps);  //  Projections of (p-q) onto tangent vector.

            return cosa.getValue();
        }

        /**
         * Calculates: <code>d((p(s) - q) DOT ps(s))/ds</code> which is the slope of the
         * project of the target point to curve point vector onto the tangent vector.
         *
         * @param s The parametric position on the curve being evaluated.
         */
        @Override
        public double derivative(double s) {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }

            /*  d(fg) = df*g + f*dg
             d(p(s) - q)/ds = ps(s);    d(ps(s))/ds = pss(s)
             d((p(s) - q) DOT ps(s))/ds = ps(s) DOT ps(s) + (p(s) - q) DOT pss(s)
             ((p(s) - q) DOT ps(s))/ds = |ps(s)|^2 + (p(s) - q) DOT pss(s)
             Derivatives and p(s) - q calculated in "function()".
             */
            Vector ps = _ders.get(1);
            Vector pss = _ders.get(2);
            FastTable.recycle((FastTable)_ders);

            Parameter value = ps.dot(ps).plus(_pmq.dot(pss));

            Vector.recycle(_pmq);
            Vector.recycle(ps);
            Vector.recycle(pss);

            return value.getValue();
        }

        public static void recycle(PerpPointEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private PerpPointEvaluatable() { }

        private static final ObjectFactory<PerpPointEvaluatable> FACTORY = new ObjectFactory<PerpPointEvaluatable>() {
            @Override
            protected PerpPointEvaluatable create() {
                return new PerpPointEvaluatable();
            }

            @Override
            protected void cleanup(PerpPointEvaluatable obj) {
                obj._curve = null;
                obj._point = null;
                obj._pmq = null;
                obj._ders = null;
            }
        };

    }

    /**
     * An Evaulatable1D that is used to find a parametric position on a curve that forms a
     * vector with the target point (which is the closest point on the target curve or
     * surface) that is perpendicular to the curve's tangent vector at that position. Such
     * a perpendicular vector indicates a point that is locally either the closest or
     * furthest away from the target point. This is then used with a 1D root finder to
     * isolate all the potential nearest/farthest points on the two curves.
     */
    protected static class PerpPointParGeomEvaluatable extends AbstractEvaluatable1D {

        private Curve _thisCurve;
        private ParametricGeometry _thatParGeom = null;
        private double _tol;
        public boolean firstPass;

        public static PerpPointParGeomEvaluatable newInstance(Curve thisCurve, ParametricGeometry thatParGeom, double tol) {
            if (thisCurve instanceof GeomTransform)
                thisCurve = thisCurve.copyToReal();

            PerpPointParGeomEvaluatable o = FACTORY.object();
            o._thisCurve = thisCurve;
            o._thatParGeom = thatParGeom;
            o._tol = tol;
            o.firstPass = false;

            return o;
        }

        /**
         * Calculates (p(s) - q) DOT ps(s) (dot product between the vector formed by the
         * target point (closest point on target parametric geometry) and a point on this
         * curve at "s" and the tangent vector at "s". Where this is zero, a perpendicular
         * point has been found. A perpendicular point is either the closest or furthest
         * away from the target point in a local region.
         *
         * @param s The parametric position on the curve being evaluated.
         * @return The dot product between the vector formed by the target point and a
         *         point on this curve.
         */
        @Override
        public double function(double s) {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }

            StackContext.enter();
            try {
                //  Get derivatives up to 2nd order.
                FastTable<Vector> ders = (FastTable)_thisCurve.getSDerivatives(s, 2);

                //  Extract the point on this curve at "s".
                Vector<Length> p = ders.get(0);
                Point pnt = Point.valueOf(p);

                //  Get the closest point on the target parametric geometry.
                Vector<Length> q = _thatParGeom.getClosest(pnt, _tol).toGeomVector();

                //  Calculate: p(s) - q.
                Vector<Length> pmq = p.minus(q);
                if (pmq.mag().isApproxZero()) {
                    return 0;
                }

                Vector ps = ders.get(1);
                Parameter cosa = pmq.dot(ps);   //  Projection of (p-q) onto tangent vector.

                return cosa.getValue();

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(PerpPointParGeomEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private PerpPointParGeomEvaluatable() { }

        private static final ObjectFactory<PerpPointParGeomEvaluatable> FACTORY = new ObjectFactory<PerpPointParGeomEvaluatable>() {
            @Override
            protected PerpPointParGeomEvaluatable create() {
                return new PerpPointParGeomEvaluatable();
            }
        };

    }

    /**
     * An Evaulatable1D that is used to find a parametric position on a curve that forms a
     * vector with the target point (which is the closest point on the target plane) that
     * is perpendicular to the curve's tangent vector at that position. Such a
     * perpendicular vector indicates a point that is locally either the closest or
     * furthest away from the target point. This is then used with a 1D root finder to
     * isolate all the potential nearest/farthest points on the curve to the plane.
     */
    protected static class PerpPointPlaneEvaluatable extends AbstractEvaluatable1D {

        private Curve _thisCurve;
        private GeomPlane _thatPlane = null;
        public boolean firstPass;

        public static PerpPointPlaneEvaluatable newInstance(Curve thisCurve, GeomPlane thatPlane) {
            if (thisCurve instanceof GeomTransform)
                thisCurve = thisCurve.copyToReal();

            PerpPointPlaneEvaluatable o = FACTORY.object();
            o._thisCurve = thisCurve;
            o._thatPlane = thatPlane;
            o.firstPass = false;

            return o;
        }

        /**
         * Calculates (p(s) - q) DOT ps(s) (dot product between the vector formed by the
         * target point (closest point on target plane) and a point on this curve at "s"
         * and the tangent vector at "s". Where this is zero, a perpendicular point has
         * been found. A perpendicular point is either the closest or furthest away from
         * the target point in a local region.
         *
         * @param s The parametric position on this curve being evaluated.
         * @return The dot product between the vector formed by the target point and a
         *         point on this curve
         */
        @Override
        public double function(double s) {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }

            StackContext.enter();
            try {
                //  Get derivatives up to 2nd order.
                FastTable<Vector> ders = (FastTable)_thisCurve.getSDerivatives(s, 2);

                //  Extract the point on this curve at "s".
                Vector<Length> p = ders.get(0);
                Point pnt = Point.valueOf(p);

                //  Get the closest point on the target plane geometry.
                Vector<Length> q = _thatPlane.getClosest(pnt).toGeomVector();

                //  Calculate: p(s) - q.
                Vector<Length> pmq = p.minus(q);
                if (pmq.mag().isApproxZero()) {
                    return 0;
                }

                Vector ps = ders.get(1);
                Parameter cosa = pmq.dot(ps);   //  Projection of (p-q) onto tangent vector.

                return cosa.getValue();

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(PerpPointPlaneEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private PerpPointPlaneEvaluatable() { }

        private static final ObjectFactory<PerpPointPlaneEvaluatable> FACTORY = new ObjectFactory<PerpPointPlaneEvaluatable>() {
            @Override
            protected PerpPointPlaneEvaluatable create() {
                return new PerpPointPlaneEvaluatable();
            }
        };

    }

    /**
     * A class used to intersect an infinite line and a curve.
     */
    private static class LineCurveIntersect {

        private static final int MAXDEPTH = 20;
        private Curve _thisCurve;
        private Parameter<Length> _tol;
        private Point _L0;
        private Vector<Length> _Ldir;
        private FastTable<Double> _sParams;

        public static LineCurveIntersect newInstance(Curve thisCurve,
                Parameter<Length> tol, GeomPoint L0, GeomVector<Length> Ldir) {

            //  The magnitude of Ldir doesn't matter, but it can not be "Dimensionless".
            if (Ldir.getUnit().equals(Dimensionless.UNIT)) {
                Ldir = Vector.valueOf(Ldir.toFloat64Vector(), SI.METER);
            }

            if (thisCurve instanceof GeomTransform)
                thisCurve = thisCurve.copyToReal();

            //  Create an instance of this class and fill in the class data.
            LineCurveIntersect o = FACTORY.object();
            o._thisCurve = thisCurve;
            o._tol = tol.to(thisCurve.getUnit());
            o._L0 = L0.immutable().to(thisCurve.getUnit());
            o._Ldir = Ldir.immutable();
            o._sParams = FastTable.newInstance();

            return o;
        }

        /**
         * Method that actually carries out the intersection adding the intersection
         * points to the list provided in the constructor.
         *
         * @return A PointString containing zero or more subrange points made by the
         *         intersection of the curve with the specified line. If no intersection
         *         is found, an empty PointString is returned.
         */
        public PointString<SubrangePoint> intersect() {
            divideAndConquerLine(1, _thisCurve, 0, 1);

            PointString<SubrangePoint> output = PointString.newInstance();
            int size = _sParams.size();
            for (int i = 0; i < size; ++i) {
                double s = _sParams.get(i);
                output.add(SubrangePoint.newInstance(_thisCurve, s));
            }
            FastTable.recycle(_sParams);
            _sParams = null;

            return output;
        }

        /**
         * Uses a recursive "Divide and Conquer" approach to intersecting an infinite line
         * with a curve. On each call, the following happens:
         * <pre>
         *      1) The curve is checked to see if it is approx. a straight line.
         *          1b) If it is, then a line-line intersection is performed, the
         *              intersection point added to the output list and the method exited.
         *      2) The curve is divided into two halves.
         *          2b) Each half is tested to see if it's bounding box is intersected by the line.
         *              If it is, then this method is called with that segment recursively.
         *              Otherwise, the method is exited.
         * </pre>
         *
         * A number of class variables are used to pass information to this recursion:
         * <pre>
         *      _tol  The tolerance to use when determining if the curve is locally linear.
         *      _L0   A point on the line being intersected with this curve (origin of the line).
         *      _Ldir   A vector indicating the direction of the line.
         *      _sParams A list used to collect the output subrange intersection point parametric locations.
         * </pre>
         *
         * @param crv The curve segment being checked for intersection with the line.
         * @param s0  The parametric position of the start of this curve segment on the
         *            master curve.
         * @param s1  The parametric position of the end of this curve segment on the
         *            master curve.
         */
        private void divideAndConquerLine(int depth, Curve crv, double s0, double s1) {
            StackContext.enter();
            try {
                //  Check to see if the input curve is within tolerance of a straight line.
                Point p0 = crv.getRealPoint(0);
                Point p1 = crv.getRealPoint(1);
                Point pAvg = p0.plus(p1).divide(2);
                Point pm = crv.getRealPoint(0.5);
                if (depth > MAXDEPTH || pAvg.distance(pm).isLessThan(_tol)) {
                    //  The input curve segment is nearly a straight line.

                    //  Intersect the input line and the line approximating the curve.
                    Vector<Length> crvDir = p1.minus(p0).toGeomVector();
                    MutablePoint sLn = MutablePoint.newInstance(2);
                    int numDims = p0.getPhyDimension();
                    Unit<Length> unit = p0.getUnit();
                    MutablePoint pi1 = MutablePoint.newInstance(numDims, unit);
                    MutablePoint pi2 = MutablePoint.newInstance(numDims, unit);
                    GeomUtil.lineLineIntersect(_L0, _Ldir, p0, crvDir, _tol, sLn, pi1, pi2);

                    if (pi1.distance(pi2).isLessThan(_tol)) {
                        //  Add an intersection point to the list.
                        double sln = sLn.getValue(1);
                        //  Convert from segment par. pos. to full curve pos.
                        double s = segmentPos2Parent(sln, s0, s1);
                        _sParams.add(s);
                    }

                } else {
                    //  Sub-divide the curve into two segments.
                    GeomList<Curve> segments = crv.splitAt(0.5);

                    //  Check for possible intersections on the left segment.
                    Curve seg = segments.get(0);
                    if (GeomUtil.lineBoundsIntersect(_L0, _Ldir, seg, null)) {
                        //  May be an intersection.
                        double sl = s0;
                        double sh = (s0 + s1) / 2.0;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerLine(depth + 1, seg, sl, sh);
                    }

                    //  Check for possible intersections on the right segment.
                    seg = segments.get(1);
                    if (GeomUtil.lineBoundsIntersect(_L0, _Ldir, seg, null)) {
                        //  May be an intersection.
                        double sl = (s0 + s1) / 2.0;
                        double sh = s1;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerLine(depth + 1, seg, sl, sh);
                    }
                }

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(LineCurveIntersect instance) {
            FACTORY.recycle(instance);
        }

        private LineCurveIntersect() { }

        private static final ObjectFactory<LineCurveIntersect> FACTORY = new ObjectFactory<LineCurveIntersect>() {
            @Override
            protected LineCurveIntersect create() {
                return new LineCurveIntersect();
            }

            @Override
            protected void cleanup(LineCurveIntersect obj) {
                obj._thisCurve = null;
                obj._tol = null;
                obj._L0 = null;
                obj._Ldir = null;
                obj._sParams = null;
            }
        };

    }

    /**
     * A class used to intersect an line segment and this curve.
     */
    private static class LineSegCurveIntersect {

        private static final int MAXDEPTH = 20;
        private Curve _thisCurve;
        private Parameter<Length> _tol;
        private LineSegment _line;
        private Point _p0;
        private Point _p1;
        private Vector<Length> _Lu;
        private GeomList<PointString<SubrangePoint>> _output;
        private FastTable<Double> _crvS, _lineS;

        public static LineSegCurveIntersect newInstance(Curve thisCurve, Parameter<Length> tol,
                LineSegment line, GeomList<PointString<SubrangePoint>> output) {

            if (thisCurve instanceof GeomTransform)
                thisCurve = thisCurve.copyToReal();
            if (line instanceof GeomTransform)
                line = line.copyToReal();
            Unit<Length> unit = thisCurve.getUnit();

            //  Create an instance of this class and fill in the class variables.
            LineSegCurveIntersect o = FACTORY.object();
            o._thisCurve = thisCurve;
            o._tol = tol.to(unit);
            o._line = line.to(unit);
            o._p0 = line.getStart().immutable().to(unit);
            o._p1 = line.getEnd().immutable().to(unit);
            o._Lu = line.getDerivativeVector().immutable().to(unit);
            o._output = output;
            o._crvS = FastTable.newInstance();
            o._lineS = FastTable.newInstance();

            return o;
        }

        /**
         * Method that actually carries out the intersection adding the intersection
         * points to the list of lists provided in the constructor.
         *
         * @return A list containing two PointString instances each containing zero or
         *         more subrange points, on this curve and the input line segment
         *         respectively, made by the intersection of this curve with the specified
         *         line segment. If no intersections are found a list of two empty
         *         PointStrings are returned.
         */
        public GeomList<PointString<SubrangePoint>> intersect() {
            divideAndConquerLineSeg(1, _thisCurve, 0, 1);

            //  Convert stored parametric positions to subrange points on the curves.
            int size = _crvS.size();
            for (int i = 0; i < size; ++i) {
                double s = _crvS.get(i);
                _output.get(0).add(SubrangePoint.newInstance(_thisCurve, s));
                s = _lineS.get(i);
                _output.get(1).add(SubrangePoint.newInstance(_line, s));
            }
            FastTable.recycle(_crvS);
            _crvS = null;
            FastTable.recycle(_lineS);
            _lineS = null;

            return _output;
        }

        /**
         * Uses a recursive "Divide and Conquer" approach to intersecting a line segment
         * with a curve. On each call, the following happens:
         * <pre>
         *      1) The curve is checked to see if it is approx. a straight line.
         *          1b) If it is, then a line-line intersection is performed, the
         *              intersection point added to the output list and the method exited.
         *      2) The curve is divided into two halves.
         *          2b) Each half is tested to see if it's bounding box is intersected by the line.
         *              If it is, then this method is called with that segment recursively.
         *              Otherwise, the method is exited.
         * </pre>
         *
         * A number of class variables are used to pass information to this recursion:
         * <pre>
         *      _tol  The tolerance to use when determining if the curve is locally linear.
         *      _line The line segment instance we are intersecting the curve with.
         *      _p0   A point at the start of the line being intersected with this curve.
         *      _p1   A point at the end of the line being intersected with this curve.
         *      _Lu   A vector indicating the direction of the line.
         *      _crvS A list used to collect the output subrange intersection point parametric
         *              locations on this curve.
         *      _lineS A list used to collect the output subrange intersection point parametric
         *              locations on the line segment curve.
         * </pre>
         *
         * @param crv The curve segment being checked for intersection with the line.
         * @param s0  The parametric position of the start of this curve segment on the
         *            master curve.
         * @param s1  The parametric position of the end of this curve segment on the
         *            master curve.
         */
        private void divideAndConquerLineSeg(int depth, Curve crv, double s0, double s1) {
            StackContext.enter();
            try {
                //  Check to see if the input curve is within tolerance of a straight line.
                Point p0 = crv.getRealPoint(0);
                Point p1 = crv.getRealPoint(1);
                Point pAvg = p0.plus(p1).divide(2);
                Point pm = crv.getRealPoint(0.5);
                if (depth > MAXDEPTH || pAvg.distance(pm).isLessThan(_tol)) {
                    //  The input curve segment is nearly a straight line.

                    //  Intersect the input line and the line approximating the curve.
                    Vector<Length> crvDir = p1.minus(p0).toGeomVector();
                    MutablePoint sLn = MutablePoint.newInstance(2);
                    int numDims = p0.getPhyDimension();
                    Unit<Length> unit = p0.getUnit();
                    MutablePoint pi1 = MutablePoint.newInstance(numDims, unit);
                    MutablePoint pi2 = MutablePoint.newInstance(numDims, unit);
                    GeomUtil.lineLineIntersect(_p0, _Lu, p0, crvDir, _tol, sLn, pi1, pi2);

                    //  Make sure the intersection points fall on the line segments.
                    double sl_1 = sLn.getValue(0);
                    if (sl_1 > 1) {
                        pi1.set(_line.getEnd());
                        sl_1 = 1;
                    } else if (sl_1 < 0) {
                        pi1.set(_line.getStart());
                        sl_1 = 0;
                    }
                    double sl_2 = sLn.getValue(1);
                    if (sl_2 > 1) {
                        pi2.set(crv.getRealPoint(1));
                        sl_2 = 1;
                    } else if (sl_2 < 0) {
                        pi2.set(crv.getRealPoint(0));
                        sl_2 = 0;
                    }
                    Parameter<Length> dist = pi1.distance(pi2);
                    if (dist.isLessThan(_tol)) {
                        //  Add an intersection point on each curve to the output lists.
                        _lineS.add(sl_1);
                        //  Convert from segment par. pos. to full curve pos.
                        double s = segmentPos2Parent(sl_2, s0, s1);
                        _crvS.add(s);
                    }

                } else {
                    //  Sub-divide the curve into two segments.
                    GeomList<Curve> segments = crv.splitAt(0.5);

                    //  Check for possible intersections on the left segment.
                    Curve seg = segments.get(0);
                    if (GeomUtil.lineSegBoundsIntersect(_p0, _p1, seg)) {
                        //  May be an intersection.
                        double sl = s0;
                        double sh = (s0 + s1) / 2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerLineSeg(depth + 1, seg, sl, sh);
                    }

                    //  Check for possible intersections on the right segment.
                    seg = segments.get(1);
                    if (GeomUtil.lineSegBoundsIntersect(_p0, _p1, seg)) {
                        //  May be an intersection.
                        double sl = (s0 + s1) / 2;
                        double sh = s1;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerLineSeg(depth + 1, seg, sl, sh);
                    }
                }

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(LineSegCurveIntersect instance) {
            FACTORY.recycle(instance);
        }

        private LineSegCurveIntersect() { }

        private static final ObjectFactory<LineSegCurveIntersect> FACTORY = new ObjectFactory<LineSegCurveIntersect>() {
            @Override
            protected LineSegCurveIntersect create() {
                return new LineSegCurveIntersect();
            }

            @Override
            protected void cleanup(LineSegCurveIntersect obj) {
                obj._thisCurve = null;
                obj._tol = null;
                obj._line = null;
                obj._p0 = null;
                obj._p1 = null;
                obj._Lu = null;
                obj._output = null;
                obj._crvS = null;
                obj._lineS = null;
            }
        };
    }

    /**
     * A class used to intersect an line segment and this curve.
     */
    private static class CurveCurveIntersect {

        private static final int MAXDEPTH = 20;
        private AbstractCurve _thisCurve;
        private Parameter<Length> _tol;
        private Curve _thatCurve;
        private GeomList<PointString<SubrangePoint>> _output;
        private FastTable<Double> _thisS, _thatS;

        public static CurveCurveIntersect newInstance(AbstractCurve thisCurve, Parameter<Length> tol,
                Curve thatCurve, GeomList<PointString<SubrangePoint>> output) {

            //  Make sure the line and curve have the same dimensions.
            int numDims = thisCurve.getPhyDimension();
            if (thatCurve.getPhyDimension() > numDims)
                throw new IllegalArgumentException(MessageFormat.format(
                        RESOURCES.getString("sameOrFewerDimErr"), "target curve", "curve"));
            else if (thatCurve.getPhyDimension() < numDims)
                thatCurve = thatCurve.toDimension(numDims);

            if (thisCurve instanceof GeomTransform)
                thisCurve = (AbstractCurve)thisCurve.copyToReal();
            if (thatCurve instanceof GeomTransform)
                thatCurve = thatCurve.copyToReal();

            //  Create an instance of this class and fill in the class variables.
            CurveCurveIntersect o = FACTORY.object();
            o._thisCurve = thisCurve;
            o._tol = tol;
            o._thatCurve = thatCurve;
            o._output = output;
            o._thisS = FastTable.newInstance();
            o._thatS = FastTable.newInstance();

            return o;
        }

        /**
         * Method that actually carries out the intersection adding the intersection
         * points to the list of lists provided in the constructor.
         *
         * @return A list containing two PointString instances each containing zero or
         *         more subrange points, on this curve and the input line segment
         *         respectively, made by the intersection of this curve with the specified
         *         line segment. If no intersections are found a list of two empty
         *         PointStrings are returned.
         */
        public GeomList<PointString<SubrangePoint>> intersect() {
            divideAndConquerCurveSeg(1, _thisCurve, 0, 1, _thatCurve, 0, 1);

            //  Convert stored parametric positions to subrange points on the curves.
            int size = _thisS.size();
            for (int i = 0; i < size; ++i) {
                double s = _thisS.get(i);
                _output.get(0).add(SubrangePoint.newInstance(_thisCurve, s));
                s = _thatS.get(i);
                _output.get(1).add(SubrangePoint.newInstance(_thatCurve, s));
            }
            FastTable.recycle(_thisS);
            _thisS = null;
            FastTable.recycle(_thatS);
            _thatS = null;

            return _output;
        }

        /**
         * Uses a recursive "Divide and Conquer" approach to intersecting a curve segment
         * with another curve. On each call, the following happens:
         * <pre>
         *      1) The curve is checked to see if it is approx. a straight line.
         *          1b) If it is, then a line-line intersection is performed, the
         *              intersection point added to the output list and the method exited.
         *      2) The curve is divided into two halves.
         *          2b) Each half is tested to see if it's bounding box is intersected by the line.
         *              If it is, then this method is called with that segment recursively.
         *              Otherwise, the method is exited.
         * </pre>
         *
         * A number of class variables are used to pass information to this recursion:
         * <pre>
         *      _tol  The tolerance to use when determining if the curve is locally linear.
         *      _line The line segment instance we are intersecting the curve with.
         *      _p0   A point at the start of the line being intersected with this curve.
         *      _p1   A point at the end of the line being intersected with this curve.
         *      _Lu   A vector indicating the direction of the line.
         *      _output A list used to collect the output subrange intersection points.
         * </pre>
         *
         * @param crv1 The 1st curve segment being checked for intersection with "that"
         *             curve.
         * @param s0_1 The parametric position of the start of the 1st curve segment on
         *             the master "this" curve.
         * @param s1_1 The parametric position of the end of the 1st curve segment on the
         *             master "this" curve.
         * @param crv2 The 2nd curve segment being checked for intersection with "this"
         *             curve.
         * @param s0_2 The parametric position of the start of the 2nd curve segment on
         *             the master "that" curve.
         * @param s1_2 The parametric position of the end of the 2nd curve segment on the
         *             master "that" curve.
         */
        private void divideAndConquerCurveSeg(int depth, Curve crv1, double s0_1, double s1_1,
                Curve crv2, double s0_2, double s1_2) {

            StackContext.enter();
            try {
                //  Check to see if the 1st input curve is within tolerance of a straight line.
                Point p0 = crv1.getRealPoint(0);
                Point p1 = crv1.getRealPoint(1);
                Point pAvg = p0.plus(p1).divide(2);
                Point pm = crv1.getRealPoint(0.5);
                if (depth > MAXDEPTH || pAvg.distance(pm).isLessThan(_tol)) {
                    //  The 1st curve segment is nearly a straight line.
                    LineSeg line1 = LineSeg.valueOf(crv1);

                    // Intersect the line segment with crv2.
                    GeomList<PointString<SubrangePoint>> strs = crv2.intersect(line1, _tol);
                    if (strs.get(0).size() > 0) {
                        //  Intersections were found.
                        //  Loop over the intersection points.
                        int numPnts = strs.get(0).size();
                        for (int i = 0; i < numPnts; ++i) {
                            SubrangePoint pnt1 = strs.get(1).get(i);
                            SubrangePoint pnt2 = strs.get(0).get(i);

                            //  Convert from segment par. pos. to full curve pos.
                            double s1 = pnt1.getParPosition().getValue(0);
                            double s = segmentPos2Parent(s1, s0_1, s1_1);
                            _thisS.add(s);

                            double s2 = pnt2.getParPosition().getValue(0);
                            s = segmentPos2Parent(s2, s0_2, s1_2);
                            _thatS.add(s);
                        }
                    }

                    return;
                }

                //  Check to see if the 2nd input curve is within tolerance of a straight line.
                p0 = crv2.getRealPoint(0);
                p1 = crv2.getRealPoint(1);
                pAvg = p0.plus(p1).divide(2);
                pm = crv2.getRealPoint(0.5);
                if (pAvg.distance(pm).isLessThan(_tol)) {
                    //  The 2nd curve segment is nearly a straight line.
                    LineSeg line2 = LineSeg.valueOf(crv2);

                    // Intersect the line segment with crv1.
                    GeomList<PointString<SubrangePoint>> strs = crv1.intersect(line2, _tol);
                    if (strs.get(0).size() > 0) {
                        //  Intersections were found.
                        //  Loop over the intersection points.
                        int numPnts = strs.get(0).size();
                        for (int i = 0; i < numPnts; ++i) {
                            SubrangePoint pnt1 = strs.get(0).get(i);
                            SubrangePoint pnt2 = strs.get(1).get(i);

                            //  Convert from segment par. pos. to full curve pos.
                            double s1 = pnt1.getParPosition().getValue(0);
                            double s = segmentPos2Parent(s1, s0_1, s1_1);
                            _thisS.add(s);

                            double s2 = pnt2.getParPosition().getValue(0);
                            s = segmentPos2Parent(s2, s0_2, s1_2);
                            _thatS.add(s);
                        }
                    }

                } else {
                    //  Sub-divide the input curves into two segments each.
                    GeomList<Curve> crv1Segs = crv1.splitAt(0.5);
                    GeomList<Curve> crv2Segs = crv2.splitAt(0.5);

                    //  Check for possible intersections on the left segment of curve 1.
                    Curve seg1 = crv1Segs.get(0);
                    Curve seg2 = crv2Segs.get(0);
                    if (GeomUtil.boundsIntersect(seg1, seg2)) {
                        //  May be an intersection.
                        double sl_1 = s0_1;
                        double sh_1 = (s0_1 + s1_1) / 2;
                        double sl_2 = s0_2;
                        double sh_2 = (s0_2 + s1_2) / 2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerCurveSeg(depth + 1, seg1, sl_1, sh_1, seg2, sl_2, sh_2);
                    }

                    seg2 = crv2Segs.get(1);
                    if (GeomUtil.boundsIntersect(seg1, seg2)) {
                        //  May be an intersection.
                        double sl_1 = s0_1;
                        double sh_1 = (s0_1 + s1_1) / 2;
                        double sl_2 = (s0_2 + s1_2) / 2;
                        double sh_2 = s1_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerCurveSeg(depth + 1, seg1, sl_1, sh_1, seg2, sl_2, sh_2);
                    }

                    //  Check for possible intersections on the right segment of curve 1.
                    seg1 = crv1Segs.get(1);
                    seg2 = crv2Segs.get(0);
                    if (GeomUtil.boundsIntersect(seg1, seg2)) {
                        //  May be an intersection.
                        double sl_1 = (s0_1 + s1_1) / 2;
                        double sh_1 = s1_1;
                        double sl_2 = s0_2;
                        double sh_2 = (s0_2 + s1_2) / 2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerCurveSeg(depth + 1, seg1, sl_1, sh_1, seg2, sl_2, sh_2);
                    }

                    seg2 = crv2Segs.get(1);
                    if (GeomUtil.boundsIntersect(seg1, seg2)) {
                        //  May be an intersection.
                        double sl_1 = (s0_1 + s1_1) / 2;
                        double sh_1 = s1_1;
                        double sl_2 = (s0_2 + s1_2) / 2;
                        double sh_2 = s1_2;

                        //  Recurse down to a finer level of detail.
                        divideAndConquerCurveSeg(depth + 1, seg1, sl_1, sh_1, seg2, sl_2, sh_2);
                    }

                }

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(CurveCurveIntersect instance) {
            FACTORY.recycle(instance);
        }

        private CurveCurveIntersect() { }

        private static final ObjectFactory<CurveCurveIntersect> FACTORY = new ObjectFactory<CurveCurveIntersect>() {
            @Override
            protected CurveCurveIntersect create() {
                return new CurveCurveIntersect();
            }

            @Override
            protected void cleanup(CurveCurveIntersect obj) {
                obj._thisCurve = null;
                obj._tol = null;
                obj._output = null;
            }
        };
    }

    /**
     * An Evaluatable1D function that calculates the signed distance between a point along
     * a curve and a point on a surface. This is used by a 1D root finder to drive that
     * distance to zero.
     */
    private static class CurveSrfIntersectFun extends AbstractEvaluatable1D {

        private Surface _srf;
        private Curve _thisCurve;
        public double tol;
        public double _nearS, _nearT;
        public boolean firstPass;

        public static CurveSrfIntersectFun newInstance(Curve thisCurve, Surface surface, double tol) {

            if (surface instanceof GeomTransform)
                surface = (Surface)surface.copyToReal();
            if (thisCurve instanceof GeomTransform)
                thisCurve = thisCurve.copyToReal();

            CurveSrfIntersectFun o = FACTORY.object();
            o._srf = surface;
            o._thisCurve = thisCurve;
            o._nearS = -1;
            o._nearT = 0;
            o.tol = tol;
            o.firstPass = false;

            return o;
        }

        /**
         * Function that calculates the signed distance between a point on an curve, Q(u),
         * and the closest point on a parametric surface, P(s,t).
         *
         * @param u Parametric distance along the curve.
         * @return The signed distance between the point on the curve at u and the closest
         *         point on the surface.
         */
        @Override
        public double function(double u) throws RootException {
            //  Don't compute anything if the 1st pass flag is set.
            if (firstPass) {
                firstPass = false;
                return 0;
            }

            StackContext.enter();
            try {
                //  Compute the point on the curve at u.
                Point Qu = _thisCurve.getRealPoint(u);

                //  Find the closest point on the surface near "Qu".
                SubrangePoint Pst;
                if (_nearS < 0)
                    Pst = _srf.getClosest(Qu, tol);
                else
                    Pst = _srf.getClosest(Qu, _nearS, _nearT, tol);
                GeomPoint st = Pst.getParPosition();
                _nearS = st.getValue(0);
                _nearT = st.getValue(1);

                //  Find the signed distance between Pst and Qu.
                Vector<Dimensionless> nhat = _srf.getNormal(_nearS, _nearT);
                Vector<Length> dP = Qu.toGeomVector().minus(Pst.toGeomVector());
                double dPsign = dP.dot(nhat).getValue();    //  Projection of dP onto nhat.
                dPsign = dPsign >= 0 ? 1 : -1;
                double d = dP.normValue();                  //  Unsigned distance.
                d = d * dPsign;                             //  Signed distance.

                return d;

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(CurveSrfIntersectFun instance) {
            FACTORY.recycle(instance);
        }

        private CurveSrfIntersectFun() { }

        @SuppressWarnings("unchecked")
        private static final ObjectFactory<CurveSrfIntersectFun> FACTORY = new ObjectFactory<CurveSrfIntersectFun>() {
            @Override
            protected CurveSrfIntersectFun create() {
                return new CurveSrfIntersectFun();
            }

            @Override
            protected void cleanup(CurveSrfIntersectFun obj) {
                obj._srf = null;
                obj._thisCurve = null;
            }
        };
    }

    /**
     * An Evaulatable1D that returns the increment in area subtended by a curve tangent
     * vector at the specified parametric location. This is used to find the enclosed area
     * of a curve. This is <code>f(s)</code> in:
     * <code>A = int_0^1 { f(s) ds } = int_0^1 { ((p(s)-p0) x p'(s)) ds }</code>.
     */
    private static class EnclAreaEvaluatable extends AbstractEvaluatable1D {

        private AbstractCurve _curve;
        private Vector<Dimensionless> _nhat;
        private Vector<Length> _p0;
        private boolean _is2D;
        private boolean _isZero;
        public int iterations;

        public static EnclAreaEvaluatable newInstance(AbstractCurve curve, GeomPoint p0) {
            if (curve instanceof GeomTransform)
                curve = (AbstractCurve)curve.copyToReal();
            if (p0 instanceof GeomTransform)
                p0 = p0.copyToReal();

            EnclAreaEvaluatable o = FACTORY.object();
            o._curve = curve;
            o._p0 = p0.toGeomVector();
            o._is2D = curve.getPhyDimension() == 2;
            o._isZero = false;

            if (!o._is2D) {
                if (curve.isLine(Parameter.valueOf(1e-10, SI.METER))) {
                    //  For straight line curves, form normal from curve end points.
                    Vector<Length> v1 = curve.getRealPoint(0).toGeomVector().minus(o._p0);
                    Vector<Length> v2 = curve.getRealPoint(1).toGeomVector().minus(o._p0);
                    Vector n = v1.cross(v2);
                    if (n.magValue() <= 1e-10) {
                        //  The target point is colinear with the line segment.  The enclosed area is always zero.
                        o._isZero = true;
                    } else
                        o._nhat = n.toUnitVector();

                } else
                    o._nhat = curve.getPlaneNormal(p0);
            }
            o.iterations = 0;

            return o;
        }

        @Override
        public double function(double s) throws RootException {
            ++iterations;

            //  If the area must be zero, then don't compute anything.
            if (_isZero)
                return 0;

            StackContext.enter();
            try {
                //  Extract the curve point (p) and velocity (p') at s.
                List<Vector<Length>> ders = _curve.getSDerivatives(s, 1);
                Vector<Length> p = ders.get(0);
                Vector<Length> pp = ders.get(1);

                //  Form the p0p vector.
                Vector<Length> p0p = p.minus(_p0);

                //  Calculate fs = p0p(s) x p'(s)
                double fs;
                if (_is2D) {
                    //  In 2D u x v = det(u, v) = ux*vy - uy*vx
                    fs = p0p.getValue(0) * pp.getValue(1) - p0p.getValue(1) * pp.getValue(0);

                } else {
                    Vector p0pxpp = p0p.cross(pp);

                    //  Determine the sign by projecting the p0p x pp vector onto the
                    //  plane normal vector for the curve.
                    fs = p0pxpp.dot(_nhat).getValue();
                }

                return fs;

            } finally {
                StackContext.exit();
            }
        }

        public static void recycle(EnclAreaEvaluatable instance) {
            FACTORY.recycle(instance);
        }

        private EnclAreaEvaluatable() { }

        private static final ObjectFactory<EnclAreaEvaluatable> FACTORY = new ObjectFactory<EnclAreaEvaluatable>() {
            @Override
            protected EnclAreaEvaluatable create() {
                return new EnclAreaEvaluatable();
            }

            @Override
            protected void cleanup(EnclAreaEvaluatable obj) {
                obj._curve = null;
                obj._nhat = null;
                obj._p0 = null;
            }
        };

    }

}
