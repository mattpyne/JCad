/*
 *   Surface  -- Interface for all surface type objects.
 *
 *   Copyright (C) 2010-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.NurbsSurface;
import jahuwaldt.js.param.Parameter;
import java.util.List;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.quantity.Volume;
import javax.measure.unit.Unit;

/**
 * Defines the interface for {@link GeomElement} objects that are surfaces with 2
 * parametric dimensions.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: June 14, 2010
 * @version August 30, 2015
 *
 * @param <T> The sub-type of this Surface element.
 */
public interface Surface<T extends Surface> extends GeomElement<T>, ParametricGeometry<T>, Transformable<T> {

    /**
     * Returns the equivalent to this surface but stated in the specified unit.
     *
     * @param unit the length unit of the surface to be returned.
     * @return an equivalent to this surface but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public T to(Unit<Length> unit) throws ConversionException;

    /**
     * Return a new surface that is identical to this one but with the transpose of the
     * parameterization of this surface. The S and T directions will be swapped. This is
     * an optional method that not all subclasses will provide. Those that do not provide
     * this method will throw an UnsupportedMethodException.
     *
     * @return A new surface, identical to this one, but with the transpose of the
     * parameterization.
     * @throws UnsupportedOperationException if this method is not supported.
     */
    public T transpose();

    /**
     * Split this surface at the specified parametric S-position returning a list
     * containing two new surfaces (a lower surface with smaller S-parametric positions
     * than "s" and an upper surface with larger S-parametric positions).
     *
     * @param s The S-parametric position where this surface should be split (must not be
     * 0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     * surface.
     */
    public GeomList<T> splitAtS(double s);

    /**
     * Split this surface at the specified parametric S-position returning a list
     * containing two new surfaces (a lower surface with smaller S-parametric positions
     * than "s" and an upper surface with larger S-parametric positions).
     *
     * @param st The S-parametric position where this surface should be split (must not be
     * 0 or 1!). Must be a 2-dimensional point with each value in the range 0 to 1, only
     * the 1st value is used. Units are ignored.
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     * surface.
     */
    public GeomList<T> splitAtS(GeomPoint st);

    /**
     * Split this surface at the specified parametric T-position returning a list
     * containing two new surfaces (a lower surface with smaller T-parametric positions
     * than "t" and an upper surface with larger T-parametric positions).
     *
     * @param t The T-parametric position where this surface should be split (must not be
     * 0 or 1!).
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     * surface.
     */
    public GeomList<T> splitAtT(double t);

    /**
     * Split this surface at the specified parametric T-position returning a list
     * containing two new surfaces (a lower surface with smaller T-parametric positions
     * than "t" and an upper surface with larger T-parametric positions).
     *
     * @param st The T-parametric position where this surface should be split (must not be
     * 0 or 1!). Must be a 2-dimensional point with each value in the range 0 to 1, only
     * the 2nd value is used. Units are ignored.
     * @return A list containing two surfaces: 0 == the lower surface, 1 == the upper
     * surface.
     */
    public GeomList<T> splitAtT(GeomPoint st);

    /**
     * Return a new surface that is identical to this one, but with the S-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     * S-parameterization reversed.
     * @see #reverseT
     */
    public T reverseS();

    /**
     * Return a new surface that is identical to this one, but with the T-parameterization
     * reversed.
     *
     * @return A new surface that is identical to this one, but with the
     * T-parameterization reversed.
     * @see #reverseS
     */
    public T reverseT();

    /**
     * Returns a copy of this ParametricGeometry instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public T copy();

    /**
     * Return a copy of this surface converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the surface to return.
     * @return This surface converted to the new dimensions.
     */
    @Override
    public T toDimension(int newDim);

    /**
     * Return a NURBS surface representation of this surface to within the specified
     * tolerance.
     *
     * @param tol The greatest possible difference between this surface and the NURBS
     * representation returned.
     * @return A NURBS surface that represents this surface to within the specified
     * tolerance.
     */
    public NurbsSurface toNurbs(Parameter<Length> tol);

    /**
     * Return a subrange point on the surface for the given parametric position on the
     * surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     * inclusive).
     * @return The subrange point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    public SubrangePoint getPoint(double s, double t);

    /**
     * Return a subrange point on the surface for the given parametric position on the
     * surface.
     *
     * @param st The parametric position to calculate a point for. Must be a 2-dimensional
     * point with each value in the range 0 to 1.0. Units are ignored.
     * @return The subrange point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public SubrangePoint getPoint(GeomPoint st);

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param s 1st parametric dimension distance to calculate a point for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate a point for (0.0 to 1.0
     * inclusive).
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    public Point getRealPoint(double s, double t);

    /**
     * Calculate a point on the surface for the given parametric position on the surface.
     *
     * @param st The parametric position to calculate a point for. Must be a 2-dimensional
     * point with each value in the range 0 to 1.0. Units are ignored.
     * @return The calculated point on the surface at the specified parameter values.
     * @throws IllegalArgumentException if there is any problem with the parameter values.
     */
    @Override
    public Point getRealPoint(GeomPoint st);

    /**
     * Return a subrange curve on the surface for the given parametric position curve.
     *
     * @param pcrv A curve in parametric space indicating the parametric positions on the
     * the surface represented by the subrange curve. Must be 2D with values between 0 and 1.
     * @return The subrange curve on the surface at the specified parametric curve positions.
     */
    public SubrangeCurve getCurve(Curve pcrv);

    /**
     * Return a subrange curve at a constant parametric s-value.
     *
     * @param s The parametric s-position to extract a subrange curve.
     * @return The subrange curve on the surface at the specified s-value.
     * @throws IllegalArgumentException if there is any problem with the parameter value.
     */
    public SubrangeCurve getSCurve(double s);

    /**
     * Return a subrange curve at a constant parametric t-value.
     *
     * @param t The parametric t-position to extract a subrange curve.
     * @return The subrange curve on the surface at the specified t-value.
     * @throws IllegalArgumentException if there is any problem with the parameter value.
     */
    public SubrangeCurve getTCurve(double t);

    /**
     * Return the T=0 Boundary for this surface as a curve.
     *
     * @return The T=0 boundary for this surface as a curve.
     */
    public Curve getT0Curve();

    /**
     * Return the T=1 Boundary for this surface as a curve.
     *
     * @return The T=1 boundary for this surface as a curve.
     */
    public Curve getT1Curve();

    /**
     * Return the S=0 Boundary for this surface as a curve.
     *
     * @return The S=0 boundary for this surface as a curve.
     */
    public Curve getS0Curve();

    /**
     * Return the S=1 Boundary for this surface as a curve.
     *
     * @return The S=1 boundary for this surface as a curve.
     */
    public Curve getS1Curve();

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric position(s) on a parametric object for the given parametric
     * position on the object, <code>d^{grade}p(s)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s), dp(s)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s), dp(s)/ds, d^2p(s)/d^2s]</code>; etc.
     * </p>
     *
     * @param st The parametric position to calculate the derivatives for. Must match the
     * parametric dimension of this parametric surface and have each value in the range 0
     * to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     * 2=2nd derivative, etc)
     * @return A list of lists of derivatives (one list for each parametric dimension).
     * Each list contains derivatives up to the specified grade at the specified
     * parametric position. Example: for a surface list element 0 is the array of
     * derivatives int the 1st parametric dimension (s), then 2nd list element is the
     * array of derivatives in the 2nd parametric dimension (t).
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    @Override
    public List<List<Vector<Length>>> getDerivatives(GeomPoint st, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param s 1st parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param grade The maximum grade to calculate the u-derivatives for (1=1st
     * derivative, 2=2nd derivative, etc)
     * @return A list of s-derivatives up to the specified grade of the surface at the
     * specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public List<Vector<Length>> getSDerivatives(double s, double t, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric s-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/ds]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/ds, d^2p(s,t)/d^2s]</code>; etc.
     * </p>
     *
     * @param st The parametric position to calculate the derivatives for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     * 2=2nd derivative, etc)
     * @return A list of s-derivatives up to the specified grade of the surface at the
     * specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public List<Vector<Length>> getSDerivatives(GeomPoint st, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s 1st parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param grade The maximum grade to calculate the v-derivatives for (1=1st
     * derivative, 2=2nd derivative, etc)
     * @return A list of t-derivatives up to the specified grade of the surface at the
     * specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public List<Vector<Length>> getTDerivatives(double s, double t, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param st The parametric position to calculate the derivatives for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param grade The maximum grade to calculate the derivatives for (1=1st derivative,
     * 2=2nd derivative, etc)
     * @return A list of t-derivatives up to the specified grade of the surface at the
     * specified parametric position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public List<Vector<Length>> getTDerivatives(GeomPoint st, int grade);

    /**
     * Calculate a derivative with respect to parametric s-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/ds</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2s</code>; etc.
     * </p>
     *
     * @param st The parametric position to calculate the derivative for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     * derivative, etc)
     * @return The specified s-derivative of the surface at the specified parametric
     * position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public Vector<Length> getSDerivative(GeomPoint st, int grade);

    /**
     * Calculate a derivative with respect to parametric t-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/dt</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2t</code>; etc.
     * </p>
     *
     * @param st The parametric position to calculate the derivative for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     * derivative, etc)
     * @return The specified t-derivative of the surface at the specified parametric
     * position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public Vector<Length> getTDerivative(GeomPoint st, int grade);

    /**
     * Calculate a derivative with respect to parametric s-distance of the given grade on
     * the surface for the given parametric position on the surface,
     * <code>d^{grade}p(s,t)/d^{grade}s</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>dp(s,t)/ds</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>d^2p(s,t)/d^2s</code>; etc.
     * </p>
     *
     * @param s 1st parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param grade The grade to calculate the derivative for (1=1st derivative, 2=2nd
     * derivative, etc)
     * @return The specified s-derivative of the surface at the specified parametric
     * position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public Vector<Length> getSDerivative(double s, double t, int grade);

    /**
     * Calculate all the derivatives from <code>0</code> to <code>grade</code> with
     * respect to parametric t-position on the surface for the given parametric position
     * on the surface, <code>d^{grade}p(s,t)/d^{grade}t</code>.
     * <p>
     * Example:<br>
     * 1st derivative (grade = 1), this returns <code>[p(s,t), dp(s,t)/dt]</code>;<br>
     * 2nd derivative (grade = 2), this returns <code>[p(s,t), dp(s,t)/dt, d^2p(s,t)/d^2t]</code>; etc.
     * </p>
     *
     * @param s 1st parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate derivative for (0.0 to 1.0
     * inclusive).
     * @param grade The maximum grade to calculate the v-derivatives for (1=1st
     * derivative, 2=2nd derivative, etc)
     * @return The specified t-derivative of the surface at the specified parametric
     * position.
     * @throws IllegalArgumentException if the grade is &lt; 0 or the parameter values are
     * invalid.
     */
    public Vector<Length> getTDerivative(double s, double t, int grade);

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param s 1st parametric dimension distance to calculate twist vector for (0.0 to
     * 1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate twist vector for (0.0 to
     * 1.0 inclusive).
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    public Vector<Length> getTwistVector(double s, double t);

    /**
     * Calculate the twist vector (d^2P/(ds*dt) = d(dP/ds)/dt) for this surface at the
     * specified position on this surface.
     *
     * @param st The parametric position to calculate the twist vector for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @return The twist vector of this surface at the specified parametric position.
     * @throws IllegalArgumentException if the parameter values are invalid.
     */
    public Vector<Length> getTwistVector(GeomPoint st);

    /**
     * Return the normal vector for this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param s 1st parametric dimension distance to calculate normal for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate normal for (0.0 to 1.0
     * inclusive).
     * @return The normal vector of the surface at the specified parametric position.
     */
    public Vector<Dimensionless> getNormal(double s, double t);

    /**
     * Return the normal vector for this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param st The parametric position to calculate the normal for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @return The normal vector of the surface at the specified parametric position.
     */
    public Vector<Dimensionless> getNormal(GeomPoint st);

    /**
     * Return the tangent plane to this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param s 1st parametric dimension distance to calculate tangent plane for (0.0 to
     * 1.0 inclusive).
     * @param t 2nd parametric dimension distance to calculate tangent plane for (0.0 to
     * 1.0 inclusive).
     * @return The tangent plane of the surface at the specified parametric position.
     */
    public Plane getTangentPlane(double s, double t);

    /**
     * Return the tangent plane to this surface at the given parametric position (s,t) on
     * this surface.
     *
     * @param st The parametric position to calculate the tangent plane for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @return The tangent plane of the surface at the specified parametric position.
     */
    public Plane getTangentPlane(GeomPoint st);

    /**
     * Returns the Gaussian Curvature for this surface at the given parametric position
     * (s,t) on this surface.
     *
     * @param s 1st parametric dimension distance to calculate curvature for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate curvature for (0.0 to 1.0
     * inclusive).
     * @return The Gaussian curvature of the surface at the specified parametric position
     * in units of 1/Length^2.
     */
    public Parameter getGaussianCurvature(double s, double t);

    /**
     * Returns the Gaussian Curvature for this surface at the given parametric position
     * (s,t) on this surface.
     *
     * @param st The parametric position to calculate the curvature for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @return The Gaussian curvature of the surface at the specified parametric position
     * in units of 1/Length^2.
     */
    public Parameter getGaussianCurvature(GeomPoint st);

    /**
     * Returns the Mean Curvature for this surface at the given parametric position (s,t)
     * on this surface.
     *
     * @param s 1st parametric dimension distance to calculate curvature for (0.0 to 1.0
     * inclusive).
     * @param t 2nd parametric dimension distance to calculate curvature for (0.0 to 1.0
     * inclusive).
     * @return The Mean curvature of the surface at the specified parametric position in
     * units of 1/Length.
     */
    public Parameter getMeanCurvature(double s, double t);

    /**
     * Returns the Mean Curvature for this surface at the given parametric position (s,t)
     * on this surface.
     *
     * @param st The parametric position to calculate the curvature for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @return The Mean curvature of the surface at the specified parametric position in
     * units of 1/Length.
     */
    public Parameter getMeanCurvature(GeomPoint st);

    /**
     * Return the surface area of this entire surface.
     *
     * @param eps The desired fractional accuracy on the surface area.
     * @return the surface area of this surface.
     */
    public Parameter<Area> getArea(double eps);

    /**
     * Return the surface area of a portion of this surface.
     *
     * @param s1 The starting 1st parametric dimension distance to calculate area for (0.0
     * to 1.0 inclusive).
     * @param t1 The starting 2nd parametric dimension distance to calculate area for (0.0
     * to 1.0 inclusive).
     * @param s2 The ending 1st parametric dimension distance to calculate area for (0.0
     * to 1.0 inclusive).
     * @param t2 The ending 2nd parametric dimension distance to calculate area for (0.0
     * to 1.0 inclusive).
     * @param eps The desired fractional accuracy on the surface area.
     * @return the surface area of a portion of this surface.
     */
    public Parameter<Area> getArea(double s1, double t1, double s2, double t2, double eps);

    /**
     * Return the surface area of a portion of this surface.
     *
     * @param st1 The starting parametric position to calculate the area for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param st2 The ending parametric position to calculate the area for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param eps The desired fractional accuracy on the surface area.
     * @return the surface area of a portion of this surface.
     */
    public Parameter<Area> getArea(GeomPoint st1, GeomPoint st2, double eps);

    /**
     * Return the enclosed volume of this entire surface.
     *
     * @param eps The desired fractional accuracy on the enclosed volume.
     * @return the enclosed volume of this surface.
     */
    public Parameter<Volume> getVolume(double eps);

    /**
     * Return the enclosed volume of a portion of this surface.
     *
     * @param s1 The starting 1st parametric dimension distance to calculate volume for
     * (0.0 to 1.0 inclusive).
     * @param t1 The starting 2nd parametric dimension distance to calculate volume for
     * (0.0 to 1.0 inclusive).
     * @param s2 The ending 1st parametric dimension distance to calculate volume for (0.0
     * to 1.0 inclusive).
     * @param t2 The ending 2nd parametric dimension distance to calculate volume for (0.0
     * to 1.0 inclusive).
     * @param eps The desired fractional accuracy on the enclosed volume.
     * @return the enclosed volume of a portion of this surface.
     */
    public Parameter<Volume> getVolume(double s1, double t1, double s2, double t2, double eps);

    /**
     * Return the enclosed volume of a portion of this surface.
     *
     * @param st1 The starting parametric position to calculate the volume for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param st2 The ending parametric position to calculate the volume for. Must be a
     * 2-dimensional point with each value in the range 0 to 1.0. Units are ignored.
     * @param eps The desired fractional accuracy on the enclosed volume.
     * @return the enclosed volume of a portion of this surface.
     */
    public Parameter<Volume> getVolume(GeomPoint st1, GeomPoint st2, double eps);

    /**
     * Return an array of SubrangePoint objects that are gridded onto the surface using
     * the specified spacings and gridding rule.  <code>gridRule</code> specifies whether
     * the subdivision spacing is applied with respect to arc-length in real space or
     * parameter space. If the spacing is arc-length, then the values are interpreted as
     * fractions of the arc length of the edge curves from 0 to 1.
     *
     * @param gridRule Specifies whether the subdivision spacing is applied with respect
     * to arc-length in real space or parameter space.
     * @param bottomSpacing A list of values used to define the subdivision spacing along
     * the T=0 parametric boundary of the surface.
     * @param topSpacing An optional list of values used to define the subdivision spacing
     * along the T=1 parametric boundary of the surface. If <code>null</code> is passed,
     * the bottomSpacing is used for the top. If topSpacing is provided, then it must have
     * the same number of values as the bottomSpacing.
     * @param leftSpacing A list of values used to define the subdivision spacing along
     * the S=0 parametric boundary of the surface.
     * @param rightSpacing An optional list of values used to define the subdivision
     * spacing along the S=1 parametric boundary of the surface. If <code>null</code> is
     * passed, the leftSpacing is used for the right. If rightSpacing is provided, then it
     * must have the same number of values as the leftSpacing.
     * @return An array of SubrangePoint objects gridded onto the surface at the specified
     * parametric positions.
     */
    public PointArray<SubrangePoint> extractGrid(GridRule gridRule, List<Double> bottomSpacing, List<Double> topSpacing,
            List<Double> leftSpacing, List<Double> rightSpacing);

    /**
     * Return an array of points that are gridded onto the surface with the number of
     * points and spacing chosen to result in straight line segments between the points
     * that have mid-points that are all within the specified tolerance of this surface.
     *
     * @param tol The maximum distance that a straight line between gridded points may
     * deviate from this surface.
     * @return An array of subrange points gridded onto the surface.
     */
    public PointArray<SubrangePoint> gridToTolerance(Parameter<Length> tol);

    /**
     * Returns the closest point on this surface to the specified point.
     *
     * @param point The point to find the closest point on this surface to.
     * @param tol Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is closest to the specified
     * point.
     */
    @Override
    public SubrangePoint getClosest(GeomPoint point, double tol);

    /**
     * Returns the closest point on this surface to the specified point near the specified
     * parametric position.
     *
     * @param point The point to find the closest point on this surface to.
     * @param nearS The parametric s-position where the search for the closest point
     * should begin.
     * @param nearT The parametric t-position where the search for the closest point
     * should begin.
     * @param tol Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is closest to the specified
     * point near the specified parametric position.
     */
    public SubrangePoint getClosest(GeomPoint point, double nearS, double nearT, double tol);

    /**
     * Returns the array of closest points on this surface to the specified array (list of
     * lists) of points.
     *
     * @param points A list of lists of points to find the closest point on this surface to.
     * @param tol Fractional tolerance to refine the distance to.
     * @return The {@link PointArray} of {@link SubrangePoint} on this surface that is
     * closest to the specified list of lists of points.
     */
    public PointArray<SubrangePoint> getClosest(List<? extends List<GeomPoint>> points, double tol);

    /**
     * Returns the farthest point on this surface from the specified point.
     *
     * @param point The point to find the farthest point on this surface from.
     * @param tol Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is farthest from the
     * specified point.
     */
    @Override
    public SubrangePoint getFarthest(GeomPoint point, double tol);

    /**
     * Returns the farthest point on this surface from the specified point near the
     * specified parametric position on the surface.
     *
     * @param point The point to find the farthest point on this surface from.
     * @param nearS The parametric s-position where the search for the closest point
     * should begin.
     * @param nearT The parametric t-position where the search for the closest point
     * should begin.
     * @param tol Fractional tolerance to refine the distance to.
     * @return The {@link SubrangePoint} on this surface that is farthest from the
     * specified point.
     */
    public SubrangePoint getFarthest(GeomPoint point, double nearS, double nearT, double tol);

    /**
     * Returns the array of farthest points on this surface from the specified array (list
     * of lists) of points.
     *
     * @param points A list of lists of points to find the farthest point on this surface from.
     * @param tol Fractional tolerance to refine the distance to.
     * @return The {@link PointArray} of {@link SubrangePoint} on this surface that is
     * farthest from the specified list of lists of points.
     */
    public PointArray<SubrangePoint> getFarthest(List<? extends List<GeomPoint>> points, double tol);

    /**
     * Returns the closest points (giving the minimum distance) between this surface and
     * the specified curve. If the specified curve intersects this surface, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If the specified curve is
     * parallel to this surface (all points are equidistant away), then any point between
     * this surface and the specified curve could be returned as the "closest".
     *
     * @param curve The curve to find the closest points between this surface and that
     * curve on.
     * @param tol Fractional tolerance to refine the distance to.
     * @return A list containing two SubrangePoint objects that represent the closest
     * point on this surface (index 0) and the closest point on the specified curve (index
     * 1).
     */
    public PointString<SubrangePoint> getClosest(Curve curve, double tol);

    /**
     * Returns the closest points (giving the minimum distance) between this surface and
     * the specified surface. If the specified surface intersects this surface, then the
     * 1st intersection found is returned (not all the intersections are found and there
     * is no guarantee about which intersection will be returned). If the specified
     * surface is parallel to this surface (all points are equidistant away), then any
     * point between this surface and the specified surface could be returned as the
     * "closest".
     *
     * @param surface The surface to find the closest points between this surface and that
     * surface on.
     * @param tol Fractional tolerance to refine the distance to.
     * @return A list containing two SubrangePoint objects that represent the closest
     * point on this surface (index 0) and the closest point on the specified surface
     * (index 1).
     */
    public PointString<SubrangePoint> getClosest(Surface surface, double tol);

    /**
     * Returns the closest point (giving the minimum distance) between this surface and
     * the specified plane. If the specified plane intersects this surface, then the 1st
     * intersection found is returned (not all the intersections are found and there is no
     * guarantee about which intersection will be returned). If the specified plane is
     * parallel to this surface (all points are equidistant away), then any point between
     * this surface and the specified plane could be returned as the "closest".
     *
     * @param plane The plane to find the closest points between this surface and that
     * plane on.
     * @param tol Fractional tolerance to refine the distance to.
     * @return A SubrangePoint that represent the closest point on this surface to the
     * specified plane.
     */
    public SubrangePoint getClosest(GeomPlane plane, double tol);

    /**
     * Return <code>true</code> if this surface is degenerate (i.e.: has area less than
     * the specified tolerance squared).
     *
     * @param tol The tolerance for determining if this surface is degenerate.
     * @return true if this surface is degenerate.
     */
    @Override
    public boolean isDegenerate(Parameter<Length> tol);

    /**
     * Return <code>true</code> if this surface is planar or <code>false</code> if it is
     * not.
     *
     * @param tol The geometric tolerance to use in determining if the surface is planar.
     * @return true if this surface is planar.
     */
    public boolean isPlanar(Parameter<Length> tol);

    /**
     * Return <code>true</code> if this surface is planar or <code>false</code> if it is
     * not.
     *
     * @param epsFT Flatness tolerance (cosine of the angle allowable between normal vectors).
     * @param epsELT Edge linearity tolerance (cosine of the angle allowable between edge
     * vectors).
     * @return true if this surface is planar.
     */
    public boolean isPlanar(double epsFT, double epsELT);

    /**
     * Return the intersections between an infinite line and this surface.
     *
     * @param L0 A point on the line (origin of the line).
     * @param Ldir The direction vector for the line (does not have to be a unit vector).
     * @param tol Tolerance (in physical space) to refine the point positions to.
     * @return A PointString containing zero or more subrange points made by the
     * intersection of this surface with the specified infinite line. If no intersection
     * is found, an empty PointString is returned.
     */
    public PointString<SubrangePoint> intersect(GeomPoint L0, GeomVector Ldir, Parameter<Length> tol);

    /**
     * Return the intersections between a line segment and this surface.
     *
     * @param line A line segment to intersect with this surface.
     * @param tol Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     * subrange points, on this surface and the input line segment respectively, made by
     * the intersection of this surface with the specified line segment. If no
     * intersections are found a list of two empty PointStrings are returned.
     */
    public GeomList<PointString<SubrangePoint>> intersect(LineSegment line, Parameter<Length> tol);

    /**
     * Return the intersections between a curve and this surface.
     *
     * @param curve The curve to intersect with this surface.
     * @param tol Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two PointString instances each containing zero or more
     * subrange points, on this surface and the input curve respectively, made by the
     * intersection of this surface with the specified curve. If no intersections are
     * found a list of two empty PointStrings are returned.
     */
    public GeomList<PointString<SubrangePoint>> intersect(Curve curve, Parameter<Length> tol);

    /**
     * Return the intersections between an infinite plane and this surface.
     *
     * @param plane The infinite plane to intersect with this surface.
     * @param tol Tolerance (in physical space) to refine the point positions to.
     * @return A PointString containing zero or more subrange points made by the
     * intersection of this surface with the specified infinite plane. If no intersection
     * is found, an empty PointString is returned.
     */
    public GeomList<SubrangeCurve> intersect(GeomPlane plane, Parameter<Length> tol);

    /**
     * Return the intersections between another surface and this surface.
     *
     * @param surface A surface to intersect with this surface.
     * @param tol Tolerance (in physical space) to refine the point positions to.
     * @return A list containing two lists of SubrangeCurve objects. Each list contains
     * zero or more subrange curves, on this surface and the input surface respectively,
     * made by the intersection of this surface with the specified surface. If no
     * intersections are found a list of two empty lists is returned.
     */
    public GeomList<GeomList<SubrangeCurve>> intersect(Surface surface, Parameter<Length> tol);

}
