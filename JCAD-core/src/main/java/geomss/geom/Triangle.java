/*
 *   Triangle  -- A concrete triangle in nD space.
 *
 *   Copyright (C) 2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import static geomss.geom.AbstractGeomElement.RESOURCES;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.ValueType;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A concrete triangle in n-dimensional space. A triangle is represented by
 * exactly three vertex points arranged in a counter-clockwise direction (or
 * winding) when viewed from the "outside" (the direction the normal vector 
 * points).
 *
 * <p> Modified by: Joseph A. Huwaldt   </p>
 *
 * @author Joseph A. Huwaldt, Date: August 26, 2015
 * @version November 28, 2015
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class Triangle extends GeomTriangle implements ValueType {

    /**
     * The corners of the triangle.
     */
    private Point _p1, _p2, _p3;
    
    /**
     * The surface area of this triangle.
     */
    private Parameter<Area> _area;
    
    /**
     * The surface normal vector for this triangle.
     */
    private Vector<Dimensionless> _n;
    

    /**
     * Returns a <code>Triangle</code> instance with the specified corner
     * vertices in counter-clockwise order/winding when looking down the surface normal
     * vector. The units of the triangle will be the units of the start point.
     *
     * @param p1 The start (beginning) of the triangle. May not be null.
     * @param p2 The second point in the triangle. May not be null.
     * @param p3 The third and last point in the triangle. May not be null.
     * @return A <code>Triangle</code> instance defined by the input points.
     */
    public static Triangle valueOf(GeomPoint p1, GeomPoint p2, GeomPoint p3) {

        //  Check the dimensionality.
        int numDims = GeomUtil.maxPhyDimension(p1,p2,p3);
        if (numDims < 2)
            throw new DimensionException(MessageFormat.format(
                    RESOURCES.getString("dimensionNotAtLeast2"), "triangle",numDims) );
        Unit<Length> unit = p1.getUnit();

        Triangle T = newInstance();
        T._p1 = p1.immutable().toDimension(numDims);
        T._p2 = p2.immutable().to(unit).toDimension(numDims);
        T._p3 = p3.immutable().to(unit).toDimension(numDims);
        
        computeTriData(T);
        
        return T;
    }

    /**
     * Returns a <code>Triangle</code> instance with the specified corner
     * vertices in counter-clockwise order/winding when looking down the surface normal
     * vector. The units of the triangle will be the units of the start point.
     *
     * @param points A list of 3 points that form the triangle. May not be null.
     * @return A <code>Triangle</code> instance defined by the input points.
     */
    public static Triangle valueOf(List<? extends GeomPoint> points) {
        if (points.size() < 3)
            throw new IllegalArgumentException(RESOURCES.getString("triThreePointsOnly"));
        
        return valueOf(points.get(0), points.get(1), points.get(2));
    }
    
    /**
     * Returns a <code>Triangle</code> instance with the specified corner
     * vertices in counter-clockwise order/winding when looking down the surface normal
     * vector. The units of the triangle will be the units of the start point.
     *
     * @param points An array of 3 points that form the triangle. May not be null.
     * @return A <code>Triangle</code> instance defined by the input points.
     */
    public static Triangle valueOf(GeomPoint[] points) {
        if (points.length < 3)
            throw new IllegalArgumentException(RESOURCES.getString("triThreePointsOnly"));
        
        return valueOf(points[0], points[1], points[2]);
    }
    
    /**
     * Compute some values for a triangle defined by 3 points. It is assumed that the
     * points have been set for the Triangle "T" before calling this method.
     */
    private static void computeTriData(Triangle T) {
        Point p1 = T._p1;
        Point p2 = T._p2;
        Point p3 = T._p3;
        
        StackContext.enter();
        try {
            //  Compute the triangle surface area and normal vector.
            Vector<Length> v13 = p3.minus(p1).toGeomVector();
            Vector<Length> v12 = p2.minus(p1).toGeomVector();
            Vector n;
            Parameter<Area> area;
            int numDims = T.getPhyDimension();
            if (numDims > 2) {
                //  area = |v12 X v13|/2 = |n|/2
                n = v12.cross(v13);
                area = n.norm().times(0.5);
            } else {
                //  Triangle area = (v1.X*v2.Y - v1.Y*v2.X)/2
                area = GeomUtil.crossArea(v12, v13).times(0.5);
                Parameter<Area> ZERO = Parameter.ZERO_AREA.to(area.getUnit());
                n = Vector.valueOf(ZERO, ZERO, area);
                area = area.abs();
            }

            if (area.isApproxZero())
                n = Vector.newInstance(p1.getPhyDimension());
            n = n.toUnitVector();
            
            //  Calculate the mean vertex location and set normal vector to that location.
            Point mp = p1.plus(p2).plus(p3).divide(3);
            n.setOrigin(mp);
            
            //  Copy out the results.
            T._area = StackContext.outerCopy(area);
            T._n = StackContext.outerCopy(n);
            
        } finally {
            StackContext.exit();
        }
    }
    
    /**
     * Recycles a <code>Triangle</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to recycle immediately.
     */
    public static void recycle(Triangle instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Return the first vertex in this triangle.
     *
     * @return The first vertex in this triangle.
     */
    @Override
    public Point getP1() {
        return _p1;
    }

    /**
     * Return the second vertex in this triangle.
     *
     * @return The second vertex in this triangle.
     */
    @Override
    public Point getP2() {
        return _p2;
    }

    /**
     * Return the third and last vertex in this triangle.
     *
     * @return The third and last vertex in this triangle.
     */
    @Override
    public Point getP3() {
        return _p3;
    }

    /**
     * Return the surface unit normal vector for this triangle.
     * If the triangle is degenerate (zero area), then the
     * normal vector will have zero length.
     * 
     * @return The surface normal vector for this triangle.
     */
    @Override
    public Vector<Dimensionless> getNormal() {
        return _n.copy();
    }
    
    /**
     * Return the surface area of one side of this triangle.
     * The returned area is always positive, but can be zero.
     * 
     * @return The surface area of one side of this triangle.
     */
    @Override
    public Parameter<Area> getArea() {
        return _area;
    }
    
    /**
     * Return a new triangle that is identical to this one, but with the order
     * of the points (and the surface normal direction) reversed.
     *
     * @return A new Triangle that is identical to this one, but with the order
     * of the points reversed.
     */
    @Override
    public Triangle reverse() {

        //  Create the reversed curve.
        Triangle T = Triangle.valueOf(_p3, _p2, _p1);

        return copyState(T);    //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner
     * of this geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMin() {
        return _p1.min(_p2.min(_p3));
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner
     * (e.g.: max X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     */
    @Override
    public Point getBoundsMax() {
        return _p1.max(_p2.max(_p3));
    }

    /**
     * Returns transformed version of this element. The returned object
     * implements {@link geomss.geom.GeomTransform} and contains this element as
     * a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new triangle that is identical to this one with the
     * specified transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public TriangleTrans getTransformed(GTransform transform) {
        return TriangleTrans.newInstance(this, requireNonNull(transform));
    }
    /**
     * Return a copy of this Triangle converted to the specified number of
     * physical dimensions. If the number of dimensions is greater than this
     * element, then zeros are added to the additional dimensions. If the number
     * of dimensions is less than this element, then the extra dimensions are
     * simply dropped (truncated). If the new dimensions are the same as the
     * dimension of this element, then this element is simply returned.
     *
     * @param newDim The dimension of the Triangle to return.
     * @return This Triangle converted to the new dimensions.
     */
    @Override
    public Triangle toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;

        Triangle T = Triangle.valueOf(_p1.toDimension(newDim), _p2.toDimension(newDim), _p3.toDimension(newDim));

        return copyState(T);    //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Returns the equivalent to this element but stated in the specified unit.
     *
     * @param unit the length unit of the element to be returned. May not be null.
     * @return an equivalent to this element but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public Triangle to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        Triangle T = Triangle.valueOf(_p1.to(unit), _p2.to(unit), _p3.to(unit));
        return copyState(T);    //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Returns a copy of this Triangle instance allocated by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this Triangle.
     */
    @Override
    public Triangle copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges
     * removed (applied).
     *
     * @return A copy of this object with any transformations or subranges
     *      removed (applied).
     */
    @Override
    public Triangle copyToReal() {
        return copy();
    }

    /**
     * Compares this Triangle against the specified object for strict equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this Triangle is identical to that
     * Triangle; <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        Triangle that = (Triangle)obj;
        return this._p1.equals(that._p1)
                && this._p2.equals(that._p2)
                && this._p3.equals(that._p3)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_p1, _p2, _p3);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<Triangle> XML = new XMLFormat<Triangle>(Triangle.class) {

        @Override
        public Triangle newInstance(Class<Triangle> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, Triangle obj) throws XMLStreamException {
            GeomTriangle.XML.read(xml, obj);     // Call parent read.

            obj._p1 = xml.getNext();
            obj._p2 = xml.getNext();
            obj._p3 = xml.getNext();

            computeTriData(obj);
        }

        @Override
        public void write(Triangle obj, OutputElement xml) throws XMLStreamException {
            GeomTriangle.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._p1);
            xml.add(obj._p2);
            xml.add(obj._p3);

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private Triangle() {
    }

    private static final ObjectFactory<Triangle> FACTORY = new ObjectFactory<Triangle>() {
        @Override
        protected Triangle create() {
            return new Triangle();
        }

        @Override
        protected void cleanup(Triangle obj) {
            obj.reset();
            obj._p1 = null;
            obj._p2 = null;
            obj._p3 = null;
        }
    };

    private static Triangle copyOf(Triangle original) {
        Triangle o = FACTORY.object();
        o._p1 = original._p1.copy();
        o._p2 = original._p2.copy();
        o._p3 = original._p3.copy();
        computeTriData(o);
        original.copyState(o);
        return o;
    }

    private static Triangle newInstance() {
        return FACTORY.object();
    }

}
