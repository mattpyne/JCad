/*
 *   IntersectType  -- Indicates the type of intersection that resulted from certain calculations.
 *
 *   Copyright (C) 2011-2015, by Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

/**
 * Indicates the type of intersection that resulted from certain intersection
 * calculations.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: October 31, 2011
 * @version August 29, 2015
 */
public enum IntersectType {

    /**
     * There is no intersection.
     */
    DISJOINT,
    /**
     * There is at least one intersection.
     */
    INTERSECT,
    /**
     * The objects are coincident (the entire object intersects).
     */
    COINCIDENT;
}
