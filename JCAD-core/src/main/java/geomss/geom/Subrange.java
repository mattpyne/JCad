/*
 *   Subrange  -- Interface all subrange type objects.
 *
 *   Copyright (C) 2009-2015, Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

/**
 * Defines the interface for {@link GeomElement} objects that are subranges of other
 * {@link ParametricGeometry} objects.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 28, 2009
 * @version September 3, 2015
 *
 * @param <T> The type of this subrange object.
 */
public interface Subrange<T extends GeomElement> {

    /**
     * Returns the parametric position on the child object that this subrange refers to.
     *
     * @return The parametric position on the child object that this subrange refers to.
     */
    public T getParPosition();

    /**
     * Sets the parametric position on the child object that this subrange refers to.
     *
     * @param par The parametric position (between 0 and 1) along each parametric
     *            dimension where the subrange is located (may not be <code>null</code>).
     */
    public void setParPosition(T par);

    /**
     * Returns the child object this subrange object is subranged onto.
     *
     * @return The child object this object is subranged onto.
     */
    public ParametricGeometry getChild();

}
