/**
 * TriangleList -- A concrete list of GeomTriangle objects.
 *
 * Copyright (C) 2015-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import static geomss.geom.AbstractGeomElement.RESOURCES;
import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.util.FastMap;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Integer64;

/**
 * A concrete list of {@link GeomTriangle} objects.  This list is similar
 * to a GeomList, but has methods specifically designed for working with large
 * sets of triangles. All the triangles in a list must have the same physical dimensions.
 * <p>
 * WARNING: This list allows geometry to be stored in different units. If consistent units
 * are required, then the user must specifically convert the list items.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt   </p>
 *
 * @author Joseph A. Huwaldt, Date: August 28, 2015
 * @version January 31, 2017
 *
 * @param <E> The type of triangle stored in this list.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class TriangleList<E extends GeomTriangle> extends AbstractGeomList<TriangleList,E> {

    private FastTable<E> _list;

    /**
     * Return the list underlying this geometry list.
     *
     * @return The list underlying this geometry list.
     */
    @Override
    protected FastTable<E> getList() {
        return _list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>TriangleList</code>
     * instance (on the stack when executing in a <code>StackContext</code>)
     * that can store a list of {@link GeomTriangle} objects.
     *
     * @return A new, empty TriangleList.
     */
    public static TriangleList newInstance() {
        TriangleList list = FACTORY.object();
        list._list = FastTable.newInstance();
        return list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>TriangleList</code>
     * instance (on the stack when executing in a <code>StackContext</code>)
     * with the specified name, that can store a list of {@link GeomTriangle}
     * objects.
     *
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @return A new, empty TriangleList with the specified name.
     */
    public static TriangleList newInstance(String name) {
        TriangleList list = TriangleList.newInstance();
        list.setName(name);
        return list;
    }

    /**
     * Return a TriangleList containing the {@link GeomTriangle} objects in the
     * specified collection.
     *
     * @param <E> The type of triangle contained in the returned list of triangles.
     * @param name The name to be assigned to this list (may be
     *  <code>null</code>).
     * @param elements A collection of triangle elements. May not be null.
     * @return A new TriangleList containing the elements in the specified collection.
     */
    public static <E extends GeomTriangle> TriangleList<E> valueOf(String name, Collection<E> elements) {
        for (Object element : elements) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof GeomTriangle))
            throw new ClassCastException(MessageFormat.format(
                    RESOURCES.getString("listElementTypeErr"), "TriangleList", "GeomTriangle"));
        }

        TriangleList<E> list = TriangleList.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a TriangleList containing the {@link GeomTriangle} objects in the
     * specified list.
     *
     * @param <E> The type of triangle contained in the returned list of triangles.
     * @param name The name to be assigned to this list (may be
     *  <code>null</code>).
     * @param elements A list of triangle elements. May not be null.
     * @return A new TriangleList containing the elements in the specified list.
     */
    public static <E extends GeomTriangle> TriangleList<E> valueOf(String name, E... elements) {
        requireNonNull(elements);
        TriangleList<E> list = TriangleList.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a TriangleList containing the {@link GeomTriangle} objects in the
     * specified array.
     *
     * @param <E> The type of triangle contained in the returned list of triangles.
     * @param elements An array of triangle elements. May not be null.
     * @return A new TriangleList containing the elements in the specified array.
     */
    public static <E extends GeomTriangle> TriangleList<E> valueOf(E... elements) {
        return TriangleList.valueOf(null, elements);
    }

    /**
     * Return a TriangleList containing the triangles
     *
     * @param vertices A list of all the vertices in the list of triangles. May not be null.
     * @param indexes An array of indexes into the vertices list.  Each set of 3 consecutive
     * indexes into the vertices list defines a triangle. May not be null.
     * @return A new TriangleList containing the elements in the specified collection.
     */
    public static TriangleList<Triangle> valueOf(List<? extends GeomPoint> vertices, int[] indexes) {
        requireNonNull(vertices);
        requireNonNull(indexes);

        TriangleList<Triangle> list = TriangleList.newInstance();
        int numTris = indexes.length/3;
        int pos = 0;
        for (int i=0; i < numTris; ++i) {
            GeomPoint p1 = vertices.get(indexes[pos++]);
            GeomPoint p2 = vertices.get(indexes[pos++]);
            GeomPoint p3 = vertices.get(indexes[pos++]);
            Triangle tri = Triangle.valueOf(p1, p2, p3);
            list.add(tri);
        }

        return list;
    }

    /**
     * Return a TriangleList containing a simple triangulation of the input
     * array of quadrilateral panels. The input quads are triangulated by
     * dividing each quad into two triangles. Any subranges or transforms on the
     * input points is lost by this triangulation.
     *
     * @param quads An array of quadrilateral panels (defined by a topologically
     * rectangular array of points). May not be null.
     * @return A new TriangleList containing a triangulation of the input quads.
     */
    public static TriangleList<Triangle> triangulateQuads(PointArray<? extends GeomPoint> quads) {
        requireNonNull(quads);

        //  Create an empty triangle list.
        TriangleList<Triangle> list = TriangleList.newInstance();

        //  Loop over all the panels in the array and turn each into two triangles.
        int numRows = quads.size();
        int numCols = quads.get(0).size();

        //  Loop over the strings.
        for (int row = 1; row < numRows; ++row) {
            PointString<?> str1 = quads.get(row - 1);
            PointString<?> str2 = quads.get(row);

            //  Loop over points in strings.
            for (int col = 1; col < numCols; ++col) {
                // Get quad points.
                Point p1 = str1.get(col - 1).immutable();
                Point p2 = str1.get(col).immutable();
                Point p3 = str2.get(col).immutable();
                Point p4 = str2.get(col - 1).immutable();

                //  Form the 1st triangle: p1,p2,p4
                Triangle tri = Triangle.valueOf(p1,p2,p4);
                list.add(tri);

                //  Form the 2nd triangle: p4,p2,p3
                tri = Triangle.valueOf(p4,p2,p3);
                list.add(tri);
            }
        }

        return list;
    }

    /**
     * Returns the number of physical dimensions of this geometry element. This
     * implementation always returns the physical dimension of the underlying
     * {@link Triangle} objects or 0 if this list has no Triangle objects in it.
     *
     * @return The number of physical dimensions of this geometry element.
     */
    @Override
    public int getPhyDimension() {
        if (isEmpty())
            return 0;
        return get(0).getPhyDimension();
    }

    /**
     * Returns the range of elements in this list from the specified start and
     * ending indexes.
     *
     * @param first index of the first element to return (0 returns the 1st
     *  element, -1 returns the last, etc).
     * @param last index of the last element to return (0 returns the 1st
     *  element, -1 returns the last, etc).
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     *  <code>index >= size()</code>
     */
    @Override
    public TriangleList<E> getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        TriangleList list = TriangleList.newInstance();
        for (int i=first; i <= last; ++i)
            list.add(get(i));
        return list;
    }

    /**
     * Replaces the {@link GeomTriangle} at the specified position in this list with the
     * specified element. Null elements are ignored. The input element must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param index   The index of the element to replace (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The element to be stored at the specified position.
     *                <code>null</code> elements are ignored.
     * @return The element previously at the specified position in this list. May not be
     *         null.
     * @throws java.lang.IndexOutOfBoundsException - if <code>index > size()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public E set(int index, E element) {
        requireNonNull(element);
        if (size() > 0 && element.getPhyDimension() != this.getPhyDimension())
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        return super.set(index, element);
    }

    /**
     * Inserts the specified {@link GeomTriangle} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored. The input
     * element must have the same physical dimensions as the other items in this list, or
     * an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the list is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value The element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public void add(int index, E value) {
        requireNonNull(value);
        if (size() > 0 && value.getPhyDimension() != this.getPhyDimension())
            throw new DimensionException(RESOURCES.getString("dimensionErr"));
        super.add(index, value);
    }

    /**
     * Inserts all of the {@link GeomTriangle} objects in the specified collection into
     * this list at the specified position. Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (increases their indices). The
     * new elements will appear in this list in the order that they are returned by the
     * specified collection's iterator. The behavior of this operation is unspecified if
     * the specified collection is modified while the operation is in progress. Note that
     * this will occur if the specified collection is this list, and it's nonempty.  The
     * input elements must have the same physical dimensions as the other items in this
     * list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified collection.
     * @param c     Elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        int thisSize = this.size();
        for (Object element : c) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof GeomTriangle))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "TriangleList", "GeomTriangle"));
            if (thisSize != 0) {
                if (((GeomElement)element).getPhyDimension() != this.getPhyDimension())
                    throw new DimensionException(RESOURCES.getString("dimensionErr"));
            }
        }
        return super.addAll(index,c);
    }

    /**
     * Returns an new {@link TriangleList} with the elements in this list in reverse
     * order.
     *
     * @return A new TriangleList with the elements in this list in reverse order.
     */
    @Override
    public TriangleList<E> reverse() {
        TriangleList newList = TriangleList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i=size-1; i >= 0; --i) {
            newList.add(get(i));
        }
        return newList;
    }

    /**
     * Return a new triangle list that is identical to this one, but with any degenerate
     * triangles removed.
     *
     * @param tol The tolerance for determining if a triangle is degenerate. A value of
     *            <code>null</code> would indicate that exactly zero area is required to
     *            be degenerate.
     * @return A new triangle list that is identical to this one, but with any degenerate
     *         triangles removed.
     */
    public TriangleList<E> removeDegenerate(Parameter<Length> tol) {
        TriangleList<E> newList = TriangleList.newInstance();
        copyState(newList);
        int size = size();
        for (int i=0; i < size; ++i) {
            E tri = get(i);
            if (!tri.isDegenerate(tol))
                newList.add(tri);
        }
        return newList;
    }

    /**
     * Return the total surface area of this list of triangles. This is the sum of the
     * area of all the triangles in this list.
     *
     * @return The surface area of all the triangles in this list.
     */
    public Parameter<Area> getArea() {
        StackContext.enter();
        try {

            Parameter<Area> area = Parameter.ZERO_AREA.to(getUnit().pow(2).asType(Area.class));
            for (E tri : this)
                area = area.plus(tri.getArea());

            return StackContext.outerCopy(area);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a copy of this list converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element,
     * then zeros are added to the additional dimensions. If the number of
     * dimensions is less than this element, then the extra dimensions are
     * simply dropped (truncated). If the new dimensions are the same as the
     * dimension of this element, then this list is simply returned.
     *
     * @param newDim The dimension of the element to return.
     * @return A copy of this list converted to the new dimensions.
     */
    @Override
    public TriangleList<E> toDimension(int newDim) {
        TriangleList newList = TriangleList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i=0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.toDimension(newDim));
        }
        return newList;
    }

    /**
     * Returns the equivalent to this list but with <I>all</I> the elements stated in the
     * specified unit.
     *
     * @param unit the length unit of the list to be returned. May not be null.
     * @return an equivalent to this list but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public TriangleList<E> to(Unit<Length> unit) {
        requireNonNull(unit);
        TriangleList newList = TriangleList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.to(unit));
        }
        return newList;
    }

    /**
     * Returns a copy of this <code>TriangleList</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public TriangleList<E> copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges
     * removed (applied).
     *
     * @return A copy of this list with any transformations or subranges removed.
     */
    @Override
    public TriangleList<Triangle> copyToReal() {
        TriangleList newList = TriangleList.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i=0; i < size; ++i) {
            E element = this.get(i);
            newList.add(element.copyToReal());
        }
        return newList;
    }

    /**
     * Returns transformed version of this element. The returned object
     * implements {@link GeomTransform} and contains transformed versions of the
     * contents of this list as children. Any list elements that are not
     * transformable will simply be added to the output list without
     * transformation.
     *
     * @param transform The transform to apply to this geometry element. May not be null.
     * @return A transformed version of this geometry element.
     * @throws DimensionException if this element is not 3D.
     */
    @Override
    public TriangleList<TriangleTrans> getTransformed(GTransform transform) {
        requireNonNull(transform);
        TriangleList list = TriangleList.newInstance();
        copyState(list);    //  Copy over the super-class state for this object to the new one.
        int size = this.size();
        for (int i=0; i < size; ++i) {
            E element = this.get(i);
            if (element instanceof Transformable)
                list.add(((Transformable)element).getTransformed(transform));
            else
                list.add(element);
        }
        return list;
    }

    /**
     * Convert this list of Triangle objects into a TriangleVertData structure with a
     * non-repeating (unique) list of vertices, non-degenerate triangles defined by
     * indices into the list of vertices, areas of all the triangles, and normals for all
     * the triangles. This is handy for converting between this list of triangles and
     * various triangulated file formats.
     *
     * @param tol The position tolerance used to determine if two vertices are coincident.
     *            Pass <code>null</code> or 0 if all vertices are to be retained.
     * @return A TriangleVertData structure with information about the triangles in this
     *         list.
     */
    public TriangleVertData toVertData(Parameter<Length> tol) {

        //  Create lists to contain per-triangle data.
        FastTable<Parameter<Area>> areas = FastTable.newInstance(); //  The area of each triangle.
        FastTable<GeomVector<Dimensionless>> normals = FastTable.newInstance(); //  The normal vector of each triangle.

        //  Remove degenerate triangles.
        TriangleList<E> triLst = this;
        if (nonNull(tol) && !tol.isApproxZero())
            triLst = triLst.removeDegenerate(tol);

        //  Create an index into the vertex list for the vertices of each triangle.
        int numTris = triLst.size();
        int[] triIndexes = new int[numTris * 3];

        //  Identify all the unique vertices in the list of triangles.
        PointString<GeomPoint> verts = uniqueVerts(triLst, triIndexes, tol);

        //  Fill in the lists of areas and normal vectors.
        for (int i = 0; i < numTris; ++i) {
            E tri = triLst.get(i);
            areas.add(tri.getArea());
            normals.add(tri.getNormal());
        }

        //  Create the output data structure and fill it in.
        TriangleVertData output = TriangleVertData.newInstance();
        output.vertices = verts;
        output.numTris = numTris;
        output.tris = triIndexes;
        output.areas = areas;
        output.normals = normals;

        return output;
    }

    /**
     * Return a map of unique vertices for each triangle in this list. Coincident vertices
     * are determined using the supplied tolerance on vertex position.
     *
     * @param triLst     A list of non-degenerate Triangle objects.
     * @param triIndexes An array of indexes into "verts" for each triangle as consecutive
     *                   sets of 3 indexes.
     * @return A new vertex list with a unique set of vertices. The input triIndexes array
     *         is also modified to use indexes into the output list of vertices.
     * @param tol        The position tolerance used to determine if two vertices are
     *                   coincident.
     */
    private <E extends GeomTriangle> PointString<GeomPoint> uniqueVerts(TriangleList<E> triLst,
            int[] triIndexes, Parameter<Length> tol) {

        /*  Algorithm is similar to the one for edges in "admesh": https://github.com/admesh/admesh
         1. Each vertex is turned into a TriVert object.
         2. Use a hash map to store the vertices.  The hash code for TriVert is such that
         any vertex with vertices within "tol" of each other should return the same
         hash code.
         3. Repeat for all triangles.  If a vertex with the same hash code already exists
         in the hash map, then a duplicate vertex has been found.
         */
        FastMap<TriVert, TriVert> vertMap = FastMap.newInstance();
        PointString<GeomPoint> vertLst = PointString.newInstance();
        GeomPoint[] triVerts = new GeomPoint[3];

        //  Loop over all the triangles in this list.
        int vert_idx = 0;
        int numTris = triLst.size();
        for (int triIdx = 0; triIdx < numTris; ++triIdx) {
            GeomTriangle tri = triLst.get(triIdx);
            if (!tri.isDegenerate(tol)) {
                tri.getPoints(triVerts);

                //  Loop over the three vertices of the triangle.
                for (int i = 0; i < 3; ++i) {
                    //  Get the vertex point for this triangle.
                    GeomPoint vertex = triVerts[i];

                    //  Form a vertex object using this vertex point.
                    TriVert vThis = TriVert.newInstance(vert_idx, vertex, tol);

                    //  Insert this vertex into the hash map and look for a duplicate.
                    boolean exists = vertMap.containsKey(vThis);
                    if (exists) {
                        //  Found a duplicate vertex; get the original from the map.
                        TriVert vOther = vertMap.get(vThis);

                        //  Index this triangles vertex to the original one.
                        triIndexes[triIdx * 3 + i] = vOther.vert_index;

                        //  Recycle the redudant vertex.
                        TriVert.recycle(vThis);

                    } else {
                        // If the vertex isn't in the map yet, add it.
                        vertMap.put(vThis, vThis);

                        //  Add the vertex to the output list as well (as position "vert_idx").
                        vertLst.add(vertex);

                        //  Index this triangle's vertex to the correct position in the list.
                        triIndexes[triIdx * 3 + i] = vert_idx;

                        //  Prepare for next vertex.
                        ++vert_idx;
                    }
                }
            }
        }

        //  Recycle the TriVert objects now that we are done with them.
        for (FastMap.Entry<TriVert, TriVert> e = vertMap.head(), end = vertMap.tail(); (e = e.getNext()) != end;) {
            TriVert v = e.getKey();
            TriVert.recycle(v);
        }
        FastMap.recycle(vertMap);

        return vertLst;
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<TriangleList> XML = new XMLFormat<TriangleList>(TriangleList.class) {

        @Override
        public TriangleList newInstance(Class<TriangleList> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            TriangleList obj = TriangleList.newInstance();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, TriangleList obj) throws XMLStreamException {
            AbstractGeomList.XML.read(xml, obj);     // Call parent read.
        }

        @Override
        public void write(TriangleList obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractGeomList.XML.write(obj, xml);    // Call parent write.
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<TriangleList> FACTORY = new ObjectFactory<TriangleList>() {
        @Override
        protected TriangleList create() {
            return new TriangleList();
        }

        @Override
        protected void cleanup(TriangleList obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a TriangleList instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(TriangleList instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected TriangleList() {}

    private static TriangleList copyOf(TriangleList<? extends GeomElement> original) {
        TriangleList o = TriangleList.newInstance();
        original.copyState(o);
        int size = original.size();
        for (int i=0; i < size; ++i) {
            GeomElement element = original.get(i);
            o.add(element.copy());
        }
        return o;
    }

    /**
     * An object that represents a vertex of a triangle. This is used to sort out a unique
     * set of vertices.
     */
    private static class TriVert {

        private static final int ONE = 1 << 16;

        public int vert_index;  //  The index to this vertex in the full vertex list.
        public int hashCode;    //  The hash code for this vertex.
        private FastTable<Integer64> vertFP;  //  The vertex coordinates in fixed point.

        /**
         * Return a new TriVert object using the supplied vertex index, vertex and
         * tolerance for use in determining if this vertex is coincident with another.
         *
         * @param vert_index The index in the full vertex list of this vertex.
         * @param vert       The actual vertex point represented by this TriVert object.
         * @param tol        The tolerance for determining if two vertices are coincident.
         * @return A new TriVert object.
         */
        public static TriVert newInstance(int vert_index, GeomPoint vert, Parameter<Length> tol) {
            TriVert o = FACTORY.object();
            o.vert_index = vert_index;

            //  Round off the input vertex coordinates and convert them to fixed-point integers.
            double tolv = 0;
            if (nonNull(tol) && !tol.isApproxZero())
                tolv = 2.0 * tol.doubleValue(vert.getUnit());

            o.vertFP = FastTable.newInstance();
            int numDims = vert.getPhyDimension();
            for (int i = 0; i < numDims; ++i) {
                double value = vert.getValue(i);
                value = approxValue(value, tolv);
                int fp = toFix(value);
                o.vertFP.add(Integer64.valueOf(fp));
            }

            //  Pre-compute the hash code.
            o.hashCode = o.vertFP.hashCode();

            return o;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if ((obj == null) || (obj.getClass() != this.getClass()))
                return false;

            TriVert that = (TriVert)obj;
            return this.vertFP.equals(that.vertFP);
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

        //  Convert a double to fixed-point integer.
        public static int toFix(double val) {
            return (int)(val * ONE);
        }

        //  Round a float to the specified tolerance.
        private static double approxValue(double value, double tolv) {
            if (tolv > 0.0)
                //  Round to tolerance (approximately).
                return tolv * Math.round(value / tolv);
            return value;
        }

        public static void recycle(TriVert instance) {
            FACTORY.recycle(instance);
        }

        private TriVert() { }

        private static final ObjectFactory<TriVert> FACTORY = new ObjectFactory<TriVert>() {
            @Override
            protected TriVert create() {
                return new TriVert();
            }

            @Override
            protected void cleanup(TriVert obj) {
                FastTable.recycle(obj.vertFP);
                obj.vertFP = null;
            }
        };
    }
}
