/**
 * CurveTestSuite -- An suite of unit tests for the geomss.geom curves.
 *
 * Copyright (C) 2014-2015 by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import geomss.geom.nurbs.CurveFactory;
import geomss.geom.nurbs.CurveUtils;
import geomss.geom.nurbs.NurbsCurve;
import jahuwaldt.js.param.Parameter;
import jahuwaldt.tools.math.MathTools;
import java.util.List;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import static javax.measure.unit.NonSI.*;
import static javax.measure.unit.SI.*;
import static javolution.lang.MathLib.*;
import javolution.testing.TestCase;
import javolution.testing.TestContext;
import javolution.testing.TestSuite;
import javolution.text.TextBuilder;

/**
 * This class holds the {@link geomss.geom} Curve unit tests and benchmarks.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 28, 2014
 * @version August 31, 2015
 */
public class CurveTestSuite extends TestSuite {

    private static final Parameter<Length> TOL = Parameter.valueOf(0.1, INCH);

    @Override
    public void run() {
        TestContext.info("---------------------------------------");
        TestContext.info("-- Test Suite for geomss.geom curves --");
        TestContext.info("---------------------------------------");

        //  A simple line segment.
        String typeStr = "LineSeg";
        TestContext.info("-- " + typeStr);
        Point p0 = Point.valueOf(0, 0, FOOT);
        Point p1 = Point.valueOf(2, 3, 5, METER);
        LineSeg line1 = LineSeg.valueOf(p0, p1);
        GenericSingleCurveTests gCrvTest = new GenericSingleCurveTests(line1, typeStr);
        gCrvTest.isPlanar = true;
        gCrvTest.isCircular = false;
        gCrvTest.isLine = true;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = p1.plus(p0.toDimension(3)).divide(2);
        gCrvTest.pu = Vector.valueOf(Point.valueOf(2, 3, 5, METER).minus(Point.valueOf(0, 0, 0, FOOT)));
        gCrvTest.puu = Vector.valueOf(METER, 0, 0, 0);
        gCrvTest.voc = Parameter.valueOf(0, FOOT.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        TestContext.test(new ArcLengthTest(line1, Parameter.valueOf(20.2244553903182, FOOT), typeStr));

        //  Test enclosed area
        TestContext.test(new EnclosedAreaTest(line1, Parameter.valueOf(0, Area.UNIT), typeStr));

        //  Point-Curve closest test.
        p0 = Point.valueOf(1, 1, 1);
        TestContext.test(new PointCurveClosestTest(line1, p0, line1.getPoint(0.263157894736842), typeStr));

        //  Test Curve-LineSeg intersection.
        p0 = Point.valueOf(0.333333333333333, 1.83333333333333, 1.83333333333333, METER);
        p1 = Point.valueOf(1.66666666666667, 1.16666666666667, 3.16666666666667, METER);
        LineSeg line2 = LineSeg.valueOf(p0, p1);
        TestContext.test(new CCIntersectionTest(line1, line2, line1.getPoint(0.5), typeStr + "-LineSeg"));

        //  Test Curve-Plane intersection.
        Plane pln = Plane.getXZ();
        pln = pln.changeRefPoint(Point.valueOf(0.5, 0.5, 0.5));
        TestContext.test(new CPlaneIntersectionTest(line1, pln, line1.getPoint(0.0508), typeStr));

        //  A 2D circle through 3 points.
        typeStr = "NURBS 2D Circular Arc";
        TestContext.info("-- " + typeStr);
        p0 = Point.valueOf(-1, 0);
        p1 = Point.valueOf(0, 1.5);
        Point p2 = Point.valueOf(1, 0);
        Curve curve = CurveFactory.createCircularArc(p0, p1, p2);
        gCrvTest = new GenericSingleCurveTests(curve, typeStr);
        gCrvTest.isPlanar = true;
        gCrvTest.isCircular = true;
        gCrvTest.isLine = false;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = Point.valueOf(METER, 0, 1.5);
        gCrvTest.pu = Vector.valueOf(METER, 4.417961017824146, 0);
        gCrvTest.puu = Vector.valueOf(METER, -3.27e-15, -18.016965743089628);
        gCrvTest.voc = Parameter.valueOf(0, METER.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        TestContext.test(new ArcLengthTest(curve, Parameter.valueOf(4.25877278332892, METER), typeStr));

        //  Test enclosed area
        TestContext.test(new EnclosedAreaTest(curve, Parameter.valueOf(-2.72350193375409, Area.UNIT), typeStr));

        //  Point-Curve closest test.
        p0 = Point.valueOf(2, 1);
        TestContext.test(new PointCurveClosestTest(curve, p0, curve.getPoint(0.827597726725164), typeStr));

        //  Test Curve-LineSeg intersection.
        p0 = Point.valueOf(-2, 0.5, METER);
        p1 = Point.valueOf(3, 0, METER);
        line2 = LineSeg.valueOf(p0, p1);
        TestContext.test(new CCIntersectionTest(curve, line2, curve.getPoint(0.100516622598477), typeStr + "-LineSeg"));

        //  Test Curve-Curve intersection.
        p0 = Point.valueOf(0.5, 1);
        Parameter R = Parameter.valueOf(1, METER);
        Curve curve2 = CurveFactory.createCircle(p0, R, null);
        TestContext.test(new CCIntersectionTest(curve, curve2, curve.getPoint(0.404600667366484), typeStr + "-" + typeStr));

        //  A 3D semi-circle
        typeStr = "NURBS 3D Circular Arc";
        TestContext.info("-- " + typeStr);
        Point o = Point.valueOf(0, 0, 0, FOOT);
        Parameter<Length> r = Parameter.valueOf(3, METER);
        Vector<Dimensionless> nhat = Vector.valueOf(Dimensionless.UNIT, 0, 0, 1);
        curve = CurveFactory.createSemiCircle(o, r, nhat);
        gCrvTest = new GenericSingleCurveTests(curve, typeStr);
        gCrvTest.isPlanar = true;
        gCrvTest.isCircular = true;
        gCrvTest.isLine = false;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = Point.valueOf(METER, 3, 0, 0);
        gCrvTest.pu = Vector.valueOf(METER, 0, 12, 0);
        gCrvTest.puu = Vector.valueOf(METER, -48, 0, 0);
        gCrvTest.voc = Parameter.valueOf(0, FOOT.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        Parameter<Length> arcL = r.times(PI);                  //  Half the perimeter of a circle.
        TestContext.test(new ArcLengthTest(curve, arcL, typeStr));

        //  Test enclosed area
        Parameter<Area> area = r.pow(2).times(PI / 2).asType(Area.class);    //  Half-circle area.
        TestContext.test(new EnclosedAreaTest(curve, area, typeStr));

        //  Point-Curve closest test.
        p0 = Point.valueOf(1, 1, 1);
        TestContext.test(new PointCurveClosestTest(curve, p0, curve.getPoint(0.707106781186547), typeStr));

        //  Test Curve-LineSeg intersection.
        p0 = o;
        p1 = Point.valueOf(6, 2, 0, METER);
        line2 = LineSeg.valueOf(p0, p1);
        TestContext.test(new CCIntersectionTest(curve, line2, curve.getPoint(0.581152226585933), typeStr + "-LineSeg"));

        //  Test Curve-Curve intersection.
        curve2 = (Curve)curve.getTransformed(GTransform.newTranslation(1, 1, 0));
        TestContext.test(new CCIntersectionTest(curve, curve2, curve.getPoint(0.359375), typeStr + "-Transformed Curve"));

        //  Test Curve-Curve intersection.
        o = Point.valueOf(1, 1, 0, FOOT);
        Parameter<Length> a = Parameter.valueOf(9, FOOT);
        Parameter<Length> b = Parameter.valueOf(3, FOOT);
        curve2 = CurveFactory.createEllipse(o, a, b, nhat);
        TestContext.test(new CCIntersectionTest(curve, curve2, curve.getPoint(0.545983754390616), typeStr + "-Ellipse"));

        //  Test Curve-Plane intersection.
        pln = Plane.getXZ();
        pln = pln.changeRefPoint(Point.valueOf(0.5, 0.5, 0.5));
        TestContext.test(new CPlaneIntersectionTest(curve, pln, curve.getPoint(0.541960108450192), typeStr));

        //  A subrange curve.
        typeStr = "3D Subrange Curve on Curve";
        TestContext.info("-- " + typeStr);
        PointString<Point> pstr = PointString.newInstance();
        pstr.add(Point.valueOf(0), Point.valueOf(.5));
        Curve pcrv = CurveFactory.fitPoints(1, pstr);
        SubrangeCurve subrange = SubrangeCurve.newInstance(curve, pcrv);

        gCrvTest = new GenericSingleCurveTests(subrange, typeStr);
        gCrvTest.isPlanar = true;
        gCrvTest.isCircular = true;
        gCrvTest.isLine = false;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = Point.valueOf(METER, 1.8, -2.4, 0);
        gCrvTest.pu = Vector.valueOf(METER, 3.84, 2.88, 0);
        gCrvTest.puu = Vector.valueOf(METER, -1.536, 8.448, 0);
        gCrvTest.voc = Parameter.valueOf(0, FOOT.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        double theta = PI / 2;        //  1/4 circle swept area.
        arcL = r.times(theta);      //  1/4 the perimeter of a circle.
        TestContext.test(new ArcLengthTest(subrange, arcL, typeStr));

        //  Test enclosed area
        //  Segment area: area = 1/2*(theta - sin(theta))*R^2
        area = r.pow(2).times(theta - sin(theta)).divide(2).asType(Area.class);
        TestContext.test(new EnclosedAreaTest(subrange, area, typeStr));

        //  Point-Curve closest test.
        p0 = Point.valueOf(1.5, -1, 1);
        TestContext.test(new PointCurveClosestTest(subrange, p0, subrange.getPoint(0.697224362268005), typeStr));

        //  Test Curve-LineSeg intersection.
        p0 = Point.valueOf(0, 0, 0, FOOT);
        p1 = Point.valueOf(6, -2, 0, METER);
        line2 = LineSeg.valueOf(p0, p1);
        TestContext.test(new CCIntersectionTest(subrange, line2, subrange.getPoint(0.837695546828134), typeStr + "-LineSeg"));

        //  Test Curve-Curve intersection.
        o = Point.valueOf(1, 1, 0, FOOT);
        a = Parameter.valueOf(10, FOOT);
        b = Parameter.valueOf(5, FOOT);
        curve2 = CurveFactory.createEllipse(o, a, b, nhat);
        TestContext.test(new CCIntersectionTest(subrange, curve2, subrange.getPoint(0.92679405977522), typeStr + "-Ellipse"));

        //  Test Curve-Plane intersection.
        pln = Plane.getXZ();
        pln = pln.changeRefPoint(Point.valueOf(0.5, -1.5, 0.5));
        TestContext.test(new CPlaneIntersectionTest(subrange, pln, subrange.getPoint(0.732050807568877), typeStr));

        //  A subrange curve.
        typeStr = "3D Curve Through Points";
        TestContext.info("-- " + typeStr);
        PointString<Point> str1 = PointString.newInstance();
        str1.add(Point.valueOf(0, 0, 0), Point.valueOf(1, 1, 0), Point.valueOf(2, 1.5, 0));
        str1.add(Point.valueOf(3, 1.75, 1), Point.valueOf(4, 2, 2));
        curve = CurveFactory.fitPoints(3, str1);

        gCrvTest = new GenericSingleCurveTests(curve, typeStr);
        gCrvTest.isPlanar = false;
        gCrvTest.isCircular = false;
        gCrvTest.isLine = false;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = Point.valueOf(METER, 2.1375567981007073, 1.5489907738067152, 0.0599427358293912);
        gCrvTest.pu = Vector.valueOf(METER, 4.2551721869519445, 1.437044936214062, 2.2654614604729866);
        gCrvTest.puu = Vector.valueOf(METER, -7.346919800970919, -7.414459049425268, 22.37280206421387);
        gCrvTest.voc = Parameter.valueOf(-5.165250025112367, METER.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        TestContext.test(new ArcLengthTest(curve, Parameter.valueOf(5.504184818759, METER), typeStr));
        Parameter<Length> L1 = curve.getArcLength(1e-7);

        //  Join with another segment.
        typeStr = "Joined 3D Nurbs Curves";
        TestContext.info("-- " + typeStr);
        str1.clear();
        str1.add(Point.valueOf(4, 2, 2), Point.valueOf(4, 3, 3), Point.valueOf(4, 3, 2),
                Point.valueOf(3, 3, 2), Point.valueOf(1, 3, 2));
        NurbsCurve crv2 = CurveFactory.fitPoints(3, str1);
        curve = CurveUtils.connectCurves((NurbsCurve)curve, crv2);

        gCrvTest = new GenericSingleCurveTests(curve, typeStr);
        gCrvTest.isPlanar = false;
        gCrvTest.isCircular = false;
        gCrvTest.isLine = false;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = Point.valueOf(METER, 4, 2, 2);
        gCrvTest.pu = Vector.valueOf(METER, -7.431457505076182, 15.951226764720996, 35.11269837220809);
        gCrvTest.puu = Vector.valueOf(METER, 180.46752981725663, -161.18729397512158, -569.6080810142133);
        gCrvTest.voc = Parameter.valueOf(2.649490819105782, METER.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        arcL = L1.plus(crv2.getArcLength(1e-7));
        TestContext.test(new ArcLengthTest(curve, arcL, typeStr));

        //  Approximate a set of noisy points.
        typeStr = "Curve Approx. 2D Points";
        TestContext.info("-- " + typeStr);
        double[] x = {0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7,
            0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55,
            1.6, 1.65, 1.7, 1.75};
        double[] y = {1.0327, 1.0832, 0.9548, 1.0009, 0.9136, 0.8508, 0.9614, 0.9067, 0.7029,
            0.7606, 0.7364, 0.6087, 0.5048, 0.4615, 0.4303, 0.3087, 0.0888, 0.0181, 0.0843, -0.0079,
            -0.2066, -0.3073, -0.4174, -0.4720, -0.5499, -0.5301, -0.5602, -0.6351, -0.6817,
            -0.7629, -0.8734, -0.9388, -0.9578, -0.9029, -1.0318, -1.0116};
        str1 = PointString.newInstance();
        for (int i = 0; i < x.length; ++i) {
            double xv = x[i];
            double yv = y[i];
            Point p = Point.valueOf(xv, yv, METER);
            str1.add(p);
        }
        curve = CurveFactory.approxPoints(3, 6, str1);

        gCrvTest = new GenericSingleCurveTests(curve, typeStr);
        gCrvTest.isPlanar = true;
        gCrvTest.isCircular = false;
        gCrvTest.isLine = false;
        gCrvTest.isDegenerate = false;
        gCrvTest.p = Point.valueOf(METER, 0.8163312552679614, 0.14070505144444262);
        gCrvTest.pu = Vector.valueOf(METER, 1.381875231791502, -2.791859338311323);
        gCrvTest.puu = Vector.valueOf(METER, 1.2648516559099208, -0.6841004122947583);
        gCrvTest.voc = Parameter.valueOf(3.8318979168872107, METER.inverse());
        TestContext.test(gCrvTest);

        //  Curve Arc Length test.
        TestContext.test(new ArcLengthTest(curve, Parameter.valueOf(2.75309413832606, METER), typeStr));

    }

    public static class GenericSingleCurveTests extends TestCase {

        private final Curve curve;
        private final String msg;
        private boolean _planar, _isLine, _isCircular, _deg;
        private Point _p;
        private List<Vector<Length>> _ders;
        Parameter _voc;

        public boolean isPlanar, isLine, isCircular, isDegenerate;
        public Vector<Length> pu, puu;
        public Point p;
        public Parameter voc;

        public GenericSingleCurveTests(Curve crv, String message) {
            curve = crv;
            msg = message;
        }

        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append(msg).append(": Generic");
        }

        @Override
        public void execute() {
            _planar = curve.isPlanar(TOL);
            _isLine = curve.isLine(TOL);
            _isCircular = curve.isCircular(TOL);
            _deg = curve.isDegenerate(TOL);
            _p = curve.getRealPoint(0.5);
            _ders = curve.getSDerivatives(0.5, 2);
            _voc = curve.getVariationOfCurvature(0.5);
        }

        @Override
        public void validate() {
            TestContext.assertEquals("curve.isPlanar()", isPlanar, _planar);
            TestContext.assertEquals("curve.isLine()", isLine, _isLine);
            TestContext.assertEquals("curve.isCircular()", isCircular, _isCircular);
            TestContext.assertEquals("curve.isDegenerate()", isDegenerate, _deg);

            TestContext.assertEquals("curve.getRealPoint(0.5)", true, p.isApproxEqual(_p));

            Vector<Length> pv = Vector.valueOf(p);
            TestContext.assertEquals("curve.getSDerivatives(0.5,2).get(0)", true, pv.isApproxEqual(_ders.get(0)));
            TestContext.assertEquals("curve.getSDerivatives(0.5,2).get(1)", true, pu.isApproxEqual(_ders.get(1)));
            TestContext.assertEquals("curve.getSDerivatives(0.5,2).get(2)", true, puu.isApproxEqual(_ders.get(2)));
            Parameter vocTol = Parameter.valueOf(Parameter.SQRT_EPS, voc.getUnit());
            TestContext.assertEquals("curve.getVariationOfCurvature(0.5)", true, voc.isApproxEqual(_voc, vocTol));
        }
    }

    public static class PointCurveClosestTest extends TestCase {

        private final Curve curve;
        private final GeomPoint pnt;
        private final String msg;
        private final SubrangePoint expectedSP;
        private SubrangePoint _pnt;

        public PointCurveClosestTest(Curve crv, GeomPoint point, SubrangePoint expInt, String message) {
            curve = crv;
            pnt = point;
            msg = message;
            expectedSP = expInt;
        }

        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append(msg).append(": Closest Point");
        }

        @Override
        public void execute() {
            _pnt = curve.getClosest(pnt, 1e-6);
        }

        @Override
        public void validate() {
            //  Round the parametric positions off to the significant digits used in finding the closest point.
            double s = _pnt.getParPosition().getValue(0);
            int place = (int)(log10(s)) - 6;
            s = MathTools.roundToPlace(s, place);

            double expS = expectedSP.getParPosition().getValue(0);
            place = (int)(log10(expS)) - 6;
            expS = MathTools.roundToPlace(expS, place);

            TestContext.assertEquals("curve.getClosest(pnt, 1e-6)", true, MathTools.isApproxEqual(s, expS));
        }
    }

    public static class CCIntersectionTest extends TestCase {

        private final Curve curve1;
        private final Curve curve2;
        private final String msg;
        private final SubrangePoint expectedSP;
        private SubrangePoint _intPnt;

        public CCIntersectionTest(Curve crv1, Curve crv2, SubrangePoint expInt, String message) {
            curve1 = crv1;
            curve2 = crv2;
            msg = message;
            expectedSP = expInt;
        }

        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append(msg).append(": Intersect");
        }

        @Override
        public void execute() {
            _intPnt = ((PointString<SubrangePoint>)curve1.intersect(curve2, TOL).getFirst()).getFirst();
        }

        @Override
        public void validate() {
            TestContext.assertEquals("curve1.intersect(curve2)", true, expectedSP.isApproxEqual(_intPnt, TOL));
        }
    }

    public static class CPlaneIntersectionTest extends TestCase {

        private final Curve curve;
        private final GeomPlane plane;
        private final String msg;
        private final SubrangePoint expectedSP;
        private SubrangePoint _intPnt;

        public CPlaneIntersectionTest(Curve crv, GeomPlane pln, SubrangePoint expInt, String message) {
            curve = crv;
            plane = pln;
            msg = message;
            expectedSP = expInt;
        }

        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append(msg).append("-Plane: Intersect");
        }

        @Override
        public void execute() {
            PointString<SubrangePoint> str = curve.intersect(plane, 1e-6);
            _intPnt = str.getFirst();
        }

        @Override
        public void validate() {
            //  Round the parametric positions off to the significant digits used in finding the closest point.
            double s = _intPnt.getParPosition().getValue(0);
            int place = (int)(log10(s)) - 6;
            s = MathTools.roundToPlace(s, place);

            double expS = expectedSP.getParPosition().getValue(0);
            place = (int)(log10(expS)) - 6;
            expS = MathTools.roundToPlace(expS, place);

            TestContext.assertEquals("curve.intersect(plane, 1e-6)", true, MathTools.isApproxEqual(s, expS));
        }
    }

    public static class ArcLengthTest extends TestCase {

        private final Curve curve;
        private final Parameter<Length> expectedL;
        private final String msg;
        private Parameter<Length> _length;

        public ArcLengthTest(Curve crv, Parameter<Length> expL, String message) {
            curve = crv;
            msg = message;
            expectedL = expL;
        }

        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append(msg).append(": Arc Length");
        }

        @Override
        public void execute() {
            _length = curve.getArcLength(1e-6);
        }

        @Override
        public void validate() {
            //  Round the parametric positions off to the significant digits used in finding the closest point.            
            Parameter<Length> tol = expectedL.times(1e-6);

            TestContext.assertEquals("curve.getArcLength(1e-6)", true, _length.isApproxEqual(expectedL, tol));
        }
    }

    public static class EnclosedAreaTest extends TestCase {

        private final Curve curve;
        private final Parameter<Area> expectedA;
        private final String msg;
        private Parameter<Area> _area;

        public EnclosedAreaTest(Curve crv, Parameter<Area> expA, String message) {
            curve = crv;
            msg = message;
            expectedA = expA;
        }

        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append(msg).append(": Enclosed Area");
        }

        @Override
        public void execute() {
            _area = curve.getEnclosedArea(curve.getRealPoint(0), 1e-6);
        }

        @Override
        public void validate() {
            //  Round the parametric positions off to the significant digits used in finding the closest point.            
            Parameter<Area> tol = expectedA.times(1e-6).abs();

            TestContext.assertEquals("curve.getEnclosedArea(curve.getPoint(0),1e-6)",
                    true, _area.isApproxEqual(expectedA, tol));
        }
    }

}
