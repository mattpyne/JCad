/*
 *   TriangleTrans  -- A GeomTransform that has a Triangle for a child.
 *
 *   Copyright (C) 2015-2018, by Joseph A. Huwaldt
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Area;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.Unit;
import javax.swing.event.ChangeListener;
import javolution.context.ObjectFactory;
import javolution.lang.Immutable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A {@link GeomTransform} object that refers to a {@link Triangle} object
 * and masquerades as a Triangle object itself.
 *
 * <p> Modified by: Joseph A. Huwaldt</p>
 *
 * @author Joseph A. Huwaldt, Date: August 26, 2015
 * @version April 10, 2018
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class TriangleTrans extends GeomTriangle implements GeomTransform<GeomTriangle> {

    /**
     * The transformation represented by this transformation element.
     */
    private GTransform _TM;

    /**
     * The object that is the child of this transform.
     */
    private GeomTriangle _child;

    /**
     * Reference to a change listener for this object's child.
     */
    private ChangeListener _childChangeListener = new ForwardingChangeListener(this);

    /**
     * Returns a 3D {@link TriangleTrans} instance holding the specified
     * {@link GeomTriangle} and {@link GTransform}.
     *
     * @param child     The GeomTriangle that is the child of this transform element (may
     *                  not be <code>null</code>).
     * @param transform The transform held by this transform element (may not be
     *                  <code>null</code>).
     * @return the transform element having the specified values.
     */
    public static TriangleTrans newInstance(GeomTriangle child, GTransform transform) {
        requireNonNull(child, MessageFormat.format(RESOURCES.getString("paramNullErr"), "child"));
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));

        if (child.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), 
                            "triangle", String.valueOf(child.getPhyDimension())));
        
        TriangleTrans obj = FACTORY.object();
        obj._TM = transform;
        obj._child = child;
        child.copyState(obj);

        //  Listen for changes to the child object and pass them on.
        if (!(child instanceof Immutable))
            child.addChangeListener(obj._childChangeListener);

        return obj;
    }

    /**
     * Returns the transformation represented by this transformation element.
     *
     * @return The transformation from the child object represented by this
     * element.
     */
    @Override
    public GTransform getTransform() {
        return _TM;
    }

    /**
     * Returns the total transformation represented by an entire chain of
     * GeomTransform objects below this one.
     *
     * @return The total transformation represented by the entire chain of
     * objects below this one.
     */
    @Override
    public GTransform getTotalTransform() {
        return GeomUtil.getTotalTransform(this);
    }

    /**
     * Sets the transformation represented by this transformation element.
     *
     * @param transform The transform to set this transform element to (may not
     * be <code>null</code>).
     */
    @Override
    public void setTransform(GTransform transform) {
        requireNonNull(transform, MessageFormat.format(RESOURCES.getString("paramNullErr"), "transform"));
        _TM = transform;
        fireChangeEvent();
    }

    /**
     * Returns the child object transformed by this transform element.
     *
     * @return The child object transformed by this transform element.
     */
    @Override
    public GeomTriangle getChild() {
        return _child;
    }

    /**
     * Return a copy of the child object transformed by this transformation.
     *
     * @return A copy of the child object transformed by this transformation.
     */
    @Override
    public Triangle copyToReal() {
        Point p1 = getP1().copyToReal();
        Point p2 = getP2().copyToReal();
        Point p3 = getP3().copyToReal();
        Triangle T = Triangle.valueOf(p1, p2, p3);
        return copyState(T);    //  Copy over the super-class state for this object to the new one.
    }

    /**
     * Return the first vertex in this triangle.
     *
     * @return The first vertex in this triangle.
     */
    @Override
    public GeomPoint getP1() {
        return _child.getP1().getTransformed(_TM);
    }

    /**
     * Return the second vertex in this triangle.
     *
     * @return The second vertex in this triangle.
     */
    @Override
    public GeomPoint getP2() {
        return _child.getP2().getTransformed(_TM);
    }

    /**
     * Return the third and last vertex in this triangle.
     *
     * @return The third and last vertex in this triangle.
     */
    @Override
    public GeomPoint getP3() {
        return _child.getP3().getTransformed(_TM);
    }

    /**
     * Return the surface unit normal vector for this triangle.
     * If the triangle is degenerate (zero area), then the
     * normal vector will have zero length.
     * 
     * @return The surface normal vector for this triangle.
     */
    @Override
    public VectorTrans<Dimensionless> getNormal() {
        return _child.getNormal().getTransformed(_TM);
    }
    
    /**
     * Return the surface area of one side of this triangle.
     * The returned area is always positive, but can be zero.
     * 
     * @return The surface area of one side of this triangle.
     */
    @Override
    public Parameter<Area> getArea() {
        return _child.getArea();
    }
    
    /**
     * Returns the number of physical dimensions of the geometry element.
     */
    @Override
    public int getPhyDimension() {
        return _child.getPhyDimension();
    }

    /**
     * Return a new triangle that is identical to this one, but with the order
     * of the points (and the surface normal direction) reversed.
     *
     * @return A new Triangle that is identical to this one, but with the order
     * of the points reversed.
     */
    @Override
    public TriangleTrans reverse() {
        return TriangleTrans.newInstance(_child.reverse(), _TM);
    }

    /**
     * Return the coordinate point representing the minimum bounding box corner
     * of this geometry element (e.g.: min X, min Y, min Z).
     *
     * @return The minimum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMin() {
        Triangle trans = copyToReal();
        Point minPoint = trans.getBoundsMin();
        return minPoint;
    }

    /**
     * Return the coordinate point representing the maximum bounding box corner
     * (e.g.: max X, max Y, max Z).
     *
     * @return The maximum bounding box coordinate for this geometry element.
     * @throws IndexOutOfBoundsException if this list contains no elements.
     */
    @Override
    public Point getBoundsMax() {
        Triangle trans = copyToReal();
        Point maxPoint = trans.getBoundsMax();
        return maxPoint;
    }

    /**
     * Returns transformed version of this element. The returned object
     * implements {@link GeomTransform} and contains this element as a child.
     *
     * @param transform The transformation to apply to this geometry. May not be null.
     * @return A new line segment that is identical to this one with the
     * specified transformation applied.
     * @throws DimensionException if this point is not 3D.
     */
    @Override
    public TriangleTrans getTransformed(GTransform transform) {
        return TriangleTrans.newInstance(this, requireNonNull(transform));
    }

    /**
     * Returns the unit in which this triangle is stated.
     */
    @Override
    public Unit<Length> getUnit() {
        return _child.getUnit();
    }

    /**
     * Returns the equivalent to this element but stated in the specified unit.
     *
     * @param unit the length unit of the element to be returned. May not be null.
     * @return an equivalent to this element but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public TriangleTrans to(Unit<Length> unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;

        return TriangleTrans.newInstance(_child.to(unit), _TM);
    }

    /**
     * Return a copy of this Triangle converted to the specified number of
     * physical dimensions. If the number of dimensions is greater than this
     * element, then zeros are added to the additional dimensions. If the number
     * of dimensions is less than this element, then the extra dimensions are
     * simply dropped (truncated). If the new dimensions are the same as the
     * dimension of this element, then this element is simply returned.
     *
     * @param newDim The dimension of the Triangle to return.
     * @return This Triangle converted to the new dimensions.
     * @throws IllegalArgumentException if the new dimension is anything other
     * than 3.
     */
    @Override
    public TriangleTrans toDimension(int newDim) {
        if (newDim == 3)
            return this;

        throw new IllegalArgumentException(
                MessageFormat.format(RESOURCES.getString("dimensionNot3_2"), this.getClass().getName()));
    }

    /**
     * Returns a copy of this TriangleTrans instance allocated by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this point.
     */
    @Override
    public TriangleTrans copy() {
        return TriangleTrans.copyOf(this);
    }

    /**
     * Compares this TriangleTrans against the specified object for strict
     * equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this transform is identical to that
     * transform; <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        TriangleTrans that = (TriangleTrans)obj;
        return this._TM.equals(that._TM)
                && this._child.equals(that._child)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_TM, _child);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<TriangleTrans> XML = new XMLFormat<TriangleTrans>(TriangleTrans.class) {

        @Override
        public TriangleTrans newInstance(Class<TriangleTrans> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, TriangleTrans obj) throws XMLStreamException {
            GeomTriangle.XML.read(xml, obj);     // Call parent read.

            obj._TM = xml.getNext();
            GeomTriangle child = xml.getNext();
            obj._child = xml.getNext();

            //  Listen for changes to the child object and pass them on.
            if (!(child instanceof Immutable))
                child.addChangeListener(obj._childChangeListener);
        }

        @Override
        public void write(TriangleTrans obj, OutputElement xml) throws XMLStreamException {
            GeomTriangle.XML.write(obj, xml);    // Call parent write.

            xml.add(obj._TM);
            xml.add(obj._child);

        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<TriangleTrans> FACTORY = new ObjectFactory<TriangleTrans>() {
        @Override
        protected TriangleTrans create() {
            return new TriangleTrans();
        }

        @Override
        protected void cleanup(TriangleTrans obj) {
            obj.reset();
            obj._TM = null;
            if (!(obj._child instanceof Immutable))
                obj._child.removeChangeListener(obj._childChangeListener);
            obj._child = null;
        }
    };

    /**
     * Recycles a <code>TriangleTrans</code> instance immediately (on the stack
     * when executing in a StackContext).
     *
     * @param instance The instance to recycle immediately.
     */
    public static void recycle(TriangleTrans instance) {
        FACTORY.recycle(instance);
    }

    @SuppressWarnings("unchecked")
    private static TriangleTrans copyOf(TriangleTrans original) {
        TriangleTrans obj = FACTORY.object();
        obj._TM = original._TM.copy();
        obj._child = original._child.copy();
        original.copyState(obj);
        if (!(obj._child instanceof Immutable))
            obj._child.addChangeListener(obj._childChangeListener);
        return obj;
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    private TriangleTrans() { }

}
