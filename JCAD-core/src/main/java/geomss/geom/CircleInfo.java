/*
 *   CircleInfo  -- An object that packages together information about circles.
 *
 *   Copyright (C) 2009-2016, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;

/**
 * An simple container that packages together information about circles.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: May 29, 2009
 * @version September 13, 2016
 */
public class CircleInfo implements XMLSerializable {
    private static final long serialVersionUID = 1L;

    /**
     * THe radius of the circle in length units.
     */
    public Parameter<Length> radius;

    /**
     * The center or origin of the circle.
     */
    public GeomPoint center;

    /**
     * The direction of the circles X axis.
     */
    public GeomVector<Dimensionless> xhat;

    /**
     * The direction of the circles Y axis.
     */
    public GeomVector<Dimensionless> yhat;

    /**
     * The angle between the start and end of the circle (if represented as a circular
     * arc).
     */
    public Parameter<Angle> angle = Parameter.TWOPI_ANGLE;

    /**
     * Return a String representation of this object.
     *
     * @return A String representation of this object.
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder("radius = ");
        buff.append(radius);
        buff.append(", center = ");
        buff.append(center);
        buff.append(", xhat = ");
        buff.append(xhat);
        buff.append(", yhat = ");
        buff.append(yhat);
        buff.append(", angle = ");
        buff.append(angle.to(NonSI.DEGREE_ANGLE));
        return buff.toString();
    }

    /**
     * Holds the default XML representation for this object.
     */
    protected static final XMLFormat<CircleInfo> XML = new XMLFormat<CircleInfo>(CircleInfo.class) {

        @Override
        public CircleInfo newInstance(Class<CircleInfo> cls, InputElement xml) throws XMLStreamException {
            return new CircleInfo();
        }

        @Override
        public void read(InputElement xml, CircleInfo obj) throws XMLStreamException {

            //  Read in the parameters of the circle.
            obj.radius = xml.get("Radius");
            obj.center = xml.get("Center");
            obj.xhat = xml.get("XHat");
            obj.yhat = xml.get("YHat");
            obj.angle = xml.get("Angle");
            
        }

        @Override
        public void write(CircleInfo obj, OutputElement xml) throws XMLStreamException {
            //  Write out the parameters of the circle.
            xml.add(obj.radius, "Radius");
            xml.add(obj.center, "Center");
            xml.add(obj.xhat, "XHat");
            xml.add(obj.yhat, "YHat");
            xml.add(obj.angle, "Angle");
        }
    };
}
