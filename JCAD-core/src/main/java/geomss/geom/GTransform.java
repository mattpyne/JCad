/**
 * GTransform -- A general transformation matrix.
 *
 * Copyright (C) 2009-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ImmortalContext;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import static javolution.lang.MathLib.abs;
import static javolution.lang.MathLib.cos;
import static javolution.lang.MathLib.sin;
import static javolution.lang.MathLib.sqrt;
import javolution.lang.ValueType;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Matrix;
import org.jscience.mathematics.vector.Float64Vector;
import org.jscience.mathematics.vector.Matrix;

/**
 * A general 4x4 transformation matrix that transforms {@link GeomPoint} objects.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: April 29, 2009
 * @version November 25, 2015
 */
@SuppressWarnings("serial")
public class GTransform implements ValueType, Cloneable, XMLSerializable {

    /**
     * The resource bundle for this package.
     */
    private static final ResourceBundle RESOURCES = AbstractGeomElement.RESOURCES;

    /**
     * Constant used to identify the X coordinate in a vector and the 1st row/column in
     * this matrix.
     */
    private static final int X = 0;

    /**
     * Constant used to identify the Y coordinate in a vector and the 2nd row/column in
     * this matrix.
     */
    private static final int Y = 1;

    /**
     * Constant used to identify the Z coordinate in a vector and the 3rd row/column in
     * this matrix.
     */
    private static final int Z = 2;

    /**
     * Constant used to identify the 4th row/column in this matrix.
     */
    private static final int W = 3;

    /**
     * The universal translation units used. The translation elements of a GTransform
     * are always stated in meters.
     */
    private static final Unit<Length> METER = SI.METER;

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private GTransform() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<GTransform> FACTORY = new ObjectFactory<GTransform>() {
        @Override
        protected GTransform create() {
            return new GTransform();
        }
    };

    @SuppressWarnings("unchecked")
    private static GTransform copyOf(GTransform original) {
        GTransform M = FACTORY.object();
        M._data = original._data.copy();
        return M;
    }

    @SuppressWarnings("unchecked")
    private static GTransform newInstance(Float64Matrix data) {
        GTransform M = FACTORY.object();
        M._data = data;
        return M;
    }

    //  The bottom row of many new matricies.
    private static final Float64Vector BOTTOM_ROW;

    /**
     * A matrix containing all zero elements.
     */
    public static final GTransform ZERO;

    /**
     * The identity matrix containing ones along the diagonal.
     */
    public static final GTransform IDENTITY;

    static {
        // Forces constants to ImmortalMemory.
        ImmortalContext.enter();
        try {
            BOTTOM_ROW = Float64Vector.valueOf(0, 0, 0, 1);
            ZERO = GTransform.valueOf(0, 0, 0, 0);
            IDENTITY = GTransform.valueOf(1, 1, 1, 1);
        } finally {
            ImmortalContext.exit();
        }
    }

    /**
     * The elements stored in this matrix. Serialization is handled by this class since
     * Float64Matrix is not Serializable.
     */
    private transient Float64Matrix _data;

    /**
     * Returns a transformation matrix holding the specified row vectors. Note: any
     * transformations in the 4th column of the matrix must have units of meters!
     *
     * @param row0 the 1st row vector. May not be null.
     * @param row1 the 2nd row vector. May not be null.
     * @param row2 the 3rd row vector. May not be null.
     * @param row3 the 4th row vector. May not be null.
     * @return the transformation matrix having the specified rows.
     * @throws DimensionException if the any of the rows do not have a dimension of 4.
     */
    public static GTransform valueOf(Float64Vector row0, Float64Vector row1, Float64Vector row2, Float64Vector row3) {
        if (row0.getDimension() != 4 || row1.getDimension() != 4
                || row2.getDimension() != 4 || row3.getDimension() != 4)
            throw new DimensionException(RESOURCES.getString("rowsMustHave4Elements"));

        GTransform M = GTransform.newInstance(Float64Matrix.valueOf(row0, row1, row2, row3));

        return M;
    }

    /**
     * Returns a transformation matrix holding the row vectors from the specified list.
     * Note: any transformations in the 4th column of the matrix must have units of
     * meters!
     *
     * @param rows The list of 4 row vectors. May not be null.
     * @return the transformation matrix having the specified rows.
     * @throws DimensionException if the rows do not have a dimension of 4.
     */
    public static GTransform valueOf(List<Float64Vector> rows) {
        if (rows.size() != 4)
            throw new DimensionException(RESOURCES.getString("fourVectorsReq"));

        return GTransform.newInstance(Float64Matrix.valueOf(rows));
    }

    /**
     * Returns a transformation matrix holding the specified diagonal vector with zeros
     * placed in all the other elements.
     *
     * @param diagX the 1st element of the diagonal vector for the matrix.
     * @param diagY the 2nd element of the diagonal vector for the matrix.
     * @param diagZ the 3rd element of the diagonal vector for the matrix.
     * @param diagW the 4th element of the diagonal vector for the matrix.
     * @return the transformation matrix having the specified diagonal.
     */
    public static GTransform valueOf(double diagX, double diagY, double diagZ, double diagW) {

        Float64Vector row0 = Float64Vector.valueOf(diagX, 0, 0, 0);
        Float64Vector row1 = Float64Vector.valueOf(0, diagY, 0, 0);
        Float64Vector row2 = Float64Vector.valueOf(0, 0, diagZ, 0);
        Float64Vector row3 = Float64Vector.valueOf(0, 0, 0, diagW);

        GTransform M = GTransform.newInstance(Float64Matrix.valueOf(row0, row1, row2, row3));

        return M;
    }

    /**
     * Returns a {@link GTransform} instance containing a copy of the specified matrix of
     * Float64 values. The matrix must have dimensions of either 3x3 for a combined scale
     * and direction cosine matrix or 4x4 for a general transformation matrix. If a 3x3
     * matrix is input, then the upper-left 3x3 portion of the output matrix will be set
     * to those values and the other elements will be set as if it were an identity matrix
     * (i.e., affine matrix with no translational component). If a 4x4 matrix is input,
     * then the 4th column (the translation) must be in units of meters!
     *
     * @param matrix the matrix of Float64 values to convert (must have dimension of 3x3
     *               or 4x4). May not be null.
     * @return the transformation matrix having the specified elements.
     */
    public static GTransform valueOf(Matrix<Float64> matrix) {
        GTransform M = null;

        if (matrix.getNumberOfColumns() == 3 && matrix.getNumberOfRows() == 3) {
            FastTable<Float64Vector> rowList = FastTable.newInstance();

            for (int i = 0; i < 3; ++i) {
                Float64Vector row = Float64Vector.valueOf(matrix.get(i, X).doubleValue(),
                        matrix.get(i, Y).doubleValue(), matrix.get(i, Z).doubleValue(), 0);
                rowList.add(row);
            }
            rowList.add(BOTTOM_ROW);

            M = GTransform.valueOf(rowList);

        } else if (matrix.getNumberOfColumns() == 4 && matrix.getNumberOfRows() == 4) {
            M = GTransform.newInstance(Float64Matrix.valueOf(matrix));

        } else
            throw new DimensionException(RESOURCES.getString("3x3or4x4Req"));

        return M;
    }

    /**
     * Returns a {@link GTransform} instance containing the specified direction cosine
     * rotation matrix, uniform scale factor, and translation vector.
     *
     * @param dcm   The direction cosine matrix to convert (must have dimension of 3x3).
     *              May not be null.
     * @param scale The uniform scale factor to apply to each axis.
     * @param trans The amount to translate in each axis X, Y & Z.
     * @return the transformation matrix having the specified elements.
     */
    public static GTransform valueOf(Matrix<Float64> dcm, double scale, Vector<Length> trans) {

        if (dcm.getNumberOfColumns() != 3 && dcm.getNumberOfRows() != 3)
            throw new DimensionException(RESOURCES.getString("3x3DCMReq"));

        //  Convert translation vector to meters.
        Float64Vector T = trans.to(METER).toFloat64Vector();

        //  Create a list of rows with the input elements.
        FastTable<Float64Vector> rowList = FastTable.newInstance();
        for (int i = 0; i < 3; ++i) {
            double iX = dcm.get(i, X).doubleValue() * scale;
            double iY = dcm.get(i, Y).doubleValue() * scale;
            double iZ = dcm.get(i, Z).doubleValue() * scale;
            Float64Vector row = Float64Vector.valueOf(iX, iY, iZ, T.getValue(i));
            rowList.add(row);
        }
        rowList.add(BOTTOM_ROW);

        //  Create the transformation matrix.
        GTransform M = GTransform.valueOf(rowList);

        return M;
    }

    /**
     * Returns a {@link GTransform} instance containing a copy of the specified rotation
     * transformation. The upper-left 3x3 portion of the output matrix will be set to DCM
     * values from the rotation and the other elements will be set as if it were an
     * identity matrix (i.e., affine matrix with no translational component).
     *
     * @param rotation the Rotation transformation to convert. May not be null.
     * @return the general transformation matrix having the specified rotational elements.
     */
    public static GTransform valueOf(Rotation rotation) {
        return valueOf(rotation.toDCM().toFloat64Matrix());
    }

    /**
     * Returns a {@link GTransform} instance containing the specified rotation
     * transformation, uniform scale factor, and translation vector.
     *
     * @param rotation The rotation transformation to convert. May not be null.
     * @param scale    The uniform scale factor to apply to each axis.
     * @param trans    The amount to translate in each axis X, Y & Z. May not be null.
     * @return the transformation matrix having the specified elements.
     */
    public static GTransform valueOf(Rotation rotation, double scale, Vector<Length> trans) {
        return valueOf(rotation.toDCM().toFloat64Matrix(), scale, requireNonNull(trans));
    }

    /**
     * Returns a {@link GTransform} instance representing the transformation from one axis
     * direction directly to another. Input vectors must be 3D.
     *
     * @param srcAxis  A vector representing the 1st or starting axis. May not be null.
     * @param destAxis A vector representing the 2nd or ending axis. May not be null.
     * @return The transformation matrix representing the rotation from the 1st (source)
     *         axis directly to the 2nd (destination) axis.
     */
    public static GTransform valueOf(GeomVector srcAxis, GeomVector destAxis) {
        GeomVector axis = srcAxis.cross(requireNonNull(destAxis));  //  Find a perpendicular axis to rotate about.
        Parameter length = axis.mag();
        Parameter dot = srcAxis.dot(destAxis);

        if (!length.isApproxZero()) {
            Vector3D v3d = axis.toUnitVector().toParameterVector().toVector3D();
            Parameter<Angle> angle = Parameter.atan2(length, dot);

            Rotation R = AxisAngle.valueOf(v3d, angle);
            return GTransform.valueOf(R);

        } else if (dot.isGreaterThan(Parameter.ZERO)) {
            // Nearly positively aligned; skip rotation, or compute
            // axis and angle using other means.
            return GTransform.IDENTITY;
        }

        // Nearly negatively aligned; axis is any vector perpendicular
        // to either vector, and angle is 180 degrees
        axis = GeomUtil.calcYHat(srcAxis);
        Vector3D v3d = axis.toParameterVector().toVector3D();
        Parameter<Angle> angle = Parameter.PI_ANGLE;

        Rotation R = AxisAngle.valueOf(v3d, angle);
        return GTransform.valueOf(R);
    }

    /**
     * Returns a transformation matrix representing a scale matrix with a uniform scale
     * factor applied to each dimension.
     *
     * @param scale The scale factor to create a scale matrix for.
     * @return the transformation matrix representing the specified scale.
     */
    public static GTransform newScale(double scale) {
        return valueOf(scale, scale, scale, 1);
    }

    /**
     * Returns a transformation matrix representing a translation matrix.
     *
     * @param transX The amount to translate, in meters, in the X axis direction.
     * @param transY The amount to translate, in meters, in the Y axis direction.
     * @param transZ The amount to translate, in meters, in the Z axis direction.
     * @return the transformation matrix representing the specified translation.
     * @throws DimensionException if the input vector does not have 3 elements.
     */
    public static GTransform newTranslation(double transX, double transY, double transZ) {

        Float64Vector row0 = Float64Vector.valueOf(1, 0, 0, transX);
        Float64Vector row1 = Float64Vector.valueOf(0, 1, 0, transY);
        Float64Vector row2 = Float64Vector.valueOf(0, 0, 1, transZ);

        GTransform T = GTransform.valueOf(row0, row1, row2, BOTTOM_ROW);

        return T;
    }

    /**
     * Returns a transformation matrix representing a translation matrix.
     *
     * @param transX The amount to translate in the X axis direction. May not be null.
     * @param transY The amount to translate in the Y axis direction. May not be null.
     * @param transZ The amount to translate in the Z axis direction. May not be null.
     * @return the transformation matrix representing the specified translation.
     * @throws DimensionException if the input vector does not have 3 elements.
     */
    public static GTransform newTranslation(Parameter<Length> transX,
            Parameter<Length> transY, Parameter<Length> transZ) {

        GTransform T = GTransform.newTranslation(transX.getValue(METER),
                transY.getValue(METER), transZ.getValue(METER));

        return T;
    }

    /**
     * Returns a transformation matrix representing a translation matrix.
     *
     * @param vector The amount to translate in each axis X, Y & Z. May not be null.
     * @return the transformation matrix representing the specified translation.
     */
    public static GTransform newTranslation(GeomVector<Length> vector) {

        //  Convert the translation to meters and create a new
        //  transformation matrix.
        GTransform T = GTransform.newTranslation(
                vector.getValue(X, METER), vector.getValue(Y, METER), vector.getValue(Z, METER));

        return T;
    }

    /**
     * Returns a transformation matrix representing a translation matrix.
     *
     * @param trans The amount to translate, in meters, in each axis X, Y & Z. May not be null.
     * @return the transformation matrix representing the specified translation.
     * @throws DimensionException if the input vector does not have 3 elements.
     */
    public static GTransform newTranslation(Float64Vector trans) {

        if (trans.getDimension() != 3)
            throw new DimensionException(RESOURCES.getString("3elementVectorReq"));

        return newTranslation(trans.getValue(X), trans.getValue(Y), trans.getValue(Z));
    }

    /**
     * Return a transformation matrix representing a counter-clockwise or right-handed
     * rotation about the X axis (when looking down the X-axis).
     *
     * @param angle The angle to rotate about the X axis. May not be null.
     * @return A transformation matrix for a counter-clockwise rotation about the X axis.
     */
    public static GTransform newRotationX(Parameter<Angle> angle) {
        double angleR = angle.getValue(SI.RADIAN);
        double sina = sin(angleR);
        double cosa = cos(angleR);

        Float64Vector row0 = Float64Vector.valueOf(    1,    0,    0,    0);
        Float64Vector row1 = Float64Vector.valueOf(    0, cosa,-sina,    0);
        Float64Vector row2 = Float64Vector.valueOf(    0, sina, cosa,    0);
//      Float64Vector row3 = Float64Vector.valueOf(    0,    0,    0,    1);

        GTransform T = GTransform.valueOf(row0, row1, row2, BOTTOM_ROW);

        return T;
    }

    /**
     * Return a transformation matrix representing a counter-clockwise or right-handed
     * rotation about the Y axis (when looking down the Y-axis).
     *
     * @param angle The angle to rotate about the Y axis. May not be null.
     * @return A transformation matrix for a counter-clockwise rotation about the Y axis.
     */
    public static GTransform newRotationY(Parameter<Angle> angle) {
        double angleR = angle.getValue(SI.RADIAN);
        double sina = sin(angleR);
        double cosa = cos(angleR);

        Float64Vector row0 = Float64Vector.valueOf( cosa,    0, sina,    0);
        Float64Vector row1 = Float64Vector.valueOf(    0,    1,    0,    0);
        Float64Vector row2 = Float64Vector.valueOf(-sina,    0, cosa,    0);
//      Float64Vector row3 = Float64Vector.valueOf(    0,    0,    0,    1);

        GTransform T = GTransform.valueOf(row0, row1, row2, BOTTOM_ROW);

        return T;
    }

    /**
     * Return a transformation matrix representing a counter-clockwise or right-handed
     * rotation about the Z axis (when looking down the Z-axis).
     *
     * @param angle The angle to rotate about the Z axis. May not be null.
     * @return A transformation matrix for a counter-clockwise rotation about the Z axis.
     */
    public static GTransform newRotationZ(Parameter<Angle> angle) {
        double angleR = angle.getValue(SI.RADIAN);
        double sina = sin(angleR);
        double cosa = cos(angleR);

        Float64Vector row0 = Float64Vector.valueOf(cosa, -sina,    0,    0);
        Float64Vector row1 = Float64Vector.valueOf(sina,  cosa,    0,    0);
        Float64Vector row2 = Float64Vector.valueOf(0,  0,    1,    0);
//      Float64Vector row3 = Float64Vector.valueOf(    0,    0,    0,    1);

        GTransform T = GTransform.valueOf(row0, row1, row2, BOTTOM_ROW);

        return T;
    }

    /**
     * Returns the number of rows for this matrix. This implementation always returns 4.
     *
     * @return The number of rows in this matrix (always 4).
     */
    public int getNumberOfRows() {
        return 4;
    }

    /**
     * Returns the number of columns for this matrix. This implementation always returns 4.
     *
     * @return The number of columns in this matrix (always 4).
     */
    public int getNumberOfColumns() {
        return 4;
    }

    /**
     * Returns a single element from this matrix.
     *
     * @param i the row index (range [0..3[).
     * @param j the column index (range [0..3[).
     * @return the matrix element at [i,j].
     */
    public Float64 get(int i, int j) {
        return _data.get(i, j);
    }

    /**
     * Returns a single element from this matrix as a <code>double</code>.
     * <p>
     * This is a convenience method for: <code>this.get(i,j).doubleValue()</code>
     * </p>
     *
     * @param i the row index (range [0..3[).
     * @param j the column index (range [0..3[).
     * @return the matrix element at [i,j].
     */
    public double getValue(int i, int j) {
        return _data.get(i, j).doubleValue();
    }

    /**
     * Return the transformation matrix that represents this GTransform object as a
     * Float64Matrix. The translation column values have units of METER.
     * 
     * @return The transformation matrix that represents this GTransform object.
     */
    public Float64Matrix getFloat64Matrix() {
        return _data;
    }

    /**
     * Return the transformation matrix that represents this GTranfrom object as a 2D Java
     * matrix. The translation column values have units of METER.
     * 
     * @return The transformation matrix that represents this GTranfrom object.
     */
    public double[][] getMatrix() {
        StackContext.enter();
        try {
            double[][] mat = new double[4][4];
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    mat[i][j] = _data.get(i, j).doubleValue();
                }
            }
            return mat;
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the row identified by the specified index in this matrix.
     *
     * @param i the row index (range [0..3[).
     * @return The specified row in this matrix as a Float64Vector.
     */
    public Float64Vector getRow(int i) {
        return _data.getRow(i);
    }

    /**
     * Returns the column identified by the specified index in this matrix.
     *
     * @param j the column index (range [0..3[).
     * @return The specified column in this matrix as a Float64Vector.
     */
    public Float64Vector getColumn(int j) {
        return _data.getColumn(j);
    }

    /**
     * Returns the diagonal vector.
     *
     * @return the vector holding the diagonal elements.
     */
    public Float64Vector getDiagonal() {
        return _data.getDiagonal();
    }

    /**
     * Transform the input point by multiplying it times this general transformation
     * matrix.
     *
     * @param p The point to be transformed. May not be null.
     * @return <code>this · p</code>
     * @throws DimensionException if p.getPhyDimension() != 3
     */
    public Point transform(GeomPoint p) {
        if (p.getPhyDimension() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("dimensionNot3"), "point's", p.getPhyDimension()));

        Unit<Length> pUnit = p.getUnit();
        Float64Vector V;
        StackContext.enter();
        try {
            //  Convert the point to units of meters (to be consistant with any translation in
            //  this transformation matrix).

            //  Convert the point to a 4 element vector.
            V = Float64Vector.valueOf(p.getValue(X, METER), p.getValue(Y, METER), p.getValue(Z, METER), 1);

            //  Multiply times the general transformation matrix.
            V = _data.times(V);

            V = StackContext.outerCopy(V);

        } finally {
            StackContext.exit();
        }

        //  Create output point.
        Point Pout = Point.valueOf(V.getValue(X), V.getValue(Y), V.getValue(Z), METER);
        return Pout.to(pUnit);
    }

    /**
     * Returns the product of this matrix with the one specified.
     *
     * @param that the matrix multiplier. May not be null.
     * @return <code>this · that</code>
     */
    public GTransform times(GTransform that) {

        GTransform M = GTransform.newInstance(this._data.times(that._data));

        return M;
    }

    /**
     * Returns the inverse of this matrix. If the matrix is singular, the
     * {@link #determinant()} will be zero (or nearly zero).
     *
     * @return <code>1 / this</code>
     * @see #determinant
     */
    public GTransform inverse() {

        GTransform M = GTransform.newInstance(_data.inverse());

        return M;
    }

    /**
     * Returns the determinant of this matrix.
     *
     * @return this matrix determinant.
     */
    public Float64 determinant() {
        return _data.determinant();
    }

    /**
     * Returns the transpose of this matrix.
     *
     * @return <code>A'</code>
     */
    public GTransform transpose() {

        GTransform M = GTransform.newInstance(_data.transpose());

        return M;
    }

    /**
     * Returns the vectorization of this matrix. The vectorization of a matrix is the
     * column vector obtained by stacking the columns of the matrix on top of one another.
     *
     * @return the vectorization of this matrix.
     */
    public Float64Vector vectorization() {
        return _data.vectorization();
    }

    /**
     * Returns a copy of this matrix allocated by the calling thread (possibly on the
     * stack).
     *
     * @return an identical and independent copy of this matrix.
     */
    @Override
    public GTransform copy() {
        return GTransform.copyOf(this);
    }

    /**
     * Returns a copy of this GTransform instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this matrix.
     * @throws java.lang.CloneNotSupportedException Never thrown.
     */
    @Override
    @SuppressWarnings("CloneDoesntCallSuperClone")
    public Object clone() throws CloneNotSupportedException {
        return copy();
    }

    /**
     * Returns the text representation of this matrix.
     *
     * @return the text representation of this matrix.
     */
    public Text toText() {
        final int m = this.getNumberOfRows();
        final int n = this.getNumberOfColumns();
        TextBuilder tmp = TextBuilder.newInstance();
        tmp.append('{');
        for (int i = 0; i < m; i++) {
            tmp.append('{');
            for (int j = 0; j < n; j++) {
                tmp.append(get(i, j));
                if (j != n - 1) {
                    tmp.append(", ");
                }
            }
            tmp.append("}");
            if (i != m - 1) {
                tmp.append(",\n");
            }
        }
        tmp.append("}");
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Returns a string representation of this matrix.
     */
    @Override
    public String toString() {
        return toText().toString();
    }

    /**
     * Compares this GTransform against the specified object for strict equality (same
     * values).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this transform is identical to that transform;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        GTransform that = (GTransform)obj;

        return this._data.equals(that._data);
    }

    /**
     * Returns the hash code for this GTransform object.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return Objects.hash(_data);
    }

    /**
     * Returns the uniform scale factor for this matrix. If the matrix has non-uniform
     * scale factors, than the maximum of the X, Y & Z scale factor is returned.
     *
     * @return the uniform or maximum scale factor.
     */
    public double getScale() {

        double[] tmp_rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //   new double[9];  // scratch matrix
        double[] tmp_scale = ArrayFactory.DOUBLES_FACTORY.array(3); //   new double[3];  // scratch matrix
        getScaleRotate(tmp_scale, tmp_rot);

        double scale = max3(tmp_scale);

        //  Recycle scratch arrays.
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp_rot);
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp_scale);

        return scale;
    }

    /**
     * Returns a 3 element vector containing the scale factors in each dimension X, Y & Z.
     *
     * @return the scale factors of this matrix
     */
    public Float64Vector getScaleVector() {

        double[] tmp_rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //   new double[9];  // scratch matrix
        double[] tmp_scale = ArrayFactory.DOUBLES_FACTORY.array(3); //   new double[3];  // scratch matrix
        getScaleRotate(tmp_scale, tmp_rot);

        //  Create the output vector (Note: tmp_scale may be longer than 3 elements).
        Float64Vector scale = Float64Vector.valueOf(tmp_scale[X], tmp_scale[Y], tmp_scale[Z]);

        //  Recycle scratch arrays.
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp_rot);
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp_scale);

        return scale;
    }

    /**
     * Returns a direction cosine matrix containing the rotational portion of this
     * transformation matrix.
     *
     * @return the direction cosine matrix
     */
    public DCMatrix getRotation() {

        double[] tmp_rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //   new double[9];  // scratch matrix
        double[] tmp_scale = ArrayFactory.DOUBLES_FACTORY.array(3); //   new double[3];  // scratch matrix
        getScaleRotate(tmp_scale, tmp_rot);

        //  Create the output vector (Note: tmp_rot may be longer than 9 elements).
        DCMatrix rot = DCMatrix.valueOf(tmp_rot[0], tmp_rot[1], tmp_rot[2],
                tmp_rot[3], tmp_rot[4], tmp_rot[5],
                tmp_rot[6], tmp_rot[7], tmp_rot[8]);

        //  Recycle scratch arrays.
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp_rot);
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp_scale);

        return rot;
    }

    /**
     * Returns the upper 3x3 values of this matrix (which contains combined rotation and
     * scale information) and places them into the output matrix.
     *
     * @return A 3x3 matrix representing the combined rotation and scaling of this matrix.
     */
    public Matrix<Float64> getRotationScale() {
        DCMatrix M = DCMatrix.valueOf(getValue(X, X), getValue(X, Y), getValue(X, Z),
                getValue(Y, X), getValue(Y, Y), getValue(Y, Z),
                getValue(Z, X), getValue(Z, Y), getValue(Z, Z));
        return M.toFloat64Matrix();
    }

    /**
     * Returns the translational components of this transformation matrix.
     *
     * @return The translational components of this transformation matrix.
     */
    public Vector<Length> getTranslation() {
        return Vector.valueOf(METER, getValue(X, W), getValue(Y, W), getValue(Z, W));
    }

    /**
     * Return a new transformation matrix that is identical to this one, but with the
     * specified uniform scale factor applied.
     *
     * @param scale The scale factor to apply to this transformation.
     * @return A new GTransform that is identical to this one, but with the specified
     *         uniform scale factor applied.
     */
    public GTransform applyScale(double scale) {

        /* Sets the scale component of the current matrix by factoring
         out the current scale (by doing an SVD) from the rotational
         component and multiplying by the new scale.
         */

        StackContext.enter();
        try {
            double[] tmp_rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //   new double[9];  // scratch matrix
            double[] tmp_scale = ArrayFactory.DOUBLES_FACTORY.array(3); //   new double[3];  // scratch matrix
            getScaleRotate(tmp_scale, tmp_rot);

            Float64Vector row0 = Float64Vector.valueOf(tmp_rot[0] * scale, tmp_rot[1] * scale, tmp_rot[2] * scale, getValue(X, W));
            Float64Vector row1 = Float64Vector.valueOf(tmp_rot[3] * scale, tmp_rot[4] * scale, tmp_rot[5] * scale, getValue(Y, W));
            Float64Vector row2 = Float64Vector.valueOf(tmp_rot[6] * scale, tmp_rot[7] * scale, tmp_rot[8] * scale, getValue(Z, W));
            Float64Vector row3 = getRow(3);

            GTransform T = GTransform.newInstance(Float64Matrix.valueOf(row0, row1, row2, row3));

            return StackContext.outerCopy(T);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new transformation matrix that is identical to this one, but with the
     * specified set of three scale factors applied in each axis (X,Y,Z).
     *
     * @param scale The scale factor in each dimension (X, Y & Z) to apply to this
     *              transformation. May not be null.
     * @return A new GTransform that is identical to this one, but with the specified set
     *         of three scale factors applied.
     * @throws DimensionException if the input list of scale factors does not have 3
     * elements.
     */
    public GTransform applyScale(Float64Vector scale) {
        if (scale.getDimension() != 3)
            throw new DimensionException(RESOURCES.getString("3elementScaleFReq"));

        /* Sets the scale component of the current matrix by factoring
         out the current scale (by doing an SVD) from the rotational
         component and multiplying by the new scale.
         */

        StackContext.enter();
        try {
            double[] tmp_rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //   new double[9];  // scratch matrix
            double[] tmp_scale = ArrayFactory.DOUBLES_FACTORY.array(3); //   new double[3];  // scratch matrix
            getScaleRotate(tmp_scale, tmp_rot);

            FastTable<Float64Vector> rowList = FastTable.newInstance();
            for (int i = 0; i < 3; ++i) {
                double iX = getValue(i, X) * scale.getValue(X);
                double iY = getValue(i, Y) * scale.getValue(Y);
                double iZ = getValue(i, Z) * scale.getValue(Z);
                Float64Vector row = Float64Vector.valueOf(iX, iY, iZ, getValue(i, W));
                rowList.add(row);
            }
            rowList.add(getRow(3));

            GTransform T = GTransform.valueOf(rowList);

            return StackContext.outerCopy(T);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new transformation matrix that is identical to this one, but with the
     * rotational components replaced with those in the specified direction cosine matrix.
     * The effect of scale is factored out, then the new rotation components are placed in
     * the upper-left 3x3 portion of the matrix and then the scale is reapplied.
     *
     * @param m1 The direction cosine matrix to apply to this matrix. May not be null.
     * @return A new GTransform that is identical to this one, but with the rotational
     *         components replaced with those in the specified direction cosine matrix.
     * @throws DimensionException if matrix is not 3x3
     */
    public GTransform applyRotation(Matrix<Float64> m1) {

        if (m1.getNumberOfColumns() != 3 && m1.getNumberOfRows() != 3)
            throw new DimensionException(RESOURCES.getString("3x3DCMReq"));

        /* Sets the rotational component of the current matrix by factoring
         out the current scale (by doing an SVD) from the rotational
         component, replacing the upper-left 3x3 with the input matrix
         and then reapplying the old scale to the new rotational components.
         */

        StackContext.enter();
        try {
            double[] tmp_rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //   new double[9];  // scratch matrix
            double[] tmp_scale = ArrayFactory.DOUBLES_FACTORY.array(3); //   new double[3];  // scratch matrix
            getScaleRotate(tmp_scale, tmp_rot);

            FastTable<Float64Vector> rowList = FastTable.newInstance();
            for (int i = 0; i < 3; ++i) {
                double iX = m1.get(i, X).doubleValue() * tmp_scale[X];
                double iY = m1.get(i, Y).doubleValue() * tmp_scale[Y];
                double iZ = m1.get(i, Z).doubleValue() * tmp_scale[Z];
                Float64Vector row = Float64Vector.valueOf(iX, iY, iZ, getValue(i, W));
                rowList.add(row);
            }
            rowList.add(getRow(3));

            GTransform T = GTransform.valueOf(rowList);

            return StackContext.outerCopy(T);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new transformation matrix with the upper-left 3x3 portion of this matrix
     * (rotation and scaling) replaced by the specified values. The remaining elements of
     * the matrix are left unchanged.
     *
     * @param m1 The 3x3 matrix that will be the new upper left portion. May not be null.
     * @return A new GTransform with the upper-left 3x3 portion of this matrix replaced by
     *         the specified values.
     * @throws DimensionException if m1 is not 3x3
     */
    public GTransform applyRotationScale(Matrix<Float64> m1) {

        if (m1.getNumberOfColumns() != 3 && m1.getNumberOfRows() != 3)
            throw new DimensionException(RESOURCES.getString("3x3MatrixReq"));

        StackContext.enter();
        try {
            FastTable<Float64Vector> rowList = FastTable.newInstance();
            for (int i = 0; i < 3; ++i) {
                double iX = m1.get(i, X).doubleValue();
                double iY = m1.get(i, Y).doubleValue();
                double iZ = m1.get(i, Z).doubleValue();
                Float64Vector row = Float64Vector.valueOf(iX, iY, iZ, getValue(i, W));
                rowList.add(row);
            }
            rowList.add(getRow(3));

            GTransform T = GTransform.valueOf(rowList);

            return StackContext.outerCopy(T);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return a new transformation matrix that is identical to this one, but with the
     * translation components replaced by the specified values. The remaining elements of
     * the matrix are left unchanged.
     *
     * @param trans The 3 element translation offsets in (X, Y & Z). May not be null.
     * @return A new GTransform that is identical to this one, but with the translation
     *         components replaced by the specified values.
     */
    public GTransform applyTranslation(Vector<Length> trans) {

        //  Convert the translation to meters and then compute the new
        //  transformation matrix.
        GTransform T = this.applyTranslation(trans.to(METER).toFloat64Vector());

        return T;
    }

    /**
     * Return a new transformation matrix that is identical to this one, but with the
     * translation components replaced by the specified values. The remaining elements of
     * the matrix are left unchanged.
     *
     * @param trans The 3 element translation offsets in meters (X, Y & Z). May not be null.
     * @return A new GTransform that is identical to this one, but with the translation
     *         components replaced by the specified values.
     * @throws DimensionException if input vector does not have exactly 3 elements.
     */
    public GTransform applyTranslation(Float64Vector trans) {
        if (trans.getDimension() != 3)
            throw new DimensionException(RESOURCES.getString("3elementVectorReq"));

        StackContext.enter();
        try {
            FastTable<Float64Vector> rowList = FastTable.newInstance();
            for (int i = 0; i < 3; ++i) {
                double iX = getValue(i, X);
                double iY = getValue(i, Y);
                double iZ = getValue(i, Z);
                Float64Vector row = Float64Vector.valueOf(iX, iY, iZ, trans.getValue(i));
                rowList.add(row);
            }
            rowList.add(getRow(3));

            GTransform T = GTransform.valueOf(rowList);

            return StackContext.outerCopy(T);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * During serialization, this will write out the Float64Matrix as a simple series of
     * <code>double</code> values. This method is ONLY called by the Java Serialization
     * mechanism and should not otherwise be used.
     */
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {

        // Call the default write object method.
        out.defaultWriteObject();

        //  Write out the matrix values.
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j)
                out.writeDouble(_data.get(i, j).doubleValue());

    }

    /**
     * During de-serialization, this will handle the reconstruction of the Float64Matrix.
     * This method is ONLY called by the Java Serialization mechanism and should not
     * otherwise be used.
     */
    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {

        // Call the default read object method.
        in.defaultReadObject();

        //  Read in the matrix values.
        FastTable<Float64Vector> rows = FastTable.newInstance();
        for (int i = 0; i < 4; ++i) {
            double v1 = in.readDouble();
            double v2 = in.readDouble();
            double v3 = in.readDouble();
            double v4 = in.readDouble();
            Float64Vector row = Float64Vector.valueOf(v1, v2, v3, v4);
            rows.add(row);
        }

        _data = Float64Matrix.valueOf(rows);

    }

    /**
     * ******* The Following taken from javax.vecmath.Matrix3d with minor mods **********
     */
    private void getScaleRotate(double scales[], double rots[]) {
        double[] tmp = ArrayFactory.DOUBLES_FACTORY.array(9);       // scratch matrix

        tmp[0] = getValue(X, X);
        tmp[1] = getValue(X, Y);
        tmp[2] = getValue(X, Z);

        tmp[3] = getValue(Y, X);
        tmp[4] = getValue(Y, Y);
        tmp[5] = getValue(Y, Z);

        tmp[6] = getValue(Z, X);
        tmp[7] = getValue(Z, Y);
        tmp[8] = getValue(Z, Z);

        compute_svd(tmp, scales, rots);

        //  Recycle the scratch array.
        ArrayFactory.DOUBLES_FACTORY.recycle(tmp);
    }

    private static final double EPS = 1.110223024E-16;

    private static void compute_svd(double[] m, double[] outScale, double[] outRot) {
        double[] u1 = ArrayFactory.DOUBLES_FACTORY.array(9);        //  new double[9];
        double[] v1 = ArrayFactory.DOUBLES_FACTORY.array(9);        //  new double[9];
        double[] t1 = ArrayFactory.DOUBLES_FACTORY.array(9);        //  new double[9];
        double[] t2 = ArrayFactory.DOUBLES_FACTORY.array(9);        //  new double[9];

        double[] tmp = t1;
        double[] single_values = t2;

        double[] rot = ArrayFactory.DOUBLES_FACTORY.array(9);       //  new double[9];
        double[] e = ArrayFactory.DOUBLES_FACTORY.array(3);         //  new double[3];
        double[] scales = ArrayFactory.DOUBLES_FACTORY.array(3);    //  new double[3];

        try {
            System.arraycopy(m, 0, rot, 0, 9);

            // u1
            if (m[3] * m[3] < EPS) {
                u1[0] = 1.0;
                u1[1] = 0.0;
                u1[2] = 0.0;
                u1[3] = 0.0;
                u1[4] = 1.0;
                u1[5] = 0.0;
                u1[6] = 0.0;
                u1[7] = 0.0;
                u1[8] = 1.0;
            } else if (m[0] * m[0] < EPS) {
                tmp[0] = m[0];
                tmp[1] = m[1];
                tmp[2] = m[2];
                m[0] = m[3];
                m[1] = m[4];
                m[2] = m[5];

                m[3] = -tmp[0]; // zero
                m[4] = -tmp[1];
                m[5] = -tmp[2];

                u1[0] = 0.0;
                u1[1] = 1.0;
                u1[2] = 0.0;
                u1[3] = -1.0;
                u1[4] = 0.0;
                u1[5] = 0.0;
                u1[6] = 0.0;
                u1[7] = 0.0;
                u1[8] = 1.0;
            } else {
                double g = 1.0 / sqrt(m[0] * m[0] + m[3] * m[3]);
                double c1 = m[0] * g;
                double s1 = m[3] * g;
                tmp[0] = c1 * m[0] + s1 * m[3];
                tmp[1] = c1 * m[1] + s1 * m[4];
                tmp[2] = c1 * m[2] + s1 * m[5];

                m[3] = -s1 * m[0] + c1 * m[3]; // zero
                m[4] = -s1 * m[1] + c1 * m[4];
                m[5] = -s1 * m[2] + c1 * m[5];

                m[0] = tmp[0];
                m[1] = tmp[1];
                m[2] = tmp[2];
                u1[0] = c1;
                u1[1] = s1;
                u1[2] = 0.0;
                u1[3] = -s1;
                u1[4] = c1;
                u1[5] = 0.0;
                u1[6] = 0.0;
                u1[7] = 0.0;
                u1[8] = 1.0;
            }

            // u2
            if (m[6] * m[6] < EPS) {
            } else if (m[0] * m[0] < EPS) {
                tmp[0] = m[0];
                tmp[1] = m[1];
                tmp[2] = m[2];
                m[0] = m[6];
                m[1] = m[7];
                m[2] = m[8];

                m[6] = -tmp[0]; // zero
                m[7] = -tmp[1];
                m[8] = -tmp[2];

                tmp[0] = u1[0];
                tmp[1] = u1[1];
                tmp[2] = u1[2];
                u1[0] = u1[6];
                u1[1] = u1[7];
                u1[2] = u1[8];

                u1[6] = -tmp[0]; // zero
                u1[7] = -tmp[1];
                u1[8] = -tmp[2];
            } else {
                double g = 1.0 / sqrt(m[0] * m[0] + m[6] * m[6]);
                double c2 = m[0] * g;
                double s2 = m[6] * g;
                tmp[0] = c2 * m[0] + s2 * m[6];
                tmp[1] = c2 * m[1] + s2 * m[7];
                tmp[2] = c2 * m[2] + s2 * m[8];

                m[6] = -s2 * m[0] + c2 * m[6];
                m[7] = -s2 * m[1] + c2 * m[7];
                m[8] = -s2 * m[2] + c2 * m[8];
                m[0] = tmp[0];
                m[1] = tmp[1];
                m[2] = tmp[2];

                tmp[0] = c2 * u1[0];
                tmp[1] = c2 * u1[1];
                u1[2] = s2;

                tmp[6] = -u1[0] * s2;
                tmp[7] = -u1[1] * s2;
                u1[8] = c2;
                u1[0] = tmp[0];
                u1[1] = tmp[1];
                u1[6] = tmp[6];
                u1[7] = tmp[7];
            }

            // v1
            if (m[2] * m[2] < EPS) {
                v1[0] = 1.0;
                v1[1] = 0.0;
                v1[2] = 0.0;
                v1[3] = 0.0;
                v1[4] = 1.0;
                v1[5] = 0.0;
                v1[6] = 0.0;
                v1[7] = 0.0;
                v1[8] = 1.0;
            } else if (m[1] * m[1] < EPS) {
                tmp[2] = m[2];
                tmp[5] = m[5];
                tmp[8] = m[8];
                m[2] = -m[1];
                m[5] = -m[4];
                m[8] = -m[7];

                m[1] = tmp[2]; // zero
                m[4] = tmp[5];
                m[7] = tmp[8];

                v1[0] = 1.0;
                v1[1] = 0.0;
                v1[2] = 0.0;
                v1[3] = 0.0;
                v1[4] = 0.0;
                v1[5] = -1.0;
                v1[6] = 0.0;
                v1[7] = 1.0;
                v1[8] = 0.0;
            } else {
                double g = 1.0 / sqrt(m[1] * m[1] + m[2] * m[2]);
                double c3 = m[1] * g;
                double s3 = m[2] * g;
                tmp[1] = c3 * m[1] + s3 * m[2];  // can assign to m[1]?
                m[2] = -s3 * m[1] + c3 * m[2];  // zero
                m[1] = tmp[1];

                tmp[4] = c3 * m[4] + s3 * m[5];
                m[5] = -s3 * m[4] + c3 * m[5];
                m[4] = tmp[4];

                tmp[7] = c3 * m[7] + s3 * m[8];
                m[8] = -s3 * m[7] + c3 * m[8];
                m[7] = tmp[7];

                v1[0] = 1.0;
                v1[1] = 0.0;
                v1[2] = 0.0;
                v1[3] = 0.0;
                v1[4] = c3;
                v1[5] = -s3;
                v1[6] = 0.0;
                v1[7] = s3;
                v1[8] = c3;
            }

            // u3
            if (m[7] * m[7] < EPS) {
            } else if (m[4] * m[4] < EPS) {
                tmp[3] = m[3];
                tmp[4] = m[4];
                tmp[5] = m[5];
                m[3] = m[6];   // zero
                m[4] = m[7];
                m[5] = m[8];

                m[6] = -tmp[3]; // zero
                m[7] = -tmp[4]; // zero
                m[8] = -tmp[5];

                tmp[3] = u1[3];
                tmp[4] = u1[4];
                tmp[5] = u1[5];
                u1[3] = u1[6];
                u1[4] = u1[7];
                u1[5] = u1[8];

                u1[6] = -tmp[3]; // zero
                u1[7] = -tmp[4];
                u1[8] = -tmp[5];

            } else {
                double g = 1.0 / sqrt(m[4] * m[4] + m[7] * m[7]);
                double c4 = m[4] * g;
                double s4 = m[7] * g;
                tmp[3] = c4 * m[3] + s4 * m[6];
                m[6] = -s4 * m[3] + c4 * m[6];  // zero
                m[3] = tmp[3];

                tmp[4] = c4 * m[4] + s4 * m[7];
                m[7] = -s4 * m[4] + c4 * m[7];
                m[4] = tmp[4];

                tmp[5] = c4 * m[5] + s4 * m[8];
                m[8] = -s4 * m[5] + c4 * m[8];
                m[5] = tmp[5];

                tmp[3] = c4 * u1[3] + s4 * u1[6];
                u1[6] = -s4 * u1[3] + c4 * u1[6];
                u1[3] = tmp[3];

                tmp[4] = c4 * u1[4] + s4 * u1[7];
                u1[7] = -s4 * u1[4] + c4 * u1[7];
                u1[4] = tmp[4];

                tmp[5] = c4 * u1[5] + s4 * u1[8];
                u1[8] = -s4 * u1[5] + c4 * u1[8];
                u1[5] = tmp[5];
            }

            single_values[0] = m[0];
            single_values[1] = m[4];
            single_values[2] = m[8];
            e[0] = m[1];
            e[1] = m[5];

            if (!(e[0] * e[0] < EPS && e[1] * e[1] < EPS)) {
                compute_qr(single_values, e, u1, v1);
            }

            scales[0] = single_values[0];
            scales[1] = single_values[1];
            scales[2] = single_values[2];

            // Do some optimization here. If scale is unity, simply return the rotation matric.
            if (almostEqual(abs(scales[0]), 1.0)
                    && almostEqual(abs(scales[1]), 1.0)
                    && almostEqual(abs(scales[2]), 1.0)) {
                //  System.out.println("Scale components almost to 1.0");

                int negCnt = 0;
                for (int i = 0; i < 3; i++) {
                    if (scales[i] < 0.0)
                        negCnt++;
                }

                if ((negCnt == 0) || (negCnt == 2)) {
                    //System.out.println("Optimize!!");
                    outScale[0] = outScale[1] = outScale[2] = 1.0;
                    System.arraycopy(rot, 0, outRot, 0, 9);

                    return;
                }
            }

            transpose_mat(u1, t1);
            transpose_mat(v1, t2);

            /*
             System.out.println("t1 is \n" + t1);
             System.out.println("t1="+t1[0]+" "+t1[1]+" "+t1[2]);
             System.out.println("t1="+t1[3]+" "+t1[4]+" "+t1[5]);
             System.out.println("t1="+t1[6]+" "+t1[7]+" "+t1[8]);

             System.out.println("t2 is \n" + t2);
             System.out.println("t2="+t2[0]+" "+t2[1]+" "+t2[2]);
             System.out.println("t2="+t2[3]+" "+t2[4]+" "+t2[5]);
             System.out.println("t2="+t2[6]+" "+t2[7]+" "+t2[8]);
             */
            svdReorder(m, t1, t2, scales, outRot, outScale);

        } finally {
            //  Recycle scratch arrays.
            ArrayFactory.DOUBLES_FACTORY.recycle(u1);
            ArrayFactory.DOUBLES_FACTORY.recycle(v1);
            ArrayFactory.DOUBLES_FACTORY.recycle(t1);
            ArrayFactory.DOUBLES_FACTORY.recycle(t2);
            ArrayFactory.DOUBLES_FACTORY.recycle(rot);
            ArrayFactory.DOUBLES_FACTORY.recycle(e);
            ArrayFactory.DOUBLES_FACTORY.recycle(scales);
        }
    }

    private static void svdReorder(double[] m, double[] t1, double[] t2, double[] scales,
            double[] outRot, double[] outScale) {

        int[] out = ArrayFactory.INTS_FACTORY.array(3);         //  new int[3];
        int[] in = ArrayFactory.INTS_FACTORY.array(3);          //  new int[3];
        double[] mag = ArrayFactory.DOUBLES_FACTORY.array(3);   //  new double[3];
        double[] rot = ArrayFactory.DOUBLES_FACTORY.array(9);   //  new double[9];

        // check for rotation information in the scales
        if (scales[0] < 0.0) {   // move the rotation info to rotation matrix
            scales[0] = -scales[0];
            t2[0] = -t2[0];
            t2[1] = -t2[1];
            t2[2] = -t2[2];
        }
        if (scales[1] < 0.0) {   // move the rotation info to rotation matrix
            scales[1] = -scales[1];
            t2[3] = -t2[3];
            t2[4] = -t2[4];
            t2[5] = -t2[5];
        }
        if (scales[2] < 0.0) {   // move the rotation info to rotation matrix
            scales[2] = -scales[2];
            t2[6] = -t2[6];
            t2[7] = -t2[7];
            t2[8] = -t2[8];
        }

        mat_mul(t1, t2, rot);

        // check for equal scales case  and do not reorder
        if (almostEqual(abs(scales[0]), abs(scales[1]))
                && almostEqual(abs(scales[1]), abs(scales[2]))) {
            System.arraycopy(rot, 0, outRot, 0, 9);
            System.arraycopy(scales, 0, outScale, 0, 3);

        } else {

            // sort the order of the results of SVD
            if (scales[0] > scales[1]) {
                if (scales[0] > scales[2]) {
                    if (scales[2] > scales[1]) {
                        out[0] = 0;
                        out[1] = 2;
                        out[2] = 1; // xzy
                    } else {
                        out[0] = 0;
                        out[1] = 1;
                        out[2] = 2; // xyz
                    }
                } else {
                    out[0] = 2;
                    out[1] = 0;
                    out[2] = 1; // zxy
                }
            } else {  // y > x
                if (scales[1] > scales[2]) {
                    if (scales[2] > scales[0]) {
                        out[0] = 1;
                        out[1] = 2;
                        out[2] = 0; // yzx
                    } else {
                        out[0] = 1;
                        out[1] = 0;
                        out[2] = 2; // yxz
                    }
                } else {
                    out[0] = 2;
                    out[1] = 1;
                    out[2] = 0; // zyx
                }
            }

            /*
             System.out.println("\nscales="+scales[0]+" "+scales[1]+" "+scales[2]);
             System.out.println("\nrot="+rot[0]+" "+rot[1]+" "+rot[2]);
             System.out.println("rot="+rot[3]+" "+rot[4]+" "+rot[5]);
             System.out.println("rot="+rot[6]+" "+rot[7]+" "+rot[8]);
             */
            // sort the order of the input matrix
            mag[0] = (m[0] * m[0] + m[1] * m[1] + m[2] * m[2]);
            mag[1] = (m[3] * m[3] + m[4] * m[4] + m[5] * m[5]);
            mag[2] = (m[6] * m[6] + m[7] * m[7] + m[8] * m[8]);

            int in0, in1, in2;
            if (mag[0] > mag[1]) {
                if (mag[0] > mag[2]) {
                    if (mag[2] > mag[1]) {
                        // 0 - 2 - 1
                        in0 = 0;
                        in2 = 1;
                        in1 = 2;// xzy
                    } else {
                        // 0 - 1 - 2
                        in0 = 0;
                        in1 = 1;
                        in2 = 2; // xyz
                    }
                } else {
                    // 2 - 0 - 1
                    in2 = 0;
                    in0 = 1;
                    in1 = 2;  // zxy
                }
            } else {  // y > x   1>0
                if (mag[1] > mag[2]) {
                    if (mag[2] > mag[0]) {
                        // 1 - 2 - 0
                        in1 = 0;
                        in2 = 1;
                        in0 = 2; // yzx
                    } else {
                        // 1 - 0 - 2
                        in1 = 0;
                        in0 = 1;
                        in2 = 2; // yxz
                    }
                } else {
                    // 2 - 1 - 0
                    in2 = 0;
                    in1 = 1;
                    in0 = 2; // zyx
                }
            }

            int index = out[in0];
            outScale[0] = scales[index];

            index = out[in1];
            outScale[1] = scales[index];

            index = out[in2];
            outScale[2] = scales[index];

            index = out[in0];
            outRot[0] = rot[index];

            index = out[in0] + 3;
            outRot[0 + 3] = rot[index];

            index = out[in0] + 6;
            outRot[0 + 6] = rot[index];

            index = out[in1];
            outRot[1] = rot[index];

            index = out[in1] + 3;
            outRot[1 + 3] = rot[index];

            index = out[in1] + 6;
            outRot[1 + 6] = rot[index];

            index = out[in2];
            outRot[2] = rot[index];

            index = out[in2] + 3;
            outRot[2 + 3] = rot[index];

            index = out[in2] + 6;
            outRot[2 + 6] = rot[index];
        }

        //  Recycle scratch arrays.
        ArrayFactory.INTS_FACTORY.recycle(out);
        ArrayFactory.INTS_FACTORY.recycle(in);
        ArrayFactory.DOUBLES_FACTORY.recycle(mag);
        ArrayFactory.DOUBLES_FACTORY.recycle(rot);
    }

    private static void compute_qr(double[] s, double[] e, double[] u, double[] v) {

        double[] cosl = ArrayFactory.DOUBLES_FACTORY.array(2);  //  new double[2];
        double[] cosr = ArrayFactory.DOUBLES_FACTORY.array(2);  //  new double[2];
        double[] sinl = ArrayFactory.DOUBLES_FACTORY.array(2);  //  new double[2];
        double[] sinr = ArrayFactory.DOUBLES_FACTORY.array(2);  //  new double[2];
        double[] m = ArrayFactory.DOUBLES_FACTORY.array(9);     //  new double[9];

        final int MAX_INTERATIONS = 10;
        final double CONVERGE_TOL = 4.89E-15;
        final double c_b48 = 1.;

        boolean converged = false;
        if (abs(e[1]) < CONVERGE_TOL || abs(e[0]) < CONVERGE_TOL)
            converged = true;

        for (int k = 0; k < MAX_INTERATIONS && !converged; k++) {
            double shift = compute_shift(s[1], e[1], s[2]);
            double f = (abs(s[0]) - shift) * (d_sign(c_b48, s[0]) + shift / s[0]);
            double g = e[0];
            compute_rot(f, g, sinr, cosr, 0);
            f = cosr[0] * s[0] + sinr[0] * e[0];
            e[0] = cosr[0] * e[0] - sinr[0] * s[0];
            g = sinr[0] * s[1];
            s[1] = cosr[0] * s[1];

            double r = compute_rot(f, g, sinl, cosl, 0);
            s[0] = r;
            f = cosl[0] * e[0] + sinl[0] * s[1];
            s[1] = cosl[0] * s[1] - sinl[0] * e[0];
            g = sinl[0] * e[1];
            e[1] = cosl[0] * e[1];

            r = compute_rot(f, g, sinr, cosr, 1);
            e[0] = r;
            f = cosr[1] * s[1] + sinr[1] * e[1];
            e[1] = cosr[1] * e[1] - sinr[1] * s[1];
            g = sinr[1] * s[2];
            s[2] = cosr[1] * s[2];

            r = compute_rot(f, g, sinl, cosl, 1);
            s[1] = r;
            f = cosl[1] * e[1] + sinl[1] * s[2];
            s[2] = cosl[1] * s[2] - sinl[1] * e[1];
            e[1] = f;

            // update u  matrices
            double utemp = u[0];
            u[0] = cosl[0] * utemp + sinl[0] * u[3];
            u[3] = -sinl[0] * utemp + cosl[0] * u[3];
            utemp = u[1];
            u[1] = cosl[0] * utemp + sinl[0] * u[4];
            u[4] = -sinl[0] * utemp + cosl[0] * u[4];
            utemp = u[2];
            u[2] = cosl[0] * utemp + sinl[0] * u[5];
            u[5] = -sinl[0] * utemp + cosl[0] * u[5];

            utemp = u[3];
            u[3] = cosl[1] * utemp + sinl[1] * u[6];
            u[6] = -sinl[1] * utemp + cosl[1] * u[6];
            utemp = u[4];
            u[4] = cosl[1] * utemp + sinl[1] * u[7];
            u[7] = -sinl[1] * utemp + cosl[1] * u[7];
            utemp = u[5];
            u[5] = cosl[1] * utemp + sinl[1] * u[8];
            u[8] = -sinl[1] * utemp + cosl[1] * u[8];

            // update v  matrices
            double vtemp = v[0];
            v[0] = cosr[0] * vtemp + sinr[0] * v[1];
            v[1] = -sinr[0] * vtemp + cosr[0] * v[1];
            vtemp = v[3];
            v[3] = cosr[0] * vtemp + sinr[0] * v[4];
            v[4] = -sinr[0] * vtemp + cosr[0] * v[4];
            vtemp = v[6];
            v[6] = cosr[0] * vtemp + sinr[0] * v[7];
            v[7] = -sinr[0] * vtemp + cosr[0] * v[7];

            vtemp = v[1];
            v[1] = cosr[1] * vtemp + sinr[1] * v[2];
            v[2] = -sinr[1] * vtemp + cosr[1] * v[2];
            vtemp = v[4];
            v[4] = cosr[1] * vtemp + sinr[1] * v[5];
            v[5] = -sinr[1] * vtemp + cosr[1] * v[5];
            vtemp = v[7];
            v[7] = cosr[1] * vtemp + sinr[1] * v[8];
            v[8] = -sinr[1] * vtemp + cosr[1] * v[8];

            m[0] = s[0];
            m[1] = e[0];
            m[2] = 0.0;
            m[3] = 0.0;
            m[4] = s[1];
            m[5] = e[1];
            m[6] = 0.0;
            m[7] = 0.0;
            m[8] = s[2];

            if (abs(e[1]) < CONVERGE_TOL || abs(e[0]) < CONVERGE_TOL)
                converged = true;
        }

        if (abs(e[1]) < CONVERGE_TOL) {
            compute_2X2(s[0], e[0], s[1], s, sinl, cosl, sinr, cosr, 0);

            double utemp = u[0];
            u[0] = cosl[0] * utemp + sinl[0] * u[3];
            u[3] = -sinl[0] * utemp + cosl[0] * u[3];
            utemp = u[1];
            u[1] = cosl[0] * utemp + sinl[0] * u[4];
            u[4] = -sinl[0] * utemp + cosl[0] * u[4];
            utemp = u[2];
            u[2] = cosl[0] * utemp + sinl[0] * u[5];
            u[5] = -sinl[0] * utemp + cosl[0] * u[5];

            // update v  matrices
            double vtemp = v[0];
            v[0] = cosr[0] * vtemp + sinr[0] * v[1];
            v[1] = -sinr[0] * vtemp + cosr[0] * v[1];
            vtemp = v[3];
            v[3] = cosr[0] * vtemp + sinr[0] * v[4];
            v[4] = -sinr[0] * vtemp + cosr[0] * v[4];
            vtemp = v[6];
            v[6] = cosr[0] * vtemp + sinr[0] * v[7];
            v[7] = -sinr[0] * vtemp + cosr[0] * v[7];
        } else {
            compute_2X2(s[1], e[1], s[2], s, sinl, cosl, sinr, cosr, 1);

            double utemp = u[3];
            u[3] = cosl[0] * utemp + sinl[0] * u[6];
            u[6] = -sinl[0] * utemp + cosl[0] * u[6];
            utemp = u[4];
            u[4] = cosl[0] * utemp + sinl[0] * u[7];
            u[7] = -sinl[0] * utemp + cosl[0] * u[7];
            utemp = u[5];
            u[5] = cosl[0] * utemp + sinl[0] * u[8];
            u[8] = -sinl[0] * utemp + cosl[0] * u[8];

            // update v  matrices
            double vtemp = v[1];
            v[1] = cosr[0] * vtemp + sinr[0] * v[2];
            v[2] = -sinr[0] * vtemp + cosr[0] * v[2];
            vtemp = v[4];
            v[4] = cosr[0] * vtemp + sinr[0] * v[5];
            v[5] = -sinr[0] * vtemp + cosr[0] * v[5];
            vtemp = v[7];
            v[7] = cosr[0] * vtemp + sinr[0] * v[8];
            v[8] = -sinr[0] * vtemp + cosr[0] * v[8];
        }

        //  Recycle the temporary arrays.
        ArrayFactory.DOUBLES_FACTORY.recycle(cosl);
        ArrayFactory.DOUBLES_FACTORY.recycle(cosr);
        ArrayFactory.DOUBLES_FACTORY.recycle(sinl);
        ArrayFactory.DOUBLES_FACTORY.recycle(sinr);
        ArrayFactory.DOUBLES_FACTORY.recycle(m);

    }

    private static double max(double a, double b) {
        if (a > b)
            return (a);
        else
            return (b);
    }

    private static double min(double a, double b) {
        if (a < b)
            return (a);
        else
            return (b);
    }

    private static double d_sign(double a, double b) {
        double x = (a >= 0 ? a : -a);
        return (b >= 0 ? x : -x);
    }

    private static double compute_shift(double f, double g, double h) {
        double ssmin;

        double fa = abs(f);
        double ga = abs(g);
        double ha = abs(h);
        double fhmn = min(fa, ha);
        double fhmx = max(fa, ha);
        if (fhmn == 0.) {
            ssmin = 0.;
        } else {
            if (ga < fhmx) {
                double as = fhmn / fhmx + 1.;
                double at = (fhmx - fhmn) / fhmx;
                double d__1 = ga / fhmx;
                double au = d__1 * d__1;
                double c = 2. / (sqrt(as * as + au) + sqrt(at * at + au));
                ssmin = fhmn * c;
            } else {
                double au = fhmx / ga;
                if (au == 0.) {
                    ssmin = fhmn * fhmx / ga;
                } else {
                    double as = fhmn / fhmx + 1.;
                    double at = (fhmx - fhmn) / fhmx;
                    double d__1 = as * au;
                    double d__2 = at * au;
                    double c = 1. / (sqrt(d__1 * d__1 + 1.) + sqrt(d__2 * d__2 + 1.));
                    ssmin = fhmn * c * au;
                    ssmin += ssmin;
                }
            }
        }

        return (ssmin);
    }

    private static void compute_2X2(double f, double g, double h, double[] single_values,
            double[] snl, double[] csl, double[] snr, double[] csr, int index) {

        final double c_b3 = 2.;
        final double c_b4 = 1.;

        double ft = f;
        double fa = abs(ft);
        double ht = h;
        double ha = abs(h);

        int pmax = 1;
        boolean swap = false;
        if (ha > fa) {
            swap = true;
        }

        if (swap) {
            pmax = 3;
            double temp = ft;
            ft = ht;
            ht = temp;
            temp = fa;
            fa = ha;
            ha = temp;

        }
        double gt = g;
        double ga = abs(gt);
        if (ga == 0.) {

            single_values[1] = ha;
            single_values[0] = fa;
            //clt = 1.;
            //crt = 1.;
            //slt = 0.;
            //srt = 0.;
        } else {
            boolean gasmal = true;

            double ssmax = single_values[0];
            double ssmin = single_values[1];

            double clt = 0.0;
            double crt = 0.0;
            double slt = 0.0;
            double srt = 0.0;
            if (ga > fa) {
                pmax = 2;
                if (fa / ga < EPS) {

                    gasmal = false;
                    ssmax = ga;
                    if (ha > 1.) {
                        ssmin = fa / (ga / ha);
                    } else {
                        ssmin = fa / ga * ha;
                    }
                    clt = 1.;
                    slt = ht / gt;
                    srt = 1.;
                    crt = ft / gt;
                }
            }
            if (gasmal) {

                if (ga > fa) {
                    pmax = 2;
                    if (fa / ga < EPS) {

                        gasmal = false;
                        ssmax = ga;
                        if (ha > 1.) {
                            ssmin = fa / (ga / ha);
                        } else {
                            ssmin = fa / ga * ha;
                        }
                        clt = 1.;
                        slt = ht / gt;
                        srt = 1.;
                        crt = ft / gt;
                    }
                }
                if (gasmal) {

                    double d = fa - ha;
                    double l;
                    if (d == fa) {
                        l = 1.;
                    } else {
                        l = d / fa;
                    }

                    double m = gt / ft;

                    double t = 2. - l;

                    double mm = m * m;
                    double tt = t * t;
                    double s = sqrt(tt + mm);

                    double r;
                    if (l == 0.) {
                        r = abs(m);
                    } else {
                        r = sqrt(l * l + mm);
                    }

                    double a = (s + r) * .5;

                    ssmin = ha / a;
                    ssmax = fa * a;
                    if (mm == 0.) {

                        if (l == 0.) {
                            t = d_sign(c_b3, ft) * d_sign(c_b4, gt);
                        } else {
                            t = gt / d_sign(d, ft) + m / t;
                        }
                    } else {
                        t = (m / (s + t) + m / (r + l)) * (a + 1.);
                    }
                    l = sqrt(t * t + 4.);
                    crt = 2. / l;
                    srt = t / l;
                    clt = (crt + srt * m) / a;
                    slt = ht / ft * srt / a;
                }
            }
            if (swap) {
                csl[0] = srt;
                snl[0] = crt;
                csr[0] = slt;
                snr[0] = clt;
            } else {
                csl[0] = clt;
                snl[0] = slt;
                csr[0] = crt;
                snr[0] = srt;
            }

            double tsign = 0;
            if (pmax == 1) {
                tsign = d_sign(c_b4, csr[0]) * d_sign(c_b4, csl[0]) * d_sign(c_b4, f);
            }
            if (pmax == 2) {
                tsign = d_sign(c_b4, snr[0]) * d_sign(c_b4, csl[0]) * d_sign(c_b4, g);
            }
            if (pmax == 3) {
                tsign = d_sign(c_b4, snr[0]) * d_sign(c_b4, snl[0]) * d_sign(c_b4, h);
            }
            single_values[index] = d_sign(ssmax, tsign);
            double d__1 = tsign * d_sign(c_b4, f) * d_sign(c_b4, h);
            single_values[index + 1] = d_sign(ssmin, d__1);
        }

    }

    private static double compute_rot(double f, double g, double[] sin, double[] cos, int index) {
        double cs, sn;
        double scale;
        double r;
        final double safmn2 = 2.002083095183101E-146;
        final double safmx2 = 4.994797680505588E+145;

        if (g == 0.) {
            cs = 1.;
            sn = 0.;
            r = f;
        } else if (f == 0.) {
            cs = 0.;
            sn = 1.;
            r = g;
        } else {
            double f1 = f;
            double g1 = g;
            scale = max(abs(f1), abs(g1));
            if (scale >= safmx2) {
                int count = 0;
                while (scale >= safmx2) {
                    ++count;
                    f1 *= safmn2;
                    g1 *= safmn2;
                    scale = max(abs(f1), abs(g1));
                }
                r = sqrt(f1 * f1 + g1 * g1);
                cs = f1 / r;
                sn = g1 / r;
                for (int i = 1; i <= count; ++i) {
                    r *= safmx2;
                }
            } else if (scale <= safmn2) {
                int count = 0;
                while (scale <= safmn2) {
                    ++count;
                    f1 *= safmx2;
                    g1 *= safmx2;
                    scale = max(abs(f1), abs(g1));
                }
                r = sqrt(f1 * f1 + g1 * g1);
                cs = f1 / r;
                sn = g1 / r;
                for (int i = 1; i <= count; ++i) {
                    r *= safmn2;
                }
            } else {
                r = sqrt(f1 * f1 + g1 * g1);
                cs = f1 / r;
                sn = g1 / r;
            }
            if (abs(f) > abs(g) && cs < 0.) {
                cs = -cs;
                sn = -sn;
                r = -r;
            }
        }

        sin[index] = sn;
        cos[index] = cs;

        return r;
    }

    private static void mat_mul(double[] m1, double[] m2, double[] m3) {
        double[] tmp = ArrayFactory.DOUBLES_FACTORY.array(9);       //  new double[9];

        tmp[0] = m1[0] * m2[0] + m1[1] * m2[3] + m1[2] * m2[6];
        tmp[1] = m1[0] * m2[1] + m1[1] * m2[4] + m1[2] * m2[7];
        tmp[2] = m1[0] * m2[2] + m1[1] * m2[5] + m1[2] * m2[8];

        tmp[3] = m1[3] * m2[0] + m1[4] * m2[3] + m1[5] * m2[6];
        tmp[4] = m1[3] * m2[1] + m1[4] * m2[4] + m1[5] * m2[7];
        tmp[5] = m1[3] * m2[2] + m1[4] * m2[5] + m1[5] * m2[8];

        tmp[6] = m1[6] * m2[0] + m1[7] * m2[3] + m1[8] * m2[6];
        tmp[7] = m1[6] * m2[1] + m1[7] * m2[4] + m1[8] * m2[7];
        tmp[8] = m1[6] * m2[2] + m1[7] * m2[5] + m1[8] * m2[8];

        System.arraycopy(tmp, 0, m3, 0, 9);

        ArrayFactory.DOUBLES_FACTORY.recycle(tmp);
    }

    private static void transpose_mat(double[] in, double[] out) {
        out[0] = in[0];
        out[1] = in[3];
        out[2] = in[6];

        out[3] = in[1];
        out[4] = in[4];
        out[5] = in[7];

        out[6] = in[2];
        out[7] = in[5];
        out[8] = in[8];
    }

    private static double max3(double[] values) {
        if (values[0] > values[1]) {
            if (values[0] > values[2])
                return (values[0]);
            else
                return (values[2]);
        } else {
            if (values[1] > values[2])
                return (values[1]);
            else
                return (values[2]);
        }
    }

    private static boolean almostEqual(double a, double b) {
        if (a == b)
            return true;

        final double EPSILON_ABSOLUTE = 1.0e-6;
        double diff = abs(a - b);

        if (diff < EPSILON_ABSOLUTE)
            return true;

        final double EPSILON_RELATIVE = 1.0e-4;
        double absA = abs(a);
        double absB = abs(b);
        double max = (absA >= absB) ? absA : absB;

        return (diff / max) < EPSILON_RELATIVE;
    }

    /**
     * Holds the default XML representation for this transformation matrix. For example:
     * <pre>
     *    &lt;GTransform&gt;
     *        &lt;Float64 value="1" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="1" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="1" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="0" /&gt;
     *        &lt;Float64 value="1" /&gt;
     *    &lt;/GTransform&gt;
     * </pre>
     */
    protected static final XMLFormat<GTransform> XML
            = new XMLFormat<GTransform>(GTransform.class) {

                @Override
                public GTransform newInstance(Class<GTransform> cls, InputElement xml) throws XMLStreamException {
                    return FACTORY.object();
                }

                @Override
                public void read(InputElement xml, GTransform M) throws XMLStreamException {

                    FastTable<Float64Vector> rowList = FastTable.newInstance();

                    try {
                        for (int i = 0; i < 4; ++i) {
                            if (!xml.hasNext())
                                throw new XMLStreamException(
                                        MessageFormat.format(RESOURCES.getString("need16ElementsFoundLess"), i));

                            double x = ((Float64)xml.getNext()).doubleValue();
                            double y = ((Float64)xml.getNext()).doubleValue();
                            double z = ((Float64)xml.getNext()).doubleValue();
                            double w = ((Float64)xml.getNext()).doubleValue();
                            Float64Vector row = Float64Vector.valueOf(x, y, z, w);
                            rowList.add(row);
                        }

                        if (xml.hasNext())
                            throw new XMLStreamException(RESOURCES.getString("gtToManyElements"));

                        M._data = Float64Matrix.valueOf(rowList);

                    } finally {
                        FastTable.recycle(rowList);
                    }

                }

                @Override
                public void write(GTransform M, OutputElement xml) throws XMLStreamException {

                    for (int i = 0; i < 4; i++) {
                        for (int j = 0; j < 4; j++) {
                            xml.add(M.get(i, j));
                        }
                    }
                }
            };

    /**
     * Tests the methods in this class.
     *
     * @param args Command-line arguments (not used).
     */
    public static void main(String args[]) {
        System.out.println("Testing GTransform:");

        Parameter<Angle> psi = Parameter.valueOf(60, javax.measure.unit.NonSI.DEGREE_ANGLE);
        Parameter<Angle> theta = Parameter.ZERO_ANGLE;
        Parameter<Angle> phi = Parameter.valueOf(20, javax.measure.unit.NonSI.DEGREE_ANGLE);
        System.out.println("\npsi = " + psi);

        GTransform T1 = GTransform.newRotationZ(psi);
        System.out.println("   T1 = \n" + T1);

        System.out.println("\nphi = " + phi);

        GTransform T2 = GTransform.newRotationX(phi);
        System.out.println("   T2 = \n" + T2);
        System.out.println("   T1.times(T2) = \n" + T1.times(T2));

        System.out.println("\npsi = " + psi + ", theta = " + theta + ", phi = " + phi);
        DCMatrix M3 = DCMatrix.getEulerTM(psi, theta, phi);
        System.out.println("M3 = \n" + M3);
        GTransform T3 = GTransform.valueOf(M3);
        System.out.println("   T3 = \n" + T3);

        Point p1 = Point.valueOf(1, 1, 1, METER);
        System.out.println("\np1 = " + p1);
        System.out.println("M3.times(p1.toVector3D()) = " + M3.times(p1.toVector3D()));
        System.out.println("T3.transform(p1) = " + T3.transform(p1));
        System.out.println("T3.transform(p1).norm() = " + T3.transform(p1).norm() + " [1.73205080756888 m]");

        GTransform T4 = T3.applyScale(2.5);
        System.out.println("\nT4 = T3.applyScale(2.5);");
        System.out.println("T4.transform(p1) = " + T4.transform(p1));
        System.out.println("T4.transform(p1).norm() = " + T4.transform(p1).norm() + " [4.33012701892219 m]");
        System.out.println("T4.getScale() = " + T4.getScale());
        System.out.println("T4.getRotation() = \n" + T4.getRotation());

        GTransform T5 = GTransform.newTranslation(Vector.valueOf(METER, 10, -5, 0.5));
        System.out.println("\np1 = " + p1);
        System.out.println("T5.transform(p1) = " + T5.transform(p1) + " [11 m, -4 m, 1.5 m]");

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new GeomXMLBinding();

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(T4, "GTransform", GTransform.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
