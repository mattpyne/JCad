/**
 * MutableVector -- Holds the floating point coordinates of an nD vector that can be
 * changed.
 *
 * Copyright (C) 2009-2015, Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.*;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * A container that holds changeable coordinates of an n-dimensional vector which
 * indicates direction, but not position.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: June 13, 2009
 * @version November 27, 2015
 *
 * @param <Q> The Quantity (unit type) of the elements of this vector.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class MutableVector<Q extends Quantity> extends GeomVector<Q> {

    //  Use an immutable Vector_stp as a backing for the MutableVector.
    private Vector<Q> _vector;

    /**
     * Returns a dimensionless <code>MutableVector</code> instance of the specified
     * dimension with all the coordinate values set to zero.
     *
     * @param dim the physical dimension of the vector to create.
     * @return the vector having the specified dimension and zero meters for values.
     */
    public static MutableVector<Dimensionless> newInstance(int dim) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.newInstance(dim);
        return V;
    }

    /**
     * Returns a <code>MutableVector</code> instance of the specified dimension and units
     * with all the coordinate values set to zero.
     *
     * @param <Q>  The Quantity (unit type) of the returned vector.
     * @param dim  the physical dimension of the vector to create.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @return the vector having the specified dimension and zero meters for values.
     */
    public static <Q extends Quantity> MutableVector<Q> newInstance(int dim, Unit<Q> unit) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.newInstance(dim, requireNonNull(unit));
        return V;
    }

    /**
     * Returns a <code>Vector_stp</code> instance containing the specified GeomVector values.
     *
     * @param <Q>    The Quantity (unit type) of the returned vector.
     * @param vector the GeomVector to be placed in a new Vector_stp instance. May not be null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> MutableVector<Q> valueOf(GeomVector<Q> vector) {
        MutableVector V = FACTORY.object();
        V._vector = vector.immutable();
        return V;
    }

    /**
     * Returns a dimensionless <code>MutableVector</code> instance holding the specified
     * <code>double</code> value or values.
     *
     * @param x The dimensionless coordinate values. May not be null.
     * @return the vector having the specified value.
     */
    public static MutableVector<Dimensionless> valueOf(double... x) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(x));
        return V;
    }

    /**
     * Returns a <code>MutableVector</code> instance holding the specified
     * <code>double</code> value or values stated in the specified units.
     *
     * @param <Q>  The Quantity (unit type) of the returned vector.
     * @param unit the unit in which the coordinates are stated. May not be null.
     * @param x    the coordinate values stated in the specified unit. May not be null.
     * @return the vector having the specified value.
     */
    public static <Q extends Quantity> MutableVector<Q> valueOf(Unit<Q> unit, double... x) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(unit), requireNonNull(x));
        return V;
    }

    /**
     * Returns a <code>MutableVector</code> instance containing the specified vector of
     * Parameter values with compatible units. All the values are converted to the same
     * units as the 1st value.
     *
     * @param <Q>    The Quantity (unit type) of the returned vector.
     * @param vector the vector of Parameter values stated in the specified unit. May not
     *               be null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> MutableVector<Q> valueOf(org.jscience.mathematics.vector.Vector<Parameter<Q>> vector) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(vector));
        return V;
    }

    /**
     * Returns a <code>MutableVector</code> instance containing the specified list of
     * Parameter values with compatible units. All the values are converted to the same
     * units as the 1st value.
     *
     * @param <Q>    The Quantity (unit type) of the returned vector.
     * @param values the list of Parameter values stated in the specified unit. May not be
     *               null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> MutableVector<Q> valueOf(List<Parameter<Q>> values) {
        MutableVector<Q> V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(values));
        return V;
    }

    /**
     * Returns a {@link MutableVector} instance containing the specified vector of Float64
     * values stated in the specified units.
     *
     * @param <Q>    The Quantity (unit type) of the returned vector.
     * @param vector the vector of Float64 values stated in the specified unit. May not be
     *               null.
     * @param unit   the unit in which the coordinates are stated. May not be null.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> MutableVector<Q> valueOf(org.jscience.mathematics.vector.Vector<Float64> vector, Unit<Q> unit) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(vector), requireNonNull(unit));
        return V;
    }

    /**
     * Returns a <code>MutableVector</code> instance holding the specified
     * <code>Parameter</code> values. All the values are converted to the same units as
     * the first value.
     *
     * @param <Q>    The Quantity (unit type) of the returned vector.
     * @param values A list of values to store in the vector. May not be null.
     * @return the vector having the specified values in the units of x.
     */
    public static <Q extends Quantity> MutableVector<Q> valueOf(Parameter<Q>... values) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(values));
        return V;
    }

    /**
     * Returns a <code>MutableVector</code> instance holding the specified
     * <code>GeomPoint</code> values.
     *
     * @param point A point who's coordinates are to be stored in the vector (making it a
     *              position vector). May not be null.
     * @return the vector having the specified point coordinate values in it.
     */
    public static MutableVector<Length> valueOf(GeomPoint point) {
        MutableVector V = FACTORY.object();
        V._vector = Vector.valueOf(requireNonNull(point));
        return V;
    }

    /**
     * Returns the number of physical dimensions of this vector.
     *
     * @return The number of physical dimensions of this vector.
     */
    @Override
    public int getPhyDimension() {
        return _vector.getPhyDimension();
    }

    /**
     * Set the value of a vector dimension to the specified Parameter.
     *
     * @param i     the dimension index.
     * @param value The new value of the parameter to set at <code>i</code>. May not be
     *              null.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public void set(int i, Parameter<Q> value) {
        requireNonNull(value);
        
        //  Get a list of values in this point.
        FastTable<Parameter<Q>> values = FastTable.newInstance();
        int size = _vector.getPhyDimension();
        for (int j = 0; j < size; ++j) {
            values.add(_vector.get(j));
        }

        //  Change the dimension indicated.
        values.set(i, value);
        _vector = Vector.valueOf(values);
        FastTable.recycle(values);

        fireChangeEvent();  //  Notify change listeners.
    }

    /**
     * Set the value of elements of this vector to the elements of the specified vector.
     *
     * @param v The new vector to make this vector equal to. May not be null.
     * @throws DimensionException <code>(v.getPhyDimension() != getPhyDimension())</code>
     */
    public void set(GeomVector<Q> v) {
        int numDims = getPhyDimension();
        if (v.getPhyDimension() != numDims)
            throw new DimensionException(RESOURCES.getString("consistantPointDim"));
        _vector = v.immutable();
    }

    /**
     * Set the value of a vector dimension to the specified double in the current vector
     * units.
     *
     * @param i     the dimension index.
     * @param value The new value of the parameter to set at <code>i</code> in the current
     *              units of this vector.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    public void setValue(int i, double value) {
        //  Create a Parameter object.
        Parameter<Q> param = Parameter.valueOf(value, getUnit());

        //  Set the specified dimension to the new parameter object.
        set(i, param);
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in this vector's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i) {
        return _vector.getValue(i);
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in the specified units.
     *
     * @param i    the dimension index.
     * @param unit the unit to return the value in. May not be null.
     * @return the value of the Parameter at <code>i</code> in the specified unit.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &ge; getPhyDimension())</code>
     */
    @Override
    public double getValue(int i, Unit<Q> unit) {
        double value = _vector.getValue(i);
        Unit<Q> thisUnit = getUnit();
        if (unit.equals(thisUnit))
            return value;
        UnitConverter cvtr = Parameter.converterOf(thisUnit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            return value;
        }
        return cvtr.convert(value);
    }

    /**
     * Set the origin point for this vector. The origin is used as a reference for drawing
     * the vector and is <i>not</i> a part of the state of this vector and is not used in
     * any calculations with this vector.
     *
     * @param origin The point to set as the origin of this vector. May not be null.
     */
    @Override
    public void setOrigin(Point origin) {
        _vector.setOrigin(requireNonNull(origin));
    }

    /**
     * Return the origin point for this vector.
     *
     * @return The origin point for this vector.
     */
    @Override
    public Point getOrigin() {
        return _vector.getOrigin();
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector (square root
     * of the dot product of this vector and itself).
     *
     * @return <code>this.norm().getValue()</code>.
     */
    @Override
    public double normValue() {
        return _vector.normValue();
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    @Override
    public Vector<Q> opposite() {
        return _vector.opposite();
    }

    /**
     * Returns the sum of this vector with the one specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added. May not be null.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector<Q> plus(GeomVector<Q> that) {
        return _vector.plus(that);
    }

    /**
     * Returns the sum of this vector with the parameter specified. The input parameter is
     * added to each component of this vector. The unit of the output vector will be the
     * units of this vector.
     *
     * @param that the parameter to be added to each element of this vector. May not be null.
     * @return <code>this + that</code>.
     */
    @Override
    public Vector<Q> plus(Parameter<Q> that) {
        return _vector.plus(that);
    }

    /**
     * Returns the difference between this vector and the one specified. The unit of the
     * output vector will be the units of this vector.
     *
     * @param that the vector to be subtracted from this vector. May not be null.
     * @return <code>this - that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector<Q> minus(GeomVector<Q> that) {
        return _vector.minus(that);
    }

    /**
     * Subtracts the supplied Parameter from each element of this vector and returns the
     * result. The unit of the output vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from each element of this vector. May
     *             not be null.
     * @return <code>this - that</code>.
     */
    @Override
    public Vector<Q> minus(Parameter<Q> that) {
        return _vector.minus(that);
    }

    /**
     * Returns the product of this vector with the specified coefficient (dimensionless).
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Vector<Q> times(double k) {
        return _vector.times(k);
    }

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier. May not be null.
     * @return <code>this · k</code>
     */
    @Override
    public Vector<? extends Quantity> times(Parameter<?> k) {
        return _vector.times(k);
    }

    /**
     * Returns the dot product (scalar product) of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    @Override
    public Parameter<? extends Quantity> times(GeomVector<?> that) {
        return _vector.times(that);
    }

    /**
     * Returns the element-by-element product of this vector with the one specified.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this .* that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     */
    @Override
    public Vector<? extends Quantity> timesEBE(GeomVector<?> that) {
        return _vector.timesEBE(that);
    }

    /**
     * Returns the cross product of two vectors.
     *
     * @param that the vector multiplier. May not be null.
     * @return <code>this x that</code>
     * @throws DimensionException if
     * <code>(that.getDimension() != this.getDimension())</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    @Override
    public Vector<? extends Quantity> cross(GeomVector<?> that) {
        return _vector.cross(that);
    }

    /**
     * Returns this vector with each element divided by the specified divisor
     * (dimensionless).
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    @Override
    public Vector<Q> divide(double divisor) {
        return _vector.divide(divisor);
    }

    /**
     * Returns this vector with each element divided by the specified divisor.
     *
     * @param divisor the divisor. May not be null.
     * @return <code>this / divisor</code>.
     */
    @Override
    public Vector<? extends Quantity> divide(Parameter<?> divisor) {
        return _vector.divide(divisor);
    }

    /**
     * Return an immutable version of this vector.
     *
     * @return An immutable version of this vector.
     */
    @Override
    public Vector<Q> immutable() {
        return _vector;
    }

    /**
     * Returns this vector converted to a unit vector by dividing all the vector's
     * elements by the length ({@link #norm}) of this vector.
     *
     * @return This vector converted to a unit vector.
     */
    @Override
    public Vector<Dimensionless> toUnitVector() {
        return _vector.toUnitVector();
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this vector are stated
     * in.
     *
     * @return The unit in which the values in this vector are stated.
     */
    @Override
    public Unit getUnit() {
        return _vector.getUnit();
    }

    /**
     * Returns the equivalent to this vector but stated in the specified unit.
     *
     * @param unit the unit of the vector to be returned. May not be null.
     * @return an equivalent to this vector but stated in the specified unit.
     * @throws ConversionException if the the input unit is not compatible with this unit.
     */
    @Override
    public MutableVector to(Unit unit) throws ConversionException {
        if (unit.equals(getUnit()))
            return this;
        MutableVector V = FACTORY.object();
        V._vector = this._vector.to(unit);
        return V;
    }

    /**
     * Return a copy of this vector converted to the specified number of physical
     * dimensions. If the number of dimensions is greater than this element, then zeros
     * are added to the additional dimensions. If the number of dimensions is less than
     * this element, then the extra dimensions are simply dropped (truncated). If the new
     * dimensions are the same as the dimension of this element, then this element is
     * simply returned.
     *
     * @param newDim The dimension of the vector to return.
     * @return A copy of this vector converted to the new dimensions.
     */
    @Override
    public MutableVector<Q> toDimension(int newDim) {
        if (newDim < 1)
            throw new IllegalArgumentException(RESOURCES.getString("vectorDimGT0"));
        int thisDim = this.getPhyDimension();
        if (newDim == thisDim)
            return this;

        MutableVector<Q> V = FACTORY.object();
        V._vector = this._vector.toDimension(newDim);

        return V;
    }

    /**
     * Returns a copy of this Vector_stp instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public MutableVector<Q> copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges removed
     * (applied).
     *
     * @return A copy of this object with any transformations or subranges removed.
     */
    @Override
    public Vector<Q> copyToReal() {
        return Vector.valueOf(_vector);
    }

    /**
     * Returns a <code>ParameterVector</code> representation of this vector.
     *
     * @return A ParameterVector that is equivalent to this vector.
     */
    @Override
    public ParameterVector<Q> toParameterVector() {
        return _vector.toParameterVector();
    }

    /**
     * Returns a <code>Float64Vector</code> containing the elements of this vector stated
     * in the current units.
     *
     * @return A Float64Vector that contains the elements of this vector in the current
     *         units.
     */
    @Override
    public Float64Vector toFloat64Vector() {
        return _vector.toFloat64Vector();
    }

    /**
     * Compares this vector against the specified object for strict equality (same values
     * and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this vector is identical to that vector;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        MutableVector that = (MutableVector)obj;
        return this._vector.equals(that._vector)
                && super.equals(obj);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        return 31*super.hashCode() + Objects.hash(_vector);
    }

    /**
     * Recycles a <code>MutableVector</code> instance immediately (on the stack when
     * executing in a <code>StackContext</code>).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(MutableVector instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<MutableVector> XML = new XMLFormat<MutableVector>(MutableVector.class) {

        @Override
        public MutableVector newInstance(Class<MutableVector> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, MutableVector obj) throws XMLStreamException {

            Unit unit = Unit.valueOf(xml.getAttribute("unit"));
            if (!(Length.UNIT.isCompatible(unit) || Dimensionless.UNIT.isCompatible(unit)))
                throw new XMLStreamException(
                        MessageFormat.format(RESOURCES.getString("incompatibleUnits"),
                                "MutableVector", "length"));

            GeomVector.XML.read(xml, obj);     // Call parent read.

            FastTable<Float64> valueList = FastTable.newInstance();
            while (xml.hasNext()) {
                Float64 value = xml.getNext();
                valueList.add(value);
            }

            obj._vector = Vector.valueOf(Float64Vector.valueOf(valueList), unit);

        }

        @Override
        public void write(MutableVector obj, OutputElement xml) throws XMLStreamException {
            GeomVector.XML.write(obj, xml);    // Call parent write.

            xml.setAttribute("unit", obj.getUnit().toString());
            int size = obj._vector.getPhyDimension();
            for (int i = 0; i < size; ++i)
                xml.add(Float64.valueOf(obj._vector.getValue(i)));

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private MutableVector() { }

    @SuppressWarnings("unchecked")
    private static final ObjectFactory<MutableVector> FACTORY = new ObjectFactory<MutableVector>() {
        @Override
        protected MutableVector create() {
            return new MutableVector();
        }

        @Override
        protected void cleanup(MutableVector obj) {
            obj.reset();
        }
    };

    @SuppressWarnings("unchecked")
    private static <Q extends Quantity> MutableVector<Q> copyOf(MutableVector<Q> original) {
        MutableVector<Q> V = FACTORY.object();
        V._vector = original._vector.copy();
        original.copyState(V);
        return V;
    }
}
