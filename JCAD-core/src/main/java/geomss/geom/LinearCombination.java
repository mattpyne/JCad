/**
 * LinearCombination -- Represents the interface for a linear combination of two or more
 * objects.
 *
 * Copyright (C) 2015, Joseph A. Huwaldt All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import java.util.Collection;
import java.util.List;

/**
 * Represents a linear combination made up of a list of two or more GeomElement objects. A
 * linear combination is formed by a weighted linear addition of a list of one or more
 * objects. For example: <code>B = W1*Obj_1 + ... + Wn*Obj_n</code> where W1 through Wn
 * are scalar weights. The linear addition is done independently of physical dimension
 * (each dimension is added separately) and the weighted addition is done in parameter
 * space in appropriate: <code> B = W1*Srf_1(s,t) + ... + Wn*Srf_n(s,t)</code>.
 *
 * <p>
 * Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: September 10, 2015
 * @version November 27, 2015
 *
 * @param <T> The type of this LinearCombination.
 * @param <E> The type of elements contained in this LinearCombination.
 */
public interface LinearCombination<T extends LinearCombination, E extends GeomElement> extends GeometryList<T, E> {
    
    /**
     * Returns an unmodifiable list view of the list of weights in this linear
     * combination. Attempts to modify the returned collection result in an
     * UnsupportedOperationException being thrown.
     *
     * @return the unmodifiable view over this list of weights.
     */
    public List<Double> unmodifiableWeightList();

    /**
     * Returns the linear combination weight at the specified position in this list.
     *
     * @param index index of weight to return (0 returns the 1st element, -1 returns the
     *              last, -2 returns the 2nd from last, etc).
     * @return the linear combination weight at the specified position in this linear
     *         combination.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index > size()</code>
     */
    public Double getWeight(int index);
    
    /**
     * Returns the first linear combination weight from this list.
     *
     * @return the first weight in this list.
     */
    public Double getFirstWeight();
    
    /**
     * Returns the last linear combination weight from this list.
     *
     * @return the last weight in this list.
     */
    public Double getLastWeight();

    /**
     * Returns the range of linear combination weights in this list from the specified
     * start and ending indexes as a List of double values.
     *
     * @param first index of the first element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @param last  index of the last element to return (0 returns the 1st element, -1
     *              returns the last, etc).
     * @return A List made up of the weights in the given range from this
     *         LinearComboCurve.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     * <code>index &ge; size()</code>
     */
    public List<Double> getWeightRange(int first, int last);

    /**
     * Inserts the specified GeomElement at the specified position in this list. Shifts
     * the element currently at that position (if any) and any subsequent elements to the
     * right (adds one to their indices). Null values are ignored.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param index  the index at which the specified element is to be inserted. (0
     *               returns the 1st element, -1 returns the last, -2 returns the 2nd from
     *               last, etc).
     * @param value  the element to be inserted. May not be null.
     * @param weight the linear combination weight of the element to be inserted. May not
     *               be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     */
    public void add(int index, E value, Double weight);
   
    /**
     * Appends the specified GeomElement to the end of this list. Null values are ignored.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param value  the element to be appended to this list. May not be null.
     * @param weight the linear combination weight of the element to be appended. May not
     *               be null.
     * @return true if this list changed as a result of this call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    public boolean add(E value, Double weight);

    /**
     * Appends all of the GeomElement objects in the specified collection to this list.
     * The behavior of this operation is undefined if the specified collection is modified
     * while the operation is in progress. (This implies that the behavior of this call is
     * undefined if the specified collection is this collection, and this collection is
     * nonempty.)
     *
     * @param elements the elements to be appended onto this list. May not be null.
     * @param weights  the linear combination weights associated with all of the elements
     *                 being appended. May not be null.
     * @return <code>true</code> if this LinearComboCurve changed as a result of the call
     */
    public boolean addAll(Collection<? extends E> elements, Collection<Double> weights);

    /**
     * Inserts all of the GeomElement objects in the specified collection and their
     * associated weights into this linear combination at the specified position. Shifts
     * the element currently at that position (if any) and any subsequent elements to the
     * right (increases their indices). The new elements will appear in this list in the
     * order that they are returned by the specified collection's iterator. The behavior
     * of this operation is unspecified if the specified collection is modified while the
     * operation is in progress. (Note that this will occur if the specified collection is
     * this list, and it's nonempty.)
     *
     * @param index    index at which to insert first element from the specified
     *                 collection.
     * @param elements the elements to be inserted into this linear combination. May not
     *                 be null.
     * @param weights  the linear combination weights associated with each element being
     *                 inserted. May not be null.
     * @return <code>true</code> if this LinearComboCurve changed as a result of the call
     */
    public boolean addAll(int index, Collection<? extends E> elements, Collection<Double> weights);

    /**
     * Replaces the weight at the specified position in this linear combination list with
     * the specified weight. The GeomElement at that position is left unchanged. Null
     * elements are ignored.
     *
     * @param index  The index of the weight to replace (0 returns the 1st element, -1
     *               returns the last, -2 returns the 2nd from last, etc).
     * @param weight The weight to be stored at the specified position. The GeomElement at
     *               that position is left unchanged. May not be null.
     * @return The weight previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > size()</code>
     */
    public Double setWeight(int index, Double weight);

    /**
     * Replaces the GeomElement and weight at the specified position in this linear
     * combination with the specified GeomElement and weight. Null elements are ignored.
     *
     * @param index  The index of the element and weight to replace (0 returns the 1st
     *               element, -1 returns the last, -2 returns the 2nd from last, etc).
     * @param curve  The GeomElement to be stored at the specified position.
     *                <code>null</code> elements are ignored.
     * @param weight The weight to be stored at the specified position.  May not be null.
     * @return The GeomElement previously at the specified position in this list. The
     *         previous weight is lost.
     * @throws java.lang.IndexOutOfBoundsException - if  <code>index > size()</code>
     */
    public E set(int index, E curve, Double weight);

}
