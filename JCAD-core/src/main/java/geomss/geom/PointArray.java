/**
 * PointArray -- A collection of PointString objects that make up a "list of strings" or
 * an "array of points".
 *
 * Copyright (C) 2003-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package geomss.geom;

import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Collection;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Area;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLReferenceResolver;
import javolution.xml.stream.XMLStreamException;

/**
 * A <code>PointArray</code> is a collection of {@link PointString} objects that make up a
 * "list of strings of points". Any number of strings may be added to an array. All the
 * strings in an array must have the same dimensions. In order to draw the array, all the
 * strings in a array must also have the same number of points.
 * <p>
 * WARNING: This list allows geometry to be stored in different units. If consistent units
 * are required, then the user must specifically convert the list items.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 22, 2017
 *
 * @param <E> The type of GeomPoint stored in this array of points.
 */
@SuppressWarnings({"serial", "CloneableImplementsClone"})
public final class PointArray<E extends GeomPoint> extends AbstractPointGeomList<PointArray<E>, PointString<E>> {

    private FastTable<PointString<E>> _list;

    /**
     * Return the list underlying this geometry list.
     *
     * @return The list underlying this geometry list.
     */
    @Override
    protected FastTable<PointString<E>> getList() {
        return _list;
    }

    /**
     * Returns a new, empty, preallocated or recycled <code>PointArray</code>
     * instance (on the stack when executing in a <code>StackContext</code>),
     * that can store a list of {@link PointString} objects.
     *
     * @return A new, empty PointArray
     */
    public static PointArray newInstance() {
        PointArray list = FACTORY.object();
        list._list = FastTable.newInstance();
        return list;
    }

    /**
     * Returns a new, preallocated or recycled <code>PointArray</code> instance
     * (on the stack when executing in a <code>StackContext</code>) with the
     * specified name, that can store a list of {@link PointString} objects.
     *
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @return A new, empty PointArray
     */
    public static PointArray newInstance(String name) {
        PointArray list = PointArray.newInstance();
        list.setName(name);
        return list;
    }

    /**
     * Return a PointArray made up of the {@link PointString} objects in the
     * specified collection.
     *
     * @param <E> The type of GeomPoint stored in this array of points.
     * @param name The name to be assigned to this list (may be <code>null</code>).
     * @param elements A collection that contains a set of strings. May not be null.
     * @return A new PointArray containing the elements in the specified collection.
     */
    public static <E extends GeomPoint> PointArray<E> valueOf(String name, Collection<? extends PointString<E>> elements) {
        for (Object element : elements) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof PointString))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "PointArray", "PointString"));
        }

        PointArray<E> list = PointArray.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a PointArray made up of the {@link PointString} objects in the
     * specified array.
     *
     * @param <E> The type of GeomPoint stored in this array of points.
     * @param name The name to be assigned to this string (may be <code>null</code>).
     * @param elements A list that contains a set of strings. May not be null.
     * @return A new PointArray containing the elements in the specified array.
     */
    public static <E extends GeomPoint> PointArray<E> valueOf(String name, PointString<E>... elements) {
        requireNonNull(elements);
        PointArray<E> list = PointArray.newInstance(name);
        list.addAll(elements);

        return list;
    }

    /**
     * Return a PointArray made up of the {@link PointString} objects in the
     * specified array.
     *
     * @param <E> The type of GeomPoint stored in this array of points.
     * @param elements A list that contains a set of strings. May not be null.
     * @return A new PointArray containing the elements in the specified array.
     */
    public static <E extends GeomPoint> PointArray<E> valueOf(PointString<E>... elements) {
        return PointArray.valueOf(null, elements);
    }

    /**
     * Returns the {@link GeomPoint} at the specified pair of indices in this
     * array. This is a convenience method that is identical to
     * <code>array.get(row).get(col)</code>.
     *
     * @param row The row (string or "T") index of the panel to be returned
     * @param col The column (point in a string or "S") index of the panel to be returned
     * @return The point at the specified row and column in this array.
     * @throws IndexOutOfBoundsException if there the specified indices do not
     * exist in this array.
     */
    public E get(int row, int col) throws IndexOutOfBoundsException {
        return this.get(row).get(col);
    }

    /**
     * Return a column of points from this array as a new PointString object. A column is
     * made up of a point from each string of the array.
     *
     * @param col The column of points to return.
     * @return A new PointString object containing the points from the specified column of
     * this array.
     * @throws IndexOutOfBoundsException if there the specified index does not exist in
     * this array.
     */
    public PointString<E> getColumn(int col) throws IndexOutOfBoundsException {
        PointString<E> output = PointString.newInstance();
        int size = size();
        for (int row = 0; row < size; ++row) {
            output.add(get(row).get(col));
        }
        return output;
    }
    
    /**
     * Returns the range of elements in this list from the specified start and
     * ending indexes.
     *
     * @param first index of the first element to return (0 returns the 1st
     *  element, -1 returns the last, etc).
     * @param last index of the last element to return (0 returns the 1st
     *  element, -1 returns the last, etc).
     * @return the list of elements in the given range from this list.
     * @throws IndexOutOfBoundsException if the given index is out of range:
     *  <code>index >= size()</code>
     */
    @Override
    public PointArray<E> getRange(int first, int last) {
        first = normalizeIndex(first);
        last = normalizeIndex(last);

        PointArray<E> list = PointArray.newInstance();
        for (int i = first; i <= last; ++i)
            list.add(get(i));
        return list;
    }

    /**
     * Returns an new {@link PointArray} with the elements in this list in
     * reverse order.
     *
     * @return A new PointArray with the elements in this list in reverse order.
     * This reverses the rows of the array.
     */
    @Override
    public PointArray<E> reverse() {
        PointArray<E> list = PointArray.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = size - 1; i >= 0; --i) {
            list.add(get(i));
        }
        return list;
    }

    /**
     * Returns an new {@link PointArray} with the points in each string in this
     * array in reverse order.
     *
     * @return A new PointArray with the points in each string in reverse order.
     * This reverses the columns of the array.
     */
    public PointArray<E> reverseStrings() {
        PointArray<E> list = PointArray.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString<E> str = this.get(i);
            list.add(str.reverse());
        }
        return list;
    }

    /**
     * Returns a new {@link PointArray} that is the transpose this array (rows
     * and columns swapped). This method can only be called if all the strings
     * in this array have the same number of points in them.
     *
     * @return A new PointArray that is the transpose of this array.
     * @throws IndexOutOfBoundsException if the number of points is not the same
     * in each string in this array.
     */
    public PointArray<E> transpose() throws IndexOutOfBoundsException {

        //  Check for string consistancy in this array.
        checkStringConsistancy();

        //  Build up the transposed array.
        PointArray<E> arr = PointArray.newInstance();
        copyState(arr);
        int numStrings = size();
        int numPointsPerString = get(0).size();
        for (int i = 0; i < numPointsPerString; ++i) {
            PointString<E> str = PointString.newInstance();
            for (int j = 0; j < numStrings; ++j)
                str.add(this.get(j).get(i));
            arr.add(str);
        }

        return arr;
    }

    /**
     * Returns a new {@link PointArray} that is identical to this array but with every
     * other row (string of points) and column (points in the strings) removed. The 1st
     * row and column and the last row and column are always retained. If there are less
     * than 3 columns in the array, then a new array is returned that contains the same
     * columns as this array. If there are less than 3 rows, then a new array is returned
     * that contains the same rows as this array.
     *
     * @return A new PointArray identical to this array, but with every other row and
     * column removed.
     */
    public PointArray<E> thin() {
        PointArray<E> arr1 = thinRows();
        PointArray<E> arr2 = arr1.thinColumns();
        PointArray.recycle(arr1);
        return arr2;
    }

    /**
     * Returns a new {@link PointArray} that is identical to this array but with every
     * other row (string of points) removed. The 1st row (index = 0) and last row (index =
     * size()-1) are always retained. If there are less than 3 rows in the array, then a
     * new array is returned that contains the same rows as this array.
     *
     * @return A new PointArray that is identical to this array but with every other row
     * removed.
     */
    public PointArray<E> thinRows() {
        PointArray<E> list = PointArray.newInstance();
        copyState(list);

        int size = size();
        if (size < 3) {
            list.addAll(this);
            return list;
        }

        int sizem1 = size - 1;
        for (int i = 0; i < sizem1; i += 2) {
            list.add(get(i));
        }
        list.add(get(size - 1));

        return list;
    }

    /**
     * Returns a new {@link PointArray} that is identical to this array but with every
     * other column (points in strings) removed. The 1st column (index = 0) and last
     * column (index = get(0).size()-1) are always retained. If there are less than 3
     * columns in the array, then a new array is returned that contains the same columns
     * as this array.
     *
     * @return A new PointArray that is identical to this array, but with every other
     * column (points in the strings) removed.
     * @throws IndexOutOfBoundsException if the number of points is not the same in each
     * string in this array.
     */
    public PointArray<E> thinColumns() throws IndexOutOfBoundsException {

        //  Check for string consistancy in this array.
        checkStringConsistancy();

        PointArray<E> list = PointArray.newInstance();
        copyState(list);

        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString<E> str = this.get(i);
            list.add(str.thin());
        }

        return list;
    }

    /**
     * Returns a new {@link PointArray} that is identical to this array but with new rows
     * (strings of points) and columns (points in each string) of points linearly
     * interpolated between each of the existing rows and columns. If there are less than
     * 2 columns in the array, then a new array is returned that contains the one column
     * in this array. If there are less than 2 points in each string of the array, then a
     * new array is returned that contains the one point in each string in this array.
     *
     * @return A new PointArray with new rows and columns linearly interpolated between
     * each of the existing rows and columns.
     * @throws IndexOutOfBoundsException if the number of points is not the same in each
     * string in this array.
     */
    public PointArray<E> enrich() throws IndexOutOfBoundsException {
        PointArray<E> arr1 = enrichColumns();
        PointArray<E> arr2 = arr1.enrichRows();
        PointArray.recycle(arr1);
        return arr2;
    }

    /**
     * Returns a new {@link PointArray} that is identical to this array but with
     * a new row (string of points) linearly interpolated between each of the existing
     * rows. If there are less than 2 rows in the array, then a new array
     * is returned that contains the one row in this array.
     *
     * @return A new PointArray with new rows linearly interpolated between
     *  each of the existing rows.
     * @throws IndexOutOfBoundsException if the number of points is not the same
     *  in each string in this array.
     */
    public PointArray<E> enrichRows() throws IndexOutOfBoundsException {

        //  Check for string consistancy in this array.
        checkStringConsistancy();

        PointArray list = PointArray.newInstance();
        copyState(list);

        int size = size();
        if (size < 2) {
            list.add(get(0));
            return list;
        }

        PointString<? extends GeomPoint> str1 = get(0);
        list.add(str1);
        int numPnts = str1.size();  //  Number of points in each string.

        for (int i = 1; i < size; ++i) {
            PointString<? extends GeomPoint> str2 = get(i);
            PointString stra = PointString.newInstance();
            for (int j = 0; j < numPnts; ++j) {
                GeomPoint p1 = str1.get(j);
                GeomPoint p2 = str2.get(j);
                Point pa = p1.plus(p2).divide(2);   //  Calculate the average point.
                stra.add(pa);
            }
            list.add(stra);
            list.add(str2);
            str1 = str2;
        }

        return list;
    }

    /**
     * Returns a new {@link PointArray} that is identical to this array but with
     * a new column (point) linearly interpolated between each point of the existing
     * strings. If there are less than 2 points in each string of the array,
     * then a new array is returned that contains the one point in each string
     * in this array.
     *
     * @return A new PointArray with new column linearly interpolated between
     *  each of the existing columns.
     * @throws IndexOutOfBoundsException if the number of points is not the same
     *  in each string in this array.
     */
    public PointArray<E> enrichColumns() throws IndexOutOfBoundsException {

        //  Check for string consistancy in this array.
        checkStringConsistancy();

        PointArray list = PointArray.newInstance();
        copyState(list);

        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString str = this.get(i);
            list.add(str.enrich());
        }

        return list;
    }

    /**
     * Return a TriangleList containing a simple triangulation of the input array of
     * quadrilateral panels. The input panels are triangulated by dividing each quad,
     * formed by two adjacent strings of points, into two triangles. Any subranges or
     * transforms on the input points is lost (using copyToReal()) by this triangulation.
     *
     * @return A new TriangleList containing a triangulation of the this array of
     * quadrilateral panels.
     * @see #copyToReal() 
     */
    public TriangleList<Triangle> triangulate() {
        return TriangleList.triangulateQuads(this);
    }

    /**
     * Return the equivalent of this list converted to the specified number of
     * physical dimensions. If the number of dimensions is greater than this
     * element, then zeros are added to the additional dimensions. If the number
     * of dimensions is less than this element, then the extra dimensions are
     * simply dropped (truncated). If the new dimensions are the same as the
     * dimension of this element, then this list is simply returned.
     *
     * @param newDim The dimension of the element to return.
     * @return The equivalent of this list converted to the new dimensions.
     */
    @Override
    public PointArray<E> toDimension(int newDim) {
        if (getPhyDimension() == newDim)
            return this;
        PointArray<E> newList = PointArray.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString element = this.get(i);
            newList.add(element.toDimension(newDim));
        }
        return newList;
    }

    /**
     * Returns the equivalent to this list but with <I>all</I> the elements stated in the
     * specified unit.
     *
     * @param unit the length unit of the list to be returned. May not be null.
     * @return an equivalent to this list but stated in the specified unit.
     * @throws ConversionException if the the input unit is not a length unit.
     */
    @Override
    public PointArray to(Unit<Length> unit) {
        requireNonNull(unit);
        PointArray list = PointArray.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString e = this.get(i);
            list.add(e.to(unit));
        }
        return list;
    }

    /**
     * Returns a copy of this <code>PointArray</code> instance
     * {@link javolution.context.AllocatorContext allocated} by the calling
     * thread (possibly on the stack).
     *
     * @return an identical and independent copy of this object.
     */
    @Override
    public PointArray<E> copy() {
        return copyOf(this);
    }

    /**
     * Return a copy of this object with any transformations or subranges
     * removed (applied).
     *
     * @return A copy of this list with any sub-element transformations or
     * subranges removed.
     */
    @Override
    public PointArray copyToReal() {
        PointArray newList = PointArray.newInstance();
        copyState(newList);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString element = this.get(i);
            newList.add(element.copyToReal());
        }
        return newList;
    }

    /**
     * Return the total number of quadrilateral panels in this array of points.
     *
     * @return the total number of panels in this array.
     * @throws IndexOutOfBoundsException if the strings in this array have
     * different lengths.
     */
    public int getNumberOfPanels() throws IndexOutOfBoundsException {
        int numStrings = size();
        if (numStrings < 2)
            return 0;

        //  Check for string consistancy in this array.
        checkStringConsistancy();

        int numPointsPerString = get(0).size();
        if (numPointsPerString < 2)
            return 0;

        return (numStrings - 1) * (numPointsPerString - 1);
    }

    /**
     * Return the total surface area of all the panels formed by this array of
     * points.
     *
     * @return The total surface area of all the panels formed by this array of
     * points.
     */
    public Parameter<Area> getArea() {
        //  Break the array up into triangle strips and sum the area of all the triangles.
        //  This code is essentially a repeat of TriangleList.getArea(), but is faster
        //  since it doesn't create as much garbage.

        StackContext.enter();
        try {
            //  Check for degenerate cases.
            Unit<Area> areaUnit = getUnit().pow(2).asType(Area.class);
            Parameter<Area> area = Parameter.ZERO_AREA.to(areaUnit);
            int numStr = size();
            if (numStr < 2)
                return StackContext.outerCopy(area);
            int numPnts = get(0).size();
            if (numPnts < 2)
                return StackContext.outerCopy(area);

            int numPntsm1 = numPnts - 1;
            int numStrm1 = numStr - 1;
            for (int i = 0; i < numStrm1; ++i) {
                PointString<E> str1 = get(i);
                PointString<E> str2 = get(i + 1);
                for (int j = 0; j < numPntsm1; ++j) {
                    GeomPoint p1 = str1.get(j);
                    GeomPoint p2 = str1.get(j + 1);
                    GeomPoint p3 = str2.get(j);
                    GeomPoint p4 = str2.get(j + 1);

                    //  Triange #1
                    Parameter<Length> a = p1.distance(p2);
                    Parameter<Length> b = p2.distance(p4);
                    Parameter<Area> dA = a.times(b).asType(Area.class);
                    area = area.plus(dA);

                    //  Triange #2
                    a = p3.distance(p4);
                    b = p3.distance(p1);
                    dA = a.times(b).asType(Area.class);
                    area = area.plus(dA);
                }
            }

            return StackContext.outerCopy(area.times(0.5));

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns transformed version of this element. The returned object
     * implements {@link GeomTransform} and contains transformed versions of the
     * contents of this list as children.
     *
     * @param transform The transform to apply to this geometry element. May not be null.
     * @return A transformed version of this geometry element.
     * @throws DimensionException if this element is not 3D.
     */
    @Override
    public PointArray getTransformed(GTransform transform) {
        requireNonNull(transform);
        PointArray list = PointArray.newInstance();
        copyState(list);
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            PointString element = this.get(i);
            list.add(element.getTransformed(transform));
        }
        return list;
    }

    /**
     * Replaces the {@link PointString} at the specified position in this list with the
     * specified element. Null elements are ignored. The input element must have the same
     * physical dimensions as the other items in this list, or an exception is thrown.
     *
     * @param index   The index of the element to replace (0 returns the 1st element, -1
     *                returns the last, -2 returns the 2nd from last, etc).
     * @param element The element to be stored at the specified position.
     *                <code>null</code> elements are ignored.
     * @return The element previously at the specified position in this list. May not be
     *         null.
     * @throws java.lang.IndexOutOfBoundsException - if <code>index > size()</code>
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public PointString<E> set(int index, PointString<E> element) {
        return super.set(index, requireNonNull(element));
    }

    /**
     * Inserts the specified {@link PointString} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored. The input
     * value must have the same physical dimensions as the other items in this list, or
     * an exception is thrown.
     * <p>
     * Note: If this method is used concurrent access must be synchronized (the list is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted. (0 returns
     *              the 1st element, -1 returns the last, -2 returns the 2nd from last,
     *              etc).
     * @param value the element to be inserted. May not be null.
     * @throws IndexOutOfBoundsException if <code>index > size()</code>
     * @throws DimensionException if the input value dimensions are different from
     * this list's dimensions.
     */
    @Override
    public void add(int index, PointString<E> value) {
        super.add(index, requireNonNull(value));
    }

    /**
     * Inserts all of the {@link PointString} objects in the specified collection into
     * this list at the specified position. Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (increases their indices). The
     * new elements will appear in this list in the order that they are returned by the
     * specified collection's iterator. The behavior of this operation is unspecified if
     * the specified collection is modified while the operation is in progress. Note that
     * this will occur if the specified collection is this list, and it's nonempty.  The
     * input elements must have the same physical dimensions as the other items in this
     * list, or an exception is thrown.
     *
     * @param index index at which to insert first element from the specified collection.
     * @param c     elements to be inserted into this collection. May not be null.
     * @return <code>true</code> if this collection changed as a result of the call.
     * @throws DimensionException if the input element's dimensions are different from
     * this list's dimensions.
     */
    @Override
    public boolean addAll(int index, Collection<? extends PointString<E>> c) {
        int thisSize = this.size();
        for (Object element : c) {
            requireNonNull(element, RESOURCES.getString("collectionElementsNullErr"));
            if (!(element instanceof PointString))
                throw new ClassCastException(MessageFormat.format(
                        RESOURCES.getString("listElementTypeErr"), "PointArray", "PointString"));
            if (thisSize != 0) {
                if (((GeomElement)element).getPhyDimension() != this.getPhyDimension())
                    throw new DimensionException(RESOURCES.getString("dimensionErr"));
            }
        }
        return super.addAll(index, c);
    }

    /**
     * Determines if each string in this array has the same number of points and
     * throws an exception if they do not.
     */
    private void checkStringConsistancy() throws IndexOutOfBoundsException {

        FastTable<PointString<E>> tlist = getList();
        int numPointsPerString = tlist.get(0).size();
        int numStrings = tlist.size();
        for (int i = 0; i < numStrings; ++i) {
            PointString str = tlist.get(i);
            if (str.size() != numPointsPerString)
                throw new IndexOutOfBoundsException(
                        MessageFormat.format(RESOURCES.getString("arrStringSizeErr"), i));
        }

    }

    /**
     * Holds the default XML representation for this object.
     */
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    protected static final XMLFormat<PointArray> XML = new XMLFormat<PointArray>(PointArray.class) {

        @Override
        public PointArray newInstance(Class<PointArray> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            PointArray obj = PointArray.newInstance();
            return obj;
        }

        @Override
        public void read(XMLFormat.InputElement xml, PointArray obj) throws XMLStreamException {
            AbstractPointGeomList.XML.read(xml, obj);     // Call parent read.
        }

        @Override
        public void write(PointArray obj, XMLFormat.OutputElement xml) throws XMLStreamException {
            AbstractPointGeomList.XML.write(obj, xml);    // Call parent write.
        }
    };

    //////////////////////
    // Factory Creation //
    //////////////////////
    private static final ObjectFactory<PointArray> FACTORY = new ObjectFactory<PointArray>() {
        @Override
        protected PointArray create() {
            return new PointArray();
        }

        @Override
        protected void cleanup(PointArray obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(PointArray instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected PointArray() {
    }

    private static <E2 extends GeomPoint> PointArray<E2> copyOf(PointArray<E2> original) {
        PointArray<E2> o = PointArray.newInstance();
        original.copyState(o);
        int size = original.size();
        for (int i = 0; i < size; ++i) {
            PointString<E2> element = original.get(i);
            o.add(element.copy());
        }
        return o;
    }

    /**
     * Tests the methods in this class.
     *
     * @param args  Command-line arguments (ignored).
     */
    public static void main(String args[]) {
        System.out.println("Testing PointArray:");

        Point p1 = Point.valueOf(0, 0, 0, NonSI.FOOT);
        Point p2 = Point.valueOf(1, 0, 0, NonSI.FOOT);
        Point p3 = Point.valueOf(2, 0, 0, NonSI.FOOT);
        Point p4 = Point.valueOf(3, 0, 0, NonSI.FOOT);
        PointString str1 = PointString.valueOf("String 1", p1, p2, p3, p4);
        System.out.println("str1 = " + str1);

        GTransform T1 = GTransform.newTranslation(Vector.valueOf(NonSI.FOOT, 0, 1, 0));
        PointString str2 = str1.getTransformed(T1);
        str2.setName("String 2");
        System.out.println("str2 = " + str2);

        PointString str3 = str2.getTransformed(T1);
        str3.setName("String 3");
        System.out.println("str3 = " + str3);

        PointString str4 = str3.getTransformed(T1);
        str4.setName("String 4");
        System.out.println("str4 = " + str4);

        PointArray arr1 = PointArray.valueOf("An array", str1, str2, str3, str4);
        System.out.println("arr1 = " + arr1);
        System.out.println("arr1.getPhyDimension() = " + arr1.getPhyDimension());
        System.out.println("arr1.size()         = " + arr1.size());

        System.out.println("arr1.getBoundsMin() = " + arr1.getBoundsMin());
        System.out.println("arr1.getBoundsMax() = " + arr1.getBoundsMax());

        GTransform T2 = GTransform.newRotationX(Parameter.valueOf(90, NonSI.DEGREE_ANGLE));
        PointArray arr2 = arr1.getTransformed(T2);
        System.out.println("\nRotate about X by 90 deg:");
        System.out.println("arr2 = " + arr2);
        System.out.println("arr2.getBoundsMin() = " + arr2.getBoundsMin());
        System.out.println("arr2.getBoundsMax() = " + arr2.getBoundsMax());

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new GeomXMLBinding();

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.setReferenceResolver(new XMLReferenceResolver()); // Enables cross-references.
            writer.write(arr1, "PointArray", PointArray.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
