/**
 * Please feel free to use any fragment of the code in this file that you need in your own
 * work. As far as I am concerned, it's in the public domain. No permission is necessary
 * or required. Credit is always appreciated if you use a large chunk or base a
 * significant product on one of my examples, but that's not required either.
 * 
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * 
 * --- Joseph A. Huwaldt
 */
package jahuwaldt.io;

import java.io.*;
import static java.util.Objects.requireNonNull;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * This is a utility class of static methods for working with ZIP archive files.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Behrouz Fallahi, Date: April 28, 2000
 * @version September 16, 2016
 */
public final class ZipUtils {
    //  This class now requires Java 1.7 or later!
    
    /**
     * A list of characters that are illegal on some of the file systems supported by this
     * program. The characters in this list include:
     * \n,\r,\t,\0,\f,',?,*,<,>,|,",:,~,@,!,#,[,],=,+,;, and ','.
     */
    public static final char[] ILLEGAL_CHARACTERS
            = {'\n', '\r', '\t', '\0', '\f', '`', '?', '*', '<', '>', '|', '\"', ':', '~', '@', '!', '#',
                '[', ']', '=', '+', ';', ','};

    private static final int BUFFER_SIZE = 2048;

    /**
     * Prevent instantiation of this utility class.
     */
    private ZipUtils() {
    }
    
    /**
     * Replace any potentially illegal characters from a file name with '_'.
     *
     * @param name The file name to be cleaned of potentially illegal characters. May not
     *             be null.
     * @return The input file name with potentially illegal characters replaced with "_".
     */
    public static String cleanFileName(String name) {
        if (name.length() == 0)
            return name;
        for (char c : ILLEGAL_CHARACTERS) {
            name = name.replace(c, '_');
        }
        return name;
    }

    /**
     * Returns true if the supplied file name contains characters that are illegal on some
     * file systems.
     *
     * @param name The file name to be checked for potentially illegal characters. May not
     *             be null.
     * @return true if the file name contains potentially illegal characters, false if it
     *         is safe.
     */
    public static boolean hasIllegalChars(String name) {
        if (name.length() == 0)
            return false;
        for (char c : ILLEGAL_CHARACTERS) {
            if (name.indexOf(c) >= 0)
                return true;
        }
        return false;
    }

    /**
     * Write a ZIP archive to the specified output stream made up of all the contents of
     * the specified file or directory, using the specified compression method. If any of
     * the files in the specified directory contain characters that are illegal on some
     * file systems, then those files are skipped and not included in the archive. The
     * output stream is closed by this method.
     *
     * @param fileOrDir The file or directory to be compressed into a ZIP archive. May not
     *                  be null.
     * @param out       The output stream to write the ZIP archive data to. May not be
     *                  null.
     * @param zipLevel  The compression level (0-9) or java.util.zip.Deflater.DEFAULT_COMPRESSION.
     * @throws java.io.IOException If there is any problem writing out the ZIP stream.
     * @see java.util.zip.Deflater.DEFAULT_COMPRESSION
     */
    public static void writeZip(File fileOrDir, OutputStream out, int zipLevel) throws IOException {
        requireNonNull(fileOrDir, "fileOrDir == null");
        requireNonNull(out, "out == null");

        //  Create a ZIP output stream to the specified output file.
        try (ZipOutputStream zos = new ZipOutputStream(out)) {
            zos.setLevel(zipLevel);

            if (fileOrDir.isDirectory())
                addDir(null, fileOrDir, zos, new byte[BUFFER_SIZE]);
            else
                addFile(null, fileOrDir, zos, new byte[BUFFER_SIZE]);
        }
    }

    /**
     * Create a ZIP archive file made up of all the contents of the specified file or
     * directory, using the specified compression method. If any of the files in the
     * specified directory contain characters that are illegal on some file systems, then
     * those files are skipped and not included in the archive.
     *
     * @param fileOrDir The file or directory to be compressed into a ZIP archive. May not
     *                  be null.
     * @param zipFile   The ZIP file to be written out. May not be null.
     * @param zipLevel  The compression level (0-9) or
     *                  java.util.zip.ZipOutputStream.DEFAULT_COMPRESSION.
     * @throws java.io.IOException If there is any problem writing out the ZIP file.
     * @see java.util.zip.ZipOutputStream.DEFAULT_COMPRESSION
     */
    public static void writeZip(File fileOrDir, File zipFile, int zipLevel) throws IOException {
        requireNonNull(fileOrDir, "fileOrDir == null");
        requireNonNull(zipFile, "zipFile == null");
        try (FileOutputStream out = new FileOutputStream(zipFile)) {
            writeZip(fileOrDir, out, zipLevel);
        }
    }

    /**
     * Create a ZIP archive file made up of all the contents of the specified file or
     * directory. If any of the files in the specified directory contain characters that
     * are illegal on some file systems, then those files are skipped and not included in
     * the archive.
     *
     * @param fileOrDir The file or directory to be compressed into a ZIP archive. May not
     *                  be null.
     * @param zipFile   The ZIP file to be written out. May not be null.
     * @throws java.io.IOException If there is any problem writing out the ZIP file.
     */
    public static void writeZip(File fileOrDir, File zipFile) throws IOException {
        requireNonNull(fileOrDir, "fileOrDir == null");
        requireNonNull(zipFile, "zipFile == null");
        writeZip(fileOrDir, zipFile, java.util.zip.Deflater.DEFAULT_COMPRESSION);
    }

    /**
     * Extracts a ZIP archive file to the specified directory. If the ZIP archive contains
     * files with characters that might be illegal on some file systems, those characters
     * are replaced with underline characters, '_'.
     *
     * @param input  An InputStream from a ZIP archive. May not be null.
     * @param outDir The directory to extract the ZIP file into. May not be null.
     * @throws java.io.IOException If there is any problem extracting from the ZIP stream.
     */
    public static void extractZip(InputStream input, File outDir) throws IOException {
        requireNonNull(input, "input == null");
        requireNonNull(outDir, "outDir == null");

        try (ZipInputStream zis = new ZipInputStream(input)) {
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                //  Sanitize the file name to remove any potentially illegal characters.
                String name = cleanFileName(entry.getName());

                if (entry.isDirectory()) {
                    //  Create the directory.
                    File d = new File(outDir, name);
                    if (!d.exists())
                        d.mkdir();

                } else {
                    //  Make sure that the directory for the file exists.
                    File file = new File(outDir, name);
                    File dir = file.getParentFile();
                    if (dir != null && !dir.exists())
                        dir.mkdir();

                    //  Write out the file.
                    try (FileOutputStream fos = new FileOutputStream(file)) {
                        byte buffer[] = new byte[BUFFER_SIZE];
                        int count;
                        while ((count = zis.read(buffer)) != -1)
                            fos.write(buffer, 0, count);
                    }
                }
            }
        }

    }

    /**
     * Extracts a ZIP archive file to the specified directory. If the ZIP archive contains
     * files with characters that might be illegal on some file systems, those characters
     * are replaced with underline characters, '_'.
     *
     *
     * @param zipFile The ZIP file to be extracted. May not be null.
     * @param outDir  The directory to extract the ZIP file into. May not be null.
     * @throws java.io.IOException If there is any problem extracting the ZIP file.
     */
    public static void extractZip(File zipFile, File outDir) throws IOException {
        requireNonNull(zipFile, "zipFile == null");
        requireNonNull(outDir, "outDir == null");

        try (FileInputStream fis = new FileInputStream(zipFile)) {
            extractZip(fis, outDir);
        }
    }

    /**
     * Add the specified directory, and all of it's contents, to the specified ZIP archive
     * file.
     *
     * @param rootPath The path, in the ZIP archive, of the directory containing this
     *                 directory (including the trailing "/").  <code>null</code> or ""
     *                 will place this directory at the top of the ZIP archive.
     * @param dir      The directory to be added to the ZIP archive.
     * @param zos      An output stream pointing to the ZIP archive file.
     * @param buffer   A byte buffer used as temporary storage to read/write the data.
     */
    private static void addDir(String rootPath, File dir, ZipOutputStream zos, byte[] buffer) throws IOException {
        if (rootPath == null)
            rootPath = "";

        // Get a listing of the directory contents
        File[] dirList = dir.listFiles();

        // Loop through the directory listing, and zip all the files.
        for (File f : dirList) {
            if (f.isDirectory()) {
                // If the File object is a directory, call this
                // function again to add its content recursively.

                // Create a new zip entry for a directory (they end in "/").
                rootPath += cleanFileName(f.getName()) + "/";
                ZipEntry anEntry = new ZipEntry(rootPath);

                // Place the zip entry in the ZipOutputStream object
                zos.putNextEntry(anEntry);

                //  Call this method again on the sub-directory.
                addDir(rootPath, f, zos, buffer);

            } else {
                // If we reached here, the File object f was not a directory
                addFile(rootPath, f, zos, buffer);
            }
        }

    }

    /**
     * Add the specified file to the specified ZIP archive. Files with character names
     * that may be illegal on some file systems will be skipped.
     *
     * @param rootPath The path, in the ZIP archive, of the directory containing this file
     *                 (including the trailing "/").  <code>null</code> or "" will place
     *                 this file at the top of the ZIP archive.
     *
     * @param file     The file to be added to the ZIP archive.
     * @param zos      An output stream pointing to the ZIP archive file.
     * @param buffer   A byte buffer used as temporary storage to read/write the data.
     */
    private static void addFile(String rootPath, File file, ZipOutputStream zos, byte[] buffer) throws IOException {

        // If the file name contains illegal characters, just skip it.
        if (hasIllegalChars(file.getName()))
            return;

        // Create an input stream from the file.
        try (FileInputStream fis = new FileInputStream(file)) {
            // Create a new zip entry
            if (rootPath == null)
                rootPath = "";
            ZipEntry anEntry = new ZipEntry(rootPath + file.getName());

            // Place the zip entry in the ZipOutputStream object
            zos.putNextEntry(anEntry);

            // Now write the content of the file to the ZipOutputStream
            int bytesIn;
            while ((bytesIn = fis.read(buffer)) != -1)
                zos.write(buffer, 0, bytesIn);
        }

    }

}
