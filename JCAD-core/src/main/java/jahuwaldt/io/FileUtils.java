/**
 * Please feel free to use any fragment of the code in this file that you need in your own
 * work. As far as I am concerned, it's in the public domain. No permission is necessary
 * or required. Credit is always appreciated if you use a large chunk or base a
 * significant product on one of my examples, but that's not required either.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 *
 * --- Joseph A. Huwaldt
 */
package jahuwaldt.io;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * This is a utility class of static methods for working with files.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: November 27, 2009
 * @version May 17, 2017
 */
public final class FileUtils {
    //  This class now requires Java 1.7 or later!

    /**
     * A list of characters that are illegal on some of the file systems supported by this
     * program. The characters in this list include:
     * \n,\r,\t,\0,\f,',?,*,<,>,|,",:,~,@,!,#,[,],=,+,;, and ','.
     */
    private static final char[] ILLEGAL_CHARACTERS
            = {'\n', '\r', '\t', '\0', '\f', '`', '?', '*', '<', '>', '|', '\"', ':', '~', '@', '!', '#',
                '[', ']', '=', '+', ';', ','};

    /**
     * Prevent instantiation of this utility class.
     */
    private FileUtils() {
    }

    /**
     * Replace any potentially illegal characters from a file name with '_'.
     *
     * @param name The file name to be cleaned of potentially illegal characters. May not
     *             be null.
     * @return The input file name with potentially illegal characters replaced with "_".
     */
    public static String cleanFileName(String name) {
        if (name.length() == 0)
            return name;
        for (char c : ILLEGAL_CHARACTERS) {
            name = name.replace(c, '_');
        }
        return name;
    }

    /**
     * Returns true if the supplied file name contains characters that are illegal on some
     * file systems.
     *
     * @param name The file name to be checked for potentially illegal characters. May not
     *             be null.
     * @return true if the file name contains potentially illegal characters, false if it
     *         is safe.
     */
    public static boolean hasIllegalChars(String name) {
        if (name.length() == 0)
            return false;
        for (char c : ILLEGAL_CHARACTERS) {
            if (name.indexOf(c) >= 0)
                return true;
        }
        return false;
    }

    /**
     * Return the file name of the specified file without the extension.
     *
     * @param file The file to have the name without extension returned. May not be null.
     * @return The name of the specified file without the extension (if there is one).
     */
    public static String getFileNameWithoutExtension(File file) {
        String name = file.getName();
        return getFileNameWithoutExtension(name);
    }

    /**
     * Return the file name of the specified file without the extension.
     *
     * @param name The file name to have the name without extension returned. May not be
     *             null.
     * @return The name of the specified file without the extension (if there is one).
     */
    public static String getFileNameWithoutExtension(String name) {
        int index = name.lastIndexOf('.');
        if (index > 0 && index <= name.length() - 2) {
            name = name.substring(0, index);
        }
        return name;
    }

    /**
     * Return the extension portion of the file's name. The extension will always be
     * returned in lower case and without the ".".
     *
     * @param file The file for which the extension is to be returned. May not be null.
     * @return The extension portion of the file's name without the "." or "" if there is
     *         no extension.
     * @see #getExtension(java.lang.String) 
     */
    public static String getExtension(File file) {
        requireNonNull(file, "file == null");
        return getExtension(file.getName());
    }

    /**
     * Return the extension portion of the file's name. The extension will always be
     * returned in lower case and without the ".".
     *
     * @param name The name of the file, including the extension. May not be null.
     * @return The extension portion of the file's name without the "." or "" if there is
     *         no extension.
     * @see #getExtension(java.io.File) 
     */
    public static String getExtension(String name) {
        int i = name.lastIndexOf('.');
        if (i > 0 && i < name.length() - 1)
            return name.substring(i + 1).toLowerCase();
        return "";
    }

    /**
     * Copy a file from the source to the destination locations.
     *
     * @param src The source file. May not be null.
     * @param dst The destination file to copy the source file to. May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * files.
     */
    public static void copy(File src, File dst) throws IOException {
        requireNonNull(src, "src == null");
        requireNonNull(dst, "dst == null");

        // If src and dst are the same; hence no copying is required.
        if (sameFile(src, dst))
            return;

        InputStream in = null;
        OutputStream out = null;
        try {

            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
            copy(in, out);

        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }
    }

    /**
     * Copy the input stream to the output stream.
     *
     * @param in  The source input stream. May not be null.
     * @param out The destination output stream. May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * streams.
     */
    public static void copy(InputStream in, OutputStream out) throws IOException {
        requireNonNull(in, "in == null");
        requireNonNull(out, "out == null");

        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
    }

    /**
     * Recursively copy the contents of an entire directory tree from source to
     * destination. This also works if a single file is passed as the source and
     * destination.
     *
     * @param source      The source directory or file to copy. May not be null.
     * @param destination The destination directory or file. May not be null.
     * @throws java.io.IOException If there is any problem copying the directories or
     * files.
     */
    public static void copyDirectory(File source, File destination) throws IOException {
        if (source.isDirectory()) {
            if (!destination.exists()) {
                boolean success = destination.mkdirs();
                if (!success)
                    throw new IOException("Could not create directory " + destination);
            }

            String files[] = source.list();

            for (String file : files) {
                File srcFile = new File(source, file);
                File destFile = new File(destination, file);

                copyDirectory(srcFile, destFile);
            }
        } else {
            copy(source, destination);
        }
    }

    /**
     * Attempt to rename a file from the source File location to the destination File
     * location. If the standard atomic rename fails, this method falls back on copying
     * the file which is dangerous as it is not atomic. The fall back will not work if the
     * source or destination is a directory and an exception is thrown in that case.
     *
     * @param src The source file to be renamed. Upon exit, this object MAY point to the
     *            same location as "dst" or it may be left unchanged. Either way, barring
     *            an exception, the file will no longer exist at the input src path
     *            location. May not be null.
     * @param dst The destination path to rename the source file to. May not be null.
     * @throws java.io.IOException if there is any problem renaming the file.
     */
    public static void rename(File src, File dst) throws IOException {
        requireNonNull(dst, "dst == null");

        if (!src.exists())
            throw new FileNotFoundException("\"" + src.getPath() + "\" not found.");

        //  If the source and destination are the same, no rename is required.
        if (sameFile(src, dst))
            return;

        //  Make sure we can write to the destination location.
        if (dst.exists()) {
            //  The file already exists, can we write to it?
            if (!dst.canWrite())
                throw new IOException("Can not write to \"" + dst.getPath() + "\".");

        } else {
            //  The file does not already exist.  Can we create a file here?
            boolean success = dst.createNewFile();
            if (!success)
                throw new IOException("Can not write to \"" + dst.getPath() + "\".");
            dst.delete();
        }

        try {
            //  Try to use the atomic rename first.
            boolean success = src.renameTo(dst);
            if (!success) {
                //  Delete the destination file, if it exists.  Then wait a second and try again.
                if (dst.exists())
                    dst.delete();
                Thread.sleep(1000);
                success = src.renameTo(dst);

                if (!success) {
                    //  If renaming the file doesn't work, fall back on copying it.
                    //  This doesn't work for directories.
                    if (!src.isFile())
                        throw new IOException("Move failed, source is dir: \"" + src.getPath() + "\".");
                    if (dst.isDirectory())
                        throw new IOException("Move failed, destination is dir: \"" + dst.getPath() + "\".");
                    copy(src, dst);
                    src.delete();
                }
            }
        } catch (InterruptedException e) {
            throw new InterruptedIOException();
        }
    }

    /**
     * Returns a list of String objects each of which represents a line in the specified
     * file. The file may optionally be GZIP compressed.
     *
     * @param file The possibly GZIP compressed file to be read in. May not be null.
     * @return A list of String objects, one for each line in the specified file.
     * @throws java.io.IOException if there is any problem reading from the file.
     */
    public static List<String> readlines(File file) throws IOException {
        requireNonNull(file, "file == null");
        FileInputStream instream = new FileInputStream(file);
        return readlines(instream);
    }

    /**
     * Returns a list of String objects each of which represents a line in the specified
     * input stream. The input stream may optionally be GZIP compressed.
     *
     * @param instream The input stream to be read in. May optionally be GZIP compressed.
     *                 May not be null.
     * @return A list of String objects, one for each line in the specified input stream.
     * @throws java.io.IOException if there is any problem reading from the stream.
     */
    public static List<String> readlines(InputStream instream) throws IOException {
        requireNonNull(instream, "instream == null");
        
        //  Deal with the possibility that the input stream is GZIP compressed.
        InputStream in = new BufferedInputStream(instream);
        if (isGZIPCompressed((BufferedInputStream)in))
            in = new GZIPInputStream(in);
        
        List<String> output = new ArrayList();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String aLine;
            do {
                aLine = reader.readLine();
                if (aLine != null)
                    output.add(aLine);

            } while (aLine != null);

        }

        return output;
    }

    /**
     * Write out a list of String objects which represent a line to the specified output
     * stream. A line ending character is appended to each line.
     *
     * @param outFile The output file to be written to. May not be null.
     * @param lines   The list of String objects to be written out. May not be null.
     * @throws IOException if there is any problem writing to the output file.
     */
    public static void writelines(File outFile, List<String> lines) throws IOException {
        requireNonNull(outFile, "outFile == null");
        requireNonNull(lines, "lines ==  null");

        try (FileOutputStream outstream = new FileOutputStream(outFile)) {
            writelines(outstream, lines);
        }
    }

    /**
     * Write out a list of String objects which represent a line to the specified output
     * stream. A line ending character is appended to each line.
     *
     * @param outstream The output stream to be written to. May not be null.
     * @param lines     The list of String objects to be written out. May not be null.
     * @throws IOException if there is any problem writing to the output stream.
     */
    public static void writelines(OutputStream outstream, List<String> lines) throws IOException {
        requireNonNull(outstream, "outstream == null");
        requireNonNull(lines, "lines == null");

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outstream))) {

            //  Write out the lines to the output file.
            int size = lines.size();
            for (int i = 0; i < size; ++i) {
                writer.write(lines.get(i));
                writer.newLine();
            }

        }

    }

    /**
     * Returns a buffer that contains the contents of the specified file.
     *
     * @param file The file to be read into a new ByteBuffer. May not be null.
     * @return The buffer containing the contents of the specified file.
     * @throws java.io.IOException if there is any problem reading from the file.
     */
    public static byte[] file2Buffer(File file) throws IOException {

        //  Read the file into a byte buffer.
        if (file.length() > (long)Integer.MAX_VALUE)
            throw new IOException(file.getName() + " is to large to convert into a ByteBuffer!");

        int fileSize = (int)file.length();
        byte[] buffer = new byte[fileSize];
        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(buffer, 0, fileSize);
        }

        return buffer;
    }

    /**
     * Write the entire contents of a byte array to the specified file.
     *
     * @param buffer The buffer to be written to the file. May not be null.
     * @param file   The file to write the buffer to (it's existing contents will be
     *               overwritten). May not be null.
     * @throws java.io.IOException if there is any problem writing to the file.
     */
    public static void buffer2File(byte[] buffer, File file) throws IOException {
        requireNonNull(buffer, "buffer == null");
        requireNonNull(file, "file == null");

        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(buffer);
        }

    }

    /**
     * Returns a ByteBuffer that contains the contents of the specified file.
     *
     * @param file The file to be read into a new ByteBuffer. May not be null.
     * @return The ByteBuffer containing the contents of the specified file.
     * @throws java.io.IOException if there is any problem reading from the file.
     */
    public static ByteBuffer file2ByteBuffer(File file) throws IOException {

        //  Read the file into a byte buffer.
        if (file.length() > Integer.MAX_VALUE)
            throw new IOException(file.getName() + " is to large to convert into a ByteBuffer!");

        int fileSize = (int)file.length();
        byte[] mybytearray = new byte[fileSize];
        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(mybytearray, 0, fileSize);
        }

        ByteBuffer buffer = ByteBuffer.wrap(mybytearray);

        return buffer;
    }

    /**
     * Write the contents of a ByteBuffer, from the beginning up to the current position,
     * to the specified file.
     *
     * @param buffer The buffer to be written to the file. May not be null.
     * @param file   The file to write the buffer to (it's existing contents will be
     *               overwritten). May not be null.
     * @throws java.io.IOException if there is any problem writing the byte buffer to the
     * file.
     */
    public static void byteBuffer2File(ByteBuffer buffer, File file) throws IOException {
        requireNonNull(file);
        int pos = buffer.position();
        buffer.flip();
        byte[] mybytearray = new byte[buffer.limit()];
        buffer.get(mybytearray);

        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(mybytearray);
        }

        buffer.position(pos);
    }

    /**
     * Read in an optionally GZIP compressed, delimited text file that contains a regular
     * array of Double values. The delimiter may be any whitespace.
     *
     * @param filePath Path to/Name of the possibly GZIP compressed table file being read
     *                 in.
     * @return A 2D double array of the values in the table file.
     * @throws IOException if there is any problem reading from the file or parsing the
     * results.
     */
    public static double[][] loadtxt(String filePath) throws IOException {
        requireNonNull(filePath, "filePath == null");
        return loadtxt(new File(filePath), "\\s+", 0);
    }
    
    /**
     * Read in an optionally GZIP compressed, delimited text file that contains a regular
     * array of Double values. The delimiter may be any whitespace.
     *
     * @param filePath Path to/Name of the possibly GZIP compressed table file being read
     *                 in.
     * @param skiprows Indicates the number of rows to skip at the top of the file (to
     *                 skip over a header for instance).
     * @return A 2D double array of the values in the table file.
     * @throws IOException if there is any problem reading from the file or parsing the
     * results.
     */
    public static double[][] loadtxt(String filePath, int skiprows) throws IOException {
        requireNonNull(filePath, "filePath == null");
        return loadtxt(new File(filePath), "\\s+", skiprows);
    }
    
    /**
     * Read in an optionally GZIP compressed, delimited text file that contains a regular
     * array of Double values. The delimiter may be any whitespace.
     *
     * @param filePath Path to/Name of the possibly GZIP compressed table file being read
     *                 in.
     * @param delimiter The delimiter to use to separate columns (such as "," for commas,
     *                  or "\\s+" for any whitespace.
     * @param skiprows Indicates the number of rows to skip at the top of the file (to
     *                 skip over a header for instance).
     * @return A 2D double array of the values in the table file.
     * @throws IOException if there is any problem reading from the file or parsing the
     * results.
     */
    public static double[][] loadtxt(String filePath, String delimiter, int skiprows) throws IOException {
        requireNonNull(filePath, "filePath == null");
        requireNonNull(delimiter, "delimiter == null");
        return loadtxt(new File(filePath), delimiter, skiprows);
    }
    
    /**
     * Read in an optionally GZIP compressed, delimited text file that contains a regular
     * array of Double values. The delimiter may be any whitespace.
     *
     * @param txtFile  The possibly GZIP compressed table file being read in.
     * @return A 2D double array of the values in the table file.
     * @throws IOException if there is any problem reading from the file or parsing the
     * results.
     */
    public static double[][] loadtxt(File txtFile) throws IOException {
        requireNonNull(txtFile, "txtFile == null");
        return loadtxt(txtFile, "\\s+", 0);
    }
    
    /**
     * Read in an optionally GZIP compressed, delimited text file that contains a regular
     * array of Double values. The delimiter may be any whitespace.
     *
     * @param txtFile  The possibly GZIP compressed table file being read in.
     * @param skiprows  Indicates the number of rows to skip at the top of the file (to
     *                  skip over a header for instance).
     * @return A 2D double array of the values in the table file.
     * @throws IOException if there is any problem reading from the file or parsing the
     * results.
     */
    public static double[][] loadtxt(File txtFile, int skiprows) throws IOException {
        requireNonNull(txtFile, "txtFile == null");
        return loadtxt(txtFile, "\\s+", skiprows);
    }
    
    /**
     * Read in an optionally GZIP compressed, delimited text file that contains a regular
     * array of Double values.
     *
     * @param txtFile   The possibly GZIP compressed table file being read in.
     * @param delimiter The delimiter to use to separate columns (such as "," for commas,
     *                  or "\\s+" for any whitespace.
     * @param skiprows  Indicates the number of rows to skip at the top of the file (to
     *                  skip over a header for instance).
     * @return A 2D double array of the values in the table file.
     * @throws IOException if there is any problem reading from the file or parsing the
     * results.
     */
    public static double[][] loadtxt(File txtFile, String delimiter, int skiprows) throws IOException {
        requireNonNull(txtFile, "txtFile == null");
        requireNonNull(delimiter, "delimiter == null");
        
        //  Deal with bad input to skiprows.
        if (skiprows < 0)
            throw new IllegalArgumentException("skiprows must be >= 0");
        
        //  Read in the lines from the file.
        List<String> lines = readlines(txtFile);
        
        int size = lines.size();
        int numRows = size - skiprows;
        if (numRows < 1)
            return new double[0][0];
        
        double[][] output = new double[numRows][];
        int col_size = -1;
        
        //  Loop over all the rows in the file.
        for (int row=0; row < numRows; ++row) {
            String[] cols = lines.get(row + skiprows).trim().split(delimiter);
            
            if (col_size < 0)
                col_size = cols.length;
            else if (col_size != cols.length)
                throw new IOException("The number of columns on line " + (row+skiprows+1) + 
                        " is different from previous lines.");
            
            //  Allocate memory for this row of the table.
            double[] row_v = new double[col_size];
            output[row] = row_v;
            
            //  Convert all the column strings to floats.
            for (int col=0; col < col_size; ++col) {
                row_v[col] = Double.parseDouble(cols[col]);
            }
        }
        
        return output;
    }
    
    /**
     * Create a temporary directory using the specified prefix.
     *
     * @param prefix The prefix string to be used in generating the file's name; must be
     *               at least three characters long. May not be null.
     * @return a reference to a temporary directory using the specified prefix.
     * @throws java.io.IOException if there is any problem creating the temporary
     * directory.
     */
    public static File createTempDirectory(String prefix) throws IOException {
        requireNonNull(prefix, "prefix == null");

        File tempFile = File.createTempFile(prefix, "", null);
        if (!tempFile.delete() || !tempFile.mkdir())
            throw new IOException("Could not create temporary directory: " + tempFile.getPath());

        return tempFile;
    }

    /**
     * Recursively deletes the directory tree indicated by the specified path. The
     * directory and all of it's contents are deleted.
     *
     * @param path The directory to be deleted. If a plain file is passed in rather than a
     *             directory, it is simply deleted. May not be null.
     * @return true if the directory and it's contents were successfully deleted.
     */
    public static boolean deleteDirectory(File path) {
        if (path.exists() && path.isDirectory()) {
            File[] files = path.listFiles();
            for (File file : files) {
                if (file.isDirectory())
                    deleteDirectory(file);
                else
                    file.delete();
            }
        }
        return (path.delete());
    }

    /**
     * Returns <code>true</code> if and only if the two File objects refer to the same
     * file in the file system.
     *
     * @param f1 The first file to check. May not be null.
     * @param f2 The 2nd file to check with the 1st one. May not be null.
     * @return true if and only if the two File objects refer to the same file in the file
     *         system.
     * @throws IOException If an I/O error occurs, which is possible because the
     * construction of the canonical pathname may require file system queries.
     */
    public static boolean sameFile(File f1, File f2) throws IOException {
        return f1.getCanonicalPath().equals(f2.getCanonicalPath());
    }

    /**
     * GZIP compress the src file writing to the destination file. The source &
     * destination files may be identical.
     *
     * @param src The source file to be compressed. May not be null.
     * @param dst The destination file to compress the source file to. This may be
     *            identical to the source location if the change is to be made in place.
     *            May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * files.
     */
    public static void gzip(File src, File dst) throws IOException {
        requireNonNull(src, "src == null");
        requireNonNull(dst, "dst = null");

        File dst2 = dst;
        if (sameFile(src, dst))
            dst2 = File.createTempFile("gzip", null);

        InputStream in = null;
        OutputStream out = null;
        try {

            in = new FileInputStream(src);
            out = new FileOutputStream(dst2);
            gzip(in, out);

        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();

            if (dst2 != dst) {
                //  If we used a temporary file, move it to the originally
                //  desired location.
                rename(dst2, dst);

                //  Delete the temporary file.
                dst2.delete();
            }
        }
    }

    /**
     * Copy a file to the specified destination directory while GZIP compressing the file.
     * The original file is not modified. The file in the output directory will have the
     * same name as the source file, but with ".gz" appended.
     *
     * @param src     The source file to be compressed. May not be null.
     * @param destDir The directory to copy the compressed file into. May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * files.
     */
    public static void copyAndGzip(File src, File destDir) throws IOException {
        requireNonNull(src, "src == null");
        requireNonNull(destDir, "destDir == null");

        File dst = new File(destDir, src.getName() + ".gz");

        InputStream in = null;
        OutputStream out = null;
        try {

            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
            gzip(in, out);

        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }

    }

    /**
     * Un-GZIP the compressed src file writing the uncompressed data to the destination
     * file. The source & destination files may be identical.
     *
     * @param src The GZIP compressed source file to be de-compressed. May not be null.
     * @param dst The destination file to uncompress the source file to. This may be
     *            identical to the source location if the change is to be made in place.
     *            May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * files.
     */
    public static void ungzip(File src, File dst) throws IOException {
        requireNonNull(src, "src == null");
        requireNonNull(dst, "dst == null");

        File dst2 = dst;
        if (sameFile(src, dst))
            dst2 = File.createTempFile("gzip", null);

        InputStream in = null;
        OutputStream out = null;
        try {

            in = new FileInputStream(src);
            out = new FileOutputStream(dst2);
            ungzip(in, out);

        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();

            if (dst2 != dst) {
                //  If we used a temporary file, move it to the originally
                //  desired location.
                rename(dst2, dst);

                //  Delete the temporary file.
                dst2.delete();
            }
        }
    }

    /**
     * Copy a file to the specified destination directory while decompressing the GZIP
     * file. The source file is not modified. A new file is created in the destination
     * directory that has the same name as the source file, but without the ".gz"
     * extension (if there is one).
     *
     * @param src     The source file GZIP file. May not be null.
     * @param destDir The directory to copy the uncompressed file into. May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * files.
     */
    public static void copyAndUngzip(File src, File destDir) throws IOException {
        requireNonNull(src, "src == null");
        requireNonNull(destDir, "destDir == null");

        String name = src.getName();
        File dst;
        if (name.toLowerCase().endsWith(".gz")) {
            int len = name.length();
            dst = new File(destDir, src.getName().substring(0, len - 3));
        } else
            dst = new File(destDir, name);

        InputStream in = null;
        OutputStream out = null;
        try {

            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
            ungzip(in, out);

        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }
    }

    /**
     * GZIP compress the input stream and write it to the output stream.
     *
     * @param in  The source input stream. May not be null.
     * @param out The destination output stream where GZIP compressed data is to be
     *            written. May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * streams.
     */
    public static void gzip(InputStream in, OutputStream out) throws IOException {
        requireNonNull(in, "in == null");
        requireNonNull(out, "out == null");

        GZIPOutputStream gzipOut = new GZIPOutputStream(out);
        copy(in, gzipOut);
        gzipOut.finish();
    }

    /**
     * Un-GZIP the compressed input stream and write the uncompressed data to the
     * specified output stream.
     *
     * @param in  The source input stream pointing to GZIP compressed data. May not be
     *            null.
     * @param out The destination output stream where uncompressed data is to be written.
     *            May not be null.
     * @throws java.io.IOException if there is any problem reading from or writing to the
     * streams.
     */
    public static void ungzip(InputStream in, OutputStream out) throws IOException {
        requireNonNull(in, "in == null");
        requireNonNull(out, "out == null");

        GZIPInputStream gzipIn = new GZIPInputStream(in);
        copy(gzipIn, out);
    }

    /**
     * Returns <code>true</code> if the specified input file is pointing at a GZIP
     * compressed data set.
     *
     * @param file The input file to be tested. May not be null.
     * @return true if the specified input file is pointing at a GZIP compressed data set.
     * @throws java.io.IOException if there is a problem reading from the specified file.
     */
    public static boolean isGZIPCompressed(File file) throws IOException {
        requireNonNull(file, "file == null");
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(file), 4)) {
            return isGZIPCompressed(is);
        }
    }

    /**
     * Returns <code>true</code> if the specified input stream is pointing at a GZIP
     * compressed data set.
     *
     * @param in The input stream to be tested. May not be null.
     * @return true if the specified input stream is pointing at a GZIP compressed data
     *         set.
     * @throws java.io.IOException if there is a problem reading from the input stream.
     */
    public static boolean isGZIPCompressed(BufferedInputStream in) throws IOException {
        requireNonNull(in, "in == null");
        byte[] signature = new byte[2];

        in.mark(4);
        in.read(signature); //read the signature
        in.reset();

        return isGZIPCompressed(signature);
    }

    /**
     * Returns <code>true</code> if the specified array of bytes represent a GZIP
     * compressed data set.
     *
     * @param bytes the array of bytes to be tested. May not be null.
     * @return true if the specified array of bytes represent a GZIP compressed data set.
     */
    public static boolean isGZIPCompressed(byte[] bytes) {
        if (bytes.length < 2) {
            return false;

        } else {
            return ((bytes[0] == (byte)(GZIPInputStream.GZIP_MAGIC))
                    && (bytes[1] == (byte)(GZIPInputStream.GZIP_MAGIC >> 8)));
        }
    }
}
