/**
 * Please feel free to use any fragment of the code in this file that you need in your own
 * work. As far as I am concerned, it's in the public domain. No permission is necessary
 * or required. Credit is always appreciated if you use a large chunk or base a
 * significant product on one of my examples, but that's not required either.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 *
 * --- Joseph A. Huwaldt
 */
package jahuwaldt.io;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import java.util.StringTokenizer;

/**
 * A convenience implementation of FilenameFilter and FileFilter that filters out all
 * files except for those type extensions that it knows about.
 *
 * Extensions are of the type ".foo", which is typically found on Windows and Unix boxes,
 * but not on the Macintosh prior to OS X. Case is ignored.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: June 18, 2004
 * @version September 16, 2016
 */
public class ExtFilenameFilter implements FilenameFilter, FileFilter {
    //  This class now requires Java 1.7 or later!

    private List<String> filters = null;

    private HashMap<String, ExtFilenameFilter> nameFilters = null;

    private String description = null;

    private String fullDescription = null;

    private boolean useExtensionsInDescription = true;

    /**
     * Creates a filename filter. If no filters are added, then all files are accepted.
     *
     * @see #addExtension(java.lang.String)
     */
    public ExtFilenameFilter() {
        this((String)null, (String)null);
    }

    /**
     * Creates a filename filter that accepts files with the given extension. Example: new
     * ExtFilenameFilter("jpg");
     *
     * @param extension The file name extension to use for this filter. Null extensions
     *                  are ignored.
     * @see #addExtension(java.lang.String)
     */
    public ExtFilenameFilter(String extension) {
        this(extension, null);
    }

    /**
     * Creates a file filter that accepts the given file type. Example:
     * <code>new ExtFilenameFilter("jpg", "JPEG Image Images");</code>
     * <p>
     * Note that the "." before the extension is not needed, but it is fine if it is
     * there.</p>
     *
     * @param extension   The file name extension to use for this filter. Null extensions
     *                    are ignored.
     * @param description A description of the file type of this filter. Null is fine.
     * @see #addExtension(java.lang.String)
     */
    public ExtFilenameFilter(String extension, String description) {
        this(new String[]{extension}, description);
    }

    /**
     * Creates a file filter from the given string array. Example:
     * <code>new ExtFilenameFilter(String {"gif", "jpg"});</code>
     * <p>
     * Note that the "." before the extension is not needed, but it is fine if it is
     * there.</p>
     *
     * @param filters An array of String objects where each entry is a file name extension
     *                to be included in this filter. May not be null.
     * @see #addExtension(java.lang.String)
     */
    public ExtFilenameFilter(String... filters) {
        this(requireNonNull(filters), null);
    }

    /**
     * Creates a file filter from the given string array and description. Example:
     * <code>new ExtFilenameFilter(String {"gif", "jpg"}, "Gif and JPG Images");</code>
     * <p>
     * Note that the "." before the extension is not needed, but it is fine if it is
     * there.</p>
     *
     * @param filters     An array of String objects where each entry is a file name
     *                    extension to be included in this filter. Any null members of the
     *                    array are ignored. The array itself may not be null.
     * @param description The description of the extensions in this filter set. Null is
     *                    fine.
     * @see #addExtension(java.lang.String)
     */
    public ExtFilenameFilter(String[] filters, String description) {
        requireNonNull(filters);
        this.filters = new ArrayList<>();
        for (String filter : filters) {
            // add filters one by one
            if (nonNull(filter))
                addExtension(filter);
        }
        setDescription(description);
        nameFilters = new HashMap<>(4);
    }

    /**
     * Return true if this file should be shown , false if it should not.
     *
     * @param f The file that is to be tested for compatibility with this filter.
     */
    @Override
    public boolean accept(File f) {
        if (nonNull(f)) {
            if (f.isDirectory())
                return true;

            String name = f.getName().toLowerCase();
            if (nonNull(nameFilters.get(name)))
                return true;

            return extensionMatch(name);
        }
        return false;
    }

    /**
     * Return true if this file should be included in a file list, false if it shouldn't.
     *
     * @param dir  The directory in which the file was found.
     * @param name The name of the file.
     */
    @Override
    public boolean accept(File dir, String name) {
        if (nonNull(dir) && nonNull(name)) {

            if (nonNull(nameFilters.get(name)))
                return true;

            return extensionMatch(name);
        }
        return false;
    }

    /**
     * Tests to see if the specified name ends with any of the allow extensions.
     *
     * @param fileName The file name to test.
     * @return <code>true</code> if the file name ends with any of the allowed extensions,
     *         <code>false</code> if it does not. If there are no extensions defined, this
     *         will always return true (all files will match).
     */
    private boolean extensionMatch(String fileName) {
        if (filters.isEmpty())
            return true;

        for (String ext : filters) {
            if (getExtension(fileName).equals(ext))
                return true;
        }

        return false;
    }

    /**
     * Return the extension portion of the file's name. The extension will always be
     * returned in lower case and without the ".".
     *
     * @param name The file name for which the extension is to be returned. May not be
     *             null.
     * @return The extension portion of the file's name without the "." or "" if there is
     *         no extension.
     * @see #getExtension(java.io.File)
     */
    public static String getExtension(String name) {
        int i = name.lastIndexOf('.');
        if (i > 0 && i < name.length() - 1)
            return name.substring(i + 1).toLowerCase().trim();
        return "";
    }

    /**
     * Return the extension portion of the file's name. The extension will always be
     * returned in lower case and without the ".".
     *
     * @param f The file object for which the extension is to be returned. May not be
     *          null.
     * @return The extension portion of the file's name without the "." or "" if there is
     *         no extension.
     * @see #getExtension(java.lang.String)
     */
    public static String getExtension(File f) {
        requireNonNull(f, "f == null");
        return getExtension(f.getName());
    }

    /**
     * Adds a file name "dot" extension to filter against.
     * <p>
     * For example: the following code will create a filter that filters out all files
     * except those that end in ".jpg" and ".tif":
     * <pre>
     * ExtFilenameFilter filter = new ExtFilenameFilter(); filter.addExtension("jpg");
     * filter.addExtension("tif");
     * </pre></p>
     * <p>
     * Note that the "." before the extension is not needed, but it is fine if it is
     * there.
     * </p>
     *
     * @param extension The file name extension to be added to this filter. May not be
     *                  null.
     */
    public final void addExtension(String extension) {
        requireNonNull(extension, "extension == null");

        if (!extension.equals("")) {
            //  Make sure that the extension never starts with a ".".
            while (extension.startsWith("."))
                extension = extension.substring(1);

            //  Store the extension in our database of extensions.
            filters.add(extension.toLowerCase());
        }

        fullDescription = null;
    }

    /**
     * Adds a full filename to filter against.
     * <p>
     * For example: the following code will create a filter that filters out all files
     * except those that end in ".jpg" and ".tif" or have the name "foo.bar":
     * <pre>
     * ExtFilenameFilter filter = new ExtFilenameFilter();
     * filter.addExtension("jpg");
     * filter.addExtension("tif");
     * filter.addFileName("foo.bar");
     * </pre></p>
     *
     * @param fileName A full file name to add to this filter for filtering against. May
     *                 not be null.
     */
    public void addFilename(String fileName) {
        requireNonNull(fileName, "fileName == null");
        if (!fileName.equals("")) {
            nameFilters.put(fileName.toLowerCase(), this);
        }
    }

    /**
     * Adds a list of extensions parsed from a comma, space or tab delimited list.
     * <p>
     * For example, the following will create a filter that filters out all files except
     * those that end in ".jpg" and ".png":
     * <pre>
     *    ExtFilenameFilter filter = new ExtFilenameFilter();
     *    filter.addExtensions("jpg,png,gif");
     * </pre></p>
     *
     * @param extensionList A delimited list of extensions to add to the filter. May not
     *                      be null.
     */
    public void addExtensions(String extensionList) {
        requireNonNull(extensionList, "extensionList == null");
        StringTokenizer tokenizer = new StringTokenizer(extensionList, ", \t");
        while (tokenizer.hasMoreTokens()) {
            addExtension(tokenizer.nextToken());
        }
    }

    /**
     * Returns the human readable description of this filter. For example: "JPEG and GIF
     * Image Files (*.jpg, *.gif)"
     *
     * @return The description of this filter.
     * @see #setDescription(java.lang.String)
     * @see #setExtensionListInDescription(boolean)
     * @see #isExtensionListInDescription()
     */
    public String getDescription() {
        if (isNull(fullDescription)) {
            if (isNull(description) || isExtensionListInDescription()) {
                if (nonNull(description))
                    fullDescription = description;
                fullDescription += " (";

                // build the description from the extension list
                Iterator<String> extensions = filters.iterator();
                if (nonNull(extensions)) {
                    fullDescription += extensions.next().substring(1);
                    while (extensions.hasNext())
                        fullDescription += ", " + extensions.next().substring(1);
                }
                fullDescription += ")";

            } else {
                fullDescription = description;
            }
        }
        return fullDescription;
    }

    /**
     * Sets the human readable description of this filter. For example:
     * filter.setDescription("GIF and JPEG Images");
     *
     * @param description The description to be used for this filter. Null is fine.
     * @see #setDescription(java.lang.String)
     * @see #setExtensionListInDescription(boolean)
     * @see #isExtensionListInDescription()
     */
    public final void setDescription(String description) {
        this.description = description;
        fullDescription = null;
    }

    /**
     * Determines whether the extension list (.jpg,.gif, etc) should show up in the human
     * readable description.
     * <p>
     * Only relevant if a description was provided in the constructor or using
     * <code>setDescription()</code></p>
     *
     * @param useExtInDescription Set to true to use the extension list in the description
     *                            of this filter.
     * @see #getDescription()
     * @see #setDescription(java.lang.String)
     * @see #isExtensionListInDescription()
     */
    public void setExtensionListInDescription(boolean useExtInDescription) {
        useExtensionsInDescription = useExtInDescription;
        fullDescription = null;
    }

    /**
     * Returns whether the extension list (.jpg,.gif, etc) should show up in the human
     * readable description.
     * </p>
     * Only relevant if a description was provided in the constructor or using
     * <code>setDescription()</code></p>
     *
     * @return true if the extension list is a part of the human readable description.
     * @see #getDescription()
     * @see #setDescription(java.lang.String)
     * @see #setExtensionListInDescription(boolean)
     */
    public boolean isExtensionListInDescription() {
        return useExtensionsInDescription;
    }

}
