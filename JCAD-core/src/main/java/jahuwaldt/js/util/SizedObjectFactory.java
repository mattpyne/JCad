/*
*   SizedObjectFactory -- A class that holds factories to produce objects that require variable size inputs.
*
*   Copyright (C) 2011,2012 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.util;

import javolution.context.ObjectFactory;


/**
* <p> This class holds factories to produces objects of variable size/length.
*     It allows for object recycling, pre-allocation and stack allocations:<code>
*
*     static SizedObjectFactory<BinomialCoef> BINOMIAL_FACTORY = new SizedObjectFactory<BinomialCoef> {
*         protected BinomialCoef create(int size) {
*             return new BinomialCoef(size);
*         }
*     };
*     ...
*     BinomialCoef bin = BINOMIAL_FACTORY.array(256);
*     </code></p>
*          
*  @author  Joseph A. Huwaldt   Date: May 11, 2011
*  @version September 15, 2012
**/
public abstract class SizedObjectFactory<T extends SizedObject> {

    /**
     * Holds factory for arrays up to size 4.
     */
    private final ObjectFactory _factory4 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(4);
        }
    };

    /**
     * Holds factory for arrays up to size 8.
     */
    private final ObjectFactory _factory8 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(8);
        }
    };

    /**
     * Holds factory for arrays up to size 16.
     */
    private final ObjectFactory _factory16 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(16);
        }
    };

    /**
     * Holds factory for arrays up to size 32.
     */
    private final ObjectFactory _factory32 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(32);
        }
    };

    /**
     * Holds factory for arrays up to size 64.
     */
    private final ObjectFactory _factory64 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(64);
        }
    };

    /**
     * Holds factory for arrays up to size 128.
     */
    private final ObjectFactory _factory128 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(128);
        }
    };

    /**
     * Holds factory for arrays up to size 256.
     */
    private final ObjectFactory _factory256 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(256);
        }
    };

    /**
     * Holds factory for arrays up to size 512.
     */
    private final ObjectFactory _factory512 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(512);
        }
    };

    /**
     * Holds factory for arrays up to size 1024.
     */
    private final ObjectFactory _factory1024 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(1024);
        }
    };

    /**
     * Holds factory for arrays up to size 2048.
     */
    private final ObjectFactory _factory2048 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(2048);
        }
    };

    /**
     * Holds factory for arrays up to size 4096.
     */
    private final ObjectFactory _factory4096 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(4096);
        }
    };

    /**
     * Holds factory for arrays up to size 8192.
     */
    private final ObjectFactory _factory8192 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(8192);
        }
    };

    /**
     * Holds factory for arrays up to size 16384.
     */
    private final ObjectFactory _factory16384 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(16384);
        }
    };

    /**
     * Holds factory for arrays up to size 32768.
     */
    private final ObjectFactory _factory32768 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(32768);
        }
    };

    /**
     * Holds factory for arrays up to size 65536.
     */
    private final ObjectFactory _factory65536 = new ObjectFactory() {
        @Override
        protected Object create() {
            return SizedObjectFactory.this.create(65536);
        }
    };

    // Above 65536 we use the heap exclusively. 

    /**
     * Default constructor.
     */
    public SizedObjectFactory() { }

    /**
     * Returns an sized object possibly recycled or preallocated of specified 
     * minimum size.
     * 
     * @param capacity the minimum size of the array to be returned.
     * @return a recycled, pre-allocated or new factory array.
     */
    public final T object(int capacity) {
        return (capacity <= 4) ? (T) _factory4.object()
                : largeArray(capacity);
    }

    private T largeArray(int capacity) {
        if (capacity <= 8)
            return (T) _factory8.object();
        if (capacity <= 16)
            return (T) _factory16.object();
        if (capacity <= 32)
            return (T) _factory32.object();
        if (capacity <= 64)
            return (T) _factory64.object();
        if (capacity <= 128)
            return (T) _factory128.object();
        if (capacity <= 256)
            return (T) _factory256.object();
        if (capacity <= 512)
            return (T) _factory512.object();
        if (capacity <= 1024)
            return (T) _factory1024.object();
        if (capacity <= 2048)
            return (T) _factory2048.object();
        if (capacity <= 4096)
            return (T) _factory4096.object();
        if (capacity <= 8192)
            return (T) _factory8192.object();
        if (capacity <= 16384)
            return (T) _factory16384.object();
        if (capacity <= 32768)
            return (T) _factory32768.object();
        if (capacity <= 65536)
            return (T) _factory65536.object();
        return create(capacity);
    }

    /**
     * Recycles the specified sized object.
     * 
     * @param object the SizedObject to be recycled.
     */
    public void recycle(SizedObject object) {
        recycle(object, object.size());
    }

    final void recycle(SizedObject array, int length) {
        if (length <= 4) {
            _factory4.recycle(array);
        } else if (length <= 8) {
            _factory8.recycle(array);
        } else if (length <= 16) {
            _factory16.recycle(array);
        } else if (length <= 32) {
            _factory32.recycle(array);
        } else if (length <= 64) {
            _factory64.recycle(array);
        } else if (length <= 128) {
            _factory128.recycle(array);
        } else if (length <= 256) {
            _factory256.recycle(array);
        } else if (length <= 512) {
            _factory512.recycle(array);
        } else if (length <= 1024) {
            _factory1024.recycle(array);
        } else if (length <= 2048) {
            _factory2048.recycle(array);
        } else if (length <= 4096) {
            _factory4096.recycle(array);
        } else if (length <= 8192) {
            _factory8192.recycle(array);
        } else if (length <= 16384) {
            _factory16384.recycle(array);
        } else if (length <= 32768) {
            _factory32768.recycle(array);
        } else if (length <= 65536) {
            _factory65536.recycle(array);
        }
    }

    /**
     * Constructs a new sized object of specified size from this factory 
     * (using the <code>new</code> keyword).
     *
     * @param size the size of the array.
     * @return a new factory array.
     */
    protected abstract T create(int size);

}