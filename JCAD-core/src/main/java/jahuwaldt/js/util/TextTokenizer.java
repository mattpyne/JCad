/*
*   TextTokenizer -- A javolution.text.Text compatible replacement for java.util.StringTokenizer
*
*   Copyright (C) 2009-2012 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
*
**/
package jahuwaldt.js.util;

import java.util.NoSuchElementException;
import java.util.Iterator;
import java.util.Enumeration;

import javolution.context.ObjectFactory;
import javolution.lang.Realtime;
import javolution.lang.Reusable;
import javolution.text.Text;


/**
 * The text tokenizer class allows an application to break a 
 * <code>Text</code> object into tokens. The tokenization method is much simpler than 
 * the one used by the <code>StreamTokenizer</code> class. The 
 * <code>TextTokenizer</code> methods do not distinguish among 
 * identifiers, numbers, and quoted strings, nor do they recognize 
 * and skip comments. 
 * <p>
 * The set of delimiters (the characters that separate tokens) may 
 * be specified either at creation time or on a per-token basis. 
 * <p>
 * An instance of <code>TextTokenizer</code> behaves in one of two 
 * ways, depending on whether it was created with the 
 * <code>returnDelims</code> flag having the value <code>true</code> 
 * or <code>false</code>: 
 * <ul>
 * <li>If the flag is <code>false</code>, delimiter characters serve to 
 *     separate tokens. A token is a maximal sequence of consecutive 
 *     characters that are not delimiters. 
 * <li>If the flag is <code>true</code>, delimiter characters are themselves 
 *     considered to be tokens. A token is thus either one delimiter 
 *     character, or a maximal sequence of consecutive characters that are 
 *     not delimiters.
 * </ul><p>
 * A <tt>TextTokenizer</tt> object internally maintains a current 
 * position within the text to be tokenized. Some operations advance this 
 * current position past the characters processed.<p>
 * A token is returned by taking a subtext of the text that was used to 
 * create the <tt>TextTokenizer</tt> object.
 * <p>
 * The following is one example of the use of the tokenizer. The code:
 * <blockquote><pre>
 *     TextTokenizer tt = TextTokenizer.valueOf("this is a test");
 *     while (tt.hasMoreTokens()) {
 *         System.out.println(tt.nextToken());
 *     }
 * </pre></blockquote>
 * <p>
 * prints the following output:
 * <blockquote><pre>
 *     this
 *     is
 *     a
 *     test
 * </pre></blockquote>
 *
 * <p>
 * <tt>TextTokenizer</tt> is heavily based on <code>java.util.StringTokenizer</code>.
 * However, there are some improvements and additional methods and capabilities.
 * <p>
 *
 *  <p>  Modified by:  Joseph A. Huwaldt   </p>
 *
 *  @author  Joseph A. Huwaldt   Date: March 12, 2009
 *  @version October 7, 2012
 */
public final class TextTokenizer implements Enumeration<Text>, Iterator<Text>, Iterable<Text>, Realtime, Reusable {
	private static final Text DEFAULT_DELIMS = Text.intern(" \t\n\r\f");
	
    private int _currentPosition;
    private int _newPosition;
    private int _maxPosition;
    private Text _text;
    private Text _delimiters;
    private boolean _retDelims;
    private boolean _delimsChanged;
	private boolean _honorQuotes = false;
	private char _quoteChar = '"';

	/**
	 * Set to true if empty tokens should be returned.
	 * For example, if "" should be returned when text starts with
	 * a delimiter, has two delimiters next to each other, or
	 * ends with a delimiter.
	 */
	private boolean _returnEmptyTokens;

    /**
     * _maxDelimChar stores the value of the delimiter character with the
     * highest value. It is used to optimize the detection of delimiter
     * characters.
     */
    private char _maxDelimChar;


    /**
     * Return a text tokenizer with an initially empty string of text and with
	 * no delimiters. Use {@link #setText} and {@link #setDelimiters} to make
	 * this instance useful.
	 */
	@SuppressWarnings("unchecked")
	public static TextTokenizer newInstance() {
		TextTokenizer o = FACTORY.object();
		o._text = Text.EMPTY;
		o._delimiters = DEFAULT_DELIMS;
		o._currentPosition = 0;
		o._newPosition = -1;
		o._maxPosition = o._text.length();
		o._delimsChanged = false;
		o._returnEmptyTokens = false;
		o._retDelims = false;
        o._honorQuotes = false;
        o._quoteChar = '"';
        o.setMaxDelimChar();
		return o;
	}

	/**
	*  Resets the internal state of this object to its default values.
	**/
    @Override
	public void reset() {
		_text = Text.EMPTY;
		_delimiters = DEFAULT_DELIMS;
		_currentPosition = 0;
		_newPosition = -1;
		_maxPosition = _text.length();
		_delimsChanged = false;
		_returnEmptyTokens = false;
		_retDelims = false;
        _honorQuotes = false;
        _quoteChar = '"';
		setMaxDelimChar();
	}
	
    /**
     * Return a text tokenizer for the specified character sequence. All  
     * characters in the <code>delim</code> argument are the delimiters 
     * for separating tokens. 
     * <p>
     * If the <code>returnDelims</code> flag is <code>true</code>, then 
     * the delimiter characters are also returned as tokens. Each 
     * delimiter is returned as a string of length one. If the flag is 
     * <code>false</code>, the delimiter characters are skipped and only 
     * serve as separators between tokens. 
     * <p>
     * Note that if <tt>delim</tt> is <tt>null</tt>, this constructor does
     * not throw an exception. However, trying to invoke other methods on the
     * resulting <tt>TextTokenizer</tt> may result in a 
     * <tt>NullPointerException</tt>.
     *
     * @param   text           the text to be parsed.
     * @param   delim          the delimiters.
     * @param   returnDelims   flag indicating whether to return the delimiters
     *                         as tokens.
     */
    public static TextTokenizer valueOf(CharSequence text, CharSequence delim, boolean returnDelims) {
		TextTokenizer o = TextTokenizer.newInstance();
		
		o._text = (text != null ? Text.valueOf(text) : Text.EMPTY);
		o._currentPosition = 0;
		o._newPosition = -1;
		o._maxPosition = o._text.length();
		
		o._delimsChanged = false;
		o._delimiters = (delim != null ? Text.valueOf(delim) : Text.EMPTY);
		o._retDelims = returnDelims;
        o.setMaxDelimChar();
		
		return o;
    }

    /**
     * Return a text tokenizer for the specified character sequence. The 
     * characters in the <code>delim</code> argument are the delimiters 
     * for separating tokens. Delimiter characters themselves will not 
     * be treated as tokens.
     *
     * @param   text    the text to be parsed.
     * @param   delim   the delimiters.
     */
    public static TextTokenizer valueOf(CharSequence text, CharSequence delim) {
		return TextTokenizer.valueOf(text, delim, false);
    }

    /**
     * Return a text tokenizer for the specified character sequence. The 
     * tokenizer uses the default delimiter set, which is 
     * <code>"&nbsp;&#92;t&#92;n&#92;r&#92;f"</code>: the space character, 
     * the tab character, the newline character, the carriage-return character,
     * and the form-feed character. Delimiter characters themselves will 
     * not be treated as tokens.
     *
     * @param   text  the text to be parsed.
     */
    public static TextTokenizer valueOf(CharSequence text) {
		return TextTokenizer.valueOf(text, DEFAULT_DELIMS, false);
    }


	/**
	 * Set the text to be tokenized in this <tt>TextTokenizer</tt>.
	 * <p>
	 * This is useful when for <tt>TextTokenizer</tt> re-use so that new string tokenizers do not
	 * have to be created for each string you want to tokenizer.
	 * <p>
	 * The text will be tokenized from the beginning of the text.
	 *
	 * @param text the text to be parsed.
	 */
	public void setText(CharSequence text){
		_text = (text != null ? Text.valueOf(text) : Text.EMPTY);
		_currentPosition = 0;
		_newPosition = -1;
		_maxPosition = _text.length();
	}
	
	/**
	 * Set the delimiters for this <tt>TextTokenizer</tt>.
	 * The position must be initialized before this method is used
	 * (setText does this and it is called from the constructor).
	 *
	 * @param delim  the delimiters
	 */
	public void setDelimiters(CharSequence delim){
		_delimiters = (delim != null ? Text.valueOf(delim) : Text.EMPTY);
		
		/* delimiter string specified, so set the appropriate flag. */
		_delimsChanged = true;

		setMaxDelimChar();
	}
	
	/**
	*  Set the character to use as the "quote" character.  All text between quote
	*  characters is considered a single token.  The default quote character is <code>'"'</code>.
	*
	*  @see #setHonorQuotes
	**/
	public void setQuoteChar(char quote) {
		_quoteChar = quote;
	}
	
	/**
	*  Sets whether or not this tokenizer recognizes quoted text using the specified
	*  quote character.  If <code>true</code> is passed, this tokenizer will consider any
	*  text between the specified quote characters as a single token.  Honoring of
	*  quotes defaults to false.
	*
	*  @see #setQuoteChar
	**/
	public void setHonorQuotes(boolean honorQuotes) {
		_honorQuotes = honorQuotes;
	}
	
	/**
	*  Returns <code>true</code> if this tokenizer honors quoted text (counts it as a single token).
	**/
	public boolean getHonorQuotes() {
		return _honorQuotes;
	}
	
     /**
     * Set _maxDelimChar to the highest char in the delimiter set.
     */
    private void setMaxDelimChar() {
		char m = 0;
		for (int i = 0; i < _delimiters.length(); i++) {
			char c = _delimiters.charAt(i);
			if (m < c)
			m = c;
		}
		_maxDelimChar = m;
    }

	/**
	 * Set whether empty tokens should be returned from this point in
	 * in the tokenizing process onward.
	 * <p>
	 * Empty tokens occur when two delimiters are next to each other
	 * or a delimiter occurs at the beginning or end of a string. If
	 * empty tokens are set to be returned, and a comma is the non token
	 * delimiter, the following table shows how many tokens are in each
	 * string.<br>
	 * <table><tr><th>String<th><th>Number of tokens<th></tr>
	 * <tr><td align=right>"one,two"<td><td>2 - normal case with no empty tokens.<td></tr>
	 * <tr><td align=right>"one,,three"<td><td>3 including the empty token in the middle.<td></tr>
	 * <tr><td align=right>"one,"<td><td>2 including the empty token at the end.<td></tr>
	 * <tr><td align=right>",two"<td><td>2 including the empty token at the beginning.<td></tr>
	 * <tr><td align=right>","<td><td>2 including the empty tokens at the beginning and the ends.<td></tr>
	 * <tr><td align=right>""<td><td>1 - all strings will have at least one token if empty tokens are returned.<td></tr></table>
	 *
	 * @param returnEmptyTokens true if and only if empty tokens should be returned.
	 */
	public void setReturnEmptyTokens(boolean returnEmptyTokens){
		_returnEmptyTokens = returnEmptyTokens;
	}

   /**
     * Skips delimiters starting from the specified position. If _retDelims
     * is false, returns the index of the first non-delimiter character at or
     * after startPos. If _retDelims is true, startPos is returned.
     */
    private int skipDelimiters(int startPos) {
		if (Text.EMPTY.equals(_delimiters))	return _maxPosition;
		
        int position = startPos;
		while (!_retDelims && position < _maxPosition) {
			char c = _text.charAt(position);
			if ( (c > _maxDelimChar) || (_delimiters.indexOf(c,0) < 0) )
				break;
			position++;
			if (_returnEmptyTokens)
				break;
		}
        return position;
    }

    /**
     * Skips ahead from startPos and returns the index of the next delimiter
     * character encountered, or _maxPosition if no such delimiter is found.
     */
    private int scanToken(int startPos) {
		boolean inQuote = false;
        int position = startPos;
        while (position < _maxPosition) {
            char c = _text.charAt(position);
			if (_honorQuotes && c == _quoteChar) {
				if (!inQuote)
					inQuote = true;
				else
					inQuote = false;
				
			} else if (!inQuote && (c <= _maxDelimChar) && (_delimiters.indexOf(c,0) >= 0))
                break;
            position++;
		}
		if (_retDelims && (startPos == position)) {
			char c = _text.charAt(position);
			if ((c <= _maxDelimChar) && (_delimiters.indexOf(c,0) >= 0))
					position++;
        }
        return position;
    }

    /**
     * Tests if there are more tokens available from this tokenizer's text. 
     * If this method returns <tt>true</tt>, then a subsequent call to 
     * <tt>nextToken</tt> with no argument will successfully return a token.
     *
     * @return  <code>true</code> if and only if there is at least one token 
     *          in the text after the current position; <code>false</code> 
     *          otherwise.
     */
    public boolean hasMoreTokens() {
		/*
		 * Temporary store this position and use it in the following
		 * nextToken() method only if the delimiters have'nt been changed in
		 * that nextToken() invocation.
		 */
		_newPosition = skipDelimiters(_currentPosition);
		return (_newPosition < _maxPosition);
    }

    /**
     * Returns the next token from this text tokenizer.
     *
     * @return     the next token from this text tokenizer.
     * @exception  NoSuchElementException  if there are no more tokens in this
     *               tokenizer's text.
     */
    public Text nextToken() {
		/* 
		 * If next position already computed in hasMoreElements() and
		 * delimiters have changed between the computation and this invocation,
		 * then use the computed value.
		 */
		_currentPosition = (_newPosition >= 0 && !_delimsChanged) ?  
								_newPosition : skipDelimiters(_currentPosition);

		/* Reset these anyway */
		_delimsChanged = false;
		_newPosition = -1;

		if (_currentPosition >= _maxPosition)
			throw new NoSuchElementException();
		
		int start = _currentPosition;
		_currentPosition = scanToken(_currentPosition);
		
		return _text.subtext(start, _currentPosition);
    }

    /**
     * Returns the next token in this text tokenizer's text. First, 
     * the set of characters considered to be delimiters by this 
     * <tt>TextTokenizer</tt> object is changed to be the characters in 
     * the string <tt>delim</tt>. Then the next token in the text
     * after the current position is returned. The current position is 
     * advanced beyond the recognized token.  The new delimiter set 
     * remains the default after this call. 
     *
     * @param      delim   the new delimiters.
     * @return     the next token, after switching to the new delimiter set.
     * @exception  NoSuchElementException  if there are no more tokens in this
     *               tokenizer's text.
     */
    public Text nextToken(CharSequence delim) {
		setDelimiters(delim);
		return nextToken();
    }

    /**
     * Returns the same value as the <code>hasMoreTokens</code>
     * method. It exists so that this class can implement the
     * <code>Enumeration</code> interface. 
     *
     * @return  <code>true</code> if there are more tokens;
     *          <code>false</code> otherwise.
     * @see     java.util.Enumeration
     * @see     #hasMoreTokens()
     */
    @Override
    public boolean hasMoreElements() {
		return hasMoreTokens();
    }

    /**
     * Returns the same value as the <code>nextToken</code> method.
	 * It exists so that this class can implement the
     * <code>Enumeration</code> interface. 
     *
     * @return     the next token in the text.
     * @exception  NoSuchElementException  if there are no more tokens in this
     *               tokenizer's text.
     * @see        java.util.Enumeration
     * @see        #nextToken()
     */
    @Override
    public Text nextElement() {
		return nextToken();
    }

	
	/**
	*  Returns an iterator over the tokens returned by this tokenizer.
	**/
    @Override
	public Iterator<Text> iterator() {
		return this;
	}
	
	/**
	 * Returns the same value as the <code>hasMoreTokens()</code> method. It exists
	 * so that this class can implement the <code>Iterator</code> interface.
	 *
	 * @return <code>true</code> if there are more tokens;
	 *     <code>false</code> otherwise.
	 *
	 * @see java.util.Iterator
	 * @see #hasMoreTokens()
	 */
    @Override
	public boolean hasNext(){
		return hasMoreTokens();
	}

	/**
	 * Returns the same value as the <code>nextToken()</code> method.
	 * It exists so that this class can implement the
	 * <code>Iterator</code> interface.
	 *
	 * @return the next token in the text.
	 * @throws NoSuchElementException if there are no more tokens in this tokenizer's text.
	 *
	 * @see java.util.Iterator
	 * @see #nextToken()
	 */
    @Override
	public Text next(){
		return nextToken();
	}

 	/**
	 * This implementation always throws <code>UnsupportedOperationException</code>.
	 * It exists so that this class can implement the <code>Iterator</code> interface.
	 *
	 * @throws UnsupportedOperationException always is thrown.
	 *
	 * @see java.util.Iterator
	 */
    @Override
	public void remove(){
		throw new UnsupportedOperationException();
	}

   /**
     * Calculates the number of times that this tokenizer's 
     * <code>nextToken</code> method can be called before it generates an 
     * exception. The current position is not advanced.
     *
     * @return  the number of tokens remaining in the text using the current
     *          delimiter set.
     * @see     #nextToken()
     */
    public int countTokens() {
		int count = 0;
		int currpos = _currentPosition;
		while (currpos < _maxPosition) {
				currpos = skipDelimiters(currpos);
			if (currpos >= _maxPosition)
			break;
				currpos = scanToken(currpos);
			count++;
		}
		return count;
    }

	/**
	 * Calculates the number of times that this tokenizer's <code>nextToken</code>
	 * method can be called before it generates an exception using the given set of
	 * delimiters.  The delimiters given will be used for future calls to
	 * nextToken() unless new delimiters are given. The current position
	 * is not advanced.
	 *
	 * @param delims the new set of delimiters.
	 * @return the number of tokens remaining in the text using the new
	 *    delimiter set.
	 *
	 * @see #countTokens()
	 */
	public int countTokens(CharSequence delims){
		setDelimiters(delims);
		return countTokens();
	}

	/**
	 * Retrieves the rest of the text as a single token.
	 * After calling this method hasMoreTokens() will always return false.
	 *
	 * @return any part of the text that has not yet been tokenized.
	 */
	public Text restOfText() {
		Text output = _text.subtext(_currentPosition, _maxPosition);
		_currentPosition = _maxPosition;
		return output;
	}

	/**
	 * Returns the same value as the <code>nextToken()</code> method.
	 * It exists so that this class can implement the
	 * <code>Realtime</code> interface.
	 *
	 * @return the next token in the text.
	 * @throws NoSuchElementException if there are no more tokens in this tokenizer's text.
	 *
	 * @see javolution.lang.Realtime
	 * @see #nextToken()
	 */
    @Override
	public Text toText() {
		return nextToken();
	}

	/**
	* Recycles a <code>TextTokenizer</code> instance immediately
	* (on the stack when executing in a <code>StackContext</code>).
	**/
	public static void recycle(TextTokenizer instance) {
		FACTORY.recycle(instance);
	}
	

	//////////////////////
	// Factory Creation //
	//////////////////////
	
	@SuppressWarnings("unchecked")
	private static final ObjectFactory<TextTokenizer> FACTORY = new ObjectFactory<TextTokenizer>() {
		@Override
		protected TextTokenizer create() {
			return new TextTokenizer();
		}
        @Override
		protected void cleanup(TextTokenizer instance) {
			instance.reset();
		}
	};

	private TextTokenizer() { }


	/**
	*  Testing code for this class.
	**/
	public static void main (String args[]) {
		System.out.println("Testing TextTokenizer:");
		
		System.out.println("\nTokenize: \"this is a test\":");
		TextTokenizer tt = TextTokenizer.valueOf("this is a test");
		while (tt.hasMoreTokens()) {
			tt.nextToken().println();
		}
		
		System.out.println("\nTokenize: \"this,is,,a,test\" returning empty tokens:");
		tt.setText("this,is,,a,test");
		tt.setDelimiters(",");
		tt.setReturnEmptyTokens(true);
		while (tt.hasMoreTokens()) {
			tt.nextToken().println();
		}
		
	}
}
