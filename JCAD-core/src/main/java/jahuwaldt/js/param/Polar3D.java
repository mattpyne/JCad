/*
 *   Polar3D -- A 3D polar coordinate of azimuth & elevation angle and magnitude.
 *
 *   Copyright (C) 2008-2015, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.*;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.vector.*;

/**
 * This class represents a 3 element {@link Vector vector} of Parameter elements
 * representing a geometrical polar coordinate with elements magnitude, azimuth angle and
 * elevation angle where elevation is measured positive above the reference plane (similar
 * to latitude).
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: November 21, 2008
 * @version November 4, 2015
 *
 * @param <Q> The Quantity (unit type, such as Length or Volume) of this vector.
 */
public final class Polar3D<Q extends Quantity> extends Coordinate3D<Q> implements XMLSerializable {
    //  Reference:  http://mathworld.wolfram.com/SphericalCoordinates.html
    
    private static final long serialVersionUID = -2077967266275152872L;

    /**
     * Constant used to identify the magnitude of in the vector.
     */
    public static final int MAGNITUDE = 0;

    /**
     * Constant used to identify the azimuth angle of the vector.
     */
    public static final int AZIMUTH = 1;

    /**
     * Constant used to identify the elevation angle of the vector.
     */
    public static final int ELEVATION = 2;

    /**
     * The magnitude of this vector.
     */
    private Parameter<Q> _mag;

    /**
     * The azimuth angle of this vector.
     */
    private Parameter<Angle> _azimuth;

    /**
     * The elevation angle of this vector.
     */
    private Parameter<Angle> _elevation;

    /**
     * Returns a {@link Polar3D} instance holding the specified <code>double</code> values
     * stated in the specified units.
     *
     * @param <Q>       the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param magnitude the magnitude (length) of the vector stated in the specified unit.
     * @param azimuth   the vector azimuth angle stated in the specified angle unit.
     * @param elevation the vector elevation angle value stated in the specified angle
     *                  unit.
     * @param magUnit   the unit in which the magnitude is stated.
     * @param angleUnit the unit in which the azimuth and elevation angles are stated.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Polar3D<Q> valueOf(double magnitude, double azimuth, double elevation,
            Unit<Q> magUnit, Unit<Angle> angleUnit) {
        Polar3D<Q> V = Polar3D.newInstance();

        V._mag = Parameter.valueOf(magnitude, magUnit);
        V._azimuth = Parameter.valueOf(azimuth, angleUnit);
        V._elevation = Parameter.valueOf(elevation, angleUnit);

        return V;
    }

    /**
     * Returns a {@link Polar3D} instance holding the specified <code>Parameter</code>
     * values. All the values are converted to the same units as the x value.
     *
     * @param <Q>       the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param magnitude the vector magnitude (length) value.
     * @param azimuth   the vector azimuth angle value.
     * @param elevation the vector elevation angle value.
     * @return the vector having the specified values in the units of magnitude.
     */
    public static <Q extends Quantity> Polar3D<Q> valueOf(Parameter<Q> magnitude, Parameter<Angle> azimuth, Parameter<Angle> elevation) {
        Polar3D<Q> V = Polar3D.newInstance();
        V._mag = magnitude;
        V._azimuth = azimuth;
        V._elevation = elevation.to(azimuth.getUnit());

        return V;
    }

    /**
     * Returns a {@link Polar3D} instance containing the polar coordinate representation
     * of the specified {@link Coordinate3D coordinate}. The azimuth and elevation
     * components of the resulting vector have units of radians.
     * <p>
     * The polar representation of a Cartesian coordinate is given by:
     * <pre>
     *         magnitude = |V|
     *         azimuth   = atan2(Y,X)
     *         elevation = 90° - acos(Z/|V|) = latitude
     * </pre></p>
     *
     * @param <Q>        the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param coordinate the input coordinate.
     * @return the polar coordinate having the specified values.
     */
    public static <Q extends Quantity> Polar3D<Q> valueOf(Coordinate3D<Q> coordinate) {

        if (coordinate instanceof Polar3D)
            return (Polar3D)coordinate;

        Vector3D<Q> vector = coordinate.toVector3D();
        double x = vector.getValue(Vector3D.X);
        double y = vector.getValue(Vector3D.Y);
        double z = vector.getValue(Vector3D.Z);

        double mag = sqrt(x * x + y * y + z * z);
        double azim = atan2(y, x);
        double phi = acos(z/mag);
        double elev = HALF_PI - phi;

        Polar3D<Q> V = Polar3D.newInstance();
        V._mag = Parameter.valueOf(mag, vector.getUnit());
        V._azimuth = Parameter.valueOf(azim, SI.RADIAN);
        V._elevation = Parameter.valueOf(elev, SI.RADIAN);

        return V;
    }

    /**
     * Returns a {@link Polar3D} instance containing the polar coordinate representation
     * of the specified {@link Coordinate3D coordinate}.
     * <p>
     * The polar representation of a Cartesian coordinate is given by:
     * <pre>
     *         magnitude = |V|
     *         azimuth   = atan2(Y,X)
     *         elevation = atan2(-Z,sqrt(X^2+Y^2))
     * </pre></p>
     *
     * @param <Q>       the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param coord     the coordinate to convert.
     * @param angleUnit the unit to use for the azimuth and elevation components.
     * @return the polar coordinate vector having the specified values.
     */
    public static <Q extends Quantity> Polar3D<Q> valueOf(Coordinate3D<Q> coord, Unit<Angle> angleUnit) {
        Polar3D<Q> V = Polar3D.valueOf(coord);
        V._azimuth = V._azimuth.to(angleUnit);
        V._elevation = V._elevation.to(angleUnit);
        return V;
    }

    /**
     * Return the specified {@link Vector3D} object as a <code>Polar3D</code> instance.
     *
     * @param vector The Vector3D object to be converted to a Polar3D.
     * @return A Polar3D instance that is equivalent to the supplied Vector3D object.
     */
    @Override
    public Polar3D<Q> fromVector3D(Vector3D<Q> vector) {
        return Polar3D.valueOf(vector, _azimuth.getUnit());
    }

    /**
     * Returns the value of a Parameter from this vector. The dimensions are defined as:
     * magnitude, azimuth angle, and elevation angle in that order.
     *
     * @param i the dimension index (0=magnitude, 1=azimuth, 2=elevation).
     * @return the value of the parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i > dimension()-1)</code>
     */
    @Override
    public Parameter get(int i) {
        Parameter value = null;
        switch (i) {
            case 0:
                value = _mag;
                break;
            case 1:
                value = _azimuth;
                break;
            case 2:
                value = _elevation;
                break;
            default:
                throw new IndexOutOfBoundsException(
                        MessageFormat.format(RESOURCES.getString("p3dIndexErr"), i));
        }
        return value;
    }

    /**
     * Returns the magnitude of this vector as a <code>double</code>, stated in this
     * vector's {@link #getUnit() unit}.
     *
     * @return the magnitude of this vector in this vector's units.
     */
    public double getMagnitudeValue() {
        return _mag.getValue();
    }

    /**
     * Returns the magnitude of this vector as a {@link Parameter}.
     *
     * @return the magnitude of this vector.
     */
    public Parameter<Q> getMagnitude() {
        return _mag;
    }

    /**
     * Returns the azimuth angle of this vector as a {@link Parameter}.
     *
     * @return the azimuth angle of this vector.
     */
    public Parameter<Angle> getAzimuth() {
        return _azimuth;
    }

    /**
     * Returns the elevation angle of this vector as a {@link Parameter}.
     *
     * @return the elevation angle of this vector.
     */
    public Parameter<Angle> getElevation() {
        return _elevation;
    }

    /**
     * Returns the Euclidian norm, magnitude, or length of this vector (square root of the
     * dot product of this vector and itself).
     *
     * @return <code>sqrt(this · this)</code>.
     */
    @Override
    public Parameter<Q> norm() {
        return _mag;
    }

    /**
     * Returns the {@link #norm() norm}, magnitude, or length value of this vector.
     *
     * @return <code>this.norm().getValue()</code>.
     */
    @Override
    public double normValue() {
        return _mag.getValue();
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    @Override
    public Polar3D<Q> opposite() {

        //  Convert to cartesian coordinates, negate that, and then convert back to polar.
        Vector3D<Q> V3D = toVector3D().opposite();
        Polar3D<Q> V = fromVector3D(V3D);

        return V;
    }

    /**
     * Returns the sum of this vector with the one specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Polar3D<Q> plus(Vector<Parameter<Q>> that) {

        //  Convert to Cartesian coordinates, add those, then convert back.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D<Q> T = ((Coordinate3D<Q>)that).toVector3D();
            Vector3D<Q> V3D = toVector3D().plus(T);
            Polar3D<Q> V = fromVector3D(V3D);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the sum of this vector with the parameter specified. The input parameter is
     * added to the magnitude of this vector; the direction is left unchanged. The unit of
     * the output vector will be the units of this vector.
     *
     * @param that the parameter to be added to the magnitude of this vector.
     * @return <code>this + that</code>.
     */
    @Override
    public Polar3D<Q> plus(Parameter<Q> that) {

        StackContext.enter();
        try {
            Polar3D<Q> V = Polar3D.newInstance();
            Parameter<Q> mag = _mag.plus(that);
            V._mag = mag;
            V._azimuth = _azimuth;
            V._elevation = _elevation;
            if (mag.getValue() < 0) {
                V._mag = mag.opposite();
                V = V.opposite();
            }

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the difference between this vector and the one specified.
     *
     * @param that the vector to be subtracted.
     * @return <code>this - that</code>.
     */
    @Override
    public Polar3D<Q> minus(Vector<Parameter<Q>> that) {

        //  Convert to Cartesian coordinates, subtract those, then convert back.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D<Q> T = ((Coordinate3D<Q>)that).toVector3D();
            Vector3D<Q> V3D = toVector3D().minus(T);
            Polar3D<Q> V = fromVector3D(V3D);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Subtracts the supplied Parameter from this vector's magnitude and returns the
     * result. The direction of the vector is left unchanged. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from the magnitude of this vector.
     * @return <code>this - that</code>.
     */
    @Override
    public Polar3D<Q> minus(Parameter<Q> that) {

        StackContext.enter();
        try {
            Polar3D<Q> V = Polar3D.newInstance();
            Parameter<Q> mag = _mag.minus(that);
            V._mag = mag;
            V._azimuth = _azimuth;
            V._elevation = _elevation;
            if (mag.getValue() < 0) {
                V._mag = mag.opposite();
                V = V.opposite();
            }

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the product of this vector with the specified coefficient. The magnitude of
     * this vector is scaled by the input coefficient and the direction is left unchanged.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Polar3D times(Parameter k) {
        Polar3D V = Polar3D.newInstance();
        V._mag = _mag.times(k);
        V._azimuth = _azimuth;
        V._elevation = _elevation;
        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient. The magnitude of
     * this vector is scaled by the input coefficient and the direction is left unchanged.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Polar3D<Q> times(double k) {
        Polar3D<Q> V = Polar3D.newInstance();
        V._mag = _mag.times(k);
        V._azimuth = _azimuth;
        V._elevation = _elevation;
        return V;
    }

    /**
     * Returns the dot product of this vector with the one specified.
     *
     * @param that the vector multiplier.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    @Override
    public Parameter times(Vector that) {

        //  Convert to Cartesian coordinates and multiply that.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D T = ((Coordinate3D)that).toVector3D();
            Parameter product = toVector3D().times(T);

            return StackContext.outerCopy(product);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the result of this vector divided by the specified divisor. The magnitude
     * of this vector is divided by the input coefficient and the direction is left
     * unchanged.
     *
     * @param that the divisor.
     * @return <code>this / that</code>.
     */
    public Polar3D<?> divide(Parameter<?> that) {
        return (Polar3D<?>)times(that.inverse());
    }

    /**
     * Returns the cross product of two 3-dimensional vectors.
     *
     * @param that the vector multiplier.
     * @return <code>this x that</code>
     * @throws DimensionException if <code>(that.getDimension() != 3)</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    @Override
    public Polar3D cross(Vector that) {

        //  Convert to Cartesian coordinates, multiply those, then convert back.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D<?> T = ((Coordinate3D<?>)that).toVector3D();
            Vector3D<?> V3D = toVector3D().cross(T);
            Polar3D<?> V = Polar3D.valueOf(V3D, _azimuth.getUnit());

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns this vector converted to a unit vector with a vector magnitude of 1.0.
     *
     * @return this vector converted to a unit vector
     */
    public Polar3D<Dimensionless> toUnitVector() {
        double magnitude = this.normValue();
        if (this.getUnit().equals(Dimensionless.UNIT) && MathTools.isApproxEqual(magnitude, 1.0))
            return (Polar3D<Dimensionless>)this;

        Polar3D<Dimensionless> V = Polar3D.newInstance();
        V._azimuth = this._azimuth;
        V._elevation = this._elevation;
        V._mag = Parameter.ONE;
        
        return V;
    }

    /**
     * Returns a copy of this vector {@link javolution.context.AllocatorContext allocated}
     * by the calling thread (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public Polar3D<Q> copy() {
        return copyOf(this);
    }

    /**
     * Returns the unit in which the magnitude in this vector are stated in.
     *
     * @return the unit in which the magnitude in this vector are stated in
     */
    @Override
    public Unit<Q> getUnit() {
        return _mag.getUnit();
    }

    /**
     * Returns the unit in which the angles in this vector are stated in.
     *
     * @return the unit in which the angles in this vector are stated in
     */
    public Unit<Angle> getAnglesUnit() {
        return _azimuth.getUnit();
    }

    /**
     * Returns the equivalent to this vector but with the magnitude stated in the
     * specified unit.
     *
     * @param <R>  the Quantity (unit type, e.g. Length or Volume) of the returned vector.
     * @param unit the unit of the magnitude of the vector to be returned.
     * @return a vector equivalent to this vector but with the magnitude stated in the
     *         specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public <R extends Quantity> Polar3D<R> to(Unit<R> unit) throws ConversionException {
        Unit<?> thisUnit = getUnit();
        if ((thisUnit == unit) || thisUnit.equals(unit))
            return (Polar3D<R>)this;
        Polar3D<R> result = Polar3D.newInstance();
        result._mag = _mag.to(unit);
        result._azimuth = _azimuth;
        result._elevation = _elevation;
        return result;
    }

    /**
     * Returns the equivalent to this vector but with the angles stated in the specified
     * unit.
     *
     * @param unit the angle unit of the angles of the vector to be returned.
     * @return a coordinate equivalent to this vector but with the angles stated in the
     *         specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public Polar3D<Q> anglesTo(Unit<Angle> unit) {
        Unit<Angle> thisUnit = _azimuth.getUnit();
        if ((thisUnit == unit) || thisUnit.equals(unit))
            return this;
        Polar3D<Q> result = Polar3D.newInstance();
        result._mag = _mag;
        result._azimuth = _azimuth.to(unit);
        result._elevation = _elevation.to(unit);
        return result;
    }

    /**
     * Casts this Polar3D to a parameterized unit of specified nature or throw a
     * <code>ClassCastException</code> if the dimension of the specified quantity and this
     * parameter's unit dimension do not match.
     *
     * @param <T>  the Quantity (unit type, e.g. Length or Volume) of the returned vector.
     * @param type the quantity class identifying the nature of the unit.
     * @return this Polar3D parameterized with the specified type.
     * @throws ClassCastException if the dimension of this parameter's unit is different
     * from the specified quantity dimension.
     * @throws UnsupportedOperationException if the specified quantity class does not have
     * a public static field named "UNIT" holding the standard unit for the quantity.
     */
    @Override
    public <T extends Quantity> Polar3D<T> asType(Class<T> type) throws ClassCastException {
        Unit<T> u = getUnit().asType(type); //  If no exception is thrown, the cast is valid.
        return (Polar3D<T>)this;
    }

    /**
     * Returns a Cartesian Vector3D representation of this vector.
     * <p>
     * The polar to Cartesian transformation is defined by:
     * <pre>
     *    |x|             | cos(elevation)*cos(azimuth)  |
     *    |y| = magnitude*| cos(elevation)*sin(azimuth)  |
     *    |z|             |     -sin(elevation)          |
     * </pre></p>
     *
     * @return a Cartesian Vector3D representation of this vector
     */
    @Override
    public Vector3D<Q> toVector3D() {

        double mag = _mag.getValue();
        double azim = _azimuth.to(SI.RADIAN).getValue();
        double elev = _elevation.to(SI.RADIAN).getValue();
        //double phi = HALF_PI - elev;

        double sPhi = cos(elev), cPhi = sin(elev);
        double x = mag * sPhi * cos(azim);
        double y = mag * sPhi * sin(azim);
        double z = -mag * cPhi;

        Vector3D<Q> V = Vector3D.valueOf(x, y, z, getUnit());

        return V;
    }

    /**
     * Compares this Polar3D against the specified object for strict equality (same values
     * and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this vector is identical to that vector;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        Polar3D<?> that = (Polar3D<?>)obj;
        if (!this._mag.equals(that._mag))
            return false;
        if (!this._azimuth.equals(that._azimuth))
            return false;

        return this._elevation.equals(that._elevation);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _mag.hashCode();
        hash = hash * 31 + var_code;

        var_code = _azimuth.hashCode();
        hash = hash * 31 + var_code;

        var_code = _elevation.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    /**
     * Holds the default XML representation. For example:
     * <pre>
     *    &lt;Magnitude value="1.73205080756888" unit="m"/&gt;
     *    &lt;Azimuth value="45.0" unit="°" /&gt;
     *    &lt;Elevation value="35.2643896827547" unit="°" /&gt;
     * </pre>
     */
    protected static final XMLFormat<Polar3D> XML = new XMLFormat<Polar3D>(Polar3D.class) {

        @Override
        public Polar3D<?> newInstance(Class<Polar3D> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, Polar3D V) throws XMLStreamException {
            V._mag = xml.get("Magnitude", Parameter.class);
            V._azimuth = xml.get("Azimuth", Parameter.class);
            V._elevation = xml.get("Elevation", Parameter.class);
        }

        @Override
        public void write(Polar3D V, OutputElement xml) throws XMLStreamException {
            xml.add(V._mag, "Magnitude", Parameter.class);
            xml.add(V._azimuth, "Azimuth", Parameter.class);
            xml.add(V._elevation, "Elevation", Parameter.class);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private Polar3D() {
    }

    private static final ObjectFactory<Polar3D<? extends Quantity>> FACTORY = new ObjectFactory<Polar3D<? extends Quantity>>() {
        @Override
        protected Polar3D<? extends Quantity> create() {
            return new Polar3D();
        }
    };

    private static <Q extends Quantity> Polar3D<Q> newInstance() {
        Polar3D<Q> measure = (Polar3D<Q>)FACTORY.object();
        return measure;
    }

    private static <Q extends Quantity> Polar3D<Q> copyOf(Polar3D<Q> original) {
        Polar3D<Q> measure = Polar3D.newInstance();
        measure._mag = original._mag.copy();
        measure._azimuth = original._azimuth.copy();
        measure._elevation = original._elevation.copy();
        return measure;
    }

    /**
     * Tests the methods in this class.
     *
     * @param args Command line arguments (ignored)
     */
    public static void main(String args[]) {
        System.out.println("Testing Polar3D:  test = result [correct result]");

        Polar3D<Length> v1 = Polar3D.valueOf(1, 20, 30, NonSI.FOOT, NonSI.DEGREE_ANGLE);
        System.out.println("v1 = " + v1);
        System.out.println("v1 (as Vector3D) = " + v1.toVector3D());
        System.out.println("  converted to m       = " + v1.to(SI.METER) + " [{0.3048 m, 20.0 °, 30.0 °}]");
        System.out.println("  v1.norm()            = " + v1.norm() + " [1.0 ft]");
        System.out.println("  v1.opposite()        = " + v1.opposite() + " [{1.0 ft, 200.0 °, 330.0 °}]");

        Parameter<Length> point = Parameter.valueOf(24, NonSI.INCH);
        System.out.println("point = " + point);
        System.out.println("  v1 + point           = " + v1.plus(point) + " [{3.0 ft, 20.0 °, 30.0 °}]");
        System.out.println("  v1 - point           = " + v1.minus(point) + " [{1.0 ft, 200.0 °, 330.0 °}]");
        System.out.println("  v1 * 2               = " + v1.times(2) + " [{2.0 ft, 20.0 °, 30.0 °}]");

        Polar3D<?> areaVector = (Polar3D<?>)v1.times(point);
        System.out.println("  v1 * point           = " + areaVector);
//        System.out.println("  converted to ft²     = " + areaVector.to(NonSI.SQUARE_FOOT) + " [{2.0 ft², 20.0 °, 30.0 °}]");

        Vector3D<Length> v2 = Vector3D.valueOf(1, 1, -1, SI.METER);
        System.out.println("v2 = " + v2);
        System.out.println("v2 (as Polar3D)= " + Polar3D.valueOf(v2, NonSI.DEGREE_ANGLE) + " [{1.73205080756888 m, 45.0 °, 35.2643896827547 °}]");
        System.out.println("  v1 + v2 = " + v1.plus(v2) + " [{6.62238689940225 ft, 41.1401661546401 °, 34.8142656403426 °}]");
        System.out.println("  v1 - v2 = " + v1.minus(v2) + " [{4.76733198496641 ft, 230.423579133255 °, 324.316200627242 °}]");
        System.out.println("  v1 · v2 = " + v1.times(v2.to(NonSI.FOOT)) + " [5.2821384976227 ft²]");
        System.out.println("  v1.cross(v2) = " + v1.cross(v2) + " [{2.09541025626521 ft, 56.9975252490327 °, 305.863065923747 °}]");
        System.out.println("  v1.angle(v2) = " + v1.angle(Polar3D.valueOf(v2)).to(NonSI.DEGREE_ANGLE) + " [21.638095609098805 deg]");

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
            binding.setAlias(org.jscience.mathematics.number.Float64.class, "Float64");
            binding.setAlias(org.jscience.mathematics.vector.Float64Matrix.class, "Float64Matrix");

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(v1, "Polar3D", Polar3D.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
