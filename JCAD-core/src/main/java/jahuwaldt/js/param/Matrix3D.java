/**
 * Matrix3D -- A 3x3 matrix of Parameter objects.
 *
 * Copyright (C) 2008-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.*;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.MathLib;
import javolution.lang.Realtime;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLSerializable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.*;

/**
 * <p>
 * This class represents a 3x3 {@link Matrix matrix} implementation for
 * {@link Parameter parameter} elements.</p>
 *
 * <p>
 * Instances of this class can be created from {@link Vector3D}, either as rows or columns
 * if the matrix is transposed. For example:
 * <pre>
 *         Vector3D<Length> column0 = Vector3D.valueOf(...);
 *         Vector3D<Length> column1 = Vector3D.valueOf(...);
 *         Vector3D<Length> column2 = Vector3D.valueOf(...);
 *         Matrix3D<Length> M = Matrix3D.valueOf(column0, column1, column2).transpose();
 * </pre></p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: November 26, 2008
 * @version October 23, 2015
 */
public final class Matrix3D<Q extends Quantity> extends Matrix<Parameter<Q>> implements Realtime, ValueType, XMLSerializable {

    private static final long serialVersionUID = -3295634256026052059L;

    /**
     * The resource bundle for this package.
     */
    protected static final ResourceBundle RESOURCES = Parameter.RESOURCES;

    /**
     * The elements stored in this matrix. Serialization is handled by this class since
     * Float64Matrix is not Serializable.
     */
    private transient Float64Matrix _data;

    /**
     * Holds the units of the matrix elements.
     */
    private Unit<Q> _unit;

    /**
     * Returns 3D matrix from a sequence of 9 <code>double</code> values.
     * <p>
     * Values are ordered as follows:
     * <pre>
     *         | a b c |
     *         | d e f |
     *         | g h i |
     * </pre></p>
     *
     * @param a    matrix element 0,0
     * @param b    matrix element 0,1
     * @param c    matrix element 0,2
     * @param d    matrix element 1,0
     * @param e    matrix element 1,1
     * @param f    matrix element 1,2
     * @param g    matrix element 2,0
     * @param h    matrix element 2,1
     * @param i    matrix element 2,2
     * @param unit the unit in which the elements are stated.
     * @return the 3D matrix having the specified elements stated in the specified units.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(double a, double b, double c, double d, double e, double f,
            double g, double h, double i, Unit<Q> unit) {
        Float64Vector row0 = Float64Vector.valueOf(a, b, c);
        Float64Vector row1 = Float64Vector.valueOf(d, e, f);
        Float64Vector row2 = Float64Vector.valueOf(g, h, i);

        Float64Matrix data = Float64Matrix.valueOf(row0, row1, row2);

        Matrix3D<Q> M = Matrix3D.newInstance(unit);
        M._data = data;

        return M;
    }

    /**
     * Returns a 3D matrix holding the specified row vectors in the stated units (column
     * vectors if {@link #transpose transposed}).
     *
     * @param row0 the 1st row vector.
     * @param row1 the 2nd row vector.
     * @param row2 the 3rd row vector.
     * @param unit the unit in which the elements are stated.
     * @return the 3D matrix having the specified elements stated in the specified units.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(Float64Vector row0, Float64Vector row1, Float64Vector row2, Unit<Q> unit) {

        Float64Matrix data = Float64Matrix.valueOf(row0, row1, row2);

        Matrix3D<Q> M = Matrix3D.newInstance(unit);
        M._data = data;

        return M;
    }

    /**
     * Returns a 3D matrix holding the specified row vectors (column vectors if
     * {@link #transpose transposed}). All the values are converted to the same units as
     * the 1st row (row0).
     *
     * @param row0 the 1st row vector.
     * @param row1 the 2nd row vector.
     * @param row2 the 3rd row vector.
     * @return the 3D matrix having the specified rows.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(Vector3D<Q> row0, Vector3D<Q> row1, Vector3D<Q> row2) {
        Unit<Q> unit = row0.getUnit();

        Float64Matrix data = Float64Matrix.valueOf(row0.toFloat64Vector(),
                row1.to(unit).toFloat64Vector(), row2.to(unit).toFloat64Vector());

        Matrix3D<Q> M = Matrix3D.newInstance(unit);
        M._data = data;

        return M;
    }

    /**
     * Returns a 3D matrix holding the row {@link Vector3D vectors} from the specified
     * collection (column vectors if {@link #transpose transposed}). All the values are
     * converted to the same units as the 1st row (row0).
     *
     * @param rows The list of row vectors. If there are more than 3 elements, an
     *             exception is thrown.
     * @return the 3D matrix having the specified rows.
     * @throws DimensionException if the rows do not have a dimension of 3.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(List<Vector3D<Q>> rows) {
        if (rows.size() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("3vectorsExpectedErr"), rows.size()));

        return Matrix3D.valueOf((Vector3D<Q>)rows.get(0), (Vector3D<Q>)rows.get(1), (Vector3D<Q>)rows.get(2));
    }

    /**
     * Returns a {@link Matrix3D} instance containing the specified matrix of Float64
     * values stated in the specified units. The input matrix must have dimensions of 3x3.
     *
     * @param matrix the matrix of Float64 values stated in the specified unit (must have
     *               dimension of 3x3).
     * @param unit   the unit in which the elements are stated.
     * @return the matrix having the specified values.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(Matrix<Float64> matrix, Unit<Q> unit) {
        if (matrix.getNumberOfColumns() != 3 || matrix.getNumberOfRows() != 3)
            throw new DimensionException(MessageFormat.format(RESOURCES.getString("3x3MatrixExpectedErr"),
                    matrix.getNumberOfRows(), matrix.getNumberOfColumns()));

        Matrix3D<Q> M = Matrix3D.newInstance(unit);
        M._data = Float64Matrix.valueOf(matrix);

        return M;
    }

    /**
     * Returns a {@link Matrix3D} instance containing the specified matrix of double
     * values stated in the specified units. The input matrix must have dimensions of 3x3.
     *
     * @param matrix the matrix of double values stated in the specified unit (must have
     *               dimension of 3x3).
     * @param unit   the unit in which the elements are stated.
     * @return the matrix having the specified values.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(double[][] matrix, Unit<Q> unit) {
        if (matrix.length != 3 || matrix[0].length != 3)
            throw new DimensionException(MessageFormat.format(RESOURCES.getString("3x3MatrixExpectedErr"),
                    matrix.length, matrix[0].length));

        Matrix3D<Q> M = Matrix3D.newInstance(unit);
        M._data = Float64Matrix.valueOf(matrix);

        return M;
    }

    /**
     * Returns a 3D matrix equivalent to the specified matrix. All the values are
     * converted to the same units as the 1st element (row=0,column=0).
     *
     * @param that the matrix to convert.
     * @return <code>that</code> or a 3D matrix holding the same elements as the specified
     *         matrix.
     */
    public static <Q extends Quantity> Matrix3D<Q> valueOf(Matrix<Parameter<Q>> that) {
        if (that instanceof Matrix3D)
            return (Matrix3D<Q>)that;

        if (that.getNumberOfColumns() != 3 || that.getNumberOfRows() != 3)
            throw new DimensionException(MessageFormat.format(RESOURCES.getString("3x3MatrixExpectedErr"),
                    that.getNumberOfRows(), that.getNumberOfColumns()));

        Vector3D<Q> row0 = Vector3D.valueOf(that.getRow(0));
        Vector3D<Q> row1 = Vector3D.valueOf(that.getRow(1));
        Vector3D<Q> row2 = Vector3D.valueOf(that.getRow(2));

        return Matrix3D.valueOf(row0, row1, row2);
    }

    /**
     * Returns the number of rows m for this matrix. This implementation always returns 3.
     */
    @Override
    public int getNumberOfRows() {
        return 3;
    }

    /**
     * Returns the number of columns n for this matrix. This implementation always returns
     * 3.
     */
    @Override
    public int getNumberOfColumns() {
        return 3;
    }

    /**
     * Returns the unit in which the {@link #get values} in this vector are stated.
     */
    public Unit<Q> getUnit() {
        return _unit;
    }

    /**
     * Returns the equivalent to this matrix but stated in the specified unit.
     *
     * @param unit the unit of the matrix to be returned.
     * @return a matrix equivalent to this vector but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public <R extends Quantity> Matrix3D<R> to(Unit<R> unit) {
        if ((_unit == unit) || this._unit.equals(unit))
            return (Matrix3D<R>)this;

        UnitConverter cvtr = Parameter.converterOf(_unit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            Matrix3D<R> result = (Matrix3D<R>)Matrix3D.copyOf(this);
            result._unit = unit;
            return result;
        }

        double a = cvtr.convert(_data.get(0, 0).doubleValue());
        double b = cvtr.convert(_data.get(0, 1).doubleValue());
        double c = cvtr.convert(_data.get(0, 2).doubleValue());
        double d = cvtr.convert(_data.get(1, 0).doubleValue());
        double e = cvtr.convert(_data.get(1, 1).doubleValue());
        double f = cvtr.convert(_data.get(1, 2).doubleValue());
        double g = cvtr.convert(_data.get(2, 0).doubleValue());
        double h = cvtr.convert(_data.get(2, 1).doubleValue());
        double i = cvtr.convert(_data.get(2, 2).doubleValue());

        Matrix3D<R> M = Matrix3D.valueOf(a, b, c, d, e, f, g, h, i, unit);
        return M;
    }

    /**
     * Casts this Matrix3D to a parameterized unit of specified nature or throw a
     * <code>ClassCastException</code> if the specified quantity and this matrix unit
     * dimension do not match.
     *
     * @param type the quantity class identifying the nature of the unit.
     * @return this matrix parameterized with the specified type.
     * @throws ClassCastException if the dimension of this parameter's unit is different
     * from the specified quantity dimension.
     * @throws UnsupportedOperationException if the specified quantity class does not have
     * a public static field named "UNIT" holding the standard unit for the quantity.
     */
    public <T extends Quantity> Matrix3D<T> asType(Class<T> type) throws ClassCastException {
        Unit<T> u = _unit.asType(type); //  If no exception is thrown, the cast is valid.
        return (Matrix3D<T>)this;
    }

    /**
     * Returns a single element from this matrix.
     *
     * @param i the row index (range [0..3[).
     * @param j the column index (range [0..3[).
     * @return the element read at [i,j].
     */
    @Override
    public Parameter<Q> get(int i, int j) {
        return Parameter.valueOf(_data.get(i, j).doubleValue(), _unit);
    }

    /**
     * Returns a single element from this matrix as a <code>double</code>.
     *
     * @param i the row index (range [0..3[).
     * @param j the column index (range [0..3[).
     * @return the element read at [i,j].
     */
    public double getValue(int i, int j) {
        return _data.get(i, j).doubleValue();
    }

    /**
     * Returns the row identified by the specified index in this matrix.
     *
     * @param i the row index (range [0..3[).
     */
    @Override
    public Vector3D<Q> getRow(int i) {
        return Vector3D.valueOf(_data.getRow(i), _unit);
    }

    /**
     * Returns the column identified by the specified index in this matrix.
     *
     * @param j the column index (range [0..3[).
     */
    @Override
    public Vector3D<Q> getColumn(int j) {
        return Vector3D.valueOf(_data.getColumn(j), _unit);
    }

    /**
     * Returns the diagonal vector.
     *
     * @return the vector holding the diagonal elements.
     */
    @Override
    public Vector3D<Q> getDiagonal() {
        return Vector3D.valueOf(_data.getDiagonal(), _unit);
    }

    /**
     * Returns the negation of this matrix.
     *
     * @return <code>-this</code>
     */
    @Override
    public Matrix3D<Q> opposite() {
        Matrix3D<Q> M = Matrix3D.newInstance(_unit);
        M._data = _data.opposite();
        return M;
    }

    /**
     * Returns the sum of this matrix with the one specified.
     *
     * @param that the matrix to be added.
     * @return <code>this + that</code>
     * @throws DimensionException if the matrix dimensions are different.
     */
    @Override
    public Matrix3D<Q> plus(Matrix<Parameter<Q>> that) {

        //  Convert input matrix to a Float64Matrix (with unit conversion if necessary).
        Float64Matrix thatData = toFloat64Matrix(that, this._unit);

        Matrix3D<Q> M = Matrix3D.newInstance(_unit);
        M._data = this._data.plus(thatData);

        return M;
    }

    /**
     * Returns the difference between this matrix and the one specified.
     *
     * @param that the matrix to be subtracted from this one.
     * @return <code>this - that</code>
     * @throws DimensionException if the matrix dimensions are different.
     */
    @Override
    public Matrix3D<Q> minus(Matrix<Parameter<Q>> that) {

        //  Convert input matrix to a Float64Matrix (with unit conversion if necessary).
        Float64Matrix thatData = toFloat64Matrix(that, this._unit);

        Matrix3D<Q> M = Matrix3D.newInstance(_unit);
        M._data = this._data.minus(thatData);

        return M;
    }

    /**
     * Returns the product of this matrix by the specified factor.
     *
     * @param k the coefficient multiplier
     * @return <code>this · k</code>
     */
    @Override
    public Matrix3D times(Parameter k) {
        Matrix3D<?> M = FACTORY.object();

        //  Find the combined units.
        M._unit = (Unit)Parameter.productOf(this.getUnit(), k.getUnit());
        M._data = this._data.times(Float64.valueOf(k.getValue()));

        return M;
    }

    /**
     * Returns the product of this matrix by the specified factor.
     *
     * @param k the coefficient multiplier
     * @return <code>this · k</code>
     */
    public Matrix3D<Q> times(double k) {
        Matrix3D<Q> M = FACTORY.object();

        M._unit = this._unit;
        M._data = this._data.times(Float64.valueOf(k));

        return M;
    }

    /**
     * Returns the product of this matrix by the specified vector.
     *
     * @param v the vector.
     * @return <code>this · v</code>
     * @throws DimensionException - if v.getDimension() != this.getNumberOfColumns()
     */
    @Override
    public Vector3D times(Vector v) {

        //  Make sure the input vector is a Vector3D instance.
        Vector3D T = Vector3D.valueOf(v);

        //  Find the combined units.
        Unit unit = Parameter.productOf(this.getUnit(), ((Parameter)v.get(0)).getUnit());

        //  Multiply the vector and the matrix.
        Float64Vector V = _data.times(T.toFloat64Vector());

        return Vector3D.valueOf(V, unit);
    }

    /**
     * Returns the product of this matrix with the one specified.
     *
     * @param that the matrix multiplier.
     * @return <code>this · that</code>
     * @throws DimensionException - if this.getNumberOfColumns() !=
     * that.getNumberOfRows().
     */
    @Override
    public Matrix3D times(Matrix that) {

        //  Convert input matrix to a Float64Matrix.
        Float64Matrix thatData = toFloat64Matrix(that, null);

        //  Find the combined units.
        Unit unit = Parameter.productOf(this.getUnit(), ((Parameter)that.get(0, 0)).getUnit());

        Matrix3D M = Matrix3D.newInstance(unit);
        M._data = this._data.times(thatData);

        return M;
    }

    /**
     * Returns the inverse of this matrix. If the matrix is singular, the
     * {@link #determinant()} will be zero (or nearly zero).
     *
     * @return <code>1 / this</code>
     */
    @Override
    public Matrix3D inverse() {
        Unit unit = _unit.inverse();

        Matrix3D M = Matrix3D.newInstance(unit);
        M._data = _data.inverse();

        return M;
    }

    /**
     * Returns the determinant of this matrix.
     *
     * @return this matrix determinant.
     */
    @Override
    public Parameter determinant() {
        Unit unit = _unit.pow(3);

        Parameter P = Parameter.valueOf(_data.determinant().doubleValue(), unit);

        return P;
    }

    /**
     * Returns the transpose of this matrix.
     *
     * @return <code>A'</code>
     */
    @Override
    public Matrix3D<Q> transpose() {

        Matrix3D<Q> M = Matrix3D.newInstance(_unit);
        M._data = _data.transpose();

        return M;
    }

    /**
     * Returns the cofactor of an element in this matrix. It is the value obtained by
     * evaluating the determinant formed by the elements not in that particular row or
     * column.
     *
     * @param i the row index.
     * @param j the column index.
     * @return the cofactor of THIS[i,j].
     */
    @Override
    public Parameter cofactor(int i, int j) {
        Unit unit = _unit.pow(2);

        Parameter P = Parameter.valueOf(_data.cofactor(i, j).doubleValue(), unit);

        return P;
    }

    /**
     * Returns the adjoint of this matrix. It is obtained by replacing each element in
     * this matrix with its cofactor and applying a + or - sign according to (-1)**(i+j),
     * and then finding the transpose of the resulting matrix.
     *
     * @return the adjoint of this matrix.
     */
    @Override
    public Matrix3D adjoint() {
        Unit unit = _unit.pow(2);

        Matrix3D M = Matrix3D.newInstance(unit);
        M._data = _data.adjoint();

        return M;
    }

    /**
     * Returns the linear algebraic matrix tensor product of this matrix and another
     * (Kronecker product).
     *
     * @param that the second matrix
     * @return <code>this times that</code>
     * @see <a href="http://en.wikipedia.org/wiki/Kronecker_product">
     * Wikipedia: Kronecker Product</a>
     */
    @Override
    public Matrix tensor(Matrix that) {
        StackContext.enter();
        try {
            Float64Matrix thatData;

            //  Convert "that" into a matrix of Float64 values.
            if (that instanceof Matrix3D) {
                Matrix3D T = (Matrix3D)that;
                thatData = T._data;

            } else {
                FastTable<Float64Vector> rows = FastTable.newInstance();
                FastTable<Float64> rowi = FastTable.newInstance();
                int numCols = that.getNumberOfColumns();
                int numRows = that.getNumberOfRows();
                for (int i = 0; i < numRows; ++i) {
                    for (int j = 0; j < numCols; ++j) {
                        Parameter p = (Parameter)that.get(i, j);
                        rowi.add(Float64.valueOf(p.getValue()));
                    }
                    rows.add(Float64Vector.valueOf(rowi));
                    rowi.clear();
                }

                thatData = Float64Matrix.valueOf(rows);
            }

            Unit unit = Parameter.productOf(this.getUnit(), ((Parameter)that.get(0, 0)).getUnit());

            Float64Matrix matrix = _data.tensor(thatData);

            FastTable rows = FastTable.newInstance();
            FastTable<Parameter> rowi = FastTable.newInstance();
            int numRows = matrix.getNumberOfRows();
            int numCols = matrix.getNumberOfColumns();
            for (int i = 0; i < numRows; ++i) {
                for (int j = 0; j < numCols; ++j) {
                    Parameter p = Parameter.valueOf(matrix.get(i, j).doubleValue(), unit);
                    rowi.add(p);
                }
                rows.add(DenseVector.valueOf(rowi));
                rowi.clear();
            }

            DenseMatrix M = DenseMatrix.valueOf(rows);

            return StackContext.outerCopy(M);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Compares this Matrix3D for approximate equality to zero (all the values are within
     * the numerical roundoff error of zero).
     *
     * @return <code>true</code> if this Matrix3D is approximately equal to zero;
     *         <code>false</code> otherwise.
     */
    public boolean isApproxZero() {

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                double value = this.getValue(i, j);
                if (MathLib.abs(value) > Parameter.EPS)
                    return false;
            }
        }

        return true;
    }

    /**
     * Returns the vectorization of this matrix. The vectorization of a matrix is the
     * column vector obtained by stacking the columns of the matrix on top of one another.
     * The default implementation returns a <code>DenseVector<Parameter<?>></code>.
     *
     * @return the vectorization of this matrix.
     */
    @Override
    public Vector<Parameter<Q>> vectorization() {
        Float64Vector V = _data.vectorization();
        FastTable<Parameter<Q>> list = FastTable.newInstance();

        int num = V.getDimension();
        for (int i = 0; i < num; ++i) {
            double value = V.getValue(i);
            Parameter<Q> p = Parameter.valueOf(value, _unit);
            list.add(p);
        }
        return DenseVector.valueOf(list);
    }

    /**
     * Returns a copy of this matrix allocated by the calling thread (possibly on the
     * stack).
     *
     * @return an identical and independent copy of this matrix.
     */
    @Override
    public Matrix3D<Q> copy() {
        return Matrix3D.copyOf(this);
    }

    /**
     * Returns a Float64Matrix containing the data in this matrix in the current units of
     * this matrix.
     */
    public Float64Matrix toFloat64Matrix() {
        return _data;
    }

    /**
     * Convert a matrix of parameter objects to a Float64Matrix stated in the specified
     * units.
     */
    private static <Q extends Quantity> Float64Matrix toFloat64Matrix(Matrix<Parameter<Q>> that, Unit<Q> unit) {

        //  Make sure input is a Matrix3D instance.
        Matrix3D<Q> T = Matrix3D.valueOf(that);

        //  Convert that vector's units to the specified units if necessary.
        Float64Matrix thatData = T._data;
        if ((unit != null) && (unit != T._unit) && !unit.equals(T._unit)) {
            UnitConverter cvtr = Parameter.converterOf(T._unit, unit);
            if (cvtr != UnitConverter.IDENTITY) {
                double a = cvtr.convert(thatData.get(0, 0).doubleValue());
                double b = cvtr.convert(thatData.get(0, 1).doubleValue());
                double c = cvtr.convert(thatData.get(0, 2).doubleValue());
                double d = cvtr.convert(thatData.get(1, 0).doubleValue());
                double e = cvtr.convert(thatData.get(1, 1).doubleValue());
                double f = cvtr.convert(thatData.get(1, 2).doubleValue());
                double g = cvtr.convert(thatData.get(2, 0).doubleValue());
                double h = cvtr.convert(thatData.get(2, 1).doubleValue());
                double i = cvtr.convert(thatData.get(2, 2).doubleValue());
                Float64Vector row0 = Float64Vector.valueOf(a, b, c);
                Float64Vector row1 = Float64Vector.valueOf(d, e, f);
                Float64Vector row2 = Float64Vector.valueOf(g, h, i);
                thatData = Float64Matrix.valueOf(row0, row1, row2);
            }
        }

        return thatData;
    }

    /**
     * During serialization, this will write out the Float64Matrix as a simple series of
     * <code>double</code> values. This method is ONLY called by the Java Serialization
     * mechanism and should not otherwise be used.
     */
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {

        // Call the default write object method.
        out.defaultWriteObject();

        //  Write out the three coordinate values.
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                out.writeDouble(_data.get(i, j).doubleValue());

    }

    /**
     * During de-serialization, this will handle the reconstruction of the Float64Matrix.
     * This method is ONLY called by the Java Serialization mechanism and should not
     * otherwise be used.
     */
    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {

        // Call the default read object method.
        in.defaultReadObject();

        //  Read in the three coordinate values.
        FastTable<Float64Vector> rows = FastTable.newInstance();
        for (int i = 0; i < 3; ++i) {
            double v1 = in.readDouble();
            double v2 = in.readDouble();
            double v3 = in.readDouble();
            Float64Vector row = Float64Vector.valueOf(v1, v2, v3);
            rows.add(row);
        }

        _data = Float64Matrix.valueOf(rows);
        FastTable.recycle(rows);

    }

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private Matrix3D() { }

    private static final ObjectFactory<Matrix3D> FACTORY = new ObjectFactory<Matrix3D>() {
        @Override
        protected Matrix3D create() {
            return new Matrix3D();
        }
    };

    private static <Q extends Quantity> Matrix3D<Q> newInstance(Unit<Q> unit) {
        Matrix3D<Q> measure = FACTORY.object();
        measure._unit = unit;
        return measure;
    }

    private static <Q extends Quantity> Matrix3D<Q> copyOf(Matrix3D<Q> original) {
        Matrix3D<Q> measure = Matrix3D.newInstance(original._unit);
        measure._data = original._data.copy();
        return measure;
    }

    /**
     * Tests the methods in this class.
     */
    public static void main(String args[]) {
        System.out.println("Testing Matrix3D:");

        Matrix3D<Length> m1 = Matrix3D.valueOf(1, 2, 3, 4, 5, 6, 7, 8, 9, SI.METER);
        System.out.println("m1 = \n" + m1);
        System.out.println("  converted to feet  = \n" + m1.to(NonSI.FOOT));
        System.out.println("  m1.getRow(1) = " + m1.getRow(1));
        System.out.println("  m1.getColumn(2) = " + m1.getColumn(2));
        System.out.println("  m1.getDiagonal() = " + m1.getDiagonal());
        System.out.println("  m1.opposite() = \n" + m1.opposite());
        System.out.println("  m1.transpose() = \n" + m1.transpose());
        System.out.println("  m1.vectorization() = \n" + m1.vectorization());
        System.out.println("  m1.determinant() = " + m1.determinant() + "; matrix is singular and can not be inverted");

        Parameter<Length> p1 = Parameter.valueOf(2, SI.METER);
        System.out.println("\np1 = " + p1);
        System.out.println("  m1.times(p1) = \n" + m1.times(p1));

        Matrix3D<Length> m2 = Matrix3D.valueOf(1, 0, 1, 0, 1, 0, -1, 0, 1, SI.METER);
        System.out.println("\nm2 = \n" + m2);
        System.out.println("  m2.determinant() = " + m2.determinant());
        System.out.println("  m2.inverse() = \n" + m2.inverse());
        System.out.println("  m2.cofactor(0,2) = " + m2.cofactor(0, 2));
        System.out.println("  m2.adjoint() = \n" + m2.adjoint());
        System.out.println("  m1.times(m2) = \n" + m1.times(m2));
        System.out.println("  m1.tensor(m2) = \n" + m1.tensor(m2));

        Vector3D<Length> v1 = Vector3D.valueOf(1, 2, 3, SI.METER);
        System.out.println("\nv1 = " + v1);
        System.out.println("  m1.times(v1) = " + m1.times(v1));

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
            binding.setAlias(jahuwaldt.js.param.Parameter.class, "Parameter");

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(m2, "Matrix3D", Matrix3D.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
