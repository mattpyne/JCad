/*
 *   DCMatrix -- A 3x3 direction cosine matrix used to represent the orientation between
 *               two reference frames.
 *
 *   Copyright (C) 2008-2016, Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import java.util.List;
import javax.measure.quantity.*;
import javax.measure.unit.SI;
import javolution.context.ImmortalContext;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.*;

/**
 * <p>
 * This class represents a 3x3 transformation or direction cosine {@link Matrix matrix}
 * that represents a relative orientation (attitude or rotation transformation) between
 * two different reference frames; B wrt A or BA. It can be used to transform coordinates
 * in reference frame A to reference frame B (A2B).</p>
 * <p>
 * Assumes matrix is used to multiply column vector on the left:
 * <code>vnew = mat * vold</code>. Works correctly for right-handed coordinate system and
 * right-handed rotations.</p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: November 26, 2008
 * @version October 25, 2016
 */
public final class DCMatrix extends AbstractRotation<DCMatrix> implements XMLSerializable {

    private static final long serialVersionUID = -1311083554884668584L;

    /**
     * Constant used to identify the X coordinate in a vector.
     */
    private static final int X = 0;

    /**
     * Constant used to identify the Y coordinate in a vector.
     */
    private static final int Y = 1;

    /**
     * Constant used to identify the Z coordinate in a vector.
     */
    private static final int Z = 2;

    /**
     * Constant used to identify the W coordinate in a quaternion.
     */
    private static final int W = 3;

    /**
     * The elements stored in this matrix. Serialization is handled by this class since
     * Float64Matrix is not Serializable.
     */
    private transient Float64Matrix _data;

    /**
     * <p>
     * Returns a 3D direction cosine matrix from a sequence of 9 <code>double</code>
     * values.</p>
     * <p>
     * Values are ordered as follows:
     * <pre>
     *         | a b c |
     *         | d e f |
     *         | g h i |
     * </pre></p>
     *
     * @param a matrix element 0,0
     * @param b matrix element 0,1
     * @param c matrix element 0,2
     * @param d matrix element 1,0
     * @param e matrix element 1,1
     * @param f matrix element 1,2
     * @param g matrix element 2,0
     * @param h matrix element 2,1
     * @param i matrix element 2,2
     * @return the 3D matrix having the specified elements.
     */
    public static DCMatrix valueOf(double a, double b, double c, double d, double e, double f,
            double g, double h, double i) {

        StackContext.enter();
        try {
            Float64Vector row0 = Float64Vector.valueOf(a, b, c);
            Float64Vector row1 = Float64Vector.valueOf(d, e, f);
            Float64Vector row2 = Float64Vector.valueOf(g, h, i);

            DCMatrix M = DCMatrix.newInstance(Float64Matrix.valueOf(row0, row1, row2));

            return StackContext.outerCopy(M);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a 3D direction cosine matrix holding the specified row vectors (column
     * vectors if {@link #transpose transposed}).
     *
     * @param row0 the 1st row vector.
     * @param row1 the 2nd row vector.
     * @param row2 the 3rd row vector.
     * @return the 3D matrix having the specified rows.
     */
    public static DCMatrix valueOf(double[] row0, double[] row1, double[] row2) {

        StackContext.enter();
        try {
            Float64Vector row0V = Float64Vector.valueOf(row0);
            Float64Vector row1V = Float64Vector.valueOf(row1);
            Float64Vector row2V = Float64Vector.valueOf(row2);
            
            DCMatrix M = DCMatrix.newInstance(Float64Matrix.valueOf(row0V, row1V, row2V));

            return StackContext.outerCopy(M);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a 3D direction cosine matrix holding the specified row vectors (column
     * vectors if {@link #transpose transposed}).
     *
     * @param row0 the 1st row vector.
     * @param row1 the 2nd row vector.
     * @param row2 the 3rd row vector.
     * @return the 3D matrix having the specified rows.
     */
    public static DCMatrix valueOf(Float64Vector row0, Float64Vector row1, Float64Vector row2) {

        DCMatrix M = DCMatrix.newInstance(Float64Matrix.valueOf(row0, row1, row2));

        return M;
    }

    /**
     * Returns a 3D direction cosine matrix holding the specified diagonal vector with
     * zeros placed in all the other elements.
     *
     * @param diagX the 1st element of the diagonal vector for the matrix.
     * @param diagY the 2nd element of the diagonal vector for the matrix.
     * @param diagZ the 3rd element of the diagonal vector for the matrix.
     * @return the 3D matrix having the specified diagonal.
     */
    public static DCMatrix valueOf(double diagX, double diagY, double diagZ) {

        StackContext.enter();
        try {
            Float64Vector row0 = Float64Vector.valueOf(diagX, 0, 0);
            Float64Vector row1 = Float64Vector.valueOf(0, diagY, 0);
            Float64Vector row2 = Float64Vector.valueOf(0, 0, diagZ);

            DCMatrix M = DCMatrix.newInstance(Float64Matrix.valueOf(row0, row1, row2));

            return StackContext.outerCopy(M);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a 3D direction cosine matrix holding the specified diagonal vector with
     * zeros placed in all the other elements.
     *
     * @param diag the diagonal vector for the matrix.
     * @return the 3D matrix having the specified diagonal.
     */
    public static DCMatrix valueOf(Float64Vector diag) {
        return DCMatrix.valueOf(diag.getValue(X), diag.getValue(Y), diag.getValue(Z));
    }

    /**
     * Returns a 3D direction cosine matrix holding the row vectors from the specified
     * collection (column vectors if {@link #transpose transposed}).
     *
     * @param rows The list of row vectors. If there are more than 3 elements, an
     *             exception is thrown.
     * @return the 3D matrix having the specified rows.
     * @throws DimensionException if the rows do not have a dimension of 3.
     */
    public static DCMatrix valueOf(List<Float64Vector> rows) {
        if (rows.size() != 3)
            throw new DimensionException(
                    MessageFormat.format(RESOURCES.getString("3vectorsExpectedErr"), rows.size()));

        return DCMatrix.valueOf(rows.get(0), rows.get(1), rows.get(2));
    }

    /**
     * Returns a {@link DCMatrix} instance containing a copy of the specified matrix of
     * Float64 values. The matrix must have dimensions of 3x3.
     *
     * @param matrix the matrix of Float64 values to convert (must have dimension of 3x3).
     * @return the matrix having the specified elements.
     */
    public static DCMatrix valueOf(Matrix<Float64> matrix) {
        if (matrix.getNumberOfColumns() != 3 || matrix.getNumberOfRows() != 3)
            throw new DimensionException(MessageFormat.format(RESOURCES.getString("3x3MatrixExpectedErr"),
                    matrix.getNumberOfRows(), matrix.getNumberOfColumns()));

        DCMatrix M = DCMatrix.newInstance(Float64Matrix.valueOf(matrix));

        return M;
    }

    /**
     * Returns a {@link DCMatrix} instance equivalent to the specified rotation
     * transform.
     *
     * @param transform The Rotation to convert to a direction cosine matrix.
     * @return the direction cosine matrix representing the specified rotation transform.
     */
    public static DCMatrix valueOf(Rotation<?> transform) {
        if (transform instanceof DCMatrix)
            return (DCMatrix)transform;
        
        return transform.toDCM();
    }

    /**
     * Return a new {@link DCMatrix} instance constructed from the specified axis pair.
     *
     * @param vector1 The vector defining the 1st axis.
     * @param vector2 The vector defining the 2nd axis.
     * @param axis1   Constant from this class designating which axis the 1st axis is
     *                (e.g.: X=0, or Z=2).
     * @param axis2   Constant from this class designating which axis the 2nd axis is
     *                (e.g.: Y=1 or X=0).
     * @return A direction cosine matrix representing a rotation from the base axis system
     *         to the specified axis orientations.
     */
    public static DCMatrix valueOf(Coordinate3D vector1, Coordinate3D vector2, int axis1, int axis2) {
        if (axis1 == axis2)
            throw new IllegalArgumentException(RESOURCES.getString("equalAxisDesignationErr"));
        if (axis1 < X || axis1 > Z)
            throw new IllegalArgumentException(MessageFormat.format(RESOURCES.getString("axisDesignationRangeErr"),1));
        if (axis2 < X || axis2 > Z)
            throw new IllegalArgumentException(MessageFormat.format(RESOURCES.getString("axisDesignationRangeErr"),2));

        StackContext.enter();
        try {
            //  Convert input vectors to unit vectors.
            Vector3D<Dimensionless> uv1 = vector1.toVector3D().toUnitVector();
            Vector3D<Dimensionless> uv2 = vector2.toVector3D().toUnitVector();

            //  Create a 3rd axis that is orthogonal to the plane defined by the 1st two.
            Vector3D<Dimensionless> uv3 = ((Coordinate3D)vector1.cross(vector2)).toVector3D();

            //  Correct vector2 to be exactly orthogonal to vector1 and vector3.
            double delta = 1.0 - uv3.magValue();
            if (delta >= 1.0E-10) {
                if (MathTools.isApproxEqual(delta, 1.0, 1e-9)) {
                    throw new IllegalStateException(RESOURCES.getString("axesParallelErr"));
                }
                uv3 = uv3.toUnitVector();
                uv2 = uv2.cross(uv1);
            }

            int axis3 = 3 - (axis1 + axis2);
            int sign = axis2 - axis1;
            if ((sign == 2) || (sign == -2))
                sign = -sign / 2;

            //  Create a list of rows of the DCM.
            FastTable<FastTable<Float64>> matrix = FastTable.newInstance();
            for (int i = 0; i < 3; ++i) {
                FastTable<Float64> row = FastTable.newInstance();
                row.setSize(3);
                matrix.add(row);
            }

            //  Fill in the DCM values.
            for (int i = 0; i < 3; ++i) {
                FastTable<Float64> row = matrix.get(i);
                row.set(axis1, Float64.valueOf(uv1.getValue(i)));
                row.set(axis2, Float64.valueOf(uv2.getValue(i)));
                row.set(axis3, Float64.valueOf(uv3.getValue(i) * sign));
            }

            //  Create and output the DCM.
            FastTable<Float64Vector> mat2 = FastTable.newInstance();
            for (int i = 0; i < 3; ++i)
                mat2.add(Float64Vector.valueOf(matrix.get(i)));
            DCMatrix dcm = DCMatrix.valueOf(mat2);

            return StackContext.outerCopy(dcm);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the number of rows <code>m</code> for this matrix. This implementation
     * always returns 3.
     *
     * @return The number of rows for this matrix
     */
    public int getNumberOfRows() {
        return 3;
    }

    /**
     * Returns the number of columns <code>n</code> for this matrix. This implementation
     * always returns 3.
     *
     * @return The number of columns for this matrix
     */
    public int getNumberOfColumns() {
        return 3;
    }

    /**
     * Returns a single element from this matrix.
     *
     * @param i the row index (range [0..3[).
     * @param j the column index (range [0..3[).
     * @return the matrix element at [i,j].
     */
    public Float64 get(int i, int j) {
        return _data.get(i, j);
    }

    /**
     * <p>
     * Returns a single element from this matrix as a <code>double</code>.</p>
     *
     * <p>
     * This is a convenience method for: <code> this.get(i,j).doubleValue();</code></p>
     *
     * @param i the row index (range [0..3[).
     * @param j the column index (range [0..3[).
     * @return the matrix element at [i,j].
     */
    public double getValue(int i, int j) {
        return _data.get(i, j).doubleValue();
    }

    /**
     * Returns the row identified by the specified index in this matrix.
     *
     * @param i the row index (range [0..3[).
     * @return The specified row of this matrix.
     */
    public Float64Vector getRow(int i) {
        return _data.getRow(i);
    }

    /**
     * Returns the column identified by the specified index in this matrix.
     *
     * @param j the column index (range [0..3[).
     * @return The specified column of this matrix.
     */
    public Float64Vector getColumn(int j) {
        return _data.getColumn(j);
    }

    /**
     * Returns the diagonal vector.
     *
     * @return the vector holding the diagonal elements.
     */
    public Float64Vector getDiagonal() {
        return _data.getDiagonal();
    }

    /**
     * Returns the negation of this matrix.
     *
     * @return <code>-this</code>
     */
    public DCMatrix opposite() {
        DCMatrix M = DCMatrix.newInstance(_data.opposite());
        return M;
    }

    /**
     * Returns the sum of this matrix with the one specified.
     *
     * @param that the matrix to be added.
     * @return <code>this + that</code>
     */
    public DCMatrix plus(DCMatrix that) {

        DCMatrix M = DCMatrix.newInstance(this._data.plus(that._data));

        return M;
    }

    /**
     * Returns the difference between this matrix and the one specified.
     *
     * @param that the matrix to be subtracted from this one.
     * @return <code>this - that</code>
     */
    public DCMatrix minus(DCMatrix that) {

        DCMatrix M = DCMatrix.newInstance(this._data.minus(that._data));

        return M;
    }

    /**
     * Returns the product of this matrix by the specified factor.
     *
     * @param k the coefficient multiplier
     * @return <code>this · k</code>
     */
    public DCMatrix times(Float64 k) {
        DCMatrix M = DCMatrix.newInstance(this._data.times(k));
        return M;
    }

    /**
     * Returns the product of this matrix by the specified vector.
     *
     * @param v the vector.
     * @return <code>this · v</code>
     * @throws DimensionException - if v.getDimension() != this.getNumberOfColumns()
     */
    public Float64Vector times(Vector<Float64> v) {

        //  Convert the input vector to a Float64Vector.
        Float64Vector V;
        if (v instanceof Float64Vector)
            V = (Float64Vector)v;
        else
            V = Float64Vector.valueOf(v);

        return _data.times(V);
    }

    /**
     * Returns the product of this matrix by the specified vector.
     *
     * @param <Q> The Quantity (unit type) of the vector.
     * @param v the vector.
     * @return <code>this · v</code>
     */
    public <Q extends Quantity> Vector3D<Q> times(Coordinate3D<Q> v) {
        return this.transform(v);
    }

    /**
     * Transforms a 3D vector from frame A to B using this rotation transformation.
     *
     * @param <Q> The Quantity (unit type) of the vector.
     * @param v the vector expressed in frame A.
     * @return the vector expressed in frame B.
     */
    @Override
    public <Q extends Quantity> Vector3D<Q> transform(Coordinate3D<Q> v) {

        //  Convert the input vector to a Float64Vector.
        Float64Vector V = v.toVector3D().toFloat64Vector();

        //  Create an output vector.
        Vector3D<Q> result = Vector3D.valueOf(_data.times(V), v.getUnit());

        return result;
    }

    /**
     * Returns the product of this direction cosine matrix and another rotation transform.
     * If this rotation transform is BA and that is AC then the returned value is:
     *  <code>BC = BA · AC</code> (or <code>C2B = A2B  · C2A</code>).
     *
     * @param that the rotation transform multiplier.
     * @return <code>this · that</code>
     */
    @Override
    public DCMatrix times(Rotation<?> that) {
        DCMatrix thatDCM = that.toDCM();
        return DCMatrix.newInstance(this._data.times(thatDCM._data));
    }

    /**
     * Returns the inverse of this matrix. If the matrix is singular, the
     * {@link #determinant()} will be zero (or nearly zero).
     *
     * @return <code>1 / this</code>
     */
    public DCMatrix inverse() {

        DCMatrix M = DCMatrix.newInstance(_data.inverse());

        return M;
    }

    /**
     * Returns the determinant of this matrix.
     *
     * @return this matrix determinant.
     */
    public Float64 determinant() {
        return _data.determinant();
    }

    /**
     * Returns the transpose of this matrix. This results in the spatial inverse of this
     * transformation: AB rather than BA.
     *
     * @return <code>this'</code>
     */
    @Override
    public DCMatrix transpose() {

        DCMatrix M = DCMatrix.newInstance(_data.transpose());

        return M;
    }

    /**
     * Returns the cofactor of an element in this matrix. It is the value obtained by
     * evaluating the determinant formed by the elements not in that particular row or
     * column.
     *
     * @param i the row index.
     * @param j the column index.
     * @return the cofactor of THIS[i,j].
     */
    public Float64 cofactor(int i, int j) {
        return _data.cofactor(i, j);
    }

    /**
     * Returns the adjoint of this matrix. It is obtained by replacing each element in
     * this matrix with its cofactor and applying a + or - sign according to
     * <code>(-1)**(i+j)</code>, and then finding the transpose of the resulting matrix.
     *
     * @return the adjoint of this matrix.
     */
    public DCMatrix adjoint() {

        DCMatrix M = DCMatrix.newInstance(_data.adjoint());

        return M;
    }

    /**
     * Returns the linear algebraic matrix tensor product of this matrix and another
     * matrix (Kronecker product).
     *
     * @param that the second matrix
     * @return <code>this times that</code>
     * @see <a href="http://en.wikipedia.org/wiki/Kronecker_product">
     * Wikipedia: Kronecker Product</a>
     */
    public Float64Matrix tensor(DCMatrix that) {
        return _data.tensor(that._data);
    }

    /**
     * Returns the vectorization of this matrix. The vectorization of a matrix is the
     * column vector obtained by stacking the columns of the matrix on top of one another.
     *
     * @return the vectorization of this matrix.
     */
    public Float64Vector vectorization() {
        return _data.vectorization();
    }

    /**
     * Returns a copy of this matrix allocated by the calling thread (possibly on the
     * stack).
     *
     * @return an identical and independent copy of this matrix.
     */
    @Override
    public DCMatrix copy() {
        return DCMatrix.copyOf(this);
    }

    /**
     * Returns the equivalent of this direction cosine matrix as a Float64Matix.
     * 
     * @return The equivalent of this direction cosine matrix as a Float64Matix.
     */
    public Float64Matrix toFloat64Matrix() {
        return _data;
    }

    /**
     * Returns the equivalent of this direction cosine matrix as a Java 2D matrix.
     * 
     * @return The equivalent of this direction cosine matrix as a Java 2D matrix.
     */
    public double[][] toMatrix() {
        StackContext.enter();
        try {
            double[][] mat = new double[3][3];
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    mat[i][j] = _data.get(i, j).doubleValue();
                }
            }
            return mat;
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a direction cosine transformation matrix from this rotation transformation.
     *
     * @return a direction cosine matrix that converts from frame A to B.
     * @see <a href="http://en.wikipedia.org/wiki/Rotation_representation_(mathematics)">
     * Wikipedia: Rotation representation (mathematics)</a>
     */
    @Override
    public DCMatrix toDCM() {
        return this;
    }

    /**
     * Returns a {@link Quaternion} constructed from the this transformation matrix.
     *
     * @return The quaternion representing this direction cosine rotation matrix.
     */
    @Override
    public Quaternion toQuaternion() {
        Quaternion Q = null;

        //  From:  "Quaternions", by Ken Shoemake
        //  http://www.cs.caltech.edu/courses/cs171/quatut.pdf
        /* This algorithm avoids near-zero divides by looking for a large component 
         *  — first w, then x, y, or z.  When the trace is greater than zero, 
         * |w| is greater than 1/2, which is as small as a largest component can be. 
         * Otherwise, the largest diagonal entry corresponds to the largest of |x|, 
         * |y|, or |z|, one of which must be larger than |w|, and at least 1/2.
         */
        double tr = this.getValue(X, X) + this.getValue(Y, Y) + this.getValue(Z, Z);
        if (tr >= 0.0) {
            double s = sqrt(1.0 + tr);
            double w = 0.5 * s;
            s = 0.5 / s;

            double x = (this.getValue(Z, Y) - this.getValue(Y, Z)) * s;
            double y = (this.getValue(X, Z) - this.getValue(Z, X)) * s;
            double z = (this.getValue(Y, X) - this.getValue(X, Y)) * s;

            Q = Quaternion.valueOf(x, y, z, w);

        } else {
            int h = X;
            if (this.getValue(Y, Y) > this.getValue(X, X))
                h = Y;
            if (this.getValue(Z, Z) > this.getValue(h, h))
                h = Z;
            switch (h) {
                case X:
                    Q = switchCase(X, Y, Z, this);
                    break;
                case Y:
                    Q = switchCase(Y, Z, X, this);
                    break;
                case Z:
                    Q = switchCase(Z, X, Y, this);
                    break;
            }
        }

        return Q;
    }

    private static Quaternion switchCase(int i, int j, int k, DCMatrix dc) {
        double[] qu = QU_FACTORY.object();

        double s = sqrt(1.0 + (dc.getValue(i, i) - (dc.getValue(j, j) + dc.getValue(k, k))));
        qu[i] = 0.5 * s;
        s = 0.5 / s;

        qu[j] = (dc.getValue(i, j) + dc.getValue(j, i)) * s;
        qu[k] = (dc.getValue(k, i) + dc.getValue(i, k)) * s;
        qu[W] = (dc.getValue(k, j) - dc.getValue(j, k)) * s;

        Quaternion q = Quaternion.valueOf(qu);

        QU_FACTORY.recycle(qu);     //  Recycle the array.

        return q;
    }

    /**
     * Returns the text representation of this direction cosine matrix.
     *
     * @return the text representation of this matrix.
     */
    @Override
    public Text toText() {
        final int m = this.getNumberOfRows();
        final int n = this.getNumberOfColumns();
        TextBuilder tmp = TextBuilder.newInstance();
        if (this.isApproxEqual(IDENTITY))
            tmp.append("{IDENTITY}");
        else {
            tmp.append('{');
            for (int i = 0; i < m; i++) {
                tmp.append('{');
                for (int j = 0; j < n; j++) {
                    tmp.append(get(i, j));
                    if (j != n - 1) {
                        tmp.append(", ");
                    }
                }
                tmp.append("}");
                if (i != m - 1) {
                    tmp.append(",\n");
                }
            }
            tmp.append("}");
        }
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Compares this DCMatrix against the specified object for strict equality (same
     * rotation type and same values).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this rotation is identical to that rotation;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        DCMatrix that = (DCMatrix)obj;
        return this._data.equals(that._data);
    }

    /**
     * Returns the hash code for this rotation.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _data.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    private static ObjectFactory<double[]> QU_FACTORY = new ObjectFactory<double[]>() {
        @Override
        protected double[] create() {
            return new double[4];
        }
    };

    /**
     * During serialization, this will write out the Float64Matrix as a simple series of
     * <code>double</code> values. This method is ONLY called by the Java Serialization
     * mechanism and should not otherwise be used.
     */
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {

        // Call the default write object method.
        out.defaultWriteObject();

        //  Write out the three coordinate values.
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                out.writeDouble(_data.get(i, j).doubleValue());

    }

    /**
     * During de-serialization, this will handle the reconstruction of the Float64Matrix.
     * This method is ONLY called by the Java Serialization mechanism and should not
     * otherwise be used.
     */
    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {

        // Call the default read object method.
        in.defaultReadObject();

        //  Read in the three coordinate values.
        FastTable<Float64Vector> rows = FastTable.newInstance();
        for (int i = 0; i < 3; ++i) {
            double v1 = in.readDouble();
            double v2 = in.readDouble();
            double v3 = in.readDouble();
            Float64Vector row = Float64Vector.valueOf(v1, v2, v3);
            rows.add(row);
        }

        _data = Float64Matrix.valueOf(rows);
        FastTable.recycle(rows);

    }

    /**
     * Holds the default XML representation for the DCMatrix object. This representation
     * consists of a series of 9 double values that represent a 3x3 matrix.
     * <pre>
     * &lt;DCMatrix&gt;
     *    &lt;Float64Matrix rows="3" columns="3"&gt;
     *        &lt;Float64 value="0.7848855672213958"/&gt;
     *        &lt;Float64 value="-0.56696369630552457"/&gt;
     *        &lt;Float64 value="0.2500136265068858"/&gt;
     *        &lt;Float64 value="0.4531538935183249"/&gt;
     *        &lt;Float64 value="0.25001362650688608"/&gt;
     *        &lt;Float64 value="-0.8556545654351748"/&gt;
     *        &lt;Float64 value="0.42261826174069944"/&gt;
     *        &lt;Float64 value="0.7848855672213958"/&gt;
     *        &lt;Float64 value="0.45315389351832508"/&gt;
     *    &lt;/Float64Matrix&gt;
     * </pre>
     */
    protected static final XMLFormat<DCMatrix> XML = new XMLFormat<DCMatrix>(DCMatrix.class) {

        @Override
        public DCMatrix newInstance(Class<DCMatrix> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(javolution.xml.XMLFormat.InputElement xml, DCMatrix dcm) throws XMLStreamException {
            
            FastTable<Float64Vector> rowList = FastTable.newInstance();
            try {
                for (int i = 0; i < 3; ++i) {
                    if (!xml.hasNext())
                        throw new XMLStreamException(
                                MessageFormat.format(RESOURCES.getString("need9ElementsFoundLess"), i));

                    double x = ((Float64)xml.getNext()).doubleValue();
                    double y = ((Float64)xml.getNext()).doubleValue();
                    double z = ((Float64)xml.getNext()).doubleValue();
                    Float64Vector row = Float64Vector.valueOf(x, y, z);
                    rowList.add(row);
                }

                if (xml.hasNext())
                    throw new XMLStreamException(RESOURCES.getString("toManyXMLElementsErr"));

                Float64Matrix m = Float64Matrix.valueOf(rowList);
                dcm._data = m;

            } finally {
                FastTable.recycle(rowList);
            }

        }

        @Override
        public void write(DCMatrix m, XMLFormat.OutputElement xml) throws XMLStreamException {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    xml.add(m.get(i, j));
                }
            }
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private DCMatrix() { }

    private static final ObjectFactory<DCMatrix> FACTORY = new ObjectFactory<DCMatrix>() {
        @Override
        protected DCMatrix create() {
            return new DCMatrix();
        }
    };

    private static DCMatrix copyOf(DCMatrix original) {
        DCMatrix M = FACTORY.object();
        M._data = original._data.copy();
        return M;
    }

    private static DCMatrix newInstance(Float64Matrix data) {
        DCMatrix M = FACTORY.object();
        M._data = data;
        return M;
    }

    /**
     * A matrix containing all zero elements.
     */
    public static final DCMatrix ZERO;

    /**
     * A DCM object that represents no relative change in orientation.
     */
    public static final DCMatrix IDENT;

    static {
        ImmortalContext.enter();
        try {
            ZERO = DCMatrix.valueOf(0, 0, 0, 0, 0, 0, 0, 0, 0);
            IDENT = DCMatrix.valueOf(1, 1, 1);

        } finally {
            ImmortalContext.exit();
        }
    }

    /**
     * Tests the methods in this class.
     * 
     * @param args Command line arguments (ignored).
     */
    public static void main(String args[]) {
        System.out.println("Testing DCMatrix:");

        DCMatrix m1 = DCMatrix.valueOf(1, 2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println("m1 = \n" + m1);
        System.out.println("  m1.getRow(1) = " + m1.getRow(1));
        System.out.println("  m1.getColumn(2) = " + m1.getColumn(2));
        System.out.println("  m1.getDiagonal() = " + m1.getDiagonal());
        System.out.println("  m1.opposite() = \n" + m1.opposite());
        System.out.println("  m1.transpose() = \n" + m1.transpose());
        System.out.println("  m1.vectorization() = \n" + m1.vectorization());
        System.out.println("  m1.determinant() = " + m1.determinant() + "; matrix is singular and can not be inverted");

        Float64 p1 = Float64.valueOf(2);
        System.out.println("\np1 = " + p1);
        System.out.println("  m1.times(p1) = \n" + m1.times(p1));

        DCMatrix m2 = DCMatrix.valueOf(1, 0, 1, 0, 1, 0, -1, 0, 1);
        System.out.println("\nm2 = \n" + m2);
        System.out.println("  m2.determinant() = " + m2.determinant());
        System.out.println("  m2.inverse() = \n" + m2.inverse());
        System.out.println("  m2.cofactor(0,2) = " + m2.cofactor(0, 2));
        System.out.println("  m2.adjoint() = \n" + m2.adjoint());
        System.out.println("  m1.times(m2) = \n" + m1.times(m2));
        System.out.println("  m1.tensor(m2) = \n" + m1.tensor(m2));

        Vector3D<Length> v1 = Vector3D.valueOf(1, 2, 3, SI.METER);
        System.out.println("\nv1 = " + v1);
        System.out.println("  m1.times(v1) = " + m1.times(v1));

        Parameter<Angle> psi = Parameter.valueOf(60, javax.measure.unit.NonSI.DEGREE_ANGLE);
        Parameter<Angle> theta = Parameter.ZERO_ANGLE;
        Parameter<Angle> phi = Parameter.ZERO_ANGLE;
        System.out.println("\npsi = " + psi + ", theta = " + theta + ", phi = " + phi);

        DCMatrix m3 = DCMatrix.getEulerTM(psi, theta, phi);
        System.out.println("m3 = \n" + m3);
        System.out.println("m3.toQuaternion() = " + m3.toQuaternion());

        Vector3D<Length> v2 = Vector3D.valueOf(1, 1, 1, SI.METER);
        System.out.println("\nv2 = " + v2);
        System.out.println("  m3.times(v2) = " + m3.times(v2));

        DCMatrix m4 = DCMatrix.getPsiThetaTM(psi, theta);
        System.out.println("  m4.times(v2) = " + m4.times(v2));

        psi = Parameter.valueOf(30, javax.measure.unit.NonSI.DEGREE_ANGLE);
        theta = Parameter.valueOf(-25, javax.measure.unit.NonSI.DEGREE_ANGLE);
        phi = Parameter.valueOf(60, javax.measure.unit.NonSI.DEGREE_ANGLE);
        System.out.println("\npsi = " + psi + ", theta = " + theta + ", phi = " + phi);

        DCMatrix m5 = DCMatrix.getEulerTM(psi, theta, phi);
        System.out.println("m5 = \n" + m5);
        Vector3D<Length> v3 = Vector3D.valueOf(1, 1, 0, SI.METER);
        System.out.println("\nv3 = " + v3);
        System.out.println("  m5.transform(v3) = " + m5.transform(v3));
        System.out.println("  m5.toQuaternion().transform(v3) = " + m5.toQuaternion().transform(v3));
        System.out.println("  m5.toQuaternion().toDCM().transform(v3) = " + m5.toQuaternion().toDCM().transform(v3));

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
            binding.setAlias(org.jscience.mathematics.number.Float64.class, "Float64");

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(m5, "DCMatrix", DCMatrix.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
