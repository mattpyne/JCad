/*
*   ParameterVector -- A n-dimensional vector of Parameter objects.
*
*   Copyright (C) 2009-2014 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.param;

import java.util.List;

import javax.measure.unit.Unit;
import javax.measure.unit.SI;
import javax.measure.unit.NonSI;
import javax.measure.converter.UnitConverter;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.*;

import javolution.lang.MathLib;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import javolution.lang.ValueType;
import javolution.util.FastTable;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import javolution.xml.XMLSerializable;

import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.*;


/**
* <p> This class represents an n-dimensional {@link Vector vector} of {@link Parameter}
*	elements sharing the same units.</p>
* 
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt   Date: March 27, 2009
*  @version March 1, 2014
**/
public class ParameterVector<Q extends Quantity> extends AbstractParamVector<Q,ParameterVector<Q>>
					implements ValueType, XMLSerializable {

	private static final long serialVersionUID = -1026285730707259617L;

	/**
	*  Constant used to identify the X (0) coordinate in the vector.
	**/
	public static final int X = 0;
	
	/**
	*  Constant used to identify the Y (1) coordinate in the vector.
	**/
	public static final int Y = 1;
	
	/**
	*  Constant used to identify the Z (2) coordinate in the vector.
	**/
	public static final int Z = 2;
	

	/**
	*  The coordinates are stored in this vector.
	*  Serialization is handled by this class since Float64Vector is not Serializable.
	**/
	protected transient Float64Vector _data;
	
	/**
	*  Holds the unit of the coordinates.
	**/
	protected Unit<Q> _unit;
	
	
	/**
 	* Returns a 2D {@link ParameterVector} instance holding the specified <code>double</code> values
	* stated in the specified units.
 	*
 	* @param x the x value stated in the specified unit.
 	* @param y the y value stated in the specified unit.
 	* @param unit the unit in which the coordinates are stated.
 	* @return the vector having the specified values.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(double x, double y, Unit<Q> unit) {
		ParameterVector<Q> V = newInstance(unit);
		
		V._data = Float64Vector.valueOf(x, y);
		
		return V;
	}
	
	/**
 	* Returns a {@link ParameterVector} instance holding the specified <code>double</code> values
	* stated in the specified units.
 	*
 	* @param x the x value stated in the specified unit.
 	* @param y the y value stated in the specified unit.
 	* @param z the z value stated in the specified unit.
 	* @param unit the unit in which the coordinates are stated.
 	* @return the vector having the specified values.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(double x, double y, double z, Unit<Q> unit) {
		ParameterVector<Q> V = newInstance(unit);
		
		V._data = Float64Vector.valueOf(x, y, z);
		
		return V;
	}

	/**
 	* Returns a {@link ParameterVector} instance holding the specified <code>double</code> values
	* stated in the specified units.
 	*
 	* @param unit the unit in which the coordinates are stated.
 	* @param values A list of values to store in the vector.
 	* @return the vector having the specified values.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(Unit<Q> unit, double... values) {
		ParameterVector<Q> V = newInstance(unit);
		
		V._data = Float64Vector.valueOf(values);
		
		return V;
	}

	/**
 	* Returns a {@link ParameterVector} instance holding the specified <code>Parameter</code> values.
	* All the values are converted to the same units as the x value.
 	*
 	* @param x the x value.
 	* @param y the y value.
 	* @param z the z value.
 	* @return the vector having the specified values in the units of x.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(Parameter<Q> x, Parameter<Q> y, Parameter<Q> z) {
		
		Unit<Q> unit = x.getUnit();
		ParameterVector<Q> V = newInstance(unit);
		V._data = Float64Vector.valueOf(x.getValue(), y.getValue(unit), z.getValue(unit));
		
		return V;
	}

	/**
 	* Returns a {@link ParameterVector} instance holding the specified <code>Parameter</code> values.
	* All the values are converted to the same units as the first value.
 	*
 	* @param values A list of values to store in the vector.
 	* @return the vector having the specified values in the units of x.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(List<Parameter<Q>> values) {
		FastTable<Float64> valueList = FastTable.newInstance();
		
		Unit<Q> unit = values.get(0).getUnit();
        int size = values.size();
        for (int i=0; i < size; ++i) {
            Parameter<Q> param = values.get(i);
			Float64 value = Float64.valueOf(param.getValue(unit));
			valueList.add(value);
		}
		
		ParameterVector<Q> V = newInstance(unit);
		V._data = Float64Vector.valueOf(valueList);
		
		FastTable.recycle(valueList);
		
		return V;
	}
	
	/**
 	* Returns a {@link ParameterVector} instance holding the specified <code>Parameter</code> values.
	* All the values are converted to the same units as the first value.
 	*
 	* @param values A list of values to store in the vector.
 	* @return the vector having the specified values in the units of x.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(Parameter<Q>... values) {
		FastTable<Float64> valueList = FastTable.newInstance();
		
		Unit<Q> unit = values[0].getUnit();
        int size = values.length;
        for (int i=0; i < size; ++i) {
            Parameter<Q> param = values[i];
			Float64 value = Float64.valueOf(param.getValue(unit));
			valueList.add(value);
		}
		
		ParameterVector<Q> V = newInstance(unit);
		V._data = Float64Vector.valueOf(valueList);
		
		FastTable.recycle(valueList);
		
		return V;
	}
	
	/**
 	* Returns a {@link ParameterVector} instance containing the specified vector of Float64 values
	* stated in the specified units.
 	*
 	* @param vector the vector of Float64 values stated in the specified unit. 
 	* @param unit the unit in which the coordinates are stated.
 	* @return the vector having the specified values.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(Vector<Float64> vector, Unit<Q> unit) {
		
		ParameterVector<Q> V = newInstance(unit);
		V._data = Float64Vector.valueOf(vector);
		
		return V;
	}

	/**
 	* Returns a {@link ParameterVector} instance containing the 
 	* specified vector of Parameter values with compatible units.
	* All the values are converted to the same units as the 1st value.
 	*
 	* @param vector the vector of Parameter values stated in the specified unit. 
 	* @return the vector having the specified values.
 	*/
	public static <Q extends Quantity> ParameterVector<Q> valueOf(Vector<Parameter<Q>> vector) {
		if (vector instanceof ParameterVector)
			return (ParameterVector<Q>)vector;
		
		Unit<Q> unit = vector.get(0).getUnit();		
		FastTable<Float64> valueList = FastTable.newInstance();
		
		int size = vector.getDimension();
		for (int i=0; i < size; ++i) {
			Parameter<Q> param = vector.get(i);
			Float64 value = Float64.valueOf(param.getValue(unit));
			valueList.add(value);
		}
		
		ParameterVector<Q> V = newInstance(unit);
		V._data = Float64Vector.valueOf(valueList);
		
		FastTable.recycle(valueList);
		
		return V;
	}


	/**
	*  Returns a Vector3D representation of this vector if possible.
	*
	*  @return A Vector3D that is equivalent to this vector
	*  @throws DimensionException if this vector has any number of dimensions
	*          other than 3.
	**/
    @Override
	public Vector3D<Q> toVector3D() {
		if (getDimension() != 3)
			throw new DimensionException( RESOURCES.getString("pvNumElementsErr") );
		return (Vector3D<Q>)Vector3D.valueOf(this);
	}
	
	/**
	*  Return the specified {@link Vector3D} object as a <code>ParameterVector</code> instance.
	*
	*  @param vector  The <code>Vector3D</code> object to be converted to a <code>ParameterVector</code>.
	*  @return A <code>ParameterVector</code> instance that is equivalent to the
	*          supplied <code>Vector3D</code> object.
	**/
    @Override
	public ParameterVector<Q> fromVector3D(Vector3D<Q> vector) {
		return (ParameterVector<Q>)ParameterVector.valueOf(vector);
	}
	
	/**
	* Returns the number of elements held by this vector.
	*
	* @return this vector dimension.
	*/
	@Override
	public int getDimension() {
		return _data.getDimension();
	}

	/**
 	* Returns the value of a Parameter from this vector.
 	*
 	* @param  i the dimension index.
 	* @return the value of the parameter at <code>i</code>.
 	* @throws IndexOutOfBoundsException <code>(i < 0) || (i >= dimension())</code>
 	*/
	@Override
	public Parameter<Q> get(int i) {
		return Parameter.valueOf(getValue(i), getUnit());
	}

	/**
	* Returns the value of the Parameter in this vector as a <code>double</code>,
	* stated in this vector's {@link #getUnit unit}.
	*
	* @param  i the dimension index.
	* @return the value of the Parameter at <code>i</code>.
	* @throws IndexOutOfBoundsException <code>(i < 0) || (i >= dimension())</code>
	*/
    @Override
	public double getValue(int i) {
		return _data.getValue(i);
	}

	/**
 	* Returns the {@link #norm}, magnitude, or length value of this vector.
 	*
 	* @return <code>this.norm().doubleValue()</code>.
 	*/
    @Override
	public double normValue() {
		return _data.normValue();
	}

	/**
	* Returns the negation of this vector.
	*
	* @return <code>-this</code>.
	*/
	@Override
	public ParameterVector<Q> opposite() {
		ParameterVector<Q> V = newInstance(this._unit);
		V._data = this._data.opposite();
		return V;
	}

	/**
	* Returns the sum of this vector with the one specified.
	* The unit of the output vector will be the units of
	* this vector.
	*
	* @param   that the vector to be added.
	* @return  <code>this + that</code>.
	* @throws  DimensionException if vector dimensions are different.
 	* @throws  ConversionException if the input vector is not in units
	*          consistent with this vector.
	*/
	@Override
	public ParameterVector<Q> plus(Vector<Parameter<Q>> that) {
		
		if (that.getDimension() != this.getDimension())
			throw new DimensionException();
		
		//	Convert input vector to a Float64Vector (with unit conversion if necessary).
		Float64Vector thatData = toFloat64Vector(that, this._unit);
		
		ParameterVector<Q> V = newInstance(this._unit);
		V._data = this._data.plus(thatData);
		
		return V;
	}
	
	/**
	* Returns the sum of this vector with the parameter specified.
	* The input parameter is added to each component of this vector.
	* The unit of the output vector will be the units of
	* this vector.
	*
	* @param   that the parameter to be added to each element of this vector.
	* @return  <code>this + that</code>.
	*/
    @Override
	public ParameterVector<Q> plus(Parameter<Q> that) {
		
		//	Convert input parameter to the units of this vector.
		double thatValue = that.getValue(this._unit);
		
        StackContext.enter();
        try {
            FastTable<Float64> valueList = FastTable.newInstance();
            int numDims = _data.getDimension();
            for (int i = 0; i < numDims; ++i) {
                double value = this._data.getValue(i) + thatValue;
                valueList.add(Float64.valueOf(value));
            }
            ParameterVector<Q> V = newInstance(this._unit);
            V._data = Float64Vector.valueOf(valueList);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
	}
	
	/**
	* Returns the difference between this vector and the one specified.
	* The unit of the output vector will be the units of
	* this vector.
	*
	* @param  that the vector to be subtracted.
	* @return <code>this - that</code>.
	* @throws  DimensionException if vector dimensions are different.
 	* @throws  ConversionException if the input vector is not in units
	*          consistent with this vector.
	*/
	@Override
	public ParameterVector<Q> minus(Vector<Parameter<Q>> that) {
		
		if (that.getDimension() != this.getDimension())
			throw new DimensionException();
		
		//	Convert input vector to a Float64Vector (with unit conversion if necessary).
		Float64Vector thatData = toFloat64Vector(that, this._unit);
		
		ParameterVector<Q> V = newInstance(this._unit);
		V._data = this._data.minus(thatData);
		
		return V;
	}

	/**
	* Subtracts the supplied Parameter from each element of this vector and returns
	* the result.  The unit of the output vector will be the units of
	* this vector.
	*
	* @param  that the Parameter to be subtracted from each element of this vector.
	* @return <code>this - that</code>.
	*/
    @Override
	public ParameterVector<Q> minus(Parameter<Q> that) {
		
		//	Convert input parameter to the units of this vector.
		double thatValue = that.getValue(this._unit);
		
        StackContext.enter();
        try {
            FastTable<Float64> valueList = FastTable.newInstance();
            int numDims = _data.getDimension();
            for (int i = 0; i < numDims; ++i) {
                double value = this._data.getValue(i) - thatValue;
                valueList.add(Float64.valueOf(value));
            }
            ParameterVector<Q> V = newInstance(this._unit);
            V._data = Float64Vector.valueOf(valueList);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
	}
	
	/**
	* Returns the product of this vector with the specified coefficient.
	*
	* @param  k the coefficient multiplier.
	* @return <code>this times k</code>
	*/
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ParameterVector times(Parameter k) {
		ParameterVector V = FACTORY.object();
		
		V._unit = Parameter.productOf(this.getUnit(), k.getUnit());
		V._data = this._data.times(k.getValue());

		return V;
	}
	
	/**
	* Returns the product of this vector with the specified coefficient.
	*
	* @param  k the coefficient multiplier.
	* @return <code>this times k</code>
	*/
    @Override
	public ParameterVector<Q> times(double k) {
		ParameterVector<Q> V = newInstance(this._unit);
		V._data = _data.times(k);
		return V;
	}	
	
	/**
	* Returns the dot product of this vector with the one specified.
	*
	* @param  that the vector multiplier.
	* @return <code>this times that</code>
	* @throws DimensionException if <code>this.dimension() != that.dimension()</code>
	* @see <a href="http://en.wikipedia.org/wiki/Dot_product">
	* 	Wikipedia: Dot Product</a>
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public Parameter times(Vector that) {
		
		if (that.getDimension() != this.getDimension())
			throw new DimensionException();
		
		//	Convert input vector to a Float64Vector.
		@SuppressWarnings("unchecked")
		Float64Vector thatData = toFloat64Vector(that, null);
		
		Float64 f = this._data.times(thatData);
		Unit<?> unit = Parameter.productOf(this.getUnit(),((Parameter)that.get(X)).getUnit());
		
		return Parameter.valueOf(f.doubleValue(), unit);
	}
	
	/**
	* Returns the element-by-element product of this vector with the one specified.
	*
	* @param  that the vector multiplier.
	* @return <code>this .* that</code>
	* @throws DimensionException if <code>this.dimension() != that.dimension()</code>
	*/
	@SuppressWarnings("rawtypes")
	public ParameterVector timesEBE(Vector that) {
		
		if (that.getDimension() != this.getDimension())
			throw new DimensionException();
		
        StackContext.enter();
        try {
            //	Convert input vector to a Float64Vector.
            @SuppressWarnings("unchecked")
            Float64Vector thatData = toFloat64Vector(that, null);

            Unit<?> unit = Parameter.productOf(this.getUnit(), ((Parameter) that.get(X)).getUnit());

            //	Carry out the multiplication.
            FastTable<Float64> valueList = FastTable.newInstance();
            int size = getDimension();
            for (int i = 0; i < size; ++i) {
                double value = this._data.getValue(i) * thatData.getValue(i);
                valueList.add(Float64.valueOf(value));
            }

            ParameterVector<?> V = newInstance(unit);
            V._data = Float64Vector.valueOf(valueList);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
	}
	
	/**
	* Returns the cross product of two vectors.
	*
	* @param  that the vector multiplier.
	* @return <code>this x that</code>
	* @throws DimensionException if <code>(that.getDimension() != this.getDimension())</code> 
	* @see <a href="http://en.wikipedia.org/wiki/Cross_product">
	*         Wikipedia: Cross Product</a>
	*/
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ParameterVector cross(Vector that) {
		
		if (that.getDimension() != this.getDimension())
			throw new DimensionException();
		
		//	Convert input vector to a Float64Vector.
		Float64Vector thatData = toFloat64Vector(that, null);
		
		Unit<?> unit = Parameter.productOf(this.getUnit(), ((Parameter)that.get(X)).getUnit());
		
		ParameterVector V = newInstance(unit);
		V._data = this._data.cross(thatData);

		return V;
	}
	
	/**
 	* Returns this vector with each element divided by the specified divisor.
 	*
 	* @param  that the divisor.
 	* @return <code>this / that</code>.
 	*/
	public ParameterVector<?> divide(Parameter<?> that) {
		return times(that.inverse());
	}

	/**
	*  Returns this vector converted to a unit vector by dividing all the
	*  vector's elements by the length ({@link #norm}) of this vector.
	**/
	@SuppressWarnings("unchecked")
	public ParameterVector<Dimensionless> toUnitVector() {
		double magnitude = this.normValue();
		if (this.getUnit().equals(Dimensionless.UNIT) && MathLib.abs(magnitude-1) <= Parameter.EPS)
			return (ParameterVector<Dimensionless>)this;
		ParameterVector<Dimensionless> V = newInstance(Dimensionless.UNIT);
		V._data = this._data.times(1.0/magnitude);
		return V;
	}
	
	/**
	* Returns a copy of this vector 
	* {@link javolution.context.AllocatorContext allocated} 
	* by the calling thread (possibly on the stack).
	*	
	* @return an identical and independent copy of this vector.
	*/
	@Override
	public ParameterVector<Q> copy() {
		return copyOf(this);
	}
	
	/**
	*  Returns the unit in which the {@link #getValue values} in this vector
	*  are stated in.
	**/
    @Override
	public Unit<Q> getUnit() {
		return _unit;
	}
	
	/**
 	* Returns the equivalent to this vector but stated in the 
 	* specified unit. 
 	*
 	* @param  unit the unit of the vector to be returned.
 	* @return a vector equivalent to this vector but stated in the 
 	* 		specified unit.
 	* @throws ConversionException if the current model does not allows for
 	* 		conversion to the specified unit.
 	*/
	@SuppressWarnings("unchecked")
    @Override
	public <R extends Quantity> ParameterVector<R> to(Unit<R> unit) {
		if ((_unit == unit) || this._unit.equals(unit))
			return (ParameterVector<R>)this;
		
        StackContext.enter();
        try {
            UnitConverter cvtr = Parameter.converterOf(_unit, unit);
            if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
                ParameterVector<R> result = (ParameterVector<R>) copyOf(this);
                result._unit = unit;
                return StackContext.outerCopy((ParameterVector<R>)result);
            }

            FastTable<Float64> valueList = FastTable.newInstance();
            int size = _data.getDimension();
            for (int i = 0; i < size; ++i) {
                double value = cvtr.convert(_data.getValue(i));
                valueList.add(Float64.valueOf(value));
            }
            
            ParameterVector<R> result = newInstance(unit);
            result._data = Float64Vector.valueOf(valueList);

            return StackContext.outerCopy(result);
        } finally {
            StackContext.exit();
        }
	}

	/**
 	* Casts this ParameterVector to a parameterized unit of specified nature or 
 	* throw a <code>ClassCastException</code> if the dimension of the 
 	* specified quantity and this parameter's unit dimension do not match.
 	* 
 	* @param  type the quantity class identifying the nature of the unit.
 	* @return this ParameterVector parameterized with the specified type.
 	* @throws ClassCastException if the dimension of this parameter's unit is different 
 	* 		from the specified quantity dimension.
 	* @throws UnsupportedOperationException if the specified quantity class
 	* 		does not have a public static field named "UNIT" holding the 
 	* 		standard unit for the quantity.
 	*/
	@SuppressWarnings("unchecked")
	public <T extends Quantity> ParameterVector<T> asType(Class<T> type) throws ClassCastException {
		@SuppressWarnings("unused")
		Unit<T> u = _unit.asType(type);	//	If no exception is thrown, the cast is valid.
		return (ParameterVector<T>)this;
	}
   
	/**
	*  Returns the values stored in this vector, stated in this vector's
	*  {@link #getUnit unit}, as a Float64Vector.
	**/
	public Float64Vector toFloat64Vector() {
		return _data;
	}
	
	/**
 	* Compares this ParameterVector against the specified object for strict 
 	* equality (same values and same units).
	*
 	* @param  obj the object to compare with.
 	* @return <code>true</code> if this vector is identical to that
 	* 		vector; <code>false</code> otherwise.
	**/
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		
		ParameterVector<?> that = (ParameterVector<?>)obj;
		if(!this._data.equals(that._data))
			return false;
		
		return this._unit.equals(that._unit);
	}

	/**
 	* Returns the hash code for this parameter.
 	* 
 	* @return the hash code value.
 	*/
    @Override
	public int hashCode() {
		int hash = 7;
		
		int var_code = _unit.hashCode();
		hash = hash*31 + var_code;
		
		var_code = _data.hashCode();
		hash = hash*31 + var_code;
		
		return hash;
	}

	/**
	*  Convert a vector of Parameter objects to a Float64Vector stated in
	*  the specified units.  If the units are null, no conversion occurs.
	**/
	private static <Q extends Quantity> Float64Vector toFloat64Vector(Vector<Parameter<Q>> that, Unit<Q> unit) {
		
        StackContext.enter();
        try {
            Float64Vector thatData;
            Unit<?> oldUnit;
            if (that instanceof ParameterVector) {
                ParameterVector<Q> V = ((ParameterVector<Q>) that);
                thatData = V.toFloat64Vector();
                oldUnit = V.getUnit();

            } else if (that instanceof Coordinate3D) {
                Vector3D<Q> T = ((Coordinate3D<Q>) that).toVector3D();
                if (!Length.UNIT.isCompatible(T.getUnit()))
                    throw new ConversionException(RESOURCES.getString("incompatibleUnits").
                            replace("<NAME/>", "input vector").replace("<TYPE/>", "length"));
                oldUnit = T.getUnit();
                thatData = T.toFloat64Vector();

            } else {
                ParameterVector<Q> V = valueOf(that);
                thatData = V.toFloat64Vector();
                oldUnit = V.getUnit();
            }


            //	Convert that vector's units to the specified units if necessary.
            if (unit != null) {
                if (!unit.equals(oldUnit)) {
                    UnitConverter cvtr = Parameter.converterOf(oldUnit, unit);
                    if (cvtr != UnitConverter.IDENTITY) {
                        FastTable<Float64> valueList = FastTable.newInstance();
                        int size = thatData.getDimension();
                        for (int i = 0; i < size; ++i) {
                            double value = cvtr.convert(thatData.getValue(i));
                            valueList.add(Float64.valueOf(value));
                        }
                        thatData = Float64Vector.valueOf(valueList);
                    }
                }
            }

            return StackContext.outerCopy(thatData);
        } finally {
            StackContext.exit();
        }
	}
	

	/**
	*  During serialization, this will write out the Float64Vector as a
	*  simple series of <code>double</code> values.    This method is ONLY called by the Java
	*  Serialization mechanism and should not otherwise be used.
	**/
	private void writeObject( java.io.ObjectOutputStream out ) throws java.io.IOException {
	
		// Call the default write object method.
		out.defaultWriteObject();
		
		//	Write out the number of elements.
		int size = _data.getDimension();
		out.writeInt( size );
		
		//	Write out the coordinate values.
		for (int i=0; i < size; ++i)
			out.writeDouble( _data.getValue(i) );
		
	}

	/**
	*  During de-serialization, this will handle the reconstruction
	*  of the Float64Vector.  This method is ONLY called by the Java
	*  Serialization mechanism and should not otherwise be used.
	**/
	private void readObject( java.io.ObjectInputStream in ) throws java.io.IOException, ClassNotFoundException {
	
		// Call the default read object method.
		in.defaultReadObject();

		//	Read in the number of elements.
		int size = in.readInt();
		
		//	Read in the coordinate values.
		FastTable<Float64> valueList = FastTable.newInstance();
		for (int i=0; i < size; ++i ) {
			double value = in.readDouble();
			valueList.add(Float64.valueOf(value));
		}
		
		_data = Float64Vector.valueOf(valueList);
		
		FastTable.recycle(valueList);
	}
	
	/**
 	* Holds the default XML representation. For example:
 	* <pre>
 	*	&lt;ParameterVector unit = "m"&gt;
 	*		&lt;Float64 value="1.0" /&gt;
 	*		&lt;Float64 value="0.0" /&gt;
 	*		&lt;Float64 value="2.0" /&gt;
 	*	&lt;/ParameterVector&gt;
	* </pre>
 	*/
	@SuppressWarnings("rawtypes")
	protected static final XMLFormat<ParameterVector> XML = new XMLFormat<ParameterVector>(ParameterVector.class) {

		@Override
		public ParameterVector<?> newInstance(Class<ParameterVector> cls, InputElement xml) throws XMLStreamException {
			
			Unit<?> unit = Unit.valueOf(xml.getAttribute("unit"));
			ParameterVector<?> V = ParameterVector.newInstance(unit);
			
			FastTable<Float64> valueList = FastTable.newInstance();
			while (xml.hasNext()) {
				Float64 value = xml.getNext();
				valueList.add(value);
			}
			V._data = Float64Vector.valueOf(valueList);
			
			FastTable.recycle(valueList);
			
			return V;
		}

		@Override
		public void read(InputElement xml, ParameterVector V) throws XMLStreamException {
			//	Nothing to do.
		}

		@Override
		public void write(ParameterVector V, OutputElement xml) throws XMLStreamException {
				
			xml.setAttribute("unit", V._unit.toString());
			int size = V._data.getDimension();
			for (int i=0; i < size; ++i)
				xml.add(V._data.get(i));
			
		}
	};
	
	///////////////////////
	// Factory creation. //
	///////////////////////

	protected ParameterVector() {}
	
	@SuppressWarnings("rawtypes")
	private static final ObjectFactory<ParameterVector> FACTORY = new ObjectFactory<ParameterVector>() {
		@Override
		protected ParameterVector create() {
			return new ParameterVector();
		}
	};

	/**
	*  Return a new instance of the ParameterVector class with the specified unit.
	*  The <code>_data</code> value will be undefined and must be set before
	*  using the returned object for anything.
	**/
	@SuppressWarnings("unchecked")
	protected static <Q extends Quantity> ParameterVector<Q> newInstance(Unit<Q> unit) {
        if (unit == null)
            throw new NullPointerException("Unit can not be null.");
		ParameterVector<Q> measure = FACTORY.object();
		measure._unit = unit;
		return measure;
	}

	@SuppressWarnings("unchecked")
	private static <Q extends Quantity> ParameterVector<Q> copyOf(ParameterVector<Q> original) {
		ParameterVector<Q> measure = FACTORY.object();
		measure._unit = original._unit;
		measure._data = original._data.copy();
		return measure;
	}


	/**
	*  Tests the methods in this class.
	**/
    public static void main (String args[]) {
		System.out.println("Testing ParameterVector:  test = result [correct result]");
		
		ParameterVector<Length> v1 = ParameterVector.valueOf(1, 2, 3, NonSI.FOOT);
		System.out.println("v1 = " + v1);
		System.out.println("  converted to m       = " + v1.to(SI.METER) + " [{0.3048 m, 0.6096 m, 0.9144 m}]");
		System.out.println("  v1.norm()            = " + v1.norm() + " [3.74165738677394 ft]");
		System.out.println("  v1.opposite()        = " + v1.opposite() + " [{-1.0 ft, -2.0 ft, -3.0 ft}]");
		System.out.println("  v1.toUnitVector()    = " + v1.toUnitVector() + " [{0.267261241912424 , 0.534522483824849 , 0.801783725737273 }]");
		
		Parameter<Length> point = Parameter.valueOf(24, NonSI.INCH);
		System.out.println("point = " + point);
		System.out.println("  v1 + point           = " + v1.plus(point) + " [{3.0 ft, 4.0 ft, 5.0 ft}]");
		System.out.println("  v1 - point           = " + v1.minus(point) + " [{-1.0 ft, 0.0 ft, 1.0 ft}]");
		System.out.println("  v1 * 2               = " + v1.times(2) + " [{2.0 ft, 4.0 ft, 6.0 ft}]");
		
		ParameterVector<?> areaVector = (ParameterVector<?>)v1.times(point);
		System.out.println("  v1 * point           = " + areaVector);
//		System.out.println("  converted to ft²     = " + areaVector.to(NonSI.SQUARE_FOOT) + " [{2.0 ft², 4.0 ft², 6.0 ft²}]");
		
		
		ParameterVector<Length> v2 = ParameterVector.valueOf(1, 1, 1, SI.METER);
		ParameterVector<?> v1xv2 = (ParameterVector<?>)v1.cross(v2);
		System.out.println("v2 = " + v2);
		System.out.println("  v1 + v2 = " + v1.plus(v2) + " [{4.28083989501312 ft, 5.28083989501312 ft, 6.28083989501312 ft}]");
		System.out.println("  v1 - v2 = " + v1.minus(v2) + " [{-2.28083989501312 ft, -1.28083989501312 ft, -0.280839895013123 ft}]");
		System.out.println("  v1 · v2 = " + v1.times(v2.to(NonSI.FOOT)) + " [19.6850393700787 ft²]");
		System.out.println("  v1.cross(v2) = " + v1xv2.to(NonSI.FOOT.pow(2)) + " [{-3.28083989501312 ft^2, 6.56167979002625 ft^2, -3.28083989501312 ft^2}]");
		System.out.println("  v1.angle(v2) = " + v1.angle(v2).to(NonSI.DEGREE_ANGLE) + " [73.6090476746643 deg]");
		
		//	Write out XML data.
		try {
			System.out.println();
			
			// Creates some useful aliases for class names.
			javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
			binding.setAlias(org.jscience.mathematics.number.Float64.class, "Float64");
			
			javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
			writer.setIndentation("    ");
			writer.setBinding(binding);
			writer.write(v1, "ParameterVector", ParameterVector.class);
			writer.flush();
			
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}