/**
 * ParamTestSuite -- An suite of unit tests for the jahuwaldt.js.param package.
 * 
 * Copyright (C) 2014 by Joseph A. Huwaldt. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.lang.MathLib;
import javolution.testing.TestCase;
import javolution.testing.TestContext;
import javolution.testing.TestSuite;
import javolution.text.TextBuilder;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Matrix;
import org.jscience.mathematics.vector.Float64Vector;


/**
 * <p> This class holds the {@link jahuwaldt.js.param} package unit tests and benchmarks.</p>
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt Date: February 27, 2014
 * @version March 1, 2014
 */
public final class ParamTestSuite extends TestSuite {
    
    @Override
    public void run() {
        TestContext.info("-----------------------------------------------");
        TestContext.info("-- Test Suite for jahuwaldt.js.param package --");
        TestContext.info("-----------------------------------------------");
        TestContext.test(new ParameterGetValue(100, NonSI.INCH, NonSI.YARD));
        TestContext.test(new ParameterToUnit(NonSI.FOOT, SI.METER));
        TestContext.test(new ParameterBasicMathOperations());
        TestContext.test(new ParameterLogicalComparisons());
        TestContext.test(new DCMatrixTests());
        TestContext.test(new QuaternionTests());
    }
    
    @Override
    public CharSequence getDescription() {
        return TextBuilder.newInstance().append("Test Suite for the jahuwaldt.js.param package.");
    }
    
    public static class ParameterGetValue extends TestCase {
        private final int _numValues;
        private final Unit _fromUnit;
        private final Unit _toUnit;
        private final double[] _values;
        private final Parameter[] _params;
        
        public ParameterGetValue(int numValues, Unit fromUnit, Unit toUnit) {
            _numValues = numValues;
            _fromUnit = fromUnit;
            _toUnit = toUnit;
            _values = new double[numValues];
            _params = new Parameter[numValues];
        }
        
        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append("Parameter.getValue(unit)");
        }
        
        @Override
        public void prepare() {
            for (int i=0; i < _numValues; ++i) {
                _params[i] = Parameter.valueOf(MathLib.random()*10,_fromUnit);
            }
        }
        
        @Override
        public int count() {
            return _numValues;
        }
        
        @Override
        public void execute() {
            for (int i=0; i < _numValues; ++i) {
                _values[i] = _params[i].getValue(_toUnit);
            }
        }
        
        @Override
        public void validate() {
            UnitConverter cvtr = Parameter.converterOf(_fromUnit, _toUnit);
            for (int i=0; i < _numValues; ++i) {
                double expected = cvtr.convert(_params[i].getValue());
                TestContext.assertEquals(expected, _values[i]);
            }
        }
        
    }

    public static class ParameterToUnit extends TestCase {
        private static final double FROMVALUE = 2;
        private final Unit _fromUnit;
        private final Unit _toUnit;
        private Parameter _param;
        private Parameter _toParam;
        
        public ParameterToUnit(Unit fromUnit, Unit toUnit) {
            _fromUnit = fromUnit;
            _toUnit = toUnit;
        }
        
        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append("Parameter.to(Unit): from = ")
                    .append(_fromUnit).append(", to = ").append(_toUnit);
        }
        
        @Override
        public void prepare() {
            _param = Parameter.valueOf(FROMVALUE, _fromUnit);
        }
        
        @Override
        public void execute() {
            _toParam = _param.to(_toUnit);
        }
        
        @Override
        public void validate() {
            UnitConverter cvtr = Parameter.converterOf(_fromUnit, _toUnit);
            Parameter expected = Parameter.valueOf(cvtr.convert(FROMVALUE),_toUnit);
            TestContext.assertEquals(expected, _toParam);
        }
        
    }

    public static class ParameterBasicMathOperations extends TestCase {
        private static final Unit unit = NonSI.FOOT;
        private static final double P1VALUE = -3.14159;
        private static final double P2VALUE = 42;
        private static final double P3VALUE = 223;
        private static final double K = 2;
        private final Parameter<Length> p1 = Parameter.valueOf(P1VALUE,unit);
        private final Parameter<Length> p2 = Parameter.valueOf(P2VALUE,unit);
        private final Parameter<Angle> p3 = Parameter.valueOf(P3VALUE, NonSI.DEGREE_ANGLE);
        private Parameter<Length> _opposite, _abs, _plus, _minus;
        private Parameter _times1, _times2, _times3, _inverse, _divide1, _divide2;
        private Parameter _sqrt, _root3, _pow3;
        private Parameter _round, _round2Place;
        private Parameter _min, _max;
        private Parameter _sin, _cos, _tan;
        private Parameter _asin, _acos, _atan, _atan2;
        
        
        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append("Parameter math operations: p1 = ")
                    .append(p1).append(", p2 = ").append(p2).append(", p3 = ").append(p3)
                    .append(", K = ").append(K);
        }
        
        @Override
        public void execute() {
            _opposite = p1.opposite();
            _abs = p1.abs();
            _plus = p1.plus(p2);
            _minus = p1.minus(p2);
            _times1 = p1.times(p2);
            _times2 = p1.times(K);
            _times3 = p1.times(Parameter.ZERO);
            _inverse = p1.inverse();
            _divide1 = p1.divide(p2);
            _divide2 = p1.divide(K);
            _sqrt = p2.sqrt();
            _root3 = p2.root(3);
            _pow3 = p1.pow(3);
            _round = p1.round();
            _round2Place = p1.roundToPlace(-2);
            _min = p1.min(p2);
            _max = p1.max(p2);
            _sin = Parameter.sin(p3);
            _cos = Parameter.cos(p3);
            _tan = Parameter.tan(p3);
            _asin = Parameter.asin((Parameter)p1.divide(p2));
            _acos = Parameter.acos((Parameter)p1.divide(p2));
            _atan = Parameter.atan((Parameter)p1.divide(p2));
            _atan2 = Parameter.atan2(p1,p2);
        }
        
        @Override
        public void validate() {
            Parameter expected = Parameter.valueOf(-P1VALUE,unit);
            TestContext.assertEquals("p1.opposite()", expected, _opposite);
            expected = Parameter.valueOf(P1VALUE+P2VALUE,unit);
            TestContext.assertEquals("p1.plus(p2)", expected, _plus);
            expected = Parameter.valueOf(P1VALUE-P2VALUE,unit);
            TestContext.assertEquals("p1.minus(p2)", expected, _minus);
            expected = Parameter.valueOf(P1VALUE*P2VALUE,Parameter.productOf(unit, unit));
            TestContext.assertEquals("p1.times(p2)", expected, _times1);
            expected = Parameter.valueOf(P1VALUE*K,unit);
            TestContext.assertEquals("p1.times(K)", expected, _times2);
            expected = Parameter.valueOf(P1VALUE*0,unit);
            TestContext.assertEquals("p1.times(Parameter.ZERO)", expected, _times3);
            expected = Parameter.valueOf(1./P1VALUE,unit.inverse());
            TestContext.assertEquals("p1.inverse()", expected, _inverse);
            expected = Parameter.valueOf(P1VALUE/P2VALUE,Parameter.productOf(unit, unit.inverse()));
            TestContext.assertEquals("p1.divide(p2)", expected, _divide1);
            expected = Parameter.valueOf(P1VALUE/K,unit);
            TestContext.assertEquals("p1.divide(K)", expected, _divide2);
            expected = Parameter.valueOf(MathLib.abs(P1VALUE),unit);
            TestContext.assertEquals("p1.abs()", expected, _abs);
            expected = Parameter.valueOf(MathLib.sqrt(P2VALUE),unit.root(2));
            TestContext.assertEquals("p2.sqrt()", expected, _sqrt);
            expected = Parameter.valueOf(MathLib.pow(P2VALUE,1./3),unit.root(3));
            TestContext.assertEquals("p2.root(3)", expected, _root3);
            expected = Parameter.valueOf(MathLib.pow(P1VALUE,3),unit.pow(3));
            TestContext.assertEquals("p1.pow(3)", expected, _pow3);
            expected = Parameter.valueOf(MathLib.round(P1VALUE),unit);
            TestContext.assertEquals("p1.round()", expected, _round);
            expected = Parameter.valueOf(MathTools.roundToPlace(P1VALUE,-2),unit);
            TestContext.assertEquals("p1.roundToPlace(-2)", expected, _round2Place);
            TestContext.assertEquals("p1.min(p2)", p1, _min);
            TestContext.assertEquals("p1.max(p2)", p2, _max);
            expected = Parameter.valueOf(MathLib.sin(P3VALUE*MathLib.PI/180),Dimensionless.UNIT);
            TestContext.assertEquals("Parameter.sin(p3)", expected, _sin);
            expected = Parameter.valueOf(MathLib.cos(P3VALUE*MathLib.PI/180),Dimensionless.UNIT);
            TestContext.assertEquals("Parameter.cos(p3)", expected, _cos);
            expected = Parameter.valueOf(MathLib.tan(P3VALUE*MathLib.PI/180),Dimensionless.UNIT);
            TestContext.assertEquals("Parameter.tan(p3)", expected, _tan);
            expected = Parameter.valueOf(MathLib.asin(P1VALUE/P2VALUE),Angle.UNIT);
            TestContext.assertEquals("Parameter.asin((Parameter)p1.divide(p2))", expected, _asin);
            expected = Parameter.valueOf(MathLib.acos(P1VALUE/P2VALUE),Angle.UNIT);
            TestContext.assertEquals("Parameter.acos((Parameter)p1.divide(p2))", expected, _acos);
            expected = Parameter.valueOf(MathLib.atan(P1VALUE/P2VALUE),Angle.UNIT);
            TestContext.assertEquals("Parameter.atan((Parameter)p1.divide(p2))", expected, _atan);
            expected = Parameter.valueOf(MathLib.atan2(P1VALUE,P2VALUE),Angle.UNIT);
            TestContext.assertEquals("Parameter.atan2(p1,p2)", expected, _atan2);
        }
        
    }
    
    public static class ParameterLogicalComparisons extends TestCase {
        private static final Unit unit = NonSI.FOOT;
        private static final double P1VALUE = 3.14159;
        private static final double P2VALUE = -42;
        private final Parameter<Length> p1 = Parameter.valueOf(P1VALUE,unit);
        private final Parameter<Length> p2 = Parameter.valueOf(P2VALUE,unit);
        private final Parameter<Length> p3 = Parameter.valueOf(P1VALUE+Parameter.EPS*2,unit);
        private boolean _equals1, _equals2;
        private int _compareTo;
        private boolean _isLessThan, _isGreaterThan, _isLargerThan;
        private boolean _isApproxEqual, _isApproxZero;
        private boolean _isNaN, _isInfinite1, _isInfinite2;
        
        
        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append("Parameter logical comparisons: p1 = ")
                    .append(p1).append(", p2 = ").append(p2).append(", p3 = p1 + ").append(Parameter.EPS);
        }
        
        @Override
        public void execute() {
            _equals1 = p1.equals(p2);
            _equals2 = p1.equals(Parameter.valueOf(P1VALUE,unit));
            _compareTo = p1.compareTo(p2);
            _isLessThan = p1.isLessThan(p2);
            _isGreaterThan = p1.isGreaterThan(p2);
            _isLargerThan = p2.isLargerThan(p1);
            _isApproxEqual = p1.isApproxEqual(p3);
            _isApproxZero = p1.isApproxZero();
            _isNaN = p1.isNaN();
            _isInfinite1 = p1.isInfinite();
            _isInfinite2 = p1.divide(Parameter.ZERO).isInfinite();
        }
        
        @Override
        public void validate() {
            TestContext.assertEquals("p1.equals(p2)", false, _equals1);
            TestContext.assertEquals("p1.equals(p1)", true, _equals2);
            TestContext.assertEquals("p1.compareTo(p2)", Double.compare(P1VALUE, P2VALUE), _compareTo);
            TestContext.assertEquals("p1.isLessThan(p2)", P1VALUE<P2VALUE, _isLessThan);
            TestContext.assertEquals("p1.isGreaterThan(p2)", P1VALUE>P2VALUE, _isGreaterThan);
            TestContext.assertEquals("p2.isLargerThan(p1)", MathLib.abs(P2VALUE)>MathLib.abs(P1VALUE), _isLargerThan);
            TestContext.assertEquals("p1.isApproxEqual(p3)",
                    MathTools.isApproxEqual(P1VALUE, P1VALUE+Parameter.EPS*2), _isApproxEqual);
            TestContext.assertEquals("p1.isApproxZero()", false, _isApproxZero);
            TestContext.assertEquals("p1.isNaN()", false, _isNaN);
            TestContext.assertEquals("p1.isInfinite()", false, _isInfinite1);
            TestContext.assertEquals("p1.divide(Parameter.ZERO).isInfinite()", true, _isInfinite2);
        }
        
    }

    public static class DCMatrixTests extends TestCase {
        private final DCMatrix m1 = DCMatrix.valueOf(1,2,3, 4,5,6, 7,8,9);
        private final DCMatrix m2 = DCMatrix.valueOf(1,0,1, 0,1,0, -1,0,1);
        private final Float64 p1 = Float64.valueOf(2);
        private final Vector3D<Length> v1 = Vector3D.valueOf(1, 2, 3, SI.METER);
		private final Parameter<Angle> psi = Parameter.valueOf(60,javax.measure.unit.NonSI.DEGREE_ANGLE);
		private final Parameter<Angle> theta = Parameter.ZERO_ANGLE;
		private final Parameter<Angle> phi = Parameter.ZERO_ANGLE;
        private final DCMatrix m3 = DCMatrix.getEulerTM(psi, theta, phi);
        private final Vector3D<Length> v2 = Vector3D.valueOf(1, 1, 1, SI.METER);
        private DCMatrix _opp, _plus, _minus, _times1, _times2, _inv, _transp;
        private Float64 _det1, _det2, _coef;
        private Float64Matrix _ten;
        private Vector3D<Length> _times3, _times4;
        private Quaternion _quat;
        private Float64Vector _row1, _col2, _diag, _vect;
        
        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append("DCMatrix");
        }
        
        @Override
        public void execute() {
            _row1 = m1.getRow(1);
            _col2 = m1.getColumn(2);
            _diag = m1.getDiagonal();
            _transp = m1.transpose();
            _vect = m1.vectorization();
            _opp = m1.opposite();
            _plus = m1.plus(m2);
            _minus = m1.minus(m2);
            _det1 = m1.determinant();
            _times1 = m1.times(p1);
            _det2 = m2.determinant();
            _inv = m2.inverse();
            _coef = m2.cofactor(0,2);
            _times2 = m1.times(m2);
            _ten = m1.tensor(m2);
            _times3 = m1.times(v1);
            _times4 = m3.times(v2);
            _quat = m3.toQuaternion();
        }
        
        @Override
        public void validate() {
            Float64Vector expectedF64v = Float64Vector.valueOf(4,5,6);
            TestContext.assertEquals("m1.getRow(1)", expectedF64v, _row1);
            expectedF64v = Float64Vector.valueOf(3,6,9);
            TestContext.assertEquals("m1.getColumn(2)", expectedF64v, _col2);
            expectedF64v = Float64Vector.valueOf(1,5,9);
            TestContext.assertEquals("m1.getDiagonal()", expectedF64v, _diag);
            DCMatrix expectedM = DCMatrix.valueOf(1,4,7, 2,5,8, 3,6,9);
            TestContext.assertEquals("m1.transpose()", expectedM, _transp);
            expectedF64v = Float64Vector.valueOf(1,4,7, 2,5,8, 3,6,9);
            TestContext.assertEquals("m1.vectorization()", expectedF64v, _vect);

            expectedM = DCMatrix.valueOf(-1,-2,-3, -4,-5,-6, -7,-8,-9);
            TestContext.assertEquals("m1.opposite()", expectedM, _opp);
            
            expectedM = DCMatrix.valueOf(2,2,4, 4,6,6, 6,8,10);
            TestContext.assertEquals("m1.plus(m2)", expectedM, _plus);
            
            expectedM = DCMatrix.valueOf(0,2,2, 4,4,6, 8,8,8);
            TestContext.assertEquals("m1.minus(m2)", expectedM, _minus);
            
            Float64 expectedF64 = Float64.valueOf(6.661338147750939E-16);
            TestContext.assertEquals("m1.determinant()", expectedF64, _det1);
            
            expectedM = DCMatrix.valueOf(2,4,6, 8,10,12, 14,16,18);
            TestContext.assertEquals("m1.times(p1)", expectedM, _times1);
            
            expectedF64 = Float64.valueOf(2);
            TestContext.assertEquals("m2.determinant()", expectedF64, _det2);
            
            expectedM = DCMatrix.valueOf(0.5, 0, -0.5, 0, 1, 0, 0.5, 0, 0.5);
            TestContext.assertEquals("m2.inverse()", expectedM, _inv);
            
            expectedF64 = Float64.valueOf(1);
            TestContext.assertEquals("m2.cofactor(0,2)", expectedF64, _coef);
            
            expectedM = DCMatrix.valueOf(-2,2,4, -2,5,10, -2,8,16);
            TestContext.assertEquals("m1.times(m2)", expectedM, _times2);
            
            Float64Vector row1 = Float64Vector.valueOf(1,0,1,2,0,2,3,0,3);
            Float64Vector row2 = Float64Vector.valueOf(0,1,0,0,2,0,0,3,0);
            Float64Vector row3 = Float64Vector.valueOf(-1,0,1,-2,0,2,-3,0,3);
            Float64Vector row4 = Float64Vector.valueOf(4,0,4,5,0,5,6,0,6);
            Float64Vector row5 = Float64Vector.valueOf(0,4,0,0,5,0,0,6,0);
            Float64Vector row6 = Float64Vector.valueOf(-4,0,4,-5,0,5,-6,0,6);
            Float64Vector row7 = Float64Vector.valueOf(7,0,7,8,0,8,9,0,9);
            Float64Vector row8 = Float64Vector.valueOf(0,7,0,0,8,0,0,9,0);
            Float64Vector row9 = Float64Vector.valueOf(-7,0,7,-8,0,8,-9,0,9);
            Float64Matrix expectedM64 = Float64Matrix.valueOf(row1,row2,row3,row4,row5,row6,row7,row8,row9);
            TestContext.assertEquals("m1.tensor(m2)", expectedM64, _ten);

            Vector3D expectedV = Vector3D.valueOf(14,32,50,SI.METER);
            TestContext.assertEquals("m2.cofactor(0,2)", expectedV, _times3);
            
            expectedV = Vector3D.valueOf(-0.3660254037844385,1.3660254037844388,1,SI.METER);
            TestContext.assertEquals("m3.times(v2)", expectedV, _times4);
            
            Quaternion expectedQ = Quaternion.valueOf(0, 0, 0.5, MathLib.sin(60*MathLib.PI/180));
            TestContext.assertEquals("m3.toQuaternion()", expectedQ, _quat);
        }
    }

    public static class QuaternionTests extends TestCase {
		private final Parameter<Angle> psi = Parameter.valueOf(60,javax.measure.unit.NonSI.DEGREE_ANGLE);
		private final Parameter<Angle> theta = Parameter.ZERO_ANGLE;
		private final Parameter<Angle> theta2 = Parameter.valueOf(30,NonSI.DEGREE_ANGLE);
		private final Parameter<Angle> phi = Parameter.ZERO_ANGLE;
        private final DCMatrix m1 = DCMatrix.getEulerTM(psi, theta, phi);
        private final Quaternion q1 = Quaternion.valueOf(m1);
        private final DCMatrix m2 = DCMatrix.getEulerTM(psi, theta2, phi);
        private final Quaternion q3 = Quaternion.valueOf(Vector3D.UNIT_Y, theta2);
        private final Vector3D<Length> v1 = Vector3D.valueOf(1, 0, 0, SI.METER);
        private Float64 _norm, _s1;
        private Vector3D _v1, _v2, _v3;
        private Quaternion _opp, _conj, _inv, _q2, _q4, _Lq1tq3, _plus, _minus, _unit;
        private DCMatrix _dcm;
        
        @Override
        public CharSequence getDescription() {
            return TextBuilder.newInstance().append("Quaternion");
        }
        
        @Override
        public void execute() {
            _opp = q1.opposite();
            _norm = q1.norm();
            _v1 = q1.getVector();
            _s1 = q1.getScalar();
            _conj = q1.conjugate();
            _inv = q1.inverse();
            _dcm = q1.toDCM();
            _v2 = q1.transform(v1);
            _q2 = Quaternion.valueOf(Vector3D.UNIT_Z, psi);
            _q4 = q1.times(q3);
            _v3 = _q4.transform(v1);
            _Lq1tq3 = Quaternion.valueOf(q1.toLeftMatrix().times(q3.toFloat64Vector()));
            _plus = q1.plus(q3);
            _minus = q1.minus(q3);
            _unit = q3.toUnit();
        }
        
        @Override
        public void validate() {
            Float64 expectedF64 = Float64.valueOf(0.99999999999999989);
            TestContext.assertEquals("q1.norm()", expectedF64, _norm);
            
            Vector3D expectedV3D = Vector3D.valueOf(0,0,0.5,Dimensionless.UNIT);
            TestContext.assertEquals("q1.getVector()", expectedV3D, _v1);
            
            expectedF64 = Float64.valueOf(MathLib.sin(60*MathLib.PI/180));
            TestContext.assertEquals("q1.getScalar()", expectedF64, _s1);
            
            Quaternion expectedQ = Quaternion.valueOf(0,0,-0.5,-MathLib.sin(60*MathLib.PI/180));
            TestContext.assertEquals("q1.opposite()", expectedQ, _opp);
            
            expectedQ = Quaternion.valueOf(0,0,-0.5,MathLib.sin(60*MathLib.PI/180));
            TestContext.assertEquals("q1.conjugate()", expectedQ, _conj);
            
            expectedQ = Quaternion.valueOf(0,0,-0.5,MathLib.sin(60*MathLib.PI/180));
            TestContext.assertEquals("q1.inverse()", true, expectedQ.isApproxEqual(_inv));
            
            DCMatrix expectedDCM = m1;
            TestContext.assertEquals("q1.toDCM()", true, expectedDCM.isApproxEqual(_dcm));
            
            expectedV3D = Vector3D.valueOf(0.5,MathLib.sin(60*MathLib.PI/180),0,SI.METER);
            TestContext.assertEquals("q1.transform(v1)", true, expectedV3D.isApproxEqual(_v2));
            
            TestContext.assertEquals("Quaternion.valueOf(Vector3D.UNIT_Z, psi)", true, q1.isApproxEqual(_q2));
            
            expectedQ = Quaternion.valueOf(-0.12940952255126037,0.22414386804201336,0.48296291314453416,0.8365163037378078);
            TestContext.assertEquals("q1.times(q3)", true, expectedQ.isApproxEqual(_q4));
            
            expectedV3D = m2.times(v1);
            TestContext.assertEquals("q4.transform(v1)", true, expectedV3D.isApproxEqual(_v3));
            
            TestContext.assertEquals("q1.toLeftMatrix().times(q3.toFloat64Vector())", true, expectedQ.isApproxEqual(_Lq1tq3));
            
            expectedQ = Quaternion.valueOf(0,0.25881904510252074,0.5,MathLib.sin(60*MathLib.PI/180)+0.9659258262890683);
            TestContext.assertEquals("q1.plus(q3)", true, expectedQ.isApproxEqual(_plus));
            
            expectedQ = Quaternion.valueOf(0,-0.25881904510252074,0.5,MathLib.sin(60*MathLib.PI/180)-0.9659258262890683);
            TestContext.assertEquals("q1.minus(q3)", true, expectedQ.isApproxEqual(_minus));
            
            expectedQ = Quaternion.valueOf(0.0, 0.25881904510252074, 0.0, 0.9659258262890683);
            TestContext.assertEquals("q3.toUnit()", true, expectedQ.isApproxEqual(_unit));
            
        }
    }
}
