/*
 *   EulerAngles -- Three angles representing the orientation between two reference frames.
 *
 *   Copyright (C) 2009-2015, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import java.text.MessageFormat;
import javax.measure.quantity.*;
import javax.measure.unit.Unit;
import javax.measure.unit.SI;
import javax.measure.unit.NonSI;
import javax.measure.converter.ConversionException;
import javolution.context.ObjectFactory;
import javolution.context.ImmortalContext;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import javolution.xml.XMLSerializable;
import javolution.text.Text;
import javolution.text.TextBuilder;
import javolution.util.FastMap;
import javolution.util.FastTable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.*;

/**
 * This class represents the three Euler angles; theta1, theta2, theta3. Euler angles may
 * be used to represents a relative attitude (attitude transformation or rotation) between
 * two different reference frames; B wrt A or BA. It can be used to transform coordinates
 * in reference frame A to reference frame B (A2B).
 * <p>
 * Reference: Shoemake, Ken (1994), "Euler angle conversion", in Paul Heckbert, Graphics
 * Gems IV, San Diego: Academic Press Professional, pp. 222-229, ISBN 978-0-12-336155-4.
 * </p>
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 4, 2009
 * @version November 4, 2015
 */
public final class EulerAngles extends AbstractRotation<EulerAngles> implements XMLSerializable {

    private static final long serialVersionUID = 6575073905088777817L;

    /**
     * Enumeration of the axes that a rotation may be made around.
     */
    public static enum Axis {

        X, Y, Z
    };

    /**
     * Enumeration of the parity of the axis permutation.
     */
    public static enum Parity {

        EVEN, ODD
    };

    /**
     * Indicates that the initial axis is NOT repeated as the last axis.
     */
    public static final boolean NO = false;

    /**
     * Indicates repetition of initial axis as the last.
     */
    public static final boolean YES = true;

    /**
     * Enumeration of the frame from which the axes are taken.
     */
    public static enum Frame {

        STATIC, ROTATING
    };

    /**
     * Enumeration of all the combinations of the Euler angle orders for both static ("s")
     * and rotating ("r") axes.
     */
    public static enum Order {

        //  Static axes.

        XYZs(Axis.X, Parity.EVEN, NO, Frame.STATIC),
        XYXs(Axis.X, Parity.EVEN, YES, Frame.STATIC),
        XZYs(Axis.X, Parity.ODD, NO, Frame.STATIC),
        XZXs(Axis.X, Parity.ODD, YES, Frame.STATIC),
        YZXs(Axis.Y, Parity.EVEN, NO, Frame.STATIC),
        YZYs(Axis.Y, Parity.EVEN, YES, Frame.STATIC),
        YXZs(Axis.Y, Parity.ODD, NO, Frame.STATIC),
        YXYs(Axis.Y, Parity.ODD, YES, Frame.STATIC),
        ZXYs(Axis.Z, Parity.EVEN, NO, Frame.STATIC),
        ZXZs(Axis.Z, Parity.EVEN, YES, Frame.STATIC),
        ZYXs(Axis.Z, Parity.ODD, NO, Frame.STATIC),
        ZYZs(Axis.Z, Parity.ODD, YES, Frame.STATIC),
        //  Rotating axes.
        ZYXr(Axis.X, Parity.EVEN, NO, Frame.ROTATING),
        XYXr(Axis.X, Parity.EVEN, YES, Frame.ROTATING),
        YZXr(Axis.X, Parity.ODD, NO, Frame.ROTATING),
        XZXr(Axis.X, Parity.ODD, YES, Frame.ROTATING),
        XZYr(Axis.Y, Parity.EVEN, NO, Frame.ROTATING),
        YZYr(Axis.Y, Parity.EVEN, YES, Frame.ROTATING),
        ZXYr(Axis.Y, Parity.ODD, NO, Frame.ROTATING),
        YXYr(Axis.Y, Parity.ODD, YES, Frame.ROTATING),
        YXZr(Axis.Z, Parity.EVEN, NO, Frame.ROTATING),
        ZXZr(Axis.Z, Parity.EVEN, YES, Frame.ROTATING),
        XYZr(Axis.Z, Parity.ODD, NO, Frame.ROTATING),
        ZYZr(Axis.Z, Parity.ODD, YES, Frame.ROTATING);

        //  A table used to cross reference Order elements and their parts
        //  encoded as a String.
        private static final FastMap<Text, Order> lookup;

        static {
            TextBuilder buf = TextBuilder.newInstance();
            ImmortalContext.enter();
            try {
                lookup = FastMap.newInstance();

                //  Fill in the lookup table.
                for (Order order : Order.values()) {
                    buf.append(order.getInitialAxis());
                    buf.append(order.getParity());
                    buf.append(order.getRepeat1st());
                    buf.append(order.getFrame());
                    Text txt = buf.toText();
                    lookup.put(txt, order);
                    buf.clear();
                }
            } finally {
                ImmortalContext.exit();
            }
        }

        private final Axis initialAxis;
        private final Parity parity;
        private final boolean rep;
        private final Frame frame;

        private Order(Axis initialAxis, Parity parity, boolean repeat1st, Frame frame) {
            this.initialAxis = initialAxis;
            this.parity = parity;
            this.rep = repeat1st;
            this.frame = frame;
        }

        /**
         * Returns the initial rotation axis.
         */
        public Axis getInitialAxis() {
            return initialAxis;
        }

        /**
         * Returns the parity of the axis permutation.
         */
        public Parity getParity() {
            return parity;
        }

        /**
         * Returns true if the initial axis is repeated as the last and false if it is
         * not.
         */
        public boolean getRepeat1st() {
            return rep;
        }

        /**
         * Returns the frame from which the axes are taken.
         */
        public Frame getFrame() {
            return frame;
        }

        /**
         * Returns the Order that corresponds to the specified order parts.
         *
         * @param initialAxis The initial rotation axis (X, Y, or Z).
         * @param parity      The parity of the axis permutation (EVEN or ODD).
         * @param repeat1st   Repeat the initial axis as the last if true, do not repeat
         *                    if false.
         * @param frame       The frame from which the axes are taken (STATIC or
         *                    ROTATING).
         * @return The Order that corresponds to the input order parts.
         */
        public static Order valueOf(Axis initialAxis, Parity parity, boolean repeat1st, Frame frame) {
            TextBuilder buf = TextBuilder.newInstance();
            buf.append(initialAxis);
            buf.append(parity);
            buf.append(repeat1st);
            buf.append(frame);
            Text txt = buf.toText();
            TextBuilder.recycle(buf);
            return lookup.get(txt);
        }
    }

    /**
     * The Euler order typically used in aerospace applications. Also known as Tait-Bryan
     * rotations. Yaw about Z axis, pitch about yawed Y axis, and finally roll about
     * pitch-yawed X axis.
     *
     * @see
     * <a href="https://en.wikipedia.org/wiki/Euler_angles#Tait.E2.80.93Bryan_angles">
     * Wikipedia: Euler Angles: Tait–Bryan angles</a>
     */
    public static final Order YAW_PITCH_ROLL = Order.ZYXr;

    //  Floating point epsilon.
    private static final double FLT_EPSILON = Parameter.EPS;

    //  The Euler Angle elements.
    private Vector3D<Angle> _theta;

    //  The Euler axis order for this set of angles.
    private Order _order;

    /**
     * Returns an {@link EulerAngles} instance holding the specified
     * <code>Vector3D&lt;Angle&gt;</code> values for the rotation angle elements and the
     * specified Euler order information.
     *
     * @param theta       The rotation angles about each axis (0=1st axis, 1=2nd axis,
     *                    2=3rd axis).
     * @param initialAxis The initial rotation axis (X, Y, or Z).
     * @param parity      The parity of the axis permutation (EVEN or ODD).
     * @param repeat1st   Repeat the initial axis as the last if true, do not repeat if
     *                    false.
     * @param frame       The frame from which the axes are taken (STATIC or ROTATING).
     * @return The EulerAngles having the specified values.
     */
    public static EulerAngles valueOf(Vector3D<Angle> theta, Axis initialAxis, Parity parity, boolean repeat1st, Frame frame) {
        return valueOf(theta, Order.valueOf(initialAxis, parity, repeat1st, frame));
    }

    /**
     * Returns an {@link EulerAngles} instance holding the specified
     * <code>Vector3D&lt;Angle&gt;</code> values for the rotation angle elements and the
     * specified Euler order information.
     *
     * @param theta      The rotation angles about each axis (0=1st axis, 1=2nd axis,
     *                   2=3rd axis).
     * @param eulerOrder The Euler order to use (one of the enumerations in this class).
     * @return The EulerAngles having the specified values.
     */
    public static EulerAngles valueOf(Vector3D<Angle> theta, Order eulerOrder) {
        EulerAngles ea = EulerAngles.newInstance(eulerOrder);
        ea._theta = theta;
        return ea;
    }

    /**
     * Returns an {@link EulerAngles} instance holding the specified
     * <code>Parameter&lt;Angle&gt;</code> values for the rotation angle elements and the
     * specified Euler order information.
     *
     * @param theta1     The rotation angle about the 1st axis.
     * @param theta2     The rotation angle about the 2nd axis.
     * @param theta3     The rotation angle about the 3rd axis.
     * @param eulerOrder The Euler order to use (one of the enumerations in this class).
     * @return The EulerAngles having the specified values.
     */
    public static EulerAngles valueOf(Parameter<Angle> theta1, Parameter<Angle> theta2, Parameter<Angle> theta3, Order eulerOrder) {
        Vector3D<Angle> theta = Vector3D.valueOf(theta1, theta2, theta3);
        return valueOf(theta, eulerOrder);
    }

    /**
     * Returns an {@link EulerAngles} object constructed from the specified rotation
     * transformation.
     *
     * @param transform The rotation transform representing a rotation from frame A to
     *                  frame B.
     * @param order     The Euler order to use (one of the enumerations in this class).
     * @return The Euler angles representing the specified direction cosine rotation
     *         matrix.
     */
    public static EulerAngles valueOf(Rotation<?> transform, Order order) {

        //  Convert the rotation to a DCM.
        DCMatrix dc = transform.toDCM();

        int i = getInitialAxisIndex(order);
        int j = getMiddleAxisIndex(order);
        int k = getLastAxisIndex(order);

        double Mii = dc.getValue(i, i);
        double Mji = dc.getValue(j, i);
        double Mjj = dc.getValue(j, j);
        double Mjk = dc.getValue(j, k);
        double Mki = dc.getValue(k, i);

        double eax, eay, eaz;
        if (order.getRepeat1st() == YES) {
            double Mij = dc.getValue(i, j);
            double Mik = dc.getValue(i, k);

            double sy = sqrt(Mij * Mij + Mik * Mik);
            if (sy > 16 * FLT_EPSILON) {
                eax = atan2(Mij, Mik);
                eay = atan2(sy, Mii);
                eaz = atan2(Mji, -Mki);

            } else {
                eax = atan2(-Mjk, Mjj);
                eay = atan2(sy, Mii);
                eaz = 0;
            }

        } else {
            double Mkj = dc.getValue(k, j);
            double Mkk = dc.getValue(k, k);

            double cy = sqrt(Mii * Mii + Mji * Mji);
            if (cy > 16 * FLT_EPSILON) {
                eax = atan2(Mkj, Mkk);
                eay = atan2(-Mki, cy);
                eaz = atan2(Mji, Mii);

            } else {
                eax = atan2(-Mjk, Mjj);
                eay = atan2(-Mki, cy);
                eaz = 0;
            }
        }

        if (order.getParity() == Parity.ODD) {
            eax = -eax;
            eay = -eay;
            eaz = -eaz;
        }

        if (order.getFrame() == Frame.ROTATING) {
            double t = eax;
            eax = eaz;
            eaz = t;
        }

        //  Bound the angles to the range 0 to 2*PI.
        eax = bound2Pi(eax);
        eay = bound2Pi(eay);
        eaz = bound2Pi(eaz);

        Vector3D<Angle> theta = Vector3D.valueOf(eax, eay, eaz, SI.RADIAN);
        return valueOf(theta, order);
    }

    /**
     * Returns the input angle (in radians) bounded to the range 0 to 2*PI.
     */
    private static double bound2Pi(double angle) {
        while (angle > TWO_PI)
            angle -= TWO_PI;
        while (angle < 0)
            angle += TWO_PI;
        return angle;
    }

    /**
     * Returns the value of an element from this set of Euler angles (0=1st axis, 1=2nd
     * axis, 2=3rd axis).
     *
     * @param i the dimension index (0=1st axis, 1=2nd axis, 2=3rd axis).
     * @return the value of the element at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &gt; dimension()-1)</code>
     */
    public Parameter<Angle> get(int i) {
        return _theta.get(i);
    }

    /**
     * Returns the value of a floating point number from this set of Euler angles (0=1st
     * axis, 1=2nd axis, 2=3rd axis). This returns the value of the specified Euler angle
     * in whatever the current units of the Euler angles are.
     *
     * @param i the floating point number index (0=1st axis, 1=2nd axis, 2=3rd axis).
     * @return the value of the floating point number at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i &gt; dimension()-1)</code>
     */
    public double getValue(int i) {
        return _theta.getValue(i);
    }

    /**
     * Returns the Euler order for this set of Euler angles.
     *
     * @return The Euler order for this set of Euler angles.
     */
    public Order getEulerOrder() {
        return _order;
    }

    /**
     * Returns the negation of this set of Euler angles (each angle is negated).
     *
     * @return <code>-this</code>.
     */
    public EulerAngles opposite() {
        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = _theta.opposite();
        return ea;
    }

    /**
     * Returns the sum of this set of Euler angles with the one specified. The unit of the
     * output set of Euler angles will be the units of this set of Euler angles.
     *
     * @param that the Euler angles to be added.
     * @return <code>this + that</code>.
     */
    public EulerAngles plus(EulerAngles that) {

        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.plus(that._theta);

        return ea;
    }

    /**
     * Returns the sum of this set of Euler angles with the one specified. The unit of the
     * output set of Euler angles will be the units of this set of Euler angles.
     *
     * @param that the set of Euler angles of angles to be added.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     * @throws ConversionException if the current model does not allow for conversion the
     * units of the input vector to an Angle unit.
     */
    public EulerAngles plus(Vector<Parameter<Angle>> that) {
        if (!that.get(0).getUnit().isCompatible(Angle.UNIT))
            throw new ConversionException(RESOURCES.getString("eaBadAngleUnits"));

        //  Convert input vector to a Vector3D object.
        Vector3D<Angle> thatV = Vector3D.valueOf(that);

        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.plus(thatV);

        return ea;
    }

    /**
     * Returns the sum of this set of Euler angles with the angle specified. The input
     * parameter is added to each component of this set of Euler angles. The unit of the
     * output Euler angles will be the units of this set of Euler angles.
     *
     * @param that the angle to be added to each element of this set of Euler angles.
     * @return <code>this + that</code>.
     */
    public EulerAngles plus(Parameter<Angle> that) {

        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.plus(that);

        return ea;
    }

    /**
     * Returns the difference between this set of Euler angles and the one specified.
     *
     * @param that the Euler angles to be subtracted.
     * @return <code>this - that</code>.
     */
    public EulerAngles minus(EulerAngles that) {

        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.minus(that._theta);

        return ea;
    }

    /**
     * Returns the difference between this set of Euler angles and the one specified.
     *
     * @param that the vector of angles to be subtracted.
     * @return <code>this - that</code>.
     * @throws DimensionException if vector dimensions are different.
     * @throws ConversionException if the current model does not allow for conversion the
     * units of the input vector to an Angle unit.
     */
    public EulerAngles minus(Vector<Parameter<Angle>> that) {
        if (!that.get(0).getUnit().isCompatible(Angle.UNIT))
            throw new ConversionException(RESOURCES.getString("eaBadAngleUnits"));

        //  Convert input vector to a Vector3D object.
        Vector3D<Angle> thatV = Vector3D.valueOf(that);

        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.minus(thatV);

        return ea;
    }

    /**
     * Subtracts the supplied angle from each element of this set of Euler angles and
     * returns the result. The unit of the output Euler angles will be the units of this
     * set of Euler angles.
     *
     * @param that the angle to be subtracted from each element of this set of Euler
     *             angles.
     * @return <code>this - that</code>.
     */
    public EulerAngles minus(Parameter<Angle> that) {

        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.minus(that);

        return ea;
    }

    /**
     * Returns the product of this set of Euler angles with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    public EulerAngles times(double k) {
        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = this._theta.times(k);
        return ea;
    }

    /**
     * Returns this set of Euler angles with each angle divided by the specified divisor.
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    public EulerAngles divide(double divisor) {
        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = _theta.divide(divisor);
        return ea;
    }

    /**
     * Transforms a 3D vector from frame A to B using this set of Euler angles.
     *
     * @param <Q> The Quantity (unit type) of the input and output vectors.
     * @param v   the vector expressed in frame A.
     * @return the vector expressed in frame B.
     */
    @Override
    public <Q extends Quantity> Vector3D<Q> transform(Coordinate3D<Q> v) {

        Quaternion q = toQuaternion();
        Vector3D<Q> out = q.transform(v);

        return out;
    }

    /**
     * Returns the spatial inverse of this transformation: AB rather than BA.
     *
     * @return <code>this'</code>
     */
    @Override
    public EulerAngles transpose() {
        Quaternion qT = toQuaternion().transpose();
        return EulerAngles.valueOf(qT, getEulerOrder());
    }

    /**
     * Returns the product of this set of Euler angles and the specified rotation
     * transform. If this set of Euler angles is BA and that is AC then the returned value
     * is: BC = BA · AC (or C2B = A2B · C2A).
     *
     * @param that the rotation transform multiplier.
     * @return <code>this · that</code>
     */
    @Override
    public EulerAngles times(Rotation<?> that) {
        return EulerAngles.valueOf(this.toDCM().times(that), this.getEulerOrder());
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this set of Euler angles
     * are stated in.
     *
     * @return The angular unit in which the values in this set of Euler angles are
     *         stated.
     */
    public Unit<Angle> getUnit() {
        return _theta.getUnit();
    }

    /**
     * Returns the equivalent to this set of Euler angles but stated in the specified
     * angle unit.
     *
     * @param unit the angle unit of the Euler angles to be returned.
     * @return A set of Euler angles equivalent to this set but stated in the specified
     *         unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public EulerAngles to(Unit<Angle> unit) {
        EulerAngles ea = EulerAngles.newInstance(_order);
        ea._theta = _theta.to(unit);
        return ea;
    }

    /**
     * Returns the values stored in this set of Euler angles, in the current units, as a
     * Float64Vector with the values ordered (1st axis angle, 2nd axis angle, 3rd axis
     * angle).
     *
     * @return The values stored in this set of Euler angles as a Float64Vector.
     */
    public Float64Vector toFloat64Vector() {
        return _theta.toFloat64Vector();
    }

    /**
     * Returns a direction cosine transformation matrix from this set of Euler angles.
     *
     * @return a direction cosine matrix that converts from frame A to B.
     * @see <a href="http://en.wikipedia.org/wiki/Rotation_matrix#EulerAngles">
     * Wikipedia: Rotation Matrix-EulerAngles</a>
     */
    @Override
    public DCMatrix toDCM() {
        StackContext.enter();
        try {
            double eax = _theta.get(Vector3D.X).getValue(SI.RADIAN);
            double eay = _theta.get(Vector3D.Y).getValue(SI.RADIAN);
            double eaz = _theta.get(Vector3D.Z).getValue(SI.RADIAN);

            if (_order.getFrame() == Frame.ROTATING) {
                double t = eax;
                eax = eaz;
                eaz = t;
            }

            if (_order.getParity() == Parity.ODD) {
                eax = -eax;
                eay = -eay;
                eaz = -eaz;
            }

            double ti = eax, tj = eay, th = eaz;
            double ci = cos(ti), cj = cos(tj), ch = cos(th);
            double si = sin(ti), sj = sin(tj), sh = sin(th);
            double cc = ci * ch, cs = ci * sh, sc = si * ch, ss = si * sh;

            int i = getInitialAxisIndex(_order);
            int j = getMiddleAxisIndex(_order);
            int k = getLastAxisIndex(_order);

            double[] a = QA_FACTORY.object();
            FastTable<Float64Vector> rows = FastTable.newInstance();
            rows.setSize(3);
            if (_order.getRepeat1st() == YES) {
                a[i] = cj;
                a[j] = sj * si;
                a[k] = sj * ci;
                rows.set(i, Float64Vector.valueOf(a));

                a[i] = sj * sh;
                a[j] = -cj * ss + cc;
                a[k] = -cj * cs - sc;
                rows.set(j, Float64Vector.valueOf(a));

                a[i] = -sj * ch;
                a[j] = cj * sc + cs;
                a[k] = cj * cc - ss;
                rows.set(k, Float64Vector.valueOf(a));

            } else {
                a[i] = cj * ch;
                a[j] = sj * sc - cs;
                a[k] = sj * cc + ss;
                rows.set(i, Float64Vector.valueOf(a));

                a[i] = cj * sh;
                a[j] = sj * ss + cc;
                a[k] = sj * cs - sc;
                rows.set(j, Float64Vector.valueOf(a));

                a[i] = -sj;
                a[j] = cj * si;
                a[k] = cj * ci;
                rows.set(k, Float64Vector.valueOf(a));
            }

            DCMatrix M = DCMatrix.valueOf(rows);
            return StackContext.outerCopy(M);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns a quaternion from this set of Euler angles.
     *
     * @return a quaternion that transforms from frame A to B.
     * @see <a href="http://en.wikipedia.org/wiki/Quaternion">
     * Wikipedia: Quaternion</a>
     */
    @Override
    public Quaternion toQuaternion() {
        StackContext.enter();
        try {
            double eax = _theta.get(Vector3D.X).getValue(SI.RADIAN);
            double eay = _theta.get(Vector3D.Y).getValue(SI.RADIAN);
            double eaz = _theta.get(Vector3D.Z).getValue(SI.RADIAN);

            if (_order.getFrame() == Frame.ROTATING) {
                double t = eax;
                eax = eaz;
                eaz = t;
            }

            if (_order.getParity() == Parity.ODD)
                eay = -eay;

            double ti = 0.5 * eax;
            double tj = 0.5 * eay;
            double th = 0.5 * eaz;

            double ci = cos(ti), cj = cos(tj), ch = cos(th);
            double si = sin(ti), sj = sin(tj), sh = sin(th);
            double cc = ci * ch, cs = ci * sh, sc = si * ch, ss = si * sh;

            int i = getInitialAxisIndex(_order);
            int j = getMiddleAxisIndex(_order);
            int k = getLastAxisIndex(_order);

            double[] a = QA_FACTORY.object();
            double qw;
            if (_order.getRepeat1st() == YES) {
                a[i] = cj * (cs + sc);
                a[j] = sj * (cc + ss);
                a[k] = sj * (cs - sc);
                qw = cj * (cc - ss);

            } else {
                a[i] = cj * sc - sj * cs;
                a[j] = cj * ss + sj * cc;
                a[k] = cj * cs - sj * sc;
                qw = cj * cc + sj * ss;
            }

            if (_order.getParity() == Parity.ODD)
                a[j] = -a[j];

            Quaternion Q = Quaternion.valueOf(a[Quaternion.X], a[Quaternion.Y], a[Quaternion.Z], qw);
            return StackContext.outerCopy(Q);

        } finally {
            StackContext.exit();
        }
    }

    /**
     * Return the index to the 1st (initial) axis (0=X, 1=Y, 2=Z).
     */
    private static int getInitialAxisIndex(Order order) {
        return order.getInitialAxis().ordinal();
    }

    private static final int[] next = {1, 2, 0, 1};

    /**
     * Return the index to the 2nd (middle) axis (0=X, 1=Y, 2=Z).
     */
    private static int getMiddleAxisIndex(Order order) {
        int i = getInitialAxisIndex(order);
        if (order.getParity() == Parity.ODD)
            ++i;
        return next[i];
    }

    /**
     * Return the index to the 3rd (last) axis (0=X, 1=Y, 2=Z).
     */
    private static int getLastAxisIndex(Order order) {
        int i = getInitialAxisIndex(order);
        if (order.getParity() != Parity.ODD)
            ++i;
        return next[i];
    }

    /**
     * Returns a copy of this set of Euler angles
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this set of Euler angles.
     */
    @Override
    public EulerAngles copy() {
        return copyOf(this);
    }

    /**
     * Returns the text representation of this set of Euler angles.
     *
     * @return the text representation of this set of Euler angles.
     */
    @Override
    public Text toText() {
        final int dimension = 3;
        TextBuilder tmp = TextBuilder.newInstance();
        if (this.isApproxEqual(IDENTITY))
            tmp.append("{IDENTITY}");
        else {
            tmp.append('{');
            for (int i = 0; i < dimension; i++) {
                tmp.append(get(i));
                tmp.append(", ");
            }
            tmp.append(_order);
            tmp.append('}');
        }
        Text txt = tmp.toText();
        TextBuilder.recycle(tmp);
        return txt;
    }

    /**
     * Compares this EulerAngles against the specified object for strict equality (same
     * rotation type and same values).
     *
     * @param that the object to compare with.
     * @return <code>true</code> if this rotation is identical to that rotation;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if ((that == null) || (that.getClass() != this.getClass()))
            return false;

        EulerAngles m = (EulerAngles)that;
        if (!_order.equals(m._order))
            return false;

        return _theta.equals(m._theta);
    }

    /**
     * Returns the hash code for this rotation.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _order.hashCode();
        hash = hash * 31 + var_code;

        var_code = _theta.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    /**
     * Holds the default XML representation. For example:
     * <pre>
     *   &lt;EulerAngles unit = "rad" order = "ZYXr"&gt;
     *       &lt;X value="0.349" /&gt;
     *       &lt;Y value="6.11" /&gt;
     *       &lt;Z value="0.524" /&gt;
     *   &lt;/EulerAngles&gt;
     * </pre>
     */
    protected static final XMLFormat<EulerAngles> XML = new XMLFormat<EulerAngles>(EulerAngles.class) {

        @Override
        public EulerAngles newInstance(Class<EulerAngles> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @SuppressWarnings("unchecked")
        @Override
        public void read(InputElement xml, EulerAngles ea) throws XMLStreamException {

            Unit<Angle> unit = (Unit<Angle>)Unit.valueOf(xml.getAttribute("unit"));
            String orderStr = xml.getAttribute("order").toString();
            try {

                Order order = Order.valueOf(orderStr);
                ea._order = order;

            } catch (IllegalArgumentException e) {
                throw new XMLStreamException(MessageFormat.format(RESOURCES.getString("eaBadOrder"), orderStr));
            } catch (NullPointerException e) {
                throw new XMLStreamException(RESOURCES.getString("eaNoOrderAttribute"));
            }

            double x = xml.get("Theta1", Float64.class).doubleValue();
            double y = xml.get("Theta2", Float64.class).doubleValue();
            double z = xml.get("Theta3", Float64.class).doubleValue();
            ea._theta = Vector3D.valueOf(x, y, z, unit);

            if (xml.hasNext())
                throw new XMLStreamException(RESOURCES.getString("toManyXMLElementsErr"));
        }

        @Override
        public void write(EulerAngles ea, OutputElement xml) throws XMLStreamException {

            xml.setAttribute("unit", ea.getUnit().toString());
            xml.setAttribute("order", ea.getEulerOrder().toString());
            xml.add(Float64.valueOf(ea._theta.getValue(Vector3D.X)), "Theta1", Float64.class);
            xml.add(Float64.valueOf(ea._theta.getValue(Vector3D.Y)), "Theta2", Float64.class);
            xml.add(Float64.valueOf(ea._theta.getValue(Vector3D.Z)), "Theta3", Float64.class);

        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private static final ObjectFactory<EulerAngles> FACTORY = new ObjectFactory<EulerAngles>() {
        @Override
        protected EulerAngles create() {
            return new EulerAngles();
        }
    };

    private static EulerAngles newInstance(Order order) {
        EulerAngles ea = FACTORY.object();
        ea._order = order;
        return ea;
    }

    private static EulerAngles copyOf(EulerAngles original) {
        EulerAngles ea = EulerAngles.newInstance(original.getEulerOrder());
        ea._theta = original._theta.copy();
        return ea;
    }

    private EulerAngles() {
    }

    private static final ObjectFactory<double[]> QA_FACTORY = new ObjectFactory<double[]>() {
        @Override
        protected double[] create() {
            return new double[3];
        }
    };

    /**
     * Tests the methods in this class.
     *
     * @param args Command line arguments (ignored)
     */
    public static void main(String args[]) {
        System.out.println("Testing EulerAngles:");

        Parameter<Angle> psi = Parameter.valueOf(60, NonSI.DEGREE_ANGLE);
        Parameter<Angle> theta = Parameter.valueOf(30, NonSI.DEGREE_ANGLE);
        Parameter<Angle> phi = Parameter.valueOf(-10, NonSI.DEGREE_ANGLE);
        System.out.println("psi = " + psi + ", theta = " + theta + ", phi = " + phi);

        EulerAngles ea1 = EulerAngles.valueOf(psi, theta, phi, YAW_PITCH_ROLL);
        DCMatrix m1 = DCMatrix.getEulerTM(psi, theta, phi);
        Quaternion q1 = Quaternion.valueOf(m1);
        System.out.println("ea1 = " + ea1);
        System.out.println("ea1.toDCM() = \n" + ea1.toDCM());
        System.out.println("m1          = \n" + m1);
        System.out.println("ea1.toQuaternion() = " + ea1.toQuaternion());
        System.out.println("q1                 = " + q1);

        Vector3D<Length> v1 = Vector3D.valueOf(1, 1, 1, SI.METER);
        System.out.println("v1 = " + v1);
        System.out.println("ea1.transform(v1) = " + ea1.transform(v1));
        System.out.println("q1.transform(v1)  = " + q1.transform(v1));
        System.out.println("m1.times(v1)      = " + m1.times(v1));
        EulerAngles ea1T = EulerAngles.valueOf(m1.transpose(), YAW_PITCH_ROLL);
        System.out.println("ea1 = " + ea1);
        System.out.println("ea1T = " + ea1T.to(NonSI.DEGREE_ANGLE));
        System.out.println("ea1.transpose() = " + ea1.transpose().to(NonSI.DEGREE_ANGLE));

        psi = Parameter.valueOf(20, NonSI.DEGREE_ANGLE);
        theta = Parameter.valueOf(-10, NonSI.DEGREE_ANGLE);
        phi = Parameter.valueOf(30, NonSI.DEGREE_ANGLE);
        System.out.println("\npsi = " + psi + ", theta = " + theta + ", phi = " + phi);
        DCMatrix m2 = DCMatrix.getEulerTM(psi, theta, phi);
        EulerAngles ea2 = EulerAngles.valueOf(m2, YAW_PITCH_ROLL);
        Quaternion q2 = Quaternion.valueOf(m2);
        EulerAngles ea3 = EulerAngles.valueOf(q2, YAW_PITCH_ROLL);
        System.out.println("m2         = \n" + m2);
        System.out.println("q2         = " + q2);
        System.out.println("ea2 (from m2) = " + ea2.to(NonSI.DEGREE_ANGLE));
        System.out.println("ea3 (from q2) = " + ea3.to(NonSI.DEGREE_ANGLE));

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
            //binding.setAlias(org.jscience.mathematics.number.Float64.class, "Float64");

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(ea1, "EulerAngles", EulerAngles.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
