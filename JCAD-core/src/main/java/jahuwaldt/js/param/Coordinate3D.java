/*
 *   Coordinate3D -- A three element vector of Parameter objects representing a 3D coordinate.
 *
 *   Copyright (C) 2008-2015, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Quantity;
import javolution.lang.ValueType;

/**
 * This class represents a 3 element vector of {@link Parameter} elements representing a
 * geometrical coordinate.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: November 21, 2008
 * @version November 4, 2015
 *
 * @param <Q> The Quantity (unit type, such as Length or Volume) of this parameter.
 */
public abstract class Coordinate3D<Q extends Quantity> extends
        AbstractParamVector<Q, Coordinate3D<Q>> implements ValueType {

    /**
     * Returns the number of elements held by this vector. This implementation always
     * returns a value of 3.
     *
     * @return this vector dimension.
     */
    @Override
    public int getDimension() {
        return 3;
    }

    /**
     * Compares this Coordinate3D against the specified Coordinate3D for approximate
     * equality (a Coordinate3D object with Vector3D values equal to this one to within
     * the numerical roundoff tolerance).
     *
     * @param obj the Coordinate3D object to compare with.
     * @return <code>true</code> if this Coordinate3D is approximately identical to that
     *         Coordinate3D; <code>false</code> otherwise.
     */
    public boolean isApproxEqual(Coordinate3D<?> obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;

        //  Check for approximate equality of Vector3D's.
        try {
            Vector3D<Q> thisV3D = this.toVector3D();
            Vector3D<Q> thatV3D = obj.to(this.getUnit()).toVector3D();
            for (int i = 0; i < 3; ++i) {
                double thisVal = thisV3D.getValue(i);
                double thatVal = thatV3D.getValue(i);
                double eps2 = MathTools.epsilon(thisVal) * 10;
                double eps = (eps2 > Parameter.EPS10 ? eps2 : Parameter.EPS10);
                if (!MathTools.isApproxEqual(thisVal, thatVal, eps))
                    return false;
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /**
     * Compares this Coordinate3D for approximate equality to zero (all the values are
     * within the numerical roundoff error of zero).
     *
     * @return <code>true</code> if this Coordinate3D is approximately equal to zero;
     *         <code>false</code> otherwise.
     */
    public boolean isApproxZero() {

        Vector3D<Q> thisV3D = this.toVector3D();
        for (int i = 0; i < 3; ++i) {
            double thisVal = thisV3D.getValue(i);
            if (MathTools.isApproxZero(thisVal))
                return false;
        }

        return true;
    }

    /**
     * Casts this Coordinate3D to a parameterized unit of specified nature or throw a
     * <code>ClassCastException</code> if the dimension of the specified quantity and this
     * parameter's unit dimension do not match.
     *
     * @param <R>  The Quantity (unit type) to cast this parameter as.
     * @param type the quantity class identifying the nature of the unit.
     * @return this AbstractParamVector parameterized with the specified type.
     * @throws ClassCastException if the dimension of this parameter's unit is different
     * from the specified quantity dimension.
     * @throws UnsupportedOperationException if the specified quantity class does not have
     * a public static field named "UNIT" holding the standard unit for the quantity.
     */
    public abstract <R extends Quantity> Coordinate3D<R> asType(Class<R> type) throws ClassCastException;

    /**
     * Returns this vector converted to a unit vector with a vector magnitude of 1.0.
     *
     * @return this vector converted to a unit vector
     */
    public abstract Coordinate3D<Dimensionless> toUnitVector();

}
