/*
*   Rotation -- Represents an attitude transformation between two different frames.
*
*   Copyright (C) 2009-2012 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.param;

import javax.measure.quantity.Quantity;

import javolution.lang.Realtime;
import javolution.lang.ValueType;


/**
* <p> This class represents a relative orientation (attitude or rotation transformation)
*     between two different reference frames; B wrt A or BA.  It can be used to
*     transform coordinates in reference frame A to reference frame B (A2B).</p>
* 	
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt   Date: October 22, 2009
*  @version December 12, 2013
**/
@SuppressWarnings("rawtypes")
public interface Rotation<T extends Rotation<?>> extends ValueType, Realtime {

	/**
	*  A Rotation object that represents no relative change in orientation.
	**/
	public static final Rotation IDENTITY = DCMatrix.IDENT;
	
	
	/**
	*  Transforms a 3D vector from frame A to B using this attitude transformation.
	*
	*  @param v the vector expressed in frame A.
	*  @return the vector expressed in frame B.
	**/
	public <Q extends Quantity> Vector3D<Q> transform(Coordinate3D<Q> v);
	
	/**
	*  Returns the spatial inverse of this transformation: AB rather than BA.
	*
	*  @return <code>this'</code>
	**/
	public T transpose();
	
	/**
	*  Returns the product of this attitude transformation and another.
	*  If this attitude transform is BA and that is AC then the returned
	*  value is:  BC = BA · AC (or C2B = A2B  · C2A).
	*
	*  @param that the attitude transform multiplier.
	*  @return <code>this · that</code>
	**/
	public T times(Rotation<?> that);
	
	/**
	*  Returns a direction cosine transformation matrix from this attitude transformation.
	*
	*  @return a direction cosine matrix that converts from frame A to B.
	*  @see <a href="http://en.wikipedia.org/wiki/Rotation_representation_(mathematics)">
	* 	Wikipedia: Rotation representation (mathematics)</a>
	**/
	public DCMatrix toDCM();
	
	/**
	*  Returns a quaternion representing this attitude transformation.
	*
	*  @return a quaternion that converts from frame A to B.
	*  @see <a href="http://en.wikipedia.org/wiki/Quaternion">>
	* 	Wikipedia: Quaternion</a>
	**/
	public Quaternion toQuaternion();
	
	/**
	*  Returns an independent copy of this attitude transform.
	**/
    @Override
	public T copy();
	
}