/*
*   AxisAngle -- An rotation axis and angle combination that represents the orientation between
*                 two reference frames.
*
*   Copyright (C) 2009-2014 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.param;

import javax.measure.quantity.*;
import javax.measure.unit.SI;

import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import javolution.xml.XMLSerializable;
import javolution.text.Text;
import javolution.text.TextBuilder;


/**
* <p> This class represents a rotation axis and rotation angle made up of Float64
*	  elements. AxisAngles may be used to represents a relative orientation
*     (attitude or rotation transformation) between two different reference
*     frames; B wrt A or BA.  It can be used to transform coordinates in reference frame A
*     to reference frame B (A2B).</p>
*
* <p> Reference: <a href="http://en.wikipedia.org/wiki/Axis_angle">
*			Wikipedia: Axis angle</a></p>
* 
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt   Date: October 22, 2009
*  @version February 27, 2014
**/
public final class AxisAngle extends AbstractRotation<AxisAngle> implements XMLSerializable {

	private static final long serialVersionUID = -8836950942718466904L;

	/**
	* The index to the vector X component of this axis angle rotation representation's axis.
	**/
	public static final int X = 0;
	
	/**
	*  The index to the vector Y component of this axis angle rotation representation's axis.
	**/
	public static final int Y = 1;
	
	/**
	*  The index to the vector Z component of this axis angle rotation representation's axis.
	**/
	public static final int Z = 2;
	
	/**
	*  The index to the angular component of this axis angle rotation representation.
	**/
	public static final int THETA = 3;
	
		
	/**
	*  The elements of the rotation axis.
	**/
	private Vector3D<Dimensionless> _axis;
	
	/**
	*  The rotation angle about the rotation axis.
	**/
	private Parameter<Angle> _theta;
	
	
	/**
 	* Returns a {@link AxisAngle} instance containing the specified rotation axis and 
	* rotation angle.
 	*
 	* @param axis The vector representing the rotation axis direction.
	* @param angle The rotation angle about the rotation axis.
 	* @return The axis/angle rotation transformation having the specified values.
 	*/
	public static AxisAngle valueOf(Vector3D<?> axis, Parameter<Angle> angle) {
		
		AxisAngle aa = AxisAngle.newInstance();
		aa._axis = axis.toUnitVector();
		aa._theta = angle;
		
		return aa;
	}
	
	/**
 	* Returns a new {@link AxisAngle} instance constructed from the specified rotation transform.
 	*
 	* @param transform The Rotation transform to convert to a new quaternion. 
 	* @return the quaternion representing the specified attitude transform.
 	*/
	public static AxisAngle valueOf(Rotation<?> transform) {
		AxisAngle aa;
		if (transform instanceof AxisAngle)
			aa = copyOf((AxisAngle)transform);
		else {
			Quaternion q = transform.toQuaternion();
			Vector3D<Dimensionless> axis = q.getVector().toUnitVector();
			double thetaV = 2*acos(q.getScalarValue());
			Parameter<Angle> theta = Parameter.valueOf(thetaV, SI.RADIAN);
			aa = AxisAngle.valueOf(axis, theta);
		}
		return aa;
	}
	

	/**
	*  Returns the rotation axis part of this axis/angle rotation transformation
	*  as a unit vector.
	**/
	public Vector3D<Dimensionless> getAxis() {
		return _axis;
	}
	
	/**
	*  Return the rotation angle.
	**/
	public Parameter<Angle> getAngle() {
		return _theta;
	}
	
	/**
	*  Returns the spatial inverse of this transformation: AB rather than BA.
	*
	*  @return <code>this' = this^*</code>
	**/
    @Override
    public AxisAngle transpose() {
        StackContext.enter();
        try {
            Quaternion q = this.toQuaternion();
            q = q.transpose();
            AxisAngle AA = AxisAngle.valueOf(q);
            AA._theta = AA._theta.to(_theta.getUnit());
            return StackContext.outerCopy(AA);
        } finally {
            StackContext.exit();
        }
    }

	/**
	*  Returns the product of this rotation transform with the specified rotation transform.
	*  If this axis/angle rotation is BA and that is AC then the returned
	*  value is:  BC = BA times AC (or C2B = A2B times C2A).
	*
	* @param  that the rotation transform multiplier.
	* @return <code>this times that</code>
	*/
    @Override
    public AxisAngle times(Rotation<?> that) {
        StackContext.enter();
        try {
            Quaternion q = this.toQuaternion();
            q = q.times(that);
            AxisAngle AA = AxisAngle.valueOf(q);
            AA._theta = AA._theta.to(_theta.getUnit());
            return StackContext.outerCopy(AA);
        } finally {
            StackContext.exit();
        }
	}
	
	/**
	* Returns the division of this rotation transform with the specified rotation transform.
	*
	* @param  that the rotation transform divisor.
	* @return <code>this / that</code>
	*/
    public AxisAngle divide(Rotation<?> that) {
        StackContext.enter();
        try {
            Quaternion q = this.toQuaternion();
            q = q.divide(that);
            AxisAngle AA = AxisAngle.valueOf(q);
            AA._theta = AA._theta.to(_theta.getUnit());
            return StackContext.outerCopy(AA);
        } finally {
            StackContext.exit();
        }
    }

	/**
	*  Transforms a 3D vector from frame A to B using this axis/angle rotation.
	*
	*  @param v the vector expressed in frame A.
	*  @return the vector expressed in frame B.
	**/
    @Override
	public <Q extends Quantity> Vector3D<Q> transform(Coordinate3D<Q> v) {
		Quaternion q = this.toQuaternion();
		return q.transform(v);
	}
	
	/**
	*  Returns a direction cosine transformation matrix from this axis/angle rotation.
	*
	*  @return a direction cosine matrix that converts from frame A to B.
	*  @see <a href="http://en.wikipedia.org/wiki/Rotation_matrix#AxisAngle">
	* 	Wikipedia: Rotation Matrix-AxisAngle</a>
	**/
    @Override
	public DCMatrix toDCM() {
		Quaternion q = toQuaternion();
		return q.toDCM();
	}
	
	/**
	*  Returns a quaternion representing this rotation transformation.
	*
	*  @return a quaternion that converts from frame A to B.
	*  @see <a href="http://en.wikipedia.org/wiki/AxisAngle">>
	* 	Wikipedia: AxisAngle</a>
	**/
    @Override
	public Quaternion toQuaternion() {
		return Quaternion.valueOf(_axis, _theta);
	}
	
	/**
	* Returns a copy of this rotation transform 
	* {@link javolution.context.AllocatorContext allocated} 
	* by the calling thread (possibly on the stack).
	*	
	* @return an identical and independent copy of this rotation transform.
	*/
    @Override
	public AxisAngle copy() {
		return copyOf(this);
	}
	
	/**
	* Returns the text representation of this rotation transform.
	*
	* @return the text representation of this rotation transform.
	*/
    @Override
	public Text toText() {
		final int dimension = 3;
		TextBuilder tmp = TextBuilder.newInstance();
		if (this.isApproxEqual(IDENTITY))
			tmp.append("{IDENTITY}");
		else {
			tmp.append("{ axis = {");
			for (int i = 0; i < dimension; i++) {
				tmp.append(_axis.get(i));
				if (i != dimension - 1)
					tmp.append(", ");
			}
			tmp.append("}, theta = ");
			tmp.append(_theta);
			tmp.append('}');
		}
		Text txt = tmp.toText();
		TextBuilder.recycle(tmp); 
		return txt;
	}

	/**
 	* Compares this AxisAngle against the specified object for strict 
 	* equality (same rotation type and same values).
	*
 	* @param  obj the object to compare with.
 	* @return <code>true</code> if this rotation is identical to that
 	* 		rotation; <code>false</code> otherwise.
	**/
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		
		AxisAngle that = (AxisAngle)obj;
        if (!this._theta.equals(that._theta))
            return false;
        
		return this._axis.equals(that._axis);
	}

	/**
 	* Returns the hash code for this rotation.
 	* 
 	* @return the hash code value.
 	*/
    @Override
	public int hashCode() {
		int hash = 7;
		
		int var_code = _theta.hashCode();
		hash = hash*31 + var_code;
		
		var_code = _axis.hashCode();
		hash = hash*31 + var_code;
		
		return hash;
	}

	
	/**
 	* Holds the default XML representation. For example:
 	* <pre>
 	*	&lt;AxisAngle&gt;
	*		&lt;Axis unit="Dimensionless"&gt;
 	*			&lt;X value="1.0" /&gt;
 	*			&lt;Y value="0.0" /&gt;
 	*			&lt;Z value="2.0" /&gt;
	*			&lt;W value="1.0" /&gt;
	*		&lt;/Axis&gt;
	*		&lt;Angle value="10" unit="rad"/&gt;
  	*	&lt;/AxisAngle&gt;
	* </pre>
 	*/
	protected static final XMLFormat<AxisAngle> XML = new XMLFormat<AxisAngle>(AxisAngle.class) {

		@Override
		public AxisAngle newInstance(Class<AxisAngle> cls, InputElement xml) throws XMLStreamException {
			return FACTORY.object();
		}

		@Override
		public void read(InputElement xml, AxisAngle aa) throws XMLStreamException {
			
			Vector3D<?> axis = xml.get("Axis", Vector3D.class);
			Parameter<Angle> angle = xml.get("Angle", Parameter.class);
			if (!angle.getUnit().isCompatible(SI.RADIAN))
				throw new XMLStreamException( RESOURCES.getString("aaBadUnits") );
			
			aa._axis = axis.toUnitVector();
			aa._theta = angle;
			
			if (xml.hasNext()) 
				throw new XMLStreamException( RESOURCES.getString("toManyXMLElementsErr") );
		}

		@Override
		public void write(AxisAngle aa, OutputElement xml) throws XMLStreamException {
				
			xml.add(aa._axis, "Axis", Vector3D.class);
			xml.add(aa._theta, "Angle", Parameter.class);
			
		}
	};
	
	
	///////////////////////
	// Factory creation. //
	///////////////////////

	private static final ObjectFactory<AxisAngle> FACTORY = new ObjectFactory<AxisAngle>() {
		@Override
		protected AxisAngle create() {
			return new AxisAngle();
		}
	};

	private static AxisAngle newInstance() {
		AxisAngle o = FACTORY.object();
		return o;
	}

	private static AxisAngle copyOf(AxisAngle original) {
		AxisAngle o = AxisAngle.newInstance();
		o._axis = original._axis.copy();
		o._theta = original._theta.copy();
		return o;
	}

	private AxisAngle() {}
	

}