/*
*   ParameterFormat -- Provides formatting and parsing for Parameter objects.
*
*   Copyright (C) 2008-2011 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.param;

import java.io.IOException;
import java.text.ParseException;

import org.jscience.economics.money.Currency;
import org.jscience.economics.money.Money;

import javax.measure.unit.Unit;
import javax.measure.unit.UnitFormat;

import javolution.lang.MathLib;
import javolution.text.TextFormat;
import javolution.text.TypeFormat;
import javolution.context.LocalContext;


/**
* <p> This class provides the interface for formatting and parsing {@link 
* 	Parameter parameter} instances.</p>
* 	
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt   Date: November 15, 2008
*  @version October 2, 2011
**/
public abstract class ParameterFormat extends TextFormat<Parameter<?>> {

	/**
 	* Holds current format.
 	*/
	private static final LocalContext.Reference<ParameterFormat> CURRENT = new LocalContext.Reference<ParameterFormat>(
			new SimpleFormat());

	/**
 	* Default constructor.
 	*/
	protected ParameterFormat() { }

	/**
 	* Returns the current {@link javolution.context.LocalContext local}  
 	* format (default <code>ParameterFormat.getSimpleFormatInstance()</code>).
 	*
 	* @return the context local format.
 	* @see #getSimpleFormatInstance
 	*/
	public static ParameterFormat newInstance() {
		return CURRENT.get();
	}

	/**
 	* Sets the current {@link javolution.context.LocalContext local} format.
 	*
 	* @param format the new format.
 	*/
	public static void setInstance(ParameterFormat format) {
		CURRENT.set(format);
	}

	/**
 	* Returns a simple ParameterFormat for which the value is stated using 
 	* the current units; for example <code>"1.34 m"</code>.
 	* This format can be used for formatting as well as for parsing.
 	*/
	public static ParameterFormat getSimpleFormatInstance() {
		return new SimpleFormat();
	}

	/**
 	* This class represents the simple format with the value  and units.
 	*/
	private static class SimpleFormat extends ParameterFormat {

		/**
 		* Default constructor.
 		*/
		private SimpleFormat() { }

		@SuppressWarnings("unchecked")
		@Override
		public Appendable format(Parameter<?> arg0, Appendable arg1)
				throws IOException {
			if (arg0.getUnit() instanceof Currency)
				return formatMoney((Parameter<Money>) arg0, arg1);
			double value = arg0.getValue();
			double error = MathLib.abs(value) * DOUBLE_RELATIVE_ERROR;
			int log10Value = (int) MathLib.floor(MathLib.log10(MathLib
					.abs(value)));
			int log10Error = (int) MathLib.floor(MathLib.log10(error));
			int digits = log10Value - log10Error - 1; // Exact digits.

			boolean scientific = (MathLib.abs(value) >= 1E6) || (MathLib.abs(value) < 1E-6);
			boolean showZeros = false;
			TypeFormat.format(value, digits, scientific, showZeros, arg1);
			arg1.append(' ');
			return UnitFormat.getInstance().format(arg0.getUnit(), arg1);
		}

		@Override
		public Parameter<?> parse(CharSequence arg0, Cursor arg1) {
			int start = arg1.getIndex();
			try {
				double amount = TypeFormat.parseDouble(arg0, arg1);
				arg1.skip(' ', arg0);
				Unit<?> unit = UnitFormat.getInstance().parseProductUnit(arg0, arg1);
				return Parameter.valueOf(amount, unit);
				
			} catch (ParseException e) {
				arg1.setIndex(start);
				arg1.setErrorIndex(e.getErrorOffset());
				return null;
			}
		}
	}

	/**
 	* Provides custom formatting for money measurements.
 	*/
	private static Appendable formatMoney(Parameter<Money> arg0, Appendable arg1)
			throws IOException {
		Currency currency = (Currency) arg0.getUnit();
		int fraction = currency.getDefaultFractionDigits();
		if (fraction == 0) {
			long amount = arg0.longValue(currency);
			TypeFormat.format(amount, arg1);
		} else if (fraction == 2) {
			long amount = MathLib.round(arg0.doubleValue(arg0.getUnit()) * 100);
			TypeFormat.format(amount / 100, arg1);
			arg1.append('.');
			arg1.append((char) ('0' + (amount % 100) / 10));
			arg1.append((char) ('0' + (amount % 10)));
		} else {
			throw new UnsupportedOperationException();
		}
		arg1.append(' ');
		return UnitFormat.getInstance().format(currency, arg1);
	}

	static final double DOUBLE_RELATIVE_ERROR = MathLib.pow(2, -53);
	
}