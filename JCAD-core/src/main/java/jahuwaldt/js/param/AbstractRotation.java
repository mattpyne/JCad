/*
*   AbstractRotation -- Partial implementation of a Rotation.
*
*   Copyright (C) 2009-2014 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import java.util.ResourceBundle;
import javax.measure.Measurable;
import javax.measure.unit.SI;
import javax.measure.quantity.*;
import static javolution.lang.MathLib.*;
import org.jscience.mathematics.vector.Float64Vector;


/**
* <p> This class represents a partial implementation of a Rotation object.</p>
* 	
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt   Date: October 22, 2009
*  @version March 1, 2014
**/
public abstract class AbstractRotation<T extends AbstractRotation<?>> implements Rotation<T> {
	
	/**
	*  The resource bundle for this package.
	**/
	protected static final ResourceBundle RESOURCES = Parameter.RESOURCES;
	
	/**
	*  Returns the direction cosine T.M. of the aerospace psi->theta Euler sequence.
	*
	*  @param psi    First rotation angle.
	*  @param theta  Second rotation angle.
	*  @return the 3x3 transformation matrix
	**/
	public static DCMatrix getPsiThetaTM(Measurable<Angle> psi, Measurable<Angle> theta) {
		double psiR = psi.doubleValue(SI.RADIAN);
		double thetaR = theta.doubleValue(SI.RADIAN);
		
		double cpsi = cos(psiR);
		double spsi = sin(psiR);
		double ctheta = cos(thetaR);
		double stheta = sin(thetaR);
		
		Float64Vector row0 = Float64Vector.valueOf( (ctheta * cpsi), -spsi, (stheta * cpsi) );
		Float64Vector row1 = Float64Vector.valueOf( (ctheta * spsi),  cpsi, (stheta * spsi) );
		Float64Vector row2 = Float64Vector.valueOf(         -stheta,     0,  ctheta);
		
		return DCMatrix.valueOf(row0, row1, row2);
	}

	/**
	*  Returns the direction cosine T.M. of the aerospace psi->theta Euler sequence from a polar coordinate.
	*
	*  @param coord  A polar coordinate (magnitude, psi=azimuth, theta=elevation).
	*  @return the 3x3 transformation matrix
	**/
	public static DCMatrix getPsiThetaTM(Polar3D<?> coord) {
		return getPsiThetaTM(coord.getAzimuth(), coord.getElevation());
	}
	
	
	/**
	*  Returns the direction cosine T.M. of the aerospace Euler 3-2-1 (psi->theta-phi) rotation sequence.
	*  The Euler angle transformation matrix of flight mechanics.
	*
	*  @param psi    First rotation angle.
	*  @param theta  Second rotation angle.
	*  @param phi    Third rotation angle.
	*  @return the 3x3 transformation matrix
	**/
	public static DCMatrix getEulerTM(Measurable<Angle> psi, Measurable<Angle> theta, Measurable<Angle> phi) {
		double psiR = psi.doubleValue(SI.RADIAN);
		double thetaR = theta.doubleValue(SI.RADIAN);
		double phiR = phi.doubleValue(SI.RADIAN);
	
		double cpsi = cos(psiR);
		double spsi = sin(psiR);
		double ctheta = cos(thetaR);
		double stheta = sin(thetaR);
		double cphi = cos(phiR);
		double sphi = sin(phiR);
		
		Float64Vector row0 = Float64Vector.valueOf( (cpsi*ctheta), (cpsi*stheta*sphi - spsi*cphi), (cpsi*stheta*cphi + spsi*sphi) );
		Float64Vector row1 = Float64Vector.valueOf( (spsi*ctheta), (spsi*stheta*sphi + cpsi*cphi), (spsi*stheta*cphi - cpsi*sphi) );
		Float64Vector row2 = Float64Vector.valueOf(       -stheta,                  (ctheta*sphi),                  (ctheta*cphi) );
		
		return DCMatrix.valueOf(row0, row1, row2);
	}

	/**
	* Returns the text representation of this rotation transform as a 
	* <code>java.lang.String</code>.
	* 
	* @return <code>toText().toString()</code>
	**/
    @Override
	public final String toString() {
		return toText().toString();
	}
	
	/**
 	* Compares this Rotation against the specified Rotation for approximate 
 	* equality (a Rotation object with DCM values equal to this one to within the 
	* numerical roundoff tolerance).
	*
 	* @param  obj the Rotation object to compare with.
 	* @return <code>true</code> if this Rotation is approximately identical to that
 	* 		Rotation; <code>false</code> otherwise.
	**/
	public boolean isApproxEqual(Rotation<?> obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		//	Check for exact equality.
		if (this.equals(obj))
			return true;
		
		//	Check for approximate equality of DCM's.
		DCMatrix thisDCM = this.toDCM();
		DCMatrix thatDCM = obj.toDCM();
		for (int i=0; i < 3; ++i) {
			for (int j=0; j < 3; ++j) {
				double thisVal = thisDCM.getValue(i,j);
				double thatVal = thatDCM.getValue(i,j);
				if (!MathTools.isApproxEqual(thisVal, thatVal, Parameter.EPS10))
					return false;
			}
		}
		
		return true;
	}

}