/**
 * Vector3D -- A three element vector of Parameter objects that may represent a Cartesian
 * coordinate.
 *
 * Copyright (C) 2008-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import javax.measure.converter.ConversionException;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.*;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ImmortalContext;
import javolution.context.ObjectFactory;
import javolution.lang.MathLib;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.*;

/**
 * This class represents a 3 element {@link Vector vector} of Parameter elements sharing
 * the same units. In geometrical terms, this vector represents a set of 3 Cartesian
 * coordinates.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: November 15, 2008
 * @version October 30, 2015
 *
 * @param <Q> The Quantity (unit type, such as Length or Volume) of this vector.
 */
public final class Vector3D<Q extends Quantity> extends Coordinate3D<Q> implements XMLSerializable {

    private static final long serialVersionUID = -306965157071885706L;

    /**
     * Constant used to identify the X coordinate in the vector.
     */
    public static final int X = 0;

    /**
     * Constant used to identify the Y coordinate in the vector.
     */
    public static final int Y = 1;

    /**
     * Constant used to identify the Z coordinate in the vector.
     */
    public static final int Z = 2;

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private Vector3D() {
    }

    private static final ObjectFactory<Vector3D> FACTORY = new ObjectFactory<Vector3D>() {
        @Override
        protected Vector3D create() {
            return new Vector3D();
        }
    };

    private static <Q extends Quantity> Vector3D<Q> newInstance(Unit<Q> unit) {
        if (unit == null)
            throw new NullPointerException("Unit can not be null.");
        Vector3D<Q> measure = FACTORY.object();
        measure._unit = unit;
        return measure;
    }

    private static <Q extends Quantity> Vector3D<Q> copyOf(Vector3D<Q> original) {
        Vector3D<Q> measure = Vector3D.newInstance(original._unit);
        measure._data = original._data.copy();
        return measure;
    }

    /**
     * A Float64Vector with all the elements set to zero.
     */
    private static final Float64Vector ZERO_F64VECTOR;

    /**
     * A dimensionless vector with all the elements set to zero.
     */
    public static final Vector3D<Dimensionless> ZERO;

    /**
     * A position or length vector with all the elements set to zero.
     */
    public static final Vector3D<Length> ZERO_POSITION;

    /**
     * A velocity vector with all the elements set to zero.
     */
    public static final Vector3D<Velocity> ZERO_VELOCITY;

    /**
     * An acceleration vector with all the elements set to zero.
     */
    public static final Vector3D<Acceleration> ZERO_ACCELERATION;

    /**
     * A force vector with all the elements set to zero.
     */
    public static final Vector3D<Force> ZERO_FORCE;

    /**
     * A angle vector with all the elements set to zero.
     */
    public static final Vector3D<Angle> ZERO_ANGLE;

    /**
     * A unit vector in the X axis direction.
     */
    public static final Vector3D<Dimensionless> UNIT_X;

    /**
     * A unit vector in the Y axis direction.
     */
    public static final Vector3D<Dimensionless> UNIT_Y;

    /**
     * A unit vector in the Z axis direction.
     */
    public static final Vector3D<Dimensionless> UNIT_Z;

    static {
        // Forces constants to ImmortalMemory.
        ImmortalContext.enter();
        try {
            ZERO_F64VECTOR = Float64Vector.valueOf(0, 0, 0);

            ZERO = Vector3D.newInstance(Dimensionless.UNIT);
            ZERO._data = ZERO_F64VECTOR;

            ZERO_POSITION = Vector3D.newInstance(Length.UNIT);
            ZERO_POSITION._data = ZERO_F64VECTOR;

            ZERO_VELOCITY = Vector3D.newInstance(Velocity.UNIT);
            ZERO_VELOCITY._data = ZERO_F64VECTOR;

            ZERO_ACCELERATION = Vector3D.newInstance(Acceleration.UNIT);
            ZERO_ACCELERATION._data = ZERO_F64VECTOR;

            ZERO_FORCE = Vector3D.newInstance(Force.UNIT);
            ZERO_FORCE._data = ZERO_F64VECTOR;

            ZERO_ANGLE = Vector3D.newInstance(Angle.UNIT);
            ZERO_ANGLE._data = ZERO_F64VECTOR;

            UNIT_X = Vector3D.newInstance(Dimensionless.UNIT);
            UNIT_X._data = Float64Vector.valueOf(1, 0, 0);

            UNIT_Y = Vector3D.newInstance(Dimensionless.UNIT);
            UNIT_Y._data = Float64Vector.valueOf(0, 1, 0);

            UNIT_Z = Vector3D.newInstance(Dimensionless.UNIT);
            UNIT_Z._data = Float64Vector.valueOf(0, 0, 1);

        } finally {
            ImmortalContext.exit();
        }
    }

    /**
     * The coordinates are stored in this vector. Serialization is handled by this class
     * since Float64Vector is not Serializable.
     */
    private transient Float64Vector _data;

    /**
     * Holds the unit of the coordinates.
     */
    private Unit<Q> _unit;

    /**
     * Returns a {@link Vector3D} instance holding the specified <code>double</code>
     * values stated in the specified units.
     *
     * @param <Q>  the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param x    the x value stated in the specified unit.
     * @param y    the y value stated in the specified unit.
     * @param z    the z value stated in the specified unit.
     * @param unit the unit in which the coordinates are stated.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector3D<Q> valueOf(double x, double y, double z, Unit<Q> unit) {
        Vector3D<Q> V = Vector3D.newInstance(unit);
        V._data = Float64Vector.valueOf(x, y, z);

        return V;
    }

    /**
     * Returns a {@link Vector3D} instance holding the specified <code>Parameter</code>
     * values. All the values are converted to the same units as the x value.
     *
     * @param <Q> the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param x   the x value.
     * @param y   the y value.
     * @param z   the z value.
     * @return the vector having the specified values in the units of x.
     */
    public static <Q extends Quantity> Vector3D<Q> valueOf(Parameter<Q> x, Parameter<Q> y, Parameter<Q> z) {

        Unit<Q> unit = x.getUnit();
        Vector3D<Q> V = Vector3D.newInstance(unit);
        V._data = Float64Vector.valueOf(x.getValue(), y.getValue(unit), z.getValue(unit));

        return V;
    }

    /**
     * Returns a {@link Vector3D} instance containing the specified vector of Float64
     * values stated in the specified units. The vector must have only 3 elements.
     *
     * @param <Q>    the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param vector the vector of Float64 values stated in the specified unit (must have
     *               dimension of 3).
     * @param unit   the unit in which the coordinates are stated.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector3D<Q> valueOf(Vector<Float64> vector, Unit<Q> unit) {
        if (vector.getDimension() != 3)
            throw new DimensionException(RESOURCES.getString("v3dBadDimErr"));

        Vector3D<Q> V = Vector3D.newInstance(unit);
        V._data = Float64Vector.valueOf(vector);

        return V;
    }

    /**
     * Returns a {@link Vector3D} instance containing the specified vector of Parameter
     * values with compatible units. The vector must have only 3 elements. All the values
     * are converted to the same units as the 1st value.
     *
     * @param <Q>    the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param vector the vector of Parameter values (must have dimension of 3).
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Vector3D<Q> valueOf(Vector<Parameter<Q>> vector) {
        if (vector instanceof Vector3D)
            return (Vector3D<Q>)vector;

        if (vector.getDimension() != 3)
            throw new DimensionException(RESOURCES.getString("v3dBadDimErr"));

        return Vector3D.valueOf(vector.get(X), vector.get(Y), vector.get(Z));
    }

    /**
     * Return the specified {@link Vector3D} object as a <code>Vector3D</code> instance.
     *
     * @param vector The <code>Vector3D</code> object to be converted to a
     *               <code>Vector3D</code>.
     * @return The input object is simply returned.
     */
    @Override
    public Vector3D<Q> fromVector3D(Vector3D<Q> vector) {
        return vector;
    }

    /**
     * Returns the value of a Parameter from this vector.
     *
     * @param i the dimension index.
     * @return the value of the parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i > dimension()-1)</code>
     */
    @Override
    public Parameter<Q> get(int i) {
        return Parameter.valueOf(getValue(i), getUnit());
    }

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code>, stated
     * in this vector's {@link #getUnit unit}.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i > dimension()-1)</code>
     */
    @Override
    public double getValue(int i) {
        return _data.getValue(i);
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector.
     *
     * @return <code>this.norm().doubleValue()</code>.
     */
    @Override
    public double normValue() {
        return _data.normValue();
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    @Override
    public Vector3D<Q> opposite() {
        Vector3D<Q> V = Vector3D.newInstance(this._unit);
        V._data = this._data.opposite();
        return V;
    }

    /**
     * Returns the sum of this vector with the one specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Vector3D<Q> plus(Vector<Parameter<Q>> that) {

        //  Convert input vector to a Float64Vector (with unit conversion if necessary).
        Float64Vector thatData = toFloat64Vector(that, this._unit);

        Vector3D<Q> V = Vector3D.newInstance(this._unit);
        V._data = this._data.plus(thatData);

        return V;
    }

    /**
     * Returns the sum of this vector with the parameter specified. The input parameter is
     * added to each component of this vector. The unit of the output vector will be the
     * units of this vector.
     *
     * @param that the parameter to be added to each element of this vector.
     * @return <code>this + that</code>.
     */
    @Override
    public Vector3D<Q> plus(Parameter<Q> that) {
        Vector3D<Q> V = Vector3D.newInstance(this._unit);

        //  Convert input parameter to the units of this vector.
        double thatValue = that.getValue(this._unit);
        double x = this._data.getValue(X) + thatValue;
        double y = this._data.getValue(Y) + thatValue;
        double z = this._data.getValue(Z) + thatValue;
        V._data = Float64Vector.valueOf(x, y, z);

        return V;
    }

    /**
     * Returns the difference between this vector and the one specified. The unit of the
     * output vector will be the units of this vector.
     *
     * @param that the vector to be subtracted.
     * @return <code>this - that</code>.
     */
    @Override
    public Vector3D<Q> minus(Vector<Parameter<Q>> that) {

        //  Convert input vector to a Float64Vector (with unit conversion if necessary).
        Float64Vector thatData = toFloat64Vector(that, this._unit);

        Vector3D<Q> V = Vector3D.newInstance(this._unit);
        V._data = this._data.minus(thatData);

        return V;
    }

    /**
     * Subtracts the supplied Parameter from each element of this vector and returns the
     * result. The unit of the output vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from each element of this vector.
     * @return <code>this - that</code>.
     */
    @Override
    public Vector3D<Q> minus(Parameter<Q> that) {
        Vector3D<Q> V = Vector3D.newInstance(this._unit);

        //  Convert input parameter to the units of this vector.
        double thatValue = that.getValue(this._unit);
        double x = this._data.getValue(X) - thatValue;
        double y = this._data.getValue(Y) - thatValue;
        double z = this._data.getValue(Z) - thatValue;
        V._data = Float64Vector.valueOf(x, y, z);

        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Vector3D times(Parameter k) {

        Unit unit = Parameter.productOf(this.getUnit(), k.getUnit());
        Vector3D<?> V = Vector3D.newInstance(unit);
        V._data = this._data.times(k.getValue());

        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Vector3D<Q> times(double k) {
        Vector3D<Q> V = Vector3D.newInstance(this._unit);
        V._data = _data.times(k);
        return V;
    }

    /**
     * Returns the dot product of this vector with the one specified.
     *
     * @param that the vector multiplier.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    @Override
    public Parameter times(Vector that) {

        //  Convert input vector to a Float64Vector.
        Float64Vector thatData = toFloat64Vector(that, null);

        Float64 f = this._data.times(thatData);
        Unit<?> unit = Parameter.productOf(this.getUnit(), ((Parameter)that.get(X)).getUnit());

        return Parameter.valueOf(f.doubleValue(), unit);
    }

    /**
     * Returns the element-by-element product of this vector with the one specified.
     *
     * @param that the vector multiplier.
     * @return <code>this .* that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     */
    public Vector3D timesEBE(Vector that) {

        //  Convert input vector to a Float64Vector.
        Float64Vector thatData = toFloat64Vector(that, null);

        Unit<?> unit = Parameter.productOf(this.getUnit(), ((Parameter)that.get(X)).getUnit());

        //  Carry out the multiplication.
        double x = this._data.getValue(X) * thatData.getValue(X);
        double y = this._data.getValue(Y) * thatData.getValue(Y);
        double z = this._data.getValue(Z) * thatData.getValue(Z);

        return Vector3D.valueOf(x, y, z, unit);
    }

    /**
     * Returns this vector with each element divided by the specified divisor
     * (dimensionless).
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    @Override
    public Vector3D<Q> divide(double divisor) {
        return times(1. / divisor);
    }

    /**
     * Returns the cross product of two 3-dimensional vectors.
     *
     * @param that the vector multiplier.
     * @return <code>this x that</code>
     * @throws DimensionException if <code>(that.getDimension() != 3)</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    @Override
    public Vector3D cross(Vector that) {

        //  Convert input vector to a Float64Vector.
        Float64Vector thatData = toFloat64Vector(that, null);

        Unit unit = Parameter.productOf(this.getUnit(), ((Parameter)that.get(X)).getUnit());

        Vector3D V = Vector3D.newInstance(unit);
        V._data = this._data.cross(thatData);

        return V;
    }

    /**
     * Returns this vector with each element divided by the specified divisor.
     *
     * @param that the divisor.
     * @return <code>this / that</code>.
     */
    public Vector3D<?> divide(Parameter<?> that) {
        return (Vector3D<?>)times(that.inverse());
    }

    /**
     * Returns this vector converted to a unit vector by dividing all the vector's
     * elements by the length ({@link #norm}) of this vector.
     *
     * @return this vector converted to a unit vector
     */
    public Vector3D<Dimensionless> toUnitVector() {
        double magnitude = this.normValue();
        if (this.getUnit().equals(Dimensionless.UNIT) && MathLib.abs(magnitude - 1) <= Parameter.EPS)
            return (Vector3D<Dimensionless>)this;

        Vector3D<Dimensionless> V = Vector3D.newInstance(Dimensionless.UNIT);
        V._data = this._data.times(1.0 / magnitude);
        return V;
    }

    /**
     * Returns a copy of this vector {@link javolution.context.AllocatorContext allocated}
     * by the calling thread (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public Vector3D<Q> copy() {
        return copyOf(this);
    }

    /**
     * Returns the unit in which the {@link #getValue values} in this vector are stated
     * in.
     *
     * @return the unit in which the values in this vector are stated
     */
    @Override
    public Unit<Q> getUnit() {
        return _unit;
    }

    /**
     * Returns the equivalent to this vector but stated in the specified unit.
     *
     * @param <R>  the Quantity (unit type, e.g. Length or Volume) of the returned vector.
     * @param unit the unit of the vector to be returned.
     * @return a vector equivalent to this vector but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public <R extends Quantity> Vector3D<R> to(Unit<R> unit) throws ConversionException {
        if ((_unit == unit) || this._unit.equals(unit))
            return (Vector3D<R>)this;

        UnitConverter cvtr = Parameter.converterOf(_unit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            Vector3D<R> result = (Vector3D<R>)Vector3D.copyOf(this);
            result._unit = unit;
            return result;
        }
        Vector3D<R> result = Vector3D.newInstance(unit);
        double x = cvtr.convert(_data.getValue(X));
        double y = cvtr.convert(_data.getValue(Y));
        double z = cvtr.convert(_data.getValue(Z));
        result._data = Float64Vector.valueOf(x, y, z);

        return result;
    }

    /**
     * Casts this Vector3D to a parameterized unit of specified nature or throw a
     * <code>ClassCastException</code> if the dimension of the specified quantity and this
     * parameter's unit dimension do not match.
     *
     * @param <T>  the Quantity (unit type, e.g. Length or Volume) of the returned vector.
     * @param type the quantity class identifying the nature of the unit.
     * @return this Vector3D parameterized with the specified type.
     * @throws ClassCastException if the dimension of this parameter's unit is different
     * from the specified quantity dimension.
     * @throws UnsupportedOperationException if the specified quantity class does not have
     * a public static field named "UNIT" holding the standard unit for the quantity.
     */
    @Override
    public <T extends Quantity> Vector3D<T> asType(Class<T> type) throws ClassCastException {
        Unit<T> u = _unit.asType(type); //  If no exception is thrown, the cast is valid.
        return (Vector3D<T>)this;
    }

    /**
     * Returns a Cartesian Vector3D representation of this vector.
     *
     * @return a Cartesian Vector3D representation of this vector
     */
    @Override
    public Vector3D<Q> toVector3D() {
        return this;
    }

    /**
     * Returns double the values stored in this vector, stated in this vector's
     * {@link #getUnit unit}, as a Float64Vector.
     *
     * @return the values stored in this vector as a Float64Vector
     */
    public Float64Vector toFloat64Vector() {
        return _data;
    }

    /**
     * Returns a 3x3 skew-symmetric matrix representation of the values in this vector
     * stated in this vector's {@link #getUnit unit}.
     * <p>
     * A skew symmetric representation of a vector is defined by:
     * <pre>
     *       | 0 -c  b|      |a|
     *       | c  0 -a| &lt;--  |b|
     *       |-b  a  0|      |c|
     * </pre></p>
     *
     * @return a 3x3 skew-symmetric matrix representation of the values in this vector
     */
    public Matrix3D<Q> toSkewSymmetric() {
        return makeSkewSymmetric(_data.getValue(X), _data.getValue(Y), _data.getValue(Z), _unit);
    }

    /**
     * Returns a 3x3 skew-symmetric matrix representation of the values in the input 3D
     * vector stated in the specified units.
     * <p>
     * A skew symmetric representation of a vector is defined by:
     * <pre>
     *       | 0 -c  b|      |a|
     *       | c  0 -a| &lt;--  |b|
     *       |-b  a  0|      |c|
     * </pre></p>
     *
     * <p>
     * This is a more efficient convenience method equivalent to:
     * <code>Vector3D.valueOf(a, b, c, Dimensionless.UNIT).toSkewSymmetric();</code></p>
     *
     * @param <R>  the Quantity (unit type, e.g. Length or Volume) of the returned matrix.
     * @param a    The 1st element of the 3D vector
     * @param b    The 2nd element of the 3D vector
     * @param c    The 3rd element of the 3D vector
     * @param unit The unit that the elements are stated in.
     * @return a 3x3 skew-symmetric matrix representation of the values in the input 3D
     *         vector.
     */
    public static <R extends Quantity> Matrix3D<R> makeSkewSymmetric(double a, double b, double c, Unit<R> unit) {
        Float64Vector row1 = Float64Vector.valueOf(0, -c, b);
        Float64Vector row2 = Float64Vector.valueOf(c, 0, -a);
        Float64Vector row3 = Float64Vector.valueOf(-b, a, 0);

        return Matrix3D.valueOf(row1, row2, row3, unit);
    }

    /**
     * Compares this Vector3D against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this vector is identical to that vector;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        Vector3D<?> that = (Vector3D<?>)obj;
        if (!this._data.equals(that._data))
            return false;

        return this._unit.equals(that._unit);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _unit.hashCode();
        hash = hash * 31 + var_code;

        var_code = _data.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    /**
     * Convert a vector of Parameter objects to a Float64Vector stated in the specified
     * units. If the units are null, no conversion occurs.
     */
    private <Q extends Quantity> Float64Vector toFloat64Vector(Vector<Parameter<Q>> that, Unit<Q> unit) {

        //  Make sure input is a Vector3D instance.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        Vector3D<Q> T = ((Coordinate3D<Q>)that).toVector3D();                       //  Convert to a Vector3D.

        //  Convert that vector's units to the specified units if necessary.
        Float64Vector thatData = T._data;
        if (unit != null) {
            if ((unit != T._unit) && !unit.equals(T._unit)) {
                UnitConverter cvtr = Parameter.converterOf(T._unit, unit);
                if (cvtr != UnitConverter.IDENTITY) {
                    double x = cvtr.convert(thatData.getValue(X));
                    double y = cvtr.convert(thatData.getValue(Y));
                    double z = cvtr.convert(thatData.getValue(Z));
                    thatData = Float64Vector.valueOf(x, y, z);
                }
            }
        }

        return thatData;
    }

    /**
     * During serialization, this will write out the Float64Vector as a simple series of
     * <code>double</code> values. This method is ONLY called by the Java Serialization
     * mechanism and should not otherwise be used.
     */
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {

        // Call the default write object method.
        out.defaultWriteObject();

        //  Write out the three coordinate values.
        out.writeDouble(_data.getValue(X));
        out.writeDouble(_data.getValue(Y));
        out.writeDouble(_data.getValue(Z));

    }

    /**
     * During de-serialization, this will handle the reconstruction of the Float64Vector.
     * This method is ONLY called by the Java Serialization mechanism and should not
     * otherwise be used.
     */
    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {

        // Call the default read object method.
        in.defaultReadObject();

        //  Read in the three coordinate values.
        double x = in.readDouble();
        double y = in.readDouble();
        double z = in.readDouble();

        _data = Float64Vector.valueOf(x, y, z);

    }

    /**
     * Holds the default XML representation. For example:
     * <pre>
     *   &lt;Vector3D unit = "m"&gt;
     *       &lt;X value="1.0" /&gt;
     *       &lt;Y value="0.0" /&gt;
     *       &lt;Z value="2.0" /&gt;
     *   &lt;/Vector3D&gt;
     * </pre>
     */
    protected static final XMLFormat<Vector3D> XML = new XMLFormat<Vector3D>(Vector3D.class) {

        @Override
        public Vector3D<?> newInstance(Class<Vector3D> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, Vector3D V) throws XMLStreamException {

            V._unit = Unit.valueOf(xml.getAttribute("unit"));
            Float64 x = xml.get("X", Float64.class);
            Float64 y = xml.get("Y", Float64.class);
            Float64 z = xml.get("Z", Float64.class);
            V._data = Float64Vector.valueOf(x.doubleValue(), y.doubleValue(), z.doubleValue());

            if (xml.hasNext())
                throw new XMLStreamException(RESOURCES.getString("toManyXMLElementsErr"));
        }

        @Override
        public void write(Vector3D V, OutputElement xml) throws XMLStreamException {

            xml.setAttribute("unit", V._unit.toString());
            xml.add(V._data.get(X), "X", Float64.class);
            xml.add(V._data.get(Y), "Y", Float64.class);
            xml.add(V._data.get(Z), "Z", Float64.class);

        }
    };

    /**
     * Tests the methods in this class.
     *
     * @param args Command line arguments (ignored).
     */
    public static void main(String args[]) {
        System.out.println("Testing Vector3D:  test = result [correct result]");

        Vector3D<Length> v1 = Vector3D.valueOf(1, 2, 3, NonSI.FOOT);
        System.out.println("v1 = " + v1);
        System.out.println("  converted to m       = " + v1.to(SI.METER) + " [{0.3048 m, 0.6096 m, 0.9144 m}]");
        System.out.println("  v1.norm()            = " + v1.norm() + " [3.74165738677394 ft]");
        System.out.println("  v1.opposite()        = " + v1.opposite() + " [{-1.0 ft, -2.0 ft, -3.0 ft}]");
        System.out.println("  v1.toUnitVector()    = " + v1.toUnitVector() + " [{0.267261241912424 , 0.534522483824849 , 0.801783725737273 }]");
        System.out.println("  v1.toSkewSymmetric() = \n" + v1.toSkewSymmetric());

        Parameter<Length> point = Parameter.valueOf(24, NonSI.INCH);
        System.out.println("point = " + point);
        System.out.println("  v1 + point           = " + v1.plus(point) + " [{3.0 ft, 4.0 ft, 5.0 ft}]");
        System.out.println("  v1 - point           = " + v1.minus(point) + " [{-1.0 ft, 0.0 ft, 1.0 ft}]");
        System.out.println("  v1 * 2               = " + v1.times(2) + " [{2.0 ft, 4.0 ft, 6.0 ft}]");

        Vector3D<?> areaVector = (Vector3D<?>)v1.times(point);
        System.out.println("  v1 * point           = " + areaVector);
//        System.out.println("  converted to ft²     = " + areaVector.to(NonSI.SQUARE_FOOT) + " [{2.0 ft², 4.0 ft², 6.0 ft²}]");

        Vector3D<Length> v2 = Vector3D.valueOf(1, 1, 1, SI.METER);
        Vector3D<?> v1xv2 = (Vector3D<?>)v1.cross(v2);
        System.out.println("v2 = " + v2);
        System.out.println("  v1 + v2 = " + v1.plus(v2) + " [{4.28083989501312 ft, 5.28083989501312 ft, 6.28083989501312 ft}]");
        System.out.println("  v1 - v2 = " + v1.minus(v2) + " [{-2.28083989501312 ft, -1.28083989501312 ft, -0.280839895013123 ft}]");
        System.out.println("  v1 · v2 = " + v1.times(v2.to(NonSI.FOOT)) + " [19.6850393700787 ft²]");
        System.out.println("  v1.cross(v2) = " + v1xv2.to(NonSI.FOOT.pow(2)) + " [{-3.28083989501312 ft^2, 6.56167979002625 ft^2, -3.28083989501312 ft^2}]");
        System.out.println("  v1.angle(v2) = " + v1.angle(v2).to(NonSI.DEGREE_ANGLE) + " [73.6090476746643 deg]");

        //  Write out XML data.
        try {
            System.out.println();

            // Creates some useful aliases for class names.
            javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
            binding.setAlias(org.jscience.mathematics.number.Float64.class, "Float64");
            binding.setAlias(org.jscience.mathematics.vector.Float64Matrix.class, "Float64Matrix");

            javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
            writer.setIndentation("    ");
            writer.setBinding(binding);
            writer.write(v1, "Vector3D", Vector3D.class);
            writer.flush();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
