/*
 *   AbstractParamVector -- A partial implementation of a vector of Parameter objects.
 *
 *   Copyright (C) 2008-2015, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.measure.Measurable;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Quantity;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.lang.MathLib;
import javolution.lang.Realtime;
import org.jscience.mathematics.structure.VectorSpaceNormed;
import org.jscience.mathematics.vector.DimensionException;
import org.jscience.mathematics.vector.Vector;

/**
 * This class represents an n-dimensional {@link Vector vector} of {@link Parameter}
 * elements.
 * 
 * <p> Modified by: Joseph A. Huwaldt </p>
 * 
 * @author Joseph A. Huwaldt, Date: November 21, 2008
 * @version October 31, 2015
 *
 * @param <Q> The Quantity (unit type, such as Length or Volume) of this parameter.
 * @param <T> The type of AbstractParamVector.
 */
public abstract class AbstractParamVector<Q extends Quantity, T extends AbstractParamVector<Q, T>> extends
        Vector<Parameter<Q>> implements
        VectorSpaceNormed<Vector<Parameter<Q>>, Parameter<Q>>, Measurable<Q>, Realtime {

    /**
     * The resource bundle for this package.
     */
    protected static final ResourceBundle RESOURCES = Parameter.RESOURCES;

    /**
     * Returns a Vector3D representation of this vector if possible.
     *
     * @return A Vector3D that is equivalent to this vector
     * @throws DimensionException if this vector has any number of dimensions other than
     * 3.
     */
    public abstract Vector3D<Q> toVector3D();

    /**
     * Return the specified {@link Vector3D} object as a vector of a particular subtype of
     * this class.
     *
     * @param vector The <code>Vector3D</code> object to be converted to a specific subtype
     *               of this class.
     * @return An instance of a specific subtype of this class that is equivalent to the
     *         supplied <code>Vector3D</code> object.
     * @throws ConversionException if the sub-type can not represent the units used in the
     * supplied vector.
     */
    public abstract T fromVector3D(Vector3D<Q> vector) throws ConversionException;

    /**
     * Returns the value of the Parameter in this vector as a <code>double</code> in the
     * current units of the specified dimension of this vector.
     *
     * @param i the dimension index.
     * @return the value of the Parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i > dimension()-1)</code>
     */
    public double getValue(int i) {
        return get(i).getValue();
    }

    /**
     * Returns the Euclidian norm, magnitude, or length of this vector (square root of the
     * dot product of this vector and itself).
     *
     * @return <code>sqrt(this · this)</code>.
     */
    @Override
    public Parameter<Q> norm() {
        return Parameter.valueOf(normValue(), getUnit());
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector.
     *
     * @return <code>this.norm().getValue()</code>.
     */
    public abstract double normValue();

    /**
     * Returns the Euclidian norm, magnitude, or length of this vector (square root of the
     * dot product of this vector and itself).
     *
     * @return <code>sqrt(this · this)</code>.
     */
    public Parameter<Q> mag() {
        return norm();
    }

    /**
     * Returns the {@link #norm}, magnitude, or length value of this vector.
     *
     * @return <code>this.norm().doubleValue()</code>.
     */
    public double magValue() {
        return normValue();
    }

    /**
     * Returns the sum of this vector with the parameter specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the parameter to be added to this vector.
     * @return <code>this + that</code>.
     */
    public abstract T plus(Parameter<Q> that);

    /**
     * Subtracts the supplied Parameter from this vector. The unit of the output vector
     * will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from this vector.
     * @return <code>this - that</code>.
     */
    public abstract T minus(Parameter<Q> that);

    /**
     * Returns the product of this vector with the specified coefficient.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    public abstract T times(double k);

    /**
     * Returns the dot product of this vector with the one specified.
     *
     * @param that the vector multiplier.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    public Parameter<?> dot(Vector that) {
        return times(that);
    }

    /**
     * Returns this vector with each element divided by the specified divisor
     * (dimensionless).
     *
     * @param divisor the divisor.
     * @return <code>this / divisor</code>.
     */
    public T divide(double divisor) {
        return times(1. / divisor);
    }

    /**
     * Returns the angle between this vector and the specified vector.
     *
     * @param that the vector to which the angle will be determined.
     * @return <code>acos(this · that)/(norm(this)*norm(that))</code>
     */
    public Parameter<Angle> angle(AbstractParamVector that) {
        double scalar = this.times(that).getValue();
        double abs1 = this.normValue();
        double abs2 = that.to(this.getUnit()).normValue();

        double argument = 1.;
        double dum = abs1 * abs2;
        if (dum > Parameter.SQRT_EPS)
            argument = scalar / dum;

        if (argument > 1.)
            argument = 1.;
        else if (argument < -1.)
            argument = -1.;

        return Parameter.valueOf(MathLib.acos(argument), SI.RADIAN);
    }

    /**
     * Returns the unit in which the values in this vector are stated.
     *
     * @return The unit in which the values in this vector are stated
     */
    public abstract Unit<Q> getUnit();

    /**
     * Returns the equivalent to this vector but stated in the specified unit.
     *
     * @param <R>  The Quantity (unit type) of the vector being output.
     * @param unit the unit of the vector to be returned.
     * @return a vector equivalent to this vector but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public abstract <R extends Quantity> AbstractParamVector<R, ? extends AbstractParamVector> to(Unit<R> unit);

    /**
     * Returns the value of this measurable stated in the specified unit as a
     * <code>double</code>. This implementation returns the {@link #normValue} stated in
     * the specified units.
     *
     * @param unit the unit in which this measurable value is stated.
     * @return the numeric value after conversion to type <code>double</code>.
     */
    @Override
    public double doubleValue(Unit<Q> unit) {
        Unit<?> thisUnit = getUnit();
        return ((thisUnit == unit) || thisUnit.equals(unit)) ? this.normValue() : this.to(unit).normValue();
    }

    /**
     * Returns the estimated integral value of this measurable stated in the specified
     * unit as a <code>long</code>. This implementation returns the {@link #normValue}
     * stated in the specified units.
     *
     * <p>
     * Note: This method differs from the <code>Number.longValue()</code> in the sense
     * that the closest integer value is returned and an ArithmeticException is raised
     * instead of a bit truncation in case of overflow (safety critical).</p>
     *
     * @param unit the unit in which the measurable value is stated.
     * @return the numeric value after conversion to type <code>long</code>.
     * @throws ArithmeticException if this quantity cannot be represented as a
     * <code>long</code> number in the specified unit.
     */
    @Override
    public final long longValue(Unit<Q> unit) {
        if (!getUnit().equals(unit))
            return this.to(unit).longValue(unit);
        double doubleValue = this.normValue();
        if ((doubleValue >= Long.MIN_VALUE) && (doubleValue <= Long.MAX_VALUE))
            return Math.round(doubleValue);
        throw new ArithmeticException(
                MessageFormat.format(RESOURCES.getString("canNotBeLong"), doubleValue, getUnit()));
    }

    /**
     * Compares the {@link #norm} of this measure with the specified measurable object.
     *
     * @param that the measure to compare with.
     * @return a negative integer, zero, or a positive integer as this measure is less
     *         than, equal to, or greater than that measurable.
     * @throws ConversionException if the current model does not allows for these measure
     * to be compared.
     */
    @Override
    public int compareTo(Measurable<Q> that) {
        double thatValue = that.doubleValue(getUnit());
        return Double.compare(this.normValue(), thatValue);
    }

    /**
     * Fills the input Java array with the values of the coordinates of this vector in
     * the current units.
     * 
     * @param array An existing array of at least this.getDimension() size that will be
     * filled with the values from this vector.
     * @return The input Java array filled with the coordinate values from this vector.
     */
    public double[] toArray(double[] array) {
        int size = this.getDimension();
        for (int i=size-1; i >= 0; --i) {
            array[i] = this.getValue(i);
        }
        return array;
    }
    
    /**
     * Returns a new Java array that contains the values of the coordinates of this vector in
     * the current units.
     * 
     * @return A new Java array of values from this vector.
     */
    public double[] toArray() {
        double[] arr = new double[this.getDimension()];
        return toArray(arr);
    }
}
