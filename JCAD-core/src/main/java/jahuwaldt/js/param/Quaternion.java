/*
*   Quaternion -- A quaternion that represents the orientation between
*                 two reference frames.
*
*   Copyright (C) 2009-2014 by Joseph A. Huwaldt.
*   All rights reserved.
*   
*   This library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; either
*   version 2 of the License, or (at your option) any later version.
*   
*   This library is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*   Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*   Or visit:  http://www.gnu.org/licenses/lgpl.html
**/
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import javax.measure.quantity.*;
import javax.measure.unit.SI;
import javax.measure.unit.NonSI;

import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;
import javolution.xml.XMLSerializable;
import javolution.text.Text;
import javolution.text.TextBuilder;

import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.*;


/**
* <p> This class represents a 4 element quaternion made up of Float64
*	  elements. Quaternions may be used to represents a relative attitude
*     (attitude transformation) between two different reference
*     frames; B wrt A or BA.  It can be used to transform coordinates in reference frame A
*     to reference frame B (A2B).</p>
*
* <p> The following quaternion definition is used:<pre>
*      q.x = e.x * sin(phi/2);
*      q.y = e.y * sin(phi/2);
*      q.z = e.z * sin(phi/2);
*      q.w = cos(phi/2);
*   </pre></p>
*
* <p> Reference: Glaese, John, "Quaternions -- A Brief Exposition", Rev. 2, NASA MSFC, 1974 and<br>
*                Farrell, Jay A. and Matthew Barth, "The Global Positioning System &
*                Inertial Navigation", pp. 39 - 42.</p>
* 
*  <p>  Modified by:  Joseph A. Huwaldt   </p>
*
*  @author  Joseph A. Huwaldt   Date: January 28, 2009
*  @version March 1, 2014
**/
public final class Quaternion extends AbstractRotation<Quaternion> implements XMLSerializable {

	private static final long serialVersionUID = 203148779610744030L;

	/**
	* The index to the vector X component of this quaternion.
	**/
	public static final int X = 0;
	
	/**
	*  The index to the vector Y component of this quaternion.
	**/
	public static final int Y = 1;
	
	/**
	*  The index to the vector Z component of this quaternion.
	**/
	public static final int Z = 2;
	
	/**
	*  The index to the scalar W component of this quaternion.
	**/
	public static final int W = 3;
	
		
	/**
	*  The elements of the quaternion.
	*  Serialization is handled by this class since Float64Matrix is not Serializable.
	**/
	private transient Float64Vector _data;
	
	
	/**
 	* Returns a {@link Quaternion} instance holding the specified <code>double</code> values
	* for the quaternion elements.
 	*
 	* @param x the x value.
 	* @param y the y value.
 	* @param z the z value.
 	* @param w the w value.
 	* @return The quaternion having the specified values.
 	*/
	public static Quaternion valueOf(double x, double y, double z, double w) {
		Quaternion Q = Quaternion.newInstance();
		Q._data = Float64Vector.valueOf(x, y, z, w);
		return Q;
	}
	
	/**
 	* Returns a {@link Quaternion} instance holding the specified <code>double</code> values
	* for the quaternion elements in the order (x, y, z, w).
 	*
 	* @param values  A 4 element array of quaternion elements (x, y, z, w).
 	* @return The quaternion having the specified values.
	* @throws DimensionException if the input array does not have 4 elements.
 	*/
	public static Quaternion valueOf(double[] values) {
		if (values.length != 4)
			throw new DimensionException( RESOURCES.getString("q4DVectorReqErr") );
		
		Quaternion Q = Quaternion.newInstance();
		Q._data = Float64Vector.valueOf(values);
		
		return Q;
	}
	
	/**
 	* Returns a {@link Quaternion} instance containing the specified unit vector and scalar.
 	*
 	* @param vector The unit vector part of the quaternion.
	* @param scalar The scalar part of the quaternion.
 	* @return The quaternion having the specified values.
 	*/
	public static Quaternion valueOf(Vector3D<Dimensionless> vector, double scalar) {
		
		Quaternion Q = Quaternion.newInstance();
		Q._data = Float64Vector.valueOf(vector.getValue(X), vector.getValue(Y), vector.getValue(Z), scalar);
		
		return Q;
	}
	
	/**
 	* Returns a {@link Quaternion} instance containing the specified vector of Float64 values.
	* The vector must have exactly 4 elements in the order (x, y, z, w).
 	*
 	* @param vector The vector of Float64 quaternion element values
 	*               (must have dimension of 4). 
 	* @return The quaternion having the specified values.
	* @throws DimensionException if the input vector does not have 4 elements.
 	*/
	public static Quaternion valueOf(Vector<Float64> vector) {
		if (vector.getDimension() != 4)
			throw new DimensionException( RESOURCES.getString("q4DVectorReqErr") );
		
		Quaternion Q = Quaternion.newInstance();
		Q._data = Float64Vector.valueOf(vector);
		
		return Q;
	}
	
	/**
 	* Returns a {@link Quaternion} representing a rotation about an arbitrary axis.
 	*
 	* @param uv  The unit vector axis of rotation expressed in a reference frame.
	* @param phi The rotation angle about the specified rotation axis.
 	* @return The quaternion representing the specified rotation angle about
 	*         the specified rotation axis.
 	*/
	public static Quaternion valueOf(Vector3D<Dimensionless> uv, Parameter<Angle> phi) {
		
		double phiR = phi.doubleValue(SI.RADIAN);
		double cphi_2 = cos(phiR/2.0);
		double sphi_2 = sin(phiR/2.0);
		
		double x = uv.getValue(X)*sphi_2;
		double y = uv.getValue(Y)*sphi_2;
		double z = uv.getValue(Z)*sphi_2;
		double w = cphi_2;
		
		Quaternion Q = Quaternion.valueOf(x,y,z,w);
		return Q;
	}
	
	/**
 	* Returns a new {@link Quaternion} instance constructed from the specified rotation transform.
 	*
 	* @param transform The Rotation transform to convert to a new quaternion. 
 	* @return the quaternion representing the specified attitude transform.
 	*/
	public static Quaternion valueOf(Rotation<?> transform) {
		Quaternion q;
		if (transform instanceof Quaternion)
			q = copyOf((Quaternion)transform);
		else
			q = transform.toQuaternion();
		return q;
	}
	

	/**
	*  Returns the vector part of this quaternion.
	**/
	public Vector3D<Dimensionless> getVector() {
		Vector3D<Dimensionless> V = Vector3D.valueOf(_data.getValue(X), _data.getValue(Y), _data.getValue(Z), Dimensionless.UNIT);
		return V;
	}
	
	/**
	*  Return the scalar part of this quaternion.
	**/
	public Float64 getScalar() {
		return _data.get(W);
	}
	
	/**
	*  Return the scalar part of this quaternion as a floating point number.
	**/
	public double getScalarValue() {
		return _data.getValue(W);
	}
	
	/**
 	* Returns the value of an element from this quaternion (0=x, 1=y, 2=z, 3=w).
 	*
 	* @param  i the dimension index (0=x, 1=y, 2=z, 3=w).
 	* @return the value of the element at <code>i</code>.
 	* @throws IndexOutOfBoundsException <code>(i < 0) || (i >= dimension())</code>
 	*/
	public Float64 get(int i) {
		return _data.get(i);
	}
	
    /**
     * Returns the value of a floating point number from this quaternion (0=x, 1=y, 2=z, 3=w).
     *
     * @param  i the floating point number index (0=x, 1=y, 2=z, 3=w).
     * @return the value of the floating point number at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i < 0) || (i >= dimension())</code>
     */
    public double getValue(int i) {
		return _data.getValue(i);
	}
	
	/**
 	* Returns the Euclidian norm, magnitude, or length of this quaternion (square root of the 
 	* dot product of this quaternion and it's conjugate).
 	*
 	* @return <code>sqrt(this · this^*)</code>.
 	*/
	public Float64 norm() {
		return _data.norm();
	}

	/**
 	* Returns the {@link #norm}, magnitude, or length value of this quaternion.
 	*
 	* @return <code>this.norm().doubleValue()</code>.
 	*/
	public double normValue() {
		return _data.normValue();
	}

	/**
	* Returns the negation of this quaternion (all elements are multiplied by -1).
	*
	* @return <code>-this</code>.
	* @see #conjugate
	*/
	public Quaternion opposite() {
		Quaternion Q = Quaternion.newInstance();
		Q._data = this._data.opposite();
		return Q;
	}

	/**
	* Returns the conjugate or spatial inverse of this quaternion.
	*
	* @return <code>this^*</code>.
	* @see <a href="http://en.wikipedia.org/wiki/Quaternion#Conjugation.2C_the_norm.2C_and_division">
	* 	Wikipedia: Quaternion Conjugation</a>
	*/
	public Quaternion conjugate() {
		double x = -this.getValue(X);
		double y = -this.getValue(Y);
		double z = -this.getValue(Z);
		double w = this.getValue(W);
		
		Quaternion Q = Quaternion.valueOf(x,y,z,w);
		return Q;
	}

	/**
	*  Returns the spatial inverse of this transformation: AB rather than BA.
	*  This implementation returns the conjugate of this quaternion.
	*
	*  @return <code>this' = this^*</code>
	**/
    @Override
	public Quaternion transpose() {
		return this.conjugate();
	}
	
	/**
	* Returns the sum of this quaternion with the one specified.
	*
	* @param   that the quaternion to be added.
	* @return  <code>this + that</code>.
	*/
	public Quaternion plus(Quaternion that) {
		
		Quaternion Q = Quaternion.newInstance();
		Q._data = this._data.plus(that._data);
		
		return Q;
	}
	
	/**
	* Returns the difference between this quaternion and the one specified.
	*
	* @param  that the quaternion to be subtracted.
	* @return <code>this - that</code>.
	*/
	public Quaternion minus(Quaternion that) {
		
		Quaternion Q = Quaternion.newInstance();
		Q._data = this._data.minus(that._data);
		
		return Q;
	}

	/**
	* Returns the product of this quaternion with the specified coefficient.
	*
	* @param  k the coefficient multiplier.
	* @return <code>this · k</code>
	*/
	public Quaternion times(Float64 k) {
		Quaternion Q = Quaternion.newInstance();
		Q._data = this._data.times(k);
		return Q;
	}
	
	/**
	* Returns the product of this quaternion with the specified coefficient.
	*
	* @param  k the coefficient multiplier.
	* @return <code>this times k</code>
	*/
	public Quaternion times(double k) {
		Quaternion Q = Quaternion.newInstance();
		Q._data = _data.times(k);
		return Q;
	}
	
	/**
	*  Returns the quaternion product of this quaternion with the specified rotation transform.
	*  If this quaternion is BA and that is AC then the returned
	*  value is:  BC = BA times AC (or C2B = A2B times C2A).
	*
	* @param  that the rotation transform multiplier.
	* @return <code>this times that</code>
	*/
    @Override
    public Quaternion times(Rotation<?> that) {
        StackContext.enter();
        try {
            Quaternion thatQ = that.toQuaternion();
            Float64Matrix M = this.toLeftMatrix();
            Float64Vector q3 = M.times(thatQ.toFloat64Vector());

            Quaternion Q = Quaternion.valueOf(q3);
            return StackContext.outerCopy(Q);

        } finally {
            StackContext.exit();
        }
    }
	
	/**
	* Returns the quaternion division of this quaternion with the specified rotation transform.
	*
	* @param  that the rotation transform divisor.
	* @return <code>this / that = this times that^-1</code>
	*/
	public Quaternion divide(Rotation<?> that) {
		Quaternion thatQ = that.toQuaternion();
		Quaternion Q = this.times(thatQ.inverse());
		return Q;
	}
	
	/**
	*  Returns the quaternion inverse of this quaternion.
	*
	* @return <code>this^-1 = this^* / norm(this)^2</code>
	**/
	public Quaternion inverse() {
		double x = _data.getValue(X);
		double y = _data.getValue(Y);
		double z = _data.getValue(Z);
		double w = _data.getValue(W);
		double Nq = x*x + y*y + z*z + w*w;		//	= q^* x q
		
		Quaternion Q = Quaternion.valueOf(-x/Nq,-y/Nq,-z/Nq,w/Nq);
		return Q;
	}
	
	/**
	*  Transforms a 3D vector from frame A to B using this quaternion.
	*
	*  @param v the vector expressed in frame A.
	*  @return the vector expressed in frame B.
	**/
    @Override
	public <Q extends Quantity> Vector3D<Q> transform(Coordinate3D<Q> v) {
		/* The following is slightly faster than: DCMatrix TM = this.toDCM(); V out = TM.times(v);
		   since it eliminates the overhead of creating a DCMatrix object.  Otherwise it is identical.
		*/
		double x = _data.getValue(X);
		double y = _data.getValue(Y);
		double z = _data.getValue(Z);
		double w = _data.getValue(W);
		
		double Nq = x*x + y*y + z*z + w*w;		//	= q^* x q
		if (Nq > 0.0)	Nq = 2.0/Nq;
		
		double Xs = x*Nq, Ys = y*Nq, Zs = z*Nq;
		double wX = w*Xs, wY = w*Ys, wZ = w*Zs;
		double xX = x*Xs, xY = x*Ys, xZ = x*Zs;
		double yY = y*Ys, yZ = y*Zs, zZ = z*Zs;
		
		Vector3D<Q> vec = v.toVector3D();
		double v1 = vec.getValue(X);
		double v2 = vec.getValue(Y);
		double v3 = vec.getValue(Z);
		
		double v1new = (1.0 - (yY + zZ))*v1 +         (xY - wZ)*v2 +         (xZ + wY)*v3;
		double v2new =         (xY + wZ)*v1 + (1.0 - (xX + zZ))*v2 +         (yZ - wX)*v3;
		double v3new =         (xZ - wY)*v1 +         (yZ + wX)*v2 + (1.0 - (xX + yY))*v3;
		
		Vector3D<Q> out = Vector3D.valueOf(v1new, v2new, v3new, v.getUnit());
		return out;
	}
	
	/**
	*  Returns this quaternion with unit length (a versor).
	*
	*  @return <code>this.divide(norm(this))</code>
	**/
	public Quaternion toUnit() {
		double magnitude = this.normValue();
		if (abs(magnitude - 1.0) <= Parameter.EPS)	return this;
		Quaternion Q = this.times(1.0/magnitude);
		return Q;
	}
	
	/**
	*  Returns the values stored in this vector as a Float64Vector with
	*  the values ordered (x, y, z, w).  The scalar is in the 4th element.
	**/
	public Float64Vector toFloat64Vector() {
		return _data;
	}
	
	/**
	*  Returns a direction cosine transformation matrix from this quaternion.
	*
	*  @return a direction cosine matrix that converts from frame A to B.
	*  @see <a href="http://en.wikipedia.org/wiki/Rotation_matrix#Quaternion">
	* 	Wikipedia: Rotation Matrix-Quaternion</a>
	**/
    @Override
	public DCMatrix toDCM() {
		//	Algorithm From:  "Quaternions", by Ken Shoemake
		//	http://www.cs.caltech.edu/courses/cs171/quatut.pdf
    
		double x = _data.getValue(X);
		double y = _data.getValue(Y);
		double z = _data.getValue(Z);
		double w = _data.getValue(W);
		
		double Nq = x*x + y*y + z*z + w*w;		//	= q^* x q
		if (Nq > 0.0)	Nq = 2.0/Nq;
		
		double Xs = x*Nq, Ys = y*Nq, Zs = z*Nq;
		double wX = w*Xs, wY = w*Ys, wZ = w*Zs;
		double xX = x*Xs, xY = x*Ys, xZ = x*Zs;
		double yY = y*Ys, yZ = y*Zs, zZ = z*Zs;
		
		Float64Vector row0 = Float64Vector.valueOf(1.0 - (yY + zZ),          xY - wZ,          xZ + wY);
		Float64Vector row1 = Float64Vector.valueOf(        xY + wZ,  1.0 - (xX + zZ),          yZ - wX);
		Float64Vector row2 = Float64Vector.valueOf(        xZ - wY,          yZ + wX,  1.0 - (xX + yY));
		DCMatrix M = DCMatrix.valueOf(row0, row1, row2);
		
		return M;
	}
	
	/**
	*  Returns a quaternion representing this attitude transformation.
	*
	*  @return a quaternion that converts from frame A to B.
	*  @see <a href="http://en.wikipedia.org/wiki/Quaternion">>
	* 	Wikipedia: Quaternion</a>
	**/
    @Override
	public Quaternion toQuaternion() {
		return this;
	}
	
	/**
	*  <p> Compute the angular velocity quaternion matrix (Omega) from an angular velocity vector of three-space.
	*      Used to propagate quaternions:
	*      qdot = Omega * q ==> <code>Float64Vector qdot = Quaternion.omega(w).times(q.toFloat64Vector());</code></p>
	*
	*  <p> Values are ordered as follows:
	*  <pre>
	*   M = 0.5* {   0   r  -q   p,  = { [Omega]' |  [w] }   where [Omega] = skewsym([w])
	*               -r   0   p   q,    {-----------------}         and [w] = [p q r]' = ang. vel. vect.
	*                q  -p   0   r,    {    -[w]  |   0  }
	*               -p  -q  -r   0}
	*  </pre>
	*
	*
	*  @param w The angular velocity vector [p q r]'.
	*  @return A 4x4 angular velocity quaternion matrix in units of radians/second.
	**/
	public static Float64Matrix omega(Vector3D<AngularVelocity> w) {
		
		w = w.to(AngularVelocity.UNIT);
		double p = 0.5*w.getValue(X);
		double q = 0.5*w.getValue(Y);
		double r = 0.5*w.getValue(Z);
		
		Float64Vector row0 = Float64Vector.valueOf(   0,   r,  -q,   p );
		Float64Vector row1 = Float64Vector.valueOf(  -r,   0,   p,   q );
		Float64Vector row2 = Float64Vector.valueOf(   q,  -p,   0,   r );
		Float64Vector row3 = Float64Vector.valueOf(  -p,  -q,  -r,   0 );
		
		Float64Matrix M = Float64Matrix.valueOf(row0, row1, row2, row3);
		return M;
	}
	
	/**
	*  Returns a 4x4 matrix version of this quaternion used to multiply on the left (q*p = Lq*p)
	*  where "Lq" is the matrix returned by this method and "p" is treated as a 4-element column vector.
	*  Values are ordered as follows:
	*  <pre>
	*  M = {  w  -z   y   x,  = { w*[E] + [Q]  |  [q]  }   where [Q] = skewsym([q])  and [q] = q.getVector()
	*         z   w  -x   y,    {----------------------}         [E] = identity matrix
	*        -y   x   w   z,    {      -[q]    |   w   }
	*        -x  -y  -z   w}
	*  </pre>
	*
	*  @return 4x4 <code>Lq</code> matrix such that <code>Lq.times(p.toFloat64Vector()) ==	q.times(p).toFloat64Vector()</code>
	**/
	public Float64Matrix toLeftMatrix() {
		double x = _data.getValue(X);
		double y = _data.getValue(Y);
		double z = _data.getValue(Z);
		double w = _data.getValue(W);
		
		Float64Vector row0 = Float64Vector.valueOf( w, -z,  y,  x);
		Float64Vector row1 = Float64Vector.valueOf( z,  w, -x,  y);
		Float64Vector row2 = Float64Vector.valueOf(-y,  x,  w,  z);
		Float64Vector row3 = Float64Vector.valueOf(-x, -y, -z,  w);		
		
		Float64Matrix M = Float64Matrix.valueOf(row0, row1, row2, row3);
		return M;
	}
	
	/**
	*  Returns a 4x4 matrix version of this quaternion used to multiply on the right (p*q = p*Rq)
	*  where "Rq" is the matrix returned by this method and "p" is treated as a 4-element column vector.
	*  Values are ordered as follows:
	*  <pre>
	*  M = {  w   z  -y   x,  = { w*[E] + [Q]' |  [q]  }   where [Q] = skewsym([q])  and [q] = q.getVector()
	*        -z   w   x   y,    {----------------------}         [E] = identity matrix
	*         y  -x   w   z,    {      -[q]    |   w   }
	*        -x  -y  -z   w}
	*  </pre>
	*
	*  @return 4x4 <code>Rq</code> matrix such that
	*          <code>Rq.times(p.toFloat64Vector()) == p.times(q.toFloat64Vector()).toFloat64Vector()</code>
	**/
	public Float64Matrix toRightMatrix() {
		double x = _data.getValue(X);
		double y = _data.getValue(Y);
		double z = _data.getValue(Z);
		double w = _data.getValue(W);
		
		Float64Vector row0 = Float64Vector.valueOf( w,  z, -y,  x);
		Float64Vector row1 = Float64Vector.valueOf(-z,  w,  x,  y);
		Float64Vector row2 = Float64Vector.valueOf( y, -x,  w,  z);
		Float64Vector row3 = Float64Vector.valueOf(-x, -y, -z,  w);
		
		Float64Matrix M = Float64Matrix.valueOf(row0, row1, row2, row3);
		return M;
	}
	
	/**
	* Returns a copy of this quaternion 
	* {@link javolution.context.AllocatorContext allocated} 
	* by the calling thread (possibly on the stack).
	*	
	* @return an identical and independent copy of this quaternion.
	*/
    @Override
	public Quaternion copy() {
		return copyOf(this);
	}
	
	/**
	* Returns the text representation of this quaternion.
	*
	* @return the text representation of this quaternion.
	*/
    @Override
	public Text toText() {
		final int dimension = 4;
		TextBuilder tmp = TextBuilder.newInstance();
		if (this.isApproxEqual(IDENTITY))
			tmp.append("{IDENTITY}");
		else {
			tmp.append('{');
			for (int i = 0; i < dimension; i++) {
				tmp.append(get(i));
				if (i != dimension - 1)
					tmp.append(", ");
			}
			tmp.append('}');
		}
		Text txt = tmp.toText();
		TextBuilder.recycle(tmp); 
		return txt;
	}

	/**
 	* Compares this Rotation against the specified Rotation for approximate 
 	* equality (a Rotation object with rotation is equal to this one to within the 
	* numerical roundoff tolerance).
	*
 	* @param  obj the Rotation object to compare with.
 	* @return <code>true</code> if this Rotation is approximately identical to that
 	* 		Rotation; <code>false</code> otherwise.
	**/
    @Override
	public boolean isApproxEqual(Rotation<?> obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		//	Check for approximate equality of components.
		Quaternion thatQ = obj.toQuaternion();
		for (int i=0; i < 4; ++i) {
			double thisVal = this.getValue(i);
			double thatVal = thatQ.getValue(i);
			if (!MathTools.isApproxEqual(thisVal, thatVal, Parameter.EPS10))
				return false;
		}
		
		return true;
	}
    
	/**
 	* Compares this Quaternion against the specified object for strict 
 	* equality (same rotation type and same values).
	*
 	* @param  obj the object to compare with.
 	* @return <code>true</code> if this rotation is identical to that
 	* 		rotation; <code>false</code> otherwise.
	**/
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		
		Quaternion that = (Quaternion)obj;
		return this._data.equals(that._data);
	}

	/**
 	* Returns the hash code for this rotation.
 	* 
 	* @return the hash code value.
 	*/
    @Override
	public int hashCode() {
		int hash = 7;
		
		int var_code = _data.hashCode();
		hash = hash*31 + var_code;
		
		return hash;
	}

	
	/**
	*  During serialization, this will write out the Float64Vector as a
	*  simple series of <code>double</code> values.    This method is ONLY called by the Java
	*  Serialization mechanism and should not otherwise be used.
	**/
	private void writeObject( java.io.ObjectOutputStream out ) throws java.io.IOException {
	
		// Call the default write object method.
		out.defaultWriteObject();

		//	Write out the three coordinate values.
		out.writeDouble( _data.getValue(X) );
		out.writeDouble( _data.getValue(Y) );
		out.writeDouble( _data.getValue(Z) );
		out.writeDouble( _data.getValue(W) );
		
	}

	/**
	*  During de-serialization, this will handle the reconstruction
	*  of the Float64Vector.  This method is ONLY called by the Java
	*  Serialization mechanism and should not otherwise be used.
	**/
	private void readObject( java.io.ObjectInputStream in ) throws java.io.IOException, ClassNotFoundException {
	
		// Call the default read object method.
		in.defaultReadObject();

		//	Read in the three coordinate values.
		double x = in.readDouble();
		double y = in.readDouble();
		double z = in.readDouble();
		double w = in.readDouble();
		
		_data = Float64Vector.valueOf(x,y,z,w);
		
	}
	
	/**
 	* Holds the default XML representation. For example:
 	* <pre>
 	*	&lt;Quaternion&gt;
 	*		&lt;X value="1.0" /&gt;
 	*		&lt;Y value="0.0" /&gt;
 	*		&lt;Z value="2.0" /&gt;
	*		&lt;W value="1.0" /&gt;
  	*	&lt;/Quaternion&gt;
	* </pre>
 	*/
	protected static final XMLFormat<Quaternion> XML = new XMLFormat<Quaternion>(Quaternion.class) {

		@Override
		public Quaternion newInstance(Class<Quaternion> cls, InputElement xml) throws XMLStreamException {
			return FACTORY.object();
		}

		@Override
		public void read(InputElement xml, Quaternion Q) throws XMLStreamException {
			
			double x = xml.get("X", Float64.class).doubleValue();
			double y = xml.get("Y", Float64.class).doubleValue();
			double z = xml.get("Z", Float64.class).doubleValue();
			double w = xml.get("W", Float64.class).doubleValue();
			Q._data = Float64Vector.valueOf(x,y,z,w);
			
			if (xml.hasNext()) 
				throw new XMLStreamException("Too many elements");
		}

		@Override
		public void write(Quaternion Q, OutputElement xml) throws XMLStreamException {
				
			xml.add(Q._data.get(X), "X", Float64.class);
			xml.add(Q._data.get(Y), "Y", Float64.class);
			xml.add(Q._data.get(Z), "Z", Float64.class);
			xml.add(Q._data.get(W), "W", Float64.class);
			
		}
	};
	
	
	///////////////////////
	// Factory creation. //
	///////////////////////

	private static final ObjectFactory<Quaternion> FACTORY = new ObjectFactory<Quaternion>() {
		@Override
		protected Quaternion create() {
			return new Quaternion();
		}
	};

	private static Quaternion newInstance() {
		Quaternion Q = FACTORY.object();
		return Q;
	}

	private static Quaternion copyOf(Quaternion original) {
		Quaternion Q = Quaternion.newInstance();
		Q._data = original._data.copy();
		return Q;
	}

	private Quaternion() {}
	

	/**
	*  Tests the methods in this class.
	**/
    public static void main (String args[]) {
		System.out.println("Testing Quaternion:");
		
		Parameter<Angle> psi = Parameter.valueOf(60,NonSI.DEGREE_ANGLE);
		Parameter<Angle> theta = Parameter.ZERO_ANGLE;
		Parameter<Angle> phi = Parameter.ZERO_ANGLE;
		System.out.println("psi = " + psi + ", theta = " + theta + ", phi = " + phi);
		
		DCMatrix m1 = DCMatrix.getEulerTM(psi, theta, phi);
		Quaternion q1 = Quaternion.valueOf(m1);
		System.out.println("m1 = \n" + m1);
		System.out.println("q1 = " + q1);
		System.out.println("q1.norm() = " + q1.norm());
		System.out.println("q1.getVector() = " + q1.getVector());
		System.out.println("q1.getScalar() = " + q1.getScalar());
		System.out.println("q1.conjugate() = " + q1.conjugate());
		System.out.println("q1.inverse() = " + q1.inverse());
		System.out.println("q1.toDCM() = \n" + q1.toDCM());
		
		Vector3D<Length> v1 = Vector3D.valueOf(1, 0, 0, SI.METER);
		System.out.println("\nv1 = " + v1);
		System.out.println("  q1.transform(v1) = " + q1.transform(v1));
		
		Quaternion q2 = Quaternion.valueOf(Vector3D.UNIT_Z, psi);
		System.out.println("q2 = (should == q1) = " + q2);
		
		theta = Parameter.valueOf(30,NonSI.DEGREE_ANGLE);
		DCMatrix m2 = DCMatrix.getEulerTM(psi, theta, phi);
		Quaternion q3 = Quaternion.valueOf(Vector3D.UNIT_Y, theta);
		Quaternion q4 = q1.times(q3);
		System.out.println("\npsi = " + psi + ", theta = " + theta + ", phi = " + phi);
		System.out.println("m2 = \n" + m2);
		System.out.println("q3 = " + q3);
		System.out.println("q4 = q1.times(q3)  = " + q4);
		System.out.println("  q4.transform(v1) = " + q4.transform(v1));
		System.out.println("  m2.times(v1)     = " + m2.times(v1));
		System.out.println("Lq1.times(q3) = " + q1.toLeftMatrix().times(q3.toFloat64Vector()));
		System.out.println("Rq1.times(q3) = " + q1.toRightMatrix().times(q3.toFloat64Vector()));
		System.out.println("q3.times(q1)  = " + q3.times(q1));
		System.out.println("q4.conjugate() = " + q4.conjugate());
		Quaternion q4T = Quaternion.valueOf(m2.transpose());
		System.out.println("q4T = " + q4T);
		System.out.println("m2.toQuaternion() = " + m2.toQuaternion());
		System.out.println("m2.transpose().toQuaternion() = " + m2.transpose().toQuaternion());
		DCMatrix m4 = q4.toDCM();
		System.out.println("\nq4 = " + q4);
		System.out.println("m4 = q4.toDCM() = \n" + m4);
		System.out.println("m4.toQuaternion() = " + m4.toQuaternion());
        System.out.println("q3.toUnit() = " + q3.toUnit());
		
		//	Write out XML data.
		try {
			System.out.println();
			
			// Creates some useful aliases for class names.
			javolution.xml.XMLBinding binding = new javolution.xml.XMLBinding();
			binding.setAlias(org.jscience.mathematics.number.Float64.class, "Float64");
            binding.setAlias(org.jscience.mathematics.vector.Float64Matrix.class, "Float64Matrix");
			
			javolution.xml.XMLObjectWriter writer = javolution.xml.XMLObjectWriter.newInstance(System.out);
			writer.setIndentation("    ");
			writer.setBinding(binding);
			writer.write(q1, "Quaternion", Quaternion.class);
			writer.flush();
			
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}