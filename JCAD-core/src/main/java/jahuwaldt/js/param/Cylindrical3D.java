/*
 *   Cylindrical3D -- A 3D cylindrical coordinate of radius, azimuth, and height.
 *
 *   Copyright (C) 2015, by Joseph A. Huwaldt.
 *   All rights reserved.
 *   
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *   Or visit:  http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.param;

import jahuwaldt.tools.math.MathTools;
import java.text.MessageFormat;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.*;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.context.StackContext;
import static javolution.lang.MathLib.*;
import javolution.xml.XMLFormat;
import javolution.xml.XMLSerializable;
import javolution.xml.stream.XMLStreamException;
import org.jscience.mathematics.vector.*;

/**
 * This class represents a 3 element {@link Vector vector} of Parameter elements
 * representing a geometrical cylindrical coordinate with elements radius, azimuth angle
 * and height.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: October 29, 2015
 * @version November 3, 2015
 *
 * @param <Q> The Quantity (unit type, such as Length or Volume) of this vector.
 */
public final class Cylindrical3D<Q extends Quantity> extends Coordinate3D<Q> implements XMLSerializable {

    private static final long serialVersionUID = 1L;

    /**
     * Constant used to identify the radius of in the vector.
     */
    public static final int RADIUS = 0;

    /**
     * Constant used to identify the azimuth angle of the vector.
     */
    public static final int AZIMUTH = 1;

    /**
     * Constant used to identify the height or altitude of the vector.
     */
    public static final int HEIGHT = 2;

    /**
     * The radius of this vector.
     */
    private Parameter<Q> _radius;

    /**
     * The azimuth angle of this vector.
     */
    private Parameter<Angle> _azimuth;

    /**
     * The height/altitude of this vector.
     */
    private Parameter<Q> _height;

    /**
     * Returns a {@link Cylindrical3D} instance holding the specified <code>double</code>
     * values stated in the specified units.
     *
     * @param <Q>        the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param radius     the radius of the vector stated in the specified radius unit.
     * @param azimuth    the vector azimuth angle stated in the specified angle unit.
     * @param height     the vector height/altitude value stated in the specified radius
     *                   unit.
     * @param radiusUnit the unit in which the radius and height is stated.
     * @param angleUnit  the unit in which the azimuth angle is stated.
     * @return the vector having the specified values.
     */
    public static <Q extends Quantity> Cylindrical3D<Q> valueOf(double radius, double azimuth, double height,
            Unit<Q> radiusUnit, Unit<Angle> angleUnit) {
        Cylindrical3D<Q> V = Cylindrical3D.newInstance();

        V._radius = Parameter.valueOf(radius, radiusUnit);
        V._azimuth = Parameter.valueOf(azimuth, angleUnit);
        V._height = Parameter.valueOf(height, radiusUnit);

        return V;
    }

    /**
     * Returns a {@link Cylindrical3D} instance holding the specified
     * <code>Parameter</code> values.
     *
     * @param <Q>     the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param radius  the vector radius value.
     * @param azimuth the vector azimuth angle value.
     * @param height  the vector height/altitude value.
     * @return the vector having the specified values in the units of magnitude.
     */
    public static <Q extends Quantity> Cylindrical3D<Q> valueOf(Parameter<Q> radius, Parameter<Angle> azimuth, Parameter<Q> height) {
        Cylindrical3D<Q> V = Cylindrical3D.newInstance();
        V._radius = radius;
        V._azimuth = azimuth;
        V._height = height.to(radius.getUnit());

        return V;
    }

    /**
     * Returns a {@link Cylindrical3D} instance containing the cylindrical coordinate
     * representation of the specified {@link Coordinate3D coordinate}. The azimuth
     * component of the resulting vector will have units of radians.
     *
     * @param <Q>        the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param coordinate the input coordinate.
     * @return the cylindrical coordinate having the specified values.
     */
    public static <Q extends Quantity> Cylindrical3D<Q> valueOf(Coordinate3D<Q> coordinate) {

        if (coordinate instanceof Cylindrical3D)
            return (Cylindrical3D)coordinate;

        Vector3D<Q> vector = coordinate.toVector3D();
        double x = vector.getValue(Vector3D.X);
        double y = vector.getValue(Vector3D.Y);

        double rho = Math.sqrt(x * x + y * y);
        double azim = Math.atan2(y, x);

        Cylindrical3D<Q> V = Cylindrical3D.newInstance();
        V._radius = Parameter.valueOf(rho, vector.getUnit());
        V._azimuth = Parameter.valueOf(azim, SI.RADIAN);
        V._height = vector.get(Vector3D.Z);

        return V;
    }

    /**
     * Returns a {@link Cylindrical3D} instance containing the cylindrical coordinate
     * representation of the specified {@link Coordinate3D coordinate}.
     *
     * @param <Q>       the Quantity (unit type, e.g. Length or Volume) of this vector.
     * @param coord     the coordinate to convert.
     * @param angleUnit the unit to use for the azimuth component.
     * @return the cylindrical coordinate vector having the specified values.
     */
    public static <Q extends Quantity> Cylindrical3D<Q> valueOf(Coordinate3D<Q> coord, Unit<Angle> angleUnit) {
        Cylindrical3D<Q> V = Cylindrical3D.valueOf(coord);
        V._azimuth = V._azimuth.to(angleUnit);
        return V;
    }

    /**
     * Return the specified {@link Vector3D} object as a <code>Cylindrical3D</code>
     * instance.
     *
     * @param vector The Vector3D object to be converted to a Cylindrical3D.
     * @return A Cylindrical3D instance that is equivalent to the supplied Vector3D
     *         object.
     */
    @Override
    public Cylindrical3D<Q> fromVector3D(Vector3D<Q> vector) {
        return Cylindrical3D.valueOf(vector, _azimuth.getUnit());
    }

    /**
     * Returns the value of a Parameter from this vector. The dimensions are defined as:
     * radius, azimuth angle, and height or altitude in that order.
     *
     * @param i the dimension index (0=radius, 1=azimuth, 2=height/altitude).
     * @return the value of the parameter at <code>i</code>.
     * @throws IndexOutOfBoundsException <code>(i &lt; 0) || (i > dimension()-1)</code>
     */
    @Override
    public Parameter get(int i) {
        Parameter value = null;
        switch (i) {
            case 0:
                value = _radius;
                break;
            case 1:
                value = _azimuth;
                break;
            case 2:
                value = _height;
                break;
            default:
                throw new IndexOutOfBoundsException(
                        MessageFormat.format(RESOURCES.getString("p3dIndexErr"), i));
        }
        return value;
    }

    /**
     * Returns the radius element of this vector as a <code>double</code>, stated in this
     * vector's {@link #getUnit() unit}.
     *
     * @return the radius element of this vector in this vector's units.
     */
    public double getRadiusValue() {
        return _radius.getValue();
    }

    /**
     * Returns the radius element of this vector as a {@link Parameter}.
     *
     * @return the radius element of this vector.
     */
    public Parameter<Q> getRadius() {
        return _radius.pow(2).times(_height.pow(2)).sqrt();
    }

    /**
     * Returns the azimuth angle of this vector as a {@link Parameter}.
     *
     * @return the azimuth angle of this vector.
     */
    public Parameter<Angle> getAzimuth() {
        return _azimuth;
    }

    /**
     * Returns the height or altitude element of this vector as a {@link Parameter}.
     *
     * @return the height element of this vector.
     */
    public Parameter<Q> getHeight() {
        return _height;
    }

    /**
     * Returns the Euclidian norm, magnitude, or length of this vector (square root of the
     * dot product of this vector and itself).
     *
     * @return <code>sqrt(this · this)</code>.
     */
    @Override
    public Parameter<Q> norm() {
        return _radius.pow(2).times(_height.pow(2)).sqrt();
    }

    /**
     * Returns the {@link #norm() norm}, magnitude, or length value of this vector.
     *
     * @return <code>this.norm().getValue()</code>.
     */
    @Override
    public double normValue() {
        double rho = _radius.getValue();
        double h = _height.getValue();
        return Math.sqrt(rho * rho + h * h);
    }

    /**
     * Returns the negation of this vector.
     *
     * @return <code>-this</code>.
     */
    @Override
    public Cylindrical3D<Q> opposite() {

        //  Convert to cartesian coordinates, negate that, and then convert back to polar.
        Vector3D<Q> V3D = toVector3D().opposite();
        Cylindrical3D<Q> V = fromVector3D(V3D);

        return V;
    }

    /**
     * Returns the sum of this vector with the one specified. The units of the output
     * vector will be the units of this vector.
     *
     * @param that the vector to be added.
     * @return <code>this + that</code>.
     * @throws DimensionException if vector dimensions are different.
     */
    @Override
    public Cylindrical3D<Q> plus(Vector<Parameter<Q>> that) {

        //  Convert to Cartesian coordinates, add those, then convert back.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D<Q> T = ((Coordinate3D<Q>)that).toVector3D();
            Vector3D<Q> V3D = toVector3D().plus(T);
            Cylindrical3D<Q> V = fromVector3D(V3D);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the sum of this vector with the parameter specified. The unit of the output
     * vector will be the units of this vector.
     *
     * @param that the parameter to be added to this vector.
     * @return <code>this + that</code>.
     */
    @Override
    public Cylindrical3D<Q> plus(Parameter<Q> that) {

        StackContext.enter();
        try {
            Vector3D<Q> V3D = toVector3D().plus(that);
            Cylindrical3D<Q> V = fromVector3D(V3D);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }

    }

    /**
     * Returns the difference between this vector and the one specified.
     *
     * @param that the vector to be subtracted.
     * @return <code>this - that</code>.
     */
    @Override
    public Cylindrical3D<Q> minus(Vector<Parameter<Q>> that) {

        //  Convert to Cartesian coordinates, subtract those, then convert back.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D<Q> T = ((Coordinate3D<Q>)that).toVector3D();
            Vector3D<Q> V3D = toVector3D().minus(T);
            Cylindrical3D<Q> V = fromVector3D(V3D);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Subtracts the supplied Parameter from this vector's and returns the result. The
     * unit of the output vector will be the units of this vector.
     *
     * @param that the Parameter to be subtracted from this vector.
     * @return <code>this - that</code>.
     */
    @Override
    public Cylindrical3D<Q> minus(Parameter<Q> that) {

        StackContext.enter();
        try {
            Vector3D<Q> V3D = toVector3D().minus(that);
            Cylindrical3D<Q> V = fromVector3D(V3D);

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the product of this vector with the specified coefficient. The magnitude of
     * this vector is scaled by the input coefficient and the direction is left unchanged.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Cylindrical3D times(Parameter k) {
        Cylindrical3D V = Cylindrical3D.newInstance();
        V._radius = _radius.times(k);
        V._azimuth = _azimuth;
        V._height = _height.times(k);
        return V;
    }

    /**
     * Returns the product of this vector with the specified coefficient. The magnitude of
     * this vector is scaled by the input coefficient and the direction is left unchanged.
     *
     * @param k the coefficient multiplier.
     * @return <code>this · k</code>
     */
    @Override
    public Cylindrical3D<Q> times(double k) {
        Cylindrical3D<Q> V = Cylindrical3D.newInstance();
        V._radius = _radius.times(k);
        V._azimuth = _azimuth;
        V._height = _height.times(k);
        return V;
    }

    /**
     * Returns the dot product of this vector with the one specified.
     *
     * @param that the vector multiplier.
     * @return <code>this · that</code>
     * @throws DimensionException if <code>this.dimension() != that.dimension()</code>
     * @see <a href="http://en.wikipedia.org/wiki/Dot_product">
     * Wikipedia: Dot Product</a>
     */
    @Override
    public Parameter times(Vector that) {

        //  Convert to Cartesian coordinates and multiply that.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D T = ((Coordinate3D)that).toVector3D();
            Parameter product = toVector3D().times(T);

            return StackContext.outerCopy(product);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns the result of this vector divided by the specified divisor. The magnitude
     * of this vector is divided by the input coefficient and the direction is left
     * unchanged.
     *
     * @param that the divisor.
     * @return <code>this / that</code>.
     */
    public Cylindrical3D<?> divide(Parameter<?> that) {
        return (Cylindrical3D<?>)times(that.inverse());
    }

    /**
     * Returns the cross product of two 3-dimensional vectors.
     *
     * @param that the vector multiplier.
     * @return <code>this x that</code>
     * @throws DimensionException if <code>(that.getDimension() != 3)</code>
     * @see <a href="http://en.wikipedia.org/wiki/Cross_product">
     * Wikipedia: Cross Product</a>
     */
    @Override
    public Cylindrical3D cross(Vector that) {

        //  Convert to Cartesian coordinates, multiply those, then convert back.
        if (!(that instanceof Coordinate3D))
            throw new ClassCastException(RESOURCES.getString("notCoordinate3D"));

        StackContext.enter();
        try {
            Vector3D<?> T = ((Coordinate3D<?>)that).toVector3D();
            Vector3D<?> V3D = toVector3D().cross(T);
            Cylindrical3D<?> V = Cylindrical3D.valueOf(V3D, _azimuth.getUnit());

            return StackContext.outerCopy(V);
        } finally {
            StackContext.exit();
        }
    }

    /**
     * Returns this vector converted to a unit vector with a vector magnitude of 1.0.
     *
     * @return this vector converted to a unit vector
     */
    public Cylindrical3D<Dimensionless> toUnitVector() {
        double magnitude = this.normValue();
        if (this.getUnit().equals(Dimensionless.UNIT) && MathTools.isApproxEqual(magnitude, 1.0))
            return (Cylindrical3D<Dimensionless>)this;

        Cylindrical3D<Dimensionless> V = Cylindrical3D.newInstance();
        V._azimuth = this._azimuth;
        V._height = (Parameter<Dimensionless>)this._height.divide(magnitude);
        V._radius = (Parameter<Dimensionless>)this._radius.divide(magnitude);
        
        return V;
    }

    /**
     * Returns a copy of this vector {@link javolution.context.AllocatorContext allocated}
     * by the calling thread (possibly on the stack).
     *
     * @return an identical and independent copy of this vector.
     */
    @Override
    public Cylindrical3D<Q> copy() {
        return copyOf(this);
    }

    /**
     * Returns the unit in which the radius and height in this vector are stated in.
     *
     * @return the unit in which the radius and height in this vector are stated in
     */
    @Override
    public Unit<Q> getUnit() {
        return _radius.getUnit();
    }

    /**
     * Returns the unit in which the azimuth angle in this vector is stated in.
     *
     * @return the unit in which the azimuth angle in this vector is stated in
     */
    public Unit<Angle> getAngleUnit() {
        return _azimuth.getUnit();
    }

    /**
     * Returns the equivalent to this vector but with the radius and height stated in the
     * specified unit.
     *
     * @param <R>  the Quantity (physical unit) type of the returned vector.
     * @param unit the unit of the radius and height of the vector to be returned.
     * @return a vector equivalent to this vector but with the radius and height stated in
     *         the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public <R extends Quantity> Cylindrical3D<R> to(Unit<R> unit) throws ConversionException {
        Unit<?> thisUnit = getUnit();
        if ((thisUnit == unit) || thisUnit.equals(unit))
            return (Cylindrical3D<R>)this;
        Cylindrical3D<R> result = Cylindrical3D.newInstance();
        result._radius = _radius.to(unit);
        result._azimuth = _azimuth;
        result._height = _height.to(unit);
        return result;
    }

    /**
     * Returns the equivalent to this vector but with the azimuth angle stated in the
     * specified unit.
     *
     * @param unit the angle unit of the azimuth of the vector to be returned.
     * @return a coordinate equivalent to this vector but with the azimuth stated in the
     *         specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public Cylindrical3D<Q> angleTo(Unit<Angle> unit) {
        Unit<Angle> thisUnit = _azimuth.getUnit();
        if ((thisUnit == unit) || thisUnit.equals(unit))
            return this;
        Cylindrical3D<Q> result = Cylindrical3D.newInstance();
        result._radius = _radius;
        result._azimuth = _azimuth.to(unit);
        result._height = _height;
        return result;
    }

    /**
     * Casts this Cylindrical3D to a parameterized unit of specified nature or throw a
     * <code>ClassCastException</code> if the dimension of the specified quantity and this
     * parameter's unit dimension do not match.
     *
     * @param <T>  the Quantity (physical unit) type of the returned vector.
     * @param type the quantity class identifying the nature of the unit.
     * @return this Cylindrical3D parameterized with the specified type.
     * @throws ClassCastException if the dimension of this parameter's unit is different
     * from the specified quantity dimension.
     * @throws UnsupportedOperationException if the specified quantity class does not have
     * a public static field named "UNIT" holding the standard unit for the quantity.
     */
    @Override
    public <T extends Quantity> Cylindrical3D<T> asType(Class<T> type) throws ClassCastException {
        Unit<T> u = getUnit().asType(type); //  If no exception is thrown, the cast is valid.
        return (Cylindrical3D<T>)this;
    }

    /**
     * Returns a Cartesian Vector3D representation of this vector.
     * <p>
     * The polar to Cartesian transformation is defined by:
     * <pre>
     *    |x|   | radius*cos(azimuth)  |
     *    |y| = | radius*sin(azimuth)  |
     *    |z|   |      height          |
     * </pre></p>
     *
     * @return a Cartesian Vector3D representation of this vector
     */
    @Override
    public Vector3D<Q> toVector3D() {

        double radius = _radius.getValue();
        double azim = _azimuth.to(SI.RADIAN).getValue();
        double z = _height.getValue();

        double x = radius * cos(azim);
        double y = radius * sin(azim);

        Vector3D<Q> V = Vector3D.valueOf(x, y, z, getUnit());

        return V;
    }

    /**
     * Compares this Cylindrical3D against the specified object for strict equality (same
     * values and same units).
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this vector is identical to that vector;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        Cylindrical3D<?> that = (Cylindrical3D<?>)obj;
        if (!this._radius.equals(that._radius))
            return false;
        if (!this._azimuth.equals(that._azimuth))
            return false;

        return this._height.equals(that._height);
    }

    /**
     * Returns the hash code for this parameter.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        int var_code = _radius.hashCode();
        hash = hash * 31 + var_code;

        var_code = _azimuth.hashCode();
        hash = hash * 31 + var_code;

        var_code = _height.hashCode();
        hash = hash * 31 + var_code;

        return hash;
    }

    /**
     * Holds the default XML representation. For example:
     * <pre>
     *    &lt;Radius value="1.73205080756888" unit="m"/&gt;
     *    &lt;Azimuth value="45.0" unit="°" /&gt;
     *    &lt;Height value="35.2643896827547" unit="m" /&gt;
     * </pre>
     */
    protected static final XMLFormat<Cylindrical3D> XML = new XMLFormat<Cylindrical3D>(Cylindrical3D.class) {

        @Override
        public Cylindrical3D<?> newInstance(Class<Cylindrical3D> cls, InputElement xml) throws XMLStreamException {
            return FACTORY.object();
        }

        @Override
        public void read(InputElement xml, Cylindrical3D V) throws XMLStreamException {
            V._radius = xml.get("Radius", Parameter.class);
            V._azimuth = xml.get("Azimuth", Parameter.class);
            V._height = xml.get("Height", Parameter.class);
        }

        @Override
        public void write(Cylindrical3D V, OutputElement xml) throws XMLStreamException {
            xml.add(V._radius, "Radius", Parameter.class);
            xml.add(V._azimuth, "Azimuth", Parameter.class);
            xml.add(V._height, "Height", Parameter.class);
        }
    };

    ///////////////////////
    // Factory creation. //
    ///////////////////////
    private Cylindrical3D() {
    }

    private static final ObjectFactory<Cylindrical3D<? extends Quantity>> FACTORY = new ObjectFactory<Cylindrical3D<? extends Quantity>>() {
        @Override
        protected Cylindrical3D<? extends Quantity> create() {
            return new Cylindrical3D();
        }
    };

    private static <Q extends Quantity> Cylindrical3D<Q> newInstance() {
        Cylindrical3D<Q> measure = (Cylindrical3D<Q>)FACTORY.object();
        return measure;
    }

    private static <Q extends Quantity> Cylindrical3D<Q> copyOf(Cylindrical3D<Q> original) {
        Cylindrical3D<Q> measure = Cylindrical3D.newInstance();
        measure._radius = original._radius.copy();
        measure._azimuth = original._azimuth.copy();
        measure._height = original._height.copy();
        return measure;
    }

}
