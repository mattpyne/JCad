/**
 * UnitSet -- A container for self-consistent and often coherent unit sets.
 *
 * Copyright (C) 2015-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.unit;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import javax.measure.converter.ConversionException;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.*;
import javax.measure.unit.NonSI;
import static javax.measure.unit.NonSI.*;
import static javax.measure.unit.SI.*;
import javax.measure.unit.Unit;
import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

/**
 * A class that contains a set of units (generally made self-consistent if not coherent)
 * and also provides lists (sets) of related units.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: July 12, 2008
 * @version March 19, 2017
 */
public final class UnitSet implements Cloneable {

    /**
     * The type of unit system being used.
     */
    public enum UnitSystem {

        /**
         * Constant used to indicate that the coherent SI units should be used.
         */
        SI_MKS,
        /**
         * Constant used to indicate that the coherent metric (cm-gram-sec) units should be used.
         */
        CGS,
        /**
         * Constant used to indicate that coherent US Customary units (ft-slug-sec) should be
         * used.
         */
        US_FSS,
        /**
         * Constant used to indicate that coherent US Customary units (ft-lbm-sec) should be
         * used.
         */
        US_FPS,
        /**
         * Constant used to indicate that a custom set of units is being used which
         * may or may <em>not</em> be coherent.
         */
        CUSTOM
    }

    /**
     * The different unit set types in this collection.
     */
    public enum SetType {

        TIME, LENGTH, MASS, ANGLE, AREA, VOLUME, VELOCITY, ACCELERATION, FORCE, INERTIA, MASS_DENSITY,
        ANGULAR_VELOCITY, TORQUE
    }

    /**
     * The angular unit "degreeU".
     */
    public static final Unit DEG = DEGREE_ANGLE;

    /**
     * The angular unit "radian".
     */
    public static final Unit RAD = Angle.UNIT;

    private static final Unit<Acceleration> FPS2 = FOOT.divide(SECOND.pow(2)).asType(Acceleration.class);
    
    //  A collection of unit sets (arrays of related and compatible units).
    private static final HashMap<SetType, Unit[]> sets = new HashMap();

    static {
        Unit<Length> CM = CENTIMETER;
        Unit<Length> MM = MILLIMETER;
        Unit MM2 = MM.pow(2);
        Unit CM2 = CM.pow(2);
        Unit M2 = SQUARE_METRE;
        Unit IN2 = INCH.pow(2);
        Unit FT2 = FOOT.pow(2);
        Unit MM3 = MM2.times(MM);
        Unit CM3 = CM2.times(CM);
        Unit M3 = CUBIC_METRE;
        Unit IN3 = CUBIC_INCH;
        Unit FT3 = FT2.times(FOOT);

        {
            Unit[] unitArr = {SECOND, MINUTE, HOUR, DAY};
            sets.put(SetType.TIME, unitArr);
        }
        {
            Unit[] unitArr = {MM, CM, METER, KILOMETER,
                    INCH, FOOT, NAUTICAL_MILE, MILE};
            sets.put(SetType.LENGTH, unitArr);
        }
        {
            Unit[] unitArr = {GRAM, KILOGRAM, METRIC_TON, OUNCE, POUND, SLUG, TON_US};
            sets.put(SetType.MASS, unitArr);
        }
        {
            Unit[] unitArr = {RADIAN, DEGREE_ANGLE, REVOLUTION, SECOND_ANGLE, MINUTE_ANGLE };
            sets.put(SetType.ANGLE, unitArr);
        }
        
        {
            Unit[] unitArr = {MM2, CM2, M2, HECTARE, KILOMETER.pow(2),
                    IN2, FT2, MILE.pow(2)};
            sets.put(SetType.AREA, unitArr);
        }
        {
            Unit[] unitArr = {MM3, CM3, LITER, M3, KILOMETER.pow(3),
                    IN3, FT3, MILE.pow(3)};
            sets.put(SetType.VOLUME, unitArr);
        }
        
        {
            Unit[] unitArr = {MM.divide(SECOND), CM.divide(SECOND), METERS_PER_SECOND, KILOMETERS_PER_HOUR,
                INCH.divide(SECOND), FEET_PER_SECOND, KNOT, MILES_PER_HOUR};
            sets.put(SetType.VELOCITY, unitArr);
        }
        {
            Unit[] unitArr = {MM.divide(SECOND.pow(2)), CM.divide(SECOND.pow(2)), METERS_PER_SQUARE_SECOND,
                INCH.divide(SECOND.pow(2)), FPS2, NonSI.G};
            sets.put(SetType.ACCELERATION, unitArr);
        }
        
        {
            Unit[] unitArr = {DYNE, NEWTON, KILOGRAM_FORCE, POUND_FORCE, POUNDAL};
            sets.put(SetType.FORCE, unitArr);
        }
       
        {
            Unit[] unitArr = {KILOGRAM.times(CM2), KILOGRAM.times(M2),
                    POUND.times(IN2), POUND.times(FT2), SLUG.times(FT2)};
            sets.put(SetType.INERTIA, unitArr);
        }
        {
            Unit[] unitArr = {KILOGRAM.divide(CM3), KILOGRAM.divide(M3),
                    POUND.divide(IN3), POUND.divide(FT3), SLUG.divide(IN3), SLUG.divide(FT3)};
            sets.put(SetType.MASS_DENSITY, unitArr);
        }
        {
            Unit[] unitArr = {RADIAN.divide(SECOND), DEGREE_ANGLE.divide(SECOND), REVOLUTION.divide(SECOND)};
            sets.put(SetType.ANGULAR_VELOCITY, unitArr);
        }
        {
            Unit[] unitArr = {NEWTON.times(CM), Torque.UNIT, POUND_FORCE.times(INCH), POUND_FORCE.times(FOOT)};
            sets.put(SetType.TORQUE, unitArr);
        }
    }

    //  The type code for this unit set.
    private UnitSystem type = UnitSystem.SI_MKS;
    
    //  Flag indicating if the unit set is coherent or not.
    private boolean isCoherent = false;
    
    //  Flag indicating if the unit set is consistent or not.
    private boolean isConsistent = false;

    //  The various units in this unit set.
    private Unit<Duration> time;
    private Unit<Length> length;
    private Unit<Mass> mass;
    private Unit<Angle> angle;
    
    //  Derived units.
    private Unit<Area> area;
    private Unit<Volume> volume;
    private Unit<Velocity> velocity;
    private Unit<Acceleration> accel;
    private Unit<Force> force;
    private Unit<VolumetricDensity> massDensity;
    private Unit<Inertia> inertia;
    private Unit<AngularVelocity> angularVelocity;
    private Unit<Torque> torque;
    

    /**
     * Construct a coherent set of units based on the system identification provided.
     *
     * @param system The Unit System type.
     */
    public UnitSet(UnitSystem system) {
        //  Define the fundamental units.
        time = SECOND;
        angle = DEG;

        switch (system) {
            case US_FSS:
                length = FOOT;
                mass = SLUG;
                break;
            
            case US_FPS:
                length = FOOT;
                mass = POUND;
                break;

            case CGS:
                length = CENTIMETER;
                mass = GRAM;
                break;
                
            case SI_MKS:
            case CUSTOM:
                length = METER;
                mass = KILOGRAM;
                break;
        }
        this.type = system;
        this.isCoherent = true;

        //  Create a consistent derived unit set.
        makeConsistent();

    }

    /**
     * Method that returns the Unit System for this unit set.
     * 
     * @return The UnitSystem for this unit set.
     */
    public UnitSystem getSystem() {
        return type;
    }

    /**
     * Returns an array of units of the specified type (array of related units).
     *
     * @param type The unit type to return an array of units for.
     * @return The unit set array requested
     */
    public static Unit[] getSet(SetType type) {
        return sets.get(type);
    }

    /**
     * Return <code>true</code> if this unit set is coherent. Coherent means that the
     * derived units in this set are a product of powers of base units with no other
     * proportionality factor than one.
     *
     * @return true if this unit set is coherent.
     * @see #isConsistent() 
     */
    public boolean isCoherent() {
        return isCoherent;
    }

    /**
     * Return <code>true</code> if this unit set is consistent. Consistent means that the
     * derived units are made up of powers of the base units though there may be
     * conversion factors involved.
     *
     * @return true if this unit set is consistent.
     * @see #makeConsistent()
     * @see #isCoherent()
     */
    public boolean isConsistent() {
        return isConsistent;
    }

    /**
     * Makes this unit set consistent by deriving a complete set of units for this
     * application based on the fundamental or base units of time, length, mass and angle.
     * Consistent means that the derived units in this set are all derived from the base
     * units.
     *
     * @see #isConsistent()
     * @see #isCoherent()
     */
    public void makeConsistent() {

        area = length.pow(2).asType(Area.class);
        volume = length.pow(3).asType(Volume.class);
        velocity = length.divide(time).asType(Velocity.class);
        accel = length.divide(time.pow(2)).asType(Acceleration.class);
        
        if (equivUnit(mass, KILOGRAM) && equivUnit(accel, METERS_PER_SQUARE_SECOND))
            force = NEWTON;
        else if (equivUnit(mass, KILOGRAM) && equivUnit(accel, G))
            force = KILOGRAM_FORCE;
        else if (equivUnit(mass, SLUG) && equivUnit(accel, FPS2))
            force = POUND_FORCE;
        else if (equivUnit(mass, POUND) && equivUnit(accel, FPS2))
            force = POUNDAL;
        else if (equivUnit(mass, POUND) && equivUnit(accel, G))
            force = POUND_FORCE;
        else
            force = mass.times(accel).asType(Force.class);
        
        massDensity = mass.divide(volume).asType(VolumetricDensity.class);
        inertia = mass.times(area).asType(Inertia.class);
        
        angularVelocity = angle.divide(time).asType(AngularVelocity.class);
        torque = force.times(length).asType(Torque.class);
        
        //  The derived units are not consistent.
        isConsistent = true;

        //  Make sure the newly derived units are in the unit sets.
        addUnitsToSets();

        //  Does this represent one of the standard coherent unit systems?
        if (time.equals(SECOND)) {
            if (length.equals(METER) && mass.equals(KILOGRAM)) {
                type = UnitSystem.SI_MKS;
                isCoherent = true;
            } else if (length.equals(CENTIMETER) && mass.equals(GRAM)) {
                type = UnitSystem.CGS;
                isCoherent = true;
            } else if (length.equals(FOOT)) {
                if (mass.equals(SLUG)) {
                    type = UnitSystem.US_FSS;
                    isCoherent = true;
                } else if (mass.equals(POUND)) {
                    type = UnitSystem.US_FPS;
                    isCoherent = true;
                }
            }
        }
        
    }
    
    /**
     * Return true if two units are equivalent to one another.  Equivalent means that
     * the conversion from unit1 to unit 2 is an identity (1.0).
     * 
     * @param unit1 The 1st unit to compare.
     * @param unit2 The 2nd unit to compare.
     * @return true if no conversion is required between unit1 and unit2.
     */
    private boolean equivUnit(Unit unit1, Unit unit2) {
        try {
            UnitConverter cvt = unit1.getConverterTo(unit2);
            return cvt.equals(UnitConverter.IDENTITY);
            
        } catch (ConversionException ignore) {
            return false;
        }
    }

    /**
     * Method that ensures that all the current units are in the unit sets.
     */
    private void addUnitsToSets() {
        //  Time
        Unit[] set = getSet(SetType.TIME);
        if (!unitInSet(set, time))
            addUnitToSet(set, time);

        //  Mass
        set = getSet(SetType.MASS);
        if (!unitInSet(set, mass))
            addUnitToSet(set, mass);

        //  Length
        set = getSet(SetType.LENGTH);
        if (!unitInSet(set, length))
            addUnitToSet(set, length);

        //  Angle
        set = getSet(SetType.ANGLE);
        if (!unitInSet(set, angle))
            addUnitToSet(set, angle);

        
        //  Area
        set = getSet(SetType.AREA);
        if (!unitInSet(set, area))
            addUnitToSet(set, area);
        
        //  Volume
        set = getSet(SetType.VOLUME);
        if (!unitInSet(set, volume))
            addUnitToSet(set, volume);
        
        //  Velocity
        set = getSet(SetType.VELOCITY);
        if (!unitInSet(set, velocity))
            addUnitToSet(set, velocity);
        
        //  Acceleration
        set = getSet(SetType.ACCELERATION);
        if (!unitInSet(set, accel))
            addUnitToSet(set, accel);
        
        //  Force
        set = getSet(SetType.FORCE);
        if (!unitInSet(set, force))
            addUnitToSet(set, force);
        
        //  Mass density
        set = getSet(SetType.MASS_DENSITY);
        if (!unitInSet(set, massDensity))
            addUnitToSet(set, massDensity);

        //  Inertia
        set = getSet(SetType.INERTIA);
        if (!unitInSet(set, inertia))
            addUnitToSet(set, inertia);

        //  Angular velocity
        set = getSet(SetType.ANGULAR_VELOCITY);
        if (!unitInSet(set, angularVelocity))
            addUnitToSet(set, angularVelocity);
        
        //  Torque
        set = getSet(SetType.TORQUE);
        if (!unitInSet(set, torque))
            addUnitToSet(set, torque);
        
    }

    /**
     * Returns true if the specified unit is in the given array of units.
     */
    private boolean unitInSet(Unit[] set, Unit unit) {
        int size = set.length;
        for (int i = 0; i < size; ++i)
            if (set[i].equals(unit))
                return true;
        return false;
    }

    /**
     * Method that adds the specified unit to the specified array of units by creating a
     * new array and adding the unit to the end of it.
     */
    private Unit[] addUnitToSet(Unit[] set, Unit unit) {
        int size = set.length;
        Unit[] newArray = new Unit[size + 1];
        System.arraycopy(set, 0, newArray, 0, size);
        newArray[size] = unit;
        return newArray;
    }

    /**
     * Return the duration of time units for this unit set.
     * 
     * @return The units of durations of time.
     */
    public Unit<Duration> time() {
        return time;
    }

    /**
     * Method used to set the time units in this unit set. All units that have a
     * time/duration component will be changed to use this time value after a call to
     * "makeCoherent()".
     *
     * @param unit the time unit to set this unit set to.
     * @throws ConversionException if the provided unit is not compatible with time
     * (seconds).
     * @see #makeConsistent()
     */
    public void setTime(Unit<Duration> unit) throws ConversionException {
        if (!unit.isCompatible(time))
            throw new ConversionException("Time units must be compatible with seconds. Input unit = " + unit);
        if (!unit.equals(time)) {
            Unit old = time;
            time = unit;
            isConsistent = false;
            if (!time.equals(SECOND)) {
                isCoherent = false;
                type = UnitSystem.CUSTOM;
            }
            propertyChange.firePropertyChange("Time", old, unit);
        }
    }

    /**
     * Return the length units for this unit set.
     * 
     * @return The length units.
     */
    public Unit<Length> length() {
        return length;
    }

    /**
     * Method used to set the length units in this unit set. All units that have a length
     * component will be changed to use this length value after a call to "makeCoherent()".
     *
     * @param unit the length unit to set this unit set to.
     * @throws ConversionException if the provided unit is not compatible with length
     * (meters).
     * @see #makeConsistent()
     */
    public void setLength(Unit<Length> unit) throws ConversionException {
        if (!unit.isCompatible(length))
            throw new ConversionException("Length units must be compatible with meters. Input unit = " + unit);
        if (!unit.equals(length)) {
            Unit old = length;
            type = UnitSystem.CUSTOM;
            isConsistent = false;
            isCoherent = false;
            length = unit;
            propertyChange.firePropertyChange("Length", old, unit);
        }
    }

    /**
     * Return the mass units for this unit set.
     * 
     * @return The mass units.
     */
    public Unit<Mass> mass() {
        return mass;
    }

    /**
     * Method used to set the mass units in this unit set. All units that have a mass
     * component will be changed to use this mass value after a call to "makeCoherent()".
     *
     * @param unit the mass unit to set this unit set to.
     * @throws ConversionException if the provided unit is not compatible with mass (kg).
     * @see #makeConsistent()
     */
    public void setMass(Unit<Mass> unit) throws ConversionException {
        if (!unit.isCompatible(mass))
            throw new ConversionException("Mass units must be compatible with kilograms. Input unit = " + unit);
        if (!unit.equals(mass)) {
            Unit old = mass;
            type = UnitSystem.CUSTOM;
            isConsistent = false;
            isCoherent = false;
            mass = unit;
            propertyChange.firePropertyChange("Mass", old, unit);
        }
    }

    /**
     * Return the angular measure units for this unit set.
     * 
     * @return The angular measure units.
     */
    public Unit<Angle> angle() {
        return angle;
    }

    /**
     * Method used to set the angular measure units in this unit set. All units that have an angular measure
     * component will be changed to use this value after a call to "makeCoherent()".
     *
     * @param unit the angle unit to set this unit set to.
     * @throws ConversionException if the provided unit is not compatible with angles (radian).
     * @see #makeConsistent()
     */
    public void setAngle(Unit<Angle> unit) throws ConversionException {
        if (!unit.isCompatible(angle))
            throw new ConversionException("Angle units must be compatible with radians. Input unit = " + unit);
        if (!unit.equals(angle)) {
            Unit old = angle;
            angle = unit;
            isConsistent = false;
            propertyChange.firePropertyChange("Angle", old, unit);
        }
    }

    /**
     * Return the area units.
     * 
     * @return Area units
     */
    public Unit<Area> area() {
        return area;
    }

    /**
     * Return the volume units.
     * 
     * @return Volume units
     */
    public Unit<Volume> volume() {
        return volume;
    }

    /**
     * Return the velocity units.
     * 
     * @return Velocity units
     */
    public Unit<Velocity> velocity() {
        return velocity;
    }
    
    /**
     * Return the acceleration units.
     * 
     * @return Acceleration units
     */
    public Unit<Acceleration> acceleration() {
        return accel;
    }
    
    /**
     * Return the force units.
     * 
     * @return Force units
     */
    public Unit<Force> force() {
        return force;
    }
    
    /**
     * Return the inertia units.
     * 
     * @return Inertia units
     */
    public Unit<Inertia> inertia() {
        return inertia;
    }

    /**
     * Return the mass density units.
     * 
     * @return Mass density units.
     */
    public Unit<VolumetricDensity> massDensity() {
        return massDensity;
    }

    /**
     * Return the angular velocity units.
     * 
     * @return Angular velocity units.
     */
    public Unit<AngularVelocity> angularVelocity() {
        return angularVelocity;
    }
    
    /**
     * Return the torque units.
     * 
     * @return The torque units.
     */
    public Unit<Torque> torque() {
        return torque;
    }
    
    /**
     * Creates and returns a copy of this object.
     * 
     * @return A clone of this UnitSet.
     */
    @Override
    public Object clone() {
        Object result = null;

        try {
            // Make a shallow copy.
            result = super.clone();

        } catch (Exception e) {
            // Can't happen if this object implements Cloneable.
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Compares this UnitSet against the specified object for strict equality.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this UnitSet is identical to that object;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        UnitSet that = (UnitSet)obj;
        if (type != that.type)
            return false;
        if (!time.equals(that.time))
            return false;
        if (!angle.equals(that.angle))
            return false;
        if (!length.equals(that.length))
            return false;
        if (!mass.equals(that.mass))
            return false;
        if (!area.equals(that.area))
            return false;
        if (!volume.equals(that.volume))
            return false;
        if (!velocity.equals(that.velocity))
            return false;
        if (!accel.equals(that.accel))
            return false;
        if (!massDensity.equals(that.massDensity))
            return false;
        if (!inertia.equals(that.inertia))
            return false;
        if (!angularVelocity.equals(that.angularVelocity))
            return false;
        
        return torque.equals(that.torque);
    }

    /**
     * Returns the hash code for this UnitSet.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.type.hashCode();
        hash = 53 * hash + this.time.hashCode();
        hash = 53 * hash + this.length.hashCode();
        hash = 53 * hash + this.mass.hashCode();
        hash = 53 * hash + this.angle.hashCode();
        
        hash = 53 * hash + this.area.hashCode();
        hash = 53 * hash + this.volume.hashCode();
        hash = 53 * hash + this.velocity.hashCode();
        hash = 53 * hash + this.accel.hashCode();
        hash = 53 * hash + this.massDensity.hashCode();
        hash = 53 * hash + this.inertia.hashCode();
        hash = 53 * hash + this.angularVelocity.hashCode();
        hash = 53 * hash + this.torque.hashCode();
        
        return hash;
    }
    
    /**
     * Use this object to notify uses of changes in the inputs to a body part.
     */
    private transient final PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    
    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        propertyChange.addPropertyChangeListener(l);
    }

    public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
        propertyChange.addPropertyChangeListener(propertyName, l);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
        propertyChange.removePropertyChangeListener(l);
    }

    public synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener l) {
        propertyChange.removePropertyChangeListener(propertyName, l);
    }

    public synchronized void clearPropertyChangeListeners() {
        PropertyChangeListener[] listeners = propertyChange.getPropertyChangeListeners();
        for (PropertyChangeListener l : listeners) {
            this.removePropertyChangeListener(l);
        }
    }
    
	/**
 	* Holds the default XML representation for this class.
 	*/
	protected static final XMLFormat<UnitSet> XML = new XMLFormat<UnitSet>(UnitSet.class) {

		@Override
		public UnitSet newInstance(Class<UnitSet> cls, XMLFormat.InputElement xml) throws XMLStreamException {
            
            //  Parse out the unit system type.
            UnitSystem[] systems = UnitSystem.values();
			int typeIdx = xml.getAttribute("type", 0);
            UnitSystem type = systems[typeIdx];
            
            //  Create the new unit set.
            UnitSet db = new UnitSet(type);
            
            //  Parse out the units.
            boolean isConsistent = xml.getAttribute("isConsistent", true);
            Unit time = Unit.valueOf(xml.getAttribute("time", db.time.toString()));
            Unit length = Unit.valueOf(xml.getAttribute("length", db.length.toString()));
            Unit mass = Unit.valueOf(xml.getAttribute("mass", db.mass.toString()));
            Unit angle = Unit.valueOf(xml.getAttribute("angle", db.angle.toString()));
            db.setTime(time);
            db.setLength(length);
            db.setMass(mass);
            db.setAngle(angle);
            
            if (isConsistent)
                db.makeConsistent();
            
            return db;
        }
        
		@Override
		public void read(XMLFormat.InputElement xml, UnitSet db) throws XMLStreamException {
            //  Nothing to do.  Already read everything in in "newInstance".
		}

		@Override
		public void write(UnitSet db, XMLFormat.OutputElement xml) throws XMLStreamException {
			xml.setAttribute("type", db.type.ordinal());
            xml.setAttribute("isConsistent", db.isConsistent);
            xml.setAttribute("time", db.time.toString());
            xml.setAttribute("length", db.length.toString());
            xml.setAttribute("mass", db.mass.toString());
            xml.setAttribute("angle", db.angle.toString());
        }
	};
	
}
