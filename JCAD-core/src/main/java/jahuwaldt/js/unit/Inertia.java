/*
 *  Inertia  -- The quantify of mass moment of inertia.
 *
 *  Copyright (C) 2015, Joseph A. Huwaldt.
 *  All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  Or visit:  http://www.gnu.org/licenses/gpl.html
 */
package jahuwaldt.js.unit;

import javax.measure.quantity.Quantity;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

/**
 * This interface represents the mass moment of inertia.
 * The system unit for this quantity is "kg*m^2" (kilogram meter squared).
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: October 23, 2015
 * @version October 29, 2015
 * @see <a href="https://en.wikipedia.org/wiki/Moment_of_inertia">
 *      Wikipedia: Moment of Inertia</a>
 */
public interface Inertia extends Quantity {

    /**
     * Holds the SI unit (Système International d'Unités) for this quantity.
     */
    @SuppressWarnings("unchecked")
    public final static Unit<Inertia> UNIT 
       = (Unit<Inertia>) SI.KILOGRAM.times(SI.METER.pow(2));

}