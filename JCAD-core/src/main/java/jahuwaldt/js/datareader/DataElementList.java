/**
 * DataElementList -- Interface in common to all data element lists.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.text.MessageFormat;
import java.util.AbstractList;
import javolution.lang.Reusable;
import javolution.text.Text;
import javolution.util.FastTable;

/**
 * A named list of {@link DataElement} objects with associated user data.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 15, 2015
 */
public abstract class DataElementList<E extends DataElement> extends AbstractList<E> implements DataElement, Reusable {

    //  The list behind this implementation.  We must do this rather than extend FastTable directly
    //  since methods that we need to override are final.
    private final FastTable<E> _list = FastTable.newInstance();

    /**
     * Name of this geometry element.
     */
    private CharSequence _name;

    //  Reference data for this element.
    private Object _userData;

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected DataElementList() { }

    /**
     * Returns the number of elements in this list. If the list contains more than
     * Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
     *
     * @return the number of elements in this list.
     */
    @Override
    public int size() {
        return _list.size();
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return.
     * @return the element at the specified position in this list.
     * @throws IndexOutOfBoundsException if the given index is out of range (index &lt; 0
     * || index &gt; size()-1)
     */
    @Override
    public E get(int index) {
        return _list.get(index);
    }

    /**
     * Replaces the {@link DataElement} at the specified position in this list with the
     * specified element. Null elements are ignored.
     *
     * @param index   The index of the element to replace.
     * @param element The element to be stored at the specified position.
     * @return The element previously at the specified position in this list.
     * @throws java.lang.IndexOutOfBoundsException - if (index &lt; 0) || (index &gt;
     * size()-1)
     */
    @Override
    public E set(int index, E element) {
        if (element == null)
            return null;

        return _list.set(index, element);
    }

    /**
     * Inserts the specified {@link DataElement} at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent elements
     * to the right (adds one to their indices). Null values are ignored.
     * <p>
     * Note: If this method is used, concurrent access must be synchronized (the table is
     * not thread-safe).
     * </p>
     *
     * @param index the index at which the specified element is to be inserted.
     * @param value the element to be inserted.
     * @throws IndexOutOfBoundsException if
     * <code>(index &lt; 0) || (index &gt; size()-1)</code>
     */
    @Override
    public void add(int index, E value) {
        if (value == null)
            return;
        _list.add(index, value);
    }

    /**
     * Appends the specified list of elements to the end of this list. Null elements are
     * ignored.
     *
     * @param elements Array of elements to be inserted.
     */
    public void addAll(E[] elements) {
        if (elements != null) {
            int size = elements.length;
            for (int i = 0; i < size; ++i) {
                E element = elements[i];
                this.add(element);
            }

        }
    }

    /**
     * Inserts the specified list of elements at the specified position in this list.
     * Shifts the elements currently at that position (if any) and any subsequent
     * parameters to the right (adds one to their indices) until all the elements have
     * been added.
     *
     * @param index    Index at which the specified list of elements is to be inserted.
     * @param elements List of elements to be inserted.
     */
    public void addAll(int index, E[] elements) {
        if (elements != null) {
            int size = elements.length;
            for (int i = 0; i < size; ++i) {
                E element = elements[i];
                this.add(index, element);
                ++index;
            }

        }
    }

    /**
     * Removes the element at the specified position in this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that
     * was removed from the list.
     *
     * @param index the index of the element to remove.
     * @return the element previously at the specified position.
     */
    @Override
    public E remove(int index) {
        return _list.remove(index);
    }

    /**
     * Removes from this list all of the elements whose index is between fromIndex,
     * inclusive, and toIndex, exclusive. This method is called by the <code>clear</code>
     * operation on this list and its subLists.
     *
     * @see #clear()
     */
    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        _list.removeRange(fromIndex, toIndex);
    }

    /**
     * Returns an iterator over the elements in this list (allocated on the stack when
     * executed in a StackContext).
     *
     * @return an iterator over this list values.
     */
    @Override
    public java.util.Iterator<E> iterator() {
        return _list.iterator();
    }

    /**
     * Returns a list iterator over the elements in this list (allocated on the stack when
     * executed in a StackContext).
     *
     * @return an iterator over this list values.
     */
    @Override
    public java.util.ListIterator<E> listIterator() {
        return _list.listIterator();
    }

    /**
     * Returns a list iterator from the specified position (allocated on the stack when
     * executed in a StackContext). The list iterator being returned does not support
     * insertion/deletion.
     *
     * @param index the index of first value to be returned from the list iterator (by a
     *              call to the next method).
     * @return a list iterator of the values in this table starting at the specified
     *         position in this list.
     */
    @Override
    public java.util.ListIterator<E> listIterator(int index) {
        return _list.listIterator(index);
    }

    /**
     * Returns the unmodifiable view associated to this collection. Attempts to modify the
     * returned collection result in an UnsupportedOperationException being thrown. The
     * view is typically part of the collection itself (created only once) and also an
     * instance of FastCollection supporting direct iterations.
     *
     * @return the unmodifiable view over this collection.
     */
    public java.util.List<E> unmodifiable() {
        return _list.unmodifiable();
    }

    /**
     * Resets the internal state of this object to its default values.
     */
    @Override
    public void reset() {
        _list.reset();
        _userData = null;
        _name = null;
    }

    /**
     * Returns the element with the specified name from this list.
     *
     * @param name The name of the element we are looking for in the list.
     * @return The element matching the specified name. If the specified element name
     *         isn't found in the list, then null is returned.
     */
    public E get(CharSequence name) {

        E element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.get(index);

        return element;
    }

    /**
     * Removes the element at the specified name in this list. Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that
     * was removed from the list.
     *
     * @param name the name of the element to remove.
     * @return the element previously at the specified position.
     */
    public E remove(CharSequence name) {

        E element = null;
        int index = getIndexFromName(name);
        if (index >= 0)
            element = this.remove(index);

        return element;
    }

    /**
     * Return the index to the 1st data element in this list with the specified name.
     *
     * @param name The name of the data element to find in this list.
     * @return The index to the named data element or -1 if it is not found.
     */
    public int getIndexFromName(CharSequence name) {
        Text nameText = Text.valueOf(name);

        int result = -1;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            DataElement element = this.get(i);
            Text eName = Text.valueOf(element.getName());
            if (eName.equals(nameText)) {
                result = i;
                break;
            }
        }
        return result;
    }

    // *******  The following support data element requirements *****
    /**
     * Return any user defined object associated with this data element. If there is no
     * user data, then <code>null</code> is returned.
     */
    @Override
    public Object getUserObject() {
        return _userData;
    }

    /**
     * Set the user defined object associated with this data element. This can be used to
     * store any type of information with a data element that could be useful. Storing
     * <code>null</code> for no user object is fine.
     */
    @Override
    public void setUserObject(Object data) {
        _userData = data;
    }

    /**
     * Return the name of this data element.
     */
    @Override
    public CharSequence getName() {
        return _name;
    }

    /**
     * Change the name of this data element to the specified name (may not be
     * <code>null</code>).
     */
    @Override
    public void setName(CharSequence name) {
        if (name == null)
            throw new NullPointerException(MessageFormat.format(
                    RESOURCES.getString("paramNullErr"),"unit"));
        _name = name;
    }

    /**
     * Compares the specified object with this list of <code>DataElement</code> objects
     * for equality. Returns true if and only if both collections contain equal values in
     * the same order.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this list is identical to that list;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        DataElementList<?> that = (DataElementList<?>)obj;
        if (!this._name.equals(that._name))
            return false;
        if (this._userData == null) {
            if (that._userData != null)
                return false;
        } else if (!this._userData.equals(that._userData))
            return false;

        return _list.equals(obj);
    }

    /**
     * Returns the hash code for this <code>DataElementList</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = _list.hashCode();

        hash = hash * 31 + _name.hashCode();
        hash = hash * 31 + (_userData != null ? _userData.hashCode() : 0);

        return hash;
    }

    /**
     * Create a Text representation of this data element which simply consists of the
     * element's name.
     */
    public Text toText() {
        return Text.valueOf(getName());
    }

    /**
     * Create a string representation of this data element which simply consists of the
     * element's name.
     */
    @Override
    public String toString() {
        return getName().toString();
    }

    /*
     *  Compares this data element with the specified element for order (where
     *  order is determined by the element's name).
     *  Returns a negative integer, zero, or a positive integer as this
     *  object is less than, equal to, or greater than the specified object.
     *  This method delegates to the Text.compareTo() method.
     *
     *  @param otherElement  The data element this one is being compared to.
     *  @throw ClassCastException if the specified object's type prevents
     *         it from being compared to this Object.
     */
    @Override
    public int compareTo(DataElement otherElement) {
        Text thisName = Text.valueOf(this.getName());
        Text otherName = Text.valueOf(otherElement.getName());
        return thisName.compareTo(otherName);
    }
}
