/**
 * DataSet -- A collection of cases that make up a data set.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.util.Collection;
import javolution.context.ObjectFactory;

/**
 * Defines a data element called a set. A data set is a collection of runs or
 * {@link DataCase cases} that make up a single test or set of associated data. Any number
 * of cases may be added to a set.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 13, 2015
 */
public final class DataSet extends DataElementList<DataCase> {

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Returns a new, preallocated or recycled <code>DataSet</code> instance (on the stack
     * when executing in a <code>StackContext</code>) with the specified name, that can
     * store a list of {@link DataCase} objects and any associated user data.
     *
     * @param name The name to be assigned to this set (may not be <code>null</code>).
     */
    public static DataSet newInstance(CharSequence name) {
        DataSet o = FACTORY.object();
        try {
            o.setName(name);
        } catch (NullPointerException e) {
            FACTORY.recycle(o);
            throw e;
        }
        return o;
    }

    private static final ObjectFactory<DataSet> FACTORY = new ObjectFactory<DataSet>() {
        @Override
        protected DataSet create() {
            return new DataSet();
        }

        @Override
        protected void cleanup(DataSet obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a set instance immediately (on the stack when executing in a
     * StackContext).
     */
    public static void recycle(DataSet instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used.
     */
    private DataSet() { }

    /**
     * Return a data set made up of any {@link DataCase cases} found in the specified
     * collection. Any objects that are not {@link DataCase} objects in the specified
     * collection will be ignored. If you pass a <code>DataSet</code> object, all the
     * cases found in it will be added to the new set.
     *
     * @param name  The name to be assigned to this set (may not be <code>null</code>).
     * @param cases A collection that contains a set of cases.
     */
    public static DataSet valueOf(String name, Collection<?> cases) {

        DataSet set = DataSet.newInstance(name);

        if (cases instanceof DataSet) {
            set.addAll((DataSet)cases);

        } else {
            for (Object obj : cases) {
                if (obj instanceof DataCase) {
                    set.add((DataCase)obj);
                }
            }
        }

        return set;
    }
}
