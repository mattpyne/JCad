/**
 * DataParam -- A data parameter or variable in a case.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.text.MessageFormat;
import javolution.text.Text;

/**
 * A data parameter or variable in a {@link DataCase case} or run.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 15, 2015
 */
public abstract class DataParam implements DataElement {

    //  The name of the parameter.
    private CharSequence _name;

    //  Reference data for this element.
    private Object _userData;

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    protected DataParam() { }

    /**
     * Return the name of this parameter.
     */
    @Override
    public CharSequence getName() {
        return _name;
    }

    /**
     * Change the name of this parameter to the specified name (may <em>not</em> be
     * <code>null</code>).
     */
    @Override
    public void setName(CharSequence name) {
        if (name == null)
            throw new NullPointerException(MessageFormat.format(
                    RESOURCES.getString("paramNullErr"),"name"));
        _name = name;
    }

    /**
     * Return any user defined object associated with this data element. If there is no
     * user data, then <code>null</code> is returned.
     */
    @Override
    public Object getUserObject() {
        return _userData;
    }

    /**
     * Set the user defined object associated with this data element. This can be used to
     * store any type of information with a data element that could be useful. Storing
     * <code>null</code> for no user object is fine.
     */
    @Override
    public void setUserObject(Object data) {
        _userData = data;
    }

    /**
     * Compares the specified object with this parameter for strict equality, same value,
     * same name, same user data.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this parameter is identical to that parameter;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        DataParam that = (DataParam)obj;
        if (!this._name.equals(that._name))
            return false;
        if (this._userData == null) {
            if (that._userData != null)
                return false;
        } else if (!this._userData.equals(that._userData))
            return false;

        return true;
    }

    /**
     * Returns the hash code for this <code>DataParam</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        hash = hash * 31 + _name.hashCode();
        hash = hash * 31 + (_userData != null ? _userData.hashCode() : 0);

        return hash;
    }

    /**
     * Resets the internal state of this object to its default values.
     */
    protected void reset() {
        _name = null;
        _userData = null;
    }

    /**
     * Create a Text representation of this parameter which simply consists of the
     * parameter's name.
     */
    public Text toText() {
        return Text.valueOf(getName());
    }

    /**
     * Create a string representation of this parameter which simply consists of the
     * parameter's name.
     */
    @Override
    public String toString() {
        return getName().toString();
    }

    /*
     *  Compares this data element with the specified element for order (where
     *  order is determined by the element's name).
     *  Returns a negative integer, zero, or a positive integer as this
     *  object is less than, equal to, or greater than the specified object.
     *  This method delegates to the Text.compareTo() method using
     *  this object's name.
     *
     *  @param otherElement  The data element this one is being compared to.
     *  @throw ClassCastException if the specified object's type prevents
     *         it from being compared to this Object.
     */
    @Override
    public int compareTo(DataElement otherElement) {
        Text thisName = Text.valueOf(getName());
        Text otherName = Text.valueOf(otherElement.getName());
        return thisName.compareTo(otherName);
    }
}
