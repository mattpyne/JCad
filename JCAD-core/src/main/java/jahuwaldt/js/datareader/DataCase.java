/**
 * DataCase -- A collection of parameters that make up a single case in a data set.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import javolution.context.ObjectFactory;
import javolution.util.FastTable;

/**
 * Defines a data element called a case. A case is a collection of
 * {@link DataParam parameters} or variables that make up a single run or test case. Any
 * number of parameters may be added to a case, but all array type parameters in a single
 * case must have the same number of elements.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 15, 2015
 */
public final class DataCase extends DataElementList<DataParam> {

    /**
     * Return a case made up of any {@link DataParam parameters} found in the specified
     * collection. Any objects that are not {@link DataParam} objects in the specified
     * collection will be ignored.
     *
     * @param name   The name to be assigned to this case (may not be <code>null</code>).
     * @param params A collection that contains a set of parameters.
     * @throws ArrayIndexOutOfBoundsException if an array added to this case has a
     * different number of elements than an array already in this case.
     */
    public static DataCase valueOf(CharSequence name, Collection<?> params) throws ArrayIndexOutOfBoundsException {

        DataCase aCase = DataCase.newInstance(name);

        //  Determine the size allowed for arrays in this case.
        int allowedSize = 0;

        for (Object obj : params) {
            if (obj instanceof ArrayParam) {

                //  Check size of arrays for consistency before adding!
                ArrayParam newArray = (ArrayParam)obj;
                int size = newArray.size();
                if (allowedSize > 0)
                    if (size != allowedSize) {
                        DataCase.recycle(aCase);
                        throw new ArrayIndexOutOfBoundsException(RESOURCES.getString("arrSizeErr"));
                    } else
                        allowedSize = size;

                aCase.add((DataParam)obj);

            } else if (obj instanceof DataParam) {
                aCase.add((DataParam)obj);
            }
        }

        return aCase;
    }

    /**
     * Replaces the parameter at the specified position in this case with the specified
     * parameter. Null elements are ignored.
     *
     * @param index   The index of the parameter to replace.
     * @param element The parameter to be stored a the specified position.
     * @return The parameter previously at the specified position in this case.
     * @throws ArrayIndexOutOfBoundsException if an array added to this case has a
     * different number of elements than an array already in this case.
     */
    @Override
    public DataParam set(int index, DataParam element) {
        if (element == null)
            return null;

        if (element instanceof ArrayParam) {
            //  Determine the size allowed for arrays in this case.
            int allowedSize = getAllowedArraySize();

            if (allowedSize > 0 && ((ArrayParam)element).size() != allowedSize)
                throw new ArrayIndexOutOfBoundsException(RESOURCES.getString("arrSizeErr"));
        }

        return super.set(index, element);
    }

    /**
     * Inserts the specified parameter at the specified position in this case. Shifts the
     * parameter currently at that position (if any) and any subsequent parameters to the
     * right (adds one to their indices). Null elements are ignored.
     *
     * @param index   Index at which the specified parameter is to be inserted.
     * @param element DataParam to be inserted.
     * @throws ClassCastException if the specified element is not a DataParam type object.
     * @throws ArrayIndexOutOfBoundsException if an array added to this case has a
     * different number of elements than an array already in this case.
     */
    @Override
    public void add(int index, DataParam element) {
        if (element == null)
            return;

        if (element instanceof ArrayParam) {
            //  Determine the size allowed for arrays in this case.
            int allowedSize = getAllowedArraySize();

            if (allowedSize > 0 && ((ArrayParam)element).size() != allowedSize)
                throw new ArrayIndexOutOfBoundsException(RESOURCES.getString("arrSizeErr"));
        }

        super.add(index, element);
    }

    /**
     * Appends the specified list of parameters to the end of this case. Null elements are
     * ignored.
     *
     * @param elements List of parameters to be inserted.
     * @throws ArrayIndexOutOfBoundsException if an array added to this case has a
     * different number of elements than an array already in this case.
     */
    @Override
    public void addAll(DataParam[] elements) {
        if (elements != null) {
            int size = elements.length;
            for (int i = 0; i < size; ++i) {
                DataParam param = elements[i];
                this.add(param);
            }
        }
    }

    /**
     * Inserts the specified list of parameters at the specified position in this case.
     * Shifts the parameter currently at that position (if any) and any subsequent
     * parameters to the right (adds one to their indices) until all the parameters have
     * been added. Null elements are ignored.
     *
     * @param index    Index at which the specified list of parameters is to be inserted.
     * @param elements List of parameters to be inserted.
     * @throws ArrayIndexOutOfBoundsException if an array added to this case has a
     * different number of elements than an array already in this case.
     */
    @Override
    public void addAll(int index, DataParam[] elements) {
        if (elements != null) {

            for (DataParam param : elements) {
                this.add(index, param);
                ++index;
            }

        }
    }

    /**
     * Returns the size of any array in this case (since all arrays must have the same
     * number of elements). If there are no arrays in this case, zero is returned.
     */
    public int getAllowedArraySize() {
        int size = 0;

        for (DataParam param : this) {
            if (param instanceof ArrayParam) {
                size = ((ArrayParam)param).size();
                break;
            }
        }

        return size;
    }

    /**
     * Returns a list of all the Text/Note objects in this case. If there are no text
     * parameters, null is returned.
     */
    public List<TextParam> getAllText() {

        List<TextParam> list = FastTable.newInstance();

        //  Loop over all the parameters and collect the text parameters.
        for (DataParam param : this) {
            if (param instanceof TextParam)
                list.add((TextParam)param);
        }

        return list;
    }

    /**
     * Removes all TextParam objects from this case.
     */
    public void clearAllText() {

        //  Loop over all the parameters and delete text parameters.
        for (ListIterator<DataParam> i = listIterator(); i.hasNext();) {
            Object obj = i.next();
            if (obj instanceof TextParam)
                i.remove();
        }
    }

    /**
     * Returns a list of all the ScalarParam objects in this case. If there are no scalar
     * parameters, null is returned.
     */
    public List<ScalarParam> getAllScalars() {

        List<ScalarParam> list = FastTable.newInstance();

        //  Loop over all the parameters and collect the scalar parameters.
        for (DataParam param : this) {
            if (param instanceof ScalarParam)
                list.add((ScalarParam)param);
        }

        return list;
    }

    /**
     * Returns a list of all the ArrayParam objects in this case. If there are no array
     * parameters, null is returned.
     */
    public List<ArrayParam> getAllArrays() {

        List<ArrayParam> list = FastTable.newInstance();

        //  Loop over all the parameters and collect the array parameters.
        for (DataParam param : this) {
            if (param instanceof ArrayParam)
                list.add((ArrayParam)param);
        }

        return list;
    }

    //////////////////////
    // Factory Creation //
    //////////////////////
    public static DataCase newInstance(CharSequence name) {
        DataCase o = FACTORY.object();
        try {
            o.setName(name);
        } catch (NullPointerException e) {
            FACTORY.recycle(o);
            throw e;
        }
        return o;
    }
    private static final ObjectFactory<DataCase> FACTORY = new ObjectFactory<DataCase>() {
        @Override
        protected DataCase create() {
            return new DataCase();
        }

        @Override
        protected void cleanup(DataCase obj) {
            obj.reset();
        }
    };

    /**
     * Recycles a case instance immediately (on the stack when executing in a
     * StackContext).
     */
    public static void recycle(DataCase instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used.
     */
    private DataCase() { }
}
