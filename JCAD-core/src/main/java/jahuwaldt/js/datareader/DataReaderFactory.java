/**
 * DataReaderFactory -- Factory that returns appropriate DataReader objects.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.io.*;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import javolution.util.FastTable;
import javolution.util.FastSet;
import javolution.util.FastComparator;

/**
 * This class returns a specific {@link DataReader} object that can read in the specified
 * file. This class implements a pluggable architecture. A new {@link DataReader} class
 * can be added by simply creating a subclass of <code>DataReader</code>, creating a
 * datareader.properties file that refers to it, and putting that properties file
 * somewhere in the Java search path.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 25, 2003
 * @version October 24, 2015
 */
public final class DataReaderFactory {

    /**
     * The resource bundle for this package.
     */
    public static final ResourceBundle RESOURCES = DataReader.RESOURCES;
    
    /**
     * All class loader resources with this name ("datareader.properties") are loaded as
     * .properties definitions and merged together to create a global list of reader
     * handler mappings.
     */
    private static final String MAPPING_RES_NAME = RESOURCES.getString("mappingResourcePath");

    //  An array containing a reference to all the readers that have been found.
    private static final DataReader[] _allReaders; // set in <clinit>

    //  Locate and load all the readers we can find.
    static {
        DataReader[] temp = null;
        try {
            temp = loadResourceList(MAPPING_RES_NAME, getClassLoader());

        } catch (Exception e) {
            System.out.println("could not load all [" + MAPPING_RES_NAME + "] mappings:");
            e.printStackTrace();
        }

        _allReaders = temp;
    }

    // this class is not extendible
    private DataReaderFactory() { }

    /**
     * Method that attempts to find a {@link DataReader} object that might be able to read
     * the data in the specified input stream.
     *
     * @param inputStream An input stream to the file that a reader is to be returned for.
     * @param pathName    The path or file name for the file that a reader is to be
     *                    returned for.
     * @return A reader that is appropriate for the specified file, or <code>null</code>
     *         if the user cancels the multiple reader selection dialog.
     * @throws IOException if an appropriate reader for the file could not be found.
     */
    public static DataReader getReader(BufferedInputStream inputStream, String pathName) throws IOException {
        String name = new File(pathName).getName();

        //  Get the list of data readers that are available.
        DataReader[] allReaders = getAllReaders();
        if (allReaders == null || allReaders.length < 1)
            throw new IOException(RESOURCES.getString("noReadersErr"));

        FastTable<DataReader> list = FastTable.newInstance();

        try {
            //  Loop through all the available readers and see if any of them will work.
            for (DataReader reader : allReaders) {

                //  Mark the current position in the input stream (the start of the stream).
                inputStream.mark(1024000);

                reader.setDefaultSetName(name);

                //  Can this reader read the specified input stream?
                int canReadFile = reader.canReadData(pathName, inputStream);

                //  If the reader is certain it can read the data, use it.
                if (canReadFile == DataReader.YES) {
                    FastTable.recycle(list);
                    return reader;
                }

                //  Otherwise, build a list of maybes.
                if (canReadFile == DataReader.MAYBE)
                    list.add(reader);

                //  Return to the start of the stream for the next pass.
                inputStream.reset();
            }

        } catch (Exception e) {
            FastTable.recycle(list);
            e.printStackTrace();
            throw new IOException(MessageFormat.format(RESOURCES.getString("fileTypeErr"),name));
        }

        if (list.size() < 1) {
            FastTable.recycle(list);
            throw new IOException(MessageFormat.format(RESOURCES.getString("unknownFileTypeErr"),name));
        }

        //  If there is only one reader in the list, try and use it.
        DataReader selectedReader;
        if (list.size() == 1)
            selectedReader = list.get(0);
        else {
            //  Ask the user to select which reader they want to try and use.
            DataReader[] possibleValues = list.toArray(new DataReader[list.size()]);
            selectedReader = (DataReader)JOptionPane.showInputDialog(null,
                    MessageFormat.format(RESOURCES.getString("selFormatMsg"),name),
                    RESOURCES.getString("selFormatTitle"),JOptionPane.INFORMATION_MESSAGE,
                    null, possibleValues, possibleValues[0]);
        }

        //  Clean up before leaving.
        FastTable.recycle(list);

        return selectedReader;
    }

    /**
     * Method that attempts to find an {@link DataReader} object that might be able to
     * read the data in the specified file.
     *
     * @param theFile The file to find a reader for.
     * @return A reader that is appropriate for the specified file, or <code>null</code>
     *         if the user cancels the multiple reader selection dialog.
     * @throws IOException if an appropriate reader for the file could not be found.
     */
    public static DataReader getReader(File theFile) throws IOException {

        if (!theFile.exists())
            throw new IOException(MessageFormat.format(
                    RESOURCES.getString("fileNotFoundErr"),theFile.getName()));

        DataReader reader = null;
        BufferedInputStream input = null;
        try {
            //  Get an input stream to the file.
            input = new BufferedInputStream(new FileInputStream(theFile));
            String pathName = theFile.getPath();

            reader = getReader(input, pathName);

        } finally {
            //  Make sure and close the input stream.
            if (input != null)
                input.close();
        }

        return reader;
    }

    /**
     * Method that returns a list of all the DataReader objects found by this
     * factory.
     *
     * @return An array of DataReader objects (can be null if static initialization
     *         failed).
     */
    public static DataReader[] getAllReaders() {
        return _allReaders;
    }

    /**
     * Loads a reader list that is a union of *all* resources named 'resourceName' as seen
     * by 'loader'. Null 'loader' is equivalent to the application loader.
     */
    private static DataReader[] loadResourceList(final String resourceName, ClassLoader loader) {
        if (loader == null)
            loader = ClassLoader.getSystemClassLoader();

        final FastSet<DataReader> result = FastSet.newInstance();

        try {
            // NOTE: using getResources() here
            final Enumeration<URL> resources = loader.getResources(resourceName);

            if (resources != null) {
                // merge all mappings in 'resources':

                while (resources.hasMoreElements()) {
                    final URL url = resources.nextElement();
                    final Properties mapping;

                    InputStream urlIn = null;
                    try {
                        urlIn = url.openStream();

                        mapping = new Properties();
                        mapping.load(urlIn); // load in .properties format

                    } catch (IOException ioe) {
                        // ignore this resource and go to the next one
                        continue;

                    } finally {
                        if (urlIn != null)
                            try {
                                urlIn.close();
                            } catch (Exception ignore) {
                            }
                    }

                    // load all readers specified in 'mapping':
                    for (Enumeration<?> keys = mapping.propertyNames(); keys.hasMoreElements();) {
                        final String format = (String)keys.nextElement();
                        final String implClassName = mapping.getProperty(format);

                        result.add(loadResource(implClassName, loader));
                    }
                }
            }

        } catch (IOException ignore) {
            // ignore: an empty result will be returned
        }

        //  Convert result Set to an array.
        DataReader[] resultArr = result.toArray(new DataReader[result.size()]);

        //  Sort the array using the specified comparator.
        Arrays.sort(resultArr, FastComparator.DEFAULT);

        //  Clean up before leaving.
        FastSet.recycle(result);

        //  Output the sorted array.
        return resultArr;
    }

    /**
     * Loads and initializes a single resource for a given format name via a given
     * classloader. For simplicity, all errors are converted to RuntimeExceptions.
     */
    private static DataReader loadResource(final String className, final ClassLoader loader) {
        if (className == null)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("paramNullErr"), "className"));
        if (loader == null)
            throw new IllegalArgumentException(MessageFormat.format(
                    RESOURCES.getString("paramNullErr"), "loader"));

        final Class<?> cls;
        final Object reader;
        try {
            cls = Class.forName(className, true, loader);
            reader = cls.newInstance();

        } catch (Exception e) {
            throw new RuntimeException(MessageFormat.format(
                    RESOURCES.getString("classLoadErr"),className),e);
        }

        if (!(reader instanceof DataReader))
            throw new RuntimeException(MessageFormat.format(
                    RESOURCES.getString("classTypeErr"),cls.getName()));

        return (DataReader)reader;
    }

    /**
     * This method decides on which class loader is to be used by all resource/class
     * loading in this class. At the very least you should use the current thread's
     * context loader. A better strategy would be to use techniques shown in
     * http://www.javaworld.com/javaworld/javaqa/2003-06/01-qa-0606-load.html
     */
    private static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }
}
