/**
 * UnitParameter -- Any parameter that has units associated with it.
 *
 * Copyright (C) 2003-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.util.Objects;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javolution.lang.ValueType;

/**
 * A class that represents a parameter in a case that has units associated with it.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version March 18, 2017
 * @param <Q> The unit Quantity type.
 * @param <E> The unit parameter sub-type.
 */
public abstract class UnitParameter<Q extends Quantity, E extends UnitParameter>
        extends DataParam implements ValueType {

    /**
     * The units of the parameter (may NOT be null!).
     */
    protected Unit<Q> _unit;

    /**
     * Construct a parameter with no name or units.
     */
    protected UnitParameter() {
    }

    /**
     * Returns the unit in which the value is stated.
     *
     * @return the parameter unit.
     */
    public Unit<Q> getUnit() {
        return _unit;
    }

    /**
     * Returns the parameter equivalent to this parameter but stated in the specified
     * unit.
     *
     * @param unit the unit of the parameter to be returned.
     * @return a parameter equivalent to this parameter but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public abstract E to(Unit<Q> unit) throws ConversionException;

    /**
     * Returns the parameter that has the same values as this parameter but with the units
     * changed (<em>without</em> converting the values).
     *
     * @param <R> The unit of measure type (or Quantity) for the input units.
     * @param unit the unit of the parameter to be returned.
     * @return a parameter equivalent to this parameter but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    public abstract <R extends Quantity> E changeTo(Unit<R> unit) throws ConversionException;

    /**
     * Compares the specified object with this parameter for strict equality same value,
     * same units, same name, same user data.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this parameter is identical to that parameter;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        UnitParameter that = (UnitParameter)obj;
        if (!Objects.equals(this._unit,that._unit))
            return false;

        return super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>UnitParameter</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode();

        hash = hash * 31 + _unit.hashCode();

        return hash;
    }

}
