/**
 * TextParam -- A parameter that contains a single text string.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Library General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/gpl.html
 */
package jahuwaldt.js.datareader;

import java.text.MessageFormat;
import javolution.context.ObjectFactory;
import javolution.text.Text;
import javolution.text.TextBuilder;

/**
 * A class that represents a text/note parameter.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 15, 2015
 */
public final class TextParam extends DataParam {

    //  The text held by this parameter.
    private Text _value;

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Returns a new, preallocated or recycled <code>TextParam</code> instance (on the
     * stack when executing in a <code>StackContext</code>) with the specified name and
     * containing no text.
     *
     * @param name The name to be assigned to this parameter (may not be
     *             <code>null</code>).
     */
    public static TextParam newInstance(CharSequence name) {
        TextParam o = FACTORY.object();
        try {
            o.setName(name);
            o._value = Text.EMPTY;
        } catch (NullPointerException e) {
            FACTORY.recycle(o);
            throw e;
        }
        return o;
    }

    private static final ObjectFactory<TextParam> FACTORY = new ObjectFactory<TextParam>() {
        @Override
        protected TextParam create() {
            return new TextParam();
        }

        @Override
        protected void cleanup(TextParam obj) {
            obj.reset();
        }
    };

    /**
     * Resets the internal state of this object to its default values.
     */
    @Override
    protected void reset() {
        super.reset();
        _value = null;
    }

    /**
     * Recycles a parameter instance immediately (on the stack when executing in a
     * StackContext).
     */
    public static void recycle(TextParam instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    private TextParam() { }

    /**
     * Returns a text parameter with the specified name and text (which may be null).
     */
    public static TextParam valueOf(CharSequence name, CharSequence text) {

        TextParam param = TextParam.newInstance(name);
        param._value = Text.valueOf(text);

        return param;
    }

    /**
     * Return the text of this text parameter. Could return an empty string if there is no
     * text.
     */
    public Text getText() {
        return _value;
    }

    /**
     * Change the text contained in this parameter. The text can not be null.
     */
    public void setText(CharSequence text) {
        if (text == null)
            throw new NullPointerException(MessageFormat.format(
                    RESOURCES.getString("paramNullErr"),"text"));
        _value = Text.valueOf(text);
    }

    /**
     * Compares the specified object with this parameter for strict equality same text,
     * same name, and same user data.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this parameter is identical to that parameter;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        TextParam that = (TextParam)obj;
        if (!this._value.equals(that._value))
            return false;

        return super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>UnitParameter</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = 7;

        hash = hash * 31 + (_value != null ? _value.hashCode() : 0);
        hash = hash * 31 + super.hashCode();

        return hash;
    }

    /**
     * Create a Text representation of this parameter which consists of the parameter's
     * name and text.
     */
    @Override
    public Text toText() {
        TextBuilder buffer = TextBuilder.newInstance();

        buffer.append(getName());
        buffer.append(" = ");
        buffer.append(getText());

        Text text = buffer.toText();
        TextBuilder.recycle(buffer);

        return text;
    }

    /**
     * Create a string representation of this parameter which consists of the parameter's
     * name and text.
     */
    @Override
    public String toString() {
        return toText().toString();
    }
}
