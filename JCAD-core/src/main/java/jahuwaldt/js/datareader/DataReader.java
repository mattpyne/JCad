/**
 * DataReader -- Interface for classes that read and write data files.
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.awt.Frame;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.ResourceBundle;

/**
 * This class provides a standard interface for reading and writing data files of various
 * formats. Sub-classes actually do the translation from the data file's format to the
 * DataSet data structure in memory and vise versa.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 6, 2003
 * @version October 15, 2015
 */
public interface DataReader extends Comparable<DataReader> {

    /**
     * The resource bundle for this package.
     */
    public static final ResourceBundle RESOURCES
            = ResourceBundle.getBundle("jahuwaldt.js.datareader.DataReader", java.util.Locale.getDefault());
    
    /**
     * Constant indicating that a reader can not read a specified file stream.
     */
    public static final int NO = 0;

    /**
     * Constant indicating that a reader can certainly read a specified file stream.
     */
    public static final int YES = 1;

    /**
     * Constant indicating that a reader might be able to read a specified file stream,
     * but can't determine for sure.
     */
    public static final int MAYBE = -1;

    /**
     * Interface for reading in data from an input stream and returning that data as a
     * list of {@link DataSet} objects.
     *
     * @param pathName The path to the file to be read.
     * @param input    An input stream containing the data to be read.
     * @return A list of DataSet objects that contain the data read in from the specified
     * stream.
     * @throws Exception If there is a problem reading the specified stream.
     */
    public List<DataSet> read(String pathName, InputStream input) throws IOException;

    /**
     * Interface for writing out all the data stored in the specified list of
     * {@link DataSet} objects to the specified output stream.
     *
     * @param output The output stream to which the data is to be written.
     * @param data   The list of DataSet objects to be written out.
     * @throws IOException If there is a problem writing to the specified stream.
     */
    public void write(OutputStream output, List<DataSet> data) throws IOException;

    /**
     * Sets the default set name to use for data formats that do not have named data sets.
     */
    public void setDefaultSetName(CharSequence name);

    /**
     * Method that determines if this reader can read data from the specified input
     * stream.
     *
     * @param pathName The path to the file to be read.
     * @param input    An input stream containing the data to be read. Any methods that
     *                 read from this stream must first set a mark and then reset back to
     *                 that mark before the method returns (even if it returns with an
     *                 exception).
     * @return DataReader.NO if the file format is not recognized by this reader.
     *         DataReader.YES if the file format is definitely recognized by this reader.
     *         DataReader.MAYBE if the file format might be readable by this reader, but
     *         that can't easily be determined without actually reading the file.
     */
    public int canReadData(String pathName, BufferedInputStream input) throws IOException;

    /**
     * Returns true if this class can write at least some data in the format supported by
     * this class. Returns false if it can not.
     */
    public boolean canWriteData();

    /**
     * Returns a data structure that contains the data in the input list filtered in
     * whatever way is necessary to save it using a particular format. This is done by
     * applying rules and/or by asking the user to choose what data should be saved. The
     * requirements for what can be saved is specific to each format.
     *
     * @param parent Determines the Frame in which the dialog is displayed; if null, or if
     *               the parentComponent has no Frame, a default Frame is used.
     * @param data   The input data set that is to be selected for saving.
     * @return A list of DataSet objects containing the selected data to be saved. If this
     *         DataReader does not supporting saving, then null is returned.
     */
    public List<DataSet> selectDataForSaving(Frame parent, List<DataSet> data);

    /**
     * Returns the preferred file extension (not including the ".") for files of this
     * DataReader's type.
     */
    public String getExtension();
}
