/**
 * ArrayParam -- An array of point values in a case.
 *
 * Copyright (C) 2003-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javolution.context.ArrayFactory;
import javolution.context.ObjectFactory;
import javolution.util.FastTable;
import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.vector.Float64Vector;

/**
 * An array of parameter values in a {@link DataCase case} or run.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version March 18, 2017
 *
 * @param <Q> The Quantity or unit type of this array of values.
 */
public final class ArrayParam<Q extends Quantity> extends UnitParameter<Q, ArrayParam> {

    //  The number of elements in the array (the size of the array).
    private int _numElements = 0;

    //  The array of points in current units.
    private double[] _values;

    /**
     * Resets the internal state of this object to its default values.
     */
    @Override
    protected void reset() {
        super.reset();
        ArrayFactory.DOUBLES_FACTORY.recycle(_values);
        _values = null;
        _numElements = 0;
    }

    /**
     * Recycles a parameter instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled.
     */
    public static void recycle(ArrayParam instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used except by subclasses.
     */
    private ArrayParam() { }

    /**
     * Construct an array with the specified name and units with the array values all set
     * to zero. The name and the units may <em>not</em> be null.
     *
     * @param <Q>  The quantity or unit type of this array of values.
     * @param name The name of this array of parameters.
     * @param unit The units to assume for each value in the array.
     * @param size The size or number of elements in this array.
     * @return An array with the specified name and units with the array values all set to
     *         zero.
     */
    public static <Q extends Quantity> ArrayParam<Q> valueOf(CharSequence name, Unit<Q> unit, int size) {
        ArrayParam<Q> param = ArrayParam.newInstance(name);
        param._unit = requireNonNull(unit, MessageFormat.format(RESOURCES.getString("paramNullErr"),"unit"));

        param._numElements = size;
        param._values = ArrayFactory.DOUBLES_FACTORY.array(size);
        Arrays.fill(param._values, 0, size, 0);

        return param;
    }

    /**
     * Construct an array with the specified name, array of values and units (name and
     * units may <em>not</em> be null).
     *
     * @param <Q>    The quantity or unit type of this array of values.
     * @param name   The name of this array of parameters.
     * @param unit   The units to assume for each value in the array.
     * @param values The Java array of values to be stored in this ArrayParam object.
     * @return An array with the specified name, array of values and units.
     */
    public static <Q extends Quantity> ArrayParam<Q> valueOf(CharSequence name, Unit<Q> unit, double[] values) {
        requireNonNull(unit, MessageFormat.format(RESOURCES.getString("paramNullErr"),"unit"));
        requireNonNull(values, MessageFormat.format(RESOURCES.getString("paramNullErr"),"values"));

        ArrayParam param = ArrayParam.newInstance(name);
        param._unit = unit;
        param._numElements = values.length;

        //  Defensively copy the supplied array.
        param._values = ArrayFactory.DOUBLES_FACTORY.array(values.length);
        System.arraycopy(values, 0, param._values, 0, values.length);
        Arrays.fill(param._values, values.length, param._values.length, 0);

        return param;
    }

    /**
     * Construct an array with the specified name, list of values and units (name and
     * units may <em>not</em> be null).
     *
     * @param <Q>    The quantity or unit type of this array of values.
     * @param name   The name of this array of parameters.
     * @param unit   The units to assume for each value in the array.
     * @param values The list of values to be stored in this array.
     * @return An array with the specified name, list of values and units.
     */
    public static <Q extends Quantity> ArrayParam<Q> valueOf(CharSequence name, Unit<Q> unit, List<Double> values) {
        requireNonNull(unit, MessageFormat.format(RESOURCES.getString("paramNullErr"),"unit"));
        requireNonNull(values, MessageFormat.format(RESOURCES.getString("paramNullErr"),"values"));

        int numElements = values.size();
        ArrayParam param = ArrayParam.newInstance(name);
        param._unit = unit;
        param._numElements = numElements;

        //  Defensively copy the supplied array.
        param._values = ArrayFactory.DOUBLES_FACTORY.array(numElements);
        for (int i = 0; i < numElements; ++i) {
            param._values[i] = values.get(i);
        }
        Arrays.fill(param._values, numElements, param._values.length, 0);

        return param;
    }

    /**
     * Return the number of elements in this array.
     *
     * @return The number of elements in this array.
     */
    public int size() {
        return _numElements;
    }

    /**
     * Return the value of an element in the array in reference SI units.
     *
     * @param idx Index to the element in the array to be returned.
     * @return The value of an element in the array in reference SI units.
     * @throws IndexOutOfBoundsException if (index &lt; 0) || (index &gt; size()-1)
     */
    public double getValueSI(int idx) {
        if (idx >= _numElements)
            throw new IndexOutOfBoundsException(MessageFormat.format(
                    RESOURCES.getString("idxOutOfBoundsErr"),idx,_numElements));

        return getUnit().toStandardUnit().convert(_values[idx]);
    }

    /**
     * Return the value of an element in the array in the current units.
     *
     * @param idx Index to the point in the array to be returned.
     * @return The value of an element in the array in the current units.
     * @throws IndexOutOfBoundsException if (index &lt; 0) || (index &gt; size()-1)
     */
    public double getValue(int idx) {
        if (idx >= _numElements)
            throw new IndexOutOfBoundsException(MessageFormat.format(
                    RESOURCES.getString("idxOutOfBoundsErr"),idx,_numElements));

        return _values[idx];
    }

    /**
     * Return the list of values as a Float64Vector, in reference SI units.
     *
     * @return The list of values as a Float64Vector, in reference SI units.
     */
    public Float64Vector getValuesSI() {

        FastTable<Float64> newValues = FastTable.newInstance();
        UnitConverter cvtr = getUnit().toStandardUnit();

        int length = _numElements;
        for (int i = 0; i < length; ++i)
            newValues.add(Float64.valueOf(cvtr.convert(_values[i])));

        Float64Vector vector = Float64Vector.valueOf(newValues);
        FastTable.recycle(newValues);

        return vector;

    }

    /**
     * Return the list of values as a Float64Vector, in current units.
     *
     * @return The list of values as a Float64Vector, in current units.
     */
    public Float64Vector getValues() {
        FastTable<Float64> newValues = FastTable.newInstance();

        int length = _numElements;
        for (int i = 0; i < length; ++i)
            newValues.add(Float64.valueOf(_values[i]));

        Float64Vector vector = Float64Vector.valueOf(newValues);
        FastTable.recycle(newValues);

        return vector;
    }

    /**
     * Returns the equivalent to this parameter but stated in the specified unit. The
     * values <em>are</em> converted to the new units.
     *
     * @param unit the unit of the parameter to be returned.
     * @return an equivalent to this parameter but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public ArrayParam<Q> to(Unit<Q> unit) throws ConversionException {
        if (Objects.equals(_unit, requireNonNull(unit)))
            return this;
        UnitConverter cvtr = Parameter.converterOf(_unit, unit);
        if (cvtr == UnitConverter.IDENTITY) { // No conversion necessary.
            return this;
        }
        ArrayParam<Q> result = ArrayParam.newInstance(getName());
        int length = _numElements;
        result._numElements = length;
        result._unit = unit;
        result._values = ArrayFactory.DOUBLES_FACTORY.array(length);
        for (int i = 0; i < length; ++i) {
            double value = cvtr.convert(_values[i]);
            result._values[i] = value;
        }
        Arrays.fill(result._values, length, result._values.length, 0);
        return result;
    }

    /**
     * Returns the parameter that has the same values as this parameter but with the units
     * changed (<em>without</em> converting the values).
     *
     * @param <R>  The Quantity or unit type of the new unit.
     * @param unit the unit of the parameter to be returned.
     * @return an equivalent to this parameter but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public <R extends Quantity> ArrayParam<R> changeTo(Unit<R> unit) throws ConversionException {
        requireNonNull(unit, MessageFormat.format(RESOURCES.getString("paramNullErr"),"unit"));
        if (Objects.equals(_unit, unit))
            return (ArrayParam<R>)this;
        ArrayParam<R> result = (ArrayParam<R>)ArrayParam.copyOf(this);
        result._unit = unit;
        return result;
    }

    /**
     * Returns a copy of this ArrayParam instance
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this parameter.
     */
    @Override
    public ArrayParam<Q> copy() {
        return copyOf(this);
    }

    /**
     * Method that sorts the values in this array in ascending numerical order. And
     * returns the values as a new array.
     *
     * @return A new ArrayParam with the values sorted in ascending numerical order.
     */
    public ArrayParam<Q> sort() {
        ArrayParam<Q> P = copyOf(this);
        Arrays.sort(P._values);
        return P;
    }

    /**
     * Compares the specified object with this parameter for strict equality same value,
     * same units, same name, same user data.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this parameter is identical to that parameter;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        ArrayParam that = (ArrayParam)obj;
        if (this._numElements != that._numElements)
            return false;
        else if (!equalArrays(this._values, that._values, this._numElements))
            return false;

        return super.equals(obj);
    }

    private boolean equalArrays(double[] arr1, double[] arr2, int length) {
        for (int i = 0; i < length; ++i) {
            double v = arr1[i];
            if (arr2[i] != v)
                return false;
        }
        return true;
    }

    /**
     * Returns the hash code for this <code>ArrayParam</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode();

        hash = hash * 31 + _numElements;
        hash = hash * 31 + Arrays.hashCode(_values);

        return hash;
    }

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Returns a new, preallocated or recycled <code>ArrayParam</code> instance (on the
     * stack when executing in a <code>StackContext</code>) with the specified name, no
     * units, and no values (zero size).
     *
     * @param name The name to be assigned to this parameter (may not be
     *             <code>null</code>).
     * @return A new ArrayParam instance with the given name and not data (zero size).
     */
    public static ArrayParam newInstance(CharSequence name) {
        ArrayParam o = FACTORY.object();
        o._numElements = 0;
        try {
            o.setName(name);
        } catch (NullPointerException e) {
            FACTORY.recycle(o);
            throw e;
        }
        return o;
    }

    private static final ObjectFactory<ArrayParam> FACTORY = new ObjectFactory<ArrayParam>() {
        @Override
        protected ArrayParam create() {
            return new ArrayParam();
        }

        @Override
        protected void cleanup(ArrayParam obj) {
            obj.reset();
        }
    };

    private static ArrayParam copyOf(ArrayParam orig) {
        ArrayParam param = ArrayParam.newInstance(orig.getName());
        param._unit = orig.getUnit();
        int length = orig._numElements;
        param._numElements = length;
        param._values = ArrayFactory.DOUBLES_FACTORY.array(length);
        System.arraycopy(orig._values, 0, param._values, 0, length);
        Arrays.fill(param._values, length, param._values.length, 0);
        return param;
    }
}
