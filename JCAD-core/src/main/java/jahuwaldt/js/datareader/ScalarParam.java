/**
 * ScalarParam -- A parameter that contains a single scalar value.
 *
 * Copyright (C) 2003-2017, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import jahuwaldt.js.param.Parameter;
import java.text.MessageFormat;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import javax.measure.converter.ConversionException;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javolution.context.ObjectFactory;
import javolution.text.Text;

/**
 * A class that represents a scalar data value in a case.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version March 18, 2017
 * @param <Q> Unit quantity type.
 */
public final class ScalarParam<Q extends Quantity> extends UnitParameter<Q, ScalarParam> {

    //  The Parameter object backing this ScalarParam object.
    private Parameter<Q> _param;

    //////////////////////
    // Factory Creation //
    //////////////////////
    /**
     * Returns a new, preallocated or recycled <code>ScalarParam</code> instance (on the
     * stack when executing in a <code>StackContext</code>) with the specified name, no
     * units, and a value of 0.
     *
     * @param name The name to be assigned to this parameter (may not be
     *             <code>null</code>).
     */
    private static ScalarParam newInstance(CharSequence name) {
        ScalarParam o = FACTORY.object();
        try {
            o.setName(name);
        } catch (NullPointerException e) {
            FACTORY.recycle(o);
            throw e;
        }
        return o;
    }

    private static <Q extends Quantity> ScalarParam<Q> copyOf(ScalarParam<Q> original) {
        ScalarParam<Q> measure = FACTORY.object();
        measure._param = original._param.copy();
        measure._unit = original._unit;
        return measure;
    }

    private static final ObjectFactory<ScalarParam> FACTORY = new ObjectFactory<ScalarParam>() {
        @Override
        protected ScalarParam create() {
            return new ScalarParam();
        }

        @Override
        protected void cleanup(ScalarParam obj) {
            obj.reset();
        }
    };

    /**
     * Resets the internal state of this object to its default values.
     */
    @Override
    protected void reset() {
        super.reset();
        _param = null;
    }

    /**
     * Returns a copy of this parameter
     * {@link javolution.context.AllocatorContext allocated} by the calling thread
     * (possibly on the stack).
     *
     * @return an identical and independent copy of this parameter.
     */
    @Override
    public ScalarParam<Q> copy() {
        return copyOf(this);
    }

    /**
     * Recycles a parameter instance immediately (on the stack when executing in a
     * StackContext).
     *
     * @param instance The instance to be recycled immediately.
     */
    public static void recycle(ScalarParam instance) {
        FACTORY.recycle(instance);
    }

    /**
     * Do not allow the default constructor to be used.
     */
    private ScalarParam() {
    }

    /**
     * Returns a scalar with the specified name, value and units.
     *
     * @param <Q>   The unit quantity type for this parameter.
     * @param name  The name of the parameter (may not be null).
     * @param value The value to assign to this scalar in the specified units.
     * @param unit  The units for this scalar (may not be null).
     * @return A new scalar with the specified value and units.
     */
    public static <Q extends Quantity> ScalarParam<Q> valueOf(CharSequence name, double value, Unit<Q> unit) {

        ScalarParam<Q> param = ScalarParam.newInstance(requireNonNull(name));
        param._unit = requireNonNull(unit);
        param._param = Parameter.valueOf(value, unit);

        return param;
    }

    /**
     * Returns the parameter equivalent to this parameter but stated in the specified
     * unit.
     *
     * @param unit The unit of the parameter to be returned (may not be null).
     * @return A parameter equivalent to this parameter but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public ScalarParam<Q> to(Unit<Q> unit) throws ConversionException {
        if (Objects.equals(_unit, requireNonNull(unit)))
            return this;
        ScalarParam<Q> result = ScalarParam.newInstance(getName());
        result._unit = unit;
        result._param = _param.to(unit);
        return result;
    }

    /**
     * Returns the parameter that has the same values as this parameter but with the units
     * changed (<em>without</em> converting the values).
     *
     * @param unit The unit of the parameter to be returned (may not be null).
     * @return a parameter equivalent to this parameter but stated in the specified unit.
     * @throws ConversionException if the current model does not allows for conversion to
     * the specified unit.
     */
    @Override
    public <R extends Quantity> ScalarParam<R> changeTo(Unit<R> unit) throws ConversionException {
        requireNonNull(unit, MessageFormat.format(RESOURCES.getString("paramNullErr"),"unit"));
        if (Objects.equals(_unit, unit))
            return (ScalarParam<R>)this;
        ScalarParam<R> result = (ScalarParam<R>)FACTORY.object();
        result._unit = unit;
        result._param = Parameter.valueOf(getValue(), unit);
        return result;
    }

    /**
     * Return the value of this parameter in reference SI units.
     * 
     * @return The value of this parameter in reference SI units.
     */
    public double getValueSI() {
        Unit<Q> SIUnit = (Unit<Q>)getUnit().getStandardUnit();
        return _param.getValue(SIUnit);
    }

    /**
     * Returns the value for this parameter stated in this parameter's
     * {@link #getUnit unit}.
     *
     * @return the value of the parameter.
     */
    public double getValue() {
        return _param.getValue();
    }

    /**
     * Compares the specified object with this parameter for strict equality same value,
     * same units, same name, same user data.
     *
     * @param obj the object to compare with.
     * @return <code>true</code> if this parameter is identical to that parameter;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (obj.getClass() != this.getClass()))
            return false;

        ScalarParam that = (ScalarParam)obj;
        if (!this._param.equals(that._param))
            return false;

        return super.equals(obj);
    }

    /**
     * Returns the hash code for this <code>UnitParameter</code>.
     *
     * @return the hash code value.
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode();

        hash = hash * 31 + _param.hashCode();

        return hash;
    }

    /**
     * Create a Text representation of this parameter which consists of the parameter's
     * name, value and unit.
     * 
     * @return The Text representation of this parameter.
     */
    @Override
    public Text toText() {
        return _param.toText();
    }

    /**
     * Create a String representation of this parameter which consists of the parameter's
     * name + value + units.
     * 
     * @return A String representation of this parameter.
     */
    @Override
    public String toString() {
        return toText().toString();
    }
}
