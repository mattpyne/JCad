/**
 * DataElement -- Defines the interface in common to all data elements..
 *
 * Copyright (C) 2003-2015, by Joseph A. Huwaldt. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA 02111-1307, USA. Or visit: http://www.gnu.org/licenses/lgpl.html
 */
package jahuwaldt.js.datareader;

import java.util.ResourceBundle;

/**
 * Defines the interface in common to all data elements.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: March 5, 2003
 * @version October 15, 2015
 */
public interface DataElement extends Comparable<DataElement> {

    /**
     * The resource bundle for this package.
     */
    public static final ResourceBundle RESOURCES = DataReader.RESOURCES;
    
    /**
     * Return the name of this data element.
     */
    public CharSequence getName();

    /**
     * Change the name of this data element to the specified name.
     */
    public void setName(CharSequence newName);

    /**
     * Return any user defined object associated with this data element. If there is no
     * user data, then null is returned.
     */
    public Object getUserObject();

    /**
     * Set the user defined object associated with this data element. This can be used to
     * store any type of information with a data element that could be useful.
     */
    public void setUserObject(Object data);
}
