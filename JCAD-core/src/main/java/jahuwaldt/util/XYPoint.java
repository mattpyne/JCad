/*
 * XYPoint  -- The interface in common to any data type that is to be stored in a PointRegionQuadTree.
 *
 * Copyright (C) 2018, by Joseph A. Huwaldt
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package jahuwaldt.util;

/**
 * The interface required of any data type that is to be stored in a PointRegionQuadTree.
 *
 * <p> Modified by: Joseph A. Huwaldt </p>
 *
 * @author Joseph A. Huwaldt, Date: February 5, 2018
 * @version February 5, 2018
 * @see PointRegionQuadTree
 */
public interface XYPoint {
    
    /**
     * Returns the X coordinate of this XYPoint in double precision.
     * 
     * @return The X coordinate of this point.
     */
    public double getX();
    
    /**
     * Returns the Y coordinate of this XYPoint in double precision.
     * 
     * @return The Y coordinate of this point.
     */
    public double getY();
}
