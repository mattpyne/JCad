package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.ArrayList;
import java.util.List;

public class GlobalUnitAssignedContext_stp extends StepEntity {

    public static String ENTITY_TYPE = "GLOBAL_UNIT_ASSIGNED_CONTEXT";

    private List<StepReference> unitsRef;

    public GlobalUnitAssignedContext_stp(List<StepReference> unitsRef) {
        super("");
        this.unitsRef = unitsRef;
    }

    public List<StepReference> getUnitsRef() {
        return unitsRef;
    }

    public List<Unit_stp> getUnits() {
        List<Unit_stp> units = new ArrayList<>();

        for (StepReference ref : unitsRef) {
            if (!(ref.getEntity() instanceof Unit_stp)) {
                Debug.log(Debug.LEVEL.ERROR, "Some how not a unit. Ref: " + ref.getRef());
            } else {
                units.add((Unit_stp) ref.getEntity());
            }
        }

        return units;
    }
}
