package org.pyne.jcad.cadexchange.step;

public class FaceOuterBound_stp extends FaceBound_stp {

    public static final String ENTITY_TYPE = "FACE_OUTER_BOUND";

    public FaceOuterBound_stp(String name, StepReference edgeLoop, boolean orientation) {
        super(name, edgeLoop, orientation);
    }
}
