package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class FaceBound_stp extends StepEntity {

    public static final String ENTITY_TYPE = "FACE_BOUND";
    
    private StepReference edgeLoop;
    private boolean orientation;

    public FaceBound_stp(String name, StepReference edgeLoop, boolean orientation) {
        super(name);
        this.edgeLoop = edgeLoop;
        this.orientation = orientation;
    }

    public EdgeLoop_stp edgeLoop() {
        if (!(edgeLoop.getEntity() instanceof EdgeLoop_stp)) {

            throw new IllegalArgumentException("Some how we do not have a EdgeLoop_stp");
        }

        return (EdgeLoop_stp) edgeLoop.getEntity();
    }

    public boolean orientation() {
        return orientation;
    }

}
