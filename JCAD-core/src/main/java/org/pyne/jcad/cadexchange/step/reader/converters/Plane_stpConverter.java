package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.pyne.jcad.cadexchange.step.Axis2Placement3D_stp;
import org.pyne.jcad.cadexchange.step.CartesianPoint_stp;
import org.pyne.jcad.cadexchange.step.Direction_stp;
import org.pyne.jcad.cadexchange.step.Plane_stp;
import org.pyne.jcad.cadexchange.step.Surface_stp;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.geom.PlanarSurface;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
public class Plane_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof Plane_stp)) {

            return Optional.empty();
        }

        Plane_stp stpPlane = (Plane_stp) stpSurface;

        Axis2Placement3D_stp centerPlacement = stpPlane.position();
        CartesianPoint_stp point = centerPlacement.location();
        Direction_stp axis = centerPlacement.axis();
        Direction_stp refAxis = centerPlacement.refAxis();

        Array<Number> pointArray = Array.from(point.x(), point.y(), point.z());
        Array<Number> planeNormal = Array.from(axis.x(), axis.y(), axis.z());
        
        Array<Number> yAxisArray = Array.from(refAxis.x(), refAxis.y(), refAxis.z());
        Array<Number> xAxisArray = Vec.cross(planeNormal, yAxisArray);

        List<Point3DC> points = new ArrayList<>();

        points.add(new Point3DC(
                Vec.add(pointArray,
                        Vec.mul(1000, xAxisArray))));
        points.add(new Point3DC(
                Vec.add(pointArray,
                        Vec.mul(1000, yAxisArray))));
        points.add(new Point3DC(
                Vec.add(pointArray,
                        Vec.mul(-1000, xAxisArray))));
        points.add(new Point3DC(
                Vec.add(pointArray,
                        Vec.mul(-1000, yAxisArray))));

        Point3DC normalP = new Point3DC(planeNormal);
        BrepSurface planeSurf = BrepBuilder.createBoundingNurbs(
                points, 
                new PlanarSurface(normalP, points.get(0).dot(normalP)));

        return Optional.of(planeSurf.impl());
    }

}
