package org.pyne.jcad.cadexchange.step;

public class ManifoldSolidBrep_stp extends StepEntity {

    public static final String ENTITY_TYPE = "MANIFOLD_SOLID_BREP";

    private StepReference shell;

    public ManifoldSolidBrep_stp(String name, StepReference shell) {
        super(name);
        this.shell = shell;
    }

    public ClosedShell_stp getShell() {
        if (!(shell.getEntity() instanceof ClosedShell_stp)) {

            throw new IllegalArgumentException("Some how we do not have a ClosedShell_stp");
        }

        return (ClosedShell_stp) shell.getEntity();
    }

}
