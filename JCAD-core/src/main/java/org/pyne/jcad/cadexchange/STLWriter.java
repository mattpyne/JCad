package org.pyne.jcad.cadexchange;

import eu.mihosoft.jcsg.CSG;
import eu.mihosoft.jcsg.FileUtil;
import org.pyne.jcad.core.csg.BrepToCSG;
import org.pyne.jcad.core.math.brep.Shell;

import java.io.File;
import java.io.IOException;
import org.pyne.jcad.core.math.brep.Topo;

public class STLWriter {

    public static void write(Topo shell, File file) throws IOException {

        if (null != file) {

            CSG csg = new BrepToCSG().fromBrep((Shell) shell);

            FileUtil.write(file.toPath(), csg.toStlString());
        }
    }

}
