package org.pyne.jcad.cadexchange.step;

import java.util.List;

public class SurfaceCurve_stp extends Curve_stp {

    public final static String ENTITY_TYPE = "SURFACE_CURVE";

    private StepReference curve3DRef;
    private List<StepReference> associatedGeometry;
    private StepReference masterRepresentation;

    public SurfaceCurve_stp(
            String name,
            StepReference curve3D,
            List<StepReference> associatedGeometry,
            StepReference masterRepresentation) {
        super(name);
        this.curve3DRef = curve3D;
        this.associatedGeometry = associatedGeometry;
        this.masterRepresentation = masterRepresentation;
    }

    public StepReference curve3DRef() {
        return curve3DRef;
    }

    public Curve_stp curve3D() {
        if (!(curve3DRef.getEntity() instanceof Curve_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Curve_stp");
        }

        return (Curve_stp) curve3DRef.getEntity();
    }

}
