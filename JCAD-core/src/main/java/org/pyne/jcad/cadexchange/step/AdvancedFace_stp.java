package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Matthew
 */
public class AdvancedFace_stp extends StepEntity {

    public static final String ENTITY_TYPE = "ADVANCED_FACE";

    private Set<StepReference> face;
    private StepReference surface;
    private boolean sameSence;

    public AdvancedFace_stp(String name, Set<StepReference> face, StepReference surface, boolean sameSence) {
        super(name);
        this.face = face;
        this.surface = surface;
        this.sameSence = sameSence;
    }

    public boolean sameSence() {
        return sameSence;
    }

    public List<FaceBound_stp> face() {
        return face.stream()
                .map(StepReference::getEntity)
                .filter(Objects::nonNull)
                .filter(FaceBound_stp.class::isInstance)
                .map(FaceBound_stp.class::cast)
                .collect(Collectors.toList());
    }

    public Surface_stp surface() {

        if (!(surface.getEntity() instanceof Surface_stp)) {

            Debug.log(Debug.LEVEL.ERROR, "Some how we do not have a Surface");
        }

        return (Surface_stp) surface.getEntity();
    }

}
