package org.pyne.jcad.cadexchange.step;

import java.util.List;
import java.util.Optional;

public class SurfaceSideStyle_stp extends StepEntity {

    public final static String ENTITY_TYPE = "SURFACE_SIDE_STYLE";

    private List<StepReference> surfaceStyleRefs;

    public SurfaceSideStyle_stp(
            String name,
            List<StepReference> surfaceStyleRefs)
    {
        super(name);
        this.surfaceStyleRefs = surfaceStyleRefs;
    }

    public List<StepReference> getSurfaceStyleRefs() {
        return surfaceStyleRefs;
    }

    public Optional<SurfaceStyleFillArea_stp> getColourStyle() {
        return surfaceStyleRefs.stream()
                .map(StepReference::getEntity)
                .filter(SurfaceStyleFillArea_stp.class::isInstance)
                .map(SurfaceStyleFillArea_stp.class::cast)
                .findAny();
    }
}