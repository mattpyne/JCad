package org.pyne.jcad.cadexchange.step.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 *
 * @author Matthew
 */
public class ClosedShellDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, ClosedShell_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }
        
        String name = entity.substring(entity.indexOf("(") + 1, entity.indexOf(","));
        String edgesGroup = entity.substring(entity.indexOf(",") + 2, entity.indexOf(")"));
        
        Matcher regexMatcher1 = StepRegex.REFS_PATTERN.matcher(edgesGroup);

        List<StepReference> faces = new ArrayList<>();
        while (regexMatcher1.find()) {
            faces.add(stepFile.getReference(regexMatcher1.group(1)));
        }

        return Optional.of(new ClosedShell_stp(name, faces));
    }

}
