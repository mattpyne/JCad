package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class MechanicalDesignGeometricPresentationRepresentation_stp extends StepEntity {

    public static final String ENTITY_TYPE = "MECHANICAL_DESIGN_GEOMETRIC_PRESENTATION_REPRESENTATION";

    private List<StepReference> itemRefs;
    private StepReference contextOfItemsRef;

    public MechanicalDesignGeometricPresentationRepresentation_stp(
            String name,
            List<StepReference> styleRefs,
            StepReference styledItemRef) {
        super(name);
        this.itemRefs = styleRefs;
        this.contextOfItemsRef = styledItemRef;
    }

    public List<StepReference> getItemRefs() {
        return itemRefs;
    }

    public Optional<StyledItem_stp> getStyledItem() {
        return itemRefs.stream()
                .map(StepReference::getEntity)
                .filter(StyledItem_stp.class::isInstance)
                .map(StyledItem_stp.class::cast)
                .findAny();
    }

    public StepReference getContextOfItemsRef() {
        return contextOfItemsRef;
    }

    public RepresentationContext_stp getContextOfItems() {
        if (!(contextOfItemsRef.getEntity() instanceof RepresentationContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "No context of items. Ref: " + contextOfItemsRef.getRef());
        }

        return (RepresentationContext_stp) contextOfItemsRef.getEntity();
    }
}
