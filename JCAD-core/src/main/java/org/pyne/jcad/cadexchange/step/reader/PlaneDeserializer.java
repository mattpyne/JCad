package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 *
 * @author Matthew
 */
public class PlaneDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Plane_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.INPUTS2_PATTERN.matcher(entity);
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = regexMatcher.group(1);
        String positionRef = regexMatcher.group(2);
        if (null == positionRef
                || null == name) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new Plane_stp(name, stepFile.getReference(positionRef)));
    }

}
