package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class Line_stp extends Curve_stp {

    public static final String ENTITY_TYPE = "LINE";

    private StepReference start;
    private StepReference direction;

    public Line_stp(StepReference start, StepReference direction) {
        super("");
        this.start = start;
        this.direction = direction;
    }

    public CartesianPoint_stp start() {

        if (!(start.getEntity() instanceof CartesianPoint_stp)) {

            throw new IllegalArgumentException("Some how we do not have a CartesianPoint_stp");
        }

        return (CartesianPoint_stp) start.getEntity();
    }

    public Vector_stp direction() {
        if (!(direction.getEntity() instanceof Vector_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Vector_stp");
        }

        return (Vector_stp) direction.getEntity();
    }

}
