package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.Optional;
import java.util.regex.Matcher;

public class SurfaceOfLinearExtrusionDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, SurfaceOfLinearExtrusion_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.INPUTS3_PATTERN.matcher(entity);
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = regexMatcher.group(1);
        String sweptCurveRef = regexMatcher.group(2);
        String extrusionRef = regexMatcher.group(3);
        if (null == sweptCurveRef
                || null == name
                || null == extrusionRef
        ) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(
                new SurfaceOfLinearExtrusion_stp(
                        name,
                        stepFile.getReference(sweptCurveRef),
                        stepFile.getReference(extrusionRef)));
    }

}
