package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class GeometricRepresentationContext3Deserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        int idxStartGeomRepCon = entity.indexOf(GeometricRepresentationContext3_stp.ENTITY_TYPE);
        if (idxStartGeomRepCon == -1) {
            return Optional.empty();
        }
        int idxStartGlobalUncerAssCon = entity.indexOf(GlobalUncertaintyAssignedContext_stp.ENTITY_TYPE);
        if (idxStartGlobalUncerAssCon == -1) {
            return Optional.empty();
        }
        int idxStartGlobalUnitAssCon = entity.indexOf(GlobalUnitAssignedContext_stp.ENTITY_TYPE);
        if (idxStartGlobalUnitAssCon == -1) {
            return Optional.empty();
        }
        int idxStartRepCon = entity.indexOf(RepresentationContext_stp.ENTITY_TYPE);
        if (idxStartRepCon < idxStartGeomRepCon + GeometricRepresentationContext3_stp.ENTITY_TYPE.length()) {
            idxStartRepCon = entity.lastIndexOf(RepresentationContext_stp.ENTITY_TYPE);
        }
        if (idxStartRepCon == -1) {
            return Optional.empty();
        }
        List<Integer> idxs = new ArrayList<>();
        idxs.add(idxStartGeomRepCon);
        idxs.add(idxStartGlobalUncerAssCon);
        idxs.add(idxStartGlobalUnitAssCon);
        idxs.add(idxStartRepCon);
        Collections.sort(idxs);

        int idxEndGeomRepCon = idxs.indexOf(idxStartGeomRepCon) + 1 >= idxs.size()
                ? entity.length()
                : idxs.get(idxs.indexOf(idxStartGeomRepCon) + 1);
        String geomRepConStr = entity.substring(idxStartGeomRepCon, idxEndGeomRepCon);
        List<String> geomRepConVars = StepRegex.variables(geomRepConStr);
        if (geomRepConVars.size() != 1 && !geomRepConVars.get(0).equals("3")) {

            return Optional.empty();
        }

        int idxEndGlobalUncerAssCon = idxs.indexOf(idxStartGlobalUncerAssCon) + 1 >= idxs.size()
                ? entity.length()
                : idxs.get(idxs.indexOf(idxStartGlobalUncerAssCon) + 1);
        String globalUncertaintyAssignedContextStr = entity.substring(idxStartGlobalUncerAssCon, idxEndGlobalUncerAssCon);

        int idxEndGlobalUnitAssCon = idxs.indexOf(idxStartGlobalUnitAssCon) + 1 >= idxs.size()
                ? entity.length()
                : idxs.get(idxs.indexOf(idxStartGlobalUnitAssCon) + 1);
        String globalUnitAssignedContextStr = entity.substring(idxStartGlobalUnitAssCon, idxEndGlobalUnitAssCon);

        int idxEndRepCon = idxs.indexOf(idxStartRepCon) + 1 >= idxs.size()
                ? entity.length()
                : idxs.get(idxs.indexOf(idxStartRepCon) + 1);
        String representationContextStr = entity.substring(idxStartRepCon, idxEndRepCon);

        List<String> repContextVariables = StepRegex.variables(representationContextStr);

        return Optional.of(new GeometricRepresentationContext3_stp(
                new GlobalUncertaintyAssignedContext_stp(StepRegex.getRefs(StepRegex.variables(globalUncertaintyAssignedContextStr).get(0), stepFile)),
                new GlobalUnitAssignedContext_stp(StepRegex.getRefs(StepRegex.variables(globalUnitAssignedContextStr).get(0), stepFile)),
                repContextVariables.get(0),
                repContextVariables.get(1)
        ));
    }

}

