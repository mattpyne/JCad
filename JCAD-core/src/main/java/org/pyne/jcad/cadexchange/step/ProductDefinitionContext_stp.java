package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ProductDefinitionContext_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT_DEFINITION_CONTEXT";
    private StepReference frameOfReferenceRef;
    private String lifeCycleStage;

    public ProductDefinitionContext_stp(
            String name,
            StepReference application,
            String lifeCycleStage) {
        super(name);
        this.lifeCycleStage = lifeCycleStage;
        this.frameOfReferenceRef = application;
    }

    public String getLifeCycleStage() {
        return lifeCycleStage;
    }

    public StepReference getFrameOfReferenceRef() {
        return frameOfReferenceRef;
    }

    public ApplicationContext_stp getFrameOfReference() {
        if(!(frameOfReferenceRef.getEntity() instanceof ApplicationContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no application found! Ref: " + frameOfReferenceRef.getRef());
        }

        return (ApplicationContext_stp) frameOfReferenceRef.getEntity();
    }
}