package org.pyne.jcad.cadexchange.step;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Matthew
 */
public class StepFile {

    private HashMap<String, StepReference> entities;

    private HashMap<String, List<? extends StepEntity>> cache;

    public StepFile() {
        this.entities = new HashMap<>();
        this.cache = new HashMap<>();
    }

    public StepReference getReference(String ref) {
        return entities.computeIfAbsent(ref.trim(), StepReference::new);
    }

    public void fillOutReference(String ref, StepEntity entity) {
        getReference(ref.trim()).setEntity(entity);
    }

    public <T extends StepEntity> List<T> getEntities(Class<T> entityClass) {
        if (cache.containsKey(entityClass.getSimpleName())) {
            return (List<T>) cache.get(entityClass.getSimpleName());
        }
        List<T> tEntities = entities.values()
                .stream()
                .map(StepReference::getEntity)
                .filter(entityClass::isInstance)
                .map(entityClass::cast)
                .collect(Collectors.toList());

        cache.put(entityClass.getSimpleName(), tEntities);
        return tEntities;
    }
}
