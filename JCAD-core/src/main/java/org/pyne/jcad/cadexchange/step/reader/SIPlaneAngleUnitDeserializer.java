package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.Optional;

public class SIPlaneAngleUnitDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!entity.contains("PLANE_ANGLE_UNIT") || !entity.contains("NAMED_UNIT") || !entity.contains("SI_UNIT")) {

            return Optional.empty();
        }

        String entityLowerCase = entity.toLowerCase();
        SIPrefix_stp prefix = SIPrefix_stp.NONE;
        for (SIPrefix_stp ea : SIPrefix_stp.values()) {
            if (entityLowerCase.contains(ea.toString())) {
                prefix = ea;
                break;
            }
        }

        SIUnitName_stp unitName = SIUnitName_stp.NONE;
        for (SIUnitName_stp ea : SIUnitName_stp.values()) {
            if (entityLowerCase.contains(ea.toString())) {
                unitName = ea;
                break;
            }
        }

        return Optional.of(new SIPlaneAngleUnit_stp(
                prefix,
                unitName
        ));
    }

}
