package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class OrientedEdge_stp extends StepEntity {

    public static final String ENTITY_TYPE = "ORIENTED_EDGE";
    
    private StepReference edge;
    private StepReference startVertex;
    private StepReference endVertex;
    private boolean orientation;

    public OrientedEdge_stp(
            String name,
            StepReference startVertex,
            StepReference endVertex,
            StepReference edge,
            boolean orientation) {
        
        super(name);
        this.edge = edge;
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        this.orientation = orientation;
    }

    public VertexPoint_stp startVertex() {
        if (null == startVertex || !(startVertex.getEntity() instanceof VertexPoint_stp)) {

            return edge().vertexA();
        }

        return (VertexPoint_stp) startVertex.getEntity();
    }

    public VertexPoint_stp endVertex() {
        if (null == endVertex || !(endVertex.getEntity() instanceof VertexPoint_stp)) {

            return edge().vertexB();
        }

        return (VertexPoint_stp) endVertex.getEntity();
    }

    public EdgeCurve_stp edge() {
        if (!(edge.getEntity() instanceof EdgeCurve_stp)) {

            throw new IllegalArgumentException("Some how we do not have a edge");
        }

        return (EdgeCurve_stp) edge.getEntity();
    }

    public boolean orientation() {
        return orientation;
    }

}
