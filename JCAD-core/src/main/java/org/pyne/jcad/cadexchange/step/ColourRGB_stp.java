package org.pyne.jcad.cadexchange.step;

public class ColourRGB_stp extends StepEntity {

    public final static String ENTITY_TYPE = "COLOUR_RGB";

    private double red;
    private double green;
    private double blue;

    public ColourRGB_stp(
            String name,
            double red,
            double green,
            double blue)
    {
        super(name);
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public double red() {
        return red;
    }

    public double green() {
        return green;
    }

    public double blue() {
        return blue;
    }

}
