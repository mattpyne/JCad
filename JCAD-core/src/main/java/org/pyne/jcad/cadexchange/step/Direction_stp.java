package org.pyne.jcad.cadexchange.step;

/**
 * @author Matthew
 */
public class Direction_stp extends StepEntity {

    public final static String ENTITY_TYPE = "DIRECTION";

    public final double x;
    public final double y;
    public final double z;

    public Direction_stp(double x, double y) {
        this(x, y, 0.);
    }

    public Direction_stp(double x, double y, double z) {
        super("");
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Direction_stp(String name, double x, double y, double z) {
        super(name);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double z() {
        return z;
    }

}
