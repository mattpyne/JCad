package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class VertexPoint_stp extends StepEntity {

    public static final String ENTITY_TYPE = "VERTEX_POINT";
    
    private StepReference point;

    public VertexPoint_stp(StepReference cartesianPoint) {
        super("");
        this.point = cartesianPoint;
    }
    
    public CartesianPoint_stp point() {
        if (!(point.getEntity() instanceof CartesianPoint_stp)){
            
            throw new IllegalArgumentException("Some how we do not have a CartesianPoint_stp");
        }
        
        return (CartesianPoint_stp) point.getEntity();
    }
}
