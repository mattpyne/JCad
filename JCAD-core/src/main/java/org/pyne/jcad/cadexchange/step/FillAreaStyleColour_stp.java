package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class FillAreaStyleColour_stp extends StepEntity {

    public final static String ENTITY_TYPE = "FILL_AREA_STYLE_COLOUR";

    private StepReference colourRef;

    public FillAreaStyleColour_stp(
            String name,
            StepReference colourRef)
    {
        super(name);
        this.colourRef = colourRef;
    }

    public StepReference getColourRef() {
        return colourRef;
    }

    public ColourRGB_stp getColour() {
        if (!(colourRef.getEntity() instanceof ColourRGB_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Don't have colour. Ref: " + colourRef.getRef());
        }

        return (ColourRGB_stp) colourRef.getEntity();
    }
}

