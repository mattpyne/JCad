package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.cadexchange.step.StepReference;
import org.pyne.jcad.cadexchange.step.SurfaceCurve_stp;

import java.util.List;

public class SeamCurve_stp extends SurfaceCurve_stp {

    public final static String ENTITY_TYPE = "SEAM_CURVE";

    public SeamCurve_stp(
            String name,
            StepReference curve3D,
            List<StepReference> associatedGeometry,
            StepReference masterRepresentation) {

        super(name, curve3D, associatedGeometry, masterRepresentation);
    }
}
