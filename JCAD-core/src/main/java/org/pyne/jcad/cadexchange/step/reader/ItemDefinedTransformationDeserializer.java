package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ItemDefinedTransformationDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, ItemDefinedTransformation_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 4) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        String description = variables.get(1);
        StepReference transformItem1Ref = stepFile.getReference(variables.get(2));
        StepReference transformItem2Ref = stepFile.getReference(variables.get(3));

        return Optional.of(
                new ItemDefinedTransformation_stp(
                        name,
                        description,
                        transformItem1Ref,
                        transformItem2Ref
                ));
    }
}