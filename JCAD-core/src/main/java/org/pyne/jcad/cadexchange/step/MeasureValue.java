package org.pyne.jcad.cadexchange.step;

public class MeasureValue extends StepEntity {

    public enum TYPE {
        ABSORBED_DOSE_MEASURE,                          // -- AP203E2/AP242
        ACCELERATION_MEASURE,                           // -- AP203E2/AP242
        AMOUNT_OF_SUBSTANCE_MEASURE,                    // -- AP203E2/AP214/AP232/AP242
        AREA_MEASURE,                                   // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
        CAPACITANCE_MEASURE,                            // -- AP203E2/AP242
        CELSIUS_TEMPERATURE_MEASURE,                    // -- AP203E2/AP214/AP232/AP242
        CONDUCTANCE_MEASURE,                            // -- AP203E2/AP242
        CONTEXT_DEPENDENT_MEASURE,                      // -- COMMON
        COUNT_MEASURE,                                  // -- COMMON
        DESCRIPTIVE_MEASURE,                            // -- COMMON
        DOSE_EQUIVALENT_MEASURE,                        // -- AP203E2/AP242
        ELECTRIC_CHARGE_MEASURE,                        // -- AP203E2/AP242
        ELECTRIC_CURRENT_MEASURE,                       // -- AP203E2/AP214/AP232/AP242
        ELECTRIC_POTENTIAL_MEASURE,                     // -- AP203E2/AP242
        ENERGY_MEASURE,                                 // -- AP203E2/AP242
        FORCE_MEASURE,                                  // -- AP203E2/AP242
        FREQUENCY_MEASURE,                              // -- AP203E2/AP242
        ILLUMINANCE_MEASURE,                            // -- AP203E2/AP242
        INDUCTANCE_MEASURE,                             // -- AP203E2/AP242
        LENGTH_MEASURE,                                 // -- COMMON
        LUMINOUS_FLUX_MEASURE,                          // -- AP203E2/AP242
        LUMINOUS_INTENSITY_MEASURE,                     // -- AP203E2/AP214/AP232/AP242
        MAGNETIC_FLUX_DENSITY_MEASURE,                  // -- AP203E2/AP242
        MAGNETIC_FLUX_MEASURE,                          // -- AP203E2/AP242
        MASS_MEASURE,                                   // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
        NONE,                                           // -- SPECIAL TO JCAD(KEEP TRACK OF FAILED IMPORTS)
        NON_NEGATIVE_LENGTH_MEASURE,                    // -- AP203E2/AP214/AP238 STEP-NC/AP242
        NUMERIC_MEASURE,                                // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
        PARAMETER_VALUE,                                // -- COMMON
        PLANE_ANGLE_MEASURE,                            // -- COMMON
        POSITIVE_LENGTH_MEASURE,                        // -- COMMON
        POSITIVE_PLANE_ANGLE_MEASURE,                   // -- COMMON
        POSITIVE_RATIO_MEASURE,                         // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
        POWER_MEASURE,                                  // -- AP203E2/AP242
        PRESSURE_MEASURE,                               // -- AP203E2/AP242
        RADIOACTIVITY_MEASURE,                          // -- AP203E2/AP242
        RATIO_MEASURE,                                  // -- COMMON
        RESISTANCE_MEASURE,                             // -- AP203E2/AP242
        SOLID_ANGLE_MEASURE,                            // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
        THERMODYNAMIC_TEMPERATURE_MEASURE,              // -- AP203E2/AP214/AP232/AP242
        TIME_MEASURE,                                   // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
        VELOCITY_MEASURE,                               // -- AP203E2/AP242
        VOLUME_MEASURE                                  // -- AP203E2/AP214/AP232/AP238 STEP-NC/AP242
    }

    private TYPE type;
    private double value;

    public MeasureValue(TYPE type, double value) {
        super("");
        this.type = type;
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public TYPE getType() {
        return type;
    }

}

