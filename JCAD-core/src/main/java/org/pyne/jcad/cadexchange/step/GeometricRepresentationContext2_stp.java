package org.pyne.jcad.cadexchange.step;

public class GeometricRepresentationContext2_stp extends RepresentationContext_stp {

    public static final String ENTITY_TYPE = "GEOMETRIC_REPRESENTATION_CONTEXT";

    private boolean parametric;

    public GeometricRepresentationContext2_stp(
            boolean parametric,
            String contextIdentifier,
            String contextType) {
        super(contextIdentifier, contextType);
        this.parametric = parametric;
    }

    public boolean isParametric() {
        return parametric;
    }

}