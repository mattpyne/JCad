package org.pyne.jcad.cadexchange.step;

public class Axis1Placement_stp extends StepEntity {

    public static final String ENTITY_TYPE = "AXIS1_PLACEMENT";

    private StepReference location;
    private StepReference axis;

    public Axis1Placement_stp(String name, StepReference location, StepReference direction) {
        super(name);
        this.location = location;
        this.axis = direction;
    }

    public CartesianPoint_stp location() {
        if (!(location.getEntity() instanceof CartesianPoint_stp)) {

            throw new IllegalArgumentException("Some how we don't have a CartiesianPoint");
        }

        return (CartesianPoint_stp) location.getEntity();
    }

    public Direction_stp axis() {
        if (!(axis.getEntity() instanceof Direction_stp)) {

            throw new IllegalArgumentException("Some how we don't have a Direction_stp");
        }

        return (Direction_stp) axis.getEntity();
    }

}
