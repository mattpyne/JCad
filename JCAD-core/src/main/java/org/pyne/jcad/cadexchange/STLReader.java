package org.pyne.jcad.cadexchange;

import eu.mihosoft.jcsg.CSG;
import eu.mihosoft.jcsg.STL;
import org.pyne.jcad.core.math.brep.Topo;
import org.pyne.jcad.core.math.brep.creators.PolygonBrepor;

import java.io.File;
import java.io.IOException;

public class STLReader {

    public static Topo read(File file) throws IOException {
        CSG csg = STL.file(file.toPath());

        return new PolygonBrepor(csg.getPolygons(), 0.1).execute();
    }
}
