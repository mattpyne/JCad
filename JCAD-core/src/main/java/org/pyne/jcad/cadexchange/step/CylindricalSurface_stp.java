package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class CylindricalSurface_stp extends Surface_stp {

    public final static String ENTITY_TYPE = "CYLINDRICAL_SURFACE";

    private StepReference position;
    private double radius;

    public CylindricalSurface_stp(String name, StepReference position, double radius) {
        super(name);
        this.position = position;
        this.radius = radius;
    }

    public double radius() {
        return radius;
    }

    public Axis2Placement3D_stp position() {
        if (!(position.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) position.getEntity();
    }

}
