package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.*;
import java.util.stream.Collectors;

public class BoundedBSplineCurveDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!entity.contains("BOUNDED_CURVE")){
            return Optional.empty();
        }

        List<String> varsBSplineCurve = StepRegex.variables(entity, "B_SPLINE_CURVE");
        List<String> varsBSplineCurveWithKnots = StepRegex.variables(entity, "B_SPLINE_CURVE_WITH_KNOTS");
//        List<String> varsCurve = StepRegex.variables(entity, "CURVE");
//        List<String> varsGeomRepItem = StepRegex.variables(entity, "GEOMETRIC_REPRESENTATION_ITEM");
        List<String> varsRationalBSplineCurve= StepRegex.variables(entity, "RATIONAL_B_SPLINE_CURVE");
//        List<String> varsRepItem= StepRegex.variables(entity, "REPRESENTATION_ITEM");

        int degree = Integer.parseInt(varsBSplineCurve.get(0));
        List<StepReference> controlPointsListRef = getControlPointsList(stepFile, varsBSplineCurve.get(1));
        String curveForm = varsBSplineCurve.get(2);
        boolean closedCurve = varsBSplineCurve.get(3).toLowerCase().equals(".t.");
        boolean selfIntersecting = varsBSplineCurve.get(2).toLowerCase().equals(".t.");
        List<Integer> knotMultiplicities = getKnotMultiplicities(varsBSplineCurveWithKnots.get(0));
        List<Double> knots = getKnots(varsBSplineCurveWithKnots.get(1));
        String knotSpec = varsBSplineCurveWithKnots.get(2);
        List<Double> weights = getWeights(varsRationalBSplineCurve.get(0));

        return Optional.of(
                new BoundedBSplineCurve(
                        "",
                        degree,
                        controlPointsListRef,
                        curveForm,
                        closedCurve,
                        selfIntersecting,
                        knotMultiplicities,
                        knots,
                        knotSpec,
                        weights));
    }

    private List<Double> getKnots(String knots) {
        knots = knots.replace("(", "");
        knots = knots.replace(")", "");
        return Arrays.asList(knots.split(",")).stream()
                .map(knotMult -> Double.parseDouble(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<Double> getWeights(String weights) {
        weights = weights.replace("(", "");
        weights = weights.replace(")", "");
        return Arrays.asList(weights.split(",")).stream()
                .map(weight -> Double.parseDouble(weight.trim()))
                .collect(Collectors.toList());
    }

    private List<Integer> getKnotMultiplicities(String knotMultiplicitiesStr) {
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace("(", "");
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace(")", "");
        return Arrays.asList(knotMultiplicitiesStr.split(",")).stream()
                .map(knotMult -> Integer.parseInt(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<StepReference> getControlPointsList(StepFile stepFile, String controlPointsListStr) {
        controlPointsListStr = controlPointsListStr.replace("(", "");
        controlPointsListStr = controlPointsListStr.replace(")", "");
        return Arrays.asList(controlPointsListStr.split(","))
                .stream()
                .map(controlPoint -> stepFile.getReference(controlPoint))
                .collect(Collectors.toList());
    }
}
