package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AdvancedBrepShapeRepresentation_stp extends ShapeRepresentation_stp {

    public static final String ENTITY_TYPE = "ADVANCED_BREP_SHAPE_REPRESENTATION";

    public AdvancedBrepShapeRepresentation_stp(
            String name,
            List<StepReference> itemRefs,
            StepReference contextOfItemsRef) {

        super(name, itemRefs, contextOfItemsRef);
    }

    public Axis2Placement3D_stp getPlacement() {
        Optional<Axis2Placement3D_stp> opPlacement = itemRefs.stream()
                .map(StepReference::getEntity)
                .filter(Axis2Placement3D_stp.class::isInstance)
                .map(Axis2Placement3D_stp.class::cast)
                .findFirst();

        if (!opPlacement.isPresent()) {
            Debug.log(Debug.LEVEL.ERROR, "Expected to have a Axis2Placement3D_stp");
            return null;
        }

        return opPlacement.get();
    }

    public List<ManifoldSolidBrep_stp> getShapes() {
        return itemRefs.stream()
                .map(StepReference::getEntity)
                .filter(ManifoldSolidBrep_stp.class::isInstance)
                .map(ManifoldSolidBrep_stp.class::cast)
                .collect(Collectors.toList());
    }

    public GeometricRepresentationContext3_stp getContextOfItems() {
        if (!(contextOfItemsRef.getEntity() instanceof GeometricRepresentationContext3_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Expected to have a GeometricRepresentationContext3_stp");
            return null;
        }

        return (GeometricRepresentationContext3_stp) contextOfItemsRef.getEntity();
    }
}