package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class SurfaceStyleFillAreaDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, SurfaceStyleFillArea_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 1) {
            Debug.log(Debug.LEVEL.ERROR, "Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new SurfaceStyleFillArea_stp(
                stepFile.getReference(variables.get(0))
        ));
    }

}