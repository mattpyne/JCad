package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.ArrayList;
import java.util.List;

public class Representation_stp extends StepEntity {

    protected List<StepReference> itemRefs;
    protected StepReference contextOfItemsRef;

    public Representation_stp(
            String name,
            List<StepReference> itemRefs,
            StepReference contextOfItemsRef) {

        super(name);
        this.itemRefs = itemRefs;
        this.contextOfItemsRef = contextOfItemsRef;
    }

    public List<StepReference> getItemRefs() {
        return itemRefs;
    }

    public List<StepEntity> getItems() {
        List<StepEntity> items = new ArrayList<>();

        for (StepReference itemRef : itemRefs) {
            items.add(itemRef.getEntity());
        }

        return items;
    }

    public StepReference getContextOfItemsRef() {
        return contextOfItemsRef;
    }

    public RepresentationContext_stp getContextOfItems() {
        if (!(contextOfItemsRef.getEntity() instanceof RepresentationContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Expected to have a RepresentationContext_stp");
            return null;
        }

        return (RepresentationContext_stp) contextOfItemsRef.getEntity();
    }
}