package org.pyne.jcad.cadexchange.step;

public class ContextDependentShapeRepresentation_stp extends StepEntity {

    public static final String ENTITY_TYPE = "CONTEXT_DEPENDENT_SHAPE_REPRESENTATION";

    private StepReference representationRelationRef;
    private StepReference representationProductRelationRef;

    public ContextDependentShapeRepresentation_stp(
            StepReference representationRelationRef,
            StepReference representationProductRelationRef) {
        super("");
        this.representationRelationRef = representationRelationRef;
        this.representationProductRelationRef = representationProductRelationRef;
    }

    public StepReference getRepresentationRelationRef() {
        return representationRelationRef;
    }

    public ShapeRepresentationRelationshipWithTransform_stp getRepresentationRelationship() {
        if (!(representationRelationRef.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a ShapeRepresentationRelationship_stp. Ref: " + representationRelationRef.getRef());
        }

        return (ShapeRepresentationRelationshipWithTransform_stp) representationRelationRef.getEntity();
    }


    public StepReference getRepresentationProductRelationRef() {
        return representationProductRelationRef;
    }

    public ProductDefinitionShape_stp getRepresentationProductRelation() {
        if (!(representationProductRelationRef.getEntity() instanceof ProductDefinitionShape_stp)) {

            throw new IllegalArgumentException("Some how we do not have a ProductDefinitionShape_stp. Ref: " + representationProductRelationRef.getRef());
        }

        return (ProductDefinitionShape_stp) representationProductRelationRef.getEntity();
    }
}
