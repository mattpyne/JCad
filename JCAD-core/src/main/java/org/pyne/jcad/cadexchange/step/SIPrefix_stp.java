package org.pyne.jcad.cadexchange.step;

public enum SIPrefix_stp {
    EXA,
    PETA,
    TERA,
    GIGA,
    MEGA,
    KILO,
    HECTO,
    DECA,
    DECI,
    CENTI,
    MILLI,
    MICRO,
    NANO,
    PICO,
    FEMTO,
    NONE,
    ATTO
}
