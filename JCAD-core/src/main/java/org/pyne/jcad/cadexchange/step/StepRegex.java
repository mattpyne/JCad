package org.pyne.jcad.cadexchange.step;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Matthew
 */
public class StepRegex {

    public static final String NUMBER = "[-+]?[0-9]*\\.?[0-9]*+(?:[eE][-+]?[0-9]+)?";
    public static final String NUMBERS = "(" + NUMBER + ")";
    public static final String REFS = "(#\\d+)";

    public static final Pattern VEC2D_PATTERN = Pattern.compile("\\((" + NUMBER + ")[\\s+]?,[\\s+]?(" + NUMBER + ")\\)");
    public static final Pattern VEC3D_PATTERN = Pattern.compile("\\((" + NUMBER + ")[\\s+]?,[\\s+]?(" + NUMBER + ")[\\s+]?,[\\s+]?(" + NUMBER + ")\\)");
    public static final Pattern REFS3_PATTERN = Pattern.compile(REFS + "[\\s+]?,[\\s+]?" + REFS + "[\\s+]?,[\\s+]?" + REFS);
    public static final Pattern BRAKET3_PATTERN = Pattern.compile("\\((.*)[\\s+]?,[\\s+]?(.*)[\\s+]?,[\\s+]?(.*)\\)");
    public static final Pattern INPUTS4_PATTERN = Pattern.compile("\\(([^,;]*)[\\s+]?,[\\s+]?([^,;]*)[\\s+]?,[\\s+]?([^,;]*)[\\s+]?,[\\s+]?([^,;]*)\\)");
    public static final Pattern INPUTS2_PATTERN = Pattern.compile("\\(([^,;]*)[\\s+]?,[\\s+]?([^,;]*)\\)");
    public static final Pattern INPUTS3_PATTERN = Pattern.compile("\\(([^,;]*)[\\s+]?,[\\s+]?([^,;]*)[\\s+]?,[\\s+]?([^,;]*)\\)");
    public static final Pattern INPUTS5_PATTERN = Pattern.compile("\\(([^,;]*)[\\s+]?,[\\s+]?([^,;]*)[\\s+]?,[\\s+]?([^,;]*)[\\s+]?,[\\s+]?([^,;]*)[\\s+]?,[\\s+]?([^,;]*)\\)");
    public static final Pattern REFS2_PATTERN = Pattern.compile("(" + REFS + ")[\\s+]?,[\\s+]?(" + REFS + ")");
    public static final Pattern REFS_PATTERN = Pattern.compile("(" + REFS + ")");
    public static final Pattern NUMBERS_PATTERN = Pattern.compile("(" + NUMBERS + ")");

    private static final Map<String, Pattern> ENTITY_PATTERN = new HashMap();

    public static List<String> variables(String entity) {
        List<String> variables = new ArrayList<>();
        int variableStart = 0;
        int bracketCount = 0;
        for (int i = 0; i < entity.length(); i++) {
            char c = entity.charAt(i);
            if (c == '(') {
                bracketCount++;
                if (bracketCount == 1) {
                    variableStart = i + 1;
                }
            } else if (c == ')') {
                bracketCount--;
                if (bracketCount == 0) {
                    variables.add(entity.substring(variableStart, i).trim());
                }
            } else if (bracketCount == 1 && c == ',') {
                variables.add(entity.substring(variableStart, i).trim());
                variableStart = i + 1;
            }

        }
        return variables;
    }

    public static List<String> variables(String entity, String internalEntity) {
        List<String> variables = new ArrayList<>();
        int variableStart = 0;
        int bracketCount = 0;
        int startIndex = entity.indexOf(" " + internalEntity + "(");
        if (startIndex == -1) {
            startIndex = entity.indexOf(" " + internalEntity + " (");
        }
        if (startIndex == -1) {
            startIndex = entity.indexOf(internalEntity + " (");
        }
        if (startIndex == -1) {
            startIndex = entity.indexOf(internalEntity + "(");
        }

        if (startIndex == -1) {
            return variables;
        }

        for (int i = startIndex; i < entity.length(); i++) {
            char c = entity.charAt(i);
            if (c == '(') {
                bracketCount++;
                if (bracketCount == 1) {
                    variableStart = i + 1;
                }
            } else if (c == ')') {
                bracketCount--;
                if (bracketCount == 0) {
                    variables.add(entity.substring(variableStart, i).trim());
                    break;
                }
            } else if (bracketCount == 1 && c == ',') {
                variables.add(entity.substring(variableStart, i).trim());
                variableStart = i + 1;
            }

        }
        return variables;
    }

    public static List<String> getSetSelects(String entity) {
        List<String> variables = new ArrayList<>();
        int variableStart = 0;
        int bracketCount = 0;
        for (int i = 0; i < entity.length(); i++) {
            char c = entity.charAt(i);
            if (c == '(') {
                bracketCount++;
                if (bracketCount == 1) {
                    variableStart = i + 1;
                }
            } else if (c == ')') {
                bracketCount--;
                if (bracketCount == 0) {
                    String result = entity.substring(variableStart, i).trim();
                    if (!result.isEmpty()) {

                        variables.add(result);
                    }
                }
            } else if (bracketCount == 1 && c == ' ') {

                String result = entity.substring(variableStart, i).trim();
                if (!result.isEmpty()) {

                    variables.add(result);
                }
                variableStart = i + 1;
            }

        }
        return variables;
    }

    public static List<StepReference> getRefs(String itemsStr, StepFile stepFile) {
        itemsStr = itemsStr.replace("(", "");
        itemsStr = itemsStr.replace(")", "");
        return Arrays.stream(itemsStr.split(","))
                .map(stepFile::getReference)
                .collect(Collectors.toList());
    }

    public static Pattern getEntityPattern(String entityType) {
        Pattern pattern = ENTITY_PATTERN.get(entityType);

        if (null == pattern) {
            pattern = Pattern.compile(
                    "(\\#\\d+)\\s*=\\s*" + entityType + "\\s*\\("
            );
            ENTITY_PATTERN.put(entityType, pattern);
        }

        return pattern;
    }

    public static boolean isType(String entity, String type) {
        return getEntityPattern(type)
                .matcher(entity)
                .lookingAt();
    }
}
