package org.pyne.jcad.cadexchange.step;

public enum SurfaceSide_stp {
    POSITIVE,
    NEGATIVE,
    BOTH,
    NONE
}
