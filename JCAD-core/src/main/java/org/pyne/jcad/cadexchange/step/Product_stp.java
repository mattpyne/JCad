package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class Product_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT";

    private String id;
    private String description;
    private StepReference frameOfReferenceRef;

    public Product_stp(
            String id,
            String name,
            String description,
            StepReference frameOfReference) {
        super(name);
        this.id = id;
        this.description = description;
        this.frameOfReferenceRef = frameOfReference;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public StepReference getFrameOfReferenceRef() {
        return frameOfReferenceRef;
    }

    public ApplicationContext_stp getFrameOfReference() {
        if(!(frameOfReferenceRef.getEntity() instanceof ApplicationContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no frame of reference found! Ref: " + frameOfReferenceRef.getRef());
        }

        return (ApplicationContext_stp) frameOfReferenceRef.getEntity();
    }
}