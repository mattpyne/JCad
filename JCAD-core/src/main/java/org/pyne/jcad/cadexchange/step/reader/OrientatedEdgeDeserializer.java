package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

/**
 *
 * @author Matthew
 */
public class OrientatedEdgeDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, OrientedEdge_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);
        if (vars.size() != 5) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new OrientedEdge_stp(
                vars.get(0),
                stepFile.getReference(vars.get(1)),
                stepFile.getReference(vars.get(2)),
                stepFile.getReference(vars.get(3)),
                vars.get(4).toLowerCase().equals(".t.")
        ));
    }

}
