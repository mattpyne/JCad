package org.pyne.jcad.cadexchange.step.reader;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 *
 * @author Matthew
 */
public class AdvancedFaceDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, AdvancedFace_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }


        String name = entity.substring(entity.indexOf("(") + 1, entity.indexOf(","));
        String faceRef = entity.substring(entity.indexOf(",") + 2, entity.indexOf(")"));

        String remaining = entity.substring(entity.indexOf(")") + 2);
        String surfaceRef = remaining.substring(0, remaining.indexOf(",")).trim();
        String sameSense = remaining;

        Matcher regexMatcher1 = StepRegex.REFS_PATTERN.matcher(faceRef);

        Set<StepReference> face = new HashSet<>();
        while (regexMatcher1.find()) {
            
            face.add(stepFile.getReference(regexMatcher1.group(1)));
        }

        if (face.isEmpty()) {
            
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }
        
        return Optional.of(new AdvancedFace_stp(
                name,
                face,
                stepFile.getReference(surfaceRef),
                sameSense.contains(".T.")));
    }

}
