package org.pyne.jcad.cadexchange.step;

import java.util.List;
import java.util.Optional;

public class FillAreaStyle_stp extends StepEntity {

    public final static String ENTITY_TYPE = "FILL_AREA_STYLE";

    private List<StepReference> fillStyleRefs;

    public FillAreaStyle_stp(
            String name,
            List<StepReference> fillStyles)
    {
        super(name);
        this.fillStyleRefs = fillStyleRefs;
    }

    public List<StepReference> getFillStyleRefs() {
        return fillStyleRefs;
    }

    public Optional<FillAreaStyleColour_stp> getColourStyle() {
        return fillStyleRefs.stream()
                .map(StepReference::getEntity)
                .filter(FillAreaStyleColour_stp.class::isInstance)
                .map(FillAreaStyleColour_stp.class::cast)
                .findAny();
    }
}