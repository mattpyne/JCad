package org.pyne.jcad.cadexchange.step;

public class ShapeRepresentationRelationship_stp extends RepresentationRelationship_stp {

    public static final String ENTITY_TYPE = "SHAPE_REPRESENTATION_RELATIONSHIP";

    public ShapeRepresentationRelationship_stp(String name, String description, StepReference rep1Ref, StepReference rep2Ref) {
        super(name, description, rep1Ref, rep2Ref);
    }
}
