package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 *
 * @author Matthew
 */
public class VectorDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Vector_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.REFS_PATTERN.matcher(entity);

        regexMatcher.find();
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String ref = regexMatcher.group(1);
        if (null == ref) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        double amount = Double.parseDouble(entity.substring(entity.lastIndexOf(",") + 1, entity.lastIndexOf(")")));

        return Optional.of(new Vector_stp(stepFile.getReference(ref), amount));
    }

}
