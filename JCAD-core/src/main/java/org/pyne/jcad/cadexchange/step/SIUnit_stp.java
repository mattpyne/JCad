package org.pyne.jcad.cadexchange.step;

public class SIUnit_stp extends Unit_stp {

    private SIPrefix_stp prefix;
    private SIUnitName_stp unitName;

    SIUnit_stp(SIPrefix_stp prefix, SIUnitName_stp unitName) {
        this.prefix = prefix;
        this.unitName = unitName;
    }

    public SIPrefix_stp getPrefix() {
        return prefix;
    }

    public SIUnitName_stp getUnitName() {
        return unitName;
    }
}
