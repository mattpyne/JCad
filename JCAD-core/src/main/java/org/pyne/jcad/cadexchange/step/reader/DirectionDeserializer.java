package org.pyne.jcad.cadexchange.step.reader;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

import static org.pyne.jcad.cadexchange.step.StepRegex.VEC2D_PATTERN;
import static org.pyne.jcad.cadexchange.step.StepRegex.VEC3D_PATTERN;

/**
 * @author Matthew
 */
public class DirectionDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Direction_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);

        String name = vars.get(0);

        List<String> coordinates = StepRegex.variables(vars.get(1));

        if (coordinates.size() == 3) {
            double x = Double.parseDouble(coordinates.get(0));
            double y = Double.parseDouble(coordinates.get(1));
            double z = Double.parseDouble(coordinates.get(2));

            return Optional.of(new Direction_stp(name, x, y, z));
        } else if (coordinates.size() == 2) {
            double x = Double.parseDouble(coordinates.get(0));
            double y = Double.parseDouble(coordinates.get(1));

            return Optional.of(new Direction_stp(x, y));
        }

        return Optional.empty();
    }

}
