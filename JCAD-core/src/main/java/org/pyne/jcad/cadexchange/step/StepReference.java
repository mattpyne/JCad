package org.pyne.jcad.cadexchange.step;

import java.util.Objects;

/**
 *
 * @author Matthew
 */
public class StepReference {

    private final String ref;
    private StepEntity entity;

    public StepReference(String ref) {
        this.ref = ref;
    }

    public StepEntity getEntity() {
        return entity;
    }

    public void setEntity(StepEntity entity) {
        this.entity = entity;
    }

    public String getRef() {
        return ref;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.ref);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StepReference other = (StepReference) obj;
        if (!Objects.equals(this.ref, other.ref)) {
            return false;
        }
        return true;
    }

}
