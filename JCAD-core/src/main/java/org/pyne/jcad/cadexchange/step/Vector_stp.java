package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class Vector_stp extends StepEntity {

    public final static String ENTITY_TYPE = "VECTOR";

    private StepReference direction;
    private double amount;
    
    public Vector_stp(StepReference direction, double amount) {
        super("");
        this.direction = direction;
        this.amount = amount;
    }

    public Direction_stp direction() {
        if (!(direction.getEntity() instanceof Direction_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Direction_stp");
        }

        return (Direction_stp) direction.getEntity();
    }
    
    public double amount() {
        return amount;
    }
}
