package org.pyne.jcad.cadexchange.step;

import java.util.ArrayList;
import java.util.List;

public class DefinitionalRepresentation_stp extends StepEntity {

    public final static String ENTITY_TYPE = "DEFINITIONAL_REPRESENTATION";

    private List<StepReference> itemsRef;
    private StepReference contextOfItemsRef;

    public DefinitionalRepresentation_stp(
            String name,
            List<StepReference> itemsRef,
            StepReference contextOfItemsRef) {
        super(name);
        this.itemsRef = itemsRef;
        this.contextOfItemsRef = contextOfItemsRef;
    }

    public List<StepReference> itemsRef() {
        return itemsRef;
    }

    public StepReference contextOfItemsRef() {
        return contextOfItemsRef;
    }

    public List<Curve_stp> items() {
        List<Curve_stp> controlPointsList = new ArrayList<>();

        for (StepReference itemRef : itemsRef) {

            if (itemRef.getEntity() instanceof Curve_stp) {
                controlPointsList.add((Curve_stp) itemRef.getEntity());
            } else {

                throw new IllegalArgumentException("Some how we do not have a Curve_stp");
            }

        }

        return controlPointsList;
    }

}