package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.Axis2Placement2D_stp;
import org.pyne.jcad.core.math.Axis3;
import org.pyne.jcad.core.math.Point3DC;

public class Axis2Placement2D_stpConverter {
    public Axis3 convert(Axis2Placement2D_stp a1) {
        Point3DC dir1 = new Point3DC(a1.axis().x, a1.axis().y, 0.);
        Point3DC dir2 = new Point3DC(a1.axis().y, a1.axis().x, 0.);
        Point3DC dir3 = dir1.crossed(dir2);
        Point3DC location = new Point3DC(a1.location().x(), a1.location().y(), a1.location().z());

        return new Axis3(dir1, dir2, dir3, location);
    }
}
