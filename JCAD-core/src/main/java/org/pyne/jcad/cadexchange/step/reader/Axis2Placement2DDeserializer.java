package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.List;
import java.util.Optional;

public class Axis2Placement2DDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Axis2Placement2D_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (3 != variables.size()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new Axis2Placement2D_stp(
                variables.get(0),
                stepFile.getReference(variables.get(1)),
                stepFile.getReference(variables.get(2))
        ));
    }

}
