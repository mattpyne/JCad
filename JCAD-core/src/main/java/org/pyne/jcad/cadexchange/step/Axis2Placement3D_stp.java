package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class Axis2Placement3D_stp extends StepEntity {

    public static final String ENTITY_TYPE = "AXIS2_PLACEMENT_3D";
    
    private StepReference location;
    private StepReference axis;
    private StepReference refAxis;

    public Axis2Placement3D_stp(String name, StepReference location, StepReference axis, StepReference refAxis) {
        super(name);
        this.location = location;
        this.axis = axis;
        this.refAxis = refAxis;
    }

    public CartesianPoint_stp location() {
        if (!(location.getEntity() instanceof CartesianPoint_stp)) {

            throw new IllegalArgumentException("Some how we don't have a CartiesianPoint");
        }

        return (CartesianPoint_stp) location.getEntity();
    }

    public Direction_stp axis() {
        if (!(axis.getEntity() instanceof Direction_stp)) {

            throw new IllegalArgumentException("Some how we don't have a Direction_stp");
        }

        return (Direction_stp) axis.getEntity();
    }

    public Direction_stp refAxis() {
        if (!(refAxis.getEntity() instanceof Direction_stp)) {

            throw new IllegalArgumentException("Some how we don't have a Direction_stp");
        }

        return (Direction_stp) refAxis.getEntity();
    }

}
