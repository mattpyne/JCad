package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class ApplicationProtocolDefinitionDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, ApplicationProtocolDefinition_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 4) {
            Debug.log(Debug.LEVEL.ERROR,"Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new ApplicationProtocolDefinition_stp(
                variables.get(0),
                variables.get(1),
                Integer.parseInt(variables.get(2)),
                stepFile.getReference(variables.get(3))
        ));
    }

}