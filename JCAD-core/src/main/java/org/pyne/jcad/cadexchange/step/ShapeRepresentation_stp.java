package org.pyne.jcad.cadexchange.step;

import java.util.List;

public class ShapeRepresentation_stp extends Representation_stp {

    public static final String ENTITY_TYPE = "SHAPE_REPRESENTATION";

    public ShapeRepresentation_stp(
            String name,
            List<StepReference> itemRefs,
            StepReference contextOfItemsRef) {
        super(name, itemRefs, contextOfItemsRef);
    }
}
