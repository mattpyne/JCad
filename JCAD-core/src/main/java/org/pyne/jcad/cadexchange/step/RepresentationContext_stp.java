package org.pyne.jcad.cadexchange.step;

public class RepresentationContext_stp extends StepEntity {

    public static final String ENTITY_TYPE = "REPRESENTATION_CONTEXT";

    private String contextIdentifier;
    private String contextType;

    public RepresentationContext_stp(
            String contextIdentifier,
            String contextType) {
        super("");
        this.contextIdentifier = contextIdentifier;
        this.contextType = contextType;
    }

    public String getContextIdentifier() {
        return contextIdentifier;
    }

    public String getContextType() {
        return contextType;
    }
}