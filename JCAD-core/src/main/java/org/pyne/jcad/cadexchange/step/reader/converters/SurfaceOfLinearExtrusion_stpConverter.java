package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.Curve_stp;
import org.pyne.jcad.cadexchange.step.SurfaceOfLinearExtrusion_stp;
import org.pyne.jcad.cadexchange.step.Surface_stp;
import org.pyne.jcad.cadexchange.step.Vector_stp;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.Surface;
import org.pyne.jcad.core.math.geom.SurfacePrimitives;

import java.util.Optional;

public class SurfaceOfLinearExtrusion_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof SurfaceOfLinearExtrusion_stp)) {

            return Optional.empty();
        }

        SurfaceOfLinearExtrusion_stp surfaceOfLinearExtrusionStp = (SurfaceOfLinearExtrusion_stp) stpSurface;

        Curve_stp sweptCurve = surfaceOfLinearExtrusionStp.sweptCurve();
        Vector_stp extrusionAxis = surfaceOfLinearExtrusionStp.extrusionAxis();

        Optional<Curve> opSweptCurve = StpCurveToTopoCurveConverter.runAllConverts(sweptCurve);

        // INFO: extrusionAxis.amount() is not used for some reason even though STEP spec says it's a variable.
        Point3DC extrusion = new Point3DC(
                extrusionAxis.direction().x() * 1000,
                extrusionAxis.direction().y() * 1000,
                extrusionAxis.direction().z() * 1000);

        return Optional.of(
                SurfacePrimitives.linearExtrusion(
                        opSweptCurve.get().transform(Matrix3.createTranslation(extrusion.times(-1))),
                        extrusion));
    }

}
