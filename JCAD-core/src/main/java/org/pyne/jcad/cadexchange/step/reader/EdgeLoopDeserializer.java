package org.pyne.jcad.cadexchange.step.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

/**
 * @author Matthew
 */
public class EdgeLoopDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, EdgeLoop_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);

        if (vars.size() != 2) {
            Debug.log(Debug.LEVEL.ERROR, "Failed to import EdgeLoop");
            return Optional.empty();
        }

        return Optional.of(
                new EdgeLoop_stp(
                        vars.get(0),
                        StepRegex.getRefs(vars.get(1), stepFile)
                )
        );
    }

}
