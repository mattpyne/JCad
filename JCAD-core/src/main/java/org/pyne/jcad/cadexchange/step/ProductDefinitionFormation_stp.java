package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ProductDefinitionFormation_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT_DEFINITION_FORMATION";

    private String id;
    private String description;
    private StepReference ofProductRef;

    public ProductDefinitionFormation_stp(
            String id,
            String description,
            StepReference ofProductRef) {
        super("");
        this.id = id;
        this.description = description;
        this.ofProductRef = ofProductRef;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public StepReference getOfProductRef() {
        return ofProductRef;
    }

    public Product_stp getOfProduct() {
        if(!(ofProductRef.getEntity() instanceof Product_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no product found! Ref: " + ofProductRef.getRef());
        }

        return (Product_stp) ofProductRef.getEntity();
    }
}