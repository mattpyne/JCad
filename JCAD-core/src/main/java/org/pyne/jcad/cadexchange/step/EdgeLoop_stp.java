package org.pyne.jcad.cadexchange.step;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Matthew
 */
public class EdgeLoop_stp extends StepEntity {

    public static final String ENTITY_TYPE = "EDGE_LOOP";
    
    private List<StepReference> edges;
    
    public EdgeLoop_stp(String name, List<StepReference> edges) {
        super(name);
        this.edges = edges;
    }

    public List<OrientedEdge_stp> edges(boolean reverse) {
        List<OrientedEdge_stp> edges = this.edges.stream()
                .map(StepReference::getEntity)
                .filter(Objects::nonNull)
                .filter(OrientedEdge_stp.class::isInstance)
                .map(OrientedEdge_stp.class::cast)
                .collect(Collectors.toList());

        if (reverse) {
            Collections.reverse(edges);
        }

        return edges;
    }

}
