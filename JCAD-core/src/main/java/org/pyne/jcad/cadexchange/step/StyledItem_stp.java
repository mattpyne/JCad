package org.pyne.jcad.cadexchange.step;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class StyledItem_stp extends StepEntity {

    public static final String ENTITY_TYPE = "STYLED_ITEM";

    private List<StepReference> styleRefs;
    private StepReference styledItemTargetRef;

    public StyledItem_stp(
            String name,
            List<StepReference> styleRefs,
            StepReference styledItemRef) {
        super(name);
        this.styleRefs = styleRefs;
        this.styledItemTargetRef = styledItemRef;
    }

    public List<StepReference> getStyleRefs() {
        return styleRefs;
    }

    public StepReference getStyledItemTargetRef() {
        return styledItemTargetRef;
    }

    public StepEntity getStyledItemTarget() {
        return styledItemTargetRef.getEntity();
    }

    public Optional<SurfaceStyleUsage_stp> getSurfaceStyleUsage() {
        return styleRefs.stream()
                .flatMap(styleRef -> {
                    if (styleRef.getEntity() instanceof PresentationStyleAssignment_stp) {
                        return ((PresentationStyleAssignment_stp)styleRef.getEntity()).getStyleRefs().stream();
                    }
                    return Stream.of(styleRef);
                })
                .map(StepReference::getEntity)
                .filter(SurfaceStyleUsage_stp.class::isInstance)
                .map(SurfaceStyleUsage_stp.class::cast)
                .findAny();
    }

    public Optional<CurveStyle_stp> getCurveStyle() {
        return styleRefs.stream()
                .flatMap(styleRef -> {
                    if (styleRef.getEntity() instanceof PresentationStyleAssignment_stp) {
                        return ((PresentationStyleAssignment_stp)styleRef.getEntity()).getStyleRefs().stream();
                    }
                    return Stream.of(styleRef);
                })
                .map(StepReference::getEntity)
                .filter(CurveStyle_stp.class::isInstance)
                .map(CurveStyle_stp.class::cast)
                .findAny();
    }
}
