package org.pyne.jcad.cadexchange.step.reader;

import eu.mihosoft.vvecmath.Vector3d;
import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.cadexchange.step.reader.converters.Axis2Placement3D_stpConverter;
import org.pyne.jcad.cadexchange.step.reader.converters.StpCurveToTopoCurveConverter;
import org.pyne.jcad.cadexchange.step.reader.converters.StpSurfaceToTopoSurfaceConverter;
import org.pyne.jcad.cadexchange.step.reader.converters.Unit_stpConverter;
import org.pyne.jcad.core.Config;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.Axis3;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.brep.*;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.units.LengthUnit;
import org.pyne.jcad.core.units.Unit;

import java.io.*;
import java.util.List;
import java.util.Objects;

import static org.pyne.jcad.cadexchange.step.reader.StepEntityDeserializer.isEntity;

/**
 * @author Matthew
 */
public class StepReader {

    private StepFile stepFile;
    private Topo result;

    private Long countOfEntitiesRead;
    private Long countOfEntitiesFailedToRead;


    /**
     *
     */
    public StepReader() {
        this.stepFile = new StepFile();
    }

    public Topo read(File file) throws IOException {

        countOfEntitiesFailedToRead = 0L;
        countOfEntitiesRead = 0L;
        result = null;

        if (canProcesses(file)) {

            creation(file);
            conversions();
        }

        Debug.log(Debug.LEVEL.INFO, "Total number of entities: " + countOfEntitiesRead);
        Debug.log(Debug.LEVEL.INFO, "Total number of entities failed to be read: " + countOfEntitiesFailedToRead);
        Debug.log(Debug.LEVEL.INFO, "Percentage successfully read: " + ((double) (countOfEntitiesRead - countOfEntitiesFailedToRead)) / ((double) countOfEntitiesRead) * 100.0);
        return result;
    }

    private void conversions() {
        List<AdvancedBrepShapeRepresentation_stp> advancedBrepShapeRepresentation_stps
                = stepFile.getEntities(AdvancedBrepShapeRepresentation_stp.class);

        if (advancedBrepShapeRepresentation_stps.size() != 1) {
            conversionsAdvancedBrepShapeReps(advancedBrepShapeRepresentation_stps);
            return;
        }

        result = conversions(advancedBrepShapeRepresentation_stps.get(0));
    }

    private Topo conversions(AdvancedBrepShapeRepresentation_stp advancedBrepShapeRepresentation) {
        List<ManifoldSolidBrep_stp> shapes = advancedBrepShapeRepresentation.getShapes();
        if (shapes.size() == 1) {
            return conversions(advancedBrepShapeRepresentation, shapes.get(0));
        }

        CompoundTopo compound = new CompoundTopo();

        for (ManifoldSolidBrep_stp shape : shapes) {
            compound.add(conversions(advancedBrepShapeRepresentation, shape));
        }

        return compound;
    }

    private Shell conversions(
            AdvancedBrepShapeRepresentation_stp advancedBrepShapeRepresentation,
            ManifoldSolidBrep_stp manifoldSolidBrepStp) {

        Shell shell = conversions(manifoldSolidBrepStp.getShell());
        applyIdIfAny(advancedBrepShapeRepresentation, manifoldSolidBrepStp, shell);
        applyTransformIfAny(advancedBrepShapeRepresentation, shell);
            /* TODO: Before turning on... fix view port scaling to be based on Shape dimensions
            applyScaleIfAny(advancedBrepShapeRepresentation, shell);
             */
        return shell;
    }

    private void applyTransformIfAny(AdvancedBrepShapeRepresentation_stp advancedBrepShapeRepresentation, Shell shell) {

        List<ShapeRepresentationRelationshipWithTransform_stp> shapeTransforms = stepFile.getEntities(ShapeRepresentationRelationshipWithTransform_stp.class);
        List<ShapeRepresentationRelationship_stp> shapeReps = stepFile.getEntities(ShapeRepresentationRelationship_stp.class);

        for (ShapeRepresentationRelationshipWithTransform_stp shapeTransform : shapeTransforms) {

            boolean thisIsShapeTransformToUse = false;
            if (shapeTransform.getRelationship().getRep1() == advancedBrepShapeRepresentation) {
                thisIsShapeTransformToUse = true;
            } else {
                for (ShapeRepresentationRelationship_stp shapeRep : shapeReps) {
                    if (shapeRep.getRep1() == shapeTransform.getRelationship().getRep2()
                            && shapeRep.getRep2() == advancedBrepShapeRepresentation) {
                        thisIsShapeTransformToUse = true;
                        break;
                    }
                }
            }

            if (thisIsShapeTransformToUse) {
                ItemDefinedTransformation_stp transform = shapeTransform.getTransformRelationship()
                        .getTransform();

                Axis2Placement3D_stpConverter axisConverter = new Axis2Placement3D_stpConverter();
                Axis3 axisTo = axisConverter.convert(transform.transformationItem1());
                Axis3 axisFrom = axisConverter.convert(transform.transformationItem2());

                shell.transform(Matrix3.createBetweenBasisTransform(axisFrom, axisTo));
                break;
            }
        }
    }

    private void applyIdIfAny(
            AdvancedBrepShapeRepresentation_stp advancedBrepShapeRepresentation,
            ManifoldSolidBrep_stp manifoldSolidBrepStp,
            Shell shell) {

        if (!manifoldSolidBrepStp.getName().isEmpty()
                || !manifoldSolidBrepStp.getName().toLowerCase().equals("none")
                || !manifoldSolidBrepStp.getName().equals("*")
                || !manifoldSolidBrepStp.getName().equals("$")) {
            shell.setId(manifoldSolidBrepStp.getName());
            return;
        }

        stepFile.getEntities(ShapeDefinitionRepresentation_stp.class)
                .stream()
                .filter(ea -> ea.getUsedRepresentation() == advancedBrepShapeRepresentation)
                .map(ShapeDefinitionRepresentation_stp::getDefinition)
                .filter(Objects::nonNull)
                .map(ProductDefinitionShape_stp::getDefinition)
                .filter(Objects::nonNull)
                .map(ProductDefinition_stp::getFormation)
                .filter(Objects::nonNull)
                .map(ProductDefinitionFormation_stp::getOfProduct)
                .filter(Objects::nonNull)
                .map(Product_stp::getId)
                .filter(Objects::nonNull)
                .findFirst()
                .ifPresent(shell::setId);
    }

    private void applyScaleIfAny(AdvancedBrepShapeRepresentation_stp advancedBrepShapeRepresentation, Shell shell) {
        GeometricRepresentationContext3_stp geomRepContext3 = advancedBrepShapeRepresentation.getContextOfItems();
        for (Unit_stp unit_stp : geomRepContext3.getUnits().getUnits()) {

            Unit unit = new Unit_stpConverter().convert(unit_stp);

            if (unit instanceof LengthUnit) {

                double scaleFactor = unit.calculateConversionFactorTo(Config.LENGTH_UNIT);

                if (!CadMath.eq(1., scaleFactor)) {
                    Debug.checkLoopsValid(shell);
                    Debug.checkEdgesValid(shell);
                    shell.transform(Matrix3.createScale(scaleFactor));
                }
            }
        }
    }

    private Shell conversions(ClosedShell_stp stpShell) {
        BrepBuilder builder = new BrepBuilder();

        for (AdvancedFace_stp stpFace : stpShell.faces()) {

            Surface_stp stpSurface = stpFace.surface();

            // Run surface converter
            BrepSurface surface = StpSurfaceToTopoSurfaceConverter.runAllConverts(stpSurface)
                    .map(BrepSurface::new)
                    .orElse(null);

            if (null == surface) {

                continue;
            }

            if (!stpFace.sameSence()) {
                surface = surface.invert();
            }
            builder.face(surface);

            FaceBound_stp outerLoop = findOuterLoop(stpFace);

            for (FaceBound_stp stpLoop : stpFace.face()) {

                if (outerLoop == stpLoop) {
                    builder.newOuterLoop();
                } else {
                    builder.newInnerLoop();
                }
                for (OrientedEdge_stp oEdge : stpLoop.edgeLoop().edges(!stpLoop.orientation())) {
                    EdgeCurve_stp edgeCurve = oEdge.edge();
                    Curve_stp stpCurve = edgeCurve.curve();

                    // Run curve converter
                    BrepCurve curve = StpCurveToTopoCurveConverter.runAllConverts(stpCurve)
                            .map(BrepCurve::new)
                            .orElse(null);

                    if (null == curve) {

                        continue;
                    }

                    CartesianPoint_stp stpVertexA = oEdge.startVertex().point();
                    CartesianPoint_stp stpVertexB = oEdge.endVertex().point();
                    Vertex vertexA = new Vertex(Vector3d.xyz(stpVertexA.x(), stpVertexA.y(), stpVertexA.z()));
                    Vertex vertexB = new Vertex(Vector3d.xyz(stpVertexB.x(), stpVertexB.y(), stpVertexB.z()));
                    double uA = curve.param(vertexA.point());
                    double uB = curve.param(vertexB.point());

                    // Bad line or no line
                    if (CadMath.eqEps(uA, uB) && curve.getType() == ParametricCurve.TYPE.LINE) {
                        curve = new BrepCurve(CurvePrimitives.line(vertexA.point(), vertexB.point()));
                    }

                    if (oEdge.orientation()) {

                        builder.edge(vertexA, vertexB, curve);
                    } else {
                        builder.edge(vertexB, vertexA, curve.invert());
                    }
                }

            }

        }
        return builder.build();
    }

    private FaceBound_stp findOuterLoop(AdvancedFace_stp stpFace) {
        List<FaceBound_stp> loops = stpFace.face();
        FaceBound_stp outerLoop = loops
                .stream()
                .filter(FaceOuterBound_stp.class::isInstance)
                .findFirst()
                .orElse(null);

        if (null == outerLoop) {
            if (loops.size() > 2) {
                boolean outerLoopSense = false;
                for (FaceBound_stp stpLoop : loops) {
                    boolean nextLoopSense = stpLoop.orientation() == stpFace.sameSence();
                    if (null == outerLoop || nextLoopSense != outerLoopSense) {
                        outerLoop = stpLoop;
                        outerLoopSense = nextLoopSense;
                    }
                }
            } else {
                outerLoop = loops.get(0);
            }
        }

        return outerLoop;
    }

    private void conversionsAdvancedBrepShapeReps(List<AdvancedBrepShapeRepresentation_stp> advancedBrepShapeRepresentation_stps) {

        result = new CompoundTopo();

        for (AdvancedBrepShapeRepresentation_stp stpShell : advancedBrepShapeRepresentation_stps) {
            Topo topo = conversions(stpShell);

            ((CompoundTopo) result).add(topo);
        }
    }

    private void conversions(List<ClosedShell_stp> stpShells) {

        result = new CompoundShell();

        for (ClosedShell_stp stpShell : stpShells) {
            Shell shell = conversions(stpShell);

            ((CompoundShell) result).add(shell);
        }
    }

    private void creation(File file) throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            StringBuilder nextRealLine = new StringBuilder();
            while ((line = br.readLine()) != null) {

                nextRealLine.append(line);

                if (nextRealLine.toString().endsWith(";")) {

                    countOfEntitiesRead++;
                    if (!deserialize(nextRealLine.toString())) {
                        countOfEntitiesFailedToRead++;

                        Debug.log(Debug.LEVEL.INFO, "Failed to read step entity: " + nextRealLine);
                    }
                    nextRealLine = new StringBuilder();
                }
            }
        }
    }

    private boolean deserialize(String serializedEntity) {
        if (isEntity(serializedEntity)) {

            return StepEntityDeserializer.runDeserializers(serializedEntity, stepFile).isPresent();
        }

        return false;
    }

    private static boolean canProcesses(File file) {
        return file.canRead()
                && (file.getName().toLowerCase().contains("stp") || file.getName().toLowerCase().contains("step"));
    }
}
