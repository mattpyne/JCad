package org.pyne.jcad.cadexchange.step.reader.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.pyne.jcad.cadexchange.step.Curve_stp;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.geom.Curve;

/**
 *
 * @author Matthew
 */
public abstract class StpCurveToTopoCurveConverter {

    private static final List<StpCurveToTopoCurveConverter> CONVERTERS;

    static {
        CONVERTERS = new ArrayList<>();
        CONVERTERS.add(new StpCircleToTopoCircleConverter());
        CONVERTERS.add(new StpLineToTopoLineConverter());
        CONVERTERS.add(new BSplineCurveWithKnots_stpConverter());
        CONVERTERS.add(new BoundedBSplineCurveConverter());
        CONVERTERS.add(new PCurve_stpConverter());
        CONVERTERS.add(new SurfaceCurve_stpConverter());
        CONVERTERS.add(new SeamCurve_stpConverter());
    }

    public abstract Optional<Curve> convert(Curve_stp stpCurve);

    public static Optional<Curve> runAllConverts(Curve_stp stpCurve) {
        for (StpCurveToTopoCurveConverter convert : CONVERTERS) {

            try {
                Optional<Curve> curve = convert.convert(stpCurve);
                if (curve.isPresent()) {

                    return curve;
                }
            } catch (IllegalArgumentException ex) {

                Debug.log(ex);
            }
        }

        Debug.log(Debug.LEVEL.ERROR,"Failed to convert " + stpCurve);

        return Optional.empty();
    }
}
