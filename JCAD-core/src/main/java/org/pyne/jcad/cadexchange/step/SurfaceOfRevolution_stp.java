package org.pyne.jcad.cadexchange.step;

public class SurfaceOfRevolution_stp extends Surface_stp {

    public final static String ENTITY_TYPE = "SURFACE_OF_REVOLUTION";

    private StepReference sweptCurveRef;

    private StepReference axisPositionRef;

    public SurfaceOfRevolution_stp(String name, StepReference sweptCurve, StepReference axisPosition) {
        super(name);
        this.sweptCurveRef = sweptCurve;
        this.axisPositionRef = axisPosition;
    }

    public Curve_stp sweptCurve() {
        if (!(sweptCurveRef.getEntity() instanceof Curve_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Curve_stp");
        }

        return (Curve_stp) sweptCurveRef.getEntity();
    }

    public Axis1Placement_stp axisPosition() {
        if (!(axisPositionRef.getEntity() instanceof Axis1Placement_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis1Placement_stp");
        }

        return (Axis1Placement_stp) axisPositionRef.getEntity();
    }


    public StepReference getSweptCurveRef() {
        return sweptCurveRef;
    }

    public StepReference getAxisPositionRef() {
        return axisPositionRef;
    }
}
