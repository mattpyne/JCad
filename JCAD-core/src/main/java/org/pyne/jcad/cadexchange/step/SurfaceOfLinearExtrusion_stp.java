package org.pyne.jcad.cadexchange.step;

public class SurfaceOfLinearExtrusion_stp extends Surface_stp {

    public final static String ENTITY_TYPE = "SURFACE_OF_LINEAR_EXTRUSION";

    private StepReference sweptCurveRef;

    private StepReference extrusionAxisRef;

    public SurfaceOfLinearExtrusion_stp(String name, StepReference sweptCurve, StepReference extrusionAxis) {
        super(name);
        this.sweptCurveRef = sweptCurve;
        this.extrusionAxisRef = extrusionAxis;
    }

    public Curve_stp sweptCurve() {
        if (!(sweptCurveRef.getEntity() instanceof Curve_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Curve_stp");
        }

        return (Curve_stp) sweptCurveRef.getEntity();
    }

    public Vector_stp extrusionAxis() {
        if (!(extrusionAxisRef.getEntity() instanceof Vector_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Vector_stp");
        }

        return (Vector_stp) extrusionAxisRef.getEntity();
    }


    public StepReference getSweptCurveRef() {
        return sweptCurveRef;
    }

    public StepReference getExtrusionAxisRef() {
        return extrusionAxisRef;
    }
}
