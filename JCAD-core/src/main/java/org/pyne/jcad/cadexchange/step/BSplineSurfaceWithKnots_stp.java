package org.pyne.jcad.cadexchange.step;

import java.util.ArrayList;
import java.util.List;

public class BSplineSurfaceWithKnots_stp extends Surface_stp {

    public static final String ENTITY_TYPE = "B_SPLINE_SURFACE_WITH_KNOTS";

    private int degreeU;
    private int degreeV;
    private List<List<StepReference>> controlPointsListRef;
    private String surfaceForm;
    private boolean closedU;
    private boolean closedV;
    private boolean selfIntersecting;
    private List<Integer> knotMultiplicitiesU;
    private List<Integer> knotMultiplicitiesV;
    private List<Double> knotsU;
    private List<Double> knotsV;
    private String knotSpec;

    public BSplineSurfaceWithKnots_stp(
            String name,
            int degreeU,
            int degreeV,
            List<List<StepReference>> controlPointsListRef,
            String surfaceForm,
            boolean closedU,
            boolean closedV,
            boolean selfIntersecting,
            List<Integer> knotMultiplicitiesU,
            List<Integer> knotMultiplicitiesV,
            List<Double> knotsU,
            List<Double> knotsV,
            String knotSpec) {

        super(name);
        this.degreeU = degreeU;
        this.degreeV = degreeV;
        this.controlPointsListRef = controlPointsListRef;
        this.surfaceForm = surfaceForm;
        this.closedU = closedU;
        this.closedV = closedV;
        this.selfIntersecting = selfIntersecting;
        this.knotMultiplicitiesU = knotMultiplicitiesU;
        this.knotMultiplicitiesV = knotMultiplicitiesV;
        this.knotsU = knotsU;
        this.knotsV = knotsV;
        this.knotSpec = knotSpec;
    }

    public int degreeU() {
        return degreeU;
    }

    public int degreeV() {
        return degreeV;
    }

    public List<List<StepReference>> controlPointsListRef() {
        return controlPointsListRef;
    }

    public String surfaceForm() {
        return surfaceForm;
    }

    public boolean isClosedU() {
        return closedU;
    }

    public boolean isClosedV() {
        return closedV;
    }

    public boolean isSelfIntersecting() {
        return selfIntersecting;
    }

    public List<Integer> knotMultiplicitiesU() {
        return knotMultiplicitiesU;
    }

    public List<Integer> knotMultiplicitiesV() {
        return knotMultiplicitiesV;
    }

    public List<Double> knotsU() {
        return knotsU;
    }

    public List<Double> knotsV() {
        return knotsV;
    }

    public String knotSpec() {
        return knotSpec;
    }

    public List<List<CartesianPoint_stp>> controlPointsList() {
        List<List<CartesianPoint_stp>> controlPointsUVList = new ArrayList<>();

        for (List<StepReference> controlPointsRef : controlPointsListRef) {

            List<CartesianPoint_stp> controlPointUList = new ArrayList<>();
            for (StepReference ctrlPointRef : controlPointsRef) {
                if (ctrlPointRef.getEntity() instanceof CartesianPoint_stp) {
                    controlPointUList.add((CartesianPoint_stp) ctrlPointRef.getEntity());
                } else {

                    throw new IllegalArgumentException("Some how we do not have a CartesianPoint_stp");
                }
            }

            controlPointsUVList.add(controlPointUList);
        }

        return controlPointsUVList;
    }

}
