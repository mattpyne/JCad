package org.pyne.jcad.cadexchange.step;

/**
 * @author Matthew
 */
public class Circle_stp extends Curve_stp {

    public final static String ENTITY_TYPE = "CIRCLE";

    private StepReference centerPlacement;
    private double radius;

    public Circle_stp(String name, StepReference centerPlacement, double radius) {
        super(name);
        this.centerPlacement = centerPlacement;
        this.radius = radius;
    }

    public boolean has2DNot3DPlacement() {
        return centerPlacement.getEntity() instanceof Axis2Placement2D_stp;
    }

    public Axis2Placement3D_stp centerPlacement3D() {

        if (!(centerPlacement.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) centerPlacement.getEntity();
    }

    public Axis2Placement2D_stp centerPlacement2D() {

        if (!(centerPlacement.getEntity() instanceof Axis2Placement2D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement2D_stp");
        }

        return (Axis2Placement2D_stp) centerPlacement.getEntity();
    }

    public double radius() {
        return radius;
    }

}
