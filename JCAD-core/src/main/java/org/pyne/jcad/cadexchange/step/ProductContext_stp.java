package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ProductContext_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT_CONTEXT";

    private String disciplineType;
    private StepReference frameOfReferenceRef;

    public ProductContext_stp(
            String name,
            String disciplineType,
            StepReference frameOfReference) {
        super(name);
        this.disciplineType = disciplineType;
        this.frameOfReferenceRef = frameOfReference;
    }

    public String getDisciplineType() {
        return disciplineType;
    }

    public StepReference getFrameOfReferenceRef() {
        return frameOfReferenceRef;
    }

    public ApplicationContext_stp getFrameOfReference() {
        if(!(frameOfReferenceRef.getEntity() instanceof ApplicationContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no frame of reference found! Ref: " + frameOfReferenceRef.getRef());
        }

        return (ApplicationContext_stp) frameOfReferenceRef.getEntity();
    }
}