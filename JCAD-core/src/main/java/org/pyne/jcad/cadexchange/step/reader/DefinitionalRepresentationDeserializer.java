package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DefinitionalRepresentationDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, DefinitionalRepresentation_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 3) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        List<StepReference> itemsRef = getItemsRef(stepFile, variables.get(1));
        StepReference contextOfItemsRef = stepFile.getReference(variables.get(2));

        return Optional.of(
                new DefinitionalRepresentation_stp(
                        name,
                        itemsRef,
                        contextOfItemsRef
                ));
    }

    private List<StepReference> getItemsRef(StepFile stepFile, String itemsStr) {
        itemsStr = itemsStr.replace("(", "");
        itemsStr = itemsStr.replace(")", "");
        return Arrays.asList(itemsStr.split(","))
                .stream()
                .map(controlPoint -> stepFile.getReference(controlPoint))
                .collect(Collectors.toList());
    }
}