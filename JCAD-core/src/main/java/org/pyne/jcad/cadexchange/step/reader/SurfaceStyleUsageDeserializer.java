package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class SurfaceStyleUsageDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, SurfaceStyleUsage_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 2) {
            Debug.log(Debug.LEVEL.ERROR, "Failed to deserialize " + entity);
            return Optional.empty();
        }

        String sideStr = variables.get(0);
        SurfaceSide_stp side = SurfaceSide_stp.NONE;
        for (SurfaceSide_stp ea : SurfaceSide_stp.values()) {
            if (sideStr.toLowerCase().contains(ea.toString().toLowerCase())) {
                side = ea;
                break;
            }
        }


        return Optional.of(new SurfaceStyleUsage_stp(
                side,
                stepFile.getReference(variables.get(1))
        ));
    }

}

