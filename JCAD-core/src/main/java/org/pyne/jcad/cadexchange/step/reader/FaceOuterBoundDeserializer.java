package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

public class FaceOuterBoundDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, FaceOuterBound_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);
        if (vars.size() != 3) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new FaceOuterBound_stp(
                vars.get(0),
                stepFile.getReference(vars.get(1)),
                vars.get(2).toLowerCase().equals(".t.")
        ));
    }

}
