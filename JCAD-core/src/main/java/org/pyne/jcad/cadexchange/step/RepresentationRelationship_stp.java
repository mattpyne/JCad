package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class RepresentationRelationship_stp extends StepEntity {

    public static final String ENTITY_TYPE = "REPRESENTATION_RELATIONSHIP";

    private String description;
    private StepReference rep1Ref;
    private StepReference rep2Ref;

    public RepresentationRelationship_stp(
            String name,
            String description,
            StepReference rep1Ref,
            StepReference rep2Ref) {
        super(name);
        this.description = description;
        this.rep1Ref = rep1Ref;
        this.rep2Ref = rep2Ref;
    }

    public String getDescription() {
        return description;
    }

    public StepReference getRep1Ref() {
        return rep1Ref;
    }

    public Representation_stp getRep1() {
        if (!(rep1Ref.getEntity() instanceof Representation_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how not Representation. Ref: " + rep1Ref.getRef());
        }
        return (Representation_stp) rep1Ref.getEntity();
    }

    public StepReference getRep2Ref() {
        return rep2Ref;
    }

    public Representation_stp getRep2() {
        if (!(rep2Ref.getEntity() instanceof Representation_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how not Representation. Ref: " + rep2Ref.getRef());
        }
        return (Representation_stp) rep2Ref.getEntity();
    }
}