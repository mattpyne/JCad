package org.pyne.jcad.cadexchange.step.reader;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 * @author Matthew
 */
public class CircleDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Circle_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);
        if (vars.size() != 3) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(
                new Circle_stp(
                        vars.get(0),
                        stepFile.getReference(vars.get(1)),
                        Double.parseDouble(vars.get(2))
                )
        );
    }

}
