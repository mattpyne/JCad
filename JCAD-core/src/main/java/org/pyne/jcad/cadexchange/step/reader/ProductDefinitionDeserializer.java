package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class ProductDefinitionDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, ProductDefinition_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 4) {
            Debug.log(Debug.LEVEL.ERROR,"Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new ProductDefinition_stp(
                variables.get(0),
                variables.get(1),
                stepFile.getReference(variables.get(2)),
                stepFile.getReference(variables.get(3))
        ));
    }

}
