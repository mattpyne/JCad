package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class SurfaceStyleUsage_stp extends StepEntity {

    public static final String ENTITY_TYPE = "SURFACE_STYLE_USAGE";

    private SurfaceSide_stp side;
    private StepReference surfaceSideStyleRef;

    public SurfaceStyleUsage_stp(
            SurfaceSide_stp side,
            StepReference surfaceSideStyleRef) {
        super("");
        this.side = side;
        this.surfaceSideStyleRef = surfaceSideStyleRef;
    }

    public SurfaceSide_stp getSide() {
        return side;
    }

    public StepReference getSurfaceSideStyleRef() {
        return surfaceSideStyleRef;
    }

    public SurfaceSideStyle_stp getSurfaceSideStyle() {
        if (!(surfaceSideStyleRef.getEntity() instanceof SurfaceSideStyle_stp)) {

            Debug.log(Debug.LEVEL.ERROR, "No surface side style. Ref: " + surfaceSideStyleRef.getRef());
        }

        return (SurfaceSideStyle_stp) surfaceSideStyleRef.getEntity();
    }
}
