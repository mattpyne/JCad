package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class GeometricRepresentationContext2Deserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        int idxStartGeomRepCon = entity.indexOf(GeometricRepresentationContext2_stp.ENTITY_TYPE);
        if (idxStartGeomRepCon == -1) {
            return Optional.empty();
        }
        int idxStartParaRepCon = entity.indexOf("PARAMETRIC_REPRESENTATION_CONTEXT");
        if (idxStartParaRepCon == -1) {
            return Optional.empty();
        }
        int idxStartRepCon = entity.indexOf(RepresentationContext_stp.ENTITY_TYPE);
        if (idxStartRepCon < idxStartGeomRepCon + GeometricRepresentationContext2_stp.ENTITY_TYPE.length()) {
            idxStartRepCon = entity.lastIndexOf(RepresentationContext_stp.ENTITY_TYPE);
        }
        if (idxStartRepCon == -1) {
            return Optional.empty();
        }
        List<Integer> idxs = new ArrayList<>();
        idxs.add(idxStartGeomRepCon);
        idxs.add(idxStartParaRepCon);
        idxs.add(idxStartRepCon);
        Collections.sort(idxs);

        int idxEndGeomRepCon = idxs.indexOf(idxStartGeomRepCon) + 1 >= idxs.size()
                ? entity.length()
                : idxs.get(idxs.indexOf(idxStartGeomRepCon) + 1);
        String geomRepConStr = entity.substring(idxStartGeomRepCon, idxEndGeomRepCon);
        List<String> geomRepConVars = StepRegex.variables(geomRepConStr);
        if (geomRepConVars.size() != 1 && !geomRepConVars.get(0).equals("2")) {

            return Optional.empty();
        }

        int idxEndRepCon = idxs.indexOf(idxStartRepCon) + 1 >= idxs.size()
                ? entity.length()
                : idxs.get(idxs.indexOf(idxStartRepCon) + 1);
        String representationContextStr = entity.substring(idxStartRepCon, idxEndRepCon);

        List<String> repContextVariables = StepRegex.variables(representationContextStr);


        return Optional.of(new GeometricRepresentationContext2_stp(
                entity.contains("PARAMETRIC_REPRESENTATION_CONTEXT"),
                repContextVariables.get(0),
                repContextVariables.get(1)
        ));
    }


}

