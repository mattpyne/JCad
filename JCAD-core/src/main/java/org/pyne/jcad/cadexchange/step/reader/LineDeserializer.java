package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 *
 * @author Matthew
 */
public class LineDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Line_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.REFS2_PATTERN.matcher(entity);
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String start = regexMatcher.group(2);
        String direction = regexMatcher.group(3);
        if (null == start || null == direction) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new Line_stp(
                        stepFile.getReference(start),
                        stepFile.getReference(direction)
                ));
    }

}
