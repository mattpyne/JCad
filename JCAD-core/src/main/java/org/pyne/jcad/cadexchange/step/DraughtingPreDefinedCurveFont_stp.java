package org.pyne.jcad.cadexchange.step;

public class DraughtingPreDefinedCurveFont_stp extends StepEntity {

    public final static String ENTITY_TYPE = "DRAUGHTING_PRE_DEFINED_CURVE_FONT";
    public final static String[] values = new String[]{
            "continuous", "chain", "chain double dash", "dashed", "dotted"
    };

    public DraughtingPreDefinedCurveFont_stp(String name) {
        super(name);
    }

    public boolean isContinuous() {
        return getName().equals(values[0]);
    }

    public boolean isChain() {
        return getName().equals(values[1]);
    }

    public boolean isChainDoubleDash() {
        return getName().equals(values[2]);
    }

    public boolean isDashed() {
        return getName().equals(values[3]);
    }

    public boolean isDotted() {
        return getName().equals(values[4]);
    }
}
