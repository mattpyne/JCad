package org.pyne.jcad.cadexchange.step;

import java.util.List;
import java.util.Optional;

public class PresentationStyleAssignment_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRESENTATION_STYLE_ASSIGNMENT";

    private List<StepReference> styleRefs;

    public PresentationStyleAssignment_stp(List<StepReference> styleRefs) {
        super("");
        this.styleRefs = styleRefs;
    }

    public List<StepReference> getStyleRefs() {
        return styleRefs;
    }

    public Optional<SurfaceStyleUsage_stp> getSurfaceStyleUsage() {
        return styleRefs.stream()
                .map(StepReference::getEntity)
                .filter(SurfaceStyleUsage_stp.class::isInstance)
                .map(SurfaceStyleUsage_stp.class::cast)
                .findAny();
    }

    public Optional<CurveStyle_stp> getCurveStyle() {
        return styleRefs.stream()
                .map(StepReference::getEntity)
                .filter(CurveStyle_stp.class::isInstance)
                .map(CurveStyle_stp.class::cast)
                .findAny();
    }
}
