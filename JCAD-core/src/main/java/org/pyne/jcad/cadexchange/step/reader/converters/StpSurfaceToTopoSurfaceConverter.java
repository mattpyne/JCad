package org.pyne.jcad.cadexchange.step.reader.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.pyne.jcad.cadexchange.step.Surface_stp;
import org.pyne.jcad.core.math.geom.Surface;

/**
 *
 * @author Matthew
 */
public abstract class StpSurfaceToTopoSurfaceConverter {

    private static final List<StpSurfaceToTopoSurfaceConverter> CONVERTERS;

    static {
        CONVERTERS = new ArrayList<>();
        CONVERTERS.add(new BoundedBSplineSurfaceConverter());
        CONVERTERS.add(new BSplineSurfaceWithKnotsConverter());
        CONVERTERS.add(new ConicalSurface_stpConverter());
        CONVERTERS.add(new CylindricalSurface_stpConverter());
        CONVERTERS.add(new Plane_stpConverter());
        CONVERTERS.add(new SphericalSurface_stpConverter());
        CONVERTERS.add(new SurfaceOfLinearExtrusion_stpConverter());
        CONVERTERS.add(new SurfaceOfRevolution_stpConverter());
        CONVERTERS.add(new ToroidalSurface_stpConverter());
    }

    public abstract Optional<Surface> convert(Surface_stp stpSurface);

    public static Optional<Surface> runAllConverts(Surface_stp stpSurface) {
        for (StpSurfaceToTopoSurfaceConverter convert : CONVERTERS) {

            Optional<Surface> surface = convert.convert(stpSurface);
            if (surface.isPresent()) {

                return surface;
            }
        }

        return Optional.empty();
    }
}
