package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import java.util.Optional;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.cadexchange.step.CartesianPoint_stp;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import verb.core.Vec;

/**
 *
 * @author Matthew
 */
public class StpCircleToTopoCircleConverter extends StpCurveToTopoCurveConverter {

    @Override
    public Optional<Curve> convert(Curve_stp stpCurve) {

        if (!(stpCurve instanceof Circle_stp)) {

            return Optional.empty();
        }

        Circle_stp stpCircle = (Circle_stp) stpCurve;

        if (stpCircle.has2DNot3DPlacement()) {
            Axis2Placement2D_stp centerPlacement = stpCircle.centerPlacement2D();
            CartesianPoint_stp center = centerPlacement.location();
            Direction_stp axis = centerPlacement.axis();
            double radius = stpCircle.radius();

            Array<Number> pointArray = Array.from(center.x(), center.y(), center.z());
            Array<Number> xAxisArray = Array.from(axis.x(), axis.y(), 0.);
            Array<Number> yAxisArray = Array.from(axis.y(), axis.x(), 0.);

            verb.geom.Circle circle = new verb.geom.Circle(
                    pointArray,
                    xAxisArray,
                    yAxisArray,
                    radius);

            return Optional.of(new NurbsCurve(circle));
        } else {
            Axis2Placement3D_stp centerPlacement = stpCircle.centerPlacement3D();
            CartesianPoint_stp center = centerPlacement.location();
            Direction_stp axis = centerPlacement.axis();
            Direction_stp refAxis = centerPlacement.refAxis();
            double radius = stpCircle.radius();

            Array<Number> pointArray = Array.from(center.x(), center.y(), center.z());
            Array<Number> zAxis = Array.from(axis.x(), axis.y(), axis.z());

            Array<Number> xAxisArray = Array.from(refAxis.x(), refAxis.y(), refAxis.z());
            Array<Number> yAxisArray = Vec.cross(zAxis, xAxisArray);

            verb.geom.Circle circle = new verb.geom.Circle(
                    pointArray,
                    xAxisArray,
                    yAxisArray,
                    radius);

            return Optional.of(new NurbsCurve(circle));
        }

    }

}
