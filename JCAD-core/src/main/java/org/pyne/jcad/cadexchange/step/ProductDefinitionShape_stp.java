package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ProductDefinitionShape_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT_DEFINITION_SHAPE";

    private String description;
    private StepReference definitionRef;

    public ProductDefinitionShape_stp(
            String name,
            String description,
            StepReference definitionRef) {
        super(name);
        this.description = description;
        this.definitionRef = definitionRef;
    }

    public String getDescription() {
        return description;
    }

    public StepReference getDefinitionRef() {
        return definitionRef;
    }

    public ProductDefinition_stp getDefinition() {
        if(!(definitionRef.getEntity() instanceof ProductDefinition_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no product definition found! Ref: " + definitionRef.getRef());
        }

        return (ProductDefinition_stp) definitionRef.getEntity();
    }
}
