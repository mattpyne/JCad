package org.pyne.jcad.cadexchange.step;

public class DimensionalExponent_stp extends StepEntity {
    private double value;

    DimensionalExponent_stp(double value) {
        super("");
    }

    public double getValue() {
        return value;
    }

    public class LengthExponent_stp extends DimensionalExponent_stp {

        public LengthExponent_stp(double value) {
            super(value);
        }
    }

    public class MassExponent_stp extends DimensionalExponent_stp {

        public MassExponent_stp(double value) {
            super(value);
        }
    }

    public class TimeExponent_stp extends DimensionalExponent_stp {

        public TimeExponent_stp(double value) {
            super(value);
        }
    }

    public class EletricCurrentExponent_stp extends DimensionalExponent_stp {

        public EletricCurrentExponent_stp(double value) {
            super(value);
        }
    }

    public class ThermodynamicTemperatureExponent_stp extends DimensionalExponent_stp {

        public ThermodynamicTemperatureExponent_stp(double value) {
            super(value);
        }
    }

    public class AmountOfSubstanceExponent_stp extends DimensionalExponent_stp {

        public AmountOfSubstanceExponent_stp(double value) {
            super(value);
        }
    }

    public class LuminousIntensityExponent_stp extends DimensionalExponent_stp {

        public LuminousIntensityExponent_stp(double value) {
            super(value);
        }
    }
}
