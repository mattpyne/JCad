package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class CurveStyleDeserializer extends StepEntityDeserializer {

    private MeasureValueDeserializer measureValueDeserializer = new MeasureValueDeserializer();

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, CurveStyle_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 4) {
            Debug.log(Debug.LEVEL.ERROR, "Failed to deserialize " + entity);
            return Optional.empty();
        }

        Optional<MeasureValue> optionalMeasureValue = measureValueDeserializer.deserialize(variables.get(2));

        if (!optionalMeasureValue.isPresent()) {
            Debug.log(Debug.LEVEL.ERROR, "Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new CurveStyle_stp(
                variables.get(0),
                stepFile.getReference(variables.get(1)),
                optionalMeasureValue.get(),
                stepFile.getReference(variables.get(3))
        ));
    }

}