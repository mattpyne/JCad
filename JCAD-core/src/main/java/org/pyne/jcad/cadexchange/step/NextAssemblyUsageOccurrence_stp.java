package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class NextAssemblyUsageOccurrence_stp extends StepEntity {

    public static final String ENTITY_TYPE = "NEXT_ASSEMBLY_USAGE_OCCURRENCE";

    private String id;
    private String description;
    private StepReference relatingProductDefinitionRef;
    private StepReference relatedProductDefinitionRef;
    private String referenceDesignator;

    public NextAssemblyUsageOccurrence_stp(
            String id,
            String name,
            String description,
            StepReference relatingProductDefinitionRef,
            StepReference relatedProductDefinitionRef,
            String referenceDesignator
    ) {
        super(name);
        this.id = id;
        this.description = description;
        this.relatingProductDefinitionRef = relatingProductDefinitionRef;
        this.relatedProductDefinitionRef = relatedProductDefinitionRef;
        this.referenceDesignator = referenceDesignator;
    }

    public String getId() {
        return id;
    }

    public StepReference getRelatingProductDefinitionRef() {
        return relatingProductDefinitionRef;
    }

    public ProductDefinition_stp getRelatingProductDefinition() {
        if (!(relatingProductDefinitionRef.getEntity() instanceof ProductDefinition_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no relatingProductDefinition. Ref: " + relatingProductDefinitionRef.getRef());
        }

        return (ProductDefinition_stp) relatingProductDefinitionRef.getEntity();
    }

    public StepReference getRelatedProductDefinitionRef() {
        return relatedProductDefinitionRef;
    }

    public ProductDefinition_stp getRelatedProductDefinition() {
        if (!(relatedProductDefinitionRef.getEntity() instanceof ProductDefinition_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no relatedProductDefinitionRef. Ref: " + relatedProductDefinitionRef.getRef());
        }

        return (ProductDefinition_stp) relatedProductDefinitionRef.getEntity();
    }

    public String getDescription() {
        return description;
    }

    public String getReferenceDesignator() {
        return referenceDesignator;
    }
}