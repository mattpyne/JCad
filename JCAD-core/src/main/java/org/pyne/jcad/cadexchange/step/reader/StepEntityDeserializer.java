package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;

import java.util.ArrayList;
import java.util.Optional;

/**
 * @author Matthew
 */
public abstract class StepEntityDeserializer {

    private static final ArrayList<StepEntityDeserializer> DESERIALIZERS;

    static {
        DESERIALIZERS = new ArrayList<>();
        DESERIALIZERS.add(new AdvancedBrepShapeRepresentationDeserializer());
        DESERIALIZERS.add(new AdvancedFaceDeserializer());
        DESERIALIZERS.add(new ApplicationContextDeserializer());
        DESERIALIZERS.add(new ApplicationProtocolDefinitionDeserializer());
        DESERIALIZERS.add(new Axis1PlacementDeserializer());
        DESERIALIZERS.add(new Axis2Placement2DDeserializer());
        DESERIALIZERS.add(new Axis2Placement3DDeserializer());
        DESERIALIZERS.add(new BSplineCurveWithKnotsDeserializer());
        DESERIALIZERS.add(new BSplineSurfaceWithKnotsDeserializer());
        DESERIALIZERS.add(new BoundedBSplineCurveDeserializer());
        DESERIALIZERS.add(new BoundedBSplineSurfaceDeserializer());
        DESERIALIZERS.add(new CartesianPointDeserializer());
        DESERIALIZERS.add(new CircleDeserializer());
        DESERIALIZERS.add(new ColourRGBDeserializer());
        DESERIALIZERS.add(new ConicalSurface_stpDeserializer());
        DESERIALIZERS.add(new ContextDependentShapeRepresentationDeserializer());
        DESERIALIZERS.add(new ClosedShellDeserializer());
        DESERIALIZERS.add(new CurveStyleDeserializer());
        DESERIALIZERS.add(new CylindricalSurfaceDeserializer());
        DESERIALIZERS.add(new DefinitionalRepresentationDeserializer());
        DESERIALIZERS.add(new DirectionDeserializer());
        DESERIALIZERS.add(new DraughtingPreDefinedCurveFontDeserializer());
        DESERIALIZERS.add(new FillAreaStyleDeserializer());
        DESERIALIZERS.add(new FillAreaStyleColourDeserializer());
        DESERIALIZERS.add(new EdgeCurveDeserializer());
        DESERIALIZERS.add(new EdgeLoopDeserializer());
        DESERIALIZERS.add(new GeometricRepresentationContext2Deserializer());
        DESERIALIZERS.add(new GeometricRepresentationContext3Deserializer());
        DESERIALIZERS.add(new ItemDefinedTransformationDeserializer());
        DESERIALIZERS.add(new FaceBoundDeserializer());
        DESERIALIZERS.add(new FaceOuterBoundDeserializer());
        DESERIALIZERS.add(new LineDeserializer());
        DESERIALIZERS.add(new ManifoldSolidBrepDeserializer());
        DESERIALIZERS.add(new MechanicalDesignGeometricPresentationRepresentationDeserializer());
        DESERIALIZERS.add(new NextAssemblyUsageOccurrenceDeserializer());
        DESERIALIZERS.add(new OrientatedEdgeDeserializer());
        DESERIALIZERS.add(new PCurveDeserializer());
        DESERIALIZERS.add(new PlaneDeserializer());
        DESERIALIZERS.add(new PresentationStyleAssignmentDeserializer());
        DESERIALIZERS.add(new ProductDeserializer());
        DESERIALIZERS.add(new ProductContextDeserializer());
        DESERIALIZERS.add(new ProductDefinitionDeserializer());
        DESERIALIZERS.add(new ProductDefinitionContextDeserializer());
        DESERIALIZERS.add(new ProductDefinitionFormationDeserializer());
        DESERIALIZERS.add(new ProductDefinitionShapeDeserializer());
        DESERIALIZERS.add(new ProductRelatedProductCategoryDeserializer());
        DESERIALIZERS.add(new SeamCurveDeserializer());
        DESERIALIZERS.add(new SILengthUnitDeserializer());
        DESERIALIZERS.add(new SIPlaneAngleUnitDeserializer());
        DESERIALIZERS.add(new SISolidAngleUnitDeserializer());
        DESERIALIZERS.add(new SphericalSurfaceDeserializer());
        DESERIALIZERS.add(new SurfaceCurveDeserializer());
        DESERIALIZERS.add(new ShapeDefinitionRepresentationDeserializer());
        DESERIALIZERS.add(new ShapeRepresentationDeserializer());
        DESERIALIZERS.add(new ShapeRepresentationRelationShipDeserializer());
        DESERIALIZERS.add(new StyledItemDeserializer());
        DESERIALIZERS.add(new SurfaceOfLinearExtrusionDeserializer());
        DESERIALIZERS.add(new SurfaceOfRevolutionConverter());
        DESERIALIZERS.add(new SurfaceStyleFillAreaDeserializer());
        DESERIALIZERS.add(new SurfaceStyleUsageDeserializer());
        DESERIALIZERS.add(new SurfaceSideStyleDeserializer());
        DESERIALIZERS.add(new ToroidalSurfaceDeserializer());
        DESERIALIZERS.add(new UncertaintyMeasureWithUnitDeserializer());
        DESERIALIZERS.add(new VectorDeserializer());
        DESERIALIZERS.add(new VertexPointDeserializer());
    }

    abstract Optional<StepEntity> deserialize(String entity, StepFile stepFile);

    public static Optional<StepEntity> runDeserializers(String entity, StepFile stepFile) {

        for (StepEntityDeserializer deserializer : DESERIALIZERS) {

            Optional<StepEntity> entityD = deserializer.deserialize(entity, stepFile);

            if (entityD.isPresent()) {

                stepFile.fillOutReference(entityRef(entity), entityD.get());

                return entityD;
            }
        }

        return Optional.empty();
    }

    public static String entityType(String entity) {
        return entity.substring(entity.indexOf("=") + 1, entity.indexOf("(")).trim();
    }

    public static String entityRef(String entity) {
        return entity.substring(0, entity.indexOf("=")).trim();
    }

    public static boolean isEntity(String entity) {
        return entity.startsWith("#");
    }
}
