package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class ShapeRepresentationRelationShipDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!entity.contains(ShapeRepresentationRelationshipWithTransform_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        if (StepRegex.isType(entity, ShapeRepresentationRelationshipWithTransform_stp.ENTITY_TYPE)) {

            List<String> vars = StepRegex.variables(entity);

            return Optional.of(
                    new ShapeRepresentationRelationship_stp(
                            vars.get(0),
                            vars.get(1),
                            stepFile.getReference(vars.get(2)),
                            stepFile.getReference(vars.get(3))
                    )
            );
        } else {

            List<String> representationRelationshipVariables = StepRegex.variables(entity, RepresentationRelationship_stp.ENTITY_TYPE);
            List<String> repTransVars = StepRegex.variables(entity, RepresentationRelationshipWithTransformation_stp.ENTITY_TYPE);

            if (representationRelationshipVariables.isEmpty() || repTransVars.isEmpty()) {
                Debug.log(Debug.LEVEL.ERROR, "Failed to deserialize " + entity);
                return Optional.empty();
            }

            return Optional.of(new ShapeRepresentationRelationshipWithTransform_stp(
                    new RepresentationRelationship_stp(
                            representationRelationshipVariables.get(0),
                            representationRelationshipVariables.get(1),
                            stepFile.getReference(representationRelationshipVariables.get(2)),
                            stepFile.getReference(representationRelationshipVariables.get(3))),
                    new RepresentationRelationshipWithTransformation_stp(
                            stepFile.getReference(repTransVars.get(0)))
            ));
        }
    }

}

