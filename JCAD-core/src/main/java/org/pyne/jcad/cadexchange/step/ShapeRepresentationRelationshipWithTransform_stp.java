package org.pyne.jcad.cadexchange.step;

public class ShapeRepresentationRelationshipWithTransform_stp extends StepEntity {

    public static final String ENTITY_TYPE = "SHAPE_REPRESENTATION_RELATIONSHIP";

    private RepresentationRelationship_stp relationship;
    private RepresentationRelationshipWithTransformation_stp transformRelationship;

    public ShapeRepresentationRelationshipWithTransform_stp(
            RepresentationRelationship_stp relationship,
            RepresentationRelationshipWithTransformation_stp transformRelationship) {
        super("");
        this.relationship = relationship;
        this.transformRelationship = transformRelationship;
    }

    public RepresentationRelationship_stp getRelationship() {
        return relationship;
    }

    public RepresentationRelationshipWithTransformation_stp getTransformRelationship() {
        return transformRelationship;
    }
}
