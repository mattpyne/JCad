package org.pyne.jcad.cadexchange.step;

public class ApplicationContext_stp extends StepEntity {

    public static final String ENTITY_TYPE = "APPLICATION_CONTEXT";
    public String application;

    public ApplicationContext_stp(String application) {
        super("");
        this.application = application;
    }

}
