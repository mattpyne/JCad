package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import org.pyne.jcad.cadexchange.step.BSplineCurveWithKnots_stp;
import org.pyne.jcad.cadexchange.step.CartesianPoint_stp;
import org.pyne.jcad.cadexchange.step.Curve_stp;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.NurbsCurve;

import java.util.List;
import java.util.Optional;

public class BSplineCurveWithKnots_stpConverter extends StpCurveToTopoCurveConverter {

    @Override
    public Optional<Curve> convert(Curve_stp stpCurve) {

        if (!(stpCurve instanceof BSplineCurveWithKnots_stp)) {

            return Optional.empty();
        }

        BSplineCurveWithKnots_stp curve = (BSplineCurveWithKnots_stp) stpCurve;

        List<CartesianPoint_stp> cartesianPoint_stps = curve.controlPointsList();
        List<Integer> knotMultiplicities = curve.knotMultiplicities();

        Number[][] cps = new Number[cartesianPoint_stps.size()][3];

        for (int i = 0; i < cartesianPoint_stps.size(); i++) {
            cps[i][0] = cartesianPoint_stps.get(i).x();
            cps[i][1] = cartesianPoint_stps.get(i).y();
            cps[i][2] = cartesianPoint_stps.get(i).z();
        }

        Array<Array<Number>> controlPoints = Array.from(cps);

        Number[] knots = new Number[knotMultiplicities.stream().reduce(0, Integer::sum)];

        int k = 0;
        for (int i = 0; i < curve.knots().size(); i++) {
            for (int j = 0; j < knotMultiplicities.get(i); j++) {
                knots[k++] = curve.knots().get(i);
            }
        }
        return Optional.of(
                new NurbsCurve(
                        verb.geom.NurbsCurve.byKnotsControlPointsWeights(
                                curve.degree(),
                                Array.ofNative(knots),
                                controlPoints,
                                null
                        )));
    }

}
