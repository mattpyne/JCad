package org.pyne.jcad.cadexchange.step;

public class ConicalSurface_stp extends Surface_stp {

    public final static String ENTITY_TYPE = "CONICAL_SURFACE";

    private StepReference position;
    private double radius;
    private double semiAngle;

    public ConicalSurface_stp(
            String name,
            StepReference position,
            double radius,
            double semiAngle) {
        super(name);
        this.position = position;
        this.radius = radius;
        this.semiAngle = semiAngle;
    }

    public Axis2Placement3D_stp position() {
        if (!(position.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) position.getEntity();
    }

    public double radius() {
        return radius;
    }

    public double getSemiAngle() {
        return semiAngle;
    }
}
