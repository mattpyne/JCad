package org.pyne.jcad.cadexchange.step;

public abstract class Unit_stp extends StepEntity {
    Unit_stp() {
        super("");
    }

    public abstract SIPrefix_stp getPrefix();

    public abstract SIUnitName_stp getUnitName();
}
