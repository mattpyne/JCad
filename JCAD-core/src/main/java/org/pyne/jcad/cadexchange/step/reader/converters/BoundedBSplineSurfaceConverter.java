package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import org.pyne.jcad.cadexchange.step.BoundedBSplineSurface;
import org.pyne.jcad.cadexchange.step.CartesianPoint_stp;
import org.pyne.jcad.cadexchange.step.Surface_stp;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.Surface;

import java.util.List;
import java.util.Optional;

public class BoundedBSplineSurfaceConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof BoundedBSplineSurface)) {

            return Optional.empty();
        }

        BoundedBSplineSurface surface = (BoundedBSplineSurface) stpSurface;

        List<List<CartesianPoint_stp>> cartesianPoint_stps = surface.controlPointsList();
        List<Integer> knotMultiplicitiesU = surface.knotMultiplicitiesU();
        List<Integer> knotMultiplicitiesV = surface.knotMultiplicitiesV();

        Number[][][] cps = new Number[cartesianPoint_stps.size()][cartesianPoint_stps.get(0).size()][3];

        for (int i = 0; i < cartesianPoint_stps.size(); i++) {
            for(int j = 0; j < cartesianPoint_stps.get(i).size(); j++) {
                cps[i][j][0] = cartesianPoint_stps.get(i).get(j).x();
                cps[i][j][1] = cartesianPoint_stps.get(i).get(j).y();
                cps[i][j][2] = cartesianPoint_stps.get(i).get(j).z();
            }
        }

        Array<Array<Array<Number>>> controlPoints = Array.from(cps);

        Number[] knotsU = getKnots(surface.knotsU(), knotMultiplicitiesU);
        Number[] knotsV = getKnots(surface.knotsV(), knotMultiplicitiesV);

        List<List<Double>> weightsList = surface.getWeights();
        Number[][] weights = new Number[weightsList.size()][weightsList.get(0).size()];
        for (int i = 0; i < weightsList.size(); i ++) {
            for(int j = 0; j < weightsList.get(i).size(); j++) {
                weights[i][j] = weightsList.get(i).get(j);
            }
        }

        return Optional.of(
                new NurbsSurface(
                        verb.geom.NurbsSurface.byKnotsControlPointsWeights(
                                surface.degreeU(),
                                surface.degreeV(),
                                Array.ofNative(knotsU),
                                Array.ofNative(knotsV),
                                controlPoints,
                                Array.from(weights)
                        )));
    }

    private Number[] getKnots(List<Double> knots, List<Integer> knotMultiplicities) {
        Number[] knotsU = new Number[knotMultiplicities.stream().reduce(0, Integer::sum)];

        int k = 0;
        for (int i = 0; i < knots.size(); i++) {
            for (int j = 0; j < knotMultiplicities.get(i); j++) {
                knotsU[k++] = knots.get(i);
            }
        }
        return knotsU;
    }

}
