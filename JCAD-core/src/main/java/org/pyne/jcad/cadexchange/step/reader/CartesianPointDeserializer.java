package org.pyne.jcad.cadexchange.step.reader;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import static org.pyne.jcad.cadexchange.step.StepRegex.VEC2D_PATTERN;
import static org.pyne.jcad.cadexchange.step.StepRegex.VEC3D_PATTERN;

/**
 * @author Matthew Pyne
 */
public class CartesianPointDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile file) {
        if (!StepRegex.isType(entity, CartesianPoint_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 2) {

            Debug.log(Debug.LEVEL.ERROR,"Failed to deserialize " + entity);
            return Optional.empty();
        }

        List<String> coordinates = StepRegex.variables(variables.get(1));

        try {
            if (coordinates.size() == 3) {

                double x = Double.parseDouble(coordinates.get(0));
                double y = Double.parseDouble(coordinates.get(1));
                double z = Double.parseDouble(coordinates.get(2));

                return Optional.of(new CartesianPoint_stp(x, y, z));
            } else if (coordinates.size() == 2) {

                double x = Double.parseDouble(coordinates.get(0));
                double y = Double.parseDouble(coordinates.get(1));

                return Optional.of(new CartesianPoint_stp(x, y));
            }
        } catch (NumberFormatException nfe) {
            Debug.log(nfe);
        }

        System.err.println("Failed to deserialize " + entity);
        return Optional.empty();
    }

}
