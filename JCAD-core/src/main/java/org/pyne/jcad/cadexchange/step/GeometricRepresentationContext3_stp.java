package org.pyne.jcad.cadexchange.step;

public class GeometricRepresentationContext3_stp extends RepresentationContext_stp {

    public static final String ENTITY_TYPE = "GEOMETRIC_REPRESENTATION_CONTEXT";

    private GlobalUncertaintyAssignedContext_stp uncertainUnits;
    private GlobalUnitAssignedContext_stp units;

    public GeometricRepresentationContext3_stp(
            GlobalUncertaintyAssignedContext_stp uncertainUnits,
            GlobalUnitAssignedContext_stp units,
            String contextIdentifier,
            String contextType) {
        super(contextIdentifier, contextType);
        this.uncertainUnits = uncertainUnits;
        this.units = units;
    }

    public GlobalUncertaintyAssignedContext_stp getUncertainUnits() {
        return uncertainUnits;
    }

    public GlobalUnitAssignedContext_stp getUnits() {
        return units;
    }

}