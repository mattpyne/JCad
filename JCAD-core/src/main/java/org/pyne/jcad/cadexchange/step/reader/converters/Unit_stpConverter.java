package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.units.*;

public class Unit_stpConverter {

    public Unit convert(Unit_stp unit_stp) {
        if (unit_stp instanceof SILengthUnit_stp) {
            return new LengthUnit(
                    getPrefix(unit_stp.getPrefix()),
                    getName(unit_stp.getUnitName()));
        } else if (unit_stp instanceof SISolidAngleUnit_stp) {
            return new SolidAngleUnit(
                    getPrefix(unit_stp.getPrefix()),
                    getName(unit_stp.getUnitName()));
        } else if (unit_stp instanceof SIPlaneAngleUnit_stp) {
            return new PlaneAngleUnit(
                    getPrefix(unit_stp.getPrefix()),
                    getName(unit_stp.getUnitName()));
        } else {
            Debug.log(Debug.LEVEL.ERROR, "Do not have a unit to convert unit_stp to. Type was " + unit_stp.getClass().getSimpleName());
            return new LengthUnit(Prefix.NONE, UnitName.NONE);
        }
    }

    private UnitName getName(SIUnitName_stp siUnitName_stp) {
        UnitName name = UnitName.NONE;
        switch (siUnitName_stp) {
            case METRE:
                name = UnitName.METRE;
                break;
            case GRAM:
                name = UnitName.GRAM;
                break;
            case SECOND:
                name = UnitName.SECOND;
                break;
            case AMPERE:
                name = UnitName.AMPERE;
                break;
            case KELIVIN:
                name = UnitName.KELIVIN;
                break;
            case MOLE:
                name = UnitName.MOLE;
                break;
            case CANDELA:
                name = UnitName.CANDELA;
                break;
            case RADIAN:
                name = UnitName.RADIAN;
                break;
            case STERADIAN:
                name = UnitName.STERADIAN;
                break;
            case HERTZ:
                name = UnitName.HERTZ;
                break;
            case NEWTON:
                name = UnitName.NEWTON;
                break;
            case PASCAL:
                name = UnitName.PASCAL;
                break;
            case JOULE:
                name = UnitName.JOULE;
                break;
            case WATT:
                name = UnitName.WATT;
                break;
            case COULOMB:
                name = UnitName.COULOMB;
                break;
            case VOLT:
                name = UnitName.VOLT;
                break;
            case FARAD:
                name = UnitName.FARAD;
                break;
            case OHM:
                name = UnitName.OHM;
                break;
            case SIEMENS:
                name = UnitName.SIEMENS;
                break;
            case WEBER:
                name = UnitName.WEBER;
                break;
            case TESLA:
                name = UnitName.TESLA;
                break;
            case HENRY:
                name = UnitName.HENRY;
                break;
            case DEGREE_CELSIUS:
                name = UnitName.DEGREE_CELSIUS;
                break;
            case LUMEN:
                name = UnitName.LUMEN;
                break;
            case LUX:
                name = UnitName.LUX;
                break;
            case BECQUEREL:
                name = UnitName.BECQUEREL;
                break;
            case GRAY:
                name = UnitName.GRAY;
                break;
            case NONE:
                name = UnitName.NONE;
                break;
            case SIEVERT:
                name = UnitName.SIEVERT;
                break;
        }
        return name;
    }

    private Prefix getPrefix(SIPrefix_stp siPrefix_stp) {
        Prefix prefix = Prefix.NONE;
        switch (siPrefix_stp) {
            case EXA:
                prefix = Prefix.EXA;
                break;
            case PETA:
                prefix = Prefix.PETA;
                break;
            case TERA:
                prefix = Prefix.TERA;
                break;
            case GIGA:
                prefix = Prefix.GIGA;
                break;
            case MEGA:
                prefix = Prefix.MEGA;
                break;
            case KILO:
                prefix = Prefix.KILO;
                break;
            case HECTO:
                prefix = Prefix.HECTO;
                break;
            case DECA:
                prefix = Prefix.DECA;
                break;
            case DECI:
                prefix = Prefix.DECI;
                break;
            case CENTI:
                prefix = Prefix.CENTI;
                break;
            case MILLI:
                prefix = Prefix.MILLI;
                break;
            case MICRO:
                prefix = Prefix.MICRO;
                break;
            case NANO:
                prefix = Prefix.NANO;
                break;
            case PICO:
                prefix = Prefix.PICO;
                break;
            case FEMTO:
                prefix = Prefix.FEMTO;
                break;
            case NONE:
                prefix = Prefix.NONE;
                break;
            case ATTO:
                prefix = Prefix.ATTO;
                break;
        }
        return prefix;
    }
}
