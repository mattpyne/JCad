package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import java.util.Optional;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.cadexchange.step.Direction_stp;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.Vec;
import verb.geom.CylindricalSurface;

/**
 *
 * @author Matthew
 */
public class CylindricalSurface_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof CylindricalSurface_stp)) {

            return Optional.empty();
        }

        CylindricalSurface_stp stpCylindricalSurface = (CylindricalSurface_stp) stpSurface;

        Axis2Placement3D_stp centerPlacement = stpCylindricalSurface.position();
        CartesianPoint_stp point = centerPlacement.location();
        Direction_stp normal = centerPlacement.axis();
        Direction_stp refAxis = centerPlacement.refAxis();
        double radius = stpCylindricalSurface.radius();

        Array<Number> pointArray = Array.from(point.x(), point.y(), point.z());
        Array<Number> normalArray = Array.from(normal.x(), normal.y(), normal.z());
        Array<Number> xAxisArray = Array.from(refAxis.x(), refAxis.y(), refAxis.z());

        pointArray = Vec.sub(pointArray, Vec.mul(500, normalArray));
        
        return Optional.of(new NurbsSurface(new CylindricalSurface(normalArray, xAxisArray, pointArray, 1000, radius)));
    }

}
