package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public abstract class StepEntity {

    private String name;

    public StepEntity(String name) {
        if (name.equals("''")) {
            this.name = "";
        } else {
            this.name = name;
        }
    }

    public String getName() {
        return name;
    }

}
