package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ShapeDefinitionRepresentation_stp extends StepEntity {

    public static final String ENTITY_TYPE = "SHAPE_DEFINITION_REPRESENTATION";

    private StepReference definitionRef;
    private StepReference usedRepresentationRef;

    public ShapeDefinitionRepresentation_stp(
            StepReference definitionRef,
            StepReference usedRepresentationRef) {
        super("");
        this.definitionRef = definitionRef;
        this.usedRepresentationRef = usedRepresentationRef;
    }

    public StepReference getDefinitionRef() {
        return definitionRef;
    }

    public ProductDefinitionShape_stp getDefinition() {
        if(!(definitionRef.getEntity() instanceof ProductDefinitionShape_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no definition found! Ref: " + definitionRef.getRef());
        }

        return (ProductDefinitionShape_stp) definitionRef.getEntity();
    }

    public StepReference getUsedRepresentationRef() {
        return usedRepresentationRef;
    }

    public ShapeRepresentation_stp getUsedRepresentation() {
        if(!(usedRepresentationRef.getEntity() instanceof ShapeRepresentation_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no usedRepresentation found! Ref: " + usedRepresentationRef.getRef());
        }

        return (ShapeRepresentation_stp) usedRepresentationRef.getEntity();
    }

}

