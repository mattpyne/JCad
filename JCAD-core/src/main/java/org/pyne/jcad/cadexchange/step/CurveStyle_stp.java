package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class CurveStyle_stp extends StepEntity {

    public static final String ENTITY_TYPE = "CURVE_STYLE";

    private StepReference curveFontRef;
    private MeasureValue curveWidth;
    private StepReference curveColourRef;

    public CurveStyle_stp(
            String name,
            StepReference curveFontRef,
            MeasureValue curveWidth,
            StepReference curveColourRef) {
        super(name);
        this.curveFontRef = curveFontRef;
        this.curveWidth = curveWidth;
        this.curveColourRef = curveColourRef;
    }

    public StepReference getCurveFontRef() {
        return curveFontRef;
    }

    public DraughtingPreDefinedCurveFont_stp getCurveFont() {
        if (!(curveFontRef.getEntity() instanceof DraughtingPreDefinedCurveFont_stp)) {

            Debug.log(Debug.LEVEL.ERROR,"No curve font. Ref: " + curveFontRef.getRef());
        }

        return (DraughtingPreDefinedCurveFont_stp) curveFontRef.getEntity();
    }

    public MeasureValue getCurveWidth() {
        return curveWidth;
    }

    public StepReference getCurveColourRef() {
        return curveColourRef;
    }

    public ColourRGB_stp getCurveColor() {
        if (!(curveColourRef.getEntity() instanceof ColourRGB_stp)) {

            Debug.log(Debug.LEVEL.ERROR,"No curve colour. Ref: " + curveColourRef.getRef());
        }

        return (ColourRGB_stp) curveColourRef.getEntity();
    }
}
