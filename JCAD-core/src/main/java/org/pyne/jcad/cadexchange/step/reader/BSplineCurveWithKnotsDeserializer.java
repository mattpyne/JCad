package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BSplineCurveWithKnotsDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, BSplineCurveWithKnots_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 9) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        int degree = Integer.parseInt(variables.get(1));
        List<StepReference> controlPointsListRef = getControlPointsList(stepFile, variables.get(2));
        String curveForm = variables.get(3);
        boolean closedCurve = variables.get(4).toLowerCase().equals(".t.");
        boolean selfIntersecting = variables.get(5).toLowerCase().equals(".t.");
        List<Integer> knotMultiplicities = getKnotMultiplicities(variables.get(6));
        List<Double> knots = getKnots(variables.get(7));
        String knotSpec = variables.get(8);

        return Optional.of(
                new BSplineCurveWithKnots_stp(
                        name,
                        degree,
                        controlPointsListRef,
                        curveForm,
                        closedCurve,
                        selfIntersecting,
                        knotMultiplicities,
                        knots,
                        knotSpec));
    }

    private List<Double> getKnots(String knots) {
        knots = knots.replace("(", "");
        knots = knots.replace(")", "");
        return Arrays.asList(knots.split(",")).stream()
                .map(knotMult -> Double.parseDouble(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<Integer> getKnotMultiplicities(String knotMultiplicitiesStr) {
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace("(", "");
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace(")", "");
        return Arrays.asList(knotMultiplicitiesStr.split(",")).stream()
                .map(knotMult -> Integer.parseInt(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<StepReference> getControlPointsList(StepFile stepFile, String controlPointsListStr) {
        controlPointsListStr = controlPointsListStr.replace("(", "");
        controlPointsListStr = controlPointsListStr.replace(")", "");
        return Arrays.asList(controlPointsListStr.split(","))
                .stream()
                .map(controlPoint -> stepFile.getReference(controlPoint))
                .collect(Collectors.toList());
    }

}
