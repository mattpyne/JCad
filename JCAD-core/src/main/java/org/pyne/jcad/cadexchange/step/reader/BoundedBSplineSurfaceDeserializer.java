package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BoundedBSplineSurfaceDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!entity.contains("BOUNDED_SURFACE")){
            return Optional.empty();
        }

        List<String> varsBSplineSurface = StepRegex.variables(entity, "B_SPLINE_SURFACE");
        List<String> varsBSplineSurfaceWithKnots = StepRegex.variables(entity, "B_SPLINE_SURFACE_WITH_KNOTS");
//        List<String> varsCurve = StepRegex.variables(entity, "CURVE");
//        List<String> varsGeomRepItem = StepRegex.variables(entity, "GEOMETRIC_REPRESENTATION_ITEM");
        List<String> varsRationalBSplineSurface= StepRegex.variables(entity, "RATIONAL_B_SPLINE_SURFACE");
//        List<String> varsRepItem= StepRegex.variables(entity, "REPRESENTATION_ITEM");

        int degreeU = Integer.parseInt(varsBSplineSurface.get(0));
        int degreeV = Integer.parseInt(varsBSplineSurface.get(1));
        List<List<StepReference>> controlPointsListRef = getControlPointsList(stepFile, varsBSplineSurface.get(2));
        String surfaceForm = varsBSplineSurface.get(3);
        boolean closedU = varsBSplineSurface.get(4).toLowerCase().equals(".t.");
        boolean closedV = varsBSplineSurface.get(5).toLowerCase().equals(".t.");
        boolean selfIntersecting = varsBSplineSurface.get(6).toLowerCase().equals(".t.");
        List<Integer> knotMultiplicitiesU = getKnotMultiplicities(varsBSplineSurfaceWithKnots.get(0));
        List<Integer> knotMultiplicitiesV = getKnotMultiplicities(varsBSplineSurfaceWithKnots.get(1));
        List<Double> knotsU = getKnots(varsBSplineSurfaceWithKnots.get(2));
        List<Double> knotsV = getKnots(varsBSplineSurfaceWithKnots.get(3));
        String knotSpec = varsBSplineSurfaceWithKnots.get(4);
        List<List<Double>> weights = getWeights(varsRationalBSplineSurface.get(0));

        return Optional.of(
                new BoundedBSplineSurface(
                        "",
                        degreeU,
                        degreeV,
                        controlPointsListRef,
                        surfaceForm,
                        closedU,
                        closedV,
                        selfIntersecting,
                        knotMultiplicitiesU,
                        knotMultiplicitiesV,
                        knotsU,
                        knotsV,
                        knotSpec,
                        weights));
    }

    private List<List<Double>> getWeights(String weightsStr) {
        List<List<Double>> weights = new ArrayList<>();

        for (String weightStr : StepRegex.variables(weightsStr)) {
            weightStr = weightStr.replace("(", "");
            weightStr = weightStr.replace(")", "");
            weights.add(Arrays.stream(weightStr.split(","))
                    .map(Double::parseDouble)
                    .collect(Collectors.toList()));
        }

        return weights;
    }

    private List<Double> getKnots(String knots) {
        knots = knots.replace("(", "");
        knots = knots.replace(")", "");
        return Arrays.asList(knots.split(",")).stream()
                .map(knotMult -> Double.parseDouble(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<Integer> getKnotMultiplicities(String knotMultiplicitiesStr) {
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace("(", "");
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace(")", "");
        return Arrays.asList(knotMultiplicitiesStr.split(",")).stream()
                .map(knotMult -> Integer.parseInt(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<List<StepReference>> getControlPointsList(StepFile stepFile, String controlPointsListStr) {
        List<List<StepReference>> controlPointsUVList = new ArrayList<>();

        for (String controlPointUV : StepRegex.variables(controlPointsListStr)) {
            controlPointUV = controlPointUV.replace("(", "");
            controlPointUV = controlPointUV.replace(")", "");
            controlPointsUVList.add(Arrays.asList(controlPointUV.split(","))
                    .stream()
                    .map(controlPoint -> stepFile.getReference(controlPoint))
                    .collect(Collectors.toList()));
        }

        return controlPointsUVList;
    }

}
