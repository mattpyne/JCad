package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.Vec;
import verb.geom.Circle;
import verb.geom.RevolvedSurface;

import java.util.Optional;

public class ToroidalSurface_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof ToroidalSurface_stp)) {

            return Optional.empty();
        }

        ToroidalSurface_stp toroidalSurface_stp = (ToroidalSurface_stp) stpSurface;

        Axis2Placement3D_stp centerPlacement = toroidalSurface_stp.position();
        CartesianPoint_stp point = centerPlacement.location();
        Direction_stp normal = centerPlacement.axis();
        Direction_stp refAxis = centerPlacement.refAxis();

        Array<Number> pointArray = Array.from(point.x(), point.y(), point.z());
        Array<Number> normalArray = Array.from(normal.x(), normal.y(), normal.z());
        Array<Number> xAxisArray = Array.from(refAxis.x(), refAxis.y(), refAxis.z());

        Circle circle = new Circle(
                Vec.add(pointArray, Vec.mul(toroidalSurface_stp.getMajorRadius(), xAxisArray)),
                xAxisArray,
                normalArray,
                toroidalSurface_stp.getMinorRadius()
        );

        return Optional.of(
                new NurbsSurface(
                        new RevolvedSurface(circle, pointArray, normalArray, 2 * Math.PI)
                )
        );
    }

}

//    // center, xaxis, yaxis, radius
//    var profile = new verb.geom.Circle( [5,0,0], [1,0,0], [0,0,1], 2 );
//
//    var base = [0,0,0];
//    var axis = [0,0,1];
//    var angle = 2 * Math.PI
//    srf1 = new verb.geom.RevolvedSurface( profile, base, axis, angle );

