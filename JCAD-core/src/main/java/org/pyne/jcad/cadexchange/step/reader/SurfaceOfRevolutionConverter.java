package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.cadexchange.step.StepRegex;
import org.pyne.jcad.cadexchange.step.SurfaceOfRevolution_stp;

import java.util.Optional;
import java.util.regex.Matcher;

public class SurfaceOfRevolutionConverter extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, SurfaceOfRevolution_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.INPUTS3_PATTERN.matcher(entity);
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = regexMatcher.group(1);
        String sweptCurveRef = regexMatcher.group(2);
        String extrusionRef = regexMatcher.group(3);
        if (null == sweptCurveRef
                || null == name
                || null == extrusionRef
        ) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(
                new SurfaceOfRevolution_stp(
                        name,
                        stepFile.getReference(sweptCurveRef),
                        stepFile.getReference(extrusionRef)));
    }

}
