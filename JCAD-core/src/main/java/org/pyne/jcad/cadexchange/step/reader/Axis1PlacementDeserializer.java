package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.Axis1Placement_stp;
import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.cadexchange.step.StepRegex;

import java.util.List;
import java.util.Optional;

public class Axis1PlacementDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Axis1Placement_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 3) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        String locationRef = variables.get(1);
        String axisRef = variables.get(2);
        if (null == locationRef
                || null == axisRef) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new Axis1Placement_stp(
                name,
                stepFile.getReference(locationRef),
                stepFile.getReference(axisRef)
        ));
    }

}
