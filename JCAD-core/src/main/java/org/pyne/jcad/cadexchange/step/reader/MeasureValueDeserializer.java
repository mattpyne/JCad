package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.MeasureValue;
import org.pyne.jcad.cadexchange.step.StepRegex;

import java.util.List;
import java.util.Optional;

public class MeasureValueDeserializer {

    public Optional<MeasureValue> deserialize(String entity) {

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 1) {
            return Optional.empty();
        }

        MeasureValue.TYPE type = MeasureValue.TYPE.NONE;

        for (MeasureValue.TYPE ea : MeasureValue.TYPE.values()) {
            if (entity.toLowerCase().contains(ea.toString().toLowerCase())) {
                type = ea;
                break;
            }
        }

        if (type == MeasureValue.TYPE.NONE) {
            return Optional.empty();
        }

        return Optional.of(new MeasureValue(type, Double.parseDouble(variables.get(0))));
    }
}
