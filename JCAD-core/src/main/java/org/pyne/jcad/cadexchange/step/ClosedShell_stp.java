package org.pyne.jcad.cadexchange.step;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Matthew
 */
public class ClosedShell_stp extends StepEntity {

    public static final String ENTITY_TYPE = "CLOSED_SHELL";

    private List<StepReference> faces;

    public ClosedShell_stp(String name, List<StepReference> faces) {
        super(name);
        this.faces = faces;
    }

    public List<AdvancedFace_stp> faces() {
        return faces.stream()
                .map(StepReference::getEntity)
                .filter(e -> null != e)
                .filter(AdvancedFace_stp.class::isInstance)
                .map(AdvancedFace_stp.class::cast)
                .collect(Collectors.toList());
    }
}
