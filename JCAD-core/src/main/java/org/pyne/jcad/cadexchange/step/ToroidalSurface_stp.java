package org.pyne.jcad.cadexchange.step;

public class ToroidalSurface_stp extends Surface_stp {

    public final static String ENTITY_TYPE = "TOROIDAL_SURFACE";

    private StepReference position;
    private double majorRadius;
    private double minorRadius;

    public ToroidalSurface_stp(
            String name,
            StepReference position,
            double majorRadius,
            double minorRadius) {
        super(name);
        this.position = position;
        this.majorRadius = majorRadius;
        this.minorRadius = minorRadius;
    }

    public Axis2Placement3D_stp position() {
        if (!(position.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) position.getEntity();
    }

    public double getMajorRadius() {
        return majorRadius;
    }

    public double getMinorRadius() {
        return minorRadius;
    }
}
