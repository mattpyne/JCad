package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.ArrayList;
import java.util.List;

public class GlobalUncertaintyAssignedContext_stp extends StepEntity {

    public static final String ENTITY_TYPE = "GLOBAL_UNCERTAINTY_ASSIGNED_CONTEXT";

    private List<StepReference> uncertaintyRef;

    public GlobalUncertaintyAssignedContext_stp(
            List<StepReference> uncertainty) {
        super("");
        this.uncertaintyRef = uncertainty;
    }

    public List<StepReference> getUncertaintyRef() {
        return uncertaintyRef;
    }

    public List<UncertaintyMeasureWithUnit_stp> getUncertainty() {
        List<UncertaintyMeasureWithUnit_stp> uncertainties = new ArrayList<>();

        for (StepReference ref : uncertaintyRef) {
            if (!(ref.getEntity() instanceof UncertaintyMeasureWithUnit_stp)) {
                Debug.log(Debug.LEVEL.ERROR,"Some how not a uncertainty measure with unit. Ref: " + ref.getRef());
            } else {
                uncertainties.add((UncertaintyMeasureWithUnit_stp) ref.getEntity());
            }
        }

        return uncertainties;
    }
}