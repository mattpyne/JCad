package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.List;
import java.util.Optional;

public class PCurveDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, PCurve_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 3) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        StepReference basisSurfacesRef = stepFile.getReference(variables.get(1));
        StepReference referenceToCurveRef = stepFile.getReference(variables.get(2));

        return Optional.of(
                new PCurve_stp(
                        name,
                        basisSurfacesRef,
                        referenceToCurveRef
                ));
    }
}