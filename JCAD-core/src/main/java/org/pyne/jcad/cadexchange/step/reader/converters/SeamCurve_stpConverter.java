package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.Curve_stp;
import org.pyne.jcad.cadexchange.step.SeamCurve_stp;
import org.pyne.jcad.core.math.geom.Curve;

import java.util.Optional;

public class SeamCurve_stpConverter extends StpCurveToTopoCurveConverter {

    @Override
    public Optional<Curve> convert(Curve_stp stpCurve) {
        if (!(stpCurve instanceof SeamCurve_stp)) {
            return Optional.empty();
        }

        SeamCurve_stp curve = (SeamCurve_stp) stpCurve;

        return StpCurveToTopoCurveConverter.runAllConverts(curve.curve3D());
    }

}
