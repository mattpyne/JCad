package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import java.util.Optional;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.cadexchange.step.Vector_stp;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import verb.core.Vec;
import verb.geom.Line;

/**
 *
 * @author Matthew
 */
public class StpLineToTopoLineConverter extends StpCurveToTopoCurveConverter {

    @Override
    public Optional<Curve> convert(Curve_stp stpCurve) {

        if (!(stpCurve instanceof Line_stp)) {

            return Optional.empty();
        }

        Line_stp stpLine = (Line_stp) stpCurve;

        CartesianPoint_stp stpStart = stpLine.start();
        Vector_stp stpVec = stpLine.direction();
        Direction_stp stpDir = stpVec.direction();

        Array<Number> dir = Array.from(stpDir.x(), stpDir.y(), stpDir.z());

        Array<Number> start = Array.from(stpStart.x(), stpStart.y(), stpStart.z());
        // INFO: Vector is not used in line. stpVec.amount()
        Array<Number> end = Vec.add(start, Vec.mul(1000, dir));

        Line line = new Line(start, end);

        return Optional.of(new NurbsCurve(line));
    }

}
