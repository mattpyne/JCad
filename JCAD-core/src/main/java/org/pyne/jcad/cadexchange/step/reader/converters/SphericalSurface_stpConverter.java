package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.Surface;
import verb.geom.SphericalSurface;

import java.util.Optional;

public class SphericalSurface_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof SphericalSurface_Stp)) {

            return Optional.empty();
        }

        SphericalSurface_Stp sphericalSurface_stp = (SphericalSurface_Stp) stpSurface;

        Axis2Placement3D_stp centerPlacement = sphericalSurface_stp.position();
        CartesianPoint_stp point = centerPlacement.location();
        Direction_stp normal = centerPlacement.axis();
        Direction_stp refAxis = centerPlacement.refAxis();
        double radius = sphericalSurface_stp.getRadius();

        Array<Number> pointArray = Array.from(point.x(), point.y(), point.z());
        Array<Number> normalArray = Array.from(normal.x(), normal.y(), normal.z());
        Array<Number> xAxisArray = Array.from(refAxis.x(), refAxis.y(), refAxis.z());

        return Optional.of(
                new NurbsSurface(
                        new SphericalSurface(pointArray, normalArray, xAxisArray, radius)
                )
        );
    }

}
