package org.pyne.jcad.cadexchange.step;

import java.util.ArrayList;
import java.util.List;

public class BoundedBSplineCurve extends Curve_stp {

    public static final String ENTITY_TYPE = "BOUNDED_CURVE";

    private int degree;
    private List<StepReference> controlPointsListRef;
    private String curveForm;
    private boolean closedCurve;
    private boolean selfIntersecting;
    private List<Integer> knotMultiplicities;
    private List<Double> knots;
    private String knotSpec;
    private final List<Double> weights;

    public BoundedBSplineCurve(
            String name,
            int degree,
            List<StepReference> controlPointsListRef,
            String curveForm,
            boolean closedCurve,
            boolean selfIntersecting,
            List<Integer> knotMultiplicities,
            List<Double> knots,
            String knotSpec,
            List<Double> wieghts) {

        super(name);
        this.degree = degree;
        this.controlPointsListRef = controlPointsListRef;
        this.curveForm = curveForm;
        this.closedCurve = closedCurve;
        this.selfIntersecting = selfIntersecting;
        this.knotMultiplicities = knotMultiplicities;
        this.knots = knots;
        this.knotSpec = knotSpec;
        this.weights = wieghts;
    }

    public int degree() {
        return degree;
    }

    public List<StepReference> controlPointsListRef() {
        return controlPointsListRef;
    }

    public String curveForm() {
        return curveForm;
    }

    public boolean isClosedCurve() {
        return closedCurve;
    }

    public boolean isSelfIntersecting() {
        return selfIntersecting;
    }

    public List<Integer> knotMultiplicities() {
        return knotMultiplicities;
    }

    public List<Double> knots() {
        return knots;
    }

    public String knotSpec() {
        return knotSpec;
    }

    public List<CartesianPoint_stp> controlPointsList() {
        List<CartesianPoint_stp> controlPointsList = new ArrayList<>();

        for (StepReference controlPointRef : controlPointsListRef) {

            if (controlPointRef.getEntity() instanceof CartesianPoint_stp) {
                controlPointsList.add((CartesianPoint_stp) controlPointRef.getEntity());
            } else {

                throw new IllegalArgumentException("Some how we do not have a CartesianPoint_stp. Ref: " + controlPointRef.getRef());
            }

        }

        return controlPointsList;
    }

    public List<Double> getWeights() {
        return weights;
    }
}
