package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.Debug;

import java.util.List;
import java.util.Optional;

public class ProductDefinitionContextDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, ProductDefinitionContext_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 3) {
            Debug.log(Debug.LEVEL.ERROR,"Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new ProductDefinitionContext_stp(
                variables.get(0),
                stepFile.getReference(variables.get(1)),
                variables.get(2)
        ));
    }

}