package org.pyne.jcad.cadexchange.step;

public class PCurve_stp extends Curve_stp {

    public static final String ENTITY_TYPE = "PCURVE";

    private StepReference basisSurfaceRef;
    private StepReference referenceToCurveRef;

    public PCurve_stp(
            String name,
            StepReference basisSurfaceRef,
            StepReference referenceToCurveRef) {
        super(name);
        this.basisSurfaceRef = basisSurfaceRef;
        this.referenceToCurveRef = referenceToCurveRef;
    }

    public StepReference basisSurfaceRef() {
        return basisSurfaceRef;
    }

    public StepReference referenceToCurveRef() {
        return referenceToCurveRef;
    }

    public DefinitionalRepresentation_stp referenceToCurve() {

        if (!(referenceToCurveRef.getEntity() instanceof DefinitionalRepresentation_stp)) {

            throw new IllegalArgumentException("Some how we do not have a DefinitionalRepresentation_stp");
        }

        return (DefinitionalRepresentation_stp) referenceToCurveRef.getEntity();
    }

    public Surface_stp basisSurface() {

        if (!(basisSurfaceRef.getEntity() instanceof Surface_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Surface_stp");
        }

        return (Surface_stp) basisSurfaceRef.getEntity();
    }

}
