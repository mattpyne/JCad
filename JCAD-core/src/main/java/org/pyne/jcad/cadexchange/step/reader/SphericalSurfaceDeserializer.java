package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.List;
import java.util.Optional;

public class SphericalSurfaceDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, SphericalSurface_Stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);

        if (vars.size() != 3) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(
                new SphericalSurface_Stp(
                        vars.get(0),
                        stepFile.getReference(vars.get(1)),
                        Double.parseDouble(vars.get(2))
                )
        );
    }

}
