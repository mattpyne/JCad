package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.Curve_stp;
import org.pyne.jcad.cadexchange.step.PCurve_stp;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.Surface;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PCurve_stpConverter extends StpCurveToTopoCurveConverter {

    @Override
    public Optional<Curve> convert(Curve_stp stpCurve) {
        if (!(stpCurve instanceof PCurve_stp)) {
            return Optional.empty();
        }

        PCurve_stp curve = (PCurve_stp) stpCurve;

        Optional<Surface> opBasisSurface = StpSurfaceToTopoSurfaceConverter.runAllConverts(curve.basisSurface());
        if (!opBasisSurface.isPresent()) {
            return Optional.empty();
        }

        Surface basisSurface = opBasisSurface.get();

        List<Curve> referenceCurves = curve.referenceToCurve()
                .items()
                .stream()
                .map(ea -> StpCurveToTopoCurveConverter.runAllConverts(ea))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        if (referenceCurves.size() != 1) {
            throw new IllegalArgumentException("Unable to process PCurve with more than one reference curve");
        }

        Curve referenceCurve = referenceCurves.get(0);

        List<Point3DC> uvPoints = referenceCurve.tessellate(CadMath.TOLERANCE);

        List<Point3DC> points = uvPoints.stream()
                .map(uvPoint -> basisSurface.point(uvPoint.x(), uvPoint.y()))
                .collect(Collectors.toList());

        return Optional.of(
                CurvePrimitives.bezierCurve(points)
        );
    }

}
