package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class SurfaceStyleFillArea_stp extends StepEntity {

    public final static String ENTITY_TYPE = "SURFACE_STYLE_FILL_AREA";

    private StepReference fillAreaStyleRef;

    public SurfaceStyleFillArea_stp(
            StepReference fillAreaStyleRef)
    {
        super("");
        this.fillAreaStyleRef = fillAreaStyleRef;
    }

    public StepReference getFillAreaStyleRef() {
        return fillAreaStyleRef;
    }

    public FillAreaStyle_stp getFillAreaStyle() {
        if (!(fillAreaStyleRef.getEntity() instanceof FillAreaStyle_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Don't have fill area style. Ref: " + fillAreaStyleRef.getRef());
        }

        return (FillAreaStyle_stp) fillAreaStyleRef.getEntity();
    }
}

