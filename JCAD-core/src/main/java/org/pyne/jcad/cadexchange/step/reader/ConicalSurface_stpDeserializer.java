package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.List;
import java.util.Optional;

public class ConicalSurface_stpDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, ConicalSurface_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> vars = StepRegex.variables(entity);

        if (vars.size() != 4) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(
                new ConicalSurface_stp(
                        vars.get(0),
                        stepFile.getReference(vars.get(1)),
                        Double.parseDouble(vars.get(2)),
                        Double.parseDouble(vars.get(3))
                )
        );
    }

}
