package org.pyne.jcad.cadexchange.step;

public class ItemDefinedTransformation_stp extends StepEntity {

    public static final String ENTITY_TYPE = "ITEM_DEFINED_TRANSFORMATION";

    private StepReference transformationItem1;
    private StepReference transformationItem2;

    public ItemDefinedTransformation_stp(
            String name,
            String description,
            StepReference transformationItem1,
            StepReference transformationItem2) {
        super("");
        this.transformationItem1 = transformationItem1;
        this.transformationItem2 = transformationItem2;
    }

    public StepReference getTransformationItem1() {
        return transformationItem1;
    }

    public StepReference getTransformationItem2() {
        return transformationItem2;
    }

    public Axis2Placement3D_stp transformationItem1() {

        if (!(transformationItem1.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) transformationItem1.getEntity();
    }

    public Axis2Placement3D_stp transformationItem2() {

        if (!(transformationItem2.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) transformationItem2.getEntity();
    }

}
