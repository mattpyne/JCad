package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

import java.util.ArrayList;
import java.util.List;

public class ProductRelatedProductCategory_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT_RELATED_PRODUCT_CATEGORY";

    private String description;
    private List<StepReference> productRefs;

    public ProductRelatedProductCategory_stp(
            String name,
            String description,
            List<StepReference> productRefs) {
        super(name);
        this.description = description;
        this.productRefs = productRefs;
    }

    public String getDescription() {
        return description;
    }

    public List<StepReference> getProductRefs() {
        return productRefs;
    }

    public List<Product_stp> getProducts() {
        List<Product_stp> products = new ArrayList<>();

        for (StepReference productRef : productRefs) {
            if (!(productRef.getEntity() instanceof Product_stp)) {
                Debug.log(Debug.LEVEL.ERROR,"Expected product for Ref: " +productRef.getRef());
            } else {
                products.add((Product_stp) productRef.getEntity());
            }
        }

        return products;
    }
}
