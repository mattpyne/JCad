package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.Curve_stp;
import org.pyne.jcad.cadexchange.step.SurfaceCurve_stp;
import org.pyne.jcad.core.math.geom.Curve;

import java.util.Optional;

public class SurfaceCurve_stpConverter extends StpCurveToTopoCurveConverter {

    @Override
    public Optional<Curve> convert(Curve_stp stpCurve) {
        if (!(stpCurve instanceof SurfaceCurve_stp)) {
            return Optional.empty();
        }

        SurfaceCurve_stp curve = (SurfaceCurve_stp) stpCurve;

        return StpCurveToTopoCurveConverter.runAllConverts(curve.curve3D());
    }

}
