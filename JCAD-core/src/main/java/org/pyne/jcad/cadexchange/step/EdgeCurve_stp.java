package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

/**
 *
 * @author Matthew
 */
public class EdgeCurve_stp extends StepEntity {

    public final static String ENTITY_TYPE = "EDGE_CURVE";

    private StepReference vertexA;
    private StepReference vertexB;
    private StepReference curve;
    private boolean sameSence;

    public EdgeCurve_stp(String name, StepReference vertexA, StepReference vertexB, StepReference curve, boolean sameSence) {
        super(name);
        this.vertexA = vertexA;
        this.vertexB = vertexB;
        this.curve = curve;
        this.sameSence = sameSence;
    }

    public VertexPoint_stp vertexA() {
        if (!(vertexA.getEntity() instanceof VertexPoint_stp)) {

            throw new IllegalArgumentException("We do not have a VertexPoint_stp for vertexA.");
        }

        return (VertexPoint_stp) vertexA.getEntity();
    }

    public VertexPoint_stp vertexB() {
        if (!(vertexB.getEntity() instanceof VertexPoint_stp)) {

            throw new IllegalArgumentException("We do not have a VertexPoint_stp for vertexB.");
        }

        return (VertexPoint_stp) vertexB.getEntity();
    }

    public Curve_stp curve() {
        if (!(curve.getEntity() instanceof Curve_stp)) {

            Debug.log(new IllegalArgumentException("Some how we do not have a Curve"));
        }

        return (Curve_stp) curve.getEntity();
    }

    public boolean isSameSence() {
        return sameSence;
    }

}
