package org.pyne.jcad.cadexchange.step.reader.converters;

import haxe.root.Array;
import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.Vec;
import verb.geom.ConicalSurface;

import java.util.Optional;

public class ConicalSurface_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof ConicalSurface_stp)) {

            return Optional.empty();
        }

        ConicalSurface_stp conicalSurface_stp = (ConicalSurface_stp) stpSurface;

        Axis2Placement3D_stp centerPlacement = conicalSurface_stp.position();
        CartesianPoint_stp point = centerPlacement.location();
        Direction_stp normal = centerPlacement.axis();
        Direction_stp refAxis = centerPlacement.refAxis();
        double radius = conicalSurface_stp.radius() *  4.;
        double semiAngle = CadMath.toRadians(conicalSurface_stp.getSemiAngle());
        double height = radius / Math.tan(semiAngle);
        double heightOffset = conicalSurface_stp.radius() / Math.tan(semiAngle);

        Array<Number> pointArray = Array.from(point.x(), point.y(), point.z());
        Array<Number> normalArray = Array.from(normal.x(), normal.y(), normal.z());
        Array<Number> xAxisArray = Array.from(refAxis.x(), refAxis.y(), refAxis.z());

        pointArray = Vec.add(pointArray, Vec.mul(height - heightOffset, normalArray));

        return Optional.of(
                new NurbsSurface(
                        new ConicalSurface(normalArray, xAxisArray, pointArray, -height, radius)
                )
        );
    }

}
