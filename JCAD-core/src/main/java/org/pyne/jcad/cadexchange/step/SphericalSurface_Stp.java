package org.pyne.jcad.cadexchange.step;

public class SphericalSurface_Stp extends Surface_stp {

    public final static String ENTITY_TYPE = "SPHERICAL_SURFACE";

    private StepReference position;
    private double radius;

    public SphericalSurface_Stp(
            String name,
            StepReference position,
            double radius) {
        super(name);
        this.position = position;
        this.radius = radius;
    }

    public Axis2Placement3D_stp position() {
        if (!(position.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) position.getEntity();
    }

    public double getRadius() {
        return radius;
    }
}
