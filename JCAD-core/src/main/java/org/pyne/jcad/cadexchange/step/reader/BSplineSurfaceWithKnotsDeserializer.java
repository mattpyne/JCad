package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BSplineSurfaceWithKnotsDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, BSplineSurfaceWithKnots_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 13) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        int degreeU = Integer.parseInt(variables.get(1));
        int degreeV = Integer.parseInt(variables.get(2));
        List<List<StepReference>> controlPointsListRef = getControlPointsList(stepFile, variables.get(3));
        String surfaceForm = variables.get(4);
        boolean closedU = variables.get(5).toLowerCase().equals(".t.");
        boolean closedV = variables.get(6).toLowerCase().equals(".t.");
        boolean selfIntersecting = variables.get(7).toLowerCase().equals(".t.");
        List<Integer> knotMultiplicitiesU = getKnotMultiplicities(variables.get(8));
        List<Integer> knotMultiplicitiesV = getKnotMultiplicities(variables.get(9));
        List<Double> knotsU = getKnots(variables.get(10));
        List<Double> knotsV = getKnots(variables.get(11));
        String knotSpec = variables.get(12);

        return Optional.of(
                new BSplineSurfaceWithKnots_stp(
                        name,
                        degreeU,
                        degreeV,
                        controlPointsListRef,
                        surfaceForm,
                        closedU,
                        closedV,
                        selfIntersecting,
                        knotMultiplicitiesU,
                        knotMultiplicitiesV,
                        knotsU,
                        knotsV,
                        knotSpec));
    }

    private List<Double> getKnots(String knots) {
        knots = knots.replace("(", "");
        knots = knots.replace(")", "");
        return Arrays.asList(knots.split(",")).stream()
                .map(knotMult -> Double.parseDouble(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<Integer> getKnotMultiplicities(String knotMultiplicitiesStr) {
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace("(", "");
        knotMultiplicitiesStr = knotMultiplicitiesStr.replace(")", "");
        return Arrays.asList(knotMultiplicitiesStr.split(",")).stream()
                .map(knotMult -> Integer.parseInt(knotMult.trim()))
                .collect(Collectors.toList());
    }

    private List<List<StepReference>> getControlPointsList(StepFile stepFile, String controlPointsListStr) {
        List<List<StepReference>> controlPointsUVList = new ArrayList<>();

        for (String controlPointUV : StepRegex.variables(controlPointsListStr)) {
            controlPointUV = controlPointUV.replace("(", "");
            controlPointUV = controlPointUV.replace(")", "");
            controlPointsUVList.add(Arrays.asList(controlPointUV.split(","))
                    .stream()
                    .map(controlPoint -> stepFile.getReference(controlPoint))
                    .collect(Collectors.toList()));
        }

        return controlPointsUVList;
    }

}
