package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew
 */
public class Plane_stp extends Surface_stp {

    public static final String ENTITY_TYPE = "PLANE";

    private StepReference position;

    public Plane_stp(String name, StepReference position) {
        super(name);
        this.position = position;
    }

    public Axis2Placement3D_stp position() {
        if (null == position || !(position.getEntity() instanceof Axis2Placement3D_stp)) {

            throw new IllegalArgumentException("Some how we do not have a Axis2Placement3D_stp");
        }

        return (Axis2Placement3D_stp) position.getEntity();
    }
}
