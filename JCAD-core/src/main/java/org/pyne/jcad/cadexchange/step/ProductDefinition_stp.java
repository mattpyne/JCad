package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ProductDefinition_stp extends StepEntity {

    public static final String ENTITY_TYPE = "PRODUCT_DEFINITION";

    private String id;
    private String description;
    private StepReference formationRef;
    private StepReference frameOfReferenceRef;

    public ProductDefinition_stp(
            String id,
            String description,
            StepReference formationRef,
            StepReference frameOfReferenceRef) {
        super("");
        this.id = id;
        this.description = description;
        this.formationRef = formationRef;
        this.frameOfReferenceRef = frameOfReferenceRef;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public StepReference getFormationRef() {
        return formationRef;
    }

    public ProductDefinitionFormation_stp getFormation() {
        if(!(formationRef.getEntity() instanceof ProductDefinitionFormation_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no formation found! Ref: " + formationRef.getRef());
        }

        return (ProductDefinitionFormation_stp) formationRef.getEntity();
    }

    public StepReference getFrameOfReferenceRef() {
        return frameOfReferenceRef;
    }

    public ProductDefinitionContext_stp getFrameOfReference() {
        if(!(frameOfReferenceRef.getEntity() instanceof ProductDefinitionContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no frame of reference found! Ref: " + frameOfReferenceRef.getRef());
        }

        return (ProductDefinitionContext_stp) frameOfReferenceRef.getEntity();
    }
}