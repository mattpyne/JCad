package org.pyne.jcad.cadexchange.step.reader;

import java.util.Optional;
import java.util.regex.Matcher;

import org.pyne.jcad.cadexchange.step.*;

/**
 * @author Matthew
 */
public class Axis2Placement3DDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, Axis2Placement3D_stp.ENTITY_TYPE)) {
            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.INPUTS4_PATTERN.matcher(entity);
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = regexMatcher.group(1);
        String locationRef = regexMatcher.group(2);
        String axisRef = regexMatcher.group(3);
        String axisRefRef = regexMatcher.group(4);
        if (null == locationRef
                || null == axisRef
                || null == axisRefRef) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new Axis2Placement3D_stp(
                name,
                stepFile.getReference(locationRef),
                stepFile.getReference(axisRef),
                stepFile.getReference(axisRefRef)
        ));
    }

}
