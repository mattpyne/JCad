package org.pyne.jcad.cadexchange.step.reader.converters;

import org.pyne.jcad.cadexchange.step.*;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.Surface;
import org.pyne.jcad.core.math.geom.SurfacePrimitives;

import java.util.Optional;

public class SurfaceOfRevolution_stpConverter extends StpSurfaceToTopoSurfaceConverter {

    @Override
    public Optional<Surface> convert(Surface_stp stpSurface) {

        if (!(stpSurface instanceof SurfaceOfRevolution_stp)) {

            return Optional.empty();
        }

        SurfaceOfRevolution_stp surface_stp = (SurfaceOfRevolution_stp) stpSurface;

        Curve_stp sweptCurve = surface_stp.sweptCurve();
        Axis1Placement_stp axis_stp = surface_stp.axisPosition();
        CartesianPoint_stp position_stp = axis_stp.location();
        Direction_stp direction_stp = axis_stp.axis();

        Optional<Curve> opSweptCurve = StpCurveToTopoCurveConverter.runAllConverts(sweptCurve);

        if (!opSweptCurve.isPresent()) {
            return Optional.empty();
        }

        Point3DC center = new Point3DC(
                position_stp.x(),
                position_stp.y(),
                position_stp.z());

        Point3DC axis = new Point3DC(
                direction_stp.x(),
                direction_stp.y(),
                direction_stp.z());

        return Optional.of(
                SurfacePrimitives.revolution(
                        opSweptCurve.get(),
                        center,
                        axis,
                        2 * Math.PI));
    }

}
