package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class ApplicationProtocolDefinition_stp extends StepEntity {

    public static final String ENTITY_TYPE = "APPLICATION_PROTOCOL_DEFINITION";
    private String status;
    private String applicationInterpretedModelSchemaName;
    private Integer applicationProtocolYear;
    private StepReference applicationRef;

    public ApplicationProtocolDefinition_stp(
            String status,
            String applicationInterpretedModelSchemaName,
            Integer applicationProtocolYear,
            StepReference application) {
        super("");
        this.status = status;
        this.applicationInterpretedModelSchemaName = applicationInterpretedModelSchemaName;
        this.applicationProtocolYear = applicationProtocolYear;
        this.applicationRef = application;
    }

    public String getStatus() {
        return status;
    }

    public String getApplicationInterpretedModelSchemaName() {
        return applicationInterpretedModelSchemaName;
    }

    public Integer getApplicationProtocolYear() {
        return applicationProtocolYear;
    }

    public StepReference getApplicationRef() {
        return applicationRef;
    }

    public ApplicationContext_stp getApplication() {
        if(!(applicationRef.getEntity() instanceof ApplicationContext_stp)) {
            Debug.log(Debug.LEVEL.ERROR, "Some how no application found! Ref: " + applicationRef.getRef());
        }

        return (ApplicationContext_stp) applicationRef.getEntity();
    }
}