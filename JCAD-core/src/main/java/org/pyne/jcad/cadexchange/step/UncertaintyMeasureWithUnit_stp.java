package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class UncertaintyMeasureWithUnit_stp extends StepEntity {

    public static final String ENTITY_TYPE = "UNCERTAINTY_MEASURE_WITH_UNIT";

    private MeasureValue valueComponent;
    private StepReference unitRef;
    private String description;

    public UncertaintyMeasureWithUnit_stp(
            MeasureValue valueComponent,
            StepReference unitRef,
            String name,
            String description) {
        super(name);
        this.valueComponent = valueComponent;
        this.unitRef = unitRef;
        this.description = description;
    }

    public MeasureValue getValueComponent() {
        return valueComponent;
    }

    public StepReference getUnitRef() {
        return unitRef;
    }

    public Unit_stp getUnit() {
        if (!(unitRef.getEntity() instanceof  Unit_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no unit. Ref: " + unitRef.getRef());
        }

        return (Unit_stp) unitRef.getEntity();
    }

    public String getDescription() {
        return description;
    }
}