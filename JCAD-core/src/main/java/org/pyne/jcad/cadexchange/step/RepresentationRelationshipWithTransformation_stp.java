package org.pyne.jcad.cadexchange.step;

import org.pyne.jcad.core.Debug;

public class RepresentationRelationshipWithTransformation_stp extends StepEntity {

    public static final String ENTITY_TYPE = "REPRESENTATION_RELATIONSHIP_WITH_TRANSFORMATION";

    private StepReference transformRef;

    public RepresentationRelationshipWithTransformation_stp(
            StepReference transformRef) {
        super("");
        this.transformRef = transformRef;
    }

    public StepReference getTransformRef() {
        return transformRef;
    }

    public ItemDefinedTransformation_stp getTransform() {
        if (!(transformRef.getEntity() instanceof ItemDefinedTransformation_stp)) {
            Debug.log(Debug.LEVEL.ERROR,"Some how no transform found! Ref: " + transformRef.getRef());
        }

        return (ItemDefinedTransformation_stp) transformRef.getEntity();
    }

}
