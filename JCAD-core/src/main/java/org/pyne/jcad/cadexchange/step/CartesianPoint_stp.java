package org.pyne.jcad.cadexchange.step;

/**
 *
 * @author Matthew Pyne
 */
public class CartesianPoint_stp extends StepEntity {
    
    public static final String ENTITY_TYPE = "CARTESIAN_POINT";
    
    private double x;
    private double y;
    private double z;
    
    public CartesianPoint_stp() {
        this(0, 0, 0);
    }

    public CartesianPoint_stp(double x, double y) {
        this(x, y, 0.);
    }

    public CartesianPoint_stp(double x, double y, double z) {
        super("");
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double z() {
        return z;
    }
    
}
