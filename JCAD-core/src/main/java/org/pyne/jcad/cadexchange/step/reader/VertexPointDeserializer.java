package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.StepEntity;
import org.pyne.jcad.cadexchange.step.StepFile;
import org.pyne.jcad.cadexchange.step.StepRegex;
import org.pyne.jcad.cadexchange.step.VertexPoint_stp;

import java.util.Optional;
import java.util.regex.Matcher;

/**
 *
 * @author Matthew
 */
public class VertexPointDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, VertexPoint_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        Matcher regexMatcher = StepRegex.REFS_PATTERN.matcher(entity);

        regexMatcher.find();
        if (!regexMatcher.find()) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String ref = regexMatcher.group(1);

        if (null == ref) {

            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        return Optional.of(new VertexPoint_stp(stepFile.getReference(ref)));
    }

}
