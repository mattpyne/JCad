package org.pyne.jcad.cadexchange.step.reader;

import org.pyne.jcad.cadexchange.step.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SeamCurveDeserializer extends StepEntityDeserializer {

    @Override
    Optional<StepEntity> deserialize(String entity, StepFile stepFile) {
        if (!StepRegex.isType(entity, SeamCurve_stp.ENTITY_TYPE)) {

            return Optional.empty();
        }

        List<String> variables = StepRegex.variables(entity);

        if (variables.size() != 4) {
            System.err.println("Failed to deserialize " + entity);
            return Optional.empty();
        }

        String name = variables.get(0);
        StepReference curve3DRef = stepFile.getReference(variables.get(1));
        List<StepReference> associatedGeometryRef = getAssociatedGeometryRef(stepFile, variables.get(2));
        StepReference masterRepresentationRef = stepFile.getReference(variables.get(3));

        return Optional.of(
                new SeamCurve_stp(
                        name,
                        curve3DRef,
                        associatedGeometryRef,
                        masterRepresentationRef
                ));
    }

    private List<StepReference> getAssociatedGeometryRef(StepFile stepFile, String associatedGeometryStr) {
        associatedGeometryStr = associatedGeometryStr.replace("(", "");
        associatedGeometryStr = associatedGeometryStr.replace(")", "");
        return Arrays.stream(associatedGeometryStr.split(","))
                .map(stepFile::getReference)
                .collect(Collectors.toList());
    }
}
