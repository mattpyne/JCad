package org.pyne.jcad.core.math.brep;

import haxe.lang.EmptyObject;

import java.util.*;

import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.brep.eval.BrepValidator;
import org.pyne.jcad.core.math.brep.eval.bool.OperationObserver;
import org.pyne.jcad.core.math.brep.eval.bool.ShellBooleanOps;
import org.pyne.jcad.core.DataContainer;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.Matrix3;
import verb.core.BoundingBox;

/**
 *
 * @author matt pyne
 */
public class Shell extends DataContainer implements Topo {

    private final ArrayList<Face> faces;

    /**
     *
     */
    public Shell() {
        faces = new ArrayList<>();
    }

    /**
     *
     * @param other
     */
    public Shell(Shell other) {
        faces = other.faces.stream()
                .map(Face::copy)
                .collect(Collectors.toCollection(ArrayList::new));
        faces.forEach(face -> face.setShell(this));
    }

    /**
     *
     * @return
     */
    @Override
    public Shell copy() {
        return clone();
    }

    @Override
    public Shell clone() {

//        BrepBuilder builder = new BrepBuilder();
//
//        for (Face face : faces) {
//
//            builder.face(face.surface().copy());
//            builder.newOuterLoop();
//            Loop outerLoop = face.getOuterLoop();
//            for (HalfEdge edge : outerLoop.halfEdges()) {
//                builder.edge(edge.vertexA(), edge.vertexB(), edge.edge().curve());
//            }
//            builder.loop.setId(outerLoop.getId());
//            builder.loop.setData(outerLoop.data());
//
//            for (Loop innerLoop : face.getInnerLoops()) {
//                builder.newInnerLoop();
//                for (HalfEdge edge : innerLoop.halfEdges()) {
//                    builder.edge(edge.vertexA(), edge.vertexB(), edge.edge().curve());
//                }
//
//                builder.loop.setId(innerLoop.getId());
//                builder.loop.setData(innerLoop.data());
//            }
//
//
//            Face faceClone = builder.buildFace();
//
//            faceClone.setId(face.getId());
//            faceClone.setData(face.data());
//
//        }
//        Shell clone = builder.build();

        Map<Vertex, Vertex> vertexClones = new HashMap();
        for (Vertex v : vertexs()) {
            vertexClones.put(v, v.clone());
        }

        Map<Edge, Edge> edgeClones = new HashMap();
        for (Edge e : edges()) {
            Edge clone = e.clone();

            clone.halfEdge1().setVertexA(vertexClones.get(e.halfEdge1().vertexA()));
            clone.halfEdge1().vertexA().addEdge(clone.halfEdge1());
            clone.halfEdge1().setVertexB(vertexClones.get(e.halfEdge1().vertexB()));
            clone.halfEdge1().vertexB().addEdge(clone.halfEdge1());

            clone.halfEdge2().setVertexA(vertexClones.get(e.halfEdge2().vertexA()));
            clone.halfEdge2().vertexA().addEdge(clone.halfEdge2());
            clone.halfEdge2().setVertexB(vertexClones.get(e.halfEdge2().vertexB()));
            clone.halfEdge2().vertexB().addEdge(clone.halfEdge2());

            edgeClones.put(e, clone);
        }

        Shell clone = new Shell();
        for (Face face : faces) {

            Face faceClone = new Face(face.surface().copy());
            faceClone.setId(face.getId());
            faceClone.setShell(clone);

            cloneLoop(face.getOuterLoop(), faceClone.getOuterLoop(), edgeClones);
            for (Loop loop : face.getInnerLoops()) {

                Loop loopClone = new Loop(faceClone);
                cloneLoop(loop, loopClone, edgeClones);
                faceClone.getInnerLoops().add(loopClone);
            }
            clone.faces.add(faceClone);
        }

        clone.setId(getId());
        if (!BrepValidator.isValid(clone)) {
            System.out.println("huh");
        }

        return clone;
    }

    private void cloneLoop(Loop loop, Loop loopClone, Map<Edge, Edge> edgeClones) {
        for (HalfEdge he : loop.halfEdges()) {
            Edge edgeClone = edgeClones.get(he.edge());
            if (edgeClone.halfEdge2().isInverted() == he.isInverted()) {
                loopClone.halfEdges().add(edgeClone.halfEdge2());
            } else {
                loopClone.halfEdges().add(edgeClone.halfEdge1());
            }
        }
        loopClone.link();
        loopClone.setId(loop.getId());
    }

    /**
     *
     * @return
     */
    public List<Face> faces() {
        return faces;
    }

    /**
     *
     * @param face
     */
    public void addFace(Face face) {
        faces.add(face);
    }
    
    /**
     *
     * @param faces
     */
    public void addFaces(Collection<Face> faces) {
        this.faces.addAll(faces);
    }

    /**
     *
     */
    public void invert() {
        faces.parallelStream()
                .forEach(Face::invert);
    }

    /**
     *
     * @return
     */
    public Stream<Face> streamFaces() {
        return faces.stream();
    }
    
    /**
     *
     * @return
     */
    public Stream<Loop> streamLoops() {
        return faces.stream().flatMap(Face::streamLoops);
    }

    /**
     *
     * @return
     */
    public Stream<Vertex> streamVertices() {
        return faces.stream()
                .flatMap(Face::streamVertices)
                .distinct()
                .collect(Collectors.toList())
                .stream();
    }

    /**
     *
     * @return
     */
    public List<Vertex> vertexs() {
        return faces.stream()
                .flatMap(f -> f.streamVertices())
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public Stream<HalfEdge> streamHalfEdges() {
        return faces.stream()
                .flatMap(Face::streamHalfEdges);
    }

    /**
     *
     * @return
     */
    public List<HalfEdge> getHalfEdges() {
        return streamHalfEdges().collect(Collectors.toList());
    }

    public Stream<Edge> streamEdges() {
        return streamHalfEdges().map(he -> he.edge()).distinct();
    }

    public LinkedHashSet<Edge> edges() {
        LinkedHashSet<Edge> edges = new LinkedHashSet();
        for (Face face : faces) {
            for (Loop loop : face.loops()) {
                for (HalfEdge he : loop.halfEdges()) {
                    edges.add(he.edge());
                }
            }
        }
        return edges;
    }

    @Override
    public void transform(Matrix3 tr) {
        cache().clear();

        streamFaces().forEach((f) -> {
            f.cache().clear();
            f.loops().forEach(l -> l.cache().clear());
            f.setSurface(f.surface().transform(tr));
        });

        streamVertices().forEach((v) -> {
            v.transform(tr);
        });

        streamEdges().forEach((e) -> {
            e.halfEdge1().cache().clear();
            e.halfEdge2().cache().clear();
            e.cache().clear();
            e.setCurve(e.curve().transform(tr));
        });
        
        Debug.checkLoopsValid(this);
        Debug.checkEdgesValid(this);
    }

    @Override
    public Shell transformed(Matrix3 tr) {
        Shell instance = copy();

        instance.transform(tr);

        return instance;
    }

    public Shell union(Shell other) {
        return new ShellBooleanOps().union(this, other);
    }

    public Shell union(Shell other, OperationObserver observer) {
        return new ShellBooleanOps().union(this, other, observer);
    }

    public Shell intersect(Shell other) {
        return new ShellBooleanOps().intersect(this, other);
    }

    public Shell intersect(Shell other, OperationObserver observer) {
        return new ShellBooleanOps().intersect(this, other, observer);
    }

    public Shell subtract(Shell other) {
        return new ShellBooleanOps().subtract(this, other);
    }

    public Shell subtract(Shell other, OperationObserver observer) {
        return new ShellBooleanOps().subtract(this, other, observer);
    }

    public BoundingBox bounds() {
        return cache().get(
                Cache.BOUNDING_BOX,
                () -> {
                    BoundingBox bounds = new BoundingBox(EmptyObject.EMPTY);
                    for (Face face : faces()) {
                        bounds.add(face.bounds().max);
                        bounds.add(face.bounds().min);
                    }
                    return bounds;
                });
    }

    @Override
    public boolean isEmpty() {
        return faces.isEmpty() 
                || faces.stream().allMatch(Face::isEmpty);
    }

}
