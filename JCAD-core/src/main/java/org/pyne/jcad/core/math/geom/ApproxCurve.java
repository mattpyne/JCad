package org.pyne.jcad.core.math.geom;

import haxe.root.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Matrix3;
import verb.core.BoundingBox;
import verb.core.Interval;

/**
 *
 * @author matt pyne
 */
public class ApproxCurve extends Curve {

    private List<Point3DC> points;
    private List<Line> segments;
    private List<Double> tShifts;

    /**
     *
     * @param points
     */
    public ApproxCurve(List<Point3DC> points) {
        super(TYPE.APPROX);
        this.points = points;
        this.segments = new ArrayList<>();
        this.tShifts = new ArrayList<>();
        tShifts.add(0.);
        for (int i = 1; i < points.size(); ++i) {
            Point3DC a = points.get(i - 1);
            Point3DC b = points.get(i);
            Line.fromSegment(a, b)
                    .ifPresent(line -> {
                        segments.add(line);
                        tShifts.add(tShifts.get(tShifts.size() - 1) + line.param(b));
                    });

        }
    }

    @Override
    public boolean intersectsSurface(Surface surface) {
        return !pointsOfSurfaceIntersection(surface).isEmpty();
    }

    @Override
    public double param(Point3DC point) {
        for (int i = 0; i < points.size(); ++i) {
            if (points.get(i).equals(point)) {
                return tShifts.get(i);
            }
        }

        for (int i = 0; i < segments.size(); ++i) {
            Line line = segments.get(i);
            double subT = line.param(point);
            if (subT > 0 && subT < tShifts.get(i + 1)) {
                return tShifts.get(i) + subT;
            }
        }
        return Double.NaN;
    }

    @Override
    public Point3DC point(double t) {
        for (int i = 0; i < points.size(); ++i) {
            if (CadMath.isZero(t - tShifts.get(i))) {

                return points.get(i);
            }
        }
        for (int i = 1; i < points.size(); ++i) {
            if (t > tShifts.get(i - 1) && t < tShifts.get(i)) {

                return segments.get(i - 1)
                        .point(t - tShifts.get(i - 1));
            }
        }
        return null;
    }

    @Override
    public Set<Point3DC> pointsOfSurfaceIntersection(Surface surface) {
        return segments.stream()
                .map(line -> line.pointsOfSurfaceIntersection(surface))
                .flatMap(Collection::stream)
                .collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public ApproxCurve getTranslated(Point3DC vector) {
        List<Point3DC> translatedPoints = points.stream()
                .map(point -> point.plus(vector))
                .collect(Collectors.toList());

        return new ApproxCurve(translatedPoints);
    }

    @Override
    public Interval<Number> domain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int degree() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Array<Number> degree1Tess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Array<Array<Number>> eval(double u, int num) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ParametricCurve transform(Matrix3 tr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int optimalSplits() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void normalizeParametrization() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ParametricCurve invert() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ParametricCurve> split(Point3DC point) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BoundingBox bounds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void translated(Point3DC vector) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
