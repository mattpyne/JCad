package org.pyne.jcad.core.entity;

import java.util.UUID;

/**
 *
 * @author Matthew Pyne
 */
public class Identification implements IComponent {

    private String name;
    private String displayName;
    private String uuid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public void invalidate() {
        this.uuid = null;
        this.displayName = null;
        this.name = null;
    }

    @Override
    public void update() {
        if (null == uuid) {
            
            this.uuid = UUID.randomUUID().toString();
        }
        
        if (null == name) {
            
            this.name = "No Name";
        }
        
        if (null == displayName) {
            
            this.displayName = "No Display Name";
        }
    }

    @Override
    public void register(Entity entity) {
    }

}
