package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.brep.creators.VertexFactory;

/**
 *
 * @author Matthew
 */
public class Node {

    double u;
    Point3DC point;
    private Vertex vertex;
    boolean entersA;
    boolean entersB;
    boolean leavesA;
    boolean leavesB;
    EdgeSplitInfo edgeSplitInfo = null;
    VertexFactory vertexFactory;

    public Node(Point3DC point, double u, Vertex vertex, VertexFactory vertexFactory) {
        this.point = point;
        this.u = u;
        this.vertex = vertex;
        this.vertexFactory = vertexFactory;
    }

    public Vertex vertex() {
        if (null == vertex) {
            vertex = vertexFactory.create(point);
        }
        return vertex;
    }

    public void setEnters(int operand, boolean value) {
        if (operand == 0) {
            entersA = value;
        } else {
            entersB = value;
        }
    }

    public void setLeaves(int operand, boolean value) {
        if (operand == 0) {
            leavesA = value;
        } else {
            leavesB = value;
        }
    }

    @Override
    public String toString() {
        return "Node{"
                + "u=" + u
                + ", point=" + point
                + ", vertex=" + vertex
                + ", entersA=" + entersA
                + ", entersB=" + entersB
                + ", leavesA=" + leavesA
                + ", leavesB=" + leavesB + '}';
    }

}
