package org.pyne.jcad.core.exe.datastructures;

import java.util.Comparator;

public class KdPointComparators {

    public static final Comparator<KdPoint> X_COMPARATOR = new Comparator<KdPoint>() {
        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(KdPoint o1, KdPoint o2) {
            if (o1.x < o2.x)
                return -1;
            if (o1.x > o2.x)
                return 1;
            return 0;
        }
    };

    public static final Comparator<KdPoint> Y_COMPARATOR = new Comparator<KdPoint>() {
        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(KdPoint o1, KdPoint o2) {
            if (o1.y < o2.y)
                return -1;
            if (o1.y > o2.y)
                return 1;
            return 0;
        }
    };

    public static final Comparator<KdPoint> Z_COMPARATOR = new Comparator<KdPoint>() {
        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(KdPoint o1, KdPoint o2) {
            if (o1.z < o2.z)
                return -1;
            if (o1.z > o2.z)
                return 1;
            return 0;
        }
    };

}
