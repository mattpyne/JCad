package org.pyne.jcad.core.math;

import haxe.root.Array;

/**
 *
 * @author Matthew Pyne
 */
public class OptimizationResult {

    private Array<Number> solution;
    private double function;
    private Array<Number> gradient;
    private Array<Array<Number>> invHessian;
    private int iterations;
    private String msg;

    /**
     * 
     * @param x0
     * @param f0
     * @param g0
     * @param H1
     * @param it
     * @param msg 
     */
    public OptimizationResult(Array<Number> x0, double f0, Array<Number> g0, Array<Array<Number>> H1, int it, String msg) {
        solution = x0;
        function = f0;
        gradient = g0;
        invHessian = H1;
        iterations = it;
        this.msg = msg;
//        System.out.println("Optimization Mesage:" + msg);
    }

    public Array<Number> getSolution() {
        return solution;
    }

    public double getFunction() {
        return function;
    }

    public Array<Number> getGradient() {
        return gradient;
    }

    public Array<Array<Number>> getInvHessian() {
        return invHessian;
    }

    public int getIterations() {
        return iterations;
    }

    public String getMsg() {
        return msg;
    }

}
