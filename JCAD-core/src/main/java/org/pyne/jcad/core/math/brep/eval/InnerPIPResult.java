package org.pyne.jcad.core.math.brep.eval;

/**
 *
 * @author Matthew Pyne
 */
public class InnerPIPResult {

    public Boolean inside = false;
    public Boolean edge = false;
    public Boolean vertex = false;

    private InnerPIPResult() {

    }

    public static InnerPIPResult inside(Boolean inside) {
        InnerPIPResult result = new InnerPIPResult();
        result.inside = inside;
        return result;
    }

    public static InnerPIPResult edge(Boolean inside) {
        InnerPIPResult result = new InnerPIPResult();
        result.edge = true;
        result.inside = inside;
        return result;
    }

    public static InnerPIPResult vertex(Boolean inside) {
        InnerPIPResult result = new InnerPIPResult();
        result.vertex = true;
        result.inside = inside;
        return result;
    }
}
