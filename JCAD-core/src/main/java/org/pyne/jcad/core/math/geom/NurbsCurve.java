package org.pyne.jcad.core.math.geom;

import haxe.lang.EmptyObject;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.geom.eval.CurveIntersectData;
import org.pyne.jcad.core.math.geom.eval.CurveTessellate;
import org.pyne.jcad.core.verbinterop.VerbNurbsHelpers;
import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.verbinterop.VerbInterop;
import verb.core.ArrayExtensions;
import verb.core.BoundingBox;
import verb.core.Interval;
import verb.core.NurbsCurveData;
import verb.core.Vec;
import verb.eval.Eval;
import verb.eval.Intersect;
import verb.eval.Modify;

/**
 *
 * @author Matthew Pyne
 */
public class NurbsCurve extends Curve {

    protected verb.geom.NurbsCurve impl;

    public NurbsCurve(verb.geom.NurbsCurve verbCurve) {
        super(VerbInterop.getCurveType(verbCurve));
        VerbNurbsHelpers.normalizeCurveParametrization(verbCurve._data);
        this.impl = verbCurve;
    }

    public NurbsCurve(verb.geom.NurbsCurve verbCurve, TYPE type) {
        super(type);
        VerbNurbsHelpers.normalizeCurveParametrization(verbCurve._data);
        this.impl = verbCurve;
    }

    @Override
    public NurbsCurve copy() {
        return new NurbsCurve(this.impl.clone(), getType());
    }

    public verb.geom.NurbsCurve getVerb() {
        return impl;
    }

    public NurbsCurveData getData() {
        return impl._data;
    }

    public void setData(NurbsCurve data) {
        cache().clear();
        impl = data.impl;
    }

    @Override
    public Interval<Number> domain() {
        return VerbNurbsHelpers.curveDomain(getData());
    }

    @Override
    public int degree() {
        return getData().degree;
    }

    @Override
    public Array<Number> degree1Tess() {
        return VerbNurbsHelpers.distinctKnots(getData());
    }

    @Override
    public Point3DC point(double param) {
        return cache().get(Cache.POINT + Double.hashCode(param),
                () -> new Point3DC(impl.point(param)));
    }

    @Override
    public double param(Point3DC point) {
        return cache().get(Cache.PARAM + point.hashCode(),
                () ->  impl.closestParam(point.asArray()));
    }

    @Override
    public int optimalSplits() {
        return getData().knots.length - 1;
    }

    @Override
    public void normalizeParametrization() {
    }

    @Override
    public NurbsCurve invert() {
        if (impl instanceof verb.geom.Circle) {
            verb.geom.Circle impl = new verb.geom.Circle(
                    ((verb.geom.Circle) this.impl).center().copy(),
                    ((verb.geom.Circle) this.impl).xaxis().copy(),
//                    Vec.mul(-1, ((verb.geom.Circle) this.impl).xaxis()),
                    Vec.mul(-1, ((verb.geom.Circle) this.impl).yaxis()),
//                    ((verb.geom.Circle) this.impl).yaxis().copy(),
                    ((verb.geom.Circle) this.impl).radius());
            return new NurbsCurve(impl);
        }
        NurbsCurveData inverted = Modify.curveReverse(
                getData().degree,
                getData().knots,
                getData().controlPoints);
        inverted = VerbNurbsHelpers.normalizeCurveParametrizationIfNeeded(inverted);
        return new NurbsCurve(newVerbCurve(inverted), getType());
    }

    public static verb.geom.NurbsCurve newVerbCurve(NurbsCurveData inverted) {
        return verb.geom.NurbsCurve.nurbsCurveNoCheck(inverted);
    }

    @Override
    public NurbsCurve transform(Matrix3 tr) {
        if (null == tr) {

            return new NurbsCurve(impl.clone(), getType());
        }
        // TODO: if shearing is found type may have to be changed to NURBS.
        return new NurbsCurve(impl.transform(tr.toVerbArray()), getType());
    }

    /**
     *
     * @param u
     * @return
     */
    @Override
    public List<ParametricCurve> split(double u) {
        Array<verb.geom.NurbsCurve> split = impl.split(u);
        split.stream().forEach(c -> VerbNurbsHelpers.normalizeCurveParametrization(c._data));

        return split.map(c -> (ParametricCurve) new NurbsCurve(c, getSplitType(split))).asList();
    }

    private TYPE getSplitType(Array<verb.geom.NurbsCurve> split) {
        TYPE splitType = getType();
        if (splitType.equals(TYPE.CIRCLE) && split.length > 1) {
            splitType = TYPE.ARC;
        }
        return splitType;
    }

    @Override
    public Array<Array<Number>> eval(double u, int num) {
        return Eval.rationalCurveDerivatives(getData(), u, num);
    }

    @Override
    public List<Point3DC> tessellate(Double uMin, Double uMax, Double tessTol) {

        Integer tessKey = Cache.TESS_LOOP
                + Double.hashCode(uMin)
                + Double.hashCode(uMax) * 66
                + Double.hashCode(null == tessTol ? 1. : tessTol);

        List<Point3DC> tess = cache().get(
                tessKey,
                () -> CurveTessellate.degree1OptTessellator(this, uMin, uMax, tessTol));

        if (tess.size() == 1) {

            cache().clear(tessKey);
            tess = cache().get(
                    tessKey,
                    () -> CurveTessellate.degree1OptTessellator(this, uMin, uMax, tessTol));
        }

        return new ArrayList(tess);
    }

    @Override
    public List<Point3DC> tessellate(Number tessTol) {
        double tessTold = tessTol == null ? Tolerance.NUMERICAL_SOLVE_TOL : tessTol.doubleValue();

        Integer tessKey = Cache.TESS_LOOP
                + Double.hashCode(ArrayExtensions.first(getData().knots).doubleValue())
                + Double.hashCode(ArrayExtensions.last(getData().knots).doubleValue()) * 24
                + Double.hashCode(tessTold);
        List<Point3DC> tess = cache().get(
                tessKey,
                () -> CurveTessellate.degree1OptTessellator(this,
                        getVerb().domain().min,
                        getVerb().domain().max,
                        tessTold));

        if (tess.size() == 1) {

            cache().clear(tessKey);
            tess = cache().get(
                    tessKey,
                    () -> CurveTessellate.degree1OptTessellator(this,
                            getVerb().domain().min,
                            getVerb().domain().max,
                            tessTold));
        }

        return new ArrayList<>(tess);
    }

    @Override
    public List<CurveIntersectData> intersectCurve(ParametricCurve other, Number tol) {

        if (!(other instanceof NurbsCurve)) {
            throw new UnsupportedOperationException("Current do not support intersect of nurbs and " + other.getClass().getSimpleName());
        }
        final double told = null == tol ? Tolerance.TOLERANCE : tol.doubleValue();

        if (!bounds().intersects(other.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
            return new ArrayList<>();
        }
        final List<CurveIntersectData> isecs = new ArrayList<>();

        isecOn(this, other, domain().min.doubleValue(), other, isecs, Tolerance.TOLERANCE_BOUNDS_FAIL);
        isecOn(this, other, domain().max.doubleValue(), other, isecs, Tolerance.TOLERANCE_BOUNDS_FAIL);
        isecOn(other, this, other.domain().min.doubleValue(), other, isecs, Tolerance.TOLERANCE_BOUNDS_FAIL);
        isecOn(other, this, other.domain().max.doubleValue(), other, isecs, Tolerance.TOLERANCE_BOUNDS_FAIL);

        Intersect.curves(getData(), ((NurbsCurve) other).getData(), Tolerance.NUMERICAL_SOLVE_TOL)
                .stream()
                .forEach(ic -> {
                    add(new CurveIntersectData(ic.u0, ic.u1, ic.point0, ic.point1), isecs, other, Tolerance.TOLERANCE_BOUNDS_FAIL);
                });

        return isecs;
    }

    private void add(CurveIntersectData i0, List<CurveIntersectData> isecs, ParametricCurve other, double told) {

        double toldd = told * told;
        for (CurveIntersectData i1 : isecs) {
            if (Vec.distSquared(i0.p0, i1.p0) < toldd) {
                return;
            }
        }
        Point3DC t0 = tangentAtParam(i0.u0.doubleValue());
        Point3DC t1 = other.tangentAtParam(i0.u1.doubleValue());

        if (t0.minus(t1).magnitudeSq() > toldd
                && t0.negated().minus(t1).magnitudeSq() > toldd) {

            isecs.add(i0);
        }

    }

    public void isecOn(ParametricCurve c0, ParametricCurve c1, double u0, ParametricCurve other, List<CurveIntersectData> isecs, double told) {
        Point3DC p0 = c0.point(u0);
        double u1 = c1.param(p0);

        if (!c1.isInside(u1)) {
            return;
        }
        Point3DC p1 = c1.point(u1);
        if (p0.minus(p1).magnitudeSq() < told * told) {

            if (c0 == other) {

                add(new CurveIntersectData(u1, u0, p1.asArray(), p0.asArray()), isecs, other, told);
            } else {

                add(new CurveIntersectData(u0, u1, p0.asArray(), p1.asArray()), isecs, other, told);
            }
        }
    }

    @Override
    public Set<Point3DC> pointsOfSurfaceIntersection(Surface surface) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void translated(Point3DC vector) {
        impl = impl.transform(Matrix3.createTranslation(vector.x(), vector.y(), vector.z()).toVerbArray());
        cache().clear();
    }

    @Override
    public Curve getTranslated(Point3DC vector) {
        return new NurbsCurve(impl.transform(Matrix3.createTranslation(vector.x(), vector.y(), vector.z()).toVerbArray()), getType());
    }

    @Override
    public List<ParametricCurve> split(Point3DC point) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BoundingBox bounds() {
        return cache().get(Cache.BOUNDING_BOX,
                () -> {
                    BoundingBox bounds = new BoundingBox(EmptyObject.EMPTY);
                    List<Point3DC> tessPoints1 = tessellate();

                    for (int i = 0; i < tessPoints1.size(); i++) {
                        bounds.add(tessPoints1.get(i).asArray());
                    }

                    return bounds;
                });
    }

    @Override
    public NurbsCurve asNurbs() {
        return this;
    }

    @Override
    public boolean intersectsSurface(Surface surface) {
        return false;
    }

    @Override
    public String toString() {
        return "NurbsCurve{" + "verb=" + impl + '}';
    }

}
