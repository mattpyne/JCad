package org.pyne.jcad.core.entity;

/**
 *
 * @author Matthew Pyne
 */
public interface IComponent {
    
    /**
     * Clears the state and data for the next update. Invalidate is quick and 
     * should be called by the user if the suspect they have changed the component.
     */
    void invalidate();
    
    /**
     * Will setup the components state and data.
     */
    void update();
    
    /**
     * Links up this component with the given entity.
     * 
     * @param entity 
     */
    void register(Entity entity);

}
