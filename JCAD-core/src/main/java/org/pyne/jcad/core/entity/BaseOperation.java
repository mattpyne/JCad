package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.math.brep.Topo;

import java.util.List;

/**
 *
 * @author Matthew Pyne
 */
public class BaseOperation implements IOperation {

    private Topo topo;
    
    public BaseOperation(Topo topo) {
        this.topo = topo;
    }

    @Override
    public void onApply(Entity entity) {
        entity.getComponent(Geometry.class).ifPresent(geom -> geom.setTopo(topo));
    }

    public Topo getTopo() {
        return topo;
    }

    public void setTopo(Topo topo) {
        this.topo = topo;
    }

    @Override
    public Topo topoAt() {
        return topo;
    }

    @Override
    public Topo topoOp() {
        return topo;
    }

    @Override
    public List<String> getErrorMessages() {
        return List.of();
    }

    @Override
    public void register(ExpressionSolver expressionSolver) {

    }

}
