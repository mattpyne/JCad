package org.pyne.jcad.core.math.geom.eval;

import geomss.geom.nurbs.BasicNurbsSurface;
import geomss.geom.nurbs.ControlPointNet;
import haxe.root.Array;
import org.jscience.mathematics.vector.Float64Vector;
import org.pyne.jcad.core.math.geom.NurbsSurface;

public class NurbsSurfaceFactory {

    public static NurbsSurface from(
            int degreeU, int degreeV,
            Number[] knotsU, Number[] knotsV,
            Number[][][] controlPoints,
            Number[][] weights) {

        return new NurbsSurface(
                verb.geom.NurbsSurface.byKnotsControlPointsWeights(
                        degreeU, degreeV,
                        Array.from(knotsU), Array.from(knotsV),
                        Array.from(controlPoints), Array.from(weights)));
    }

    public static NurbsSurface from(BasicNurbsSurface surface) {
        return from(
                surface.getTDegree(), surface.getSDegree(),
                float64VectorToDoubleArray(surface.getTKnotVector().getAll()), float64VectorToDoubleArray(surface.getSKnotVector().getAll()),
                controlPointsToDoubleArray(surface.getControlPoints()), weightsToDoubleArray(surface.getControlPoints()));
    }

    private static Number[][][] controlPointsToDoubleArray(ControlPointNet cps) {
        Number[][][] a = new Number[cps.getNumberOfColumns()][cps.getNumberOfRows()][cps.get(0, 0).getPhyDimension()];

        for (int i = 0; i < cps.getNumberOfColumns(); i++) {
            for (int j = 0; j < cps.getNumberOfRows(); j++) {
                for (int k = 0; k < cps.get(j, i).getPhyDimension(); k++) {
                    a[i][j][k] = cps.get(j, i).getValue(k);
                }
            }
        }
        return a;
    }

    private static Number[][] weightsToDoubleArray(ControlPointNet cps) {
        Number[][] a = new Number[cps.getNumberOfColumns()][cps.getNumberOfRows()];

        for (int i = 0; i < cps.getNumberOfColumns(); i++) {
            for (int j = 0; j < cps.getNumberOfRows(); j++) {
                a[i][j] = cps.get(j, i).getWeight();
            }
        }
        return a;
    }

    private static Number[] float64VectorToDoubleArray(Float64Vector fv) {
        Number[] a = new Number[fv.getDimension()];

        for (int i = 0; i < fv.getDimension(); i++) {

            a[i] = fv.get(i).doubleValue();
        }
        return a;
    }

}
