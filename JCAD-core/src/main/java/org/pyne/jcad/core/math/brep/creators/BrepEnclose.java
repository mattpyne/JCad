package org.pyne.jcad.core.math.brep.creators;

import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.PlanarSurface;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.PathMath;
import org.pyne.jcad.core.math.geom.SurfacePrimitives;

/**
 * @author Matthew Pyne
 */
public class BrepEnclose {

    public static Shell createPrism(List<Point3DC> basePoints, double height) {
        if (!PathMath.isCCW(basePoints)) {
            Collections.reverse(basePoints);
        }
        Point3DC normal = PathMath.normalOfCCWSeg(basePoints).negated();
        PlanarSurface baseSurface = new PlanarSurface(normal, normal.dot(basePoints.get(0)));
        Point3DC extrudeVector = baseSurface.normal(null).times(-height);
        PlanarSurface lidSurface = baseSurface.translate(extrudeVector).invert();
        List<Point3DC> lidPoints = basePoints.stream()
                .map(point -> point.plus(extrudeVector))
                .collect(Collectors.toList());

        List<BrepCurve> basePath = new ArrayList<>();
        List<BrepCurve> lidPath = new ArrayList<>();

        for (int i = 0; i < basePoints.size(); i++) {
            int j = (i + 1) % basePoints.size();
            basePath.add(new BrepCurve(CurvePrimitives.line(basePoints.get(i), basePoints.get(j))));
            lidPath.add(new BrepCurve(CurvePrimitives.line(lidPoints.get(i), lidPoints.get(j))));
        }

        return enclose(basePath, lidPath, lidSurface, baseSurface);
    }

    public static Shell enclose(BrepCurve basePath, BrepCurve lidPath) {
        return enclose(Arrays.asList(basePath), Arrays.asList(lidPath), null, null);
    }

    public static Shell enclose(List<BrepCurve> basePath, List<BrepCurve> lidPath) {
        return enclose(basePath, lidPath, null, null);
    }

    public static Shell enclose(List<BrepCurve> basePath, List<BrepCurve> lidPath, PlanarSurface basePlane, PlanarSurface lidPlane) {

        if (basePath.size() != lidPath.size()) {
            throw new IllegalArgumentException("Must have the same number of lid paths and base paths");
        }

        if (basePath.size() == 1) {
            basePath = basePath.get(0).split(0.5);
            lidPath = lidPath.get(0).split(0.5);
        }

        List<BrepSurface> walls = new ArrayList<>();

        for (int i = 0; i < basePath.size(); i++) {
            walls.add(createWall(basePath.get(i), lidPath.get(i)));
        }

        return assemble(walls, basePlane, lidPlane);
    }

    private static Shell assemble(List<BrepSurface> walls, PlanarSurface basePlane, PlanarSurface lidPlane) {
        Shell shell = new Shell();

        List<Edge> wallEdges = walls.stream()
                .map(wall -> Edge.fromCurve(new BrepCurve(wall.isoCurveAlignV(0))))
                .collect(Collectors.toList());
        List<Edge> baseEdges = new ArrayList<>();
        List<Edge> lidEdges = new ArrayList<>();

        for (int i = 0; i < wallEdges.size(); ++i) {
            int j = (i + 1) % wallEdges.size();
            Edge curr = wallEdges.get(i);
            Edge next = wallEdges.get(j);
            BrepSurface wall = walls.get(i);

            Edge baseEdge = new Edge(
                    new BrepCurve(wall.isoCurveAlignU(1.)),
                    curr.halfEdge1().vertexB(),
                    next.halfEdge1().vertexB());

            Edge lidEdge = new Edge(
                    new BrepCurve(wall.isoCurveAlignU(0)),
                    curr.halfEdge1().vertexA(),
                    next.halfEdge1().vertexA());

            Face wallFace = new Face(wall);
            List<HalfEdge> faceEdges = wallFace.getOuterLoop().halfEdges();
            faceEdges.add(baseEdge.halfEdge2());
            faceEdges.add(curr.halfEdge2());
            faceEdges.add(lidEdge.halfEdge1());
            faceEdges.add(next.halfEdge1());
            wallFace.getOuterLoop().link();

            baseEdges.add(baseEdge);
            lidEdges.add(lidEdge);
            shell.faces().add(wallFace);

            Debug.mismatchBounds(wallFace);
        }

        Face base = new Face();
        Face lid = new Face();

        Collections.reverse(lidEdges);

        baseEdges.forEach(e -> base.getOuterLoop().halfEdges().add(e.halfEdge1()));
        lidEdges.forEach(e -> lid.getOuterLoop().halfEdges().add(e.halfEdge2()));

        base.getOuterLoop().link();
        lid.getOuterLoop().link();

        base.setSurface(BrepBuilder.createBoundingNurbs(base.getOuterLoop().tessellate(), basePlane));
        lid.setSurface(BrepBuilder.createBoundingNurbs(lid.getOuterLoop().tessellate(), lidPlane));

        Debug.mismatchBounds(base);
        Debug.mismatchBounds(lid);

        shell.addFace(base);
        shell.addFace(lid);
        shell.faces().forEach(f -> f.setShell(shell));
        return shell;
    }

    private static BrepSurface createWall(BrepCurve curve1, BrepCurve curve2) {
        return loft(curve1, curve2);
    }

    public static BrepSurface loft(BrepCurve curve1, BrepCurve curve2) {
        return new BrepSurface(
                SurfacePrimitives.loft(
                        curve1.getImpl(),
                        curve2.getImpl()));
    }

    public static BrepSurface linearExtrusion(BrepCurve sweptCurve, Point3DC extrusionVector) {
        return new BrepSurface(
                SurfacePrimitives.linearExtrusion(
                        sweptCurve.getImpl(),
                        extrusionVector));
    }

    public static Face loft(Edge edge1, Edge edge2) {
        BrepCurve curve1 = edge1.curve();
        BrepCurve curve2 = edge2.curve();

        BrepSurface wall = loft(curve1, curve2);

        Edge baseEdge = new Edge(
                new BrepCurve(wall.isoCurveAlignU(1.)),
                edge1.halfEdge1().vertexB(),
                edge2.halfEdge1().vertexB());

        Edge lidEdge = new Edge(
                new BrepCurve(wall.isoCurveAlignU(0)),
                edge1.halfEdge1().vertexA(),
                edge2.halfEdge1().vertexA());

        Face wallFace = new Face(wall);
        List<HalfEdge> faceEdges = wallFace.getOuterLoop().halfEdges();
        faceEdges.add(baseEdge.halfEdge2());
        faceEdges.add(edge1.halfEdge2());
        faceEdges.add(lidEdge.halfEdge1());
        faceEdges.add(edge2.halfEdge1());
        wallFace.getOuterLoop().link();

        return wallFace;
    }
    
    public static Shell extrude(Face baseFace, Point3DC dir) {

        baseFace = baseFace.copy();

        Point3DC normal = baseFace.surface().normal(Point3DC.ZERO);
        double angle = normal.dot(dir);

        if (angle < 0) {
            List<BrepCurve> baseCurves = baseFace.getOuterLoop()
                    .streamEdges()
                    .map(Edge::curve)
                    .collect(Collectors.toList());

            List<BrepCurve> lidCurves = baseCurves.stream()
                    .map(bC -> bC.translate(dir))
                    .collect(Collectors.toList());

            return enclose(lidCurves, baseCurves);
        }

        List<BrepCurve> baseCurves = baseFace.getOuterLoop()
                .streamEdges()
                .map(Edge::curve)
                .collect(Collectors.toList());

        List<BrepCurve> lidCurves = baseCurves.stream()
                .map(bC -> bC.translate(dir))
                .collect(Collectors.toList());

        return enclose(baseCurves, lidCurves);
    }
}
