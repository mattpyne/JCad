package org.pyne.jcad.core.math.brep.creators;

import java.util.Arrays;
import java.util.List;

import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.Shell;

/**
 *
 * @author Matthew Pyne
 */
public class ShellPrimitives {

    private static final Matrix3 IDENTITY = new Matrix3();

    public static Shell box(double width, double height, double depth, Matrix3 transform) {
        double wh = width * 0.5;
        double hh = height * 0.5;
        double dh = depth * 0.5;

        transform = null != transform ? transform : IDENTITY;

        List<Point3DC> basePoints = Arrays.asList(transform.apply(new Point3DC(-wh, -hh, -dh)),
                transform.apply(new Point3DC(wh, -hh, -dh)),
                transform.apply(new Point3DC(wh, hh, -dh)),
                transform.apply(new Point3DC(-wh, hh, -dh)));

        return BrepEnclose.createPrism(basePoints, depth);
    }
    
    public static Shell box(double width, double height, double depth) {
        double wh = width * 0.5;
        double hh = height * 0.5;
        double dh = depth * 0.5;
        
        List<Point3DC> basePoints = Arrays.asList(new Point3DC(-wh, -hh, -dh),
                new Point3DC(wh, -hh, -dh),
                new Point3DC(wh, hh, -dh),
                new Point3DC(-wh, hh, -dh));

        return BrepEnclose.createPrism(basePoints, depth);
    }
    
    public static Shell cylinder(double radius, double height) {
        NurbsCurve circle1 = CurvePrimitives.circle(Point3DC.Z_ONE.times(height / 2), radius);
        NurbsCurve circle2 = CurvePrimitives.circle(Point3DC.Z_ONE.times(-height / 2), radius);
        
        return BrepEnclose.enclose(new BrepCurve(circle2), new BrepCurve(circle1));
    }

    public static Shell cylinder(double radius, double height, Matrix3 transform) {
        NurbsCurve circle1 = CurvePrimitives.circle(Point3DC.Z_ONE.times(height / 2), radius).transform(transform);
        NurbsCurve circle2 = CurvePrimitives.circle(Point3DC.Z_ONE.times(-height / 2), radius).transform(transform);

        return BrepEnclose.enclose(new BrepCurve(circle2), new BrepCurve(circle1));
    }

    public static Shell ellipticCylinder(double radiusX, double radiusY, double height) {
        NurbsCurve ellipse1 = CurvePrimitives.ellipse(Point3DC.Z_ONE.times(height / 2), radiusX, radiusY);
        NurbsCurve ellipse2 = CurvePrimitives.ellipse(Point3DC.Z_ONE.times(-height / 2), radiusX, radiusY);

        return BrepEnclose.enclose(new BrepCurve(ellipse2), new BrepCurve(ellipse1));
    }
    
    public static Shell ellipticCylinder(double radiusX, double radiusY, double height, Matrix3 transform) {
        NurbsCurve ellipse1 = CurvePrimitives.ellipse(Point3DC.Z_ONE.times(height / 2), radiusX, radiusY).transform(transform);
        NurbsCurve ellipse2 = CurvePrimitives.ellipse(Point3DC.Z_ONE.times(-height / 2), radiusX, radiusY).transform(transform);

        return BrepEnclose.enclose(new BrepCurve(ellipse2), new BrepCurve(ellipse1));
    }
}
