package org.pyne.jcad.core.csg;

import eu.mihosoft.vvecmath.Vector3d;
import java.util.List;
import java.util.stream.Collectors;

import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author Matthew
 */
public class JCSGInterop {
    
    public static List<Vector3d> vecFrom(List<Point3DC> points) {
        return points.stream().map(Vector3d.class::cast).collect(Collectors.toList());
    }
    
    public static List<Point3DC> pointFrom(List<Vector3d> points) {
        return points.stream().map(Point3DC::new).collect(Collectors.toList());
    }
}
