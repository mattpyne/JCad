package org.pyne.jcad.core.math.brep;

import eu.mihosoft.vvecmath.Vector3d;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;

/**
 *
 * @author matt pyne
 */
public class Vertex extends DataContainer implements Topo {

    private Point3DC pos;
    private HashSet<HalfEdge> edges;

    /**
     *
     */
    public Vertex() {
        this.edges = new HashSet<>();
    }

    public Vertex(double x, double y, double z) {
        this(new Point3DC(x, y, z));
    }

    /**
     *
     * @param point
     */
    public Vertex(Vector3d point) {
        this();
        this.pos = new Point3DC(point.x(), point.y(), point.z());
    }

    /**
     *
     * @param other
     */
    public Vertex(Vertex other) {
        this();
        this.pos = new Point3DC(other.point());
    }

    /**
     *
     * @return
     */
    @Override
    public Vertex copy() {
        return new Vertex(this);
    }

    @Override
    public Vertex clone() {
        Vertex v = new Vertex();
        v.pos = new Point3DC(this.point());
        v.setId(this.getId());
        return v;
    }

    /**
     *
     * @return
     */
    public Point3DC point() {
        return pos;
    }

    /**
     *
     * @param point
     */
    public void setPos(Point3DC point) {
        this.pos = point;
    }

    /**
     *
     * @param edge
     */
    public void addEdge(HalfEdge edge) {
        edges.add(edge);
    }

    /**
     *
     * @param edge
     */
    public void removeEdge(HalfEdge edge) {
        edges.remove(edge);
    }

    /**
     *
     * @param other
     * @return
     */
    public Stream<HalfEdge> edgeFor(Vertex other) {
        boolean samePositionClosedEdge = (CadMath.eq(point(), other.point(), CadMath.TOLERANCE_SQ));

        return edges.stream()
                .filter(edge -> {

                    if (samePositionClosedEdge) {
                        return edge.getPos2().equals(other.point())
                                    && edge.getPos1().equals(other.point());
                    } else {
                        if (edge.getPos1().equals(this.point())) {
                            return edge.getPos2().equals(other.point());
                        } else {
                            return edge.getPos1().equals(other.point());
                        }
                    }

                });
    }

    @Override
    public String toString() {
        return "Vertex{" + "pos=" + pos + '}';
    }

    @Override
    public void transform(Matrix3 tr) {
        cache().clear();
        pos = tr.apply(pos);
    }

    @Override
    public Vertex transformed(Matrix3 tr) {
        Vertex instance = copy();

        instance.transform(tr);

        return instance;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.pos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertex other = (Vertex) obj;
        if (!Objects.equals(this.pos, other.pos)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmpty() {
        return null == pos;
    }

}
