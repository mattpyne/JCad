package org.pyne.jcad.core.entity;

import java.util.ArrayList;
import java.util.List;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.geom.Curve;

/**
 *
 * @author Matthew Pyne
 */
public class Sketch {

    private final BrepSurface surface;
    private final List<Curve> curves;

    public Sketch(BrepSurface surf) {
        curves = new ArrayList<>();
        this.surface = surf;
    }

    public List<Curve> curves() {
        return curves;
    }

    public void addCurve(Curve curve) {
        if (!curves.contains(curve)) {

            curves.add(curve);
        }
    }

    public void removeEntity(Curve curve) {
        curves.remove(curve);
    }

    public void clear() {
        curves.clear();
    }

    public BrepSurface surface() {
        return surface;
    }

}
