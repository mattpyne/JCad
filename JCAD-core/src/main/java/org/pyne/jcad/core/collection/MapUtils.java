package org.pyne.jcad.core.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapUtils {

    public static <T, K> void addToList(Map<T, List<K>> map, T key, K value) {
        List<K> list = map.computeIfAbsent(key, k -> new ArrayList<>());
        list.add(value);
    }

}
