package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.brep.Face;

public interface OperationObserver {

    void beforeDetectedLoops(Face face);

}
