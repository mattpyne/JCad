package org.pyne.jcad.core.math;

import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.vvecmath.Vector3d;

/**
 *
 * @author Matthew Pyne
 */
public class BBox {

    private Double minX = Double.MAX_VALUE;
    private Double minY = Double.MAX_VALUE;
    private Double minZ = Double.MAX_VALUE;
    private Double maxX = -Double.MAX_VALUE;
    private Double maxY = -Double.MAX_VALUE;
    private Double maxZ = -Double.MAX_VALUE;

    public BBox copy() {
        BBox copy = new BBox();
        
        copy.minX = minX.doubleValue();
        copy.minY = minY.doubleValue();
        copy.minZ = minZ.doubleValue();
        copy.maxX = maxX.doubleValue();
        copy.maxY = maxY.doubleValue();
        copy.maxZ = maxZ.doubleValue();
        
        return copy;
    }
    
    public void checkBounds(Double x, Double y, Double z) {
        z = null == z ? 0. : z;
        minX = Math.min(minX, x);
        minY = Math.min(minY, y);
        minZ = Math.min(minZ, z);
        maxX = Math.max(maxX, x);
        maxY = Math.max(maxY, y);
        maxZ = Math.max(maxZ, z);
    }

    public void checkPoint(Point3DC point) {
        checkBounds(point.getX(), point.getY(), point.getZ());
    }

    public Point3DC center() {
        return new Point3DC(minX + (maxX - minX) / 2,
                minY + (maxY - minY) / 2,
                minZ + (maxZ - minZ) / 2);
    }

    public Point3DC min() {
        return new Point3DC(minX, minY, minZ);
    }

    public Point3DC max() {
        return new Point3DC(maxX, maxY, maxZ);
    }

    public double width() {
        return maxX - minX;
    }

    public double height() {
        return maxY - minY;
    }

    public double depth() {
        return maxZ - minZ;
    }

    public void expand(double delta) {
        minX -= delta;
        minY -= delta;
        minZ -= delta;
        maxX += delta;
        maxY += delta;
        maxZ += delta;
    }

    public Polygon toPolygon() {
        return Polygon.fromPoints(
                Vector3d.xy(minX, minY),
                Vector3d.xy(maxX, minY),
                Vector3d.xy(maxX, maxY),
                Vector3d.xy(minX, maxY)
        );
    }
}
