package org.pyne.jcad.core.math.geom.eval;

import haxe.root.Array;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.pyne.jcad.core.verbinterop.VerbNurbsHelpers;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import verb.core.Interval;
import verb.core.NurbsCurveData;
import verb.core.Vec;
import verb.eval.Eval;

/**
 *
 * @author Matthew Pyne
 */
public class CurveTessellate {

    /**
     *
     * @param curve
     * @return
     */
    public static Array<Number> curveTessellate(NurbsCurveData curve) {
        return curveTessellate(curve, null, null, null, null);
    }

    /**
     *
     * @param curve
     * @param min
     * @param max
     * @param tessTol
     * @param scale
     * @return
     */
    public static Array<Number> curveTessellate(
            NurbsCurveData curve,
            Number min,
            Number max,
            Number tessTol,
            Number scale) {

        if (curve.degree == 1) {
            return VerbNurbsHelpers.distinctKnots(curve);
        }

        Interval<Number> domain = VerbNurbsHelpers.curveDomain(curve);
        double dMax = domain.max.doubleValue();
        double dMin = domain.min.doubleValue();

        double maxd = null != max ? max.doubleValue() : dMax;
        double mind = null != min ? min.doubleValue() : dMin;

        Array<Number> out = new Array();
        double nSplits = curve.knots.length = 1;

        double splitStep = (dMax - dMin) / nSplits;
        nSplits = Math.round((maxd - mind) / splitStep);
        splitStep = (maxd - mind) / nSplits;

        Array<Number> splits = new Array();
        splits.push(min);
        for (int i = 1; i < nSplits; ++i) {
            splits.push(i * splitStep);
        }
        splits.push(max);

        for (int i = 1; i < splits.length; ++i) {
            Number u1 = splits.get(i - 1);
            out.push(u1);
            refine(u1,
                    splits.get(i),
                    curveStep(curve, u1, tessTol, scale),
                    out,
                    curve,
                    tessTol,
                    scale);
        }

        out.push(max);
        return out;
    }
    
    /**
     *
     * @param curve
     * @return
     */
    public static Array<Array<Number>> curveTessellatePoints(NurbsCurveData curve) {
        return curveTessellate(curve).map(pt -> VerbNurbsHelpers.curvePoint(curve, pt.doubleValue()));
    }

    private static void refine(
            Number u1,
            Number u2,
            Number step,
            Array<Number> out,
            NurbsCurveData curve,
            Number tessTol,
            Number scale) {

        if (step.doubleValue() < u2.doubleValue() - u1.doubleValue()) {
            double mid = u1.doubleValue() + (u2.doubleValue() - u1.doubleValue()) * 0.5;
            refine(u1, mid, step, out, curve, tessTol, scale);
            out.push(mid);
            refine(mid, u2, curveStep(curve, mid, tessTol, scale), out, curve, tessTol, scale);
        }
    }
    
    /**
     *
     * @param curve
     * @param min
     * @param max
     * @param tessTol
     * @param scale
     * @return
     */
    public static List<Point3DC> curveTess(Curve curve, Number min, Number max, Number tessTol, Number scale) {
        return curveTessParams(curve, min, max, tessTol, scale).stream()
                .map(u -> curve.point(u.doubleValue()))
                .collect(Collectors.toList());
    }

    private static Array<Number> curveTessParams(Curve curve, Number min, Number max, Number tessTol, Number scale) {

        tessTol = null == tessTol ? 1. : tessTol;
        scale = null == scale ? 1. : scale;
        
        double dMin = curve.domain().min.doubleValue();
        double dMax = curve.domain().max.doubleValue();

        Array<Number> out = new Array();
        int nSplits = curve.optimalSplits();

        double splitStep = (dMax - dMin) / nSplits;
        nSplits = (int) Math.round((max.doubleValue() - min.doubleValue()) / splitStep);
        splitStep = (max.doubleValue() - min.doubleValue()) / nSplits;

        Array<Number> splits = new Array();
        splits.push(min);

        for (int i = 1; i < nSplits; ++i) {
            splits.push(min.doubleValue() + i * splitStep);
        }
        splits.push(max);

        for (int i = 1; i < splits.length; ++i) {
            Number u1 = splits.get(i - 1);
            out.push(u1);
            refine(u1,
                    splits.get(i),
                    curveStep(curve, u1, tessTol, scale),
                    out, curve, tessTol, scale);
        }
        out.push(max);
        return out;
    }

    private static Number curveStep(NurbsCurveData nurbsData, Number u, Number tessTol, Number scale) {
        Array<Array<Number>> ders = Eval.rationalCurveDerivatives(nurbsData, u.doubleValue(), 2);
        Array<Number> r1 = ders.get(0);
        Array<Number> r2 = ders.get(1);

        Number r11sq = Vec.dot(r1, r1);
        Number r11 = Math.sqrt(r11sq.doubleValue());

        Array<Number> r1CrossR2 = Vec.cross(r1, r2);
        double r = r11sq.doubleValue() * r11.doubleValue() / Math.sqrt(Vec.dot(r1CrossR2, r1CrossR2));
        double tol = tessTol.doubleValue() / scale.doubleValue();

        return 2 * Math.sqrt(tol * (2 * r - tol)) / r11.doubleValue();
    }

    private static Number curveStep(Curve curve, Number u, Number tessTol, Number scale) {
        Array<Array<Number>> ders = curve.eval(u.doubleValue(), 2);
        Array<Number> r1 = ders.get(0);
        Array<Number> r2 = ders.get(1);

        Number r11sq = Vec.dot(r1, r1);
        Number r11 = Math.sqrt(r11sq.doubleValue());

        Array<Number> r1CrossR2 = Vec.cross(r1, r2);
        double r = r11sq.doubleValue() * r11.doubleValue() / Math.sqrt(Vec.dot(r1CrossR2, r1CrossR2));
        double tol = tessTol.doubleValue() / scale.doubleValue();

        return 2. * Math.sqrt(tol * (2. * r - tol)) / r11.doubleValue();
    }

    private static void refine(
            Number u1,
            Number u2,
            Number step,
            Array<Number> out,
            Curve curve,
            Number tessTol,
            Number scale) {

        if (step.doubleValue() < u2.doubleValue() - u1.doubleValue()) {
            double mid = u1.doubleValue() + (u2.doubleValue() - u1.doubleValue()) * 0.5;
            refine(u1, mid, step, out, curve, tessTol, scale);
            out.push(mid);
            refine(mid, u2, curveStep(curve, mid, tessTol, scale), out, curve, tessTol, scale);
        }
    }
    
    /**
     *
     * @param curve
     * @param min
     * @param max
     * @param tol
     * @return
     */
    public static List<Point3DC> degree1OptTessellator(NurbsCurve curve, Number min, Number max, Number tol) {
        if (curve.degree() == 1) {

            return curve.degree1Tess()
                    .stream()
                    .filter(Objects::nonNull)
                    .map(u -> curve.point(u.doubleValue()))
                    .collect(Collectors.toList());
        }

        return curve.asNurbs().getVerb().tessellate(tol)
                .stream()
                .map(Point3DC::new)
                .collect(Collectors.toList());
    }

}
