package org.pyne.jcad.core.math.geom;

import eu.mihosoft.vvecmath.Vector3d;
import haxe.root.Array;
import org.pyne.jcad.core.math.Point3DC;
import verb.core.Interval;

import java.util.ArrayList;
import java.util.List;

public class MeshPoint extends Point3DC {

    private MeshSurface surf;
    private List<MeshPoint> sharedPoints;
    private Point3DC p3OnProjectionPlane;
    private Double u;
    private Double v;
    private Array<Number> uv;
    private Point3DC uvPt;

    public MeshPoint(Point3DC p3OnOriginalSurface, MeshSurface surf) {
        super(p3OnOriginalSurface);
        this.surf = surf;
    }

    public MeshPoint(double x, double y, double z, MeshSurface surf) {
        super(x, y, z);
        this.surf = surf;
    }

    public void addSharedPoint(MeshPoint pt) {
        if (null == sharedPoints) {
            sharedPoints = new ArrayList<>();
        }

        sharedPoints.add(pt);
    }

    public void setSharedPoint(List<MeshPoint> sharedPoints) {
        this.sharedPoints = sharedPoints;
    }

    public List<MeshPoint> sharedPoints() {
        return sharedPoints;
    }

    public Point3DC getP3OnMesh() {
        return this;
    }

    public double getU() {
        if (null == u) {

            calcUV();
        }

        return u;
    }

    public double getV() {
        if (null == v) {

            calcUV();
        }

        return v;
    }

    public double normalizedU() {

        double u = getU();

        Interval<Number> domain = surf.domainU();

        return (u - domain.min.doubleValue()) / domain.width().doubleValue();
    }

    public double normalizedV() {

        double v = getV();

        Interval<Number> domain = surf.domainV();

        return (v - domain.min.doubleValue()) / domain.width().doubleValue();
    }

    public Array<Number> uv() {
        if (null == uv) {
            uv = Array.from(getU(), getV());
        }

        return uv;
    }

    public Point3DC uvPt() {
        if (null == uvPt) {

            uvPt = Point3DC.xy(getU(), getV());
        }

        return uvPt;
    }

    public Point3DC getP3OnProjectionPlane() {
        if (null == p3OnProjectionPlane) {

            p3OnProjectionPlane = surf.getSimpleSurface().project(this);
        }

        return p3OnProjectionPlane;
    }

    private void calcUV() {
        Array<Number> uv = surf.getSimpleSurface().param(getP3OnProjectionPlane());
        u = uv.get(0).doubleValue();
        v = uv.get(1).doubleValue();
    }

}