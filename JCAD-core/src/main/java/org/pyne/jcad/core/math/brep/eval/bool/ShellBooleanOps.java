package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.brep.creators.BrepFaceEvolve;
import org.pyne.jcad.core.math.brep.creators.VertexFactory;
import org.pyne.jcad.core.math.brep.eval.*;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.math.geom.Tolerance;
import org.pyne.jcad.core.math.geom.eval.CurveIntersectData;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.BrepCurve;
import haxe.root.Array;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.brep.Face;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.exe.ConsumerBatcher;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;

import static org.pyne.jcad.core.math.VecMath.leftTurningMeasure;
import org.pyne.jcad.core.math.brep.CompoundShell;
import org.pyne.jcad.core.math.brep.Edge;

/**
 *
 * @author matt pyne
 */
public class ShellBooleanOps {

    public enum TYPE {
        UNION, INTERSECT, SUBTRACT
    };

    private VertexFactory vertexFactory;
    public static final String MY = "__BOOLEAN_ALGORITHM_DATA__";

    public Shell union(Shell shell1, Shell shell2) {
        return apply(shell1, shell2, TYPE.UNION, null);
    }

    public Shell union(Shell shell1, Shell shell2, OperationObserver observer) {
        return apply(shell1, shell2, TYPE.UNION, observer);
    }

    public Shell intersect(Shell shell1, Shell shell2) {
        return apply(shell1, shell2, TYPE.INTERSECT, null);
    }

    public Shell intersect(Shell shell1, Shell shell2, OperationObserver observer) {
        return apply(shell1, shell2, TYPE.INTERSECT, observer);
    }

    public Shell subtract(Shell shell1, Shell shell2) {
        return apply(shell1, shell2, TYPE.SUBTRACT, null);
    }
    public Shell subtract(Shell shell1, Shell shell2, OperationObserver observer) {
        return apply(shell1, shell2, TYPE.SUBTRACT, observer);
    }

    public Shell subtract(Shell shell1, CompoundShell compoundShell) {
        Shell shell2 = new Shell();

        for (Shell shell : compoundShell.shells()) {

            shell2.addFaces(shell.faces());
        }

        return subtract(shell1, shell2);
    }

    public void invert(Shell shell) {
        for (Edge edge : shell.edges()) {
            edge.invert();
        }

        for (Face face : shell.faces()) {
            face.setSurface(face.surface().invert());

            for (Loop loop : face.loops()) {
                for (int i = 0; i < loop.halfEdges().size(); i++) {
                    loop.halfEdges().set(i, loop.halfEdges().get(i).twin());
                }
                loop.reverse();
                loop.link();
            }
        }
        shell.putData("inverted", !shell.data("inverted", Boolean.class).orElse(Boolean.FALSE));
    }

    public Shell apply(Shell shellA, Shell shellB, TYPE type, OperationObserver operationObserver) {

        Debug.soutBoolOperationStarted();

        shellA = prepareWorkingCopy(shellA);
        shellB = prepareWorkingCopy(shellB);

//        preCalc(shellA, shellB);
        // Do intersection section check
        if (type == TYPE.SUBTRACT) {
            invert(shellB);
            type = TYPE.INTERSECT;
        }

        List<Face> workingFaces = collectFaces(shellA, shellB);

        initOperationData(workingFaces);
        mergeVertices(shellA, shellB);
        initVertexFactory(shellA, shellB);

        intersectEdges(shellA, shellB);

        List<MergeFacesInfo> mergedFaces = mergeOverlappingFaces(shellA, shellB, type);

        intersectFaces(shellA, shellB, mergedFaces, type);

        workingFaces = replaceMergedFaces(workingFaces, mergedFaces);
        for (Face workFace : workingFaces) {
            workFace.op.initGraph();
        }

        Debug.checkFaceDataForError(workingFaces);

        for (Face workFace : workingFaces) {

            if (null != operationObserver) {
                operationObserver.beforeDetectedLoops(workFace);
            }

            workFace.op.detectedLoops = detectLoops(workFace.surface(), workFace.op);

            Debug.checkEdgesValid(workFace.op.detectedLoops);
            Debug.soutDetectLoopCount(workFace);
        }

        for (Face workFace : workingFaces) {
            workFace.op.detectedLoops.forEach(Loop::link);
        }

        //BrepReconstructer.reassemble(workingFaces.stream().flatMap(f -> f.op.detectedLoops.stream()));
        removeInvalidLoops(workingFaces);

        Debug.checkDetectedLoopsValid(workingFaces.stream());

        List<Face> faces = new ArrayList();
        for (Face face : workingFaces) {
            loopsToFaces(face, face.op.detectedLoops, faces);
        }

        Debug.checkLoopsValid(faces.stream());
        faces = filterFace(faces);

        Debug.checkLoopsValid(faces.stream());

        Shell result = new Shell();
        faces.forEach(face -> {
            face.setShell(result);
            result.faces().add(face);
        });

        cleanUpOperationData(result);

        Debug.checkShellValid(result);

        return result;
    }

    private void preCalc(Shell shellA, Shell shellB) {

        ConsumerBatcher.executeSync(shellA.faces(), facesBatch -> {
            for (Face faceA : facesBatch) {
                for (Face faceB : shellB.faces()) {
                    if (faceA.bounds().intersects(faceB.bounds(), Tolerance.TOLERANCE)) {

                        faceA.surface().intersectSurface(faceB.surface());
                        for (Loop loopA : faceA.loops()) {
                            for (Loop loopB : faceB.loops()) {
                                if (loopA.bounds().intersects(loopB.bounds(), Tolerance.TOLERANCE)) {
                                    for (HalfEdge heA : loopA.halfEdges()) {
                                        for (HalfEdge heB : loopB.halfEdges()) {
                                            if (heA.bounds().intersects(heB.bounds(), Tolerance.TOLERANCE)) {
                                                heA.edge().curve().intersectCurve(heB.edge().curve());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    List<Face> filterFace(List<Face> faces) {
        Set<Face> result = new HashSet();
        for (Face face : faces) {
            traversFaces(face, f -> {

                if (result.contains(f) || doesFaceContainNewEdge(f)) {
                    result.add(face);
                    return true;
                }
                return false;
            });
        }

        return new ArrayList(result);
    }

    private void traversFaces(Face face, Predicate<Face> callback) {
        Stack<Face> stack = new Stack();
        stack.push(face);
        Set<Face> seen = new HashSet();

        while (!stack.isEmpty()) {
            face = stack.pop();
            if (seen.contains(face)) {
                continue;
            }
            seen.add(face);
            if (callback.test(face)) {
                return;
            }
            for (Loop loop : face.loops()) {
                for (HalfEdge halfEdge : loop.halfEdges()) {
                    Face twinFace = halfEdge.twin().loop().face();
                    if (null == twinFace) {
                        // this happened because there is no face created for a 
                        // valid and legit detected loop
                        return;
                    } else {
                        stack.push(twinFace);
                    }
                }
            }
        }
    }

    private boolean doesFaceContainNewEdge(Face face) {
        for (HalfEdge e : face.halfEdges()) {
            if (HalfEdgeSolveData.getPriority(e) > 0
                    || HalfEdgeSolveData.getPriority(e.twin()) > 0
                    || HalfEdgeSolveData.get(e).affected) {

                return true;
            }
        }
        return false;
    }

    void loopsToFaces(Face originFace, Collection<Loop> loops, List<Face> out) {
        BrepFaceEvolve.EvolveFace(originFace, loops).forEach(newFace -> out.add(newFace));
    }

    void removeInvalidLoops(List<Face> faces) {
        Set<Loop> detectedLoopsSet = getDetectedLoops(faces);

        //discarded by face merge routine || has reference to not reassembled loop 
        for (Face face : faces) {
            face.op.detectedLoops = getValidLoops(face, detectedLoopsSet);

            Debug.soutValidDetectedLoops(face);
        }
    }

    private Set<Loop> getDetectedLoops(List<Face> faces) {
        Set<Loop> detectedLoopsSet = new HashSet();
        if (null == faces) {
            return detectedLoopsSet;
        }
        for (Face face : faces) {
            if (null == face.op.detectedLoops) {
                continue;
            }
            for (Loop loop : face.op.detectedLoops) {
                detectedLoopsSet.add(loop);
            }
        }
        return detectedLoopsSet;
    }

    private static Set<Loop> getValidLoops(Face face, Set<Loop> detectedLoopsSet) {
        return face.op.detectedLoops.stream()
                .filter(loop -> BrepValidator.loopIsValid(loop, detectedLoopsSet))
                .collect(Collectors.toSet());
    }

    List<Face> replaceMergedFaces(List<Face> workingFaces, final List<MergeFacesInfo> mergedFaces) {
        if (mergedFaces.isEmpty()) {

            return workingFaces;
        }

        workingFaces = workingFaces.stream()
                .filter(face -> mergedFaces.stream().noneMatch(mergeFaceInfo -> mergeFaceInfo.originFaces.contains(face)))
                .collect(Collectors.toList());

        for (MergeFacesInfo mergeFacesInfo : mergedFaces) {
            for (FacePrototype facePrototype : mergeFacesInfo.facePrototypes) {
                Face fakeFace = new Face(facePrototype.surface);
                for (Loop loop : facePrototype.loops) {
                    List<HalfEdge> actualHalfEdges = new ArrayList();
                    loop.halfEdges().forEach(he -> HalfEdgeSolveData.addDecayed(he, actualHalfEdges));
                    loop.setHalfEdges(actualHalfEdges);
                    fakeFace.getInnerLoops().add(loop);
                    loop.setFace(fakeFace);
                    loop.link();
                }
                initOperationDataForFace(fakeFace);
                workingFaces.add(fakeFace);
                for (Face originFace : mergeFacesInfo.originFaces) {
                    originFace.op.newEdges
                            .forEach(e -> addNewEdge(fakeFace, e));
                }
            }
        }

        return workingFaces;
    }

    void intersectFaces(Shell shellA, Shell shellB, List<MergeFacesInfo> mergedFaces, TYPE operationType) {
        for (Face faceA : shellA.faces()) {

            if (isMergedFace(mergedFaces, faceA)) {
                continue;
            }
            Debug.soutRunningFaceIntersectionOn(faceA);

            for (Face faceB : shellB.faces()) {

                if (isMergedFace(mergedFaces, faceB)) {
                    continue;
                }
                if (faceB.bounds().intersects(faceA.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {

                    List<BrepCurve> curves = faceA.surface().intersectSurface(faceB.surface());

                    Debug.soutFoundIntersection(faceB);

                    for (BrepCurve curve : curves) {

                        if (hasCoincidentEdge(curve, faceA) || hasCoincidentEdge(curve, faceB)) {

                            Debug.soutCurveHasEdgeCoincident();
                            continue;
                        }

                        curve = fixCurveDirection(curve, faceA.surface(), faceB.surface(), operationType);
                        List<Node> nodes = new ArrayList();
                        collectNodesOfIntersectionOfFace(curve, faceA, nodes, 0);
                        collectNodesOfIntersectionOfFace(curve, faceB, nodes, 1);

                        if (!nodes.isEmpty()) {

                            List<Edge> newEdges = new ArrayList();
                            split(nodes, curve, newEdges, faceA, faceB);

                            newEdges.forEach(e -> {
                                Debug.soutAddingNewEdgeFromIntersection();
                                addNewEdge(faceA, e.halfEdge1());
                                addNewEdge(faceB, e.halfEdge2());
                            });
                        }
                    }
                }
                transferEdges(faceA, faceB, operationType);
                transferEdges(faceB, faceA, operationType);
            }

            Debug.soutHowManyNewEdgesAfterFaceIntersection(faceA);
        }
    }

    private boolean isMergedFace(List<MergeFacesInfo> mergedFaces, Face face) {
        boolean isMergedFace = false;
        for (MergeFacesInfo mergedFace : mergedFaces) {
            if (mergedFace.originFaces.contains(face)) {
                isMergedFace = true;
                break;
            }
        }
        return isMergedFace;
    }

    private void transferEdges(Face faceSource, Face faceDest, TYPE operationType) {
        for (Loop loop : faceSource.loops()) {
            for (HalfEdge edge : loop.halfEdges()) {
                if (EdgeSolveData.isEdgeTransferred(edge.edge())) {
                    continue;
                }
                if (edgeCollinearToFace(edge, faceDest)) {
                    HalfEdge validEdge = chooseValidEdge(edge, faceDest, operationType);
                    HalfEdge twin = validEdge.twin();
                    twin.loop()
                            .face().op
                            .markTransferredFrom(twin);
                    EdgeSolveData.markEdgeTransferred(twin.edge());
                    addNewEdge(faceDest, twin);
                }
            }
        }
    }

    private HalfEdge chooseValidEdge(HalfEdge edge, Face face, TYPE operationType) {
        return canEdgeBeTransferred(edge, face, operationType) ? edge : edge.twin();
    }

    private boolean edgeCollinearToFace(HalfEdge edge, Face face) {
        return edge.cache().get(Cache.EDGE_COLLINEAR_FACE + face.getId().hashCode(),
                () -> {
                    if (!edge.bounds()
                            .intersects(face.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {

                        return false;
                    }

                    List<Point3DC> tess = edge.tessellate();
                    for (int i = 0; i < tess.size(); ++i) {

                        Point3DC pt1 = tess.get(i);
                        Array<Number> params = face.surface().param(pt1);
                        double u = params.__a[0].doubleValue();
                        double v = params.__a[1].doubleValue();
                        Point3DC pt2 = face.surface().point(u, v);
                        if (!Tolerance.veq(pt1, pt2)) {

                            return false;
                        }
                    }
                    return PointOnFace.rayCast(face, edge.edge().curve().middlePoint()).inside;
                });
    }

    private boolean addNewEdge(Face face, final HalfEdge edge) {
        if (edge.vertexA() == edge.vertexB()) {

            return false;
        }

        if (face.op.newEdges.stream()
                .anyMatch(oEdge -> HalfEdge.isSameEdge(oEdge, edge))) {

            return false;
        }

        face.op.newEdges.add(edge);
        HalfEdgeSolveData.setPriority(edge, 100);
        return true;
    }

    private void split(List<Node> nodes, BrepCurve curve, List<Edge> result, Face faceA, Face faceB) {
        if (nodes.isEmpty()) {
            return;
        }

        nodes.sort((n1, n2) -> {
            if (n1.u == n2.u) {
                return 0;
            }
            return n1.u - n2.u > 0 ? 1 : -1;
        });
        Node initNode = nodes.get(0);

        Map<Edge, List<Node>> edgesToSplitsMap = new HashMap<>();

        boolean insideA = PointOnFace.rayCast(faceA, initNode.point).strictInside;
        boolean insideB = PointOnFace.rayCast(faceB, initNode.point).strictInside;

        Node inNode = null;

        for (Node node : nodes) {
            boolean wasInside = insideA && insideB;
            boolean hadLeft = false;
            if (node.entersA) {
                insideA = true;
                inNode = node;
            }
            if (node.leavesA) {
                insideA = false;
                hadLeft = true;
            }
            if (node.entersB) {
                insideB = true;
                inNode = node;
            }
            if (node.leavesB) {
                insideB = false;
                hadLeft = true;
            }

            if (inNode == null) {
                continue;
            }
            if (wasInside && hadLeft) {

                BrepCurve edgeCurve = curve;
                Vertex vertexA = inNode.vertex();
                Vertex vertexB = node.vertex();

                if (!Tolerance.ueq(inNode.u, curve.uMin())) {

                    if (edgeCurve.split(vertexA.point()).size() == 2) {

                        edgeCurve = edgeCurve.split(vertexA.point()).get(1);
                    }
                }

                if (!Tolerance.ueq(node.u, curve.uMax())) {

                    edgeCurve = edgeCurve.split(vertexB.point()).get(0);
                }

                Edge edge = new Edge(edgeCurve, vertexA, vertexB);
                result.add(edge);
                checkNodeForEdgeSplit(inNode, edgesToSplitsMap);
                checkNodeForEdgeSplit(node, edgesToSplitsMap);
            }
        }

        if (edgesToSplitsMap.isEmpty()) {

            Debug.soutFailedToSplitEdgesAtNodes(nodes);
        }

        List<Object[]> edgesToSplits = new ArrayList();
        for (Map.Entry<Edge, List<Node>> isecsEntry : edgesToSplitsMap.entrySet()) {
            edgesToSplits.add(new Object[]{isecsEntry.getKey(), isecsEntry.getValue()});
        }

        for (Object[] edgeNodes : edgesToSplits) {

            ((List<Node>) edgeNodes[1]).sort((n1, n2) -> n1.edgeSplitInfo.u - n2.edgeSplitInfo.u > 0 ? 1 : -1);
            for (Node node : (List<Node>) edgeNodes[1]) {
                SplitEdge split = splitEdgeByVertex((Edge) edgeNodes[0], node.vertex());
                if (null != split) {
                    edgeNodes[0] = split.e1;
                }
            }
        }

    }

    private void checkNodeForEdgeSplit(Node node, Map<Edge, List<Node>> edgesToSplitsMap) {
        if (node.edgeSplitInfo != null) {
            addToListInMap(edgesToSplitsMap, node.edgeSplitInfo.edge.edge(), node);
        }
    }

    private void collectNodesOfIntersectionOfFace(BrepCurve curve, Face face, List<Node> nodes, int operand) {
        for (Loop loop : face.loops()) {
            if (curve.bounds().intersects(loop.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
                collectNodesOfIntersection(curve, loop, nodes, operand);
            }
        }
    }

    private void collectNodesOfIntersection(BrepCurve curve, Loop loop, List<Node> nodes, int operand) {
        List<Enclose> encloses = loop.encloses();

        for (Enclose enclose : encloses) {
            if (curve.passesThrough(enclose.currVertex2.point())) {

                Enclose.ENCLOSE_CLASSIFICATION classification = Enclose.isCurveEntersEnclose(curve.getImpl(), enclose.curr, enclose.next);
                if (classification == Enclose.ENCLOSE_CLASSIFICATION.ENTERS
                        || classification == Enclose.ENCLOSE_CLASSIFICATION.LEAVES) {

                    Node node = nodeByPoint(nodes, enclose.currVertex2.point(), null, curve.getImpl(), enclose.currVertex2);
                    if (classification == Enclose.ENCLOSE_CLASSIFICATION.ENTERS) {

                        node.setEnters(operand, true);
                    } else {

                        node.setLeaves(operand, true);
                    }
                }
            }
        }

        for (HalfEdge edge : loop.halfEdges()) {
            if (edge.bounds().intersects(curve.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
                intersectCurveWithEdge(curve, edge, nodes, operand);
            }
        }
    }

    private void intersectCurveWithEdge(BrepCurve curve, HalfEdge edge, List<Node> nodes, int operand) {
        List<CurveIntersectData> points = edge.edge().curve().intersectCurve(curve);

        for (CurveIntersectData point : points) {

            double u0 = point.u0.doubleValue();
            double u1 = point.u1.doubleValue();
            Vertex existing = vertexFactory.find(point.p0);
//            if (null != existing) {
//
//                // vertex allready exists, means either we hit and end of edge and this case is handled by enclosure ananlysis
//                continue;
//            }

            Node node = nodeByPoint(nodes, new Point3DC(point.p0), u1, null, null);
            if (isCurveEntersEdgeAtPoint(curve, edge, node.point)) {

                node.setEnters(operand, true);
            } else {

                node.setLeaves(operand, true);
            }
            node.edgeSplitInfo = new EdgeSplitInfo(edge, u0);
        }
    }

    private boolean isCurveEntersEdgeAtPoint(BrepCurve curve, HalfEdge edge, Point3DC point) {
        Point3DC normal = edge.loop().face().surface().normal(point);
        Point3DC edgeTangent = edge.tangent(point);
        Point3DC curveTangent = curve.tangentAtPoint(point);

        return CadMath.isOnPositiveHalfPlaneFromVec(edgeTangent, curveTangent, normal);
    }

    private Node nodeByPoint(
            List<Node> nodes,
            Point3DC point,
            Number u,
            ParametricCurve curve,
            Vertex vertex) {

        Node node = nodes.stream()
                .filter(n -> n.point.equals(point))
                .findAny()
                .orElse(null);
        if (null == node) {
            if (null == u) {
                u = curve.param(point);
            }
            node = new Node(point, u.doubleValue(), vertex, vertexFactory);
            nodes.add(node);
        }
        return node;
    }

    private static BrepCurve fixCurveDirection(BrepCurve curve, BrepSurface surface1, BrepSurface surface2, TYPE type) {
        Point3DC point = curve.middlePoint();
        Point3DC tangent = curve.tangentAtPoint(point);
        Point3DC normal1 = surface1.normal(point);
        Point3DC normal2 = surface2.normal(point);

        Point3DC expectedDirection = normal1.crossed(normal2);

        if (type == TYPE.UNION) {
            expectedDirection = expectedDirection.negated();
        }

        boolean sameAsExpected = expectedDirection.dot(tangent) > 0;
        if (!sameAsExpected) {
            curve = curve.invert();
        }
        return curve;
    }

    private static boolean hasCoincidentEdge(BrepCurve curve, Face face) {
        for (HalfEdge edge : face.halfEdges()) {
            if (curveAndEdgeCoincident(curve, edge)) {
                return true;
            }
        }
        return false;
    }

    List<MergeFacesInfo> mergeOverlappingFaces(Shell shellA, Shell shellB, TYPE type) {
        List<OverLappingFaces> groups = OverLappingFaces.findOverlappingFaces(shellA, shellB);
        List<MergeFacesInfo> mergedFaces = new ArrayList();

        for (OverLappingFaces group : groups) {
            MergeFacesInfo faceMergeInfo = MergeFacesInfo.mergeFaces(new ArrayList(group.groupA), new ArrayList(group.groupB), type);
            mergedFaces.add(faceMergeInfo);
        }

        return mergedFaces;
    }

    Set<Loop> detectLoops(BrepSurface surface, EdgeGraph graph) {
        graph.graphEdges.sort(Comparator.comparingInt(HalfEdgeSolveData::getPriority));

        Set<Loop> loops = new HashSet<>();
        Set<HalfEdge> seen = new HashSet<>();
        while (true) {
            if (graph.graphEdges.isEmpty()) {
                break;
            }
            HalfEdge edge = graph.graphEdges.get(graph.graphEdges.size() - 1);
            graph.graphEdges.remove(graph.graphEdges.size() - 1);
            if (null == edge) {
                break;
            }
            if (seen.contains(edge)) {
                continue;
            }
            Loop loop = new Loop();
            while (null != edge) {
                seen.add(edge);
                loop.halfEdges().add(edge);
                if (loop.halfEdges().get(0).vertexA().equals(edge.vertexB())) {
                    loops.add(loop);
                    break;
                }

                List<HalfEdge> candidates = graph.vertexToEdge.get(edge.vertexB());
                if (null == candidates) {
                    break;
                }
                final HalfEdge edgeT = edge;
                candidates = candidates.stream()
                        .filter(c -> !c.vertexB().equals(edgeT.vertexA()) || !HalfEdge.isSameEdge(c, edgeT))
                        .collect(Collectors.toList());

                edge = findMaxTurningLeft(edge, candidates, surface);

                if (seen.contains(edge)) {

                    break;
                }
            }
        }
        return loops;
    }

    private static <T, K> void addToListInMap(Map<T, List<K>> map, T key, K value) {
        List<K> list = map.get(key);
        if (null == list) {
            list = new ArrayList();
            map.put(key, list);
        }
        list.add(value);
    }

    private static Point3DC edgeVector(HalfEdge edge) {
        return edge.tangent(edge.vertexA().point());
    }

    private static HalfEdge findMaxTurningLeft(HalfEdge pivotEdge, List<HalfEdge> edges, BrepSurface surface) {
        if (edges.isEmpty()) {
            return null;
        }
        if (edges.size() == 1) {
            return edges.get(0);
        }
        edges = new ArrayList(edges);
        Point3DC pivot = pivotEdge.tangent(pivotEdge.vertexB().point()).negated();
        Point3DC normal = surface.normal(pivotEdge.vertexB().point());

        Collections.sort(
                edges,
                (e1, e2) -> {

                    double delta = leftTurningMeasure(pivot, edgeVector(e1), normal) - leftTurningMeasure(pivot, edgeVector(e2), normal);
                    if (Tolerance.ueq(delta, 0)) {

                        return HalfEdgeSolveData.getPriority(e2) - HalfEdgeSolveData.getPriority(e1);
                    }

                    return delta > 0 ? 1 : -1;
                });

        return edges.get(0);
    }

    public boolean canEdgeBeTransferred(HalfEdge edge, Face face, TYPE type) {
        Point3DC testPoint = edge.edge().curve().middlePoint();
        Point3DC edgeTangent = edge.tangent(testPoint);
        Point3DC edgeFaceNormal = edge.loop().face().surface().normal(testPoint);
        Point3DC edgeFaceDir = edgeFaceNormal.crossed(edgeTangent);
        Point3DC faceNormal = face.surface().normal(testPoint);
        double outsideMeasure = edgeFaceDir.dot(faceNormal);

        if (CadMath.isZero(outsideMeasure)) {
            throw new RuntimeException("this case should be condidered before calling this method");
        }

        boolean outside = outsideMeasure > 0;
        return (type == TYPE.INTERSECT) != outside;
    }

    List<Face> collectFaces(Shell shell1, Shell shell2) {
        List<Face> out = new ArrayList(shell1.faces().size() + shell2.faces().size());
        out.addAll(shell1.faces());
        out.addAll(shell2.faces());
        return out;
    }

    Shell prepareWorkingCopy(Shell shell) {
        Debug.mismatchBounds(shell.faces());
        Debug.checkLoopsValid(shell);

        Shell workingCopy = shell.clone();

        Debug.mismatchBounds(workingCopy.faces());
        Debug.checkLoopsValid(workingCopy);

        cleanUpOperationData(workingCopy);
        return workingCopy;
    }

    private void cleanUpOperationData(Shell shell) {
        shell.cache().clear();
        for (Face face : shell.faces()) {
            face.op = null;
//            face.cache().clear();
//            face.surface().impl().cache().clear();
            for (Loop loop : face.loops()) {
//                loop.cache().clear();
                for (HalfEdge he : loop.halfEdges()) {
//                    he.edge().getHalfEdge1().cache().clear();
//                    he.edge().getHalfEdge2().cache().clear();
//                    he.edge().cache().clear();
                    HalfEdgeSolveData.clear(he.edge().halfEdge1());
                    HalfEdgeSolveData.clear(he.edge().halfEdge2());
                    EdgeSolveData.clear(he.edge());
                }
            }
        }

    }

    void mergeVertices(Shell shell1, Shell shell2) {
        for (Face face1 : shell1.faces()) {
            for (Loop loop1 : face1.loops()) {
                for (HalfEdge he1 : loop1.halfEdges()) {

                    Vertex v11 = he1.vertexA();
                    Vertex v12 = he1.vertexB();
                    for (Face face2 : shell2.faces()) {
                        for (Loop loop2 : face2.loops()) {
                            for (HalfEdge he2 : loop2.halfEdges()) {

                                Vertex v21 = he2.vertexA();
                                Vertex v22 = he2.vertexB();

                                if (Tolerance.veq(v11.point(), v21.point())) {
                                    he2.setVertexA(v11);
                                } else if (Tolerance.veq(v12.point(), v21.point())) {
                                    he2.setVertexA(v12);
                                }

                                if (Tolerance.veq(v12.point(), v22.point())) {
                                    he2.setVertexB(v12);
                                } else if (Tolerance.veq(v11.point(), v22.point())) {
                                    he2.setVertexB(v11);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void initOperationData(List<Face> faces) {
        for (Face face : faces) {
            initOperationDataForFace(face);
        }
    }

    void initOperationDataForFace(Face face) {
        face.op = new FaceOperationData(face);
    }

    void initVertexFactory(Shell shell1, Shell shell2) {
        vertexFactory = new VertexFactory();
        for (Vertex v : shell1.vertexs()) {
            vertexFactory.addVertice(v);
        }
        for (Vertex v : shell2.vertexs()) {
            vertexFactory.addVertice(v);
        }
    }

    void intersectEdges(Shell shell1, Shell shell2) {
        HashMap<Edge, List<VertexIntersection>> isecs = findEdgeIntersections(shell1, shell2);

        Debug.soutNumberOfIntersectingEdges(isecs.size());
        Debug.soutNumberOfEdgeIntersections((int) isecs.values().stream().flatMap(List::stream).count());

        splitAtIntersections(isecs);
    }

    void splitAtIntersections(HashMap<Edge, List<VertexIntersection>> isecs) {
        List<Object[]> isecsArray = new ArrayList();
        for (Map.Entry<Edge, List<VertexIntersection>> isecsEntry : isecs.entrySet()) {
            isecsArray.add(new Object[]{isecsEntry.getKey(), isecsEntry.getValue()});
        }

        for (Object[] isecsEntry : isecsArray) {
            Collections.sort((List<VertexIntersection>) isecsEntry[1], (p1, p2) -> p1.u - p2.u > 0 ? 1 : -1);
            for (VertexIntersection vertInt : (List<VertexIntersection>) isecsEntry[1]) {
                if (vertInt.vertexHolder.isEmpty()) {

                    vertInt.vertexHolder.add(vertexFactory.create(((Edge) isecsEntry[0]).curve().point(vertInt.u)));
                } else if (null == vertInt.vertexHolder.get(0)) {

                    vertInt.vertexHolder.set(0, vertexFactory.create(((Edge) isecsEntry[0]).curve().point(vertInt.u)));
                }
            }
        }

        for (Object[] isecsEntry : isecsArray) {
            for (VertexIntersection vertInt : (List<VertexIntersection>) isecsEntry[1]) {
                SplitEdge split = splitEdgeByVertex((Edge) isecsEntry[0], vertInt.vertexHolder.get(0));
                if (null != split) {
                    isecsEntry[0] = split.e1;
                }
            }
        }
    }

    HashMap<Edge, List<VertexIntersection>> findEdgeIntersections(Shell shell1, Shell shell2) {
        HashMap<Edge, List<VertexIntersection>> isecs = new HashMap();
        for (Face face1 : shell1.faces()) {
            for (Face face2 : shell2.faces()) {
                if (face1.bounds().intersects(face2.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
                    for (Loop loop1 : face1.loops()) {
                        for (Loop loop2 : face2.loops()) {
                            if (loop1.bounds().intersects(loop2.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
                                for (HalfEdge he1 : loop1.halfEdges()) {
                                    for (HalfEdge he2 : loop2.halfEdges()) {
                                        Edge e1 = he1.edge();
                                        Edge e2 = he2.edge();

                                        if (e1.halfEdge1().bounds().intersects(e2.halfEdge1().bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {

                                            List<CurveIntersectData> points = e1.curve().intersectCurve(e2.curve());
                                            if (!points.isEmpty()) {

                                                List<VertexIntersection> allParams = isecs.get(e1);
                                                if (null == allParams) {

                                                    isecs.put(e1, points.stream()
                                                            .map(p -> new VertexIntersection(p.u0, p.p0))
                                                            .collect(Collectors.toList()));
                                                } else {
                                                    for (CurveIntersectData point : points) {

                                                        boolean contains = false;
                                                        for (VertexIntersection vI : allParams) {
                                                            if (Tolerance.veq(vI.p, new Point3DC(point.p0))) {
                                                                contains = true;
                                                                break;
                                                            }
                                                        }

                                                        if (!contains) {
                                                            allParams.add(new VertexIntersection(point.u0, point.p0));
                                                        }
                                                    }
                                                }

                                                allParams = isecs.get(e2);
                                                if (null == allParams) {

                                                    isecs.put(e2, points.stream()
                                                            .map(p -> new VertexIntersection(p.u1, p.p1))
                                                            .collect(Collectors.toList()));
                                                } else {
                                                    for (CurveIntersectData point : points) {
                                                        boolean contains = false;
                                                        for (VertexIntersection vI : allParams) {
                                                            if (Tolerance.veq(vI.p, new Point3DC(point.p1))) {
                                                                contains = true;
                                                                break;
                                                            }
                                                        }

                                                        if (!contains) {
                                                            allParams.add(new VertexIntersection(point.u1, point.p1));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return isecs;
    }

    private static SplitEdge splitEdgeByVertex(Edge edge, Vertex vertex) {
        if (edge.halfEdge1().vertexA().equals(vertex)
                || edge.halfEdge1().vertexB().equals(vertex)) {

            Debug.soutSplitingEdgeAtEnds();
            return null;
        }

        List<BrepCurve> curves = edge.curve().split(vertex.point());
        if (curves.size() != 2) {

            Debug.soutSplitingEdgeButNotTwo();
            return null;
        }

        Edge edge1 = new Edge(curves.get(0), edge.halfEdge1().vertexA(), vertex);
        Edge edge2 = new Edge(curves.get(1), vertex, edge.halfEdge1().vertexB());

        updateInLoop(edge.halfEdge1(), edge1.halfEdge1(), edge2.halfEdge1());
        updateInLoop(edge.halfEdge2(), edge2.halfEdge2(), edge1.halfEdge2());

        HalfEdgeSolveData.transferPriority(edge.halfEdge1(), edge1.halfEdge1());
        HalfEdgeSolveData.transferPriority(edge.halfEdge1(), edge2.halfEdge1());

        HalfEdgeSolveData.transferPriority(edge.halfEdge2(), edge2.halfEdge2());
        HalfEdgeSolveData.transferPriority(edge.halfEdge2(), edge1.halfEdge2());

        if (EdgeSolveData.isEdgeTransferred(edge)) {
            EdgeSolveData.markEdgeTransferred(edge1);
            EdgeSolveData.markEdgeTransferred(edge2);
        }

        return new SplitEdge(edge1, edge2);
    }

    private static void updateInLoop(HalfEdge halfEdge, HalfEdge h1, HalfEdge h2) {
        List<HalfEdge> halfEdges = halfEdge.loop().halfEdges();
        int indexOldHalfEdge = halfEdges.indexOf(halfEdge);
        if (-1 == indexOldHalfEdge) {
            indexOldHalfEdge = halfEdges.size() - 1;
        }
        halfEdges.add(indexOldHalfEdge, h1);
        halfEdges.add(indexOldHalfEdge + 1, h2);
        halfEdges.remove(halfEdge);

        h1.setLoop(halfEdge.loop());
        h2.setLoop(halfEdge.loop());

        h1.setPrev(halfEdge.getPrev());
        h1.getPrev().setNext(h1);

        h1.setNext(h2);
        h2.setPrev(h1);

        h2.setNext(halfEdge.getNext());
        h2.getNext().setPrev(h2);

        HalfEdgeSolveData.createIfEmpty(halfEdge).decayed = new DecayedHalfEdges(h1, h2);
    }

    private static boolean curveAndEdgeCoincident(BrepCurve curve, HalfEdge edge) {
        return edge.cache().get(Cache.CURVE_EDGE_COINCIDENT + curve.getId().hashCode(),
                () -> {
                    if (!edge.bounds()
                            .intersects(curve.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {

                        return false;
                    }

                    List<Point3DC> tess = edge.tessellate();

                    int touches = 0;

                    for (int i = tess.size() - 1; i >= 0; i--) {
                        Point3DC pt1 = tess.get(i);

                        Point3DC pt2 = curve.point(curve.param(pt1));

                        if (Tolerance.ueq(pt1, pt2)) {
                            touches++;
                        }
                    }

                    Debug.checkPartialTangency(
                            touches, 
                            tess.size(),
                            curve,
                            edge);

                    return touches == tess.size();

                });
    }

}
