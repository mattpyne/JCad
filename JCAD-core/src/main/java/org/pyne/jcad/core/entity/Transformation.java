package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.math.Transform;

/**
 *
 * @author Matthew Pyne
 */
public class Transformation implements IComponent {

    private Transform absoluteTransform;
    private Entity entity;

    public Transform getAbsoluteTransform() {
        if (null == absoluteTransform) {

            absoluteTransform = new Transform();
            entity.getComponent(Connections.class)
                    .ifPresent(connections -> absoluteTransform.apply(connections.transformsToTop()));
        }
        return absoluteTransform;
    }

    public void setAbsoluteTransform(Transform absoluteTransform) {
        this.absoluteTransform = absoluteTransform;
    }

    @Override
    public void invalidate() {
        absoluteTransform = null;
    }

    @Override
    public void update() {

    }

    @Override
    public void register(Entity entity) {
        this.entity = entity;
    }

}
