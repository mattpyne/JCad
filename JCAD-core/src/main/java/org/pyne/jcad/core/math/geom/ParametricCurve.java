package org.pyne.jcad.core.math.geom;

import org.pyne.jcad.core.math.geom.eval.CurveIntersectData;
import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import verb.core.BoundingBox;
import verb.core.Interval;

/**
 *
 * @author Matthew Pyne
 */
public abstract class ParametricCurve extends DataContainer {

    public static enum TYPE {
        LINE, NURBS, ARC, CIRCLE, BEZIER, ELLIPSE, APPROX
    };

    private Point3DC middlePoint;

    private TYPE type;

    public ParametricCurve(TYPE type) {
        super();
        this.type = type;
    }
    
    /**
     *
     * @param u
     * @return
     */
    public boolean isInside(double u) {
        return u >= domain().min.doubleValue() && u <= domain().max.doubleValue();
    }

    /**
     *
     * @param surface
     * @return
     */
    public abstract boolean intersectsSurface(Surface surface);

    /**
     *
     * @param surface
     * @return
     */
    public abstract Set<Point3DC> pointsOfSurfaceIntersection(Surface surface);

    public Point3DC tangentAtPoint(Point3DC point) {
        return tangentAtParam(param(point));
    }

    /**
     *
     * @param u
     * @return
     */
    public Point3DC tangentAtParam(double u) {
        Array<Array<Number>> dr = eval(u, 1);
        return new Point3DC(dr.__a[1]).normalized();
    }
    
    public abstract ParametricCurve getTranslated(Point3DC vector);
    
    public abstract void translated(Point3DC vector);

    public abstract Interval<Number> domain();

    public abstract int degree();

    public abstract Array<Number> degree1Tess();

    public abstract Array<Array<Number>> eval(double u, int num);

    public abstract Point3DC point(double param);

    /**
     *
     * @param point
     * @return
     */
    public boolean passesThrough(Point3DC point) {
        if (!bounds().contains(point.asArray(), Tolerance.TOLERANCE * 1E2)) {
            return false;
        }
        return Tolerance.eqSqTol(0, point.distanceSq(point(param(point))));
    }

    /**
     *
     * @return
     */
    public Point3DC middlePoint() {
        if (null == middlePoint) {
            middlePoint = point(0.5);
        }
        return middlePoint;
    }

    public abstract double param(Point3DC point);

    public abstract ParametricCurve transform(Matrix3 tr);

    public abstract List<ParametricCurve> split(Point3DC point);

    public List<ParametricCurve> split(double d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public abstract int optimalSplits();

    public abstract void normalizeParametrization();

    public abstract ParametricCurve invert();

    public List<Point3DC> tessellate() {
        return tessellate(null);
    }

    public List<Point3DC> tessellate(Number tessTol) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Point3DC> tessellate(double uMin, double uMax) {
        return tessellate(uMin, uMax, Tolerance.TESS_TOLERANCE);
    }

    public List<CurveIntersectData> intersectCurve(ParametricCurve other) {
        List<CurveIntersectData> intersectionData = cache().get(
                Cache.CURVE_CURVE_INTERSECTION + other.getId().hashCode(),
                () -> {
              
                    if (other.bounds().intersects(bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {

                        List<CurveIntersectData> cids = intersectCurve(other, Tolerance.NUMERICAL_SOLVE_TOL);
                        other.cache().get(
                                Cache.CURVE_CURVE_INTERSECTION + getId().hashCode(),
                                () -> cids);

                        return cids;
                    }

                    List<CurveIntersectData> cids = new ArrayList();
                    other.cache().get(
                            Cache.CURVE_CURVE_INTERSECTION + getId().hashCode(),
                            () -> cids);
                    return cids;
                });

        return intersectionData;
    }

    public List<CurveIntersectData> intersectCurve(ParametricCurve other, Number tol) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ParametricCurve copy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public abstract BoundingBox bounds();

    public List<Point3DC> tessellate(Double uMin1, Double uMax1, Double tol) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public NurbsCurve asNurbs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

}
