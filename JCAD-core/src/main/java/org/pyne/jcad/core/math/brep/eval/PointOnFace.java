package org.pyne.jcad.core.math.brep.eval;

import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.geom.Tolerance;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Vertex;

/**
 *
 * @author Matthew Pyne
 */
public class PointOnFace {

    /**
     *
     * @param face
     * @param point
     * @return
     */
    public static PointOnFaceData rayCast(Face face, Point3DC point) {
        return rayCast(face, point, null);
    }

    /**
     *
     * @param face
     * @param point
     * @param surface
     * @return
     */
    public static PointOnFaceData rayCast(Face face, Point3DC point, BrepSurface surface) {
        PointOnFaceData result = new PointOnFaceData();
        surface = null != surface ? surface : face.surface();

        // Quick fail if the point is not near.
        if (!face.bounds()
                .contains(point.asArray(), Tolerance.TOLERANCE_RAY_CAST)) {

            return result;
        }

        for (HalfEdge edge : face.halfEdges()) {
            
            if (Tolerance.veq(point, edge.vertexA().point())) {
                
                result.inside = true;
                result.strictInside = false;
                result.edge = edge;
                return result;
            }
        }

        for (HalfEdge edge : face.halfEdges()) {
            
            if (edge.edge().curve().passesThrough(point)) {
                
                result.inside = true;
                result.strictInside = false;
                result.edge = edge;
                return result;
            }
        }
        HalfEdge closestEdge = null;
        Point3DC closestPt = null;
        Double closestDist = null;
        for (Loop loop : face.loops()) {

            if (loop.bounds()
                    .contains(point.asArray(), Tolerance.TOLERANCE_RAY_CAST)) {
                
                for (HalfEdge he : loop.halfEdges()) {
                    
                    Point3DC closestPoint = he.closestPointTo(point);
                    double dist = point.distanceSq(closestPoint);
                    if (null == closestDist || dist < closestDist) {
                        
                        closestEdge = he;
                        closestPt = closestPoint;
                        closestDist = dist;
                    }
                }
            }
        }
        HalfEdge encloseEdge1 = null;
        HalfEdge encloseEdge2 = null;
        Vertex encloseVertex = null;
        
        if (Tolerance.veq(closestPt, closestEdge.vertexA().point())) {
            
            encloseEdge1 = closestEdge.getPrev();
            encloseEdge2 = closestEdge;
            encloseVertex = closestEdge.vertexA();
        } else if (Tolerance.veq(closestPt, closestEdge.vertexB().point())) {
            
            encloseEdge1 = closestEdge;
            encloseEdge2 = closestEdge.getNext();
            encloseVertex = closestEdge.vertexB();
        }

        Point3DC tangent;
        if (null != encloseEdge1) {
            
            tangent = encloseEdge1.tangentAtEnd().plus(encloseEdge2.tangentAtStart()).normalized();
        } else {
            
            tangent = closestEdge.tangent(closestPt);
        }

        Point3DC testee = (null != encloseVertex ? encloseVertex.point() : closestPt);
        testee = testee.minus(point).normalized();

        Point3DC normal = surface.normal(closestPt);

        boolean inside = !CadMath.isOnPositiveHalfPlaneFromVec(tangent, testee, normal);


//        boolean inside = face.getOuterLoop()
//                .bounds()
//                .contains(point.asArray(), Tolerance.TOLERANCE);
//
//        if (inside == true) {
//
//            inside = Tolerance.veq(point, surface.closestPoint(point));
//        }

        result.inside = inside;
        result.strictInside = inside;

        return result;

    }

    /**
     *
     * @param face
     * @param pt
     * @return
     */
    public static boolean point(Face face, Point3DC pt) {

        if (!face.bounds()
                .contains(pt.asArray(), Tolerance.TOLERANCE)) {

            return false;
        }

        Point3DC closestPoint = face.surface().closestPoint(pt);

        if (!closestPoint.equals(pt)) {
            return false;
        }

        return face.env2D().pip
                .classifyPointInsideLoops(face.surface().workingPoint(pt)).inside;
    }

}
