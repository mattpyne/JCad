package org.pyne.jcad.core.math;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Matthew Pyne
 */
public class PathMath {

    public static Point3DC normalOfCCWSeg(List<Point3DC> ccwSequence) {
        Point3DC a = ccwSequence.get(0);
        Point3DC b = ccwSequence.get(1);

        Point3DC ab = b.minus(a);
        for (int i = 2; i < ccwSequence.size(); i++) {
            Point3DC c = ccwSequence.get(i);
            Point3DC normal = ab.crossed(c.minus(a)).normalized();
            if (!normal.isZero()) {
                return normal;
            }
        }

        return null;
    }

    public static boolean isCCW(List<Point3DC> path2D) {
        return area(path2D) >= 0.;
    }

    public static double area(List<Point3DC> path2D) {
        int n = path2D.size();
        double a = 0.;
        for (int i = 0; i < n; i++) {
            Point3DC prev = path2D.get(i);
            Point3DC next = path2D.get((i + 1) % n);

            a += prev.x() * next.y() - next.x() * prev.y();
        }
        return a * 0.5;
    }

    /**
     * Shuffles the path so that the starting point is the closest to zero.
     *
     * @param path
     * @return
     */
    public static List<Point3DC> startPathClosestToZero(List<Point3DC> path) {
        int minIndex = 0;
        for (int i = 0; i < path.size(); i++) {

            if (path.get(minIndex).distanceSq(Point3DC.ZERO) > path.get(i).distanceSq(Point3DC.ZERO)) {
                minIndex = i;
            }
        }

        List<Point3DC> bottom = path.subList(minIndex, path.size());
        List<Point3DC> top = path.subList(0, minIndex);

        List<Point3DC> points = new ArrayList();
        points.addAll(bottom);
        points.addAll(top);

        path.clear();
        path.addAll(points);

        return path;
    }

    public static boolean isClosed(List<Point3DC> points) {
        if (points.isEmpty()) {
            return false;
        }
        return points.get(0).equals(points.get(points.size() - 1));
    }

    public static boolean isSelfIntersected(List<Point3DC> points) {
        Set<Line2D> plines = new HashSet<>();

        for (int i = 0; i < points.size(); i++) {

            int j = (i + 1) % points.size();
            Point3DC pti = points.get(i);
            Point3DC ptj = points.get(j);

            plines.add(new Line2D.Double(pti.x(), pti.y(), ptj.x(), ptj.y()));
        }

        return new ShamosHoeyIntersectionAlgo(false).perform(plines);
    }

}
