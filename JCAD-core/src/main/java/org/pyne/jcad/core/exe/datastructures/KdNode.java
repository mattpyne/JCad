package org.pyne.jcad.core.exe.datastructures;

import static org.pyne.jcad.core.exe.datastructures.KdPointComparators.X_COMPARATOR;
import static org.pyne.jcad.core.exe.datastructures.KdPointComparators.Y_COMPARATOR;
import static org.pyne.jcad.core.exe.datastructures.KdPointComparators.Z_COMPARATOR;
import static org.pyne.jcad.core.exe.datastructures.KdTree.X_AXIS;
import static org.pyne.jcad.core.exe.datastructures.KdTree.Y_AXIS;

public class KdNode<T> implements Comparable<KdNode<T>> {

    final KdPoint<T> id;
    final int k;
    final int depth;

    KdNode parent = null;
    KdNode lesser = null;
    KdNode greater = null;

    public KdNode(KdPoint<T> id) {
        this.id = id;
        this.k = 3;
        this.depth = 0;
    }

    public KdNode(KdPoint<T> id, int k, int depth) {
        this.id = id;
        this.k = k;
        this.depth = depth;
    }

    public static int compareTo(int depth, int k, KdPoint o1, KdPoint o2) {
        int axis = depth % k;
        if (axis == X_AXIS)
            return X_COMPARATOR.compare(o1, o2);
        if (axis == Y_AXIS)
            return Y_COMPARATOR.compare(o1, o2);
        return Z_COMPARATOR.compare(o1, o2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return 31 * (this.k + this.depth + this.id.hashCode());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof KdNode))
            return false;

        KdNode kdNode = (KdNode) obj;
        if (this.compareTo(kdNode) == 0)
            return true;
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(KdNode o) {
        return compareTo(depth, k, this.id, o.id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("k=").append(k);
        builder.append(" depth=").append(depth);
        builder.append(" id=").append(id.toString());
        return builder.toString();
    }
}