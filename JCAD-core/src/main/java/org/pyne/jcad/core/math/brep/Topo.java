package org.pyne.jcad.core.math.brep;

import org.pyne.jcad.core.math.Matrix3;

/**
 *
 * @author Matthew Pyne
 */
public interface Topo {

    public Topo copy();

    public void transform(Matrix3 tr);

    public Topo transformed(Matrix3 tr);
    
    public String getId();

    public boolean isEmpty();

}
