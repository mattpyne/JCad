package org.pyne.jcad.core.math.brep.creators;

import haxe.lang.EmptyObject;
import org.pyne.jcad.core.math.brep.BrepSurface;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.PathMath;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.eval.PointInPolygon;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.BoundingBox;

/**
 *
 * @author Matthew Pyne
 */
public class BrepFaceEvolve {

    public static List<Face> EvolveFace(Face originFace, Collection<Loop> loops) {
        List<Face> out = new ArrayList<>();
        BrepSurface originSurface = originFace.surface();

        List<NestedLoop> nestedLoops = getNestedLoops(originSurface, loops);
        for (NestedLoop nestedLoop : nestedLoops) {
            if (nestedLoop.getLevel() == 0) {
                createFaces(originSurface, originFace, loops, null, out, nestedLoop, 0);
            }
        }

        if (!out.isEmpty()) {
            out.get(0).setId(originFace.getId());
        } else {
            Debug.soutEmptyLoopFromEvolvFace();
        }

        return out;
    }
    
    public static List<Face> EvolveFace(BrepSurface surface, Collection<Loop> loops) {
        List<Face> out = new ArrayList<>();

        List<NestedLoop> nestedLoops = getNestedLoops(surface, loops);
        for (NestedLoop nestedLoop : nestedLoops) {
            if (nestedLoop.getLevel() == 0) {
                createFaces(surface, loops, null, out, nestedLoop, 0);
            }
        }

        if (out.isEmpty()) {
            Debug.soutEmptyLoopFromEvolvFace();
        }

        return out;
    }

    private static BrepSurface invertSurface(BrepSurface invertedSurface, BrepSurface originSurface) {
        if (null == invertedSurface) {
            invertedSurface = originSurface.invert();
        }
        return invertedSurface;
    }

    private static void createFaces(BrepSurface originSurface,
            Face originFace,
            Collection<Loop> originLoops,
            BrepSurface invertedSurface,
            List<Face> out,
            NestedLoop nestedLoop,
            int level) {

        BrepSurface surface;
        if (nestedLoop.isInverted()) {
            surface = invertSurface(invertedSurface, originSurface);
        } else {
            surface = originSurface;
        }

        Loop loop = nestedLoop.getLoop();
        Face newFace = new Face(surface);
        newFace.setData(originFace.data());
        newFace.setOuterLoop(loop);
        loop.setFace(newFace);
        out.add(newFace);

        for (NestedLoop child : nestedLoop.getNesting()) {
            if (child.getLevel() == level + 2) {
                createFaces(originSurface, originFace, originLoops, invertedSurface, out, child, level + 2);
            } else if (child.getLevel() == level + 1) {
                if (nestedLoop.isInverted() != child.isInverted()) {
                    child.getLoop().setFace(newFace);
                    newFace.addInnerLoop(child.getLoop());
                } else {
                    createFaces(originSurface, originFace, originLoops, invertedSurface, out, child, level + 1);
                }
            }
        }

    }
    
    private static void createFaces(BrepSurface originSurface,
            Collection<Loop> originLoops,
            BrepSurface invertedSurface,
            List<Face> out,
            NestedLoop nestedLoop,
            int level) {

        BrepSurface surface;
        if (nestedLoop.isInverted()) {
            surface = invertSurface(invertedSurface, originSurface);
        } else {
            surface = originSurface;
        }

        Loop loop = nestedLoop.getLoop();
        Face newFace = new Face(surface);
        newFace.setOuterLoop(loop);
        loop.setFace(newFace);
        out.add(newFace);

        for (NestedLoop child : nestedLoop.getNesting()) {
            if (child.getLevel() == level + 2) {
                createFaces(originSurface, originLoops, invertedSurface, out, child, level + 2);
            } else if (child.getLevel() == level + 1) {
                if (nestedLoop.isInverted() != child.isInverted()) {
                    child.getLoop().setFace(newFace);
                    newFace.addInnerLoop(child.getLoop());
                } else {
                    createFaces(originSurface, originLoops, invertedSurface, out, child, level + 1);
                }
            }
        }

    }
    public static Face EvolveOneFace(Face originFace, Collection<Loop> loops) {
        List<Face> out = new ArrayList<>();
        BrepSurface originSurface = originFace.surface();

        List<NestedLoop> nestedLoops = getNestedLoops(originSurface, loops);
        for (NestedLoop nestedLoop : nestedLoops) {
            if (nestedLoop.getLevel() == 0) {
                createOneFaces(originSurface, originFace, null, out, nestedLoop);
            }
        }

        if (!out.isEmpty()) {
            out.get(0).setId(originFace.getId());
        } else {
            Debug.soutEmptyLoopFromEvolvFace();
        }

        return out.get(0);
    }
    
    public static Face EvolveOneFace(BrepSurface surface, Collection<Loop> loops) {
        List<Face> out = new ArrayList<>();

        List<NestedLoop> nestedLoops = getNestedLoops(surface, loops);
        for (NestedLoop nestedLoop : nestedLoops) {
            if (nestedLoop.getLevel() == 0) {
                createOneFaces(surface, out, nestedLoop);
            }
        }

        if (out.isEmpty()) {
            Debug.soutEmptyLoopFromEvolvFace();
        }

        return out.get(0);
    }
    
    public static Face EvolveOneFace(BrepSurface surface, Loop loop) {
        Face face = new Face(surface);
        
        face.setOuterLoop(loop);
        
        return face;
    }

    private static void createOneFaces(BrepSurface originSurface,
            Face originFace,
            BrepSurface invertedSurface,
            List<Face> out,
            NestedLoop nestedLoop) {

        BrepSurface surface;
        if (nestedLoop.isInverted()) {
            surface = invertSurface(invertedSurface, originSurface);
        } else {
            surface = originSurface;
        }

        Loop loop = nestedLoop.getLoop();
        Face newFace = new Face(surface);
        newFace.setData(originFace.data());
        newFace.setOuterLoop(loop);
        loop.setFace(newFace);
        out.add(newFace);

        for (NestedLoop child : nestedLoop.getNesting()) {

            if (nestedLoop.isInverted() == child.isInverted()) {

                child.getLoop().reverse();
                child.getLoop().setHalfEdges(loop.halfEdges()
                        .stream()
                        .map(HalfEdge::twin)
                        .collect(Collectors.toList()));
            }

            child.getLoop().setFace(newFace);
            newFace.addInnerLoop(child.getLoop());
        }

    }
    
    private static void createOneFaces(BrepSurface originSurface,
            List<Face> out,
            NestedLoop nestedLoop) {

        BrepSurface surface = originSurface;

        Loop loop = nestedLoop.getLoop();
        Face newFace = new Face(surface);
        newFace.setOuterLoop(loop);
        loop.setFace(newFace);
        out.add(newFace);

        for (NestedLoop child : nestedLoop.getNesting()) {
            
            Loop childLoop = child.getLoop();
            if (nestedLoop.isInverted() == child.isInverted()) {

                childLoop.reverse();
                childLoop.setHalfEdges(childLoop.halfEdges()
                        .stream()
                        .map(HalfEdge::twin)
                        .collect(Collectors.toList()));
                childLoop.link();
            }

            childLoop.setFace(newFace);
            newFace.addInnerLoop(childLoop);
        }

    }

    private static List<NestedLoop> getNestedLoops(BrepSurface surface, Collection<Loop> brepLoops) {
        List<NestedLoop> loops = brepLoops.stream()
                .map(loop -> new NestedLoop(loop, surface))
                .collect(Collectors.toList());

        for (int i = 0; i < loops.size(); ++i) {
            NestedLoop loop = loops.get(i);
            for (int j = 0; j < loops.size(); ++j) {
                if (i == j) {
                    continue;
                }
                NestedLoop other = loops.get(j);
                if (contains(loop, other)) {
                    loop.getNesting().add(other);
                    other.increaseLevel();
                }
            }
        }
        return loops.stream()
                .filter(l -> l.getLevel() == 0)
                .collect(Collectors.toList());
    }

    private static boolean contains(NestedLoop loop, NestedLoop other) {
        
        if (!loop.getLoop().bounds().contains(other.getLoop().bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
            
            return false;
        }
                
        for (Point3DC point : other.getWorkingPolygon()) {

            if (!loop.getPip().classifyPointInsideLoops(point).inside) {
                
                return false;
            }
        }
        return true;
    }
}

class NestedLoop {

    private Loop loop;
    private List<Point3DC> workingPolygon;
    private Boolean inverted;
    private PointInPolygon pip;
    private List<NestedLoop> nesting;
    private Integer level;
    private BrepSurface surface;

    public NestedLoop(Loop loop, BrepSurface surface) {
        this.loop = loop;
        this.surface = surface;
        nesting = new ArrayList<>();
        level = 0;
    }

    public PointInPolygon getPip() {
        if (null == pip) {
            
            BoundingBox workingBounds = new BoundingBox(EmptyObject.EMPTY);
            BoundingBox bounds = loop.bounds();
            
            workingBounds.add(surface.workingPoint(new Point3DC(bounds.min)).asArray());
            workingBounds.add(surface.workingPoint(new Point3DC(bounds.max)).asArray());
            
            pip = new PointInPolygon(getWorkingPolygon(), workingBounds);
        }
        return pip;
    }

    public Loop getLoop() {
        return loop;
    }

    public List<Point3DC> getWorkingPolygon() {
        if (null == workingPolygon) {
            workingPolygon = loop.tessellate()
                    .stream()
                    .map(p -> surface.workingPoint(p))
                    .collect(Collectors.toList());
        }
        return workingPolygon;
    }

    public boolean isInverted() {
        if (null == inverted) {
            inverted = !PathMath.isCCW(getWorkingPolygon());
        }
        return inverted;
    }

    public List<NestedLoop> getNesting() {
        return nesting;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    void increaseLevel() {
        this.level = this.level + 1;
    }

}
