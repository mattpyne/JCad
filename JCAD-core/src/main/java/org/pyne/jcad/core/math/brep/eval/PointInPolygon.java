package org.pyne.jcad.core.math.brep.eval;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import verb.core.BoundingBox;

/**
 *
 * @author Matthew Pyne
 */
public class PointInPolygon {

    private LoopPIP outerLoop;
    private List<LoopPIP> innerLoops;
    private double tol;

    public PointInPolygon(List<Point3DC> outerLoop, List<List<Point3DC>> innerLoops) {
        this(outerLoop, innerLoops, null);
    }

    public PointInPolygon(List<Point3DC> outerLoop, List<List<Point3DC>> innerLoops, Number tol) {
        this.outerLoop = new LoopPIP(outerLoop);
        if (null != innerLoops) {
            this.innerLoops = innerLoops.stream().map(LoopPIP::new).collect(Collectors.toList());
        }
        this.tol = null == tol ? 1e-6 : tol.doubleValue();
    }
    
    public PointInPolygon(List<Point3DC> outerLoop, BoundingBox outerBounds) {
        this.outerLoop = new LoopPIP(outerLoop, outerBounds);
        this.tol = 1e-6;
    }

    public InnerPIPResult classifyPointInsideLoops(Point3DC pt) {
        
        InnerPIPResult outer = classifyPointInsideLoops(pt, outerLoop);
        if (outer.inside) {
            if (outer.vertex || outer.edge) {
                return outer;
            }
        }
        
        if (null != innerLoops
                && !innerLoops.isEmpty()) {

            for (LoopPIP innerLoop : innerLoops) {
                InnerPIPResult inner = classifyPointInsideLoops(pt, innerLoop);
                if (inner.edge || inner.vertex) {
                    return inner;
                }
                if (inner.inside) {
                    return InnerPIPResult.inside(Boolean.FALSE);
                }
            }
        }

        return outer;
    }

    private InnerPIPResult classifyPointInsideLoops(Point3DC pt, LoopPIP loop) {
        
        if (!loop.bounds().contains(pt.asArray(), tol)) {
            return InnerPIPResult.inside(Boolean.FALSE);
        }
        
        for (Point3DC pt2 : loop) {
            if (pt2.equals(pt)) {
                return InnerPIPResult.vertex(Boolean.TRUE);
            }
        }

        List<Integer> grads = new ArrayList<>();
        int n = loop.size();

        for (int i = 0; i < n; ++i) {
            int j = (i + 1) % n;
            Point3DC a = loop.get(i);
            Point3DC b = loop.get(j);
            double dy = b.getY() - a.getY();
            if (CadMath.eq(dy, 0, tol)) {
                grads.add(0);
            } else if (dy > 0) {
                grads.add(1);
            } else {
                grads.add(-1);
            }
        }

        HashSet<Point3DC> skip = new HashSet<>();
        boolean inside = false;
        for (int i = 0; i < n; ++i) {
            int j = (i + 1) % n;
            Point3DC a = loop.get(i);
            Point3DC b = loop.get(j);

            boolean shouldbeSkipped = skip.contains(a) || skip.contains(b);

            boolean aEq = CadMath.eq(pt.getY(), a.getY(), tol);
            boolean bEq = CadMath.eq(pt.getY(), b.getY(), tol);

            if (aEq) {
                skip.add(a);
            }
            if (bEq) {
                skip.add(b);
            }

            if (a.equals(b)) {
                System.err.println("unable to classify invalid polygon");
            }

            Point3DC edgeLowPt = a;
            Point3DC edgeHighPt = b;

            double edgeDx = edgeHighPt.getX() - edgeLowPt.getX();
            double edgeDy = edgeHighPt.getY() - edgeLowPt.getY();

            if (aEq && bEq) {
                if (((edgeHighPt.x() <= pt.x()) && (pt.x() <= edgeLowPt.x()))
                        || ((edgeLowPt.x() <= pt.x()) && (pt.x() <= edgeHighPt.x()))) {

                    return InnerPIPResult.edge(true);
                } else {
                    continue;
                }
            }

            if (shouldbeSkipped) {
                continue;
            }

            if (edgeDy < 0) {
                edgeLowPt = b;
                edgeDx = -edgeDx;
                edgeHighPt = a;
                edgeDy = -edgeDy;
            }
            if (!aEq && !bEq && (pt.y() < edgeLowPt.y() || pt.y() > edgeHighPt.y())) {
                continue;
            }

            if (bEq) {
                if (grads.get(i) * nextGrad(i, grads) < 0) {
                    continue;
                }
            } else if (aEq) {
                if (grads.get(i) * prevGrad(i, grads) < 0) {
                    continue;
                }
            }

            double perpEdge = edgeDx * (pt.y() - edgeLowPt.y()) - edgeDy * (pt.x() - edgeLowPt.x());
            if (CadMath.eq(perpEdge, 0, tol)) {
                return InnerPIPResult.edge(true);
            }
            if (perpEdge < 0) {
                continue;
            }
            inside = !inside;		// true intersection left of pt
        }

        return InnerPIPResult.inside(inside);
    }

    private Integer nextGrad(int start, List<Integer> grads) {
        for (int i = 0; i < grads.size(); ++i) {
            int idx = (i + start + 1) % grads.size();
            if (grads.get(idx) != 0) {
                return grads.get(idx);
            }
        }
        return null;
    }

    private Integer prevGrad(int start, List<Integer> grads) {
        for (int i = 0; i < grads.size(); ++i) {
            int idx = (start - i - 1 + grads.size()) % grads.size();
            if (grads.get(idx) != 0) {
                return grads.get(idx);
            }
        }
        return null;
    }
}

