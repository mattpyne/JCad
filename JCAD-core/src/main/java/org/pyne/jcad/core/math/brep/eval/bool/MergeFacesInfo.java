package org.pyne.jcad.core.math.brep.eval.bool;

import java.util.*;

import haxe.lang.EmptyObject;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.eval.EdgeGraph;
import org.pyne.jcad.core.math.brep.eval.FacePrototype;
import org.pyne.jcad.core.math.brep.eval.PointOnFace;
import verb.core.BoundingBox;

/**
 *
 * @author Matthew Pyne
 */
public class MergeFacesInfo {

    public List<Face> originFaces = new ArrayList<>();
    public List<FacePrototype> facePrototypes = new ArrayList();
    private List<Point3DC> allPoints = new ArrayList<>();
    private ShellBooleanOps.TYPE type;
    private Set<HalfEdge> inValid = new HashSet();
    private Set<HalfEdge> coincidentEdges = new HashSet();
    private BoundingBox bounds;

    public static MergeFacesInfo mergeFaces(List<Face> facesA, List<Face> facesB, ShellBooleanOps.TYPE type) {
        MergeFacesInfo info = new MergeFacesInfo();
        info.originFaces.addAll(facesA);
        info.originFaces.addAll(facesB);
        info.type = type;
        info.bounds = new BoundingBox(EmptyObject.EMPTY);

        for (Face face : info.originFaces) {
            for (HalfEdge e : face.halfEdges()) {
                info.allPoints.add(e.vertexA().point());
            }
        }

        for (Face face : info.originFaces) {
            for (HalfEdge e : face.halfEdges()) {
                info.bounds.add(e.bounds().max);
                info.bounds.add(e.bounds().min);
            }
        }

        for (Face faceA : facesA) {
            for (Face faceB : facesB) {
                info.invalidate(faceA, faceB);
            }
        }

        Set<HalfEdge> leftovers = null;
        for (Face referenceFace : info.originFaces) {

            EdgeGraph graph = new EdgeGraph();
            for (Face face : info.originFaces) {
                for (HalfEdge edge : face.halfEdges()) {
                    if (!info.inValid.contains(edge) && (leftovers == null || leftovers.contains(edge))) {
                        graph.add(edge);
                    }
                }
            }

            leftovers = new HashSet(graph.graphEdges);
            Set<Loop> detectedLoops = new ShellBooleanOps().detectLoops(referenceFace.surface(), graph);

            for (Loop loop : detectedLoops) {
                for (HalfEdge edge : loop.halfEdges()) {
                    leftovers.remove(edge);
                }
            }

            if (!detectedLoops.isEmpty()) {
                FacePrototype facePrototype = new FacePrototype();
                facePrototype.loops = detectedLoops;
                facePrototype.surface = BrepBuilder.createBoundingNurbs(
                        Arrays.asList(
                                new Point3DC(info.bounds.min).minus(Point3DC.UNITY),  // Adding one so am sure the
                                new Point3DC(info.bounds.max).plus(Point3DC.UNITY)), // surface is bigger than the loop
                        referenceFace.surface().getSimpleSurface());
                info.facePrototypes.add(facePrototype);
            }
        }

        return info;
    }

    private void invalidate(Face faceA, Face faceB) {
        for (HalfEdge edgeA : faceA.halfEdges()) {
            for (HalfEdge edgeB : faceB.halfEdges()) {
                checkCoincidentEdges(edgeA, edgeB);
            }
        }

        invalidateImpl(faceA, faceB);
        invalidateImpl(faceB, faceA);
    }

    private void invalidateEdge(Face face, HalfEdge edge) {
        Point3DC pt = edge.edge().curve().middlePoint();
        if (PointOnFace.rayCast(face, pt).inside) {
            EdgeSolveData.markEdgeTransferred(edge.edge());
            if (new ShellBooleanOps().canEdgeBeTransferred(edge.twin(), face, type)) {
                HalfEdgeSolveData.setPriority(edge, 10);
            } else {
                inValid.add(edge);
            }
        }
    }

    private void invalidateImpl(Face faceX, Face faceY) {
        for (HalfEdge edgeX : faceX.halfEdges()) {
            if (coincidentEdges.contains(edgeX)) {
                continue;
            }
            invalidateEdge(faceY, edgeX);
        }
    }

    private void checkCoincidentEdges(HalfEdge edgeA, HalfEdge edgeB) {
        if (!HalfEdge.isSameEdge(edgeA, edgeB)) {
            return;
        }
        HalfEdgeSolveData.markCollision(edgeA, edgeB);
        coincidentEdges.add(edgeA);
        coincidentEdges.add(edgeB);
        EdgeSolveData.markEdgeTransferred(edgeA.edge());
        EdgeSolveData.markEdgeTransferred(edgeB.edge());
        if (edgeA.vertexA().equals(edgeB.vertexA())) {

            Face faceAAdjacent = edgeA.twin().loop().face();
            Face faceBAdjacent = edgeB.twin().loop().face();
            FaceOperationData faceAAdjacentOp = faceAAdjacent.op;

            if (null != faceAAdjacentOp.overlaps
                    && !faceAAdjacentOp.overlaps.isEmpty()
                    && faceAAdjacentOp.overlaps.contains(faceBAdjacent)) {

                inValid.add(edgeB);
            } else {

                System.out.println("edges can't be coincident for this operation");
            }
        } else if (edgeA.vertexA().equals(edgeB.vertexB())) {

            inValid.add(edgeA);
            inValid.add(edgeB);
        }
    }
}
