package org.pyne.jcad.core.math;

import org.pyne.jcad.core.verbinterop.VerbMath;
import haxe.root.Array;
import java.util.function.Function;

import verb.core.Mat;

/**
 *
 * @author Matthew Pyne
 */
public class Optimization {

    /**
     * 
     * @param f
     * @param x0
     * @param tol
     * @param gradient
     * @return 
     */
    public static OptimizationResult fmin_bfgs(
            Function<Array<Number>, Double> f,
            Array<Number> x0,
            Number tol,
            Function<Array<Number>, Array<Number>> gradient) {
        
        return fmin_bfgs(f, x0, tol, gradient, null);
    }
    
    /**
     *
     * @param f
     * @param x0
     * @param tol
     * @param gradient
     * @param maxIteration
     * @return
     */
    public static OptimizationResult fmin_bfgs(
            Function<Array<Number>, Double> f,
            Array<Number> x0,
            Number tol,
            Function<Array<Number>, Array<Number>> gradient,
            Number maxIteration) {

        if (null == tol) {
            tol = 1e-8;
        }
        if (null == maxIteration) {
            maxIteration = 1000;
        }
        x0 = x0.copy();
        int n = x0.length;
        double f0 = f.apply(x0);
        Double f1 = null;
        if (Double.isNaN(f0)) {
            throw new RuntimeException("uncmin: f(x) is a NaN!");
        }
        Array<Array<Number>> H1 = Mat.identity(n);
        Array<Number> step;
        Array<Number> g0 = gradient.apply(x0);
        Array<Number> g1;
        double df0;
        Array<Number> x1;
        Array<Number> s = null;
        Array<Number> y;
        double ys;
        Array<Number> Hy;

        double nstep;
        double t;
        int it = 0;
        String msg = "";

        while (it < maxIteration.intValue()) {
//            if (!VerbMath.allIsfinite(g0)) {
//                msg = "Gradient has Infinity or NaN";
//                break;
//            }
            step = VerbMath.neg(VerbMath.dot(H1, g0));
//            if (!VerbMath.allIsfinite(step)) {
//                msg = "Search direction has Infinity or NaN";
//                break;
//            }
            nstep = VerbMath.norm2(step);
            if (nstep < tol.doubleValue()) {
                msg = "Newton step smaller tol";
                break;
            }
            t = 1.;
            df0 = VerbMath.dotVec(g0, step);
            // line search
            x1 = x0;
            double tL = 0;
            double tR = 100;
            while (it < maxIteration.intValue()) {
                if (t * nstep < tol.doubleValue()) {
                    break;
                }
                s = VerbMath.mul(t, step);
                x1 = VerbMath.addVecs(x0, s);
                f1 = f.apply(x1);
                // nocadel, 3.7(a,b)                
                if (f1 - f0 >= 0.1 * t * df0 || Double.isNaN(f1)) {
                    tR = t;
                    t = (tL + tR) * 0.5;
                    ++it;
                } else {
                    double slope = VerbMath.dotVec(gradient.apply(x1), step);
                    if (slope <= 0.9 * Math.abs(df0)) {
                        break;
                    } else if (slope >= 0.9 * df0) {
                        tR = t;
                        t = (tL + tR) * 0.5;
                    } else {
                        tL = t;
                        t = (tL + tR) * 0.5;
                    }
                }
            }
            if (t * nstep < tol.doubleValue()) {
                msg = "Line search step size smaller than tol";
                break;
            }
            if (it == maxIteration.intValue()) {
                msg = "maxit reached during line search";
                break;
            }
            g1 = gradient.apply(x1);
            y = VerbMath.subVecs(g1, g0);
            if (null == s) {
                ys = 0;
            } else {
                ys = VerbMath.dotVec(y, s);
            }
            Hy = VerbMath.dot(H1, y);

            // BFGS update on H1
            H1 = VerbMath.sub(
                    VerbMath.add(H1,
                            VerbMath.mul(VerbMath.tensorVec(s, s),
                                    (ys + VerbMath.dotVec(y, Hy)) / (ys * ys))),
                    VerbMath.div(
                            VerbMath.add(VerbMath.tensorVec(Hy, s),
                                    VerbMath.tensorVec(s, Hy)), ys));

            x0 = x1;
            f0 = f1;
            g0 = g1;
            ++it;
        }
        return new OptimizationResult(x0, f0, g0, H1, it, msg);
    }
}
