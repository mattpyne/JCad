package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Topo;
import org.pyne.jcad.core.math.brep.eval.BrepValidator;

/**
 *
 * @author Matthew Pyne
 */
public class Geometry implements IComponent {

    private Topo topo;

    public Topo getTopo() {
        return topo;
    }

    public void setTopo(Topo topo) {
        this.topo = topo;
        if (topo instanceof Shell) {
            Debug.checkLoopsValid((Shell) topo);
        }
    }

    @Override
    public void invalidate() {
        topo = null;
    }

    @Override
    public void update() {
        
    }

    @Override
    public void register(Entity entity) {
    }

}
