package org.pyne.jcad.core.math.geom;

import eu.mihosoft.jcsg.Edge;
import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.vvecmath.Vector3d;
import geomss.geom.*;
import geomss.geom.Vector;
import geomss.geom.nurbs.BasicNurbsSurface;
import geomss.geom.nurbs.SurfaceFactory;
import haxe.lang.EmptyObject;
import haxe.root.Array;
import org.pyne.jcad.core.collection.MapUtils;
import org.pyne.jcad.core.csg.JCSGInterop;
import org.pyne.jcad.core.exe.datastructures.KdPoint;
import org.pyne.jcad.core.exe.datastructures.KdTree;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.eval.NurbsSurfaceFactory;
import verb.core.Interval;
import verb.core.MeshData;

import javax.measure.quantity.Dimensionless;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.pyne.jcad.core.math.CadMath.intersectPoint;

public class MeshSurface extends Surface {

    private List<Polygon> polygons;
    private PlanarSurface projectionSurface;
    private List<List<MeshPoint>> polyMeshPoints;
    private HashSet<MeshPoint> meshPoints;
    private Interval<Number> domainU;
    private Interval<Number> domainV;
    boolean curvedSurf;
    private MeshData meshData;
    private KdTree<MeshPoint> kdMeshPointUVTree;
    private KdTree<Polygon> kdPolygonTree;

    public MeshSurface() {
        this.polygons = new ArrayList<>();
    }

    public MeshSurface(MeshSurface other) {
        this.polygons = other.polygons.stream()
                .map(p -> Polygon.fromPoints(p.getVertices3d()))
                .collect(Collectors.toList());
    }

    public void add(Polygon poly) {
        if (polygons.isEmpty()) {

            projectionSurface = new PlanarSurface(JCSGInterop.pointFrom(poly.getVertices3d()));
        }

        if (poly.getPlane().getNormal().minus(getSimpleSurface().normal(null)).magnitudeSq() > Tolerance.TOLERANCE) {
            curvedSurf = true;
        }
        polygons.add(poly);
    }

    public List<Polygon> getPolygons() {
        return polygons;
    }

    public boolean contains(Polygon poly) {
        return polygons.contains(poly);
    }

    public MeshData tessellate() {

        if (null != meshData) {
            return meshData;
        }

        meshData = new MeshData(EmptyObject.EMPTY);

        meshData.points = new Array<>();
        meshData.uvs = new Array<>();
        meshData.normals = new Array<>();
        meshData.faces = new Array<>();

        int id = 0;
        for (List<MeshPoint> mePts : polyMeshPoints()) {

            if (mePts.isEmpty()) {

                continue;
            }

            Array<Number> faces = new Array<>(new Number[mePts.size()]);
            int faceId = 0;
            Array<Number> normal = new Point3DC(mePts.get(0).getP3OnMesh().parent.getPlane().getNormal()).asArray();

            for (MeshPoint mePt : mePts) {

                meshData.points.push(mePt.getP3OnMesh().asArray());
                meshData.uvs.push(mePt.uv());
                meshData.normals.push(normal.copy());

                faces.set(faceId, id);
                faceId++;
                id++;
            }

            meshData.faces.push(faces);
        }

        return meshData;
    }

    @Override
    public PlanarSurface getSimpleSurface() {
        return projectionSurface;
    }

    public Point3DC getNormal() {
        return getSimpleSurface().normal(null);
    }

    public Point3DC project(Point3DC pt) {

        Array<Number> uv = param(pt);
        return point(uv.get(0).doubleValue(), uv.get(1).doubleValue());
    }

    public Point3DC point(double u, double v) {

        Point3DC pt = getSimpleSurface().point(u, v);
        Point3DC normal = getNormal();

        Collection<Polygon> closestPolygons = kdMeshPointUVTree().nearestNeighbourSearch(3, new KdPoint(u, v))
                .stream()
                .map(kdp -> kdp.get())
                .flatMap(mePt -> Stream.concat(mePt.sharedPoints().stream(), Stream.of(mePt)))
                .map(mePt -> mePt.parent)
                .distinct()
                .collect(Collectors.toList());

        for (Polygon poly : closestPolygons) {
            Vector3d pnormal = poly.getPlane().getNormal();
            Vector3d anchor = poly.getPlane().getAnchor();

            Vector3d transferPointOnPoly = intersectPoint(normal, pt, pnormal, anchor);

            if (poly.contains(transferPointOnPoly)) {

                Point3DC p3 = new Point3DC(transferPointOnPoly);
                p3.parent = poly;
                return p3;
            }
        }

        Polygon closestPolygon = null;
        Vector3d closestPt = null;
        double closest = Double.MAX_VALUE;

        for (Polygon poly : closestPolygons) {
            Vector3d pnormal = poly.getPlane().getNormal();
            Vector3d anchor = poly.getPlane().getAnchor();

            Vector3d transferPointOnPoly = intersectPoint(normal, pt, pnormal, anchor);

            for (Edge edge : poly.edges()) {

                double closestTest = edge.distance(transferPointOnPoly);
                if (closestTest < closest) {
                    closestPolygon = poly;
                    closestPt = transferPointOnPoly;
                    closest = closestTest;
                }
            }
        }

        Point3DC p3 = new Point3DC(closestPt);
        p3.parent = closestPolygon;

        return p3;
    }

    private KdTree<Polygon> kdPolygonTree() {

        if (null != kdPolygonTree) {

            return kdPolygonTree;
        }

        List<KdPoint<Polygon>> kdPoints = new ArrayList<>();
        for (Polygon poly : polygons) {
            Point3DC polygonAnchor = new Point3DC(poly.getPlane().getAnchor());

            Array<Number> uv = param(polygonAnchor);
            kdPoints.add(new KdPoint(uv.get(0).doubleValue(), uv.get(1).doubleValue(), poly));
        }

        kdPolygonTree = new KdTree(kdPoints);

        return kdPolygonTree;
    }

    private KdTree<MeshPoint> kdMeshPointUVTree() {

        if (null != kdMeshPointUVTree) {

            return kdMeshPointUVTree;
        }

        List<KdPoint<MeshPoint>> kdPoints = new ArrayList<>();
        for (MeshPoint mePt : uniqueMeshPoints()) {
            kdPoints.add(new KdPoint(mePt.getU(), mePt.getV(), mePt));
        }

        kdMeshPointUVTree = new KdTree(kdPoints);

        return kdMeshPointUVTree;
    }

    @Override
    public Array<Number> param(Point3DC point) {
        return getSimpleSurface().param(point);
    }

    public Interval<Number> domainU() {
        if (null == domainU) {

            calcDomainUV();
        }

        return domainU;
    }

    public Interval<Number> domainV() {
        if (null == domainV) {

            calcDomainUV();
        }

        return domainV;
    }

    @Override
    public PlanarSurface tangentPlane(double u, double v) {
        return getSimpleSurface();
    }

    @Override
    public List<Curve> intersectSurfaceForSameClass(Surface other, double tol) {
        return toNurbs().intersectSurfaceForSameClass(other.toNurbs(), tol);
    }

    @Override
    public Point3DC normal(Point3DC point) {

        Array<Number> uv = param(point);

        return new Point3DC(getContainingPolygon(uv.get(0).doubleValue(), uv.get(1).doubleValue()).getPlane().getNormal());
    }

    @Override
    public Point3DC normalUV(double u, double v) {
        return new Point3DC(getContainingPolygon(u, v).getPlane().getNormal());
    }

    private void calcDomainUV() {

        double minU = Double.MAX_VALUE;
        double maxU = -Double.MAX_VALUE;

        double minV = Double.MAX_VALUE;
        double maxV = -Double.MAX_VALUE;

        for (List<MeshPoint> meshPoints : polyMeshPoints()) {
            for (MeshPoint meshPoint : meshPoints) {
                if (meshPoint.getU() < minU) {
                    minU = meshPoint.getU();
                }

                if (meshPoint.getU() > maxU) {
                    maxU = meshPoint.getU();
                }

                if (meshPoint.getV() < minV) {
                    minV = meshPoint.getV();
                }

                if (meshPoint.getV() > maxV) {
                    maxV = meshPoint.getV();
                }
            }
        }

        domainU = new Interval<>(minU, maxU);
        domainV = new Interval<>(minV, maxV);
    }

    private Polygon getContainingPolygon(double u, double v) {
        Point3DC pt = getSimpleSurface().point(u, v);
        Point3DC normal = getNormal();

        Collection<Polygon> closestPolygons = kdMeshPointUVTree().nearestNeighbourSearch(3, new KdPoint(u, v))
                .stream()
                .map(kdp -> kdp.get())
                .flatMap(mePt -> Stream.concat(mePt.sharedPoints().stream(), Stream.of(mePt)))
                .map(mePt -> mePt.parent)
                .distinct()
                .collect(Collectors.toList());

        for (Polygon poly : closestPolygons) {
            Vector3d pnormal = poly.getPlane().getNormal();
            Vector3d anchor = poly.getPlane().getAnchor();

            Vector3d transferPointOnPoly = intersectPoint(normal, pt, pnormal, anchor);

            if (poly.contains(transferPointOnPoly)) {

                return poly;
            }
        }

        // Extend the out side the mesh. Find the closest edge.
        Polygon closestPolygon = null;
        double closest = Double.MAX_VALUE;

        for (Polygon poly : closestPolygons) {
            Vector3d pnormal = poly.getPlane().getNormal();
            Vector3d anchor = poly.getPlane().getAnchor();

            Vector3d transferPointOnPoly = intersectPoint(normal, pt, pnormal, anchor);

            for (Edge edge : poly.edges()) {

                double closestTest = edge.distance(transferPointOnPoly);
                if (closestTest < closest) {

                    closestPolygon = poly;
                    closest = closestTest;
                }
            }
        }

        return closestPolygon;
    }

    public boolean contains(Point3DC point) {

        Array<Number> uv = param(point);

        return getContainingPolygon(uv.get(0).doubleValue(), uv.get(1).doubleValue()).contains(point);
    }

    /**
     * Points for the mesh. Grouped based on polygon. Orientation of polygon is preserved.
     *
     * @return
     */
    public List<List<MeshPoint>> polyMeshPoints() {
        if (null == polyMeshPoints) {

            Map<MeshPoint, List<MeshPoint>> sharedMeshPointsMap = new HashMap<>();

            polyMeshPoints = new ArrayList<>();
            for (Polygon poly : polygons) {

                List<MeshPoint> meshPoints = new ArrayList<>();
                for (Vector3d vector3d : poly.getVertices3d()) {

                    MeshPoint mePt = new MeshPoint(vector3d.x(), vector3d.y(), vector3d.z(), this);
                    mePt.parent = poly;
                    MapUtils.addToList(sharedMeshPointsMap, mePt, mePt);
                    meshPoints.add(mePt);
                }
                polyMeshPoints.add(meshPoints);
            }

            for (List<MeshPoint> mePts : polyMeshPoints) {
                for (MeshPoint mePt : mePts) {

                    List<MeshPoint> sharedMeshPoints = new ArrayList<>(sharedMeshPointsMap.get(mePt));
                    sharedMeshPoints.remove(mePt);

                    mePt.setSharedPoint(sharedMeshPoints);
                }
            }
        }

        return polyMeshPoints;
    }

    public HashSet<MeshPoint> uniqueMeshPoints() {

        if (null == meshPoints) {
            List<List<MeshPoint>> polyMeshPoints = polyMeshPoints();

            meshPoints = polyMeshPoints.stream()
                    .flatMap(List::stream)
                    .collect(Collectors.toCollection(HashSet::new));
        }

        return meshPoints;
    }

    public NurbsSurface toNurbs() {

        // Create grid of points add new ones as needed on UV
        HashMap<Integer, List<MeshPoint>> uPoints = new HashMap<>();
        HashMap<Integer, List<MeshPoint>> vPoints = new HashMap<>();

        for (List<MeshPoint> meshPoints : polyMeshPoints()) {
            for (MeshPoint meshPoint : meshPoints) {

                MapUtils.addToList(uPoints, Double.hashCode(CadMath.round(meshPoint.getU(), 6)), meshPoint);
                MapUtils.addToList(vPoints, Double.hashCode(CadMath.round(meshPoint.getV(), 6)), meshPoint);
            }
        }

        int gridUSize = uPoints.size();
        int gridVSize = vPoints.size();
        gridUSize = gridUSize > 50 ? 50 : gridUSize;
        gridVSize = gridVSize > 50 ? 50 : gridVSize;

        double maxU = domainU().max.doubleValue();
        double minU = domainU().min.doubleValue();
        double maxV = domainV().max.doubleValue();
        double minV = domainV().min.doubleValue();
        double uDomainWidth = maxU - minU;
        double vDomainWidth = maxV - minV;
        double uStep = uDomainWidth / (gridUSize - 1);
        double vStep = vDomainWidth / (gridVSize - 1);

        System.out.println("uDomainWidth: " + uDomainWidth);
        System.out.println("vDomainWidth: " + vDomainWidth);
        PointArray<GeomPoint> points = initialParamGrid(gridUSize, gridVSize, minU, minV, uStep, vStep);
        NurbsSurface surf = createNurbsSurface(points, polyMeshPoints().get(0).get(0));

        // Testing if surf fits mesh.
//        for (PointString<GeomPoint> gps : points) {
//            for (GeomPoint gp : gps) {
//
//                Point3D p3 = new Point3D(gp.getX(), gp.getY(), gp.getZ());
//
//                double diffMeshSurf = surf.closestPoint(p3).distance(p3);
//                if (diffMeshSurf > 1e-1) {
//                    System.out.println("Large difference Mesh Surf: " + diffMeshSurf);
//                }
//            }
//        }
//
//        for (List<MeshPoint> mePts : polyMeshPoints()) {
//            for (MeshPoint mePt : mePts) {
//
//                double diffMeshSurf = surf.closestPoint(mePt.getP3OnMesh()).distance(mePt.getP3OnMesh());
//                if (diffMeshSurf > 3e-1) {
//
//                    System.out.println("Large original difference Mesh Surf: " + diffMeshSurf);
//                }
//            }
//        }

        return surf;
    }

    private NurbsSurface createNurbsSurface(PointArray<GeomPoint> points, MeshPoint testPoint) {
        // Fit a Nurbs Surface to the grid
        int degreeS = points.get(0).size() > 3 ? 3 : points.get(0).size() - 1;
        int degreeT = points.size() > 3 ? 3 : points.size() - 1;
//        int ns = points.get(0).size() > 20 ? 20 : points.get(0).size() - 1;
//        int nt = points.size() > 20 ? 20 : points.size() - 1;
        BasicNurbsSurface geomSurf;
//        try {
//
//            geomSurf = SurfaceFactory.approxPoints(degreeS, degreeT, ns, nt, points);
//            System.out.println("approximation successful");
//        } catch (Exception ex) {

        geomSurf = SurfaceFactory.fitPoints(degreeS, degreeT, points);
        System.out.println("fall back to fit");
//        }

        Vector<Dimensionless> norm = geomSurf.getNormal(testPoint.normalizedU(), testPoint.normalizedV());
        Point3DC normP3 = Point3DC.xyz(norm.getValue(0), norm.getValue(1), norm.getValue(2));

        NurbsSurface surf = NurbsSurfaceFactory.from(geomSurf);
        Polygon testPoly = testPoint.getP3OnMesh().parent;
        if (testPoly.getPlane()
                .getNormal().dot(normP3) > 0) {

            surf = surf.invert();
        }

        return surf;
    }

    private PointArray<GeomPoint> initialParamGrid(int gridUSize, int gridVSize,
                                                   double minU, double minV,
                                                   double uStep, double vStep) {

        PointArray<GeomPoint> points = PointArray.newInstance();
        List<PointString<GeomPoint>> tempPointStrings = new ArrayList<>(gridUSize);
        for (int i = 0; i < gridUSize; i++) {

            PointString newPoints = PointString.newInstance();

            double u = minU + uStep * i;
            List<GeomPoint> tempPoints = new ArrayList<>(gridVSize);
            for (int j = 0; j < gridVSize; j++) {

                double v = minV + vStep * j;

                // find point on mesh
                Point3DC p3 = point(u, v);
                tempPoints.add(Point.valueOf(p3.x(), p3.y(), p3.z()));
            }
            newPoints.addAll(tempPoints);
            tempPointStrings.add(newPoints);
        }
        points.addAll(tempPointStrings);

        return points;
    }

    @Override
    public MeshSurface copy() {
        return new MeshSurface(this);
    }

    @Override
    public HashSet<PlanarSurface> getApproxSurface() {
        return null;
    }

}
