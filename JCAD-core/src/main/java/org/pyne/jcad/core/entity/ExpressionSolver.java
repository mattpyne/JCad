package org.pyne.jcad.core.entity;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.PrimitiveElement;

import java.util.ArrayList;
import java.util.List;

public class ExpressionSolver implements IComponent {

    public static ExpressionSolver empty = new ExpressionSolver();

    List<Argument> arguments = new ArrayList<>();

    Entity entity;

    @Override
    public void invalidate() {
        if (null == entity) {
            return;
        }

    }

    @Override
    public void update() {

    }

    @Override
    public void register(Entity entity) {
        this.entity = entity;
    }

    public double eval(String value) {
        return new Expression(value, arguments.toArray(new PrimitiveElement[0])).calculate();
    }

    public boolean checkSyntax(String value) {
        return new Expression(value, arguments.toArray(new PrimitiveElement[0])).checkSyntax();
    }
}
