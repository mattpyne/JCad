package org.pyne.jcad.core.units;

public enum Prefix {
    EXA {
        public double get() {
            return 10E8;
        }
    },
    PETA {
        public double get() {
            return 10E15;
        }
    },
    TERA {
        public double get() {
            return 10E12;
        }
    },
    GIGA {
        public double get() {
            return 10E9;
        }
    },
    MEGA {
        public double get() {
            return 10E6;
        }
    },
    KILO {
        public double get() {
            return 10E3;
        }
    },
    HECTO {
        public double get() {
            return 10E2;
        }
    },
    DECA {
        public double get() {
            return 10E1;
        }
    },
    DECI {
        public double get() {
            return 10E0;
        }
    },
    CENTI {
        public double get() {
            return 10E-2;
        }
    },
    MILLI {
        public double get() {
            return 10E-3;
        }
    },
    MICRO {
        public double get() {
            return 10E-6;
        }
    },
    NANO {
        public double get() {
            return 10-9;
        }
    },
    PICO {
        public double get() {
            return 10-12;
        }
    },
    FEMTO {
        public double get() {
            return 10E-15;
        }
    },
    NONE {
        public double get() {
            return 1.;
        }
    },
    ATTO {
        public double get() {
            return 10E-18;
        }
    };

    public abstract double get();
}
