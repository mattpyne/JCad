package org.pyne.jcad.core.csg;

import eu.mihosoft.jcsg.CSG;
import eu.mihosoft.jcsg.Polygon;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.pyne.jcad.core.math.brep.CompoundShell;
import org.pyne.jcad.core.math.brep.Shell;

import org.pyne.jcad.core.math.brep.Topo;
import org.pyne.jcad.core.math.brep.creators.BrepTessellator;

/**
 *
 * @author Matthew Pyne
 */
public class BrepToCSG {

    public CSG fromBrep(Topo topo) {

        if (topo instanceof Shell) {

            return from((Shell) topo);
        } else if (topo instanceof CompoundShell) {

            return from((CompoundShell) topo);
        }

        return CSG.emptyCSG();
    }

    private CSG from(Shell shell) {

        return CSG.fromPolygons(polygons(shell));
    }

    private static List<Polygon> polygons(Shell shell) {
        return streamPolygons(shell)
                .collect(Collectors.toList());
    }

    private static Stream<Polygon> streamPolygons(Shell shell) {
        return BrepTessellator.tessellate(shell)
                .stream()
                .map(Polygon::fromPoints);
    }

    private CSG from(CompoundShell compoundShell) {

        return CSG.fromPolygons(compoundShell.shells()
                .stream()
                .flatMap(shell -> streamPolygons(shell))
                .collect(Collectors.toList()));
    }
}
