package org.pyne.jcad.core.math.brep.eval.bool;

import java.util.ArrayList;
import org.pyne.jcad.core.math.brep.HalfEdge;

/**
 *
 * @author Matthew Pyne
 */
public class DecayedHalfEdges extends ArrayList<HalfEdge> {
    
    public DecayedHalfEdges(HalfEdge he0, HalfEdge he1) {
        add(he0);
        add(he1);
    }
}
