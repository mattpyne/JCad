package org.pyne.jcad.core.exe;

import java.util.function.Function;

public class ArrayVisitor {

    /**
     * Loop around the i,j in array
     *
     * @param iCol
     * @param jRow
     * @param array
     * @param strategy a function to run on each and if returns true will stop traversing
     */
    public static <T> void traverseRadially(int iCol, int jRow, T[][] array, Function<T, Boolean> strategy) {

        int maxD = array.length > array[0].length ? array.length : array[0].length;

        for (int k = 1; k < maxD; k++) {

            // start corner for the kth loop around i, j
            int y = jRow + k;
            int x = iCol + k;
            // segment length
            int k2 = k * 2;

            // Go across the left
            for (int l = 0; l <= k2; l++) {
                if (checkAndApply(x - l, y, array, strategy)) return;
            }

            // Go down top
            for (int l = 1; l <= k2; l++) {
                if (checkAndApply(x - k2, y - l, array, strategy)) return;
            }

            // Go across right
            for (int l = k2 - 1; l >= 0; l--) {
                if (checkAndApply(x - l, y - k2, array, strategy)) return;
            }

            // Go down bottom
            for (int l = k2 - 1; l >= 1; l--) {
                if (checkAndApply(x, y - l, array, strategy)) return;
            }
        }
    }

    private static <T> boolean checkAndApply(int i, int j, T[][] array, Function<T, Boolean> strategy) {
        if (validIndex(i, j, array)) {

            if (strategy.apply(array[i][j])) {

                return true;
            }
        }
        return false;
    }

    public static boolean validIndex(int i, int j, Object[][] array) {
        return array.length > i && i >= 0 && array[0].length > j && j >= 0;
    }

    public static void main(String[] args) {

        Number[][] array =
                {       {1, 2,    3,   11,  22,    33},
                        {4, 5,    6,   44,  55,    66},
                        {7, 8,    9,   77,  88,    99},
                        {10, 11, 12, 1010, 1111, 1212}};

        System.out.println(array[1][1]);
        System.out.println("=====================");
        traverseRadially(1, 2, array, (x) -> {
            System.out.println(x);
            return false;
        });
    }

}
