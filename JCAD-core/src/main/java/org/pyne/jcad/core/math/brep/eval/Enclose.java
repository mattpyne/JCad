package org.pyne.jcad.core.math.brep.eval;

import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.math.geom.Tolerance;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Vertex;

import static org.pyne.jcad.core.math.VecMath.leftTurningMeasure;

/**
 *
 * @author Matthew Pyne
 */
public class Enclose {

    public static enum ENCLOSE_CLASSIFICATION {
        UNDEFINED,
        ENTERS,
        LEAVES,
        TANGENTS,
    };

    public HalfEdge curr;
    public HalfEdge next;
    public Vertex currVertex2;

    public Enclose() {
    }

    public static ENCLOSE_CLASSIFICATION isCurveEntersEnclose(ParametricCurve curve, HalfEdge a, HalfEdge b) {
        Point3DC pt = a.vertexB().point();
        Point3DC normal = a.loop().face().surface().normal(pt);

        Point3DC testee = curve.tangentAtPoint(pt);
        Point3DC inVec = a.tangentAtEnd();
        Point3DC outVec = b.tangentAtStart();

        boolean coiIn = Tolerance.veqNeg(inVec, testee);
        boolean coiOut = Tolerance.veq(outVec, testee);

        if (coiIn && coiOut) {
            return ENCLOSE_CLASSIFICATION.UNDEFINED;
        }

        Point3DC testeeNeg = testee.negated();
        
        boolean coiInNeg = Tolerance.veqNeg(inVec, testeeNeg);
        boolean coiOutNeg = Tolerance.veq(outVec, testeeNeg);

        if (coiInNeg || coiOutNeg) {
            return ENCLOSE_CLASSIFICATION.UNDEFINED;
        }

        ENCLOSE_CLASSIFICATION result;
        if (coiIn || coiOut) {
            
            boolean insideEncloseNeg = isInsideEnclose(normal, testeeNeg, inVec, outVec);
            return insideEncloseNeg ? ENCLOSE_CLASSIFICATION.LEAVES : ENCLOSE_CLASSIFICATION.ENTERS;
        } else {
            
            boolean insideEnclose = isInsideEnclose(normal, testee, inVec, outVec);
            boolean insideEncloseNeg = isInsideEnclose(normal, testeeNeg, inVec, outVec);
            
            if (insideEnclose == insideEncloseNeg) {
                
                result = ENCLOSE_CLASSIFICATION.TANGENTS;
            } else {
                
                result = insideEnclose ? ENCLOSE_CLASSIFICATION.ENTERS : ENCLOSE_CLASSIFICATION.LEAVES;
            }
        }
        return result;
    }

    public static boolean isInsideEnclose(Point3DC normal, Point3DC testee, Point3DC inVec, Point3DC outVec) {
        Point3DC pivot = inVec.negated();

        double enclosureAngle = leftTurningMeasure(pivot, outVec, normal);
        double testeeAngle = leftTurningMeasure(pivot, testee, normal);
        return testeeAngle < enclosureAngle;
    }
}
