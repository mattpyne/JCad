package org.pyne.jcad.core.math.geom;

/**
 *
 * @author matt pyne
 */
public abstract class Curve extends ParametricCurve {
    
    public Curve(TYPE type) {
        super(type);
    }
}
