package org.pyne.jcad.core.verbinterop;

import haxe.root.Array;
import java.util.List;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.ParametricCurve;

/**
 *
 * @author Matthew Pyne
 */
public class VerbInterop {

    /**
     * Type of verb curve.
     * 
     * @param verbCurve
     * @return 
     */
    public static ParametricCurve.TYPE getCurveType(verb.geom.NurbsCurve verbCurve) {

        if (verbCurve instanceof verb.geom.Line
                 || verbCurve.degree() == 1) {
            
            return ParametricCurve.TYPE.LINE;

        } else if (verbCurve instanceof verb.geom.Circle) {

            return ParametricCurve.TYPE.CIRCLE;

        } else if (verbCurve instanceof verb.geom.Arc) {

            return ParametricCurve.TYPE.ARC;

        } else if (verbCurve instanceof verb.geom.BezierCurve) {
            
            return ParametricCurve.TYPE.BEZIER;
            
        } else if (verbCurve instanceof verb.geom.Ellipse) {
            
            return ParametricCurve.TYPE.ELLIPSE;
            
        }

        return ParametricCurve.TYPE.NURBS;
    }
    
    /**
     * Type of verb curve.
     * 
     * @param surface
     * @param useV
     * @return 
     */
    public static ParametricCurve.TYPE getIsoCurveType(verb.geom.NurbsSurface surface, boolean useV) {

        if ((surface.degreeU() == 1 && !useV)
                || (surface.degreeV() == 1 && useV)) {
            
            return ParametricCurve.TYPE.LINE;
            
        } else if ((surface instanceof verb.geom.ConicalSurface || surface instanceof verb.geom.CylindricalSurface)
                && useV) {
            
            return ParametricCurve.TYPE.ARC;
            
        } 
        else if (surface instanceof verb.geom.SphericalSurface) {
            
            return ParametricCurve.TYPE.CIRCLE;
            
        } 
//        else if (surface instanceof verb.geom.) {
//            
//            return ParametricCurve.TYPE.BEZIER;
//            
//        } 
//        else if (surface instanceof verb.geom.Ellipse) {
//            
//            return ParametricCurve.TYPE.ELLIPSE;
//            
//        }

        return ParametricCurve.TYPE.NURBS;
    }
    
    /**
     * Creates a Haxe Array out of list of Point3D.
     * 
     * @param points
     * @return 
     */
    public static Array<Array<Number>> points(List<Point3DC> points) {
        Array verbPoints = new Array(new Array[points.size()]);

        for (int i = 0; i < points.size(); i++) {
            verbPoints.set(i, points.get(i).asArray());
        }

        return verbPoints;
    }
}
