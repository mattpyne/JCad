package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.math.brep.Topo;

/**
 *
 * @author Matthew Pyne
 */
public class EntityFactory {

    public Entity createEmpty() {
        Entity entity = new Entity();

        entity.addComponent(new Connections());
        entity.addComponent(new Material());
        entity.addComponent(new Identification());
        
        entity.addComponent(new Transformation());

        entity.addComponent(new Constructor());
        entity.addComponent(new Geometry());
        entity.addComponent(new Mesh());
        entity.addComponent(new Boundary());
        entity.addComponent(new Sketches());
        entity.addComponent(new ExpressionSolver());

        return entity;
    }
    
    public Entity create(Topo topo) {
        Entity entity = new Entity();

        entity.addComponent(new Connections());
        entity.addComponent(new Material());
        entity.addComponent(new Identification());
        
        entity.addComponent(new Transformation());
       
        Constructor contructor = new Constructor();
        contructor.add(new BaseOperation(topo));
        entity.addComponent(contructor);
        entity.addComponent(new Geometry());
        entity.addComponent(new Mesh());
        entity.addComponent(new Boundary());
        entity.addComponent(new Sketches());
        entity.addComponent(new ExpressionSolver());

        return entity;
    }
}
