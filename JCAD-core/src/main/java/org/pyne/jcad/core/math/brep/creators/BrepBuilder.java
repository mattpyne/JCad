package org.pyne.jcad.core.math.brep.creators;

import eu.mihosoft.vvecmath.Vector3d;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.math.geom.PlanarSurface;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.BrepCurve;
import eu.mihosoft.jcsg.Polygon;
import haxe.root.Array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.pyne.jcad.core.math.BBox;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Vertex;
import verb.core.Vec;

/**
 * @author matt pyne
 */
public class BrepBuilder {

    private Shell shell;
    public Face face;
    public Loop loop;

    private VertexFactory vertFactory;

    public BrepBuilder() {
        shell = new Shell();
        vertFactory = new VertexFactory();
    }

    public BrepBuilder face(BrepSurface surface) {
        face = new Face(surface);
        shell.addFace(face);
        loop = null;
        return this;
    }

    public BrepBuilder loop(Vertex... vertices) {
        return loop(Arrays.asList(vertices));
    }

    public BrepBuilder loop(List<Vertex> vertices) {

        vertices = vertices.stream()
                .map(this::getWorkingVertex)
                .collect(Collectors.toList());

        newLoop();
        for (int i = 0; i < vertices.size(); ++i) {

            internalEdge(vertices.get(i), vertices.get((i + 1) % vertices.size()), null);
        }
        return this;
    }

    public BrepBuilder newOuterLoop() {
        if (null != loop) {
            loop.link();
        }
        loop = face.getOuterLoop();

        return this;
    }

    public BrepBuilder newInnerLoop() {
        if (null != loop) {
            loop.link();
        }
        loop = new Loop();
        face.addInnerLoop(loop);
        loop.setFace(face);

        return this;
    }

    public BrepBuilder newLoop() {

        if (null != loop) {
            loop.link();
        }

        if (loop == null) {
            loop = face.getOuterLoop();
        } else {
            loop = new Loop();
            face.addInnerLoop(loop);
        }
        loop.setFace(face);

        return this;
    }

    public BrepBuilder edge(Vertex a, Vertex b, BrepCurve curve) {
        a = getWorkingVertex(a);
        b = getWorkingVertex(b);

        internalEdge(a, b, curve);

        return this;
    }

    private Vertex getWorkingVertex(Vertex a) {
        Point3DC point = a.point();
        a = vertFactory.find(a);

        if (null == a) {

            a = vertFactory.create(point);
        }
        return a;
    }

    private BrepBuilder internalEdge(Vertex a, Vertex b, BrepCurve curve) {
        BrepCurve finalCurve = curve;
        Optional<HalfEdge> opHe = a.edgeFor(b)
                .filter(halfEdge -> {
                    return null == halfEdge.loop()
                            && halfEdge.edge().curve().getType() == finalCurve.getType();
                }).findFirst();
        if (opHe.isEmpty()) {

            if (null == curve) {
                curve = new BrepCurve(CurvePrimitives.line(a.point(), b.point()));
            } else if (!curve.passesThrough(a.point()) || !curve.passesThrough(b.point())) {
                if (curve.getType() != ParametricCurve.TYPE.LINE) {
                    Debug.log(Debug.LEVEL.ERROR, "Vertex not passing through edge but not a line what do I do");
                } else {
                    curve = new BrepCurve(CurvePrimitives.line(a.point(), b.point()));
                }
            }

            Edge edge = new Edge(curve, a, b);
            if (!loop.halfEdges().isEmpty()) {
                addEdgeToLoopWithOrientationCheck(edge);
            } else {
                loop.addEdge(edge.halfEdge1());
            }

        } else {
            if (!loop.halfEdges().isEmpty()) {
                addEdgeToLoopWithOrientationCheck(opHe.get());
            } else {
                loop.addEdge(opHe.get());
            }

        }
        return this;
    }

    private void addEdgeToLoopWithOrientationCheck(Edge edge) {
        if (edge.halfEdge1().getPos1().equals(loop.halfEdges().get(loop.halfEdges().size() - 1).getPos2())) {
            loop.addEdge(edge.halfEdge1());
        } else if (edge.halfEdge2().getPos1().equals(loop.halfEdges().get(loop.halfEdges().size() - 1).getPos2())) {
            loop.addEdge(edge.halfEdge2());
        } else {
            if (edge.halfEdge1().getPos2().equals(loop.halfEdges().get(0).getPos1())) {
                loop.addEdgeToFront(edge.halfEdge1());
            } else if (edge.halfEdge2().getPos2().equals(loop.halfEdges().get(0).getPos1())) {
                loop.addEdgeToFront(edge.halfEdge2());
            }
        }
    }

    private void addEdgeToLoopWithOrientationCheck(HalfEdge edge) {
        if (edge.getPos1().equals(loop.halfEdges().get(loop.halfEdges().size() - 1).getPos2())) {
            loop.addEdge(edge);
        } else {
            loop.addEdgeToFront(edge);
        }
    }

    public BrepBuilder edgeTrim(Vertex a, Vertex b, BrepCurve curve) {
        double u1 = curve.param(a.point());
        curve = curve.split(u1).get(1);
        double u2 = curve.param(b.point());
        curve = curve.split(u2).get(0);
        internalEdge(a, b, curve);
        return this;
    }

    public Face buildFace() {
        return face;
    }

    public Shell build() {
        for (Face face : shell.faces()) {
            for (Loop loop : face.loops()) {
                loop.link();
            }
            if (face.surface() == null) {
                face.setSurface(createBoundingNurbs(face.getOuterLoop().tessellate(), null));
            }
        }
        for (Face face : shell.faces()) {
            for (HalfEdge he : face.halfEdges()) {
                HalfEdge twin = he.twin();
                if (twin.loop() == null) {
                    Face nullFace = new Face(face.surface());
                    nullFace.getOuterLoop().addEdge(twin);
                    nullFace.getOuterLoop().link();
                }
            }
        }
        return shell;
    }

    public static BrepSurface createBoundingNurbs(PlanarSurface plane, Point3DC... points) {
        return createBoundingNurbs(Arrays.asList(points), plane);
    }

    public static BrepSurface createBoundingNurbs(List<Point3DC> points, PlanarSurface plane) {
        if (null == plane) {

            plane = new PlanarSurface(points);
        }

        List<Point3DC> points2d = new ArrayList<>();
        for (Point3DC point : points) {
            points2d.add(new Point3DC(plane.param(point)));
        }

        BBox bBox = new BBox();
        points2d.forEach(p -> bBox.checkPoint(p));

        Polygon polygon = bBox.toPolygon();
        List<Point3DC> polygonByPoints = new ArrayList<>();
        for (Vector3d pos : polygon.getVertices3d()) {

            polygonByPoints.add(plane.point(pos.x(), pos.y()));
        }

        Array<Array<Array<Number>>> controlPoints = new Array();
        Array<Number> cP3 = polygonByPoints.get(3).asArray();
        Array<Number> cP2 = polygonByPoints.get(2).asArray();
        Array<Number> cP0 = polygonByPoints.get(0).asArray();
        Array<Number> cP1 = polygonByPoints.get(1).asArray();
        Array<Array<Number>> cpUs = new Array();
        cpUs.push(cP3);
        cpUs.push(cP2);
        Array<Array<Number>> cpVs = new Array();
        cpVs.push(cP0);
        cpVs.push(cP1);
        controlPoints.push(cpUs);
        controlPoints.push(cpVs);

        Array<Number> knotsU = new Array<>(new Number[]{0, 0, 1, 1});
        Array<Number> knotsV = new Array<>(new Number[]{0, 0, 1, 1});

        verb.geom.NurbsSurface planeNurbs = verb.geom.NurbsSurface.byKnotsControlPointsWeights(1, 1,
                knotsU,
                knotsV,
                controlPoints,
                null);

        if (Vec.dot(planeNurbs.normal(0.5, 0.5), plane.normal(null).asArray()) < 0.9) {

            return new BrepSurface(new NurbsSurface(planeNurbs, true, null));
        }

        return new BrepSurface(new NurbsSurface(planeNurbs));
    }
}
