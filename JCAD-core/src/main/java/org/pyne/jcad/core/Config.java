package org.pyne.jcad.core;

import org.pyne.jcad.core.units.*;

public class Config {

    public static LengthUnit LENGTH_UNIT = new LengthUnit(Prefix.NONE, UnitName.METRE);
    public static PlaneAngleUnit PLANE_ANGLE_UNIT = new PlaneAngleUnit(Prefix.NONE, UnitName.RADIAN);
    public static SolidAngleUnit SOLID_ANGLE_UNIT = new SolidAngleUnit(Prefix.NONE, UnitName.RADIAN);

}
