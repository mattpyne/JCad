package org.pyne.jcad.core.math.brep.eval;

/**
 *
 * @author Matthew
 */
import java.util.ArrayList;
import java.util.List;

import org.pyne.jcad.core.math.geom.PlanarSurface;
import org.pyne.jcad.core.math.geom.Surface;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Shell;

public class OverLappingFaces {

    public List<Face> groupA = new ArrayList();
    public List<Face> groupB = new ArrayList();

    public static List<OverLappingFaces> findOverlappingFaces(Shell shellA, Shell shellB) {
        List<OverLappingFaces> overlapGroups = new ArrayList();
        for (Face faceA : shellA.faces()) {
            for (Face faceB : shellB.faces()) {
                if (overlaps(faceA, faceB)) {
                    OverLappingFaces group = overlapGroups.stream()
                            .filter(g -> g.groupA.contains(faceA) || g.groupB.contains(faceB))
                            .findAny()
                            .orElse(null);
                    if (null == group) {
                        group = new OverLappingFaces();
                        overlapGroups.add(group);
                    }
                    group.groupA.add(faceA);
                    group.groupB.add(faceB);
                    faceA.op.overlaps = group.groupB;
                    faceB.op.overlaps = group.groupA;
                }
            }
        }
        return overlapGroups;
    }

    public static boolean overlapsImpl(Face face1, Face face2) {
        for (HalfEdge e1 : face1.halfEdges()) {
            if (PointOnFace.point(face2, e1.vertexA().point())) {
                return true;
            }
        }
        return false;
    }

    public static boolean overlaps(Face face1, Face face2) {
        Surface ss1 = face1.surface().getSimpleSurface();
        Surface ss2 = face2.surface().getSimpleSurface();

        if (ss2 != null && ss2 != null
                && ss1 instanceof PlanarSurface
                && ss2 instanceof PlanarSurface
                && ((PlanarSurface) ss1).coplanarUnsigned((PlanarSurface) ss2)) {

            return overlapsImpl(face1, face2) || overlapsImpl(face2, face1); // face1.bounds().intersects(face2.bounds(), -Tolerance.TOLERANCE);
        }
        return false;
    }
}
