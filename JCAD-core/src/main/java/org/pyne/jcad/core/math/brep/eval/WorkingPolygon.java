package org.pyne.jcad.core.math.brep.eval;

import java.util.ArrayList;
import java.util.List;
import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author Matthew
 */
public class WorkingPolygon {

    public List<Point3DC> outer = new ArrayList();
    public List<List<Point3DC>> inners = new ArrayList();
}
