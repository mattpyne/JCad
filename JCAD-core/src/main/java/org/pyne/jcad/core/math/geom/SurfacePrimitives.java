package org.pyne.jcad.core.math.geom;

import haxe.root.Array;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.BrepSurface;
import verb.geom.CylindricalSurface;
import verb.geom.RevolvedSurface;

/**
 * @author Matthew Pyne
 */
public class SurfacePrimitives {

    /**
     * @param height
     * @param radius
     * @return
     */
    public static NurbsSurface cylindrical(double height, double radius) {
        verb.geom.NurbsSurface cylindrical = new CylindricalSurface(
                Point3DC.Z_ONE.asArray(),                                        // axis
                Point3DC.X_ONE.asArray(),                                        // xaxis
                Point3DC.ZERO.asArray(),                                         // base
                height,
                radius);

        return new NurbsSurface(cylindrical);
    }

    public static NurbsSurface linearExtrusion(ParametricCurve sweptCurve, Point3DC extrusionVector) {

        ParametricCurve translatedCurve = sweptCurve.transform(
                Matrix3.createTranslation(extrusionVector));

        return loft(sweptCurve, translatedCurve);
    }

    public static NurbsSurface loft(ParametricCurve curve1, ParametricCurve curve2) {
        return new NurbsSurface(verb.geom.NurbsSurface.byLoftingCurves(
                new Array(new verb.geom.NurbsCurve[]{
                        curve1.asNurbs().getVerb(),
                        curve2.asNurbs().getVerb()}),
                1));
    }

    public static NurbsSurface revolution(ParametricCurve curve, Point3DC center, Point3DC axis, double angle) {
        return new NurbsSurface(
                new RevolvedSurface(
                        curve.asNurbs().getVerb(),
                        center.asArray(),
                        axis.asArray(),
                        angle
                ));
    }
}
