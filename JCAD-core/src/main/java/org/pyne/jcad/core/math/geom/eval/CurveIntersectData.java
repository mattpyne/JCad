package org.pyne.jcad.core.math.geom.eval;

import haxe.root.Array;

/**
 *
 * @author Matthew Pyne
 */
public class CurveIntersectData {

    public Number u0;
    public Number u1;
    public Array<Number> p0;
    public Array<Number> p1;

    public CurveIntersectData(Number u0, Number u1, Array<Number> point1, Array<Number> point2) {
        this.u0 = u0;
        this.u1 = u1;
        this.p0 = point1;
        this.p1 = point2;
    }

    public CurveIntersectData copy() {
        return new CurveIntersectData(u0.doubleValue(), u1.doubleValue(), p1.copy(), p0.copy());
    }

    @Override
    public String toString() {
        return "CurveIntersectData{" + "u0=" + u0 + ", u1=" + u1 + ", p0=" + p0 + ", p1=" + p1 + '}';
    }

}
