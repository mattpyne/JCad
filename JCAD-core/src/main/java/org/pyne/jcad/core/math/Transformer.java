package org.pyne.jcad.core.math;

import org.pyne.jcad.core.math.brep.Topo;

/**
 *
 * @author Matthew Pyne
 */
public class Transformer {

    /**
     *
     * @param <T>
     * @param topo
     * @param x
     * @param y
     * @param z
     * @return
     */
    public <T extends Topo> T translate(T topo, double x, double y, double z) {

        Matrix3 transform = new Matrix3().translate(x, y, z);

        topo.transform(transform);

        return topo;
    }

    /**
     *
     * @param <T>
     * @param topo
     * @param pivot
     * @param axis
     *
     * @param angle angle in rad
     * @return
     */
    public <T extends Topo> T rotate(T topo,
            Point3DC pivot,
            Point3DC axis,
            double angle) {

        Matrix3 transform = Matrix3.createRotation(angle, axis, pivot);

        topo.transform(transform);

        return topo;
    }

    /**
     * rotate around origin (0, 0, 0)
     *
     * @param <T>
     * @param topo
     * @param axis
     * @param angle angle in rad
     * @return
     */
    public <T extends Topo> T rotate(T topo,
            Point3DC axis,
            double angle) {

        Matrix3 transform = Matrix3.createRotation(angle,
                axis,
                Point3DC.ZERO);

        topo.transform(transform);

        return topo;
    }

}
