package org.pyne.jcad.core.math;

/**
 *
 * @author Matthew Pyne
 */
public class Transform {

    private Matrix3 mat;

    public Transform() {
        this.mat = new Matrix3();
    }

    public Transform apply(Transform trans) {
        if (null == trans) {
            return this;
        }

        this.mat.combine(trans.mat);
        return this;
    }

    public double[] toArray1D() {
        return mat.toArray1D();
    }

    public double[][] toArray() {
        return mat.toArray();
    }

    public static Matrix3 translate(double dx, double dy, double dz) {
        return new Matrix3().translate(dx, dy, dz);
    }
    
    public static Matrix3 translate(Point3DC dir) {
        return new Matrix3().translate(dir.x(), dir.y(), dir.z());
    }

    public static Matrix3 rotation(double rad, double x, double y, double z) {
        return Matrix3.createRotation(rad, Point3DC.xyz(x, y, z), Point3DC.ZERO);
    }
}
