package org.pyne.jcad.core.math.geom;

import eu.mihosoft.vvecmath.Vector3d;
import haxe.lang.EmptyObject;
import haxe.root.Array;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import verb.core.BoundingBox;
import verb.core.Interval;

/**
 *
 * @author matt pyne
 */
public class Line extends Curve {

    private Point3DC point;
    private Point3DC dir;
    private HashMap<Surface, Point3DC> pointsCache;

    public Line(Vector3d point, Vector3d dir) {
        super(TYPE.LINE);
        this.point = new Point3DC(point);
        this.dir = new Point3DC(dir);
        this.pointsCache = new HashMap();
    }

    @Override
    public boolean intersectsSurface(Surface surface) {
        return !pointsOfSurfaceIntersection(surface).isEmpty();
    }

    @Override
    public double param(Point3DC point) {
        return point.minus(this.point).dot(this.dir);
    }

    @Override
    public Point3DC point(double t) {
        return point.plus(dir.times(t));
    }

    @Override
    public Set<Point3DC> pointsOfSurfaceIntersection(Surface surface) {

        Set<Point3DC> intersectionPoints = new HashSet<>();

        // Run through the approximate check with planar surfaces.
        // TODO: add more surfaces but if possible calculate exact
        // values.
        for (PlanarSurface surf : surface.getApproxSurface()) {

            // Check cache
            if (pointsCache.containsKey(surf)) {
                Point3DC potentialIntersection = pointsCache.get(surf);
                if (null != potentialIntersection) {
                    intersectionPoints.add(potentialIntersection);
                }
                continue;
            }

            // If not found in cache calc and cache
            Point3DC surfNormal = surf.normal(null);
            Point3DC s0 = surfNormal.times(surf.w());

            double u = surfNormal.dot(s0.minus(point)) / surfNormal.dot(this.dir);

            Point3DC potentialIntersection = point(u);

            // check if the interseciton point is bounded by the surf.
            if (surf.contains(potentialIntersection)) {

                intersectionPoints.add(potentialIntersection);
                pointsCache.put(surf, potentialIntersection);
            } else {

                pointsCache.put(surf, null);
            }
        }

        return intersectionPoints;
    }

    @Override
    public Line getTranslated(Point3DC vector) {
        return new Line(point.plus(vector), dir.clone());
    }

    /**
     *
     * @param plane1
     * @param plane2
     * @return
     */
    public static Optional<Line> fromTwoPlanesIntersection(
            PlanarSurface plane1,
            PlanarSurface plane2) {

        Vector3d n1 = plane1.normal(null);
        Vector3d n2 = plane2.normal(null);

        // Planes are parrell.. no intersection possible.
        if (Math.abs(n1.dot(n2)) < CadMath.TOLERANCE) {

            return Optional.empty();
        }

        Vector3d v = n1.crossed(n2).normalized();
        ParametricPlane pf1 = plane1.toParametricForm();
        ParametricPlane pf2 = plane2.toParametricForm();
        Vector3d r0diff = pf1.getR0().minus(pf2.getR0());
        Vector3d ww = r0diff.minus(n2.times(r0diff.dot(n2)));
        Vector3d p0 = pf2.getR0().plus(ww.times(n1.dot(r0diff) / n1.dot(ww)));

        return Optional.of(new Line(p0, v));
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Optional<Line> fromSegment(Vector3d a, Vector3d b) {

        Vector3d dir = a.minus(b);

        // Same segments.
        if (Math.abs(dir.magnitudeSq()) < CadMath.TOLERANCE) {

            return Optional.empty();
        }

        return Optional.of(new Line(a, dir.normalized()));
    }

    @Override
    public Interval<Number> domain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int degree() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Array<Number> degree1Tess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int optimalSplits() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void normalizeParametrization() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Line invert() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Line transform(Matrix3 tr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Array<Array<Number>> eval(double u, int num) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ParametricCurve> split(Point3DC point) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public BoundingBox bounds() {
        return cache().get(
                Cache.BOUNDING_BOX, 
                () -> new BoundingBox(EmptyObject.EMPTY)
                        .add(point(domain().min.doubleValue()).asArray())
                        .add(point(domain().max.doubleValue()).asArray()));
    }

    @Override
    public void translated(Point3DC vector) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
