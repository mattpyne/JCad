package org.pyne.jcad.core.math.brep.eval;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.geom.Tolerance;

/**
 * Used to clean up a BREP model in order to make it valid.
 *
 * @author Matthew Pyne
 */
public class BrepReconstructer {

    /**
     * Will connect all face edges and loops into a proper shell.
     *
     * @param loops
     */
    public static void reassemble(Stream<Loop> loops) {
        ArrayList<Loop> loopBucket = new ArrayList<>();

        loops.forEach(loop -> addLoopToReassemble(loopBucket, loop));
    }

    /**
     * Will connect all face edges and loops into a proper shell.
     *
     * @param shell
     * @return
     */
    public static Shell reassemble(Shell shell) {
        Shell reassembledShell = new Shell();

        shell.faces().forEach(face -> addFaceToReassemble(reassembledShell, face));

        return reassembledShell;
    }

    public static void addFaceToReassemble(Shell reassemble, Face face) {
        List<HalfEdge> hes = face.halfEdges();

        for (Face shellFace : reassemble.faces()) {

            if (!shellFace.bounds().intersects(face.bounds(), Tolerance.EPSILON)) {
                continue;
            }

            for (HalfEdge shellHe : shellFace.halfEdges()) {
                for (HalfEdge he : hes) {

                    if (!shellHe.bounds().intersects(he.bounds(), Tolerance.EPSILON)
                            || !HalfEdge.isSameEdge(he, shellHe)) {

                        continue;
                    }

                    Edge edge = he.edge();
                    shellHe.setEdge(edge);

                    if (edge.halfEdge1() == he) {

                        edge.setHalfEdge2(shellHe);
                    } else {

                        edge.setHalfEdge1(shellHe);
                    }

                    if (!he.oppositeDirection(shellHe)) {

                        shellHe.loop().reverse();
                    }
                    shellHe.loop().link();
                    he.loop().link();
                }
            }
        }

        reassemble.addFace(face);
    }

    private static void addLoopToReassemble(ArrayList<Loop> loopBucket, Loop loop) {

        for (Loop bucketLoop : loopBucket) {

            if (!bucketLoop.bounds().intersects(loop.bounds(), Tolerance.EPSILON)) {
                continue;
            }

            tryLinkingLoops(bucketLoop, loop);
        }

        loopBucket.add(loop);
    }

    protected static void tryLinkingLoops(Loop loop0, Loop loop1) {
        for (HalfEdge he0 : loop0.halfEdges()) {
            for (HalfEdge he1 : loop1.halfEdges()) {
                
                if (!he0.bounds().intersects(he1.bounds(), Tolerance.EPSILON)
                        || !HalfEdge.isSameEdge(he1, he0)) {
                    
                    continue;
                }
                
                Edge edge = he1.edge();
                he0.setEdge(edge);
                
                if (edge.halfEdge1() == he1) {
                    
                    edge.setHalfEdge2(he0);
                } else {
                    
                    edge.setHalfEdge1(he0);
                }
                
                if (!he1.oppositeDirection(he0)) {
                    
                    he0.loop().reverse();
                }
                he0.loop().link();
                he1.loop().link();
            }
        }
    }

}
