package org.pyne.jcad.core.entity;

import java.util.ArrayList;
import org.pyne.jcad.core.math.Transform;

/**
 *
 * @author Matthew Pyne
 */
public class Connections extends ArrayList<Connection> implements IComponent {

    private Connection connection;

    public Connections() {
    }

    public Connections copy() {
//        Connections copy = new Connections();
//        
//        copy.connection = connection.copy();
        
        return this;
    }
    
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void invalidate() {
        clear();
    }

    @Override
    public void update() {
    }

    public void add(Entity entity) {
        entity.getComponent(Connections.class)
                .map(Connections::getConnection)
                .ifPresent(con -> {
                    con.setParent(connection.getChild());
                    add(con);
                });
    }

    public Transform transformsToTop() {
                
        Entity parent = connection.getParent();
        if (null != parent) {
            
            Transform parentTransform = parent.getComponent(Connections.class)
                    .map(Connections::transformsToTop)
                    .orElse(null);
            
            if (null != parentTransform) {
                
                return parentTransform.apply(connection.getTransform());
            }
        }
        
        return connection.getTransform();
    }

    @Override
    public void register(Entity entity) {
        connection = new Connection();
        connection.setChild(entity);
    }

}
