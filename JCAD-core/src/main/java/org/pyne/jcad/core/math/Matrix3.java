package org.pyne.jcad.core.math;

import eu.mihosoft.vvecmath.Vector3d;
import haxe.root.Array;

import java.util.Optional;

/**
 * @author matt pyne
 */
public class Matrix3 {

    private double mxx, mxy, mxz, tx;
    private double myx, myy, myz, ty;
    private double mzx, mzy, mzz, tz;

    /**
     *
     */
    public Matrix3() {
        reset();
    }

    /**
     * @return
     */
    public Matrix3 reset() {
        mxx = 1;
        mxy = 0;
        mxz = 0;
        tx = 0;
        myx = 0;
        myy = 1;
        myz = 0;
        ty = 0;
        mzx = 0;
        mzy = 0;
        mzz = 1;
        tz = 0;
        return this;
    }

    /**
     * @param b
     * @return
     */
    public Matrix3 setBasis(Axis3 b) {
        mxx = b.v1().x();
        mxy = b.v2().x();
        mxz = b.v3().x();
        tx = 0;
        myx = b.v1().y();
        myy = b.v2().y();
        myz = b.v3().y();
        ty = 0;
        mzx = b.v1().z();
        mzy = b.v2().z();
        mzz = b.v3().z();
        tz = 0;
        return this;
    }

    /**
     * @param dx
     * @param dy
     * @param dz
     * @return
     */
    public Matrix3 translate(double dx, double dy, double dz) {
        tx += dx;
        ty += dy;
        tz += dz;
        return this;
    }

    /**
     * @param mxx
     * @param mxy
     * @param mxz
     * @param myx
     * @param myy
     * @param myz
     * @param mzx
     * @param mzy
     * @param mzz
     * @return
     */
    public Matrix3 set3(
            double mxx, double mxy, double mxz,
            double myx, double myy, double myz,
            double mzx, double mzy, double mzz) {

        this.mxx = mxx;
        this.mxy = mxy;
        this.mxz = mxz;
        this.myx = myx;
        this.myy = myy;
        this.myz = myz;
        this.mzx = mzx;
        this.mzy = mzy;
        this.mzz = mzz;
        return this;
    }

    /**
     * @param mxx
     * @param mxy
     * @param mxz
     * @param tx
     * @param myx
     * @param myy
     * @param myz
     * @param ty
     * @param mzx
     * @param mzy
     * @param mzz
     * @param tz
     * @return
     */
    public Matrix3 set34(
            double mxx, double mxy, double mxz, double tx,
            double myx, double myy, double myz, double ty,
            double mzx, double mzy, double mzz, double tz) {

        this.mxx = mxx;
        this.mxy = mxy;
        this.mxz = mxz;
        this.tx = tx;
        this.myx = myx;
        this.myy = myy;
        this.myz = myz;
        this.ty = ty;
        this.mzx = mzx;
        this.mzy = mzy;
        this.mzz = mzz;
        this.tz = tz;
        return this;
    }

    /**
     * @param m
     * @return
     */
    public Matrix3 setMatrix(Matrix3 m) {
        mxx = m.mxx;
        mxy = m.mxy;
        mxz = m.mxz;
        tx = m.tx;
        myx = m.myx;
        myy = m.myy;
        myz = m.myz;
        ty = m.ty;
        mzx = m.mzx;
        mzy = m.mzy;
        mzz = m.mzz;
        tz = m.tz;
        return this;
    }

    /**
     * @return
     */
    public double[][] toArray() {
        return new double[][]{
                {mxx, mxy, mxz, tx},
                {myx, myy, myz, ty},
                {mzx, mzy, mzz, tz}
        };
    }

    /**
     * @return
     */
    public double[] toArray1D() {
        return new double[]{
                mxx, mxy, mxz, tx,
                myx, myy, myz, ty,
                mzx, mzy, mzz, tz
        };
    }

    /**
     * @return
     */
    public Array<Array<Number>> toVerbArray() {
        Array<Array<Number>> array = new Array<>();
        array.push(new Array<>(new Number[]{mxx, mxy, mxz, tx}));
        array.push(new Array<>(new Number[]{myx, myy, myz, ty}));
        array.push(new Array<>(new Number[]{mzx, mzy, mzz, tz}));
        array.push(new Array<>(new Number[]{0, 0, 0, 1}));
        return array;
    }

    /**
     * @return
     */
    public Optional<Matrix3> invert() {
        double det =
                mxx * (myy * mzz - mzy * myz) +
                        mxy * (myz * mzx - mzz * myx) +
                        mxz * (myx * mzy - mzx * myy);

        if (Math.abs(det) < CadMath.TOLERANCE) {

            return Optional.empty();
        }

        double cxx = myy * mzz - myz * mzy;
        double cyx = -myx * mzz + myz * mzx;
        double czx = myx * mzy - myy * mzx;
        double cxt = -mxy * (myz * tz - mzz * ty)
                - mxz * (ty * mzy - tz * myy)
                - tx * (myy * mzz - mzy * myz);
        double cxy = -mxy * mzz + mxz * mzy;
        double cyy = mxx * mzz - mxz * mzx;
        double czy = -mxx * mzy + mxy * mzx;
        double cyt = mxx * (myz * tz - mzz * ty)
                + mxz * (ty * mzx - tz * myx)
                + tx * (myx * mzz - mzx * myz);
        double cxz = mxy * myz - mxz * myy;
        double cyz = -mxx * myz + mxz * myx;
        double czz = mxx * myy - mxy * myx;
        double czt = -mxx * (myy * tz - mzy * ty)
                - mxy * (ty * mzx - tz * myx)
                - tx * (myx * mzy - mzx * myy);

        Matrix3 result = new Matrix3();
        result.mxx = cxx / det;
        result.mxy = cxy / det;
        result.mxz = cxz / det;
        result.tx = cxt / det;
        result.myx = cyx / det;
        result.myy = cyy / det;
        result.myz = cyz / det;
        result.ty = cyt / det;
        result.mzx = czx / det;
        result.mzy = czy / det;
        result.mzz = czz / det;
        result.tz = czt / det;

        return Optional.of(result);
    }

    /**
     * @param transform
     * @return
     */
    public Matrix3 combine(Matrix3 transform) {
        double txx = transform.mxx;
        double txy = transform.mxy;
        double txz = transform.mxz;
        double ttx = transform.tx;
        double tyx = transform.myx;
        double tyy = transform.myy;
        double tyz = transform.myz;
        double tty = transform.ty;
        double tzx = transform.mzx;
        double tzy = transform.mzy;
        double tzz = transform.mzz;
        double ttz = transform.tz;

        Matrix3 m = new Matrix3();
        m.mxx = (mxx * txx + mxy * tyx + mxz * tzx);
        m.mxy = (mxx * txy + mxy * tyy + mxz * tzy);
        m.mxz = (mxx * txz + mxy * tyz + mxz * tzz);
        m.tx = (mxx * ttx + mxy * tty + mxz * ttz + tx);
        m.myx = (myx * txx + myy * tyx + myz * tzx);
        m.myy = (myx * txy + myy * tyy + myz * tzy);
        m.myz = (myx * txz + myy * tyz + myz * tzz);
        m.ty = (myx * ttx + myy * tty + myz * ttz + ty);
        m.mzx = (mzx * txx + mzy * tyx + mzz * tzx);
        m.mzy = (mzx * txy + mzy * tyy + mzz * tzy);
        m.mzz = (mzx * txz + mzy * tyz + mzz * tzz);
        m.tz = (mzx * ttx + mzy * tty + mzz * ttz + tz);

        return m;
    }

    /**
     * @param vector
     * @return
     */
    public Point3DC apply(Vector3d vector) {
        double x = vector.x();
        double y = vector.y();
        double z = vector.z();
        return new Point3DC(Vector3d.xyz(
                this.mxx * x + this.mxy * y + this.mxz * z + this.tx,
                this.myx * x + this.myy * y + this.myz * z + this.ty,
                this.mzx * x + this.mzy * y + this.mzz * z + this.tz));
    }

    /**
     * @param angle
     * @param axis
     * @param pivot
     * @return
     */
    public static Matrix3 createRotation(double angle, Point3DC axis, Point3DC pivot) {
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        double axisX, axisY, axisZ;
        Matrix3 m = new Matrix3();

        if (axis.equals(L3Space.X)
                || axis.equals(L3Space.Y)
                || axis.equals(L3Space.Z)) {

            axisX = axis.x();
            axisY = axis.y();
            axisZ = axis.z();
        } else {
            // normalize
            double mag = axis.magnitude();

            if (mag == 0.0) {
                return m;
            } else {
                axisX = axis.x() / mag;
                axisY = axis.y() / mag;
                axisZ = axis.z() / mag;
            }
        }

        double px = pivot.x();
        double py = pivot.y();
        double pz = pivot.z();

        m.mxx = cos + axisX * axisX * (1 - cos);
        m.mxy = axisX * axisY * (1 - cos) - axisZ * sin;
        m.mxz = axisX * axisZ * (1 - cos) + axisY * sin;

        m.tx = px * (1 - m.mxx) - py * m.mxy - pz * m.mxz;

        m.myx = axisY * axisX * (1 - cos) + axisZ * sin;
        m.myy = cos + axisY * axisY * (1 - cos);
        m.myz = axisY * axisZ * (1 - cos) - axisX * sin;
        m.ty = py * (1 - m.myy) - px * m.myx - pz * m.myz;

        m.mzx = axisZ * axisX * (1 - cos) - axisY * sin;
        m.mzy = axisZ * axisY * (1 - cos) + axisX * sin;
        m.mzz = cos + axisZ * axisZ * (1 - cos);
        m.tz = pz * (1 - m.mzz) - px * m.mzx - py * m.mzy;

        return m;
    }

    public static Matrix3 createBetweenBasisTransform(Axis3 fromBasis, Axis3 toBasis) {
        Matrix3 m = new Matrix3();
        Point3DC x1 = toBasis.v2();
        Point3DC y1 = toBasis.v3();
        Point3DC z1 = toBasis.v1();

        Point3DC x2 = fromBasis.v2();
        Point3DC y2 = fromBasis.v3();
        Point3DC z2 = fromBasis.v1();

        m.mxx = x1.dot(x2);
        m.mxy = x1.dot(y2);
        m.mxz = x1.dot(z2);
        m.myx = y1.dot(x2);
        m.myy = y1.dot(y2);
        m.myz = y1.dot(z2);
        m.mzx = z1.dot(x2);
        m.mzy = z1.dot(y2);
        m.mzz = z1.dot(z2);

        Point3DC translation = fromBasis.location.minus(toBasis.location);
        m.tx = translation.x();
        m.ty = translation.y();
        m.tz = translation.z();

        return m;
    }

    public static Matrix3 createTranslation(double x, double y, double z) {
        return new Matrix3().translate(x, y, z);
    }

    public static Matrix3 createTranslation(Point3DC translate) {
        return createTranslation(
                translate.x(),
                translate.y(),
                translate.z()
        );
    }

    public static Matrix3 createScale(double scaleFactor) {
        Matrix3 m = new Matrix3();
        m.mxx = scaleFactor;
        m.myy = scaleFactor;
        m.mzz = scaleFactor;
        return m;
    }
}
