package org.pyne.jcad.core.exe;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 *
 * @author Matthew
 */
public class ConsumerBatcher {

    static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    /**
     * Partitions data into batches based on available processors;
     *
     * @param <T>
     * @param data
     * @return
     */
    public static <T> Collection<List<T>> create(List<T> data) {

        int batchSize = (int) Math.ceil(data.size() / Runtime.getRuntime().availableProcessors());

        final AtomicInteger counter = new AtomicInteger(0);

        return data.stream()
                .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / batchSize))
                .values();
    }

    public static <T> void executeSync(List<T> data, Consumer<List<T>> batchProcess) {
        
//        batchProcess.accept(data);
//        
//        return;
        if (data.size() < Runtime.getRuntime().availableProcessors() * 10) {

            return;
        }

        List<Callable<Object>> callables = create(data).stream()
                .map(d -> Executors.callable(() -> batchProcess.accept(d)))
                .collect(Collectors.toList());

        try {
            
            long startProcessingTime = System.currentTimeMillis();
            
            List<Future<Object>> futures = executor.invokeAll(callables);
            
            System.out.println("Batch comsumer complete. Time to complete: " + (System.currentTimeMillis() - startProcessingTime));
        } catch (InterruptedException ex) {

            ex.printStackTrace();
        }
    }
    
    public static void awaitTerminationAfterShutdown(ExecutorService threadPool) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException ex) {
            threadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
}
}
