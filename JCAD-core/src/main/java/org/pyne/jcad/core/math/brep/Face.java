package org.pyne.jcad.core.math.brep;

import org.pyne.jcad.core.math.brep.eval.Env2D;
import org.pyne.jcad.core.math.brep.eval.PointInPolygon;
import org.pyne.jcad.core.math.brep.eval.WorkingPolygon;
import org.pyne.jcad.core.DataContainer;
import eu.mihosoft.vvecmath.Vector3d;
import haxe.lang.EmptyObject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import verb.core.BoundingBox;

/**
 *
 * @author matt pyne
 */
public class Face extends DataContainer implements Topo {

    private BrepSurface surface;
    private Shell shell;
    private final List<Loop> innerLoops;
    private Loop outerLoop;
    private Env2D env2d;

    public Face() {
        super();
        this.innerLoops = new ArrayList<>();
        this.outerLoop = new Loop(this);
    }

    /**
     *
     * @param surface
     */
    public Face(BrepSurface surface) {
        super();
        this.surface = surface;
        this.outerLoop = new Loop(this);
        this.innerLoops = new ArrayList<>();
    }

    /**
     *
     * @param other
     */
    public Face(Face other) {
        super();
        surface = other.surface.copy();
        innerLoops = other.innerLoops
                .stream()
                .map(Loop::copy)
                .collect(Collectors.toList());
        innerLoops.forEach(loop -> loop.setFace(this));
        outerLoop = other.outerLoop.copy();
        outerLoop.setFace(this);
    }

    /**
     *
     * @return
     */
    @Override
    public Face copy() {
        return new Face(this);
    }

    @Override
    public void transform(Matrix3 tr) {
        cache().clear(Cache.BOUNDING_BOX);
        cache().clear(Cache.FACE_WORKING_POLYGON);
        loops().forEach(loop -> loop.transform(tr));
        surface = surface.transform(tr);
    }

    @Override
    public Face transformed(Matrix3 tr) {
        Face instance = copy();

        instance.transform(tr);

        return instance;
    }

    public WorkingPolygon createWorkingPolygon() {
        return cache().get(Cache.FACE_WORKING_POLYGON, () -> {
            WorkingPolygon workingPolygon = new WorkingPolygon();
            List<Point3DC> outer = outerLoop.tessellate()
                    .stream()
                    .map(pt -> surface.workingPoint(pt))
                    .collect(Collectors.toList());

            List<List<Point3DC>> inners = innerLoops.stream()
                    .map(loop -> loop.tessellate()
                    .stream()
                    .map(pt -> surface.workingPoint(pt))
                    .collect(Collectors.toList()))
                    .collect(Collectors.toList());

            workingPolygon.outer = outer;
            workingPolygon.inners = inners;

            return workingPolygon;
        });
    }

    public Env2D env2D() {
        if (null == env2d) {
            WorkingPolygon workingPolygon = createWorkingPolygon();
            env2d = new Env2D();
            env2d.pip = new PointInPolygon(workingPolygon.outer, workingPolygon.inners);
            env2d.workIngPolygon = workingPolygon;
        }
        return env2d;
    }

    /**
     *
     */
    public void invert() {
        surface = surface.invert();
        cache().clear(Cache.FACE_WORKING_POLYGON);
        cache().clear(Cache.BOUNDING_BOX);
    }

    /**
     *
     * @return
     */
    public List<Loop> getInnerLoops() {
        return innerLoops;
    }

    public List<Loop> loops() {
        List<Loop> loops = new ArrayList(innerLoops);
        if (null != outerLoop) {
            loops.add(outerLoop);
        }
        return loops;
    }

    /**
     *
     * @return
     */
    public Stream<Loop> streamLoops() {
        return Stream.concat(innerLoops.stream(), Stream.of(outerLoop));
    }

    /**
     *
     * @return
     */
    public Stream<Vertex> streamVertices() {
        return streamLoops().flatMap(Loop::streamVertices);
    }

    /**
     *
     * @return
     */
    public List<Vertex> getVertices() {
        return streamVertices()
                .collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public Stream<Vector3d> streamPositions() {
        return streamVertices().parallel()
                .map(Vertex::point);
    }

    /**
     *
     * @return
     */
    public List<Vector3d> getPositions() {
        return streamVertices().parallel()
                .map(Vertex::point)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param loop
     */
    public void addInnerLoop(Loop loop) {
        innerLoops.add(loop);
        cache().clear(Cache.FACE_WORKING_POLYGON);
    }

    /**
     *
     * @return
     */
    public Stream<HalfEdge> streamHalfEdges() {
        return streamLoops().flatMap(Loop::streamHalfEdges);
    }
    
    public Stream<Edge> streamEdges() {
        return streamLoops().flatMap(Loop::streamEdges);
    }

    /**
     *
     * @return
     */
    public List<HalfEdge> halfEdges() {
        List<HalfEdge> edges = new ArrayList<>();
        for (Loop loop : loops()) {
            for (HalfEdge edge : loop.halfEdges()) {
                edges.add(edge);
            }
        }
        return edges;
    }

    /**
     *
     * @return
     */
    public BrepSurface surface() {
        if (null == surface) {
            // Assume try to create a surface.(Approx or Nurbs maybe?) 
        }
        return surface;
    }

    /**
     *
     * @param surface
     */
    public void setSurface(BrepSurface surface) {
        this.surface = surface;
    }

    /**
     *
     * @return
     */
    public Shell getShell() {
        return shell;
    }

    /**
     *
     * @param shell
     */
    public void setShell(Shell shell) {
        this.shell = shell;
    }

    /**
     *
     * @return
     */
    public Loop getOuterLoop() {
        return outerLoop;
    }

    /**
     *
     * @param outerLoop
     */
    public void setOuterLoop(Loop outerLoop) {
        this.outerLoop = outerLoop;
        outerLoop.setFace(this);
        cache().clear(Cache.BOUNDING_BOX);
        cache().clear(Cache.FACE_WORKING_POLYGON);
    }

    public BoundingBox bounds() {
        return cache().get(Cache.BOUNDING_BOX,
                () -> {
                    BoundingBox bounds = new BoundingBox(EmptyObject.EMPTY);
                    for (Loop loop : loops()) {
                        if (!loop.isEmpty()) {
                            bounds.add(loop.bounds().max);
                            bounds.add(loop.bounds().min);
                        }
                    }
                    return bounds;
                });
    }
    
    @Override
    public boolean isEmpty() {
        List<Loop> loops = loops();
        return loops.isEmpty()
                || loops.stream().allMatch(Loop::isEmpty);
    }
}
