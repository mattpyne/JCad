package org.pyne.jcad.core.math.geom;

import verb.core.Vec;

/**
 * @author Matthew
 */
public class Circle extends NurbsCurve {

    public Circle(verb.geom.Circle verbCurve) {
        super(verbCurve);
    }

    public void setRadius(double rad) {
        impl = new verb.geom.Circle(
                ((verb.geom.Circle) impl).center().copy(),
                ((verb.geom.Circle) impl).xaxis().copy(),
                ((verb.geom.Circle) impl).yaxis().copy(),
                rad);
    }

//    @Override
//    public NurbsCurve invert() {
//        verb.geom.Circle impl = new verb.geom.Circle(
//                ((verb.geom.Circle) this.impl).center().copy(),
//                Vec.mul(-1, ((verb.geom.Circle) this.impl).xaxis()),
//                Vec.mul(1, ((verb.geom.Circle) this.impl).yaxis()),
//                ((verb.geom.Circle) this.impl).radius());
//        return new NurbsCurve(impl);
//    }
}
