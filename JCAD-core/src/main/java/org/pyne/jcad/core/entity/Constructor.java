package org.pyne.jcad.core.entity;

import java.util.ArrayList;

/**
 *
 * @author Matthew Pyne
 */
public class Constructor implements IComponent {

    private int operationEnd;
    private final ArrayList<IOperation> operations;

    private Entity entity;

    public Constructor() {
        this.operations = new ArrayList<>();
        this.operationEnd = -1;
    }

    public ArrayList<IOperation> operations() {
        return operations;
    }

    public int operationEnd() {
        return operationEnd;
    }

    public void setOperationEnd(int operationEnd) {
        this.operationEnd = operationEnd;
    }

    @Override
    public void invalidate() {
        operationEnd = -1;
    }

    @Override
    public void update() {
        if (operationEnd == -1) {

            operationEnd = operations.size();
        }

        for (int i = 0; i < operationEnd; i++) {
            operations.get(i).onApply(entity);
        }
    }

    @Override
    public void register(Entity entity) {
        this.entity = entity;
    }

    public void add(IOperation operation) {
        operations.add(operation);
    }

}
