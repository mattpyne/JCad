package org.pyne.jcad.core.math.brep;

import haxe.root.Array;
import java.util.List;
import java.util.stream.Collectors;

import org.pyne.jcad.core.math.geom.PlanarSurface;
import org.pyne.jcad.core.math.geom.Surface;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.geom.Curve;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.BoundingBox;
import verb.core.Interval;
import verb.core.MeshData;

/**
 *
 * @author Matthew Pyne
 */
public class BrepSurface {

    private Surface impl;

    public BrepSurface(Surface impl) {
        this.impl = impl;
    }

    public BrepSurface(BrepSurface other) {
        this(other.impl);
    }

    public BrepSurface copy() {
        return new BrepSurface(this);
    }

    public Surface impl() {
        return impl;
    }

    public Interval<Number> domainU() {
        return impl.domainU();
    }

    public Interval<Number> domainV() {
        return impl.domainV();
    }

    public BrepSurface transform(Matrix3 tr) {
        return new BrepSurface(impl.transform(tr));
    }

    public Point3DC workingPoint(Point3DC pt) {
        return impl.workingPoint(pt);
    }

    /**
     * May not match Face bounds. Surfaces can be trimmed by the loops of a face
     * for the the final result so the surface bounds may be bigger then the
     * face which the surface helps to define.
     *
     * @return
     */
    public BoundingBox bounds() {
        return impl.bounds();
    }

    public Point3DC normal(Point3DC closestPt) {
        return impl.normal(closestPt);
    }

    public Curve isoCurveAlignV(double param) {
        return impl.isoCurveAlignV(param);
    }

    public Curve isoCurveAlignU(double param) {
        return impl.isoCurveAlignU(param);
    }

    public BrepSurface invert() {
        return new BrepSurface(impl.invert());
    }

    public Point3DC closestPoint(Point3DC pt) {
        return impl.closestPoint(pt);
    }

    public Point3DC middlePt() {
        return impl.middlePt();
    }

    public Array<Number> param(Point3DC pt) {
        return impl.param(pt);
    }

    public Point3DC point(double u, double v) {
        return impl.point(u, v);
    }

    public PlanarSurface getSimpleSurface() {
        return impl.getSimpleSurface();
    }

    public List<BrepCurve> intersectSurface(BrepSurface surface) {
        return impl.intersectSurface(surface.impl)
                .stream()
                .map(BrepCurve::new)
                .collect(Collectors.toList());
    }

    public MeshData tessellate() {
        return impl.tessellate();
    }

    public Point3DC createWorkingPoint(Array<Number> array, Point3DC hashVector3d) {
        return impl.createWorkingPoint(array, hashVector3d);
    }

    public Point3DC workingPointTo3D(Point3DC p) {
        return impl.workingPointTo3D(p);
    }

    public boolean isPlane() {
        return impl.isPlane();
    }

    public boolean isPointOn(Point3DC point) {
        return point.distanceSq(closestPoint(point)) < Tolerance.TOLERANCE_SQ;
    }
}
