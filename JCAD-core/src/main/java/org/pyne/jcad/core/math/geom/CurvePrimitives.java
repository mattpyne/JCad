package org.pyne.jcad.core.math.geom;

import java.util.List;

import org.pyne.jcad.core.verbinterop.VerbInterop;
import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author Matthew
 */
public class CurvePrimitives {

    /**
     *
     * @param start
     * @param end
     * @return
     */
    public static NurbsCurve line(Point3DC start, Point3DC end) {
        verb.geom.NurbsCurve line = new verb.geom.Line(
                start.asArray(),                                                // start
                end.asArray());                                                 // end
        
        return new NurbsCurve(line);
    }
    
    /**
     * 
     * @param center
     * @param xAxis
     * @param yAxis
     * @param radius
     * @return 
     */
    public static NurbsCurve circle(Point3DC center, Point3DC xAxis, Point3DC yAxis, double radius) {
        verb.geom.NurbsCurve circle = new verb.geom.Circle(
                center.asArray(),                                               // center
                xAxis.asArray(),                                                // xaxis
                yAxis.asArray(),                                                // yaxis
                radius);                                                        // radius

        return new NurbsCurve(circle);
    }
    
    /**
     * 
     * @param center
     * @param radius
     * @return 
     */
    public static NurbsCurve circle(Point3DC center, double radius) {
        verb.geom.Circle circle = new verb.geom.Circle(
                center.asArray(),                                               // center
                Point3DC.X_ONE.asArray(),                                       // xaxis
                Point3DC.Y_ONE.asArray(),                                       // yaxis
                radius);                                                        // radius

        return new NurbsCurve(circle);
    }
    
    /**
     * 
     * @param center
     * @param xRadius
     * @param yRadius
     * @return 
     */
    public static NurbsCurve ellipse(Point3DC center, double xRadius, double yRadius) {
        verb.geom.NurbsCurve ellipse = new verb.geom.Ellipse(
                center.asArray(), 
                Point3DC.X_ONE.times(xRadius).asArray(),                         // xaxis
                Point3DC.Y_ONE.times(yRadius).asArray());                        // yaxis
        
        return new NurbsCurve(ellipse);
    }
    
    /**
     * 
     * @param points
     * @return
     */
    public static NurbsCurve bezierCurve(List<Point3DC> points) {
        return new NurbsCurve(new verb.geom.BezierCurve(VerbInterop.points(points), null));
    }
    
    /**
     * 
     * @param center
     * @param radius
     * @param minAngle
     * @param maxAngle
     * @return 
     */
    public static NurbsCurve arc(Point3DC center, double radius, double minAngle, double maxAngle) {
        verb.geom.NurbsCurve arc = new verb.geom.Arc(
                center.asArray(),
                Point3DC.X_ONE.asArray(),                                        // xaxis
                Point3DC.Y_ONE.asArray(),                                        // yaxis
                radius,
                minAngle,
                maxAngle);
        
        return new NurbsCurve(arc);
    }
}
