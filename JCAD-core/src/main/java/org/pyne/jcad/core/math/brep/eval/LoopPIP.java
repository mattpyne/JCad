package org.pyne.jcad.core.math.brep.eval;

import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import org.pyne.jcad.core.math.Point3DC;
import verb.core.BoundingBox;

/**
 *
 * @author Matthew Pyne
 */
public class LoopPIP extends ArrayList<Point3DC> {

    private BoundingBox bounds;

    public LoopPIP(List<Point3DC> loop) {
        super(loop);
    }
    
    public LoopPIP(List<Point3DC> loop, BoundingBox bounds) {
        super(loop);
        this.bounds = bounds;
    }

    public BoundingBox bounds() {
        if (null == bounds) {
            Array<Array<Number>> boundPoints = new Array(new Array[size()]);

            for (int i = 0; i < size(); i++) {
                boundPoints.set(i, get(i).asArray());
            }
            bounds = new BoundingBox(boundPoints);
        }
        return bounds;
    }
}
