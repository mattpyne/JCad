package org.pyne.jcad.core.math.brep.eval.bool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.geom.Tolerance;

/**
 *
 * @author Matthew Pyne
 */
public class ShellBoolOps {

    private enum CLASSIFCATION {
        AinB, BinA, INTERS, NONE
    };

    private Shell shellA;
    private HashMap<Face, CLASSIFCATION> facesA;
    private HashMap<Edge, CLASSIFCATION> edgesA;
    private HashMap<Vertex, CLASSIFCATION> vertexsA;

    private Shell shellB;
    private HashMap<Face, CLASSIFCATION> facesB;
    private HashMap<Edge, CLASSIFCATION> edgesB;
    private HashMap<Vertex, CLASSIFCATION> vertexsB;

    private List<Face> facesAB;
    private List<Edge> edgesAB;
    private List<Vertex> vertexsAB;

    public ShellBoolOps(Shell shellA, Shell shellB) {
        this.shellA = shellA.clone();
        this.shellB = shellB.clone();
    }

    private void preProcessing() {
        facesA = new HashMap<>();
        shellA.streamFaces().forEach(face -> facesA.put(face, CLASSIFCATION.NONE));
        edgesA = new HashMap<>();
        shellA.streamEdges().forEach(edge -> edgesA.put(edge, CLASSIFCATION.NONE));
        vertexsA = new HashMap<>();
        shellA.streamVertices().forEach(vertex -> vertexsA.put(vertex, CLASSIFCATION.NONE));

        facesB = new HashMap<>();
        shellB.streamFaces().forEach(face -> facesB.put(face, CLASSIFCATION.NONE));
        edgesB = new HashMap<>();
        shellB.streamEdges().forEach(edge -> edgesB.put(edge, CLASSIFCATION.NONE));
        vertexsB = new HashMap<>();
        shellB.streamVertices().forEach(vertex -> vertexsB.put(vertex, CLASSIFCATION.NONE));

        edgesAB = new ArrayList<>();
        vertexsAB = new ArrayList<>();
        
        shellA.streamFaces().forEach(faceA -> {

            shellB.streamFaces().forEach(faceB -> {

                if (faceA.bounds().intersects(faceB.bounds(), Tolerance.TOLERANCE)) {

                    faceA.surface().intersectSurface(faceB.surface()).forEach(curve -> {

                        Vertex vertexStartAB = new Vertex(curve.start());
                        Vertex vertexEndAB = new Vertex(curve.end());
                        vertexsAB.add(vertexStartAB);
                        vertexsAB.add(vertexEndAB);
                        edgesAB.add(new Edge(curve, vertexStartAB, vertexEndAB));
                    });
                }
            });
        });

    }
}
