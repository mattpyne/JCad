package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.Color;

/**
 *
 * @author Matthew Pyne
 */
public class Material implements IComponent {

    private String description;
    private Color color;

    public Material() {
        color = new Color(0.7, 0.7, 0.7, 1.);
        description = "Material Empty";
    }
    
    @Override
    public void invalidate() {
    }

    @Override
    public void update() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void register(Entity entity) {
    }

}
