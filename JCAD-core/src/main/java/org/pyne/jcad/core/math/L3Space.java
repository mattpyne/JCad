package org.pyne.jcad.core.math;

import eu.mihosoft.vvecmath.Vector3d;

/**
 *
 * @author matt pyne
 */
public class L3Space {

    public static final Point3DC ORIGIN = new Point3DC(Vector3d.ZERO);
    public static final Point3DC X = new Point3DC(Vector3d.X_ONE);
    public static final Point3DC Y = new Point3DC(Vector3d.Y_ONE);
    public static final Point3DC Z = new Point3DC(Vector3d.Z_ONE);

    public static final Axis3 STANDARD_AXIS = new Axis3();
    public static final Axis3 STANDARD_AXIS_XY = STANDARD_AXIS;
    public static final Axis3 STANDARD_AXIS_XZ = new Axis3(X, Z, Y);
    public static final Axis3 STANDARD_AXIS_ZY = new Axis3(Z, Y, X);

    /**
     *
     * @param normal
     * @return
     */
    public static Axis3 basisForPlane(Point3DC normal) {
        Vector3d alignPlane, x, y;

        if (Math.abs(normal.dot(Y)) < 0.5) {

            alignPlane = normal.crossed(Y);
        } else {

            alignPlane = normal.crossed(Z);
        }

        y = alignPlane.crossed(normal).normalized();
        x = y.crossed(normal).normalized();

        return new Axis3(
                new Point3DC(x),
                new Point3DC(y),
                new Point3DC(normal));
    }
}
