package org.pyne.jcad.core.exe.datastructures;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static org.pyne.jcad.core.exe.datastructures.KdPointComparators.X_COMPARATOR;
import static org.pyne.jcad.core.exe.datastructures.KdPointComparators.Y_COMPARATOR;
import static org.pyne.jcad.core.exe.datastructures.KdPointComparators.Z_COMPARATOR;

public class KdPoint<T> implements Comparable<KdPoint<T>> {

    protected final double x;
    protected final double y;
    protected final double z;

    private T object;

    /**
     * z is defaulted to zero.
     *
     * @param x
     * @param y
     */
    public KdPoint(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    /**
     * z is defaulted to zero.
     *
     * @param x
     * @param y
     */
    public KdPoint(double x, double y, T object) {
        this.x = x;
        this.y = y;
        this.z = 0;
        this.object = object;
    }

    /**
     * Default constructor
     *
     * @param x
     * @param y
     * @param z
     */
    public KdPoint(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Does not use R to calculate x, y, and z. Where R is the approximate radius of earth (e.g. 6371KM).
     *
     * @param latitude
     * @param longitude
     */
    public KdPoint(Double latitude, Double longitude) {
        x = cos(Math.toRadians(latitude)) * cos(Math.toRadians(longitude));
        y = cos(Math.toRadians(latitude)) * sin(Math.toRadians(longitude));
        z = sin(Math.toRadians(latitude));
    }

    public void attachObject(T obj) {
        this.object = obj;
    }

    public T get() {
        return object;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    /**
     * Computes the Euclidean distance from this point to the other.
     *
     * @param o1 other point.
     * @return euclidean distance.
     */
    public double euclideanDistance(KdPoint o1) {
        return euclideanDistance(o1, this);
    }

    /**
     * Computes the Euclidean distance from one point to the other.
     *
     * @param o1 first point.
     * @param o2 second point.
     * @return euclidean distance.
     */
    private static final double euclideanDistance(KdPoint o1, KdPoint o2) {
        return Math.sqrt(Math.pow((o1.x - o2.x), 2) + Math.pow((o1.y - o2.y), 2) + Math.pow((o1.z - o2.z), 2));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return 31 * (int) (this.x + this.y + this.z);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof KdPoint))
            return false;

        KdPoint xyzPoint = (KdPoint) obj;
        if (Double.compare(this.x, xyzPoint.x) != 0)
            return false;
        if (Double.compare(this.y, xyzPoint.y) != 0)
            return false;
        if (Double.compare(this.z, xyzPoint.z) != 0)
            return false;
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(KdPoint o) {
        int xComp = X_COMPARATOR.compare(this, o);
        if (xComp != 0)
            return xComp;
        int yComp = Y_COMPARATOR.compare(this, o);
        if (yComp != 0)
            return yComp;
        int zComp = Z_COMPARATOR.compare(this, o);
        return zComp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        builder.append(x).append(", ");
        builder.append(y).append(", ");
        builder.append(z);
        builder.append(")");
        return builder.toString();
    }
}