package org.pyne.jcad.core.math.brep;

import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.Matrix3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CompoundTopo extends DataContainer implements Topo {

    private List<Topo> topos;

    public CompoundTopo() {

    }

    public void addAll(Collection<Shell> shells) {
        shells.forEach(this::add);
    }

    public void add(Topo topo) {
        if (null == this.topos) {
            this.topos = new ArrayList<>();
        }

        if (this.topos.contains(topo)) {
            return;
        }

        this.topos.add(topo);

    }

    @Override
    public CompoundTopo copy() {
        CompoundTopo copy = new CompoundTopo();

        topos.forEach(shell -> copy.add(shell.copy()));

        return copy;
    }

    @Override
    public void transform(Matrix3 tr) {
        topos.forEach(shell -> shell.transform(tr));
    }

    @Override
    public Topo transformed(Matrix3 tr) {
        CompoundTopo copy = copy();

        copy.transform(tr);

        return copy;
    }

    public List<Topo> topos() {
        return topos;
    }

    @Override
    public boolean isEmpty() {
        return topos.isEmpty()
                || topos.stream().allMatch(Topo::isEmpty);
    }

}
