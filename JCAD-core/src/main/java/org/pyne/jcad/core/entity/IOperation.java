package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.math.brep.Topo;

import java.util.List;

/**
 *
 * @author Matthew Pyne
 */
public interface IOperation {
    
    void onApply(Entity entity);
    
    Topo topoAt();
    
    Topo topoOp();

    default boolean failed() {
        return !getErrorMessages().isEmpty();
    }

    List<String> getErrorMessages();

    void register(ExpressionSolver expressionSolver);
}
