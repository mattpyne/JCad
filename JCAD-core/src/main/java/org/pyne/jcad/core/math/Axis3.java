package org.pyne.jcad.core.math;

import static org.pyne.jcad.core.math.L3Space.X;
import static org.pyne.jcad.core.math.L3Space.Y;
import static org.pyne.jcad.core.math.L3Space.Z;
import static org.pyne.jcad.core.math.Point3DC.ZERO;

/**
 *
 * @author matt pyne
 */
public class Axis3 {

    public final Point3DC location;

    public final Point3DC v1;
    public final Point3DC v2;
    public final Point3DC v3;

    public Axis3() {
        v1 = X.clone();
        v2 = Y.clone();
        v3 = Z.clone();
        location = ZERO.clone();
    }

    public Axis3(Point3DC v1, Point3DC v2, Point3DC v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        location = ZERO.clone();
    }

    public Axis3(Point3DC v1, Point3DC v2, Point3DC v3, Point3DC location) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.location = location;
    }

    public Point3DC v1() {
        return v1;
    }

    public Point3DC v2() {
        return v2;
    }

    public Point3DC v3() {
        return v3;
    }

    public Point3DC getLocation() {
        return location;
    }
}
