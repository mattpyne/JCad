package org.pyne.jcad.core.entity;

import java.util.ArrayList;
import java.util.Collection;
import org.pyne.jcad.core.math.BBox;

/**
 *
 * @author Matthew Pyne
 */
public class Boundary implements IComponent {

    private Entity entity;
    private BBox box;

    public Boundary copy() {
        Boundary copy = new Boundary();
        copy.box = box.copy();
        return copy;
    }
    
    @Override
    public void invalidate() {
        box = null;
    }

    @Override
    public void update() {
        if (null == box) {

            box = new BBox();

            entity.getComponent(Mesh.class)
                    .map(Mesh::triangles)
                    .orElse(new ArrayList<>())
                    .stream()
                    .flatMap(Collection::stream)
                    .forEach(box::checkPoint);
        }
    }

    @Override
    public void register(Entity entity) {
        this.entity = entity;
    }

}
