package org.pyne.jcad.core;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepCurve;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.eval.BrepValidator;
import org.pyne.jcad.core.math.brep.eval.bool.Node;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.BoundingBox;

/**
 *
 * @author Matthew
 */
public class Debug {

    public enum LEVEL {
        ERROR, INFO;
    }

    public static boolean ON = false;

    public static boolean ERRORS = true;
    public static boolean WARNING = true;
    public static boolean INFO = true;

    private static boolean errorsOff() {
        return !(ON && ERRORS);
    }

    private static boolean warningOff() {
        return !(ON && WARNING);
    }

    private static boolean infoOff() {
        return !(ON && INFO);
    }

    public static void log(Exception ex) {
        if (errorsOff()) {
            return;
        }

        ex.printStackTrace();
    }

    public static void log(LEVEL level, String message) {
        if (level == LEVEL.ERROR) {
            if (errorsOff()) {
                return;
            }

            System.err.println(message);
        } else if (level == LEVEL.INFO) {
            if (infoOff()) {
                return;
            }

            System.out.println(message);
        }
    }

    public static void mismatchBounds(Collection<Face> faces) {
        if (errorsOff()) {
            return;
        }

        for (Face face : faces) {

            mismatchBounds(face);
        }
    }

    public static void mismatchBounds(Face face) {
        if (warningOff()) {
            return;
        }

        BoundingBox faceBounds = face.bounds();
        BoundingBox surfaceBounds = face.surface().bounds();
        if (!Tolerance.ueq(new Point3DC(faceBounds.max), new Point3DC(surfaceBounds.max))
                || !Tolerance.ueq(new Point3DC(faceBounds.min), new Point3DC(surfaceBounds.min))) {

            System.out.println("Face and surface Bounds do not match.");
        }
    }

    public static void checkEdgesValid(Shell shell) {
        if (errorsOff()) {
            return;
        }

        checkFaceEdgesValid(shell.streamFaces());
    }

    public static void checkFaceEdgesValid(Stream<Face> faces) {
        if (errorsOff()) {
            return;
        }

        Stream<Edge> edges = faces.flatMap(Face::streamEdges);
        if (!BrepValidator.isValidEdges(edges)) {

            System.err.println("Edges are not valid.");
        }
    }

    public static void checkEdgesValid(Collection<Loop> loop) {
        if (errorsOff()) {
            return;
        }

        checkEdgesValid(loop.stream().flatMap(Loop::streamEdges));
    }
    
    public static void checkEdgesValid(Loop loop) {
        if (errorsOff()) {
            return;
        }

        checkEdgesValid(loop.streamEdges());
    }
    
    public static void checkEdgesValid(Stream<Edge> edges) {
        if (errorsOff()) {
            return;
        }

        if (!BrepValidator.isValidEdges(edges)) {

            System.err.println("Edges are not valid.");
        }
    }
    
    public static void checkLoopsValid(Shell shell) {
        if (errorsOff()) {
            return;
        }

        checkLoopsValid(shell.streamFaces());
    }

    public static void checkLoopsValid(Stream<Face> faces) {
        if (errorsOff()) {
            return;
        }

        if (!BrepValidator.isValidFaces(faces)) {

            System.err.println("Loops are not valid.");
        }
    }

    public static void checkDetectedLoopsValid(Stream<Face> faces) {
        if (errorsOff()) {
            return;
        }

        Set<Loop> outerLoops = faces.flatMap(f -> f.op.detectedLoops.stream()).collect(Collectors.toSet());

        if (!BrepValidator.isValidLoops(outerLoops)) {

            System.err.println("Detected loops not valid");
        }
    }

    public static void checkFaceDataForError(List<Face> workingFaces) {
        if (errorsOff()) {
            return;
        }

        if (workingFaces.stream().anyMatch(f -> !f.op.collidedEdges.isEmpty())) {

            System.err.println("====Face Errors====");
            for (Face face : workingFaces) {

                System.err.println("Face has collided edges " + face);
                for (HalfEdge err : face.op.collidedEdges) {
                    System.err.println("/t Error HalfEdge: " + err);
                }
            }
        }
    }
    
    public static void checkPartialTangency(
            int touches,
            int tessCount, 
            BrepCurve curve,
            HalfEdge edge) {
        
        if (errorsOff()) {
            return;
        }

        if (tessCount != touches && touches != 0) {

            System.err.println("\t Partial Tangency: Edge: " + edge.getId() + " Curve: " + curve.getId());
        }
    }

    public static void checkShellValid(Shell result) {
        if (errorsOff()) {
            return;
        }

        if (result.faces().isEmpty()) {

            System.err.println("Shell is not valid.");
        }
    }

    public static void soutDetectLoopCount(Face face) {
        if (infoOff()) {
            return;
        }

        System.out.println("Detected loops: " + face.op.detectedLoops.size());
    }

    public static void soutValidDetectedLoops(Face face) {
        if (infoOff()) {
            return;
        }

        System.out.println("Valid Detected Loops: " + face.op.detectedLoops.size());
    }

    public static void soutRunningFaceIntersectionOn(Face faceA) {
        if (infoOff()) {
            return;
        }

        System.out.println("Looking for face intersection for " + faceA.getId());
    }

    public static void soutFoundIntersection(Face faceB) {
        if (infoOff()) {
            return;
        }

        System.out.println("Found intersection with " + faceB.getId());
    }

    public static void soutAddingNewEdgeFromIntersection() {
        if (infoOff()) {
            return;
        }

        System.out.println("adding edges");
    }

    public static void soutHowManyNewEdgesAfterFaceIntersection(Face faceA) {
        if (infoOff()) {
            return;
        }

        System.out.println("Face New edges after intersection: " + faceA.op.newEdges.size());
    }

    public static void soutFailedToSplitEdgesAtNodes(List<Node> nodes) {
        if (infoOff()) {
            return;
        }

        System.out.println("==============Failed to split face intersection curves at nodes==============");

        if (nodes.isEmpty()) {
            System.out.println("No nodes is why.");
        }

        for (Node node : nodes) {
            System.out.println("Node: " + node);
        }
    }

    public static void soutSplitingEdgeButNotTwo() {
        if (infoOff()) {
            return;
        }

        System.out.println("Splitting edge but didn't get 2 edges. Should be checked before this.");
    }

    public static void soutEmptyLoopFromEvolvFace() {
        if (infoOff()) {
            return;
        }

        System.out.println("EvolveFace from loop was a empty loop");
    }

    public static void soutBoolOperationStarted() {
        if (infoOff()) {
            return;
        }

        System.out.println("==== Boolean Operation Started ====");
    }

    public static void soutSplitingEdgeAtEnds() {
        if (infoOff()) {
            return;
        }

        System.out.println("Spliting edge at ends.");
    }

    public static void soutNumberOfEdgeIntersections(int ints) {
        if (infoOff()) {
            return;
        }

        System.out.println("Number of Edge Intersections: " + ints);
    }

    public static void soutNumberOfIntersectingEdges(int ints) {
        if (infoOff()) {
            return;
        }

        System.out.println("Number of Intersecting Edges: " + ints);
    }

    public static void soutCurveHasEdgeCoincident() {
        if (infoOff()) {
            return;
        }

        System.out.println("Curve has Edge Coincident");
    }

}
