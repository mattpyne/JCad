package org.pyne.jcad.core.math.brep.creators;

import de.lighti.clipper.Clipper;
import de.lighti.clipper.DefaultClipper;
import de.lighti.clipper.Path;
import de.lighti.clipper.Paths;
import eu.mihosoft.jcsg.Edge;
import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.vvecmath.Vector3d;
import org.pyne.jcad.core.collection.MapUtils;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.ClipperInterop;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.BrepSurface;
import org.pyne.jcad.core.math.brep.Topo;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.geom.MeshPoint;
import org.pyne.jcad.core.math.geom.MeshSurface;
import org.pyne.jcad.core.math.geom.NurbsSurface;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.Interval;

import java.util.*;
import java.util.stream.Collectors;

public class PolygonBrepor {

    private ArrayList<Edge> edges;
    private ArrayList<Polygon> polygons;

    private HashMap<Polygon, List<Edge>> sharedEdgeMap;

    private List<MeshSurface> meshSurfs;

    private double smoothness;

    public PolygonBrepor(List<Polygon> polygons, double smoothness) {
        this.polygons = new ArrayList<>(polygons);
        this.smoothness = smoothness;
    }

    public Topo execute() {

        BrepBuilder brepBuilder = new BrepBuilder();

        sharedEdgeMap = new HashMap<>();
        edges = new ArrayList<>();
        meshSurfs = new ArrayList<>();

        populateEdges(polygons);
        populateSharedEdgeMap();
        populatePolygonSurfaces();

        for (MeshSurface meshSurf : meshSurfs) {

            List<List<MeshPoint>> polyMeshPoints = meshSurf.polyMeshPoints();

//            NurbsSurface surf = meshSurf.toNurbs();
//
//            if (null == surf) {
//                continue;
//            }

            brepBuilder.face(new BrepSurface(meshSurf));
//            brepBuilder.face(new BrepSurface(surf));

//            Interval<Number> domainU = meshSurf.domainU();
//            Interval<Number> domainV = meshSurf.domainV();
            Paths polyPaths = new Paths();
            for (List<MeshPoint> projectionPoints : polyMeshPoints) {

                List<Point3DC> workingPoints = new ArrayList<>();
                for (MeshPoint p : projectionPoints) {

                    Point3DC wp = new Point3DC(
//                            (p.getU() - domainU.min.doubleValue()) / domainU.width().doubleValue(),
//                            (p.getV() - domainV.min.doubleValue()) / domainV.width().doubleValue(),
                            p.getU(),
                            p.getV(),
                            0)
                            .times(meshSurf.WORKING_POINT_SCALE_FACTOR);

//                    if (surf.isMirrored()) {
//                        wp.setX(wp.getX() * -1);
//                    }

                    wp.__3D = p.getP3OnMesh();

                    workingPoints.add(wp);
                }

                polyPaths.add(ClipperInterop.clipperPath(workingPoints));
            }

            Paths solutions = new Paths();
            Clipper cpr = new DefaultClipper();
            cpr.addPaths(polyPaths, Clipper.PolyType.SUBJECT, true);
            cpr.execute(
                    Clipper.ClipType.UNION,
                    solutions,
                    Clipper.PolyFillType.NON_ZERO,
                    Clipper.PolyFillType.NON_ZERO);

            // Outer Paths... should have only one outer path.
            solutions.stream()
                    .filter(p -> !p.orientation())
                    .map(ClipperInterop::createPointPathFrom)
                    .findFirst()
                    .ifPresent(pts ->
                            brepBuilder.loop(pts.stream()
                                    .map(meshSurf::workingPointTo3D)
                                    .map(Vertex::new)
                                    .collect(Collectors.toList())));

            // Inner paths
            solutions.stream()
                    .filter(Path::orientation)
                    .map(ClipperInterop::createPointPathFrom)
                    .forEach(pts ->
                            brepBuilder.loop(pts.stream()
                                    .map(meshSurf::workingPointTo3D)
                                    .map(Vertex::new)
                                    .collect(Collectors.toList())));
        }

        return brepBuilder.build();
    }

    private void populatePolygonSurfaces() {
        // create collection of polygons which are attached and have a smooth transitions
        while (!sharedEdgeMap.isEmpty()) {

            Map.Entry<Polygon, List<Edge>> sharedEdges = sharedEdgeMap.entrySet().iterator().next();

            MeshSurface polygonSurface = new MeshSurface();
            createSmoothPolygonSurface(sharedEdges.getKey(), polygonSurface);

            if (!polygonSurface.getPolygons().isEmpty()) {

                meshSurfs.add(polygonSurface);
            }
        }
    }


    private void createSmoothPolygonSurface(Polygon polygon, MeshSurface out) {

        List<Edge> sharedEdges = sharedEdgeMap.remove(polygon);
        if (null == sharedEdges) {

            return;
        }

        out.add(polygon);

        Vector3d n1 = polygon.getPlane().getNormal().normalized();
        for (Edge sharedEdge : sharedEdges) {

            Polygon nextPoly = sharedEdge.getParent();
            if (out.contains(nextPoly)) {

                continue;
            }

            Vector3d n2 = nextPoly.getPlane().getNormal().normalized();

            if (n2.minus(out.getNormal()).magnitudeSq() < Tolerance.TOLERANCE_SQ) {

                createSmoothPolygonSurface(nextPoly, out);
                continue;
            }

            if (n1.dot(n2) > 1 - smoothness
                    && n2.dot(out.getNormal()) > 0.0) {

                createSmoothPolygonSurface(nextPoly, out);
            }
        }
    }

    private void populateSharedEdgeMap() {

        // Populate map with edges in the same direction.
        HashMap<Integer, List<Edge>> directionEdgeMap = new HashMap<>();
        for (Edge edge : edges) {

            int precision = 5;
            Vector3d direction = edge.getDirection();
            double hashValue = 14 * CadMath.round(Math.abs(direction.getX()), precision);
            hashValue = hashValue * 14 + CadMath.round(Math.abs(direction.getY()), precision);
            hashValue = hashValue * 14 + CadMath.round(Math.abs(direction.getZ()), precision);
            int hash = Double.valueOf(hashValue).hashCode();

            MapUtils.addToList(directionEdgeMap, hash, edge);
        }

        // Find all shared edges and populate the sharedEdgeMap
        for (List<Edge> edges : directionEdgeMap.values()) {

            for (int i = 0; i < edges.size(); i++) {
                for (int j = i + 1; j < edges.size(); j++) {

                    Edge edgeI = edges.get(i);
                    Edge edgeJ = edges.get(j);

                    boolean p1Eq = Tolerance.veq(edgeJ.getP1().pos, edgeI.getP1().pos)
                            || Tolerance.veq(edgeJ.getP1().pos, edgeI.getP2().pos);

                    boolean p2Eq = Tolerance.veq(edgeJ.getP2().pos, new Point3DC(edgeI.getP1().pos))
                            || Tolerance.veq(edgeJ.getP2().pos, edgeI.getP2().pos);

                    if (p1Eq ^ p2Eq) {

                        continue;
                    }

                    // Shared edge
                    if (edgeI.contains(edgeJ.getP1().pos, Tolerance.TOLERANCE)
                            || edgeI.contains(edgeJ.getP2().pos, Tolerance.TOLERANCE)) {

                        MapUtils.addToList(sharedEdgeMap, edgeI.getParent(), edgeJ);
                        MapUtils.addToList(sharedEdgeMap, edgeJ.getParent(), edgeI);
                    }
                }
            }
        }

        if (sharedEdgeMap.size() != polygons.size()) {

            System.err.println("Failed to add polygons to the shared edge map");
        }
    }

    private void populateEdges(List<Polygon> polygons) {
        for (Polygon polygon : polygons) {
            edges.addAll(polygon.edges());
        }
    }

}
