package org.pyne.jcad.core.math.brep;

import org.pyne.jcad.core.DataContainer;
import eu.mihosoft.jcsg.Polygon;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.geom.CurvePrimitives;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.geom.Arc;
import verb.geom.Circle;

/**
 * Precision hash based edge.
 *
 * @author matt pyne
 */
public class Edge extends DataContainer implements Topo {

    private HalfEdge halfEdge1;
    private HalfEdge halfEdge2;
    private BrepCurve curve;
    private Set<Polygon> parents;


    Edge() {

    }

    /**
     * @param curve
     */
    public Edge(BrepCurve curve) {
        this(curve, new Vertex(curve.point(0)), new Vertex(curve.point(1)));
    }

    /**
     * @param curve
     * @param point1
     * @param point2
     */
    public Edge(
            BrepCurve curve,
            Vertex point1,
            Vertex point2) {

        this.curve = curve;
        Point3DC minPt = point1.point();
        Point3DC maxPt = point2.point();
        double uMin = curve.param(minPt);
        double uMax = curve.param(maxPt);

        if (curve.isClosed() && uMax <= uMin && CadMath.isZero(uMax)) {
            uMax = 1.;
        }

        if (curve.getType() == ParametricCurve.TYPE.CIRCLE) {

            if (uMax <= uMin) {
                Debug.log(Debug.LEVEL.ERROR, "Umax less than Umin when creating edge. Wrong orientation?");

                uMax += 1;
            }

            Circle circle = (Circle) curve.getImpl().asNurbs().getVerb();
            this.curve = new BrepCurve(
                    new NurbsCurve(
                            new Arc(
                                    circle.center().copy(),
                                    circle.xaxis().copy(),
                                    circle.yaxis().copy(),
                                    circle.radius(),
                                    uMin * 2 * Math.PI,
                                    uMax * 2 * Math.PI
                            )));

        } else {

            if (uMax < uMin) {
                Debug.log(Debug.LEVEL.ERROR, "Umax less that Umin when creating edge. Wrong orientation?");

                Point3DC tempPt = minPt;
                minPt = maxPt;
                maxPt = tempPt;
                double tempU = uMin;
                uMin = uMax;
                uMax = tempU;
            }

            if (!Tolerance.eqEps(uMax, 1.0)) {
                List<BrepCurve> splitCurves = this.curve.split(maxPt);
                if (splitCurves.size() == 2) {
                    double dist0 = splitCurves.get(0).end().distanceSq(maxPt);

                    double dist1 = splitCurves.get(1).end().distanceSq(maxPt);

                    if (dist0 < dist1) {
                        this.curve = splitCurves.get(0);
                    } else {
                        this.curve = splitCurves.get(1);
                    }
                }
            }

            if (!Tolerance.eqEps(uMin, 0.0)) {
                List<BrepCurve> splitCurves = this.curve.split(minPt);
                if (splitCurves.size() == 2) {
                    double dist0 = splitCurves.get(0).end().distanceSq(maxPt);
                    dist0 += splitCurves.get(0).start().distanceSq(minPt);

                    double dist1 = splitCurves.get(1).end().distanceSq(maxPt);
                    dist1 += splitCurves.get(1).end().distanceSq(minPt);

                    if (dist0 < dist1) {
                        this.curve = splitCurves.get(0);
                    } else {
                        this.curve = splitCurves.get(1);
                    }
                }
            }

        }

        this.halfEdge1 = new HalfEdge(this, false, point1, point2);
        this.halfEdge2 = new HalfEdge(this, true, point2, point1);

        if (Debug.ON) {
            if (!this.halfEdge1.tessellate().get(0).equals(point1.point())) {
                Debug.log(Debug.LEVEL.ERROR, "New halfedge1 is tessellated in the wrong direction?");
            }
            if (!this.halfEdge2.tessellate().get(0).equals(point2.point())) {
                Debug.log(Debug.LEVEL.ERROR, "New halfedge2 is tessellated in the wrong direction?");
            }
        }

        this.parents = new HashSet<>();
    }

    /**
     * @param other
     */
    private Edge(Edge other) {
        this(other.curve.copy(), other.halfEdge1.vertexA().copy(), other.halfEdge1.vertexB().copy());
    }

    @Override
    public Edge clone() {
        Edge clone = new Edge();

        clone.curve = curve.copy();

        clone.halfEdge1 = this.halfEdge1.copy();
        clone.halfEdge1.setEdge(clone);
        clone.halfEdge2 = this.halfEdge2.copy();
        clone.halfEdge2.setEdge(clone);

        clone.setId(this.getId());
        return clone;
    }

    public void invert() {
        HalfEdge t = this.halfEdge1;
        this.halfEdge1 = this.halfEdge2;
        this.halfEdge2 = t;
        this.halfEdge1.setInverted(false);
        this.halfEdge2.setInverted(true);
        this.curve = curve.invert();
    }

    public void link(HalfEdge halfEdge1, HalfEdge halfEdge2) {
        halfEdge1.setEdge(this);
        halfEdge2.setEdge(this);
        this.halfEdge1 = halfEdge1;
        this.halfEdge2 = halfEdge2;
    }

    public HalfEdge halfEdge1() {
        return halfEdge1;
    }

    public void setHalfEdge1(HalfEdge halfEdge1) {
        this.halfEdge1 = halfEdge1;
    }

    public HalfEdge halfEdge2() {
        return halfEdge2;
    }

    public void setHalfEdge2(HalfEdge halfEdge2) {
        this.halfEdge2 = halfEdge2;
    }

    public BrepCurve curve() {
        return curve;
    }

    public void setCurve(BrepCurve curve) {
        this.curve = curve;
        cache().clear();
    }

    /**
     * @return
     */
    public Set<Polygon> getParents() {
        return parents;
    }

    /**
     * @return
     */
    public Stream<Polygon> streamParents() {
        return parents.stream();
    }

    /**
     * @param parent
     */
    public void addParent(Polygon parent) {
        if (null != parent) {
            parents.add(parent);
        }
    }

    /**
     * @param parents
     */
    public void addAllParent(Collection<Polygon> parents) {
        parents.forEach(this::addParent);
    }

    /**
     * @return
     */
    @Override
    public Edge copy() {
        return new Edge(this);
    }

    public static Edge fromCurve(BrepCurve curve) {
        return new Edge(curve,
                new Vertex(curve.point(0)),
                new Vertex(curve.point(1.)));
    }

    /**
     * @param tr
     */
    @Override
    public void transform(Matrix3 tr) {
        cache().clear();
        halfEdge1.vertexA().transform(tr);
        halfEdge1.vertexB().transform(tr);
        curve = curve.transform(tr);
    }

    @Override
    public Edge transformed(Matrix3 tr) {
        Edge instance = copy();

        instance.transform(tr);

        return instance;
    }

    @Override
    public boolean isEmpty() {
        return null == halfEdge1 || null == halfEdge2;
    }
}
