package org.pyne.jcad.core.entity;

import java.util.List;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.creators.BrepTessellator;

/**
 *
 * @author Matthew Pyne
 */
public class Mesh implements IComponent {

    private BrepTessellator tessellator;
    private Entity entity;

    public Mesh() {
    }

    public List<List<Point3DC>> triangles() {
        return tessellator.surfaceTriangles();
    }

    @Override
    public void invalidate() {
        tessellator = null;
        
    }

    @Override
    public void update() {
        if (null == tessellator) {

            tessellator = new BrepTessellator();

            entity.getComponent(Geometry.class)
                    .map(Geometry::getTopo)
                    .ifPresent(tessellator::add);
        }
    }

    @Override
    public void register(Entity entity) {
        this.entity = entity;
    }

}
