package org.pyne.jcad.core.math.brep.eval.bool;

import static org.pyne.jcad.core.math.brep.eval.bool.ShellBooleanOps.MY;
import org.pyne.jcad.core.math.brep.Edge;

/**
 *
 * @author Matthew Pyne
 */
public class EdgeSolveData {

    private static final EdgeSolveData EMPTY = new EdgeSolveData();

    boolean transfered = false;

    public static EdgeSolveData get(Edge edge) {
        if (!edge.hasData(MY, EdgeSolveData.class)) {
            return EdgeSolveData.EMPTY;
        }
        return edge.data(MY, EdgeSolveData.class).get();
    }

    public static EdgeSolveData createIfEmpty(Edge edge) {
        if (!edge.hasData(MY, EdgeSolveData.class)) {
            edge.putData(MY, new EdgeSolveData());
        }
        return edge.data(MY, EdgeSolveData.class).get();
    }

    public static boolean isEdgeTransferred(Edge edge) {
        return get(edge).transfered;
    }

    public static void markEdgeTransferred(Edge edge) {
        createIfEmpty(edge).transfered = true;
    }
    
    public static void clear(Edge edge) {
        if (null == edge) {
            return;
        }
        edge.delete(MY);
    }
}
