package org.pyne.jcad.core.math.geom;

import eu.mihosoft.vvecmath.Plane;
import eu.mihosoft.vvecmath.Vector3d;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import haxe.root.Array;
import org.pyne.jcad.core.math.*;
import verb.core.Interval;

/**
 * @author matt pyne
 */
public class PlanarSurface extends Surface {

    private Integer normalPlaneHash;
    private ParametricPlane parametricPlane;
    private Matrix3 transform3d;
    private Matrix3 transform2d;
    private Point3DC normal;
    private Axis3 basis;
    private double w;
    private final Interval<Number> domainV;
    private final Interval<Number> domainU;

    /**
     *
     */
    public PlanarSurface() {
        super();
        domainU = new Interval<>(-1e8, 1e8);
        domainV = new Interval<>(-1e8, 1e8);
    }

    public PlanarSurface(List<Point3DC> points) {
        this();
        normal = PathMath.normalOfCCWSeg(points);
        w = points.get(0).dot(normal);
    }

    public PlanarSurface(Point3DC normal, double w) {
        this();
        this.normal = normal;
        this.w = w;
    }

    /**
     * @param other
     */
    public PlanarSurface(PlanarSurface other) {
        super(other);
        normalPlaneHash = other.normalPlaneHash;
        w = other.w;
        normal = other.normal.copy();
        domainU = new Interval<>(other.domainU.min, other.domainU.max);
        domainV = new Interval<>(other.domainV.min, other.domainV.max);
    }

    public Axis3 calculateBasis() {
        return L3Space.basisForPlane(normal);
    }

    public Axis3 basis() {
        if (null != basis) {
            basis = calculateBasis();
        }
        return basis;
    }

    @Override
    public List<Curve> intersectSurfaceForSameClass(Surface other, double tol) {
        if (other instanceof PlanarSurface) {
            return new ArrayList<>();
        }

        Optional<Line> line = Line.fromTwoPlanesIntersection(this, (PlanarSurface) other);
        return line.<List<Curve>>map(Arrays::asList).orElseGet(ArrayList::new);

    }

    /**
     * @return
     */
    @Override
    public PlanarSurface copy() {
        return new PlanarSurface(this);
    }

    /**
     * @param point
     * @return
     */
    @Override
    public Point3DC normal(Point3DC point) {
        return normal;
    }

    @Override
    public Point3DC normalUV(double u, double v) {
        return normal;
    }

    /**
     * @return
     */
    public double w() {

        return w;
    }

    /**
     * @return
     */
    public Axis3 getBasis() {
        if (null == basis) {

            basis = L3Space.basisForPlane(normal);
        }
        return basis;
    }

    /**
     * @return
     */
    public ParametricPlane toParametricForm() {
        if (null == parametricPlane) {

//            Axis3 b = getBasis();
            parametricPlane = new ParametricPlane(
                    normal,
                    w);
        }
        return parametricPlane;
    }

    /**
     * @return
     */
    public Matrix3 get2DTransformation() {
        if (null == transform3d) {

            transform2d = get3DTransformation()
                    .invert()
                    .orElse(new Matrix3());
        }
        return transform2d;
    }

    /**
     * @return
     */
    public Matrix3 get3DTransformation() {
        if (null == transform3d) {

            Matrix3 basis = new Matrix3().setBasis(getBasis());
            Matrix3 translate = new Matrix3();
            translate.translate(0, 0, w);
            transform3d = basis.combine(translate);
        }
        return transform3d;
    }

    public boolean coplanarUnsigned(PlanarSurface other) {
        return normal.times(w).equals(other.normal.times(other.w));
    }

    /**
     * @return
     */
    @Override
    public PlanarSurface invert() {
        return new PlanarSurface(normal.negated(), -w);
    }

    /**
     * @param translate
     * @return
     */
    public PlanarSurface translate(Vector3d translate) {
        return new PlanarSurface(normal, normal.dot(normal.times(w).plus(translate)));
    }

    public Point3DC project(Point3DC point) {
        return point.minus(normal.times(normal.dot(point) - w));
    }

    /**
     * @param point
     * @return
     */
    public Array<Number> param(Point3DC point) {

        return toParametricForm().param(project(point));
    }

    /**
     * @param u
     * @param v
     * @return
     */
    public Point3DC point(double u, double v) {

        return toParametricForm().point(u, v);
    }

//    /**
//     * @param point
//     * @return
//     */
//    public Array<Number> param(Point3D point) {
//
//        Point3D p = get2DTransformation().apply(point);
//
//        return new Array<>(new Number[]{p.getX(), p.getY()});
//    }
//
//    /**
//     * @param u
//     * @param v
//     * @return
//     */
//    public Point3D point(double u, double v) {
//
//        return get3DTransformation().apply(Vector3d.xy(u, v));
//    }

    @Override
    public PlanarSurface tangentPlane(double u, double v) {
        return this;
    }

    @Override
    public HashSet<PlanarSurface> getApproxSurface() {
        HashSet<PlanarSurface> surfs = new HashSet<>();

        surfs.add(this);

        return surfs;
    }

    @Override
    public NurbsSurface toNurbs() {
        Point3DC p0 = point(domainU().min.doubleValue(), domainV().min.doubleValue());
        Point3DC p1 = point(domainU().min.doubleValue(), domainV().max.doubleValue());
        Point3DC p2 = point(domainU().max.doubleValue(), domainV().max.doubleValue());
        Point3DC p3 = point(domainU().max.doubleValue(), domainV().min.doubleValue());

        return new NurbsSurface(verb.geom.NurbsSurface.byCorners(
                p0.asArray(), p1.asArray(), p2.asArray(), p3.asArray()
        ));
    }

    @Override
    public Interval<Number> domainU() {
        return domainU;
    }

    @Override
    public Interval<Number> domainV() {
        return domainV;
    }

    @Override
    public boolean contains(Point3DC p) {
        return p.minus(closestPoint(p)).magnitudeSq() < Tolerance.TOLERANCE;
    }

    @Override
    public int hashCode() {
        // If no hash try creating one from the set the polygons. All we need
        // is a plane.
        if (null == normalPlaneHash) {

            normalPlaneHash = normal.hashCode();
            normalPlaneHash += 80 * Double.hashCode(CadMath.round(w, 5));
        }

        return normalPlaneHash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlanarSurface other = (PlanarSurface) obj;
        return Objects.equals(other.hashCode(), hashCode());
    }

    private void clearCache() {
        parametricPlane = null;
        transform2d = null;
        transform3d = null;
        basis = null;
    }

}
