package org.pyne.jcad.core.math.geom;

import java.util.*;

import haxe.root.Array;
import org.pyne.jcad.core.math.Point3DC;

/**
 *
 * @author matt pyne
 */
public class ApproxSurface extends Surface {

    private Set<PlanarSurface> planarSurfs;

    /**
     *
     */
    public ApproxSurface() {
        planarSurfs = new HashSet<>();
    }

    /**
     *
     * @param other
     */
    public ApproxSurface(ApproxSurface other) {
        super(other);
        planarSurfs = new HashSet<>();
    }

    /**
     *
     * @param surface
     */
    public void addSurface(PlanarSurface surface) {
        if (null != surface) {
            planarSurfs.add(surface);
        }
    }

    /**
     *
     * @param surfaces
     */
    public void addAllSurface(Collection<PlanarSurface> surfaces) {
        if (null != surfaces && !surfaces.isEmpty()) {
            surfaces.forEach(this::addSurface);
        }
    }

    /**
     *
     * @param surface
     */
    public void addSurface(ApproxSurface surface) {
        if (null != surface) {
            surface.getPlanarSurfs()
                    .forEach(this::addSurface);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public HashSet<PlanarSurface> getApproxSurface() {
        return new HashSet<>(planarSurfs);
    }

    /**
     *
     * @return
     */
    public Set<PlanarSurface> getPlanarSurfs() {
        return planarSurfs;
    }

    @Override
    public ApproxSurface copy() {
        return new ApproxSurface(this);
    }

    @Override
    public List<Curve> intersectSurfaceForSameClass(Surface other, double tol) {
        List<Curve> ints = new ArrayList<>();

        for (PlanarSurface ps1 : other.getApproxSurface()) {
            for (PlanarSurface ps2 : getApproxSurface()) {

                ints.addAll(ps1.intersectSurfaceForSameClass(ps2, Tolerance.TOLERANCE));
            }
        }

        return ints;
    }

    @Override
    public PlanarSurface tangentPlane(double u, double v) {
        return null;
    }

    @Override
    public Point3DC point(double u, double v) {
        return null;
    }

    @Override
    public Array<Number> param(Point3DC point) {
        return null;
    }

    @Override
    public Point3DC normal(Point3DC point) {
        for (PlanarSurface ps : planarSurfs) {

            if (ps.contains(point)) {

                return ps.normal(point);
            }
        }

        return Point3DC.ZERO;
    }

    @Override
    public Point3DC normalUV(double u, double v) {
        return normal(point(u, v));
    }

    @Override
    public NurbsSurface toNurbs() {
        return null;
    }

    @Override
    public boolean contains(Point3DC pt) {
        return planarSurfs.stream()
                .anyMatch(ps -> ps.contains(pt));
    }


}
