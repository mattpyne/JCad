package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.brep.HalfEdge;

/**
 *
 * @author Matthew
 */
class EdgeSplitInfo {

    HalfEdge edge;
    double u;

    public EdgeSplitInfo() {
    }

    public EdgeSplitInfo(HalfEdge edge, double u) {
        this.edge = edge;
        this.u = u;
    }
}
