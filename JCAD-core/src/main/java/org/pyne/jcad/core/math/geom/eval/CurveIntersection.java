package org.pyne.jcad.core.math.geom.eval;

import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.pyne.jcad.core.verbinterop.VerbNurbsHelpers;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Optimization;
import org.pyne.jcad.core.math.geom.NurbsCurve;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import verb.core.Interval;
import verb.core.NurbsCurveData;
import verb.core.Vec;
import verb.eval.Eval;

/**
 *
 * @author Matthew Pyne
 */
public class CurveIntersection {

    public static Array<Number> curveExactIntersection(NurbsCurveData curve1, NurbsCurveData curve2, Number u1, Number u2) {

        Function f = (Function<Array<Number>, Double>) (Array<Number> t) -> {
            double x1 = t.get(0).doubleValue();
            double x2 = t.get(1).doubleValue();
            Array<Number> u1Su2 = Vec.sub(VerbNurbsHelpers.curvePoint(curve1, x1), VerbNurbsHelpers.curvePoint(curve2, x2));
            return Vec.dot(u1Su2, u1Su2);
        };

        Function grad = (Function<Array<Number>, Array<Number>>) (Array<Number> t) -> {
            double x1 = t.get(0).doubleValue();
            double x2 = t.get(1).doubleValue();
            Array<Array<Number>> d1 = Eval.rationalCurveDerivatives(curve1, x1, 1);
            Array<Array<Number>> d2 = Eval.rationalCurveDerivatives(curve2, x2, 1);
            Array<Number> r = Vec.sub(d1.get(0), d2.get(0));
            Array<Number> drdu = d1.get(1);
            Array<Number> drdt = Vec.mul(-1, d2.get(1));

            Array<Number> result = new Array();
            result.push(2 * Vec.dot(drdu, r));
            result.push(2 * Vec.dot(drdt, r));
            return result;
        };

        Array<Number> params = new Array();
        params.push(u1);
        params.push(u2);

        return Optimization.fmin_bfgs(f, params, CadMath.EPSILON_SQ, grad).getSolution();
    }
    
    /**
     *
     * @param curve1
     * @param curve2
     * @return Number, Number, Array<Number> Array<Number.
     */
    public static Array<CurveIntersectData> curveIntersect(NurbsCurveData curve1, NurbsCurveData curve2) {

        Array<CurveIntersectData> result = new Array();
        Array<Array<Number>> segs1 = CurveTessellate.curveTessellatePoints(curve1);
        Array<Array<Number>> segs2 = CurveTessellate.curveTessellatePoints(curve2);

        for (int i = 0; i < segs1.length - 1; i++) {
            Array<Number> a1 = segs1.get(i);
            Array<Number> b1 = segs1.get(i + 1);
            for (int j = 0; j < segs2.length - 1; j++) {
                Array<Number> a2 = segs2.get(j);
                Array<Number> b2 = segs2.get(j + 1);

                //TODO: minimize
                Array<Object> isec = intersectSegs(a1, b1, a2, b2);
                if (null != isec) {
                    Array<Number> point1 = (Array<Number>) isec.get(0);
                    Array<Number> point2 = (Array<Number>) isec.get(1);
                    Number l1 = (Number) isec.get(4);
                    Number l2 = (Number) isec.get(5);

                    Number u1 = VerbNurbsHelpers.curveClosestParam(curve1, point1);
                    Number u2 = VerbNurbsHelpers.curveClosestParam(curve2, point2);

                    Array<Number> u1u2Exact = curveExactIntersection(curve1, curve2, u1, u2);
                    u1 = u1u2Exact.get(0);
                    u2 = u1u2Exact.get(1);

                    result.push(new CurveIntersectData(u1, u2, point1, point2));
                    if (CadMath.eq(u1, l1)) {
                        i++;
                    }
                    if (CadMath.eq(u2, l2)) {
                        j++;
                    }
                }
            }
        }
        return result;
    }
    
    public static List<CurveIntersectData> curveInterset(
            ParametricCurve curve1,
            ParametricCurve curve2,
            Interval<Number> isecRange1,
            Interval<Number> isecRange2) {

        NurbsCurve nCurve1 = curve1.asNurbs();
        NurbsCurve nCurve2 = curve2.asNurbs();
        
        List<CurveIntersectData> result = new ArrayList();
        double uMin1 = isecRange1.min.doubleValue();
        double uMax1 = isecRange1.max.doubleValue();
        double uMin2 = isecRange2.min.doubleValue();
        double uMax2 = isecRange2.max.doubleValue();
        List<Point3DC> points1 = nCurve1.tessellate(uMin1, uMax1, CadMath.TOLERANCE_SQ);
        List<Point3DC> points2 = nCurve2.tessellate(uMin2, uMax2, CadMath.TOLERANCE_SQ);

        for (int i = 0; i < points1.size() - 1; i++) {
            Array<Number> a1 = points1.get(i).asArray();
            Array<Number> b1 = points1.get(i + 1).asArray();
            for (int j = 0; j < points2.size() - 1; j++) {
                Array<Number> a2 = points2.get(j).asArray();
                Array<Number> b2 = points2.get(j + 1).asArray();

                //TODO: minimize
                Array<Object> isec = intersectSegs(a1, b1, a2, b2);
                if (null != isec) {
                    Array<Number> point1 = (Array<Number>) isec.get(0);
                    Array<Number> point2 = (Array<Number>) isec.get(1);
                    Number l1 = (Number) isec.__a[4];
                    Number l2 = (Number) isec.__a[5];

                    Number u1 = curve1.param(new Point3DC(point1));
                    Number u2 = curve2.param(new Point3DC(point2));
                    
                    Array<Number> u1u2Exact = curveExactIntersection(nCurve1.getData(), nCurve2.getData(), u1, u2);
                    u1 = u1u2Exact.get(0);
                    u2 = u1u2Exact.get(1);

                    if (!isInRange(u1.doubleValue(), isecRange1) || !isInRange(u2.doubleValue(), isecRange2)) {
                        continue;
                    }
                    result.add(new CurveIntersectData(u1, u2, point1, point2));
                    if (CadMath.eq(u1, l1)) {
                        i++;
                    }
                    if (CadMath.eq(u2, l2)) {
                        j++;
                    }
                }
            }
        }
        return result;
    }

    /**
     *
     * @param a1
     * @param b1
     * @param a2
     * @param b2
     * @return
     */
    public static Array<Object> intersectSegs(Array<Number> a1, Array<Number> b1, Array<Number> a2, Array<Number> b2) {

        Point3DC a1V = new Point3DC(a1);
        Point3DC a2V = new Point3DC(a2);
        Point3DC b1V = new Point3DC(b1);
        Point3DC b2V = new Point3DC(b2);

        Point3DC v1 = b1V.minus(a1V);
        Point3DC v2 = b2V.minus(a2V);
        double l1 = v1.magnitude();
        double l2 = v2.magnitude();
        v1 = v1.divided(l1);
        v2 = v2.divided(l2);

        Array<Number> u1u2 = lineLineIntersection(a1V, a2V, v1, v2);
        double u1 = u1u2.get(0).doubleValue();
        double u2 = u1u2.get(1).doubleValue();
        Point3DC point1 = a1V.plus(v1.times(u1));
        Point3DC point2 = a2V.plus(v2.times(u2));
        double p2p1L = point1.distanceSq(point2);

        if (!Double.isInfinite(u1) && !Double.isInfinite(u2)
                && CadMath.eq(p2p1L, 0, CadMath.TOLERANCE_SQ)
                && ((u1 > 0 && u1 < l1) || CadMath.eq(u1, 0) || CadMath.eq(u1, l1))
                && ((u2 > 0 && u2 < l2) || CadMath.eq(u2, 0) || CadMath.eq(u2, l2))) {

            return new Array(new Object[]{point1.asArray(), point2.asArray(), u1, u2, l1, l2});
        }

        return null;
    }

    /**
     *
     * @param p1
     * @param p2
     * @param v1
     * @param v2
     * @return u1u2
     */
    public static Array<Number> lineLineIntersection(Point3DC p1, Point3DC p2, Point3DC v1, Point3DC v2) {

        Point3DC zAx = v1.crossed(v2);
        Point3DC n1 = zAx.crossed(v1).normalized();
        Point3DC n2 = zAx.crossed(v2).normalized();
        return new Array(new Number[]{
            n2.dot(p2.minus(p1)) / n2.dot(v1),
            n1.dot(p1.minus(p2)) / n1.dot(v2)});
    }

    /**
     *
     * @param p
     * @param range
     * @return
     */
    private static boolean isInRange(double p, Interval<Number> range) {
        return p >= range.min.doubleValue() && p <= range.max.doubleValue();
    }
}
