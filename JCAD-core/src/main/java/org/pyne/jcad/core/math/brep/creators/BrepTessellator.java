package org.pyne.jcad.core.math.brep.creators;

import de.lighti.clipper.Clipper;
import de.lighti.clipper.DefaultClipper;
import de.lighti.clipper.Path;
import de.lighti.clipper.Paths;
import eu.mihosoft.jcsg.Polygon;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.csg.JCSGInterop;
import org.pyne.jcad.core.math.*;
import org.pyne.jcad.core.math.brep.*;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.Interval;
import verb.core.MeshData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Matthew Pyne
 */
public class BrepTessellator {

    private boolean performed;
    private HashSet<Topo> topos;

    private List<List<Point3DC>> surfaceTriangles;

    /**
     *
     */
    public BrepTessellator() {
    }

    public void add(Topo topo) {

        if (null == topos) {

            topos = new HashSet<>();
        }

        topos.addAll(new TopoExplorer(topo).faces().collect(Collectors.toList()));
    }

    public void perform() {

        performed = true;
        if (null == surfaceTriangles) {

            surfaceTriangles = new ArrayList<>();
        }

        if (null == topos) {
            return;
        }

        for (Topo topo : topos) {

            if (topo instanceof Face) {
                surfaceTriangles.addAll(tessellate((Face) topo));
            }
        }
    }

    public List<List<Point3DC>> surfaceTriangles() {
        if (!performed) {

            perform();
        }

        return surfaceTriangles;
    }


    public void clear() {
        surfaceTriangles.clear();
        performed = false;
        topos = null;
    }

    public static List<List<Point3DC>> tessellate(Shell shell) {
        return shell.faces()
                .stream()
                .flatMap(f -> tessellate(f).stream())
                .collect(Collectors.toList());
    }

    public static List<List<Point3DC>> tessellate(Face face) {
        return tessellate(face.surface().impl(), face);
    }

    private static List<List<Point3DC>> tessellate(
            Surface surf,
            Face face) {

        Loop outerLoop = face.getOuterLoop();
        List<Point3DC> outerPointLoop = pointLoop(outerLoop, surf);

        List<List<Point3DC>> innerPointsLoops = new ArrayList<>();
        for (Loop loop : face.getInnerLoops()) {

            List<Point3DC> innerPointLoop = pointLoop(loop, surf);

            innerPointsLoops.add(innerPointLoop);
        }

        MeshData tess = surf.tessellate();
        List<List<Point3DC>> nurbsTriangles = tess.faces.stream()
                .filter(Objects::nonNull)
                .map(f -> f.stream()
                        .map(i -> surf.createWorkingPoint(
                                tess.uvs.get(i.intValue()),
                                new Point3DC(tess.points.get(i.intValue()))))
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        if (PathMath.isSelfIntersected(outerPointLoop)) {

//            return nurbsTriangles.stream()
//                    .map(tr -> tr.stream()
//                            .map(surf::workingPointTo3D)
//                            .collect(Collectors.toList()))
//                    //.filter(tr -> tr.stream().anyMatch(p -> face.bounds().contains(p.asArray(), Tolerance.TOLERANCE)))
//                    .collect(Collectors.toList());
        }

        try {
            List<List<Point3DC>> paths = clip(nurbsTriangles, outerPointLoop, innerPointsLoops);

            List<List<Point3DC>> triangles = new ArrayList<>();

            for (List<Point3DC> path : paths) {
                if (path.size() == 3) {
                    triangles.add(path);
                    continue;
                }
                triangles.addAll(triangulate(path));
            }

            return triangles.stream()
                    .map(tr -> tr.stream()
                            .map(surf::workingPointTo3D)
                            .collect(Collectors.toList()))
                    //.filter(tr -> tr.stream().anyMatch(p -> face.bounds().contains(p.asArray(), Tolerance.TOLERANCE)))
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            return nurbsTriangles.stream()
                    .map(tr -> tr.stream()
                            .map(surf::workingPointTo3D)
                            .collect(Collectors.toList()))
                    //.filter(tr -> tr.stream().anyMatch(p -> face.bounds().contains(p.asArray(), Tolerance.TOLERANCE)))
                    .collect(Collectors.toList());
        }

    }

    public static List<List<Point3DC>> tessellate(BrepSurface surf) {
        MeshData tess = surf.tessellate();
        return tess.faces.stream()
                .filter(Objects::nonNull)
                .map(f -> f.stream().map(i -> new Point3DC(tess.points.get(i.intValue()))).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    public static List<Point3DC> pointLoop(Loop outerLoop, Surface surf) {

        List<Point3DC> loopPoints = outerLoop.tessellate();

        Interval<Number> dU = surf.domainU();
        Interval<Number> dV = surf.domainV();

        double workingDUMax = dU.max.doubleValue() * Surface.WORKING_POINT_SCALE_FACTOR;
        double workingDUMin = dU.min.doubleValue() * Surface.WORKING_POINT_SCALE_FACTOR;
        double workingDVMax = dV.max.doubleValue() * Surface.WORKING_POINT_SCALE_FACTOR;
        double workingDVMin = dV.min.doubleValue() * Surface.WORKING_POINT_SCALE_FACTOR;

        Point3DC p0 = surf.point(dU.min.doubleValue(), dV.min.doubleValue());
        boolean uClosed = p0.equals(surf.point(dU.max.doubleValue(), dV.min.doubleValue()));
        boolean vClosed = p0.equals(surf.point(dU.min.doubleValue(), dV.max.doubleValue()));

        List<Point3DC> pipLoop = new ArrayList();
        for (Point3DC loopPoint : loopPoints) {
            Point3DC wp = surf.workingPoint(loopPoint);

            if (wp.getX() > workingDUMax || wp.getX() < workingDUMin) {
                Debug.log(Debug.LEVEL.ERROR, "Bad working point out side domain. " +
                        "Last time this is was param finding calculation problem with cylinders.");
            }
            if (wp.getY() > workingDVMax || wp.getY() < workingDVMin) {
                Debug.log(Debug.LEVEL.ERROR, "Bad working point out side domain. " +
                        "Last time this is was param finding calculation problem with cylinders.");
            }

            if (pipLoop.isEmpty()) {
                pipLoop.add(wp);
                continue;
            }

            if (uClosed && (CadMath.eq(wp.x(), workingDUMax) || CadMath.eq(wp.x(), workingDUMin))) {
                Point3DC pj = pipLoop.get(pipLoop.size() - 1);

                double dU0 = Math.abs(pj.x() - workingDUMax);
                double dU0p = Math.abs(pj.x() - workingDUMin);
                if (dU0 < dU0p) {
                    wp.setX(workingDUMax);
                } else {
                    wp.setX(workingDUMin);
                }
            }

            if (vClosed && (CadMath.eq(wp.y(), workingDVMax) || CadMath.eq(wp.y(), workingDVMin))) {
                Point3DC pj = pipLoop.get(pipLoop.size() - 1);

                double dV0 = Math.abs(pj.y() - workingDVMax);
                double dV0p = Math.abs(pj.y() - workingDVMin);
                if (dV0 < dV0p) {
                    wp.setY(workingDVMax);
                } else {
                    wp.setY(workingDVMin);
                }
            }

            if (!pipLoop.get(pipLoop.size() - 1).equals(wp)) {
                pipLoop.add(wp);
            }
        }

        if (pipLoop.isEmpty()) {
            return pipLoop;
        }

        Point3DC wP0 = pipLoop.get(0);
        if (uClosed && (CadMath.eq(wP0.x(), workingDUMax) || CadMath.eq(wP0.x(), workingDUMin))) {
            Point3DC pj = pipLoop.get(1);

            double dU0 = Math.abs(pj.x() - workingDUMax);
            double dU0p = Math.abs(pj.x() - workingDUMin);
            if (dU0 < dU0p) {
                wP0.setX(workingDUMax);
            } else {
                wP0.setX(workingDUMin);
            }
        }

        if (vClosed && (CadMath.eq(wP0.y(), workingDVMax) || CadMath.eq(wP0.y(), workingDVMin))) {
            Point3DC pj = pipLoop.get(1);

            double dV0 = Math.abs(pj.y() - workingDVMax);
            double dV0p = Math.abs(pj.y() - workingDVMin);
            if (dV0 < dV0p) {
                wP0.setY(workingDVMax);
            } else {
                wP0.setY(workingDVMin);
            }
        }

        return pipLoop;
    }

    /**
     * @param path
     * @return
     */
    public static List<List<Point3DC>> triangulate(List<Point3DC> path) {

        return Polygon.fromPoints(path).toTriangles()
                .stream()
                .map(Polygon::getVertices3d)
                .map(JCSGInterop::pointFrom)
                .collect(Collectors.toList());
    }

    private static List<List<Point3DC>> clip(
            List<List<Point3DC>> nurbsTriangles,
            List<Point3DC> outerLoop,
            List<List<Point3DC>> innerloops) {

        if (null == outerLoop || outerLoop.isEmpty()) {

            return nurbsTriangles;
        }

        boolean triInCCW = PathMath.isCCW(nurbsTriangles.get(0));

        Paths clipPaths = new Paths();
        Path outerPath = ClipperInterop.clipperPath(outerLoop);

        clipPaths.add(outerPath);

        innerloops.forEach(loop -> {

            Path innerPath = ClipperInterop.clipperPath(loop);

            clipPaths.add(innerPath);
        });

        Paths solutionPaths = new Paths();
        for (List<Point3DC> tr : nurbsTriangles) {

            Clipper cpr = new DefaultClipper();
            Path subjPath = ClipperInterop.clipperPath(tr);

            cpr.addPaths(clipPaths, Clipper.PolyType.SUBJECT, true);
            cpr.addPath(subjPath, Clipper.PolyType.CLIP, true);

            Paths paths = new Paths();
            cpr.execute(
                    Clipper.ClipType.INTERSECTION,
                    paths,
                    Clipper.PolyFillType.EVEN_ODD,
                    Clipper.PolyFillType.EVEN_ODD);

            // Handle triangles which contain holes
            List<Path> holes = paths.stream()
                    .filter(p -> !p.orientation())
                    .collect(Collectors.toList());

            List<Path> nonHoles = paths.stream()
                    .filter(p -> p.orientation())
                    .collect(Collectors.toList());

            if (nonHoles.size() == 1 && holes.size() > 0) {

                Polygon poly = Polygon.fromPoints(ClipperInterop.createPointPathFrom(nonHoles.get(0)));

                holes.forEach(hole -> {

                    poly.addHole(Polygon.fromPoints(ClipperInterop.createPointPathFrom(hole)));
                });

                solutionPaths.addAll(poly.toTriangles2()
                        .stream()
                        .map(Polygon::getVertices3d)
                        .map(JCSGInterop::pointFrom)
                        .map(ClipperInterop::clipperPath)
                        .collect(Collectors.toList()));

                continue;
            }

            if (nonHoles.size() == 1) {

                if (PathMath.isCCW(ClipperInterop.createPointPathFrom(nonHoles.get(0))) != PathMath.isCCW(tr)) {

                    nonHoles.get(0).reverse();

                }
            }
            solutionPaths.addAll(paths);
        }

        return solutionPaths.stream()
                .map(ClipperInterop::createPointPathFrom)
                .map(triOut -> {

                    if (PathMath.isCCW(triOut) != triInCCW) {
                        Collections.reverse(triOut);
                    }

                    return triOut;
                })
                .collect(Collectors.toList());
    }


}
