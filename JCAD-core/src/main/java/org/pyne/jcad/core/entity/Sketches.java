package org.pyne.jcad.core.entity;

import java.util.ArrayList;

/**
 *
 * @author Matthew Pyne
 */
public class Sketches extends ArrayList<Sketch> implements IComponent {

    public Sketches() {
    }

    @Override
    public void invalidate() {
    }

    @Override
    public void update() {
    }

    @Override
    public void register(Entity entity) {
    }

}
