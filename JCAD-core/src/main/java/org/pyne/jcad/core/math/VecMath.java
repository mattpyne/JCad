package org.pyne.jcad.core.math;

import org.pyne.jcad.core.math.geom.Tolerance;

/**
 *
 * @author Matthew Pyne
 */
public class VecMath {

    public static double leftTurningMeasure(Point3DC v1, Point3DC v2, Point3DC normal) {
        double measure = v1.dot(v2);
        if (Tolerance.ueq(measure, 1)) {
            return 0;
        }
        measure += 3;
        if (v1.crossed(v2).dot(normal) < 0) {
            measure = 4 - measure;
        }
        // make it positive all the way
        return measure;
    }
}
