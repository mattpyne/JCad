package org.pyne.jcad.core.math.brep.eval;

import org.pyne.jcad.core.math.brep.BrepSurface;
import java.util.Set;
import org.pyne.jcad.core.math.brep.Loop;

/**
 *
 * @author Matthew Pyne
 */
public class FacePrototype {

    public Set<Loop> loops;
    public BrepSurface surface;
}
