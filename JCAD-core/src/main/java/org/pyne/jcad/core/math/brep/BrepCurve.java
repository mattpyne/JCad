package org.pyne.jcad.core.math.brep;

import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.geom.ParametricCurve;
import org.pyne.jcad.core.math.geom.eval.CurveIntersectData;

import java.util.List;
import java.util.stream.Collectors;

import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.geom.ParametricCurve.TYPE;
import verb.core.BoundingBox;
import verb.core.Interval;

/**
 * @author Matthew Pyne
 */
public class BrepCurve extends DataContainer {

    private ParametricCurve impl;

    public BrepCurve(ParametricCurve impl) {
        super();
        this.impl = impl;
    }

    public BrepCurve(ParametricCurve impl, double uMin, double uMax) {
        this(impl);
    }

    public BrepCurve(BrepCurve other) {
        this(other.impl.copy());
    }

    public BrepCurve copy() {
        return new BrepCurve(this);
    }

    public ParametricCurve getImpl() {
        return impl;
    }

    /**
     * @param vector
     * @return
     */
    public BrepCurve translate(Point3DC vector) {
        Matrix3 tr = new Matrix3().translate(vector.getX(), vector.getY(), vector.getZ());
        return transform(tr);
    }

    /**
     * @return
     */
    public BrepCurve invert() {
        return new BrepCurve(impl.invert());
    }

    public Point3DC tangentAtPoint(Point3DC point) {
        return impl.tangentAtPoint(point);
    }

    /**
     * @param u
     * @return
     */
    public Point3DC tangentAtParam(double u) {
        return impl.tangentAtParam(u);
    }

    /**
     * @param point
     * @return
     */
    public double param(Point3DC point) {
        return impl.param(point);
    }

    /**
     * @param u
     * @return
     */
    public Point3DC point(double u) {
        return impl.point(u);
    }

    /**
     * @return
     */
    public Point3DC start() {
        return impl.point(uMin().doubleValue());
    }

    /**
     * @return
     */
    public Point3DC end() {
        return impl.point(uMax().doubleValue());
    }

    /**
     * @param point
     * @return
     */
    public List<BrepCurve> split(Point3DC point) {
        return split(param(point));
    }

    /**
     * @param u
     * @return
     */
    public List<BrepCurve> split(double u) {
        return impl.split(u).stream()
                .map(BrepCurve::new)
                .collect(Collectors.toList());
    }

    public List<Point3DC> tessellate(Number tessTol) {
        return impl.tessellate(tessTol);
    }

    public BrepCurve transform(Matrix3 tr) {
        return new BrepCurve(impl.transform(tr));
    }

    public Interval<Number> domain() {
        return impl.domain();
    }

    public Number uMax() {
        return domain().max;
    }

    public Number uMin() {
        return domain().min;
    }

    public BoundingBox bounds() {
        return impl.bounds();
    }

    public Point3DC middlePoint() {
        return impl.middlePoint();
    }

    public List<CurveIntersectData> intersectCurve(BrepCurve curve) {
        return impl.intersectCurve(curve.impl);
    }

    public boolean passesThrough(Point3DC pos) {
        return impl.passesThrough(pos);
    }

    public List<Point3DC> tessellate() {
        return impl.tessellate();
    }

    public List<Point3DC> tessellate(double uMin, double uMax) {
        return impl.tessellate(uMin, uMax);
    }

    public TYPE getType() {
        return impl.getType();
    }

    public boolean isClosed() {
        return CadMath.eq(point(uMax().doubleValue()), point(uMin().doubleValue()), CadMath.TOLERANCE_SQ);
    }

    @Override
    public String toString() {
        return "BrepCurve{" + "impl=" + impl + '}';
    }

}
