package org.pyne.jcad.core.math;

import de.lighti.clipper.Path;
import de.lighti.clipper.Point;

import java.util.List;
import java.util.stream.Collectors;

public class ClipperInterop {

    public static Path clipperPath(List<Point3DC> path) {
        Path clipperPath = new Path();

        path.forEach(p -> clipperPath.add(new Point.LongPoint((long) p.getX(), (long) p.getY())));

        return clipperPath;
    }

    public static List<Point3DC> createPointPathFrom(Path path) {
        return path.stream()
                .map(pt -> new Point3DC(pt.getX(), pt.getY(), pt.getZ()))
                .collect(Collectors.toList());
    }
}
