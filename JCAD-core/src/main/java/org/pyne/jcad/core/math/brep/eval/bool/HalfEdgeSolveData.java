package org.pyne.jcad.core.math.brep.eval.bool;

import java.util.List;
import org.pyne.jcad.core.math.brep.HalfEdge;
import static org.pyne.jcad.core.math.brep.eval.bool.ShellBooleanOps.MY;

/**
 *
 * @author Matthew Pyne
 */
public class HalfEdgeSolveData {

    private static final HalfEdgeSolveData EMPTY = new HalfEdgeSolveData();

    public int priority = 0;
    public boolean affected = false;
    public DecayedHalfEdges decayed;

    public static HalfEdgeSolveData get(HalfEdge edge) {
        if (null == edge) {
            return HalfEdgeSolveData.EMPTY;
        }
        if (!edge.hasData(MY, HalfEdgeSolveData.class)) {
            return HalfEdgeSolveData.EMPTY;
        }
        return edge.data(MY, HalfEdgeSolveData.class).get();
    }

    public static HalfEdgeSolveData createIfEmpty(HalfEdge edge) {
        if (null == edge) {
            return HalfEdgeSolveData.EMPTY;
        }
        if (!edge.hasData(MY, HalfEdgeSolveData.class)) {
            edge.putData(MY, new HalfEdgeSolveData());
        }
        return edge.data(MY, HalfEdgeSolveData.class).get();
    }

    public static void clear(HalfEdge edge) {
        if (null == edge) {
            return;
        }
        edge.delete(MY);
    }

    public static void setPriority(HalfEdge edge, int value) {
        if (null == edge) {
            return;
        }
        createIfEmpty(edge).priority = value;
    }

    public static void markAffected(HalfEdge edge) {
        if (null == edge) {
            return;
        }
        createIfEmpty(edge).affected = true;
    }

    public static void markCollision(HalfEdge he1, HalfEdge he2) {
        markNeighborhoodAffected(he1);
        markNeighborhoodAffected(he2);
    }

    private static void markNeighborhoodAffected(HalfEdge edge) {
        if (null == edge) {
            return;
        }
        markAffected(edge);
        markAffected(edge.getNext());
        markAffected(edge.getPrev());
    }

    public static void addPriority(HalfEdge he, int value) {
        if (null == he) {
            return;
        }
        createIfEmpty(he).priority += value;
    }

    public static int getPriority(HalfEdge edge) {
        if (null == edge) {
            return -1;
        }
        return get(edge).priority;
    }

    public static void transferPriority(HalfEdge from, HalfEdge to) {
        if (null == from || null == to) {
            return;
        }
        int priority = getPriority(from);
        if (priority != 0) {
            setPriority(to, priority);
        }
    }

    public static void addDecayed(HalfEdge he, List<HalfEdge> out) {
        if (null == he) {
            return;
        }
        DecayedHalfEdges decayed = HalfEdgeSolveData.createIfEmpty(he).decayed;
        if (null != decayed) {
            decayed.forEach(de -> addDecayed(de, out));
        } else {
            out.add(he);
        }
    }
}
