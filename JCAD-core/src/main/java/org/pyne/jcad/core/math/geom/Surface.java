package org.pyne.jcad.core.math.geom;

import haxe.root.Array;
import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import verb.core.BoundingBox;
import verb.core.Interval;
import verb.core.LazyMeshBoundingBoxTree;
import verb.core.MeshData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.pyne.jcad.core.Cache;

/**
 *
 * @author matt pyne
 */
public abstract class Surface extends DataContainer {

    public final static double WORKING_POINT_SCALE_FACTOR = 100000;
    public final static double WORKING_POINT_UN_SCALE_FACTOR = 1. / WORKING_POINT_SCALE_FACTOR;

    protected PlanarSurface simpleSurface;
    protected boolean mirrored;

    /**
     *
     */
    public Surface() {
    }

    /**
     *
     * @param other
     */
    public Surface(Surface other) {
    }

    public Array<Number> middleUV() {
        return new Array<>(new Number[]{
            (domainU().max.doubleValue() - domainU().min.doubleValue()) * 0.5,
            (domainV().max.doubleValue() - domainV().min.doubleValue()) * 0.5
        });
    }

    public Point3DC middlePt() {
        Array<Number> uv = middleUV();
        return point(uv.get(0).doubleValue(), uv.get(1).doubleValue());
    }

    public abstract Point3DC point(double u, double v);

    public Surface transform(Matrix3 tr) {
        return toNurbs().transform(tr);
    }

    public Point3DC closestPoint(Point3DC point) {
        Array<Number> param = param(point);

        return point(param.get(0).doubleValue(), param.get(1).doubleValue());
    }

    /**
     *
     * @param point [u, v]
     * @return
     */
    public abstract Array<Number> param(Point3DC point);

    public Point3DC workingPoint(Point3DC point) {
        return createWorkingPoint(param(point), point);
//        return cache().get(
//                "workingPoint:" + point,
//                () -> createWorkingPoint(param(point), point));
    }

    public Point3DC createWorkingPoint(Array<Number> uv, Point3DC point) {
        Point3DC wp = new Point3DC(
                uv.__a[0].doubleValue(),
                uv.__a[1].doubleValue(),
                0)
                .times(WORKING_POINT_SCALE_FACTOR);

        if (mirrored) {
            wp.setX(wp.getX() * -1);
        }

        wp.__3D = point;

        return wp;
    }

    public Point3DC workingPointTo3D(Point3DC wp) {
        if (wp.__3D == null) {
            Point3DC uv = wp.times(WORKING_POINT_UN_SCALE_FACTOR);
            if (mirrored) {
                uv.setX(uv.getX() * -1);
            }
            wp.__3D = point(uv.getX(), uv.getY());
        }
        return wp.__3D;
    }

    public boolean isMirrored() {
        return mirrored;
    }

    public static boolean isMirrored(Surface surface) {
        double uMin = surface.domainU().min.doubleValue();
        double vMIn = surface.domainV().min.doubleValue();
        Point3DC x = surface.isoCurveAlignU(uMin).tangentAtParam(vMIn);
        Point3DC y = surface.isoCurveAlignV(uMin).tangentAtParam(vMIn);

        return x.crossed(y).dot(surface.normalUV(uMin, vMIn)) < 0.;
    }

    public Surface invert() {
        throw new UnsupportedOperationException();
    }

    public PlanarSurface getSimpleSurface() {
        if (null == simpleSurface) {
            
        }
        return simpleSurface;
    }

    public MeshData tessellate() {
        return toNurbs().tessellate();
    }

    public Interval<Number> domainU() {
        return new Interval<>(-1e8, 1e8);
    }

    public Interval<Number> domainV() {
        return new Interval<>(-1e8, 1e8);
    }

    public abstract PlanarSurface tangentPlane(double u, double v);

    public abstract List<Curve> intersectSurfaceForSameClass(Surface other, double tol);

    public List<Curve> intersectSurface(Surface other) {
        return cache().get(
                Cache.SURF_SURF_INTERSECTION + other.getId().hashCode(),
                () -> {

                    List<Curve> intersections = new ArrayList<>();
                    if (bounds().intersects(other.bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {

                        intersections = intersectSurface(other, Tolerance.NUMERICAL_SOLVE_TOL);
                    }

                    final List<Curve> intersectionsFinal = intersections;

                    other.cache().get(Cache.SURF_SURF_INTERSECTION + getId().hashCode(),
                            () -> intersectionsFinal.stream().map(Curve::copy).collect(Collectors.toList()));

                    return intersections;
                });
    }

    public List<Curve> intersectSurface(Surface other, Number tol) {
        tol = null == tol ? CadMath.TOLERANCE : tol;
        if (this.getClass().isInstance(other)) {
            return intersectSurfaceForSameClass(other, tol.doubleValue());
        }
        return toNurbs().intersectSurfaceForSameClass(other, tol.doubleValue());
    }

    public Point3DC normal(Point3DC point) {
        return cache().get(
                Cache.NORMAL + point.hashCode(),
                () -> {
                    Array<Number> uv = param(point);
                    return normalUV(uv.__a[0].doubleValue(), uv.__a[1].doubleValue());
                });
    }

    public abstract Point3DC normalUV(double u, double v);

    public abstract NurbsSurface toNurbs();

    /**
     *
     * @return
     */
    public abstract Surface copy();

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return false;
    }

    /**
     *
     * @return
     */
    public abstract HashSet<PlanarSurface> getApproxSurface();

    public Curve isoCurveAlignU(double param) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Curve isoCurveAlignV(double param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public BoundingBox bounds() {
        return cache().get(
                Cache.BOUNDING_BOX,
                () -> new LazyMeshBoundingBoxTree(tessellate(), null).boundingBox());
    }

    public abstract boolean contains(Point3DC pt);
    
    public int degreeU(){
        return toNurbs().degreeU();
    }
    
    public int degreeV(){
        return toNurbs().degreeV();
    }
    
    public boolean isPlane() {
        return degreeU() == 1 && degreeV() == 1;
    }

    public List<Surface> split(double u, boolean useV) {
        return toNurbs().split(u, useV);
    }
}
