package org.pyne.jcad.core.math;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import eu.mihosoft.vvecmath.Vector3d;
import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.brep.creators.ShellPrimitives;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.Interval;

/**
 *
 * @author matts
 */
public class CadMath {

    public static final double TOLERANCE = 1e-6;
    public static final double TOLERANCE_SQ = TOLERANCE * TOLERANCE;
    public static final double EPSILON = 1e-12;
    public static final double EPSILON_SQ = EPSILON * EPSILON;
    private static double DEGREE_TO_RADIANS_CONVERSION = Math.PI / 180.0;
    private static double RADIANS_TO_DEGREE_CONVERSION = 1. / DEGREE_TO_RADIANS_CONVERSION;

    public static double[] toEuler(double x, double y, double z, double angle) {
        angle = angle * Math.PI / 180.;
        double heading, attitude, bank;
        double s = Math.sin(angle);
        double c = Math.cos(angle);
        double t = 1 - c;
        //  if axis is not already normalised then uncomment this
        // double magnitude = Math.sqrt(x*x + y*y + z*z);
        // if (magnitude==0) throw error;
        // x /= magnitude;
        // y /= magnitude;
        // z /= magnitude;
        double radToDegree = 180. / Math.PI;
        if ((x * y * t + z * s) > 0.998) { // north pole singularity detected
            heading = 2 * Math.atan2(x * Math.sin(angle / 2), Math.cos(angle / 2));
            attitude = Math.PI / 2;
            bank = 0;

            return new double[]{bank * radToDegree, heading * radToDegree, attitude * radToDegree};
        }
        if ((x * y * t + z * s) < -0.998) { // south pole singularity detected
            heading = -2 * Math.atan2(x * Math.sin(angle / 2), Math.cos(angle / 2));
            attitude = -Math.PI / 2;
            bank = 0;
            return new double[]{bank * radToDegree, heading * radToDegree, attitude * radToDegree};
        }
        heading = Math.atan2(y * s - x * z * t, 1 - (y * y + z * z) * t);
        attitude = Math.asin(x * y * t + z * s);
        bank = Math.atan2(x * s - y * z * t, 1 - (x * x + z * z) * t);

        return new double[]{bank * radToDegree, heading * radToDegree, attitude * radToDegree};
    }

    public static Vector3d intersectPoint(Vector3d rayVector, Vector3d rayPoint, Vector3d planeNormal, Vector3d planePoint) {
        Vector3d diff = rayPoint.minus(planePoint);
        double prod1 = diff.dot(planeNormal);
        double prod2 = rayVector.dot(planeNormal);
        double prod3 = prod1 / prod2;
        return rayPoint.minus(rayVector.times(prod3));
    }

    public static boolean isOnPositiveHalfPlaneFromVec(Point3DC vec, Point3DC testee, Point3DC normal) {
        return vec.crossed(testee).dot(normal) > 0;
    }

    /**
     * Rounds a double to specified decimal places.
     *
     * @param d
     * @param places
     * @return
     */
    public static double round(double d, int places) {
        if (places > 18 || places < -18) {
            throw new IllegalArgumentException("Argument is out of range.");
        } else {
            double n = Math.pow(10, places);
            return (Double) (Math.round(d * n) / n);
        }
    }

    /**
     * Gets the number of decimals the double is evaluated to.
     *
     * @param d
     * @return
     */
    public static int precision(double d) {
        String[] dStringSplit = Double.toString(d).split(".");
        return dStringSplit.length == 2
                ? dStringSplit[1].length()
                : 0;
    }

    /**
     *
     * @param d
     * @return
     */
    public static boolean isZero(double d) {
        return d < EPSILON && d > -EPSILON;
    }

    /**
     *
     * @param input
     * @param min
     * @param max
     * @return
     */
    public static float clamp(float input, float min, float max) {
        return (input < min) ? min : (input > max) ? max : input;
    }

    /**
     *
     * @param input
     * @param min
     * @param max
     * @return
     */
    public static double clamp(double input, double min, double max) {
        return (input < min) ? min : (input > max) ? max : input;
    }

    /**
     *
     * @param a
     * @param b
     * @param tol
     * @return
     */
    public static boolean eq(Number a, Number b, double tol) {
        return Math.abs(a.doubleValue() - b.doubleValue()) < tol;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean eq(Number a, Number b) {
        return eq(a, b, TOLERANCE);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean eqEps(Number a, Number b) {
        return eq(a, b, EPSILON);
    }

    public static double distanceSq(Vector3d a, Vector3d b) {
        return a.minus(b).magnitudeSq();
    }

    public static double distanceSqANeqB(Vector3d a, Vector3d b) {
        return a.minus(b.negated()).magnitudeSq();
    }

    public static boolean eq(Vector3d a, Vector3d b, double toleranceSq) {
        return eq(distanceSq(a, b), 0, toleranceSq);
    }

    public static boolean eqANegB(Vector3d a, Vector3d b, double toleranceSq) {
        return eq(distanceSqANeqB(a, b), 0, toleranceSq);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long startTime = new Date().getTime();

        int numOps = 100;
        Shell shell = ShellPrimitives.box(1000, 250, 250, null);

        for (int i = 0; i < numOps; i++) {

            shell = shell.subtract(ShellPrimitives.cylinder(1, 400, new Matrix3().translate(1 * i - 490, 0, 0)));
        }

        System.out.println((new Date().getTime() - startTime) / 1000. + " seconds to do " + numOps + " cuts");
    }

    public static double toRadians(double degrees) {
        return degrees * DEGREE_TO_RADIANS_CONVERSION;
    }

    public static double toDegrees(double radians) {
        return radians * RADIANS_TO_DEGREE_CONVERSION;
    }

    public static List<Point3DC> convertPointsToUV(Surface surf, List<Point3DC> loopPoints) {

        loopPoints = loopPoints.stream()
                .map(t -> new Point3DC(surf.param(t)))
                .collect(Collectors.toList());

        Interval<Number> dU = surf.domainU();
        Interval<Number> dV = surf.domainV();

        double UMax = dU.max.doubleValue();
        double UMin = dU.min.doubleValue();
        double VMax = dV.max.doubleValue();
        double VMin = dV.min.doubleValue();

        Point3DC p0 = surf.point(dU.min.doubleValue(), dV.min.doubleValue());
        boolean uClosed = p0.equals(surf.point(dU.max.doubleValue(), dV.min.doubleValue()));
        boolean vClosed = p0.equals(surf.point(dU.min.doubleValue(), dV.max.doubleValue()));

        List<Point3DC> pipLoop = new ArrayList();
        for (Point3DC loopPoint : loopPoints) {

            if (loopPoint.getX() > UMax || loopPoint.getX() < UMin) {
                Debug.log(Debug.LEVEL.ERROR, "Bad point out side domain. " +
                        "Last time this is was param finding calculation problem with cylinders.");
            }
            if (loopPoint.getY() > VMax || loopPoint.getY() < VMin) {
                Debug.log(Debug.LEVEL.ERROR, "Bad point out side domain. " +
                        "Last time this is was param finding calculation problem with cylinders.");
            }

            if (pipLoop.isEmpty()) {
                pipLoop.add(loopPoint);
                continue;
            }

            if (uClosed && (CadMath.eq(loopPoint.x(), UMax) || CadMath.eq(loopPoint.x(), UMin))) {
                Point3DC pj = pipLoop.get(pipLoop.size() - 1);

                double dU0 = Math.abs(pj.x() - UMax);
                double dU0p = Math.abs(pj.x() - UMin);
                if (dU0 < dU0p) {
                    loopPoint.setX(UMax);
                } else {
                    loopPoint.setX(UMin);
                }
            }

            if (vClosed && (CadMath.eq(loopPoint.y(), VMax) || CadMath.eq(loopPoint.y(), VMin))) {
                Point3DC pj = pipLoop.get(pipLoop.size() - 1);

                double dV0 = Math.abs(pj.y() - VMax);
                double dV0p = Math.abs(pj.y() - VMin);
                if (dV0 < dV0p) {
                    loopPoint.setY(VMax);
                } else {
                    loopPoint.setY(VMin);
                }
            }

            if (!pipLoop.get(pipLoop.size() - 1).equals(loopPoint)) {
                pipLoop.add(loopPoint);
            }
        }

        if (pipLoop.isEmpty()) {
            return pipLoop;
        }

        Point3DC P0 = pipLoop.get(0);
        if (uClosed && (CadMath.eq(P0.x(), UMax) || CadMath.eq(P0.x(), UMin))) {
            Point3DC pj = pipLoop.get(1);

            double dU0 = Math.abs(pj.x() - UMax);
            double dU0p = Math.abs(pj.x() - UMin);
            if (dU0 < dU0p) {
                P0.setX(UMax);
            } else {
                P0.setX(UMin);
            }
        }

        if (vClosed && (CadMath.eq(P0.y(), VMax) || CadMath.eq(P0.y(), VMin))) {
            Point3DC pj = pipLoop.get(1);

            double dV0 = Math.abs(pj.y() - VMax);
            double dV0p = Math.abs(pj.y() - VMin);
            if (dV0 < dV0p) {
                P0.setY(VMax);
            } else {
                P0.setY(VMin);
            }
        }

        return pipLoop;
    }

}
