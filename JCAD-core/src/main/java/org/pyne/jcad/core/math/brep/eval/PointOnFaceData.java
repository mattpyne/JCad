package org.pyne.jcad.core.math.brep.eval;

import org.pyne.jcad.core.math.brep.HalfEdge;

/**
 *
 * @author Matthew Pyne
 */
public class PointOnFaceData {
    public boolean inside;
    public boolean strictInside;
    public HalfEdge edge;
}
