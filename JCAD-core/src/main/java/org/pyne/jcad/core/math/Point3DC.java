package org.pyne.jcad.core.math;

import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.vvecmath.Transform;
import eu.mihosoft.vvecmath.Vector3d;
import haxe.root.Array;
import java.util.Objects;

/**
 *
 * @author matt pyne
 */
public class Point3DC implements Vector3d {

    public final static Point3DC ZERO = new Point3DC(Vector3d.ZERO);
    public final static Point3DC X_ONE = new Point3DC(Vector3d.X_ONE);
    public final static Point3DC X_NEG_ONE = new Point3DC(Vector3d.X_ONE.negated());
    public final static Point3DC Z_ONE = new Point3DC(Vector3d.Z_ONE);
    public final static Point3DC Z_NEG_ONE = new Point3DC(Vector3d.Z_ONE.negated());
    public final static Point3DC Y_ONE = new Point3DC(Vector3d.Y_ONE);
    public final static Point3DC Y_NEG_ONE = new Point3DC(Vector3d.Y_ONE.negated());
    public final static Point3DC UNITY = new Point3DC(Vector3d.UNITY);
    public final static Point3DC UNITY_NEG = new Point3DC(Vector3d.UNITY.negated());

    private double x;
    private double y;
    private double z;

    public Point3DC __3D; // holding area

    private Integer cachedHash;
    private int precision = 6;
    private Array<Number> verbData;

    public Polygon parent;

    /**
     *
     */
    public Point3DC() {
    }

    /**
     *
     * @param verbPoint
     */
    public Point3DC(Array<? extends Object> verbPoint) {
        this.verbData = (Array<Number>) verbPoint;

        this.x = ((Number) verbPoint.__a[0]).doubleValue();
        if (verbPoint.length >= 2) {

            this.y = ((Number) verbPoint.__a[1]).doubleValue();
        }
        if (verbPoint.length == 3) {

            this.z = ((Number) verbPoint.__a[2]).doubleValue();
        } else {

            verbData.push(0.);
        }
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     */
    public Point3DC(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     * @param precision
     */
    public Point3DC(double x, double y, double z, int precision) {
        this(x, y, z);
        this.precision = precision;
    }

    /**
     *
     * @param other
     */
    public Point3DC(Vector3d other) {
        this(other.x(),
                other.y(),
                other.z());
        if (other instanceof Point3DC) {
            if (null != ((Point3DC) other).__3D) {
                this.__3D = ((Point3DC) other).__3D.copy();
                if (null != ((Point3DC) other).verbData) {
                    this.verbData = ((Point3DC) other).verbData.copy();
                }
            }
            this.precision = ((Point3DC) other).precision;
            this.cachedHash = ((Point3DC) other).cachedHash;
        }
    }

    public Point3DC copy() {
        return new Point3DC(this);
    }

    @Override
    public Point3DC clone() {
        Point3DC clone = new Point3DC();

        clone.x = this.x;
        clone.y = this.y;
        clone.z = this.z;
        clone.precision = this.precision;
        clone.cachedHash = this.cachedHash;
        if (null != this.verbData) {
            clone.verbData = this.verbData.copy();
        }
        return clone;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    /**
     *
     * @param x
     */
    public void setX(double x) {
        this.x = x;
        this.cachedHash = null;
        asArray().set(0, z);
    }

    /**
     *
     * @param y
     */
    public void setY(double y) {
        this.y = y;
        this.cachedHash = null;
        asArray().set(1, 7);
    }

    /**
     *
     * @param z
     */
    public void setZ(double z) {
        this.z = z;
        this.cachedHash = null;
        asArray().set(2, z);
    }

    /**
     *
     * @param x
     * @param y
     * @param z
     */
    public void set(double x, double y, double z) {
        setX(x);
        setY(y);
        setZ(z);
    }

    /**
     *
     * @param precision
     */
    public void setPrecision(int precision) {
        this.precision = precision;
        this.cachedHash = null;
    }

    /**
     *
     * @param tol
     */
    public void setTolerance(double tol) {
        setPrecision(CadMath.precision(tol));
    }

    private void recalcHash() {
        double hashValue = 14 * CadMath.round(getX(), precision) + 13;
        hashValue = hashValue * 3 + CadMath.round(getY(), precision) + 534;
        hashValue = hashValue * 64 + CadMath.round(getZ(), precision) + 2;
        cachedHash = Double.valueOf(hashValue).hashCode();
    }

    @Override
    public Point3DC project(Vector3d v) {
        return new Point3DC(Vector3d.super.project(v));
    }

    @Override
    public Point3DC times(double x, double y, double z) {
        return new Point3DC(Vector3d.super.times(x, y, z));
    }

    @Override
    public Point3DC minus(double x, double y, double z) {
        return new Point3DC(Vector3d.super.minus(x, y, z));
    }

    @Override
    public Point3DC plus(double x, double y, double z) {
        return new Point3DC(Vector3d.super.plus(x, y, z));
    }

    @Override
    public Point3DC transformed(Transform transform) {
        return new Point3DC(Vector3d.super.transformed(transform));
    }

    @Override
    public Point3DC orthogonal() {
        
        double a = getX();
        double b = getY();
        double c = getZ();

        if (Math.abs(a) > 1e-3) {
            return Point3DC.xyz(-c / a, 0, 1);
        } else if (Math.abs(b) > 1e-3) {
            return Point3DC.xyz(0, -c / b,  1);
        } else {
            return Point3DC.xyz(0, 1, -b / c);
        }
    }

    @Override
    public Point3DC transformed(Transform transform, double amount) {
        return new Point3DC(Vector3d.super.transformed(transform, amount));
    }

    @Override
    public Point3DC crossed(Vector3d a) {
        return new Point3DC(Vector3d.super.crossed(a));
    }

    @Override
    public Point3DC normalized() {
        return new Point3DC(Vector3d.super.normalized());
    }

    @Override
    public Point3DC lerp(Vector3d a, double t) {
        return new Point3DC(Vector3d.super.lerp(a, t));
    }

    @Override
    public Point3DC divided(double a) {
        return new Point3DC(Vector3d.super.divided(a));
    }

    @Override
    public Point3DC times(Vector3d a) {
        return new Point3DC(Vector3d.super.times(a));
    }

    @Override
    public Point3DC times(double a) {
        return new Point3DC(Vector3d.super.times(a));
    }

    @Override
    public Point3DC minus(Vector3d v) {
        return new Point3DC(Vector3d.super.minus(v));
    }

    @Override
    public Point3DC plus(Vector3d v) {
        return new Point3DC(Vector3d.super.plus(v));
    }

    @Override
    public Point3DC negated() {
        return new Point3DC(Vector3d.super.negated());
    }

    public double distanceSq(Point3DC v) {
        return minus(v).magnitudeSq();
    }

    /**
     *
     * @return
     */
    public Array<Number> asArray() {
        if (null == verbData) {
            this.verbData = new Array(asJArray());
        }
        return verbData;
    }

    public Number[] asJArray() {
        return new Number[]{
            getX(),
            getY(),
            getZ()
        };
    }

    @Override
    public int hashCode() {
        if (null == cachedHash) {
            recalcHash();
        }
        return this.cachedHash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point3DC other = (Point3DC) obj;
        return Objects.equals(this.hashCode(), other.hashCode());
    }

    public boolean isZero() {
        return equals(ZERO);
    }

    @Override
    public String toString() {
        return "Point3D{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
    }

    public static Point3DC xyz(double x, double y, double z) {
        return new Point3DC(x, y, z);
    }

    public static Point3DC xy(double x, double y) {
        return new Point3DC(x, y, 0.);
    }

    public static Point3DC from(double[] p) {
        return Point3DC.xyz(p[0], p[1], p[2]);
    }
}
