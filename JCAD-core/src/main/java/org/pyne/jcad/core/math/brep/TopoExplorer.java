package org.pyne.jcad.core.math.brep;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author Matthew Pyne
 */
public class TopoExplorer {

    private final Topo topo;

    /**
     * Used to find topos in topos
     *
     * @param topo parent topo to search in
     */
    public TopoExplorer(Topo topo) {
        this.topo = topo;
    }

    /**
     * Stream all faces in the topo.
     *
     * @return
     */
    public Stream<Face> faces() {

        if (topo instanceof Shell) {

            return ((Shell) topo).streamFaces();
        } else if (topo instanceof Face) {

            return Stream.of((Face) topo);
        }

        return Stream.empty();
    }

    /**
     * Stream all edges in the topo.
     *
     * @return
     */
    public Stream<Edge> edges() {

        return halfEdges().map(HalfEdge::edge);
    }
    
    /**
     * Stream all halfedges in the topo.
     *
     * @return
     */
    public Stream<HalfEdge> halfEdges() {

        return faces().flatMap(Face::streamHalfEdges);
    }

    /**
     * Finds the first topo matching the given id.
     *
     * @param id
     * @return
     */
    public Optional<Topo> findById(String id) {
        if (topo.getId().equals(id)) {

            return Optional.of(topo);
        }

        if (topo instanceof Shell) {

            for (Face face : ((Shell) topo).faces()) {

                Optional<Topo> result = findById(face, id);
                if (result.isPresent()) {

                    return result;
                }
            }

        } else if (topo instanceof Face) {

            return findById((Face) topo, id);
        } else if (topo instanceof Edge) {

            return findById((Edge) topo, id);
        }

        return Optional.empty();
    }

    private Optional<Topo> findById(Face face, String id) {
        if (face.getId().equals(id)) {

            return Optional.of(face);
        }

        for (HalfEdge he : face.halfEdges()) {

            Optional<Topo> result = findById(he.edge(), id);
            if (result.isPresent()) {

                return result;
            }
        }

        return Optional.empty();
    }

    private Optional<Topo> findById(Edge edge, String id) {
        if (edge.getId().equals(id)) {

            return Optional.of(edge);
        }

        Vertex vertexA = edge.halfEdge1().vertexA();
        if (vertexA.getId().equals(id)) {

            return Optional.of(vertexA);
        }

        Vertex vertexB = edge.halfEdge1().vertexA();
        if (vertexB.getId().equals(id)) {

            return Optional.of(vertexB);
        }

        return Optional.empty();
    }

}
