package org.pyne.jcad.core.math.brep;

import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.vvecmath.Vector3d;
import haxe.lang.EmptyObject;
import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import verb.core.BoundingBox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.math.geom.ParametricCurve;


/**
 *
 * @author matt pyne
 */
public class HalfEdge extends DataContainer {

    private Edge edge;
    private Loop loop;
    private HalfEdge next;
    private HalfEdge prev;
    private Vertex vertexA;
    private Vertex vertexB;
    private Point3DC unitDirection;
    private Double distance;
    private boolean inverted;

    /**
     *
     */
    public HalfEdge() {
        this.inverted = false;
    }

    /**
     *
     * @param edge
     * @param inverted
     * @param vertex1
     * @param vertex2
     */
    public HalfEdge(Edge edge, boolean inverted, Vertex vertex1, Vertex vertex2) {
        this.vertexA = vertex1;
        this.vertexB = vertex2;
        this.edge = edge;
        this.inverted = inverted;
        this.vertexA.addEdge(this);
        this.vertexB.addEdge(this);
    }

    /**
     *
     * @param other
     */
    private HalfEdge(HalfEdge other) {
        this();
        this.vertexA = other.vertexA.copy();
        this.vertexB = other.vertexB.copy();
        this.inverted = other.inverted;
        this.vertexA.addEdge(this);
        this.vertexB.addEdge(this);
    }

    /**
     *
     * @return
     */
    public HalfEdge copy() {
        return new HalfEdge(this);
    }

    public Point3DC closestPointTo(Point3DC pt) {
        return edge.curve().point(edge.curve().param(pt));
    }

    public List<Point3DC> tessellate() {
        BrepCurve curve = edge().curve();
        List<Point3DC> res = curve.tessellate(
                curve.param(vertexA().point()),
                curve.param(vertexB().point()));


        if (inverted) {
            Collections.reverse(res);
        }

        if (!res.isEmpty()) {
            res.remove(0);
        }
        res.add(0, vertexA.point().copy());

        if (res.size() > 1) {
            res.remove(res.size() - 1);
        }
        res.add(vertexB.point().copy());

        return res;
    }

    public Point3DC tangentAtEnd() {
        return tangent(vertexB.point());
    }

    public Point3DC tangentAtStart() {
        return tangent(vertexA.point());
    }

    public Point3DC tangent(Point3DC point) {
        Point3DC tangent = edge.curve().tangentAtPoint(point).normalized();
        if (inverted) {
            tangent = tangent.negated();
        }
        return tangent;
    }

    public void transform(Matrix3 tr) {
        cache().clear();
        Vertex vert11 = vertexA();
        vert11.setPos(tr.apply(vert11.point()));
        Vertex vert21 = vertexB();
        vert21.setPos(tr.apply(vert21.point()));
    }

    public boolean isInverted() {
        return inverted;
    }

    public void setInverted(boolean inverted) {
        this.inverted = inverted;
    }

    /**
     *
     * @return
     */
    public Edge edge() {
        return edge;
    }

    /**
     *
     * @param edge
     */
    public void setEdge(Edge edge) {
        this.edge = edge;
    }

    public Loop loop() {
        return loop;
    }

    public void setLoop(Loop loop) {
        this.loop = loop;
    }

    public HalfEdge getNext() {
        return next;
    }

    public void setNext(HalfEdge next) {
        this.next = next;
    }

    public HalfEdge getPrev() {
        return prev;
    }

    public void setPrev(HalfEdge prev) {
        this.prev = prev;
    }

    /**
     *
     * @return
     */
    public HalfEdge twin() {
        return this.edge.halfEdge1().equals(this)
                ? this.edge.halfEdge2()
                : this.edge.halfEdge1();

    }

    /**
     *
     * @return
     */
    public boolean isValid() {
        return !(getPos1().equals(getPos2()) && edge().curve().getType() == ParametricCurve.TYPE.LINE);
    }


    /**
     *
     * @param edge
     * @return
     */
    public boolean oppositeDirection(HalfEdge edge) {
        return getUnitDirection().minus(edge.getUnitDirection()).equals(Vector3d.ZERO);
    }

    /**
     *
     * @param edge
     * @return
     */
    public boolean sameDirection(HalfEdge edge) {
        return getUnitDirection().equals(edge.getUnitDirection());
    }

    /**
     *
     * @param edge
     * @return
     */
    public boolean longer(HalfEdge edge) {
        return getDistance() > edge.getDistance();
    }

    /**
     *
     * @param polygon
     * @return
     */
    public boolean intersectsPolygon(Polygon polygon) {

        return polygon.contains(vertexA.point())
                || polygon.contains(vertexB.point());
    }

    /**
     *
     * @return
     */
    public Vector3d getPos1() {
        return vertexA.point();
    }

    /**
     *
     * @param vert
     */
    public void setVertexA(Vertex vert) {
        vertexA = vert;
        twin().vertexB = vert;
        reset();
    }

    /**
     *
     * @return
     */
    public Vector3d getPos2() {
        return vertexB.point();
    }

    /**
     *
     * @param vert
     */
    public void setVertexB(Vertex vert) {
        vertexB = vert;
        twin().vertexA = vert;
        reset();
    }

    /**
     *
     * @return
     */
    public List<double[]> getPositionsList() {
        List<double[]> points = new ArrayList<>();
        points.add(vertexA.point().get());
        points.add(vertexB.point().get());
        return points;
    }

    /**
     *
     * @return
     */
    public double[] getVert1() {
        return getPos1().get();

    }

    /**
     *
     * @return
     */
    public List<double[]> getVertices() {
        List<double[]> points = new ArrayList<>();
        points.add(getVert1());
        points.add(vertexB.point().get());
        return points;
    }

    /**
     *
     * @return
     */
    public Stream<Vector3d> streamPositions() {
        return Stream.of(vertexA.point(), vertexB.point());
    }

    /**
     *
     * @return
     */
    public Stream<double[]> streamPositionsArray() {
        return Stream.of(vertexA.point().get(), vertexB.point().get());
    }

    /**
     *
     * @return
     */
    public Stream<Vertex> streamVertices() {
        return Stream.of(vertexA, vertexB);
    }

    /**
     *
     * @return
     */
    public Vertex vertexA() {
        return vertexA;
    }

    /**
     *
     * @return
     */
    public Vertex vertexB() {
        return vertexB;
    }

    /**
     *
     * @return
     */
    public Vector3d getUnitDirection() {
        if (null == unitDirection) {
            recalcUnitDirection();
        }
        return unitDirection;
    }

    /**
     *
     * @return
     */
    public double getDistance() {
        if (null == distance) {
            recalcDistance();
        }
        return distance;
    }

    /**
     *
     */
    private void recalcUnitDirection() {
        this.unitDirection = new Point3DC(getPos1().minus(getPos2())
                .normalized());
    }

    /**
     *
     */
    private void recalcDistance() {
        this.distance = getPos1().minus(getPos2())
                .magnitude();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.vertexA);
        hash = 21 * hash + Objects.hashCode(this.vertexB);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HalfEdge other = (HalfEdge) obj;
        if (!Objects.equals(this.vertexA, other.vertexA)) {
            return false;
        }
        if (!Objects.equals(this.vertexB, other.vertexB)) {
            return false;
        }
        return true;
    }


    /**
     *
     */
    private void reset() {
        unitDirection = null;
        distance = null;
    }

    public static boolean isSameEdge(HalfEdge e1, HalfEdge e2) {
        
        if (!e1.getPos1().equals(e2.getPos1())
                && !e1.getPos1().equals(e2.getPos2())) {
            
            return false;
        }
        
        if (!e1.getPos2().equals(e2.getPos1())
                && !e1.getPos2().equals(e2.getPos2())) {
            
            return false;
        }
        
        List<Point3DC> tess = e1.tessellate();
        for (Point3DC pt1 : tess) {
            Point3DC pt2 = e2.edge().curve().point(e2.edge().curve().param(pt1));
            if (!pt1.equals(pt2)) {
                return false;
            }
        }
        return true;
    }

    public BoundingBox bounds() {
        return cache().get(Cache.BOUNDING_BOX,
                () -> {
                    return edge().curve().bounds();
                });
    }

    @Override
    public String toString() {
        return "HalfEdge{" + "vertexA=" + vertexA + ", vertexB=" + vertexB + '}';
    }

}
