package org.pyne.jcad.core.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Matthew Pyne
 */
public class EntityExplorer {

    /**
     *
     * @param out Output collection containing all entities in root including
     * root.
     * @param root Starting entity
     */
    public void entities(Collection<Entity> out, Entity root) {

        out.add(root);

        root.getComponent(Connections.class)
                .map(Connections::stream)
                .orElse(Stream.empty())
                .map(Connection::getChild)
                .forEach(childEntity -> entities(out, childEntity));
    }

    /**
     * 
     * @param root
     * @return direct child entities of root
     */
    public List<Entity> directChildren(Entity root) {

        return root.getComponent(Connections.class)
                .map(Connections::stream)
                .orElse(Stream.empty())
                .map(Connection::getChild)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param root Starting entity
     * @return
     */
    public List<Entity> entities(Entity root) {

        List<Entity> entities = new ArrayList<>();

        entities(entities, root);

        return entities;
    }
}
