package org.pyne.jcad.core.math.brep;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.pyne.jcad.core.DataContainer;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Matrix3;

/**
 *
 * @author Matthew Pyne
 */
public class CompoundShell extends DataContainer implements Topo {

    private List<Shell> shells;

    public CompoundShell() {

    }

    public void addAll(Collection<Shell> shells) {
        shells.forEach(this::add);
    }
    
    public void add(Shell shell) {
        if (null == shells) {
            shells = new ArrayList<>();
        }

        if (shells.contains(shell)) {
            return;
        }

        shells.add(shell);

    }

    @Override
    public CompoundShell copy() {
        CompoundShell copy = new CompoundShell();

        shells.forEach(shell -> copy.add(shell.copy()));

        return copy;
    }

    @Override
    public void transform(Matrix3 tr) {
        shells.forEach(shell -> shell.transform(tr));
    }

    @Override
    public Topo transformed(Matrix3 tr) {
        CompoundShell copy = copy();

        copy.transform(tr);

        return copy;
    }

    public List<Shell> shells() {
        return shells;
    }

    @Override
    public boolean isEmpty() {
        return shells.isEmpty()
                || shells.stream().allMatch(Shell::isEmpty);
    }

}
