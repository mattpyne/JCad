package org.pyne.jcad.core.math.brep.eval.bool;

import org.pyne.jcad.core.math.brep.Edge;

/**
 *
 * @author Matthew
 */
class SplitEdge {

    Edge e0;
    Edge e1;

    public SplitEdge(Edge e0, Edge e1) {
        this.e0 = e0;
        this.e1 = e1;
    }
}
