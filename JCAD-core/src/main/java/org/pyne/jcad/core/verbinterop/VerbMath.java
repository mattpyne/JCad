package org.pyne.jcad.core.verbinterop;

import haxe.root.Array;
import java.util.stream.Collectors;
import verb.core.Mat;
import verb.core.Vec;

/**
 *
 * @author Matthew Pyne
 */
public class VerbMath {

    /**
     *
     * @param a
     * @return
     */
    public static boolean isNan(Array<Number> a) {
        return a.stream()
                .filter(x -> null != x)
                .anyMatch(x -> Double.isNaN(x.doubleValue()));
    }

    /**
     *
     * @param a
     * @return
     */
    public static boolean allIsfinite(Array<Number> a) {
        return a.stream()
                .filter(x -> null != x)
                .allMatch(x -> Double.isFinite(x.doubleValue()));
    }

    /**
     *
     * @param a
     * @return
     */
    public static Array<Number> neg(Array<Number> a) {
        a = a.copy();

        for (Number n : a.asList()) {
            if (null != n) {
                n = -n.doubleValue();
            }
        }

        return a;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Number> dot(Array<Array<Number>> a, Array<Number> b) {
        return Mat.dot(a, b);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static double dotVec(Array<Number> a, Array<Number> b) {
        return Vec.dot(a, b);
    }

    /**
     * You can compute the 2-norm of an Array, which is the square root of the
     * sum of the squares of the entries.
     *
     * @param a
     * @return
     */
    public static double norm2(Array<Number> a) {
        double sum2 = a.stream()
                .filter(x -> null != x)
                .collect(Collectors.summingDouble(n -> n.doubleValue() * n.doubleValue()));

        return Math.sqrt(sum2);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> tensor(Array<Array<Number>> a, Array<Array<Number>> b) {
        return Mat.mult(a, b);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> tensorVec(Array<Number> a, Array<Number> b) {
        return Mat.mult(new Array(a.__a), new Array(b.__a));
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> mul(Array<Array<Number>> a, double b) {
        return Mat.mul(b, a);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Number> mul(double b, Array<Number> a) {
        a = a.copy();

        for (Number n : a.asList()) {
            if (null != n) {
                n = n.doubleValue() * b;
            }
        }

        return a;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Number> addVecs(Array<Number> a, Array<Number> b) {
        a = a.copy();

        for (int i = 0; i < a.length; i++) {
            if (null != a.get(i) && null != b.get(i)) {
                a.set(i, a.get(i).doubleValue() + b.get(i).doubleValue());
            }
        }

        return a;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> add(Array<Array<Number>> a, Array<Array<Number>> b) {
        return Mat.add(a, b);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Number> subVecs(Array<Number> a, Array<Number> b) {
        a = a.copy();

        for (int i = 0; i < a.length; i++) {
            if (null != a.get(i) && null != b.get(i)) {
                a.set(i, a.get(i).doubleValue() - b.get(i).doubleValue());
            }
        }

        return a;
    }

    /**
     * Checks a entries against b entries. If all a is greater it is greater.
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean greaterThan(Array<Number> a, Array<Number> b) {
        for (int i = 0; i < a.length; i++) {
            if (null != a.get(i) && null != b.get(i)) {
                if (a.get(i).doubleValue() <= b.get(i).doubleValue()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Checks a entries against b entries. If all a is greater it is greater.
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean greaterThanOrEqual(Array<Number> a, Array<Number> b) {
        for (int i = 0; i < a.length; i++) {
            if (null != a.get(i) && null != b.get(i)) {
                if (a.get(i).doubleValue() < b.get(i).doubleValue()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     *
     * @param a
     * @return
     */
    public static Array<Number> abs(Array<Number> a) {
        a = a.copy();

        for (int i = 0; i < a.length; i++) {
            if (null != a.get(i)) {
                a.set(i, Math.abs(a.get(i).doubleValue()));
            }
        }

        return a;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean lessThanOrEqual(Array<Number> a, Array<Number> b) {
        for (int i = 0; i < a.length; i++) {
            if (null != a.get(i) && null != b.get(i)) {
                if (a.get(i).doubleValue() > b.get(i).doubleValue()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> sub(Array<Array<Number>> a, Array<Array<Number>> b) {
        return Mat.sub(a, b);
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> div(Array<Array<Number>> a, Array<Array<Number>> b) {
        a = a.copy();

        for (int i = 0; i < a.length; i++) {
            Array<Number> ra = a.get(i);
            Array<Number> rb = a.get(i);
            for (int j = 0; i < ra.length; j++) {
                if (null != ra.get(i) && null != rb.get(i)) {
                    ra.set(j, ra.get(j).doubleValue() / rb.get(j).doubleValue());
                }
            }
        }

        return a;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public static Array<Array<Number>> div(Array<Array<Number>> a, double b) {
        return Mat.div(a, b);
    }
}
