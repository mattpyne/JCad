package org.pyne.jcad.core;

public class Color {
    public static final Color CYAN = new Color(0., 1., 1., 0.);

    public final double r;
    public final double g;
    public final double b;
    public final double a;

    public Color(double r, double g, double b, double a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
}
