package org.pyne.jcad.core.math.brep.eval;

import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Vertex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Matthew Pyne
 */
public class EdgeGraph {

    public HashMap<Vertex, List<HalfEdge>> vertexToEdge = new HashMap<>();
    public List<HalfEdge> graphEdges = new ArrayList<>();

    public void add(HalfEdge he) {
        List<HalfEdge> edges = vertexToEdge.get(he.vertexA());
        if (null == edges) {
            edges = new ArrayList<>();
            vertexToEdge.put(he.vertexA(), edges);
        }
        edges.add(he);
        graphEdges.add(he);
    }

}
