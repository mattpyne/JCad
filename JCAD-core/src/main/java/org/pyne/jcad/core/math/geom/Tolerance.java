package org.pyne.jcad.core.math.geom;

import eu.mihosoft.vvecmath.Vector3d;
import org.pyne.jcad.core.math.CadMath;

/**
 *
 * @author Matthew Pyne
 */
public class Tolerance {

    public static final double TESS_TOLERANCE = 2.5E-3;
    public static final double TOLERANCE_BOUNDS_FAIL = 1E-3;
    public static final double TOLERANCE = CadMath.TOLERANCE;//1E-3;
    public static final double TOLERANCE_SQ = TOLERANCE * TOLERANCE;

    public static final double EPSILON = CadMath.EPSILON;//1E-12;
    public static final double EPSILON_SQ = EPSILON * EPSILON;

    // tolerance used for paramtric domain which is between 0..1
    public static final double TOLERANCE_01 = TOLERANCE * 1E-2;
    public static final double TOLERANCE_RAY_CAST = 1E-2;
    public static final double TOLERANCE_01_SQ = TOLERANCE * TOLERANCE;

    public static final double NUMERICAL_SOLVE_TOL = 1E-8;

    public static final boolean eqTol(Number a, Number b) {
        return CadMath.eq(a, b, TOLERANCE);
    }

    public static final boolean eqSqTol(Number a, Number b) {
        return CadMath.eq(a, b, TOLERANCE_SQ);
    }

    public static final boolean eqEps(Number a, Number b) {
        return CadMath.eq(a, b, EPSILON);
    }

    public static final boolean veq(Vector3d a, Vector3d b) {
        return CadMath.eq(a, b, TOLERANCE_SQ);
    }

    public static final boolean veqNeg(Vector3d a, Vector3d b) {
        return CadMath.eqANegB(a, b, TOLERANCE_SQ);
    }

    public static final boolean ueq(Vector3d a, Vector3d b) {
        return CadMath.eq(a, b, TOLERANCE_SQ);
    }

    public static final boolean ueq(Number a, Number b) {
        return CadMath.eq(a, b, TOLERANCE);
    }
}
