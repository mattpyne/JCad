package org.pyne.jcad.core.verbinterop;

import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.Interval;
import verb.core.LazyMeshBoundingBoxTree;
import verb.core.MeshData;
import verb.core.MeshIntersectionPoint;
import verb.core.NurbsCurveData;
import verb.core.NurbsSurfaceData;
import verb.core.Pair;
import verb.core.Vec;
import verb.eval.Analyze;
import verb.eval.Eval;
import verb.eval.Intersect;

/**
 *
 * @author Matthew Pyne
 */
public class VerbNurbsHelpers {

    /**
     *
     * @param nurbsData
     * @return
     */
    public static Interval<Number> curveDomain(NurbsCurveData nurbsData) {
        if (nurbsData == null || nurbsData.knots == null) {
            return new Interval<>(0., 1.);
        }
        return new Interval<>(
                nurbsData.knots.get(0),
                nurbsData.knots.get(nurbsData.knots.length - 1));
    }

    /**
     *
     * @param nurbsData
     * @return
     */
    public static Array<Number> distinctKnots(NurbsCurveData nurbsData) {
        Array<Number> out = new Array(new Number[nurbsData.knots.length]);
        out.set(0, nurbsData.knots.get(0));
        int j = 0;
        for (int i = 1; i < nurbsData.knots.length; i++) {
            Number ithKnot = nurbsData.knots.get(i);
            if (!CadMath.eq(out.get(j), ithKnot)) {
                j++;
                out.set(j, ithKnot);
            }
        }
        return out.slice(0, j + 1);
    }

    /**
     *
     * @param curve
     * @param u
     * @return
     */
    public static Array<Number> curvePoint(NurbsCurveData curve, double u) {
        return Eval.rationalCurvePoint(curve, u);
    }

    /**
     *
     * @param curve
     * @param point
     * @return
     */
    public static double curveClosestParam(NurbsCurveData curve, Array<Number> point) {
        return Analyze.rationalCurveClosestParam(curve, point);
    }

    public static Array<NurbsCurveData> surfaceIntersect(
            NurbsSurfaceData surface0,
            NurbsSurfaceData surface1, 
            Number tol) {

        return Intersect.surfaces(surface0, surface1, tol.doubleValue());
//        MeshData tess0 = Tess.rationalSurfaceAdaptive(surface0, null);
//        MeshData tess1 = Tess.rationalSurfaceAdaptive(surface1, null);
//        
//        Array<Array<MeshIntersectionPoint>> resApprox = meshesIntersect(tess0, tess1, CadMath.TOLERANCE, CadMath.TOLERANCE_SQ, CadMath.TOLERANCE_01);
//        Array<Array<SurfaceSurfaceIntersectionPoint>> exactPls = resApprox.map(new Intersect_surfaces_59__Fun(tol.doubleValue(), surface1, surface0));
//        
//        int degreeU = Math.max(surfaceMaxDegree(surface0), surfaceMaxDegree(surface1));
//        int inserts = degreeU - 1;
//        Array<NurbsCurveData> nurbses = new Array();
//        
//        for (int i = 0; i < exactPls.length; i++) {
//            Array<SurfaceSurfaceIntersectionPoint> p1 = exactPls.__get(i);
//            Array<Array<Number>> points = p1.map(new Function(1, 0) {
//                @Override
//                public Object __hx_invoke1_o(double __fn_float1, Object __fn_dyn1) {
//                    return ((SurfaceSurfaceIntersectionPoint)__fn_dyn1).point;
//                }
//            });
//            NurbsCurveData polyline = Make.polyline(points);
//            Interval<Number> domain = curveDomain(polyline);
//            Number uMin = domain.min;
//            Number uMax = domain.max;
//            double insertStep = (uMax.doubleValue() - uMin.doubleValue()) / (inserts + 1.);
//            
//            Array<Array<Number>> normalizedPoints = new Array(new Array[]{});
//            normalizedPoints.push(points.__get(0));
//            for (int j = 0; j < inserts; j++) {
//                Array<Number> roughPt = curvePoint(polyline, i + insertStep);
//                
//            }
//            
//        }
//
//        Make.rationalInterpCurve(points, degreeU, null, null, null);
    }

    public static int surfaceMaxDegree(NurbsSurfaceData surface) {
        return Math.max(surface.degreeU, surface.degreeV);
    }
    
    /**
     *
     * @param mesh0
     * @param mesh1
     * @param TOLERANCE
     * @param TOLERANCE_SQ
     * @param TOLERANCE_01
     * @return
     */
    public static Array<Array<MeshIntersectionPoint>> meshesIntersect(MeshData mesh0, MeshData mesh1, double TOLERANCE, double TOLERANCE_SQ, double TOLERANCE_01) {
        LazyMeshBoundingBoxTree bbtree0 = new LazyMeshBoundingBoxTree(mesh0, null);
        LazyMeshBoundingBoxTree bbtree1 = new LazyMeshBoundingBoxTree(mesh1, null);
        Array<Pair<Number, Number>> bbints = Intersect.boundingBoxTrees(bbtree0, bbtree1, CadMath.TOLERANCE);
        List<Interval<MeshIntersectionPoint>> points = bbints.stream()
                .map(ids
                        -> {
                    if (null == ids) {
                        return null;
                    }
                    return Intersect.triangles(
                            mesh0,
                            ids.item0.intValue(),
                            mesh1,
                            ids.item1.intValue());
                })
                .filter(x -> x != null)
                .filter(x1 -> Vec.distSquared(x1.min.point, x1.max.point) > CadMath.TOLERANCE_SQ)
                .collect(Collectors.toList());

        List<Interval<MeshIntersectionPoint>> tempPoints = new ArrayList<>(points);

        points.forEach(a -> tempPoints.removeIf(b -> {
            if (a == b) {
                return false;
            }
            Array<Number> s1 = Vec.sub(a.min.uv0, b.min.uv0);
            double d1 = Vec.dot(s1, s1);
            Array<Number> s2 = Vec.sub(a.max.uv0, b.max.uv0);
            double d2 = Vec.dot(s2, s2);
            Array<Number> s3 = Vec.sub(a.min.uv0, b.max.uv0);
            double d3 = Vec.dot(s3, s3);
            Array<Number> s4 = Vec.sub(a.max.uv0, b.min.uv0);
            double d4 = Vec.dot(s4, s4);
            return d1 < Tolerance.TOLERANCE_01 && d2 < Tolerance.TOLERANCE_01 || d3 < Tolerance.TOLERANCE_01 && d4 < Tolerance.TOLERANCE_01;
        }));
        Interval<MeshIntersectionPoint>[] mips2 = tempPoints.toArray(new Interval[tempPoints.size()]);
        Array<Interval<MeshIntersectionPoint>> mips = new Array(mips2);
        return Intersect.makeMeshIntersectionPolylines(mips);
    }

    /**
     *
     * @param curve
     */
    public static void normalizeCurveParametrization(NurbsCurveData curve) {
        Interval<Number> domain = curveDomain(curve);
        double min = domain.min.doubleValue();
        double max = domain.max.doubleValue();
        double d = max - min;

        for (int i = 0; i < curve.knots.length; i++) {
            double val = curve.knots.get(i).doubleValue();
            if (CadMath.eqEps(val, min)) {
                curve.knots.set(i, 0);
            } else if (CadMath.eqEps(val, max)) {
                curve.knots.set(i, 1);
            } else {
                curve.knots.set(i, (val - min) / d);
            }
        }
    }

    /**
     *
     * @param curve
     * @return
     */
    public static NurbsCurveData normalizeCurveParametrizationIfNeeded(NurbsCurveData curve) {
        Interval<Number> domain = curveDomain(curve);
        double min = domain.min.doubleValue();
        double max = domain.max.doubleValue();
        if (min != 0 || max != 1) {
            normalizeCurveParametrization(curve);
        }
        return curve;
    }

}
