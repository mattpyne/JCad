package org.pyne.jcad.core.math.brep.creators;

import haxe.root.Array;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Vertex;
import org.pyne.jcad.core.math.geom.Tolerance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public class VertexFactory {

    private List<Vertex> vertices = new ArrayList();

    public void addVertice(Vertex vertice) {
        vertices.add(vertice);
    }

    public void addVertices(Collection<Vertex> vertices) {
        vertices.forEach(v -> addVertice(v));
    }

    public Vertex find(Point3DC point) {
        for (Vertex vertex : vertices) {
            if (Tolerance.veq(point, vertex.point())) {
                return vertex;
            }
        }
        return null;
    }

    public Vertex find(Array<Number> point) {
        Point3DC pointToCheck = new Point3DC(point);
        for (Vertex vertex : vertices) {
            if (Tolerance.veq(pointToCheck, vertex.point())) {
                return vertex;
            }
        }
        return null;
    }

    public Vertex find(Vertex vertex) {
        return find(vertex.point());
    }

    public Vertex create(Point3DC point, Function<Vertex, Vertex> onExistent) {
        Vertex vertex = find(point);
        if (null == vertex) {
            vertex = new Vertex(point);
            vertices.add(vertex);
        } else if (null != onExistent) {
            return onExistent.apply(vertex);
        }
        return vertex;
    }

    public Vertex create(Point3DC point) {
        return create(point, null);
    }
}
