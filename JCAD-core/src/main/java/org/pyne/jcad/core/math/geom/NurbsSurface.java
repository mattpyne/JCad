package org.pyne.jcad.core.math.geom;

import org.pyne.jcad.core.verbinterop.VerbNurbsHelpers;
import haxe.root.Array;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.pyne.jcad.core.Cache;

import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.verbinterop.VerbInterop;
import verb.core.Interval;
import verb.core.MeshData;
import verb.core.NurbsCurveData;
import verb.core.NurbsSurfaceData;
import verb.eval.AdaptiveRefinementOptions;
import verb.eval.Intersect;
import verb.eval.Make;

/**
 * @author Matthew Pyne
 */
public class NurbsSurface extends Surface {

    private verb.geom.NurbsSurface verbSurf;
    private NurbsSurfaceData data;

    private double uMid;
    private double vMid;
    private double uMin;
    private double vMin;
    private double uMax;
    private double vMax;

    public NurbsSurface(verb.geom.NurbsSurface verbSurface, boolean inverted, PlanarSurface simpleSurface) {
        super();
        if (inverted) {
            verbSurface = verbSurface.reverse(true);
        }
        
        Interval<Number> domainU = verbSurface.domainU();
        Interval<Number> domainV = verbSurface.domainV();

        uMin = domainU.min.doubleValue();
        uMax = domainU.max.doubleValue();
        vMin = domainV.min.doubleValue();
        vMax = domainV.max.doubleValue();
        uMid = (uMax - uMid) * 0.5;
        vMid = (vMax - vMid) * 0.5;
        
        this.data = verbSurface.asNurbs();
        this.verbSurf = verbSurface;

        this.mirrored = isMirrored(this);
        this.simpleSurface = null != simpleSurface ? simpleSurface : figureOutSimpleSurface(this);
    }

    public NurbsSurface(verb.geom.NurbsSurface verbSurface) {
        this(verbSurface, false, null);
    }

    public verb.geom.NurbsSurface impl() {
        return verbSurf;
    }

    @Override
    public Interval<Number> domainU() {
        return verbSurf.domainU();
    }

    @Override
    public Interval<Number> domainV() {
        return verbSurf.domainV();
    }

    @Override
    public NurbsSurface toNurbs() {
        return this;
    }

    @Override
    public Surface transform(Matrix3 tr) {
        return new NurbsSurface(verbSurf.transform(tr.toVerbArray()), false, null);
    }

    @Override
    public Point3DC normalUV(double u, double v) {
        Point3DC normal = new Point3DC(verbSurf.normal(u, v));
        return normal.normalized();
    }

    public Point3DC normalInMiddle() {
        return normalUV(uMid, vMid);
    }

    public Point3DC pointInMiddle() {
        return point(uMid, vMid);
    }

    public Point3DC southWestPoint() {
        return point(uMin, vMin);
    }

    public Point3DC southEastPoint() {
        return point(uMax, vMin);
    }

    public Point3DC northEastPoint() {
        return point(uMax, vMax);
    }

    public Point3DC northWestPoint() {
        return point(uMin, vMax);
    }

    @Override
    public Point3DC point(double u, double v) {
        return cache().get(Cache.POINT + Double.hashCode(v) + Double.hashCode(u) * 66,
                () -> new Point3DC(verbSurf.point(u, v)));
    }

    /**
     * @param point [u, v]
     * @return
     */
    @Override
    public Array<Number> param(Point3DC point) {
        return cache().get(
                Cache.PARAM + point.hashCode(),
                () -> verbSurf.closestParam(point.asArray()));
    }

    @Override
    public List<Curve> intersectSurfaceForSameClass(Surface other, double tol) {
        if (!(other instanceof NurbsSurface)) {
            return new ArrayList();
        }

        NurbsSurface otherNurbsSurface = (NurbsSurface) other;
        if (!otherNurbsSurface.bounds().intersects(bounds(), Tolerance.TOLERANCE_BOUNDS_FAIL)) {
            return new ArrayList();
        }

        Array<NurbsCurveData> curves = Intersect.surfaces(
                tessellate(),
                otherNurbsSurface.tessellate(),
                data,
                otherNurbsSurface.data,
                tol);
        
        // @TODO If planes are degreeU 1 in each then any intersection is a line.
        // Make it so!

        List<Curve> normalizedCurves = new ArrayList();
        for (int i = 0; i < curves.length; i++) {
            normalizedCurves.add(
                    new NurbsCurve(
                            NurbsCurve.newVerbCurve(
                                    VerbNurbsHelpers.normalizeCurveParametrizationIfNeeded(
                                            curves.get(i)
                                    )
                            )));
        }

        return normalizedCurves;
    }

    @Override
    public MeshData tessellate() {
        return cache().get(Cache.SURF_MESH,
                () -> {
                    AdaptiveRefinementOptions options = new AdaptiveRefinementOptions();
                    options.normTol = Tolerance.TESS_TOLERANCE;
                    return verbSurf.tessellate(options);
                });
    }

    @Override
    public NurbsSurface invert() {
        return new NurbsSurface(verbSurf.clone(), true, null);
    }

    public Curve isoCurve(double param, boolean useV) {
        return cache().get(
                Cache.SURF_ISO_CURVE + Double.hashCode(param * (useV ? 1 : -3)),
                () -> {
                    NurbsCurveData data = Make.surfaceIsocurve(this.data, param, useV);
                    return new NurbsCurve(NurbsCurve.newVerbCurve(data), VerbInterop.getIsoCurveType(verbSurf, useV));
                });
    }

    @Override
    public Curve isoCurveAlignU(double param) {
        return isoCurve(param, true);
    }

    @Override
    public Curve isoCurveAlignV(double param) {
        return isoCurve(param, false);
    }

    @Override
    public PlanarSurface tangentPlane(double u, double v) {
        Point3DC normal = normalUV(u, v);
        return new PlanarSurface(normal, normal.dot(point(u, v)));
    }

    @Override
    public NurbsSurface copy() {
        return new NurbsSurface(verbSurf.clone(), false, null);
    }

    @Override
    public HashSet<PlanarSurface> getApproxSurface() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static PlanarSurface figureOutSimpleSurface(NurbsSurface nurbs) {
        if (VerbNurbsHelpers.surfaceMaxDegree(nurbs.data) == 1) {
            return nurbs.tangentPlane(nurbs.uMid, nurbs.vMid);
        }
        return null;
    }

    @Override
    public boolean contains(Point3DC potentialIntersection) {
        return false;
    }

    @Override
    public int degreeV() {
        return data.degreeV;
    }

    @Override
    public int degreeU() {
        return data.degreeU;
    }

    @Override
    public List<Surface> split(double u, boolean useV) {
        return impl().split(u, useV).stream()
                .map(NurbsSurface::new)
                .collect(Collectors.toList());
    }
}
