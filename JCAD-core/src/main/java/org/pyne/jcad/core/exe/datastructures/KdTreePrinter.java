package org.pyne.jcad.core.exe.datastructures;

import java.util.ArrayList;
import java.util.List;

class KdTreePrinter {

    public static String getString(KdTree tree) {
        if (tree.root == null)
            return "Tree has no nodes.";
        return getString(tree.root, "", true);
    }

    private static String getString(KdNode node, String prefix, boolean isTail) {
        StringBuilder builder = new StringBuilder();

        if (node.parent != null) {
            String side = "left";
            if (node.parent.greater != null && node.id.equals(node.parent.greater.id))
                side = "right";
            builder.append(prefix + (isTail ? "└── " : "├── ") + "[" + side + "] " + "depth=" + node.depth + " id="
                    + node.id + "\n");
        } else {
            builder.append(prefix + (isTail ? "└── " : "├── ") + "depth=" + node.depth + " id=" + node.id + "\n");
        }
        List<KdNode> children = null;
        if (node.lesser != null || node.greater != null) {
            children = new ArrayList<KdNode>(2);
            if (node.lesser != null)
                children.add(node.lesser);
            if (node.greater != null)
                children.add(node.greater);
        }
        if (children != null) {
            for (int i = 0; i < children.size() - 1; i++) {
                builder.append(getString(children.get(i), prefix + (isTail ? "    " : "│   "), false));
            }
            if (children.size() >= 1) {
                builder.append(getString(children.get(children.size() - 1), prefix + (isTail ? "    " : "│   "),
                        true));
            }
        }

        return builder.toString();
    }
}