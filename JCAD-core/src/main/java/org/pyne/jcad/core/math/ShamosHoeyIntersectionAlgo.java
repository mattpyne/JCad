package org.pyne.jcad.core.math;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

/**
 *
 * @author Matthew
 */
public class ShamosHoeyIntersectionAlgo {

    private final static double EPS_SQ = 1E-6;

    private boolean checkEnds;
    
    public ShamosHoeyIntersectionAlgo(boolean checkEnds) {
        this.checkEnds = checkEnds;
    }
    
    public boolean perform(Collection<Line2D> lines) {
        List<AlgEvent> events = new ArrayList<>(lines.size() * 2);
        for (Line2D li : lines) {
            if (li.getX1() < li.getX2()) {
                Line2D l = new Line2D.Double(li.getP1(), li.getP2());
                events.add(new AlgEvent(l, true));
                events.add(new AlgEvent(l, false));
            } else if (li.getX1() > li.getX2()) {
                Line2D l = new Line2D.Double(li.getP2(), li.getP1());
                events.add(new AlgEvent(l, true));
                events.add(new AlgEvent(l, false));
            } else {
                if (li.getY1() < li.getY2()) {
                    Line2D l = new Line2D.Double(li.getP1(), li.getP2());
                    events.add(new AlgEvent(l, true));
                    events.add(new AlgEvent(l, false));
                } else if (li.getY1() > li.getY2()) {
                    Line2D l = new Line2D.Double(li.getP2(), li.getP1());
                    events.add(new AlgEvent(l, true));
                    events.add(new AlgEvent(l, false));
                } else {
                    return true;
                }
            }
        }
        try {
            Collections.sort(events, new AlgEvtComparator());
        } catch (IllegalArgumentException ex) {
            return true;
        }
        TreeSet<Line2D> sl = new TreeSet<>(new LineComparator());
        for (AlgEvent e : events) {
            if (e.isStart) {
                Line2D nl = e.line;
                Line2D above = sl.higher(nl);
                if (above != null && checkEnds(above, nl)) {
                    if (above.intersectsLine(nl)) {
                        return true;
                    }
                }
                Line2D below = sl.lower(nl);
                if (below != null && checkEnds(below, nl)) {
                    if (below.intersectsLine(nl)) {
                        return true;
                    }
                }
                sl.add(nl);
            } else {
                Line2D nl = e.line;
                Line2D above = sl.higher(nl);
                Line2D below = sl.lower(nl);
                sl.remove(nl);
                if (above != null && below != null && checkEnds(above, below)) {
                    if (above.intersectsLine(below)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkEnds(Line2D o1, Line2D o2) {
        
        if (checkEnds) {
            return true;
        }

        return o1.getP1().distanceSq(o2.getP1()) > EPS_SQ
                && o1.getP1().distanceSq(o2.getP2()) > EPS_SQ
                && o1.getP2().distanceSq(o2.getP1()) > EPS_SQ
                && o1.getP2().distanceSq(o2.getP2()) > EPS_SQ;
    }

    class AlgEvent {

        public Line2D line;
        public boolean isStart;

        AlgEvent(Line2D l, boolean isStart) {
            line = l;
            this.isStart = isStart;
        }

        Point2D getPoint() {
            return (isStart) ? line.getP1() : line.getP2();
        }

        double getX() {
            return (isStart) ? line.getX1() : line.getX2();
        }

        double getY() {
            return (isStart) ? line.getY1() : line.getY2();
        }

        @Override
        public String toString() {
            return "start =  " + isStart + ", point = " + this.getPoint() + ", line = " + line.getP1() + " : " + line.getP2();
        }

    }

    class AlgEvtComparator implements Comparator<AlgEvent> {

        @Override
        public int compare(AlgEvent o1, AlgEvent o2) {
            if (o1.getX() < o2.getX()) {
                return -1;
            } else if (o1.getX() > o2.getX()) {
                return 1;
            } else {
                if (o1.getY() < o2.getY()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }

    }

    /**
     * Class to compare lines, to ensure above-below order.
     */
    class LineComparator implements Comparator<Line2D> {

        @Override
        public int compare(Line2D o1, Line2D o2) {
            if (o1.getY1() < o2.getY1()) {
                return -1;
            } else if (o1.getY1() > o2.getY2()) {
                return 1;
            } else {
                if (o1.getY2() < o2.getY2()) {
                    return -1;
                } else if (o1.getY2() > o2.getY2()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

    }
}
