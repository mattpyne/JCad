package org.pyne.jcad.core.math.brep.eval.bool;

import java.util.ArrayList;
import java.util.HashMap;
import org.pyne.jcad.core.math.brep.HalfEdge;
import org.pyne.jcad.core.math.brep.Face;
import java.util.List;
import java.util.Set;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.eval.EdgeGraph;

/**
 *
 * @author Matthew Pyne
 */
public class FaceOperationData extends EdgeGraph {

    public Set<Loop> detectedLoops;
    public Face face;
    public List<HalfEdge> newEdges;
    public List<HalfEdge> collidedEdges;
    public List<Face> overlaps;
    public List<HalfEdge> transferedFrom;

    public FaceOperationData(Face face) {
        super();
        this.face = face;
        this.collidedEdges = new ArrayList<>();
        this.newEdges = new ArrayList<>();
        this.vertexToEdge = new HashMap<>();
        this.overlaps = null;
    }

    public void markTransferredFrom(HalfEdge edge) {
        if (null == transferedFrom) {
            transferedFrom = new ArrayList<>();
        }
        transferedFrom.add(edge);
    }

    public void initGraph() {
        vertexToEdge.clear();
        for (HalfEdge he : face.halfEdges()) {
            addToGraph(he);
        }
        for (HalfEdge he : newEdges) {
            addToGraph(he);
        }
    }

    private void addToGraph(HalfEdge he) {

        if (null != transferedFrom && transferedFrom.contains(he)) {
            return;
        }

        HalfEdge opp = findOppositeEdge(he);
        if (null != opp) {
            collidedEdges.add(opp);
            collidedEdges.add(he);
        }

        List<HalfEdge> list = vertexToEdge.get(he.vertexA());
        if (null == list) {
            list = new ArrayList<>();
            vertexToEdge.put(he.vertexA(), list);
        } else {
            for (HalfEdge ex : list) {
                if (he.vertexB().equals(ex.vertexB()) && HalfEdge.isSameEdge(he, ex)) {
                    collidedEdges.add(ex);
                    collidedEdges.add(he);
                }
            }
        }
        list.add(he);
        graphEdges.add(he);
    }

    private HalfEdge findOppositeEdge(HalfEdge e1) {
        List<HalfEdge> others = vertexToEdge.get(e1.vertexB());
        if (null != others) {
            for (HalfEdge e2 : others) {
                if (e1.vertexA().equals(e2.vertexB()) && HalfEdge.isSameEdge(e1, e2)) {
                    return e2;
                }
            }
        }
        return null;
    }
}
