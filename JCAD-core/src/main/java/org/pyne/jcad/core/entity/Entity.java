package org.pyne.jcad.core.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * The object which all actual things are. For example features such as pockets,
 * cutouts, bends, thick faces, countersinks etc
 *
 * @author Matthew Pyne
 */
public class Entity {

    private final ArrayList<IComponent> components;

    /**
     * 
     */
    public Entity() {
        super();
        this.components = new ArrayList<>();
    }
    
//    public Entity copy() {
//        Entity copy = new Entity();
//        
//        components.forEach(comp -> copy.addComponent(comp.copy()));
//        
//        return copy;
//    }

    /**
     * You can only have one type of component on an entity. If a component of
     * the same type is found it will be replaced.
     *
     * @param component
     */
    public void addComponent(IComponent component) {
        component.register(this);
        components.removeIf((t) ->  t.getClass().isInstance(component));
        
        components.add(component);
    }

    /**
     * Finds the component of the given type.
     *
     * @param <T>
     * @param componentType
     * @return
     */
    public <T extends IComponent> Optional<T> getComponent(Class<T> componentType) {
        return components.stream()
                .filter(componentType::isInstance)
                .map(componentType::cast)
                .findAny();
    }
    
    /**
     * 
     */
    public void update() {
        components.forEach(IComponent::update);
    }
    
    /**
     * 
     */
    public void invalidate() {
        components.forEach(IComponent::invalidate);
    }

    /**
     * 
     * @return 
     */
    public Collection<IComponent> getComponents() {
        return components;
    }

}
