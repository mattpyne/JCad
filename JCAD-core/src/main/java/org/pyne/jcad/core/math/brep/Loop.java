package org.pyne.jcad.core.math.brep;

import org.pyne.jcad.core.Debug;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.brep.eval.Enclose;
import org.pyne.jcad.core.DataContainer;
import eu.mihosoft.jcsg.Polygon;
import eu.mihosoft.vvecmath.Vector3d;
import haxe.lang.EmptyObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.pyne.jcad.core.Cache;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.geom.Surface;
import verb.core.BoundingBox;

/**
 *
 * @author matts pyne
 */
public class Loop extends DataContainer implements Topo {

    private List<HalfEdge> edges;
    private Boolean closed;

    private Polygon polygonForm;
    private Face face;

    /**
     *
     */
    public Loop() {
        edges = new ArrayList<>();
    }

    /**
     *
     * @param parentFace
     */
    public Loop(Face parentFace) {
        edges = new ArrayList<>();
        face = parentFace;
    }

    /**
     *
     * @param other
     */
    public Loop(Loop other) {
        this();
        for (int i = 0; i < other.edges.size(); i++) {
            HalfEdge he = other.edges.get(i);
            Edge edge = he.edge().copy();

            edges.add(he.isInverted() ? edge.halfEdge2() : edge.halfEdge1());
        }
        link();
    }

    /**
     *
     * @return
     */
    public Loop copy() {
        return new Loop(this);
    }

    public List<Enclose> encloses() {
        List<Enclose> encloses = new ArrayList();
        int length = edges.size();
        for (int i = 0; i < length; i++) {
            int j = (i + 1) % length;
            HalfEdge curr = edges.get(i);
            HalfEdge next = edges.get(j);
            if (!curr.vertexB().equals(next.vertexA())) {
                throw new RuntimeException("Generating enclose on invalid Loop");
            }
            Enclose enclose = new Enclose();
            enclose.curr = curr;
            enclose.next = next;
            enclose.currVertex2 = curr.vertexB();
            encloses.add(enclose);
        }
        return encloses;
    }

    @Override
    public void transform(Matrix3 tr) {
        cache().clear();
        edges.forEach(edge -> edge.edge().transform(tr));
    }

    @Override
    public Loop transformed(Matrix3 tr) {
        Loop instance = copy();

        instance.transform(tr);

        return instance;
    }

    /**
     *
     * @return
     */
    public Face face() {
        return face;
    }

    /**
     *
     * @param face
     */
    public void setFace(Face face) {
        this.face = face;
    }

    /**
     *
     * @param edgeToAdd
     * @return
     */
    public boolean addEdge(HalfEdge edgeToAdd) {

        if (null == edgeToAdd) {
            return false;
        }

        if (!edgeToAdd.isValid()) {
            return false;
        }

        if (!edges.isEmpty() && Debug.ON) {

            if (!edgeToAdd.getPos1().equals(edges.get(edges.size() - 1).getPos2())) {
                Debug.log(Debug.LEVEL.ERROR, "Adding a edge which has the wrong orientation");
            }

            List<Point3DC> tessOfEdgeBefore = edges.get(edges.size() - 1).tessellate();
            List<Point3DC> tessOfEdge = edgeToAdd.tessellate();

            if (!tessOfEdge.get(0).equals(tessOfEdgeBefore.get(tessOfEdgeBefore.size() - 1))) {
                Debug.log(Debug.LEVEL.ERROR, "Adding a edge which has the wrong orientation");
            }
        }

        edgeToAdd.setLoop(this);

        return edges.add(edgeToAdd);
    }

    /**
     *
     * @param edgeToAdd
     * @return
     */
    public boolean addEdgeToFront(HalfEdge edgeToAdd) {

        if (null == edgeToAdd) {
            return false;
        }

        if (!edgeToAdd.isValid()) {
            return false;
        }

        if (!edges.isEmpty() && Debug.ON) {

            if (!edgeToAdd.getPos2().equals(edges.get(0).getPos1())) {
                Debug.log(Debug.LEVEL.ERROR, "Adding a edge which has the wrong orientation");
            }

            List<Point3DC> tessOfEdgeBefore = edges.get(0).tessellate();
            List<Point3DC> tessOfEdge = edgeToAdd.tessellate();

            if (!tessOfEdge.get(tessOfEdge.size() - 1).equals(tessOfEdgeBefore.get(0))) {
                Debug.log(Debug.LEVEL.ERROR, "Adding a edge which has the wrong orientation");
            }
        }

        edges.add(0, edgeToAdd);

        edgeToAdd.setLoop(this);

        return true;
    }

    /**
     *
     */
    public void reverse() {
        Collections.reverse(edges);
        cache().clear(Cache.TESS_LOOP);
        cache().clear(Cache.TESS_LOOP_UV);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return edges.isEmpty();
    }

    /**
     *
     * @return
     */
    public int size() {
        return edges.size();
    }

    /**
     *
     * @return
     */
    public Stream<Edge> streamEdges() {
        return edges.stream().map(HalfEdge::edge);
    }
    
    /**
     *
     * @return
     */
    public Stream<HalfEdge> streamHalfEdges() {
        return edges.stream();
    }

    /**
     *
     * @return
     */
    public List<HalfEdge> halfEdges() {
        return edges;
    }

    public void setHalfEdges(List<HalfEdge> actualHalfEdges) {
        edges = actualHalfEdges;
    }

    /**
     *
     * @return
     */
    public Stream<double[]> streamVerticesArray() {
        return streamHalfEdges().flatMap(HalfEdge::streamPositionsArray);
    }

    /**
     *
     * @return
     */
    public Stream<Vertex> streamVertices() {
        return streamHalfEdges().flatMap(HalfEdge::streamVertices);
    }

    /**
     *
     * @return
     */
    public List<double[]> getVertices() {
        return streamVerticesArray().collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public Stream<double[]> streamUniqueOrderedVertices() {

        return streamHalfEdges()
                .map(HalfEdge::getVert1);
    }

    /**
     *
     * @return
     */
    public Stream<Point3DC> streamPoints() {

        return streamUniqueOrderedVertices()
                .map(pt -> new Point3DC(pt[0], pt[1], pt[2]));
    }

    /**
     *
     * @return
     */
    public List<Point3DC> getPoints() {
        return streamPoints().collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public Stream<Vector3d> streamUniqueOrderedPoints() {

        return streamHalfEdges()
                .map(HalfEdge::getPos1);
    }

    /**
     *
     * @return
     */
    public List<Vector3d> getUniqueOrderedPoints() {
        return streamUniqueOrderedPoints().collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public List<double[]> getUniqueOrderedVertices() {
        return streamUniqueOrderedVertices().collect(Collectors.toList());
    }

    /**
     *
     * @param loop
     * @return
     */
    public boolean contains(Loop loop) {
        if (loop == this) {

            return true;
        }

        Polygon poly = getPolygonForm();
        Polygon otherPoly = loop.getPolygonForm();
        if (null == poly
                || null == otherPoly) {

            return false;
        }

        return poly.contains(otherPoly);
    }

    /**
     *
     * @param concavPolygonSet1
     * @param concavePolygonSet2
     * @return
     */
    public boolean contains(List<Polygon> concavPolygonSet1, List<Polygon> concavePolygonSet2) {
        return concavePolygonSet2
                .stream()
                .noneMatch((polygon2) -> (!contains(concavePolygonSet2, polygon2)));
    }

    /**
     *
     * @param concavePolySet
     * @param poly
     * @return
     */
    public boolean contains(List<Polygon> concavePolySet, Polygon poly) {

        return poly.vertices
                .stream()
                .noneMatch((vert) -> (!contains(concavePolySet, vert.pos)));
    }

    /**
     *
     * @param concavePolySet
     * @param point
     * @return
     */
    public boolean contains(List<Polygon> concavePolySet, Vector3d point) {
        return concavePolySet
                .stream()
                .anyMatch((polygon) -> (polygon.contains(point)));
    }

    /**
     *
     * @param loops
     * @return
     */
    public boolean contains(List<Loop> loops) {
        return loops.stream()
                .allMatch(this::contains);
    }

    /**
     *
     * @param loops
     * @return
     */
    public boolean notContained(List<Loop> loops) {
        return loops.stream()
                .allMatch(this::notContained);
    }

    /**
     *
     * @param loop
     * @return
     */
    public boolean notContained(Loop loop) {
        if (loop == this) {
            return true;
        }

        Polygon poly = getPolygonForm();
        Polygon otherPoly = loop.getPolygonForm();
        if (null == poly
                || null == otherPoly) {

            return true;
        }

        return !otherPoly.contains(poly);
    }

    /**
     * A set of Convex polygons. The set may be concave.
     *
     * @return
     * @author Matt P.
     */
    public Polygon getPolygonForm() {
        if (null == polygonForm) {

            List<Vector3d> verts = getUniqueOrderedPoints();
            if (verts.size() >= 3) {

                polygonForm = Polygon.fromPoints(verts);
                if (!polygonForm.isValid()) {
                    System.out.println("huh");
                }
            }
        }
        return polygonForm;
    }

    /**
     *
     * @return
     */
    public HalfEdge getStart() {
        if (edges.isEmpty()) {

            return null;
        }
        return edges.get(0);
    }

    /**
     *
     * @return
     */
    public HalfEdge getEnd() {
        if (edges.isEmpty()) {

            return null;
        }
        return edges.get(edges.size() - 1);
    }

    /**
     *
     * @return
     */
    public boolean isClosed() {
        if (edges.isEmpty()) {
            return false;
        }
        if (null == closed) {

            closed = getStart().vertexA().equals(getEnd().vertexB());
        }
        return closed;
    }

    /**
     *
     */
    private void clearCache() {
        polygonForm = null;
        closed = null;
    }

    public void link() {
        for (int i = 0; i < edges.size(); i++) {
            int j = (i + 1) % edges.size();

            HalfEdge curr = edges.get(i);
            HalfEdge next = edges.get(j);

            curr.setNext(next);
            next.setPrev(curr);

            curr.setLoop(this);
        }
    }

    public List<Point3DC> tessellate() {
        return cache().get(Cache.TESS_LOOP, () -> {
            List<Point3DC> out = new ArrayList<>();
            for (HalfEdge e : edges) {
                List<Point3DC> curvePoints = e.tessellate();

                curvePoints.remove(0);

                out.addAll(curvePoints);
            }
            return out;
        });
    }

    public List<Point3DC> tessellateUV() {
        return cache().get(Cache.TESS_LOOP_UV, () -> {
            Surface surf = face().surface().impl();

            return CadMath.convertPointsToUV(surf, tessellate());
        });
    }

    public BoundingBox bounds() {
        return cache().get(
                Cache.BOUNDING_BOX,
                () -> {
                    BoundingBox bounds = new BoundingBox(EmptyObject.EMPTY);
                    for (HalfEdge he : halfEdges()) {
                        bounds.add(he.bounds().max);
                        bounds.add(he.bounds().min);
                    }
                    return bounds;
                });
    }
}
