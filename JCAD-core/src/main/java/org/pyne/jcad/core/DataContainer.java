package org.pyne.jcad.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.pyne.jcad.core.math.brep.eval.bool.FaceOperationData;

/**
 *
 * @author matt pyne
 */
public class DataContainer {

    private String id;
    private Map<String, Object> data;
    public FaceOperationData op;
    private Cache cache;

    public DataContainer() {

    }

    public DataContainer(DataContainer other) {
        cache = null;
        id = other.id;
        data = other.data;
    }

    /**
     *
     * @return
     */
    public String getId() {
        if (null == id) {
            id = UUID.randomUUID().toString();
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public <T> boolean hasData(String key, Class<T> clazz) {
        return data(key, clazz).isPresent();
    }

    /**
     *
     * @param <T>
     * @param key
     * @param clazz
     * @return
     */
    public <T> Optional<T> data(String key, Class<T> clazz) {

        Object entry = this.data().get(key);

        if (clazz.isInstance(entry)) {
            return Optional.of((T) entry);
        }

        return Optional.empty();
    }

    /**
     *
     * @param key
     * @param entry
     */
    public void putData(String key, Object entry) {
        data().put(key, entry);
    }

    /**
     *
     * @param key
     */
    public void delete(String key) {
        data().remove(key);
    }

    public Map<String, Object> data() {
        if (null == data) {
            data = new HashMap<>();
        }
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Cache cache() {
        if (null == cache) {
            cache = new Cache();
        }
        return cache;
    }

}
