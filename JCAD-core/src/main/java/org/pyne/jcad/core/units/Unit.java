package org.pyne.jcad.core.units;


public abstract class Unit {

    private Prefix prefix;
    private UnitName unitName;

    Unit(Prefix prefix, UnitName unitName) {
        this.prefix = prefix;
        this.unitName = unitName;
    }

    public Prefix getPrefix() {
        return prefix;
    }

    public UnitName getUnitName() {
        return unitName;
    }

    public double calculateConversionFactorTo(Unit other) {
        if (!this.getClass().isInstance(other)) {
            return 0.;
        }

        return  getPrefix().get() / other.getPrefix().get();
    }
}
