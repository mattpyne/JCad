package org.pyne.jcad.core.math.brep.eval.bool;

import haxe.root.Array;
import java.util.ArrayList;
import java.util.List;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Vertex;

/**
 *
 * @author Matthew
 */
class VertexIntersection {

    double u;
    Point3DC p;
    List<Vertex> vertexHolder = new ArrayList();

    public VertexIntersection(double u, Point3DC p) {
        this.u = u;
        this.p = p;
    }

    public VertexIntersection(Number u, Point3DC p) {
        this.u = u.doubleValue();
        this.p = p;
    }

    public VertexIntersection(Number u, Array<Number> p) {
        this.u = u.doubleValue();
        this.p = new Point3DC(p);
    }
}
