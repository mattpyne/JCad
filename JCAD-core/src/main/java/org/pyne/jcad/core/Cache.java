package org.pyne.jcad.core;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * @author Matthew Pyne
 */
public class Cache {

    public final static int EDGE_COLLINEAR_FACE = 1;
    public final static int CURVE_EDGE_COINCIDENT = 2;
    public final static int POINT = 3;
    public final static int PARAM = 4;
    public final static int SURF_ISO_CURVE = 5;
    public final static int FACE_WORKING_POLYGON = 6;
    public final static int BOUNDING_BOX = 7;
    public final static int TESS_LOOP = 8;
    public final static int TESS_LOOP_UV = 9;
    public final static int CURVE_CURVE_INTERSECTION = 10;
    public final static int SURF_SURF_INTERSECTION = 11;
    public final static int NORMAL = 12;
    public final static int SURF_MESH = 13;
    
    
    private final static boolean CACHING_OFF = false;
    private final Map<Integer, Object> map = new HashMap();

    public <T extends Object> T get(Integer id, final Supplier<T> op) {
        if (CACHING_OFF) {
            return op.get();
        }

        Object result = map.get(id);
        if (null == result) {

            result = op.get();
            map.put(id, result);
        }

        return (T) result;
    }
    
    public void clear(Integer key) {
        map.remove(key);
    }

    public void clear() {
        map.clear();
    }
}
