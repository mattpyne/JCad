package org.pyne.jcad.core.entity;

import org.pyne.jcad.core.math.Transform;

/**
 *
 * @author Matthew Pyne
 */
public class Connection {

    private Entity parent;
    private Entity child;

    private Transform transform;

    public Transform getTransform() {
        return transform;
    }

    public void setTransform(Transform transform) {
        this.transform = transform;
    }

    public Entity getParent() {
        return parent;
    }

    public void setParent(Entity parent) {
        this.parent = parent;
    }

    public Entity getChild() {
        return child;
    }

    public void setChild(Entity child) {
        this.child = child;
    }

}
