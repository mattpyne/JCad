package org.pyne.jcad.core.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Matrix3;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.*;
import org.pyne.jcad.core.math.brep.creators.BrepBuilder;
import org.pyne.jcad.core.math.brep.creators.BrepEnclose;
import org.pyne.jcad.core.math.brep.creators.BrepFaceEvolve;
import org.pyne.jcad.core.math.brep.eval.bool.OperationObserver;
import org.pyne.jcad.core.math.geom.Tolerance;

/**
 *
 * @author Matthew Pyne
 */
public class ExtrudeOperation implements IOperation {

    private final Expression amount;
    private final Expression offset;
    private final Expression tapper;
    private final Expression xDir;
    private final Expression yDir;
    private final Expression zDir;
    private List<Loop> loops;
    private BrepSurface surface;

    private Topo topoAt;
    private List<String> errorMessages = new ArrayList<>();
    private OperationObserver observer;

    public ExtrudeOperation() {
        amount = new Expression("0");
        offset = new Expression("0");
        xDir = new Expression("0");
        yDir = new Expression("0");
        zDir = new Expression("1");
        loops = new ArrayList<>();
        tapper = new Expression("0");
    }

    public ExtrudeOperation(
            double amount,
            Point3DC dir,
            List<Loop> loops,
            BrepSurface surface) {

        this.amount = new Expression(amount);
        this.offset = new Expression("0");
        this.tapper = new Expression("0");
        this.xDir = new Expression(dir.getX());
        this.yDir = new Expression(dir.getY());
        this.zDir = new Expression(dir.getZ());
        this.loops = loops;
        this.surface = surface;
    }

    public Expression getAmount() {
        return amount;
    }

    public double getAmountEval() {
        return amount.eval();
    }

    public void setAmountValue(double amount) {
        this.amount.setValue(amount);
    }

    public void setAmountValue(String amount) {
        this.amount.setValue(amount);
    }

    public Expression getxDir() {
        return xDir;
    }

    public Expression getyDir() {
        return yDir;
    }

    public Expression getzDir() {
        return zDir;
    }

    public double getTapperEval() {
        return tapper.eval();
    }

    public Expression getTapper() {
        return tapper;
    }

    public void setTapperValue(double tapper) {
        this.tapper.setValue(tapper);
    }

    public void setTapperValue(String tapper) {
        this.tapper.setValue(tapper);
    }

    public List<Loop> getLoops() {
        return loops;
    }

    public void setLoops(List<Loop> loops) {
        this.loops = loops;
    }

    public BrepSurface getSurface() {
        return surface;
    }

    public void setSurface(BrepSurface surface) {
        this.surface = surface;
    }

    public Expression getOffset() {
        return offset;
    }

    public double getOffsetEval() {
        return offset.eval();
    }

    public void setOffsetValue(double offset) {
        this.offset.setValue(offset);
    }

    public void setOffsetValue(String offset) {
        this.offset.setValue(offset);
    }

    @Override
    public void onApply(Entity entity) {
        errorMessages.clear();

        entity.getComponent(Geometry.class).ifPresentOrElse(geom -> {

            Topo extrusion = topoOp();

            if (extrusion.isEmpty()) {
                errorMessages.add("Made an empty shape for extrusion.");
                return;
            }

            if (null == geom.getTopo()) {
                geom.setTopo(extrusion);
                return;
            }

            if (extrusion instanceof Shell && geom.getTopo() instanceof Shell) {

                extrudeShell(geom, (Shell) extrusion);
            } else if (extrusion instanceof CompoundShell && geom.getTopo() instanceof Shell) {

                ((CompoundShell)extrusion).shells()
                        .forEach(extrusionShell ->
                                extrudeShell(geom, extrusionShell));
            } else {
                errorMessages.add("Unknown type for extrusion. " +
                        "Extrusion: " + extrusion.getClass().getSimpleName() +
                        "Entity: " + geom.getTopo().getClass().getSimpleName());
            }

        }, () -> {
            errorMessages.add("Entity does have geometry.");
        });

    }

    private void extrudeShell(Geometry geom, Shell extrusion) {
        Shell baseShell = (Shell) geom.getTopo();

        boolean subtract = shouldSubtract(baseShell);

        Shell result;
        if (subtract) {

            result = baseShell.subtract(extrusion, observer);
        } else {

            result = baseShell.union(extrusion, observer);
        }

        if (result.faces().isEmpty()) {
            errorMessages.add("Failed to " + (subtract ? "subtract" : "union") + " extrusion.");
            return;
        }
        geom.setTopo(result);
        setTopoAt(result);
    }

    private boolean shouldSubtract(Shell baseShell) {
        boolean subtract = true;
        Point3DC dir = getDir();
        if (getAmountEval() < 0) {
            dir = dir.negated();
        }

        Point3DC nSketch = surface.normal(Point3DC.ZERO);
        Point3DC nFace = null;

        outer:
        for (Face faceA : baseShell.faces()) {
            Point3DC nA = faceA.surface().normal(Point3DC.ZERO);
            if (CadMath.eq(nA, nSketch.negated(), Tolerance.TOLERANCE) || CadMath.eq(nA, nSketch, Tolerance.TOLERANCE)) {

                if (surface.bounds().intersects(faceA.bounds(), Tolerance.TOLERANCE)) {
                    nFace = nA;
                    break;
                }
            }
        }

        if (null != nFace && Math.abs(dir.angle(nFace)) < 90.) {
            subtract = false;
        }
        return subtract;
    }

    private Point3DC getDir() {
        return new Point3DC(xDir.eval(), yDir.eval(), zDir.eval());
    }

    @Override
    public Topo topoAt() {
        return topoAt;
    }

    private void setTopoAt(Topo topo) {
        topoAt = topo;
    }

    @Override
    public Topo topoOp() {
        if (loops.isEmpty()) {
            return new Shell();
        }

        double offsetD = offset.eval();
        Point3DC offsetV = getDir().normalized().times(offsetD);
        Matrix3 offsetMat = Matrix3.createTranslation(-offsetV.x(), -offsetV.y(), -offsetV.z());
        if (null == surface) {
            surface = BrepBuilder.createBoundingNurbs(loops.get(0).tessellate(), null);
            surface.transform(offsetMat);
        }

        List<Loop> offsetLoops = loops.stream()
                .map(loop -> loop.transformed(offsetMat))
                .collect(Collectors.toList());

        List<Face> faces = BrepFaceEvolve.EvolveFace(surface, offsetLoops);

        // Single Face extrusion so a single Shell.
        double amountEval = amount.eval();
        if (faces.size() == 1) {
            return BrepEnclose.extrude(faces.get(0), getDir().times(amountEval + offsetD));
        }

        // Multiple Shells so CompoundShell
        CompoundShell compoundShell = new CompoundShell();
        for (Face face : faces) {
            compoundShell.add(BrepEnclose.extrude(face, getDir().times(amountEval + offsetD)));
        }

        return compoundShell;
    }

    @Override
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public void register(ExpressionSolver expressionSolver) {
        tapper.setSolver(expressionSolver);
        amount.setSolver(expressionSolver);
        offset.setSolver(expressionSolver);
        xDir.setSolver(expressionSolver);
        yDir.setSolver(expressionSolver);
        zDir.setSolver(expressionSolver);
    }

    public void setObserver(OperationObserver observer) {
        this.observer = observer;
    }
}
