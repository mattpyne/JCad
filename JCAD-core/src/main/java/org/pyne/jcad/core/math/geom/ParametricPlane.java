package org.pyne.jcad.core.math.geom;

import eu.mihosoft.vvecmath.Vector3d;
import haxe.root.Array;
import org.pyne.jcad.core.math.CadMath;
import org.pyne.jcad.core.math.Point3DC;

import java.util.Objects;

/**
 * @author matt pyne
 */
public class ParametricPlane {

    private Point3DC r0;
    private Point3DC r1;
    private Point3DC r2;

    private int form;

    /**
     * @param r0
     * @param r1
     * @param r2
     */
    public ParametricPlane(Vector3d r0, Vector3d r1, Vector3d r2) {
        this.r0 = new Point3DC(r0);
        this.r1 = new Point3DC(r1);
        this.r2 = new Point3DC(r2);
    }

    public ParametricPlane(Point3DC normal, double w) {
        double a = normal.getX();
        double b = normal.getY();
        double c = normal.getZ();
        double d = w;

        if (Math.abs(a) > 1e-3) {
            form = 1;
        } else if (Math.abs(b) > 1e-3) {
            form = 2;
        } else {
            form = 3;
        }

        switch (form) {
            case 1:
                r0 = Point3DC.xyz(d / a, 0, 0);
                r1 = Point3DC.xyz(-b / a, 1, 0);
                r2 = Point3DC.xyz(-c / a, 0, 1);
                break;
            case 2:
                r0 = Point3DC.xyz(0, d / b, 0);
                r1 = Point3DC.xyz(1, -a / b,  0);
                r2 = Point3DC.xyz(0, -c / b,  1);
                break;
            default:
                r0 = Point3DC.xyz(0, 0, d / c);
                r1 = Point3DC.xyz(1, 0, -a / c);
                r2 = Point3DC.xyz(0, 1, -b / c);
                break;
        }
    }

    /**
     * @return
     */
    public Point3DC getR0() {
        return r0;
    }

    /**
     * @param r0
     */
    public void setR0(Vector3d r0) {
        this.r0 = new Point3DC(r0);
    }

    /**
     * @return
     */
    public Vector3d getR1() {
        return r1;
    }

    /**
     * @param r1
     */
    public void setR1(Vector3d r1) {
        this.r1 = new Point3DC(r1);
    }

    /**
     * @return
     */
    public Vector3d getR2() {
        return r2;
    }

    /**
     * @param r2
     */
    public void setR2(Vector3d r2) {
        this.r2 = new Point3DC(r2);
    }

    /**
     * @param u
     * @param v
     * @return
     */
    public Point3DC point(double u, double v) {
        return r0.plus(r1.times(u)).plus(r2.times(v));
    }

    public Array<Number> param(Point3DC point) {
        if (form == 1) {

            return Array.from(point.getY(), point.getZ());
        } else if (form == 2) {

            return Array.from(point.getX(), point.getZ());
        } else {

            return Array.from(point.getX(), point.getY());
        }
    }


    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ParametricPlane other = (ParametricPlane) obj;
        if (!Objects.equals(this.r0, other.r0)) {
            return false;
        }
        if (!Objects.equals(this.r1, other.r1)) {
            return false;
        }
        return Objects.equals(this.r2, other.r2);
    }

}
