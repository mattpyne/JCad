package org.pyne.jcad.core.math.brep.eval;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.pyne.jcad.core.math.Point3DC;
import org.pyne.jcad.core.math.brep.Edge;
import org.pyne.jcad.core.math.brep.Face;
import org.pyne.jcad.core.math.brep.Loop;
import org.pyne.jcad.core.math.brep.Shell;
import org.pyne.jcad.core.math.geom.Tolerance;
import verb.core.BoundingBox;

/**
 * Checks if a BREP model is valid.
 *
 * @author Matthew Pyne
 */
public class BrepValidator {

    public static boolean isValid(Shell shell) {
        if (shell.faces().isEmpty()) {
            return false;
        }
        
        return isValidFaces(shell.faces());
    }

    public static boolean isValidFaces(Collection<Face> faces) {
        return isValidFaces(faces.stream());
    }

    public static boolean isValidFaces(Stream<Face> faces) {
        return isValidLoops(faces.flatMap(Face::streamLoops)
                                 .collect(Collectors.toSet()));
    }
    
    public static boolean isValidEdge(Edge edge) {
        return null != edge.halfEdge1().loop() 
                && null != edge.halfEdge2().loop();
    }
    
    public static boolean isValidEdges(Stream<Edge> edges) {
        return edges.allMatch(BrepValidator::isValidEdge);
    }

    public static boolean isValidLoops(Collection<Loop> loops) {
        if (loops.isEmpty()) {
            return false;
        }
        
        return loops.stream().allMatch(l -> loopIsValid(l, loops));
    }

    public static boolean loopIsValid(Loop loop, Collection<Loop> detectedLoopsSet) {
        return loop.streamHalfEdges().allMatch(e -> {
            if (detectedLoopsSet.contains(e.twin().loop())) {
                return true;
            } else {
                return false;
            }
        });
    }

    public static boolean isValidFaceBounds(Stream<Face> faces) {
        return faces.allMatch(face -> {

            BoundingBox faceBounds = face.bounds();
            BoundingBox surfaceBounds = face.surface().bounds();
            return Tolerance.ueq(new Point3DC(faceBounds.max), new Point3DC(surfaceBounds.max))
                    && Tolerance.ueq(new Point3DC(faceBounds.min), new Point3DC(surfaceBounds.min));
        });
    }
}
