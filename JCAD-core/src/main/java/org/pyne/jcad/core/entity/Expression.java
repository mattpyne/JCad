package org.pyne.jcad.core.entity;

public class Expression {

    private ExpressionSolver solver;
    private String value;

    public Expression(double amount) {
        this(Double.toString(amount));
    }

    public Expression(String value) {
        this.value = value;
        this.solver = ExpressionSolver.empty;
    }

    public ExpressionSolver getSolver() {
        return solver;
    }

    public void setSolver(ExpressionSolver solver) {
        this.solver = solver;
    }

    public String getValue() {
        return value;
    }

    public void setValue(double amount) {
        setValue(Double.toString(amount));
    }

    public void setValue(String value) {
        this.value = value;
    }

    public double eval() {
        return solver.eval(value);
    }

    public boolean checkSyntax() {
        return solver.checkSyntax(value);
    }
}
