# Default resources for the geomss.geom package.

# General
dimensionErr = Input element has incompatible dimensions.
# paramNullErr: {0} = parameter name
paramNullErr = Input parameter "{0}" can not be null.
# dimensionNot3: {0} = object type, {1} = dimension
dimensionNot3 = The {0} physical dimension must be 3, it is {1,number,integer}.
# dimensionNot3_2: {0} = object type
dimensionNot3_2 = The {0} physical dimension can not be anything but 3.
# dimensionNotAtLeast2 and dimensionNotAtLeast3: {0} = object type, {1} = dimension
dimensionNotAtLeast2 = The {0} physical dimension must be at least 2, it is {1,number,integer}.
dimensionNotAtLeast3 = The {0} physical dimension must be at least 3, it is {1,number,integer}.
# incompatibleUnits: {0} = parameter name, {1} = unit type name
incompatibleUnits = The {0} units are not compatible with {1}.
# zeroLengthListErr: {0} = list name
zeroLengthListErr = {0} must not have zero length.
# numGTZero: {0} = type string, {1} = number.
numGTZero = The {0} must be > 0, got {1,number}.
# size: The number of elements in a list.
size = size

# AbstractGeomList resources
#listElementTypeErr: {0} = the object class name, {1} = the element type the object may contain.
listElementTypeErr = This {0} may contain only {1} objects.
listNoGeometry = List contains no geometry.
collectionElementsNullErr = Elements of the input collection can not be null.

# PointArray resources
# arrStringSizeErr: {0} = index
arrStringSizeErr = String {0,number,integer} does not have the same number of points as the 1st string.

# AbstractCurve resources
crvInvalidSValue = Curves require a parameter in the range 0-1 inclusive; got s = {0,number}.
# invalidParamDim: {0} = the type of geometry object, {1} = the number of dimensions required.
invalidParamDim = {0} objects require a {1,number,integer}-dimensional parameter in the range 0-1 inclusive.
undefinedPrincipleNormal = The principal normal for the curve is undefined.
undefinedBinormal = Binormal vectors exist only for curves with at least 3 dimensions.
crvPointDimensionErr = The input point must have the same physical dimensions as this curve.
# crvUnsupportedGridRule: {0} = type of grid rule
crvUnsupportedGridRule = The {0} grid rule not supported by this curve.
# sameOrFewerDimErr: {0} = the first geometry type, {1} = the second geometry type.
sameOrFewerDimErr = The {0} must have the same or fewer dimensions than this {1}.
# inputDimOutOfRange: {0} = the object type
inputDimOutOfRange = The input dimension must be >= 0 and less than the physical dimension of this {0}.
# maxPointsWarningMsg: {0} = "curve" or "surface"; {1} = The ID number for the curve.
maxPointsWarningMsg = WARNING: Max. number of gridded points exceeded for {0} ID#{1,number,integer}.

# AbstractSurface resources
srfInvalidSValue = Surfaces require "s" parameter in range 0-1 inclusive; got s = {0,number}.
srfInvalidTValue = Surfaces require "t" parameter in range 0-1 inclusive; got t = {0,number}.
srfTransposeUnsupported = Method "transpose()" is not supported for this surface type.
lstSameSizeErr = The {0} must have the same number of elements as the {1}.
# srfUnsupportedGridRule: {0} = type of grid rule
srfUnsupportedGridRule = {0} grid rule not supported by this surface.

# GeomUtil resources
pointsColinearErr = The input points must not be co-linear.
t1ParallelTop2p1Err = Vector t1 is parallel to a vector from p1 to p2.
pOutDimErr = The output point must be the same physical dimension as the input geometry.

# GridSpacing resources
invTanhSpacing = Invalid inputs for tanh spacing: n must be <= 1/sqrt(ds1*ds2) or <= {0,number}.
spacingTypeLabels = Linear,Square,1 - Sqrt,Arc-Cosine,Cosine,Square Root

# GTransform resources
rowsMustHave4Elements = All rows must have exactly 4 elements.
fourVectorsReq = Four vector's are expected.
3x3or4x4Req = A 3x3 or 4x4 matrix is expected.
3x3DCMReq = Expected a 3x3 direction cosine matrix.
3elementVectorReq = Input vector must have exactly 3 elements.
3elementScaleFReq = The list of scale factors must have 3 elements.
3x3MatrixReq = Expected a 3x3 matrix.
need16ElementsFoundLess = GTransform requires 16 elements; {0,number,integer} found.
gtToManyElements = Too many elements; 16 are required.

# LoftedSurface resources
tDirDegreeErr = The t-direction degree must be > 0.
# incDefiningCrvCount: 0 = The type of object, 1 = the number of curves required, 2 = the number found.
incDefiningCrvCount = A {0} must have at least {1,number,integer} defining curves, but only {2,number,integer} were found.
incCrvDimension = The input {0} must have a physical dimension of {0,number,integer} to be used in this surface.
gradeLTZeroErr = Derivative grade must be >= 0.

# TFI Surface resources
s00t00NotCoincident = The start of the s0 curve and start of the t0 curve must be coincident.  Offset={0}.
s10t01NotCoincident = The start of the s1 curve and end of the t0 curve must be coincident.  Offset={0}.
s11t11NotCoincident = The end of the s1 curve and end of the t1 curve must be coincident.  Offset={0}.
s01t10NotCoincident = The end of the s0 curve and start of the t1 curve must be coincident.  Offset={0}.
# crvMissingXML: {0} = name
crvMissingXML = The {0} curve is missing from XML.

# Plane resources
pointsEqualErr = None of the input points may be equal to the others.

# Point resources
pointDimGT0 = The point dimension must be > 0.
consistantPointDim = Input point must be the same dimension as this point.

# SubrangeCurve resources
scIncParDim = The input "par" dimension must match the child's parametric dimension.
scUnsupportedParDim = Can not calc. derivatives on curve subranged onto a parDim = {0,number,integer} object.

# SubrangeSurface resources.
ssNumBCrvs = Incorrect number of parametric boundary curves.  Must have 4.

# Vector_stp resources.
vectorDimGT0 = The vector dimension must be > 0.

# Note resources
defFontName = RomanD
# defFontStyle: 0 = PLAIN, 1 = BOLD, 2 = ITALIC
defFontStyle = 1
defFontSize = 12

# This is a comma separated list of font's that should be loaded at run-time.
appFontNames = RomanD,OCR-B
appFontPaths = geomss/geom/RomanD.ttf,geomss/geom/OCR-B.ttf

# ModelNote resources
rotationDimensionLTE3 = To use a Rotation orientation, the physical dimension must be <= 3, it is {0,number,integer}.

# Triangle class
triThreePointsOnly = A triangle must be defined with exactly 3 points.

# Plane class
# planeNumPointsErr: {0} = The number of points input.
planeNumPointsErr = At least three input points are required, got {0,number,integer}.

# From the nurbs sub-package:

# CurveFactory class
posMultFactorErr = The multiplicative factor must be > 0, got {0,number}.
posDegreeErr = The degree must be > 0, got {0,number,integer}.
numPointsLTDegree = The number of points must exceed the degree of the curve:
# numItemMissmatchErr: {0} = the first item being compared, {1} = the size of the 1st item, {2} = second item being compared, {3} = size of 2nd item.
numItemMissmatchErr = The number of {0} ({1,number,integer}) must be the same as the number of {2} ({3,number,integer}).
# numPointsLEOneErr: {0} = parametric direction string ("S" or "T").
numPointsLEOneErr = The number of control points in the {0} direction must be > 1.
numPointsGTnumCPErr = The number of data points must exceed the number of control points in the curve.
paramArrNotZeroErr = The parameterization array must have 0.0 for the 1st element.
paramArrNotOneErr = The parameterization array must have 1.0 for the last element.
paramNotMonotonicErr = The parameterization array must be monotonically increasing.

# NurbsCurve class
numInsertionsLE0Err = The number of insertions must be > 0.
invalidKnotValue = Knots must be in the range 0-1 inclusive; got {0,number}.
knotsNotIncreasingErr = New knots must be monotonically increasing and between 0 and 1.
# illegalKnotIndexForRemoval: {0} = index number, {1} = minimum index, {2} = maxim index.
illegalKnotIndexForRemoval = Index {0,number,integer} is illegal for knot removal, must be >= {1,number,integer} and < {2,number,integer}.
negNumKnotsRemoveErr = Only a positive number of knots can be removed.
# canNotSplitAtEnds: {0} = object type string
canNotSplitAtEnds = Can not split a {0} at it's ends!

# BasicNurbsCurve class
crvWrongNumKnotsErr = NURBS curve has wrong number of knots: has {0,number,integer}, needs {1,number,integer} knots.
noControlPointsErr = No control points supplied.

# BasicNurbsSurface class
srfWrongNumSKnotsErr = NURBS surface has wrong number of knots in s-direction: has {0,number,integer}, needs {1,number,integer} knots.
srfWrongNumTKnotsErr = NURBS surface has wrong number of knots in t-direction: has {0,number,integer}, needs {1,number,integer} knots.

# ControlPointNet class
# irregMatrixErr: {0} = name string
irregMatrixErr = The {0} matrix must be regular/rectangular.

# CurveFactory class
# seExponent: {0} = name of exponent.
seExponent = Super-Ellipse exponent "{0}" must be > 0
pntsToClose = Parametric spacing between adjacent points is to small. Thin input points.

# CurveUtils class
reqTwoCurves = Must have at least 2 curves.
degLineInput = A degenerate line (i.e.: zero length line) was input.
nonCoPlanarLines = The input lines are not co-planar.
parallelLinesInput = The input lines are parallel.

# SurfaceFactory class
pointAxisDimErr = The input point and axis must have the same physical dimensions.
crvAxisDimErr = Input axis must have the same physical dimensions as the input curve.
numColsLEQDegErr = The number of columns must exceed the q degree of the surface.
numRowsLEPDegErr = The number of rows must exceed the p degree of the surface.
numPointsInEachColErr = "The number of points in each column in the array must be the same.
# numDPLEnumCPErr: {0} = parametric direction string ("S" or "T")
numDPLEnumCPErr = The number of data points must exceed the number of control points in the {0}-direction.
skinnedNotEnoughCrvs = Not enough curves input to create a skinned surface.

# From the CST sub-package.
approxPntsNotCoplanarErr = The points being approximated must be co-planar.
xhatNotCoincidentWithPntsPlane = The xhat vector must be coincident with the plane of the points.
crvsDifferentOrder = The {0} and {1} curves must be of the same order.
orderNumCoefsErr = The input order and number of input coefficients are inconsistent.
oneDCurve = The input curve must be 1D.
singleBezierSegmentErr = The input curve must represent a single Bezier segment.
