JCAD

Purpose is to provide pure Java based CAD application to the community.

Whats done!

	Core: 
		GeometryModel:

			Topo -> Shell 
					-> Faces
				 -> Face 
					-> Outer and inner loops 
					-> Surface
				 -> Loop 
					-> HalfEdges
				 -> Edge 
					-> Curve
					-> HalfEdge and twin HalfEdge
				 -> HalfEdge 
					-> VertexA and VertexB
			
			Curve -> Nurbs Curve -> Line, Arc, Circle, Bezier
			Surface -> Nurbs Surface -> Plane, Cylindrical, Bezier
					
		Boolean operations:
			- Subtract
			- Add
			- Intersect

		File data exchange to and from core geometry model:
			- STL import/export
			- STEP import
				- 3D Circle
				- 3D Line
				- Plane surface
				- Cylindrical surface
				- Nurb surface
				- BREP Shapes
			
	Client:
	
		Simple 3D viewer
		Plugin Manager
		    - Dependency injection
			- Load new plugins dynamically
		Window Manager
			- Save window layout
			- Drag and drop panels in window
		Controls
			- Loading step/stl files
			- Saving stl files
		
TODO:	
		
	Better canvas controls:
	
		* Zoom should be relative to the mouse
		* Pan should move the view and not rotate the body. 
		
	MachiningModel:
	
		* Thick face
			* 2 parrallel planar faces connected by one shared face per edge
		* Bend
			* 2 parrallel cylindrical faces connected by edges with one shared face between
		* Straight through cutout
			* Removal of material
		* Countersink
			* Removal of material
		* Pocket 
			* Removal of material
		* Fillet 
			* Rounding the edge between faces. 
		* Chamfer
	
	Client tools:
		
		* Planar sketcher. 
			* Viewer mode which locks to a plane.
			    * Initial plane is defined by clicking a face
				* Unlock button(Picture of a lock) Go back to free edit mode. Top Right overlayed on to canvas?
				* Displays tools such path drawing. On the left?
				* ThickFace will have different options?
			* Sketch path using line, arc, circle, rectangle, bezier
				* Change path type to Graphic, Cutout.
		* Drawn paths can be extruded into cut or an addition. Tapper for extrusion possible.
		* Add Bend to side of ThickFace
		* Properties panel of selected
		* View angle(Right, Left, Top, Bottom, Back, Front, Edges as well, Face)
		* Dimensioning (Dimension Between edges and faces. Vertex to vertex maybe?)
		
		
	Feature detection:
		
		* Thick face
		* Bend
		* Straight through cutout
		* Countersink
		* Pocket 
		* Fillet 
		* Chamfer
		
    
